using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace Modules.VRFeatures
{
    public class UIDebug : MonoBehaviour
    {
        public TMP_Text Label;

        void OnEnable()
        {
            Application.logMessageReceived += LogCallback;
        }

        void OnDisable()
        {
            Application.logMessageReceived -= LogCallback;
        }

        void LogCallback(string logString, string stackTrace, LogType type)
        {
            Label.text = "\n" + Time.time.ToString("0.00") + " " + logString + "\n" + stackTrace + "\n" + Label.text;
        }
    }
}
