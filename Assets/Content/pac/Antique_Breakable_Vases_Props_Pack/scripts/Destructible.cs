﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destructible : MonoBehaviour {

    public GameObject brokenVariant;

    private void OnMouseDown()
    {
        GameObject brokenObject = Instantiate(brokenVariant, transform.position, transform.rotation) as GameObject;
        brokenObject.transform.localScale = gameObject.transform.localScale;

        Destroy(gameObject);
    }

   
}
