﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator : MonoBehaviour
{
    [SerializeField] private float minRotationSpeed = 0f;
    [SerializeField] private float maxRotationSpeed = 5f;

    [SerializeField] private float rotationSpeed;
    [SerializeField] private Vector3 rotationAxis = new Vector3(0, 1, 0);

    void Start()
    {
        rotationSpeed = Random.Range(minRotationSpeed, maxRotationSpeed);
    }

    void Update()
    {
        transform.Rotate(rotationAxis, 45 * rotationSpeed * Time.deltaTime, Space.Self);
    }
}
