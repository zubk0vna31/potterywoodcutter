Universal Render Pipeline upgrade
---------------------------------

Supported version: 7.1.8 and up only!

1) Import Garage Workshop pack to your URP project, and run Garage_URP pack!
2) Enjoy!

High Definition Render Pipeline upgrade
---------------------------------------

Supported version: 7.1.8 and up only!

1) Import Garage Workshop to your configured HDRP project!
2) Run the auto-upgrade tool on it (Edit/Render pipeline/project materials to HDRP)
3) When done, run Garage_HDRP unitypackage!
3) Enjoy!