using System;
using System.IO;
using UnityEditor;
using UnityEngine;

public class Screenshot
{
    private const string ScreenshotsDirectory = "Screenshots";
    private const string ScreenshotName = "screenshot";
    private const string ScreenshotPathFormat = ScreenshotsDirectory + "/" + ScreenshotName + "_{0}.png";

    [MenuItem("Golden Ray Utils/Take screenshot %k")]
    static void TakeScreenshot()
    {
        if (!Directory.Exists(ScreenshotsDirectory))
        {
            Directory.CreateDirectory(ScreenshotsDirectory);
        }

        string[] filenames = Directory.GetFiles(ScreenshotsDirectory);

        int nextSceenshotId = 0;
        foreach (var filename in filenames)
        {
            if (filename.StartsWith(ScreenshotName, StringComparison.CurrentCultureIgnoreCase))
            {
                string filenameNoExtention = Path.GetFileNameWithoutExtension(filename);
                int id = int.Parse(filenameNoExtention.Split('_')[1]);
                nextSceenshotId = Math.Max(nextSceenshotId, id + 1);
            }
        }

        ScreenCapture.CaptureScreenshot(string.Format(ScreenshotPathFormat, nextSceenshotId));
    }
}