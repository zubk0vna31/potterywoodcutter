using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Deformer : MonoBehaviour
{
    public float Radius = 5;

    public Vector3 Position
    {
        get => _transform.position;
    }

    private Transform _transform;

    private void Awake()
    {
        _transform = transform;
    }
}
