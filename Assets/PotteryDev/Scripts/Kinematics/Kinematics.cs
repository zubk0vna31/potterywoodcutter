using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Connection
{
    public Kinematics Point;
    public float Distance;
}

public class Kinematics : MonoBehaviour
{
    public List<Connection> ConnectedKinematics = new List<Connection>();
    public bool UseGravity;
    public bool IsKinematic;

    [HideInInspector]
    public bool IsActive;

    public Vector3 Position
    {
        get => _transform.position;
        set => _transform.position = value;
    }

    public Vector3Int CurrentPositionInt
    {
        get => new Vector3Int(Mathf.RoundToInt(Position.x), Mathf.RoundToInt(Position.y), Mathf.RoundToInt(Position.z));
    }
    public Vector3Int LastPositionInt
    {
        get => _lastPositionInt;
        set => _lastPositionInt = value;
    }

    public Vector3 Velocity
    {
        get => _velocity;
        set => _velocity = value;
    }

    public float Speed
    {
        get => Velocity.magnitude;
    }

    private Transform _transform;
    private Vector3 _velocity;
    private Vector3Int _lastPositionInt = Vector3Int.one * int.MaxValue;

    private void Awake()
    {
        _transform = transform;
    }

    public void UpdatePosition()
    {
        if (IsKinematic || IsActive)
        {
            Velocity = Vector3.zero;
            return;
        }

        if (UseGravity)
            AddForce(Physics.gravity);

        _transform.position += _velocity * Time.fixedDeltaTime;
    }

    public void AddForce(Vector3 force, ForceMode forceMode = ForceMode.Acceleration)
    {
        switch (forceMode)
        {
            case ForceMode.Acceleration:
            _velocity += force * Time.fixedDeltaTime;
            break;

            case ForceMode.VelocityChange:
            _velocity += force;
            break;
        }
    }
}
