
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConnectedPoints : MonoBehaviour
{
    public Deformer[] deformers;

    [Header("Physics")]
    public Kinematics[] Points;
    public float Distance = 3;
    public float MaxForce = 10;
    public float Spring = 100;
    public float Damper = 10;

    [Header("Generation")]
    public int Resolution = 3;
    public Kinematics PointPrefab;

    //public MarchingCubes MarchingCubes;
    
    private void Awake()
    {
        GeneratePointGrid();

        Container container = GetComponent<Container>();
        container.metaBalls = Points;

        //MCBlob MCBlob = GetComponent<MCBlob>();
        //MCBlob.BlobObjectsLocations = Points;
    }

    void GeneratePointGrid()
    {
        //Container container = GetComponent<Container>();
        //container.metaBalls = new MetaBall[Resolution * Resolution * Resolution];
        Points = new Kinematics[Resolution * Resolution * Resolution];
        //container.metaBalls = new MetaBall[Resolution * Resolution * Resolution];
        int i = 0;
        for (int x = 0; x < Resolution; x++)
        {
            for (int y = 0; y < Resolution; y++)
            {
                for (int z = 0; z < Resolution; z++)
                {
                    Vector3 position = new Vector3(x, y, z) * Distance;
                    Kinematics point = Instantiate(PointPrefab, position, Quaternion.identity);
                    //point.transform.parent = transform;
                    Points[i] = point;
                    //container.metaBalls[i] = point.GetComponent<MetaBall>();
                    i++;
                }
            }
        }

        foreach (var point in Points)
        {
            foreach (var otherPoint in Points)
            {
                if (point == otherPoint) continue;

                float distance = (otherPoint.Position - point.Position).magnitude;
                if (distance < Distance * 2)
                {
                    Connection connection = new Connection();
                    connection.Point = otherPoint;
                    connection.Distance = distance;
                    point.ConnectedKinematics.Add(connection);
                }

            }
        }

        //FindNeighbours();
    }
    /*
    void FindNeighbours()
    {
        foreach (Kinematics point in Points)
        {

            foreach (Kinematics otherPoint in Points)
            {
                if (point == otherPoint) continue;

                float distance = (otherPoint.Position - point.Position).magnitude;
                if (distance <= Distance)
                {
                    if (!point.ConnectedKinematics.Contains(otherPoint))
                        point.ConnectedKinematics.Add(otherPoint);
                }
                else
                {
                    bool canDetach = false;
                    foreach (Kinematics connectedPoint in point.ConnectedKinematics)
                    {
                        if (connectedPoint == otherPoint) continue;

                        foreach (Kinematics othersConnectedPoint in otherPoint.ConnectedKinematics)
                        {
                            if (othersConnectedPoint == point) continue;

                            if (connectedPoint == othersConnectedPoint)
                            {
                                canDetach = true;
                                break;
                            }
                        }
                        if (canDetach)
                            break;
                    }

                    if (canDetach)
                        point.ConnectedKinematics.Remove(otherPoint);
                }
            }
        }
    }
    */
    private void FixedUpdate()
    {

        PushPoints();

        foreach (Kinematics point in Points)
        {
            //moving towards another point
            Vector3 avgAcceleration = Vector3.zero;
            int count = point.ConnectedKinematics.Count;
            
            foreach (Connection target in point.ConnectedKinematics)
            {
                avgAcceleration += CalculateAcceleration(point, target);
            }

            if (count > 0)
                avgAcceleration /= count;

            point.AddForce(avgAcceleration, ForceMode.Acceleration);
            point.UpdatePosition();
            
            //voxels
                //MarchingCubes.UpdateVoxels(point);
        }
        
            //MarchingCubes.CreateMesh();
    }


    void PushPoints()
    {
        foreach (Deformer deformer in deformers)
        {
            foreach (Kinematics point in Points)
            {
                point.IsActive = false;
                Vector3 vector = point.Position - deformer.Position;

                if (vector.magnitude >= deformer.Radius) continue;

                point.IsActive = true;

                Vector3 direction = vector.normalized;
                point.Position = deformer.Position + direction * deformer.Radius;
                point.Velocity = Vector3.zero;
                //point.AddForce(-point.Velocity / Time.deltaTime * 10, ForceMode.Acceleration);
            }
        }
    }

    private Vector3 CalculateAcceleration(Kinematics point, Connection target)
    {
        Vector3 vector = target.Point.Position - point.Position;
        Vector3 direction = vector.normalized;

        float force = (vector.magnitude - target.Distance) * Spring;
        force = Mathf.Clamp(force, -MaxForce, MaxForce);

        Vector3 acceleration = force * direction;
        acceleration -= point.Velocity * Damper;
        return acceleration;
    }
}
