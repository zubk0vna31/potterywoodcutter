using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Ring
{
    public Vector2 Position;
    public float Width;
    
    public Ring(Vector2 position, float width)
    {
        Position = position;
        Width = width;
    }

    public void SetData(Vector2 offset, float width)
    {
        Position = offset;
        Width = width;
    }

    public void SetHeight(float height)
    {
        Position.y = height;
    }
    public void SetOffset(float offset)
    {
        Position.x = offset;
    }
}

public class MeshRing
{
    public Vector2 Position;
    public Vector3[] Vertices;

    public float Shadow;

    public RingType Type;

    public Vector2 VectorToNext;
    public Vector2 VectorToPrev;

    private int _currentId = 0;
    
    public MeshRing(int count)
    {
        Vertices = new Vector3[count];
    }

    public void CreateRing(Vector2 position, RingType type)
    {
        Position = position;
        Type = type;
        if (type != RingType.Default)
            Position.x = 0;

        float angleStep = 360f / Vertices.Length;

        for (int j = 0; j < Vertices.Length; j++)
        {
            Quaternion rotation = Quaternion.Euler(0, angleStep * j, 0);
            Add(rotation * position);
        }
    }

    public void Add(Vector3 point)
    {
        Vertices[_currentId] = point;
        _currentId++;
    }

    public void SetPosition(Vector2 position)
    {
        Position = position;

        float angleStep = 360f / Vertices.Length;

        for (int j = 0; j < Vertices.Length; j++)
        {
            Quaternion rotation = Quaternion.Euler(0, angleStep * j, 0);
            Vertices[j] = rotation * position;
        }
    }

    public Vector3 GetPosition(int i)
    {
        return Vertices[i];
    }

    public Vector3 GetPosition()
    {
        return new Vector3(0, Position.y, 0);
    }
}

public enum RingType
{
    Default,
    OpeningPoint,
    ClosingPoint
}