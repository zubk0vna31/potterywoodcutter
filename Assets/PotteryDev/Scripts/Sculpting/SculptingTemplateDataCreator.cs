#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;

namespace PWS.Pottery.ItemDataHolderService
{
    public class SculptingTemplateDataCreator : MonoBehaviour
    {
        [ContextMenu("Create")]
        void Create()
        {
            SculptingDataEditController dataController = GetComponent<SculptingDataEditController>();

            SculptingTemplateData asset = ScriptableObject.CreateInstance<SculptingTemplateData>();
            //asset.Data = new ItemDataHolder(dataController.Data);

            AssetDatabase.CreateAsset(asset, "Assets/PotteryDev/Data/SculptingTemplateData.asset");
            AssetDatabase.SaveAssets();

        }
    }
}
#endif