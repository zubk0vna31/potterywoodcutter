using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PotterySculptTool : MonoBehaviour
{
    public SculptingDataController DataController;

    public bool HoleTool;
    public bool PushTool;
    public bool HeightTool;

    private Vector3 _position;
    private float _radius = 0.1f;
    private Transform _tr;

    private Vector3 mousePositionLast;

    // Start is called before the first frame update
    void Start()
    {
        _tr = transform;
        _position = _tr.position;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 mousePosition = Input.mousePosition;
        if (Input.GetMouseButton(0))
        {
            _position += (mousePosition - mousePositionLast) * 0.002f;
            _tr.position = _position;

            Hole();
            Height();
            Push();
        }
        mousePositionLast = mousePosition;

        _radius += Input.mouseScrollDelta.y * 0.01f;
        _tr.localScale = Vector3.one * _radius * 2;
    }

    void Height()
    {
        if (!HeightTool)
            return;

        bool canGrow = false;
        foreach (Ring ring in DataController.Data.Rings)
        {
            float halfWidht = ring.Width / 2;
            Vector3 ringPoint = DataController.transform.position + Vector3.up * ring.Position.y;
            Vector3 vectorToRing = ringPoint - _position;

            if (vectorToRing.magnitude < Mathf.Abs(ring.Position.x) + _radius)
                canGrow = true;
        }

        if (!canGrow) return;

        float height = _tr.position.y - DataController.transform.position.y;
        if (height > DataController.Data.ProductHeight)
        {
            if (height - _radius < DataController.Data.ProductHeight)
            {
                DataController.SetProductHeight(height - _radius);
            }
        }
        else
        {
            if (height + _radius > DataController.Data.ProductHeight)
            {
                DataController.SetProductHeight(height + _radius);
            }
        }
    }

    void Hole()
    {
        if (!HoleTool)
            return;

        bool canMakeHole = false;
        foreach (Ring ring in DataController.Data.Rings)
        {
            float halfWidht = ring.Width / 2;
            Vector3 ringPoint = DataController.transform.position + Vector3.up * ring.Position.y;
            Vector3 vectorToRing = ringPoint - _position;

            if (vectorToRing.magnitude < Mathf.Abs(ring.Position.x) + _radius)
                canMakeHole = true;
        }

        if (!canMakeHole) return;

        float height = _tr.position.y - DataController.transform.position.y;
        if (height > DataController.Data.TopClosureHeight)
        {
            if (height - _radius < DataController.Data.TopClosureHeight)
            {
                DataController.SetTopClosureHeight(height - _radius);
            }
        }
    }

    void Push()
    {
        if (!PushTool)
            return;

        foreach (Ring ring in DataController.Data.Rings)
        {
            float halfWidht = ring.Width / 2;
            Vector3 ringPoint = DataController.transform.position + Vector3.up * ring.Position.y;
            Vector3 vectorToRing = ringPoint - _position;


            float verticalAmount = Mathf.Clamp01(Mathf.Abs(vectorToRing.y) / (_radius + halfWidht));
            verticalAmount = Mathf.Cos(verticalAmount * Mathf.PI / 2);

            if (Mathf.Abs(vectorToRing.y) > (_radius + halfWidht))
                continue;

            if (HorizontalDistance(vectorToRing) > ring.Position.x)
            {
                float offset = HorizontalDistance(vectorToRing) - (_radius + halfWidht) * verticalAmount;
                offset = Mathf.Max(0, offset);

                if (offset < ring.Position.x)
                {
                    DataController.SetRingOffset(ring, offset);
                }
            }
            else
            {
                float offset = HorizontalDistance(vectorToRing) + (_radius + halfWidht) * verticalAmount;
                offset = Mathf.Max(0, offset);

                if (offset > ring.Position.x)
                {
                    DataController.SetRingOffset(ring, offset);
                }
            }
        }
    }

    float HorizontalDistance(Vector3 vectorToRing)
    {
        return Mathf.Max(Mathf.Abs(vectorToRing.x), Mathf.Abs(vectorToRing.z));
    }
}
