using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SculptingData
{
    public float ProductHeight;
    public float BottomClosureHeight;
    public float TopClosureHeight;

    public List<Ring> Rings;

    public Ring TopRing
    {
        get => Rings[Rings.Count - 1];
    }
    public Ring BottomRing
    {
        get => Rings[0];
    }

    public SculptingData(SculptingData data)
    {
        BottomClosureHeight = data.BottomClosureHeight;
        TopClosureHeight = data.TopClosureHeight;
        ProductHeight = data.ProductHeight;

        Rings = new List<Ring>();
        foreach (Ring ring in data.Rings)
        {
            Rings.Add(new Ring(ring.Position, ring.Width));
        }
    }

    public SculptingData(float productHeight, float productRadius, float wallWidth)
    {
        BottomClosureHeight = 0;
        TopClosureHeight = productHeight;
        ProductHeight = productHeight;

        Rings = new List<Ring>();
        Rings.Add(new Ring(new Vector2(productRadius, 0), wallWidth));
    }

}
