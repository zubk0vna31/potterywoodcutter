using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SculptingTemplateData", menuName = "PWS/Pottery", order = 1)]
public class SculptingTemplateData : ScriptableObject
{
    public SculptingData Data;
}
