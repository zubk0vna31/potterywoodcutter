using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationController : MonoBehaviour
{
    public float Speed = 1;

    private Transform _tr;

    private void Start()
    {
        _tr = transform;
    }

    // Update is called once per frame
    void Update()
    {
        _tr.Rotate(_tr.up, Speed * Time.deltaTime);
    }
}
