﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SculptingMesher : MonoBehaviour
{
    [Header("Mesh Settings")]
    public int Vertices = 16;

    [Header("Smoothing")]
    [Range(0, 180)]
    public float SmoothAngleThreshold = 140;
    [Range(0, 10)]
    public int SmoothSegments = 1;
    [Range(0, 1f)]
    public float SmoothLerpValue = 0.5f;
    public float SmoothMaxOffset = 0.5f;

    private List<MeshRing> _meshRings = new List<MeshRing>();
    private int _verticesCount;
    private List<Vector3> _vertices = new List<Vector3>();
    private List<Vector2> _uvs = new List<Vector2>();
    private List<int> _triangles = new List<int>();

    private MeshFilter _meshFilter;
    private Mesh _mesh;
    private Transform _tr;
    private SculptingDataController _dataController;

    // Start is called before the first frame update
    void Start()
    {
        _tr = transform;
        _meshFilter = GetComponent<MeshFilter>();
        _dataController = GetComponent<SculptingDataController>();
        _mesh = new Mesh();
        _mesh.MarkDynamic();
        _meshFilter.mesh = _mesh;
    }
    
    void Update()
    {
        UpdateMesh();
    }

    void UpdateMesh()
    {
        if (!_dataController) return;
        if (_dataController.Data == null) return;
        if (!_dataController.IsUpdated) return;

        if (Vertices < 3) return;

        _meshRings.Clear();
        _verticesCount = 0;

        bool hasHole = _dataController.Data.BottomClosureHeight > _dataController.Data.TopClosureHeight;

        if (!hasHole)
        {
            //bottom central point
            AddMeshRing(new Vector2(0, _dataController.Data.BottomClosureHeight), RingType.OpeningPoint);

            //inside surface from central point height to bottom ring
            AddInsidePoints(true);

        }

        //bottom edge mesh
        AddClosureMeshRing(0, true);

        //outside surface
        AddOutsideRings();

        //top edge mesh
        AddClosureMeshRing(_dataController.Data.Rings.Count - 1, false);

        if (!hasHole)
        {
            //inside surface from top ring to central point height
            AddInsidePoints(false);

            //top central point
            AddMeshRing(new Vector2(0, _dataController.Data.TopClosureHeight), RingType.ClosingPoint);
        }

        if (hasHole)
        {
            int firstRing = 0;
            int lastRing = _dataController.Data.Rings.Count - 1;
            for (int i = lastRing; i >= firstRing; i--)
            {
                AddSurfaceMeshRing(i, false, firstRing, lastRing);
            }
        }

        SmoothMeshRings();

        CreateMesh();

        _dataController.IsUpdated = false;
    }

    void AddOutsideRings()
    {
        int firstRing = 0;
        int lastRing = _dataController.Data.Rings.Count - 1;
        for (int i = firstRing; i <= lastRing; i++)
        {
            AddSurfaceMeshRing(i, true, firstRing, lastRing);
        }
    }

    void AddInsidePoints(bool isBottom)
    {
        int firstRing = 0;
        int lastRing = _dataController.Data.Rings.Count - 1;

        for (int i = lastRing; i >= firstRing; i--)
        {
            bool canAddRing = isBottom
                ? _dataController.Data.Rings[i].Position.y + _dataController.DefaulRingsOffset < _dataController.Data.BottomClosureHeight 
                : _dataController.Data.Rings[i].Position.y - _dataController.DefaulRingsOffset > _dataController.Data.TopClosureHeight;
            if (canAddRing)
            {
                AddSurfaceMeshRing(i, false, firstRing, lastRing);
            }

            if (!isBottom && i < lastRing)
            {
                Vector2 position0 = _dataController.Data.Rings[i].Position;
                Vector2 position1 = _dataController.Data.Rings[i + 1].Position;

                if (_dataController.Data.TopClosureHeight >= position0.y && _dataController.Data.TopClosureHeight < position1.y)
                {
                    Vector2 vectorToTopRing = position1 - position0;
                    float lerp = (_dataController.Data.TopClosureHeight - position0.y) / vectorToTopRing.y;
                    Vector2 lerpPosition = Vector2.Lerp(position0, position1, lerp);
                    float lerpWidth = Mathf.Lerp(_dataController.Data.Rings[i].Width, _dataController.Data.Rings[i + 1].Width, lerp);
                    AddMeshRing(lerpPosition - Vector2.right * lerpWidth / 2);
                }
            }
        }

    }

    private void AddClosureMeshRing(int ringId, bool isBottomClosure)
    {
        int bottomSign = isBottomClosure ? 1 : -1;
        float halfWidth = _dataController.Data.Rings[ringId].Width / 2;

        Vector2 curPosition = _dataController.Data.Rings[ringId].Position;
        Vector2 nextPosition = _dataController.Data.Rings[ringId + bottomSign].Position;
        Vector2 vectorToNextPoint = (nextPosition - curPosition).normalized;

        Vector2 perpendicularVectorToNextPoint = Vector2.Perpendicular(vectorToNextPoint);

        Vector2 surfacePoint0 = curPosition + perpendicularVectorToNextPoint * halfWidth;
        Vector2 surfacePoint1 = curPosition - perpendicularVectorToNextPoint * halfWidth;

        if (isBottomClosure && _dataController.Data.Rings[ringId].Position.y >= _dataController.Data.BottomClosureHeight)
            surfacePoint0.y = surfacePoint1.y = _dataController.Data.BottomClosureHeight;

        if (!isBottomClosure && _dataController.Data.Rings[ringId].Position.y <= _dataController.Data.TopClosureHeight)
            surfacePoint0.y = surfacePoint1.y = _dataController.Data.TopClosureHeight;

        AddMeshRing(surfacePoint0);
        AddMeshRing(surfacePoint1);

    }

    private void AddSurfaceMeshRing(int ringId, bool outside, int firstRing, int lastRing)
    {
        Vector2 intersection = FindIntersection(ringId, outside, firstRing, lastRing);
        if (intersection == Vector2.zero)
            return;

        AddMeshRing(intersection);
    }

    Vector2 FindIntersection(int ringId, bool outside, int firstRing, int lastRing)
    {
        int outsideSign = outside ? 1 : -1;
        float halfWidth = _dataController.Data.Rings[ringId].Width / 2;

        if (ringId > firstRing && ringId < lastRing)
        {
            int prev = ringId - outsideSign;
            int next = ringId + outsideSign;

            Vector2 vectorToPrevPoint = (_dataController.Data.Rings[prev].Position - _dataController.Data.Rings[ringId].Position).normalized;
            Vector2 vectorToNextPoint = (_dataController.Data.Rings[next].Position - _dataController.Data.Rings[ringId].Position).normalized;

            float angleBetweenVectors = Vector2.SignedAngle(vectorToPrevPoint, vectorToNextPoint);
            float angleBetweenVectorsAbs = Mathf.Abs(angleBetweenVectors);

            if (angleBetweenVectorsAbs != 180)
            {
                Vector2 perpendicularVectorToPrevPoint = Vector2.Perpendicular(vectorToPrevPoint);
                Vector2 perpendicularVectorToNextPoint = -Vector2.Perpendicular(vectorToNextPoint);

                Vector2 surfacePointPrev = _dataController.Data.Rings[prev].Position + perpendicularVectorToPrevPoint * halfWidth;
                Vector2 surfacePointNext = _dataController.Data.Rings[next].Position + perpendicularVectorToNextPoint * halfWidth;

                Vector2 surfaceIntersectionPoint = VectorHelper.LineLineIntersection(surfacePointPrev, -vectorToPrevPoint * 100, surfacePointNext, -vectorToNextPoint * 100);

                return surfaceIntersectionPoint;
            }
            else
            {
                Vector2 position = _dataController.Data.Rings[ringId].Position + Vector2.right * halfWidth * outsideSign;

                return position;
            }
        }
        else return Vector2.zero;

    }

    private void AddMeshRing(Vector2 point, RingType pointType = RingType.Default)
    {
        MeshRing meshRing = new MeshRing(Vertices);
        meshRing.CreateRing(point, pointType);
        _meshRings.Add(meshRing);

        _verticesCount += Vertices;
    }

    private void InsertMeshRing(int i, Vector2 point)
    {
        MeshRing meshRing = new MeshRing(Vertices);
        meshRing.CreateRing(point, RingType.Default);
        _meshRings.Insert(i, meshRing);

        _verticesCount += Vertices;
    }

    void RemoveMeshRing(int i)
    {
        _meshRings.RemoveAt(i);
        _verticesCount -= Vertices;
    }

    void SmoothMeshRings()
    {
        if (_meshRings.Count < 1)
            return;
        if (SmoothSegments <= 0)
            return;

        for (int i = 0; i < _meshRings.Count; i++)
        {
            if (_meshRings[i].Type != RingType.Default)
                continue;

            int next = (int)Mathf.Repeat(i + 1, _meshRings.Count);
            int prev = (int)Mathf.Repeat(i - 1, _meshRings.Count);

            _meshRings[i].VectorToPrev = (_meshRings[prev].Position - _meshRings[i].Position);
            _meshRings[i].VectorToNext = (_meshRings[next].Position - _meshRings[i].Position);
        }

        int cur = 0;
        while (cur < _meshRings.Count)
        {
            if (_meshRings[cur].Type != RingType.Default)
            {
                cur++;
                continue;
            }

            Vector2 position = _meshRings[cur].Position;
            Vector2 vectorToNext = _meshRings[cur].VectorToNext;
            Vector2 vectorToPrev = _meshRings[cur].VectorToPrev;

            float angle = Vector2.SignedAngle(vectorToPrev, vectorToNext);
            float angleAbs = Mathf.Abs(angle);

            if (angleAbs >= SmoothAngleThreshold)
            {
                Vector2 vectorToPrevLerp = vectorToPrev * SmoothLerpValue;
                vectorToPrevLerp = Vector2.ClampMagnitude(vectorToPrevLerp, SmoothMaxOffset);
                Vector2 vectorToNextLerp = vectorToNext * SmoothLerpValue;
                vectorToNextLerp = Vector2.ClampMagnitude(vectorToNextLerp, SmoothMaxOffset);

                Vector2 smoothVector = (vectorToPrevLerp + vectorToNextLerp) / 2;
                Vector2 smoothPosition = position + smoothVector;
                _meshRings[cur].SetPosition(smoothPosition);
                cur++;
            }
            else
            {
                RemoveMeshRing(cur);

                int segments = SmoothSegments + 1;
                for (int seg = 1; seg < segments; seg++)
                {
                    float lerp = (float) seg / segments;
                    float lerpInverse = 1 - lerp;

                    Vector2 vectorToPrevLerp = vectorToPrev * SmoothLerpValue;
                    vectorToPrevLerp = Vector2.ClampMagnitude(vectorToPrevLerp, SmoothMaxOffset);
                    Vector2 vectorToNextLerp = vectorToNext * SmoothLerpValue;
                    vectorToNextLerp = Vector2.ClampMagnitude(vectorToNextLerp, SmoothMaxOffset);

                    Vector2 prevPosition = position + vectorToPrevLerp;
                    Vector2 nextPosition = position + vectorToNextLerp;

                    Vector2 position1 = Vector2.Lerp(prevPosition, position, lerp);
                    Vector2 position2 = Vector2.Lerp(position, nextPosition, lerp);
                    Vector2 smoothPosition = Vector2.Lerp(position1, position2, lerp);

                    InsertMeshRing(cur, smoothPosition);
                    cur++;
                }
            }
        }
    }

    void CreateMesh()
    {
        _vertices.Clear();
        _uvs.Clear();
        _triangles.Clear();

        for (int i = 0; i < _meshRings.Count; i++)
        {
            int ring = _vertices.Count;

            int nextMeshRingId = (i + 1) % _meshRings.Count;
            MeshRing nextMeshRing = _meshRings[nextMeshRingId];

            if (_meshRings[i].Type == RingType.OpeningPoint)
            {
                _vertices.Add(_meshRings[i].GetPosition());
                _uvs.Add(new Vector2(0, _meshRings[i].Position.y));

                if (nextMeshRing.Type != RingType.Default)
                    continue;

                for (int j = 0; j < Vertices; j++)
                {
                    int outA = ring;
                    int outB = ring + 1 + (1 + j) % Vertices;
                    int outC = ring + 1 + j;

                    outA %= _verticesCount;
                    outB %= _verticesCount;
                    outC %= _verticesCount;

                    _triangles.Add(outA);
                    _triangles.Add(outB);
                    _triangles.Add(outC);
                }
                continue;
            }
            else if (_meshRings[i].Type == RingType.ClosingPoint)
            {
                _vertices.Add(_meshRings[i].GetPosition());
                _uvs.Add(new Vector2(0, _meshRings[i].Position.y));
                continue;
            }


            for (int j = 0; j < Vertices; j++)
            {
                _vertices.Add(_meshRings[i].GetPosition(j));
                _uvs.Add(new Vector2((float)j / Vertices, _meshRings[i].Position.y));

                if (nextMeshRing.Type == RingType.ClosingPoint)
                {

                    int outA = ring + j;
                    int outB = ring + (j + 1) % Vertices;
                    int outC = ring + Vertices;

                    outA %= _verticesCount;
                    outB %= _verticesCount;
                    outC %= _verticesCount;

                    _triangles.Add(outA);
                    _triangles.Add(outB);
                    _triangles.Add(outC);
                }
                else if (nextMeshRing.Type == RingType.Default)
                {
                    int outA = ring + j;
                    int outB = ring + (j + 1) % Vertices;
                    int outC = ring + j + Vertices;
                    int outD = ring + (j + 1) % Vertices + Vertices;

                    outA %= _verticesCount;
                    outB %= _verticesCount;
                    outC %= _verticesCount;
                    outD %= _verticesCount;

                    _triangles.Add(outA);
                    _triangles.Add(outB);
                    _triangles.Add(outC);

                    _triangles.Add(outB);
                    _triangles.Add(outD);
                    _triangles.Add(outC);
                }
            }
        }

        _mesh.Clear();
        _mesh.vertices = _vertices.ToArray();
        _mesh.uv = _uvs.ToArray();
        _mesh.triangles = _triangles.ToArray();
        _mesh.RecalculateNormals();
    }
}
