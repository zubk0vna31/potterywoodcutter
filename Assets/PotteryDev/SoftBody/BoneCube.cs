using UnityEngine;

//complete list of unity inpector attributes https://docs.unity3d.com/ScriptReference/AddComponentMenu.html?_ga=2.45747431.2107391006.1601167752-1733939537.1520033247
//inspector attributes https://unity3d.college/2017/05/22/unity-attributes/
//Nvidia Flex video https://youtu.be/TNAKv1dkYyQ

public class BoneCube : MonoBehaviour
{
    /*
        0-----1-----2     9----10----11    18----19----20
        |     |     |     |     |     |     |     |     |
        |     |     |     |     |     |     |     |     |
        3-----4-----5    12----13----14    21----22----23
        |     |     |     |     |     |     |     |     |
        |     |     |     |     |     |     |     |     |
        6-----7-----8    15----16----17    24----25----26
     */
    [Header("Bones")]
    public GameObject[] Bones = null;
    public float Resolution = 2;
    [Header("Spring Joint Settings")]
    [Tooltip("Strength of spring")]
    public float Spring = 100f;
    [Tooltip("Higher the value the faster the spring oscillation stops")]
    public float Damper = 0.2f;
    [Header("Other Settings")]
    public LineRenderer PrefabLine = null;
    public bool ViewLines = true;

    ref GameObject GetBone(int x, int y, int z) {
        int res = (int) Resolution;
        int res2 = (int) (Resolution * Resolution);

        int i = x + y * res + z * res2;
        return ref Bones[i];
    }

    private void Start()
    {
        Softbody.Init(Spring, Damper, RigidbodyConstraints.None, PrefabLine, ViewLines);

        /*for (int i = 0; i < Bones.Length; i++)
        {
            Softbody.AddCollider(ref Bones[i]);
        }*/

        for (int i = 0; i < Bones.Length; i++)
        {
            if (i == 13) continue;

            Softbody.AddSpring(ref Bones[i], ref Bones[13]);
        }

        Softbody.AddSpring(ref GetBone(0, 0, 0), ref GetBone(1, 1, 0));
        Softbody.AddSpring(ref GetBone(1, 0, 0), ref GetBone(1, 1, 0));
        Softbody.AddSpring(ref GetBone(2, 0, 0), ref GetBone(1, 1, 0));
        Softbody.AddSpring(ref GetBone(2, 1, 0), ref GetBone(1, 1, 0));
        Softbody.AddSpring(ref GetBone(2, 2, 0), ref GetBone(1, 1, 0));
        Softbody.AddSpring(ref GetBone(1, 2, 0), ref GetBone(1, 1, 0));
        Softbody.AddSpring(ref GetBone(0, 2, 0), ref GetBone(1, 1, 0));
        Softbody.AddSpring(ref GetBone(0, 1, 0), ref GetBone(1, 1, 0));

        Softbody.AddSpring(ref GetBone(0, 0, 2), ref GetBone(1, 1, 2));
        Softbody.AddSpring(ref GetBone(1, 0, 2), ref GetBone(1, 1, 2));
        Softbody.AddSpring(ref GetBone(2, 0, 2), ref GetBone(1, 1, 2));
        Softbody.AddSpring(ref GetBone(2, 1, 2), ref GetBone(1, 1, 2));
        Softbody.AddSpring(ref GetBone(2, 2, 2), ref GetBone(1, 1, 2));
        Softbody.AddSpring(ref GetBone(1, 2, 2), ref GetBone(1, 1, 2));
        Softbody.AddSpring(ref GetBone(0, 2, 2), ref GetBone(1, 1, 2));
        Softbody.AddSpring(ref GetBone(0, 1, 2), ref GetBone(1, 1, 2));


        Softbody.AddSpring(ref GetBone(0, 0, 0), ref GetBone(0, 1, 1));
        Softbody.AddSpring(ref GetBone(0, 0, 1), ref GetBone(0, 1, 1));
        Softbody.AddSpring(ref GetBone(0, 0, 2), ref GetBone(0, 1, 1));
        Softbody.AddSpring(ref GetBone(0, 1, 2), ref GetBone(0, 1, 1));
        Softbody.AddSpring(ref GetBone(0, 2, 2), ref GetBone(0, 1, 1));
        Softbody.AddSpring(ref GetBone(0, 2, 1), ref GetBone(0, 1, 1));
        Softbody.AddSpring(ref GetBone(0, 2, 0), ref GetBone(0, 1, 1));
        Softbody.AddSpring(ref GetBone(0, 1, 0), ref GetBone(0, 1, 1));

        Softbody.AddSpring(ref GetBone(2, 0, 0), ref GetBone(2, 1, 1));
        Softbody.AddSpring(ref GetBone(2, 0, 1), ref GetBone(2, 1, 1));
        Softbody.AddSpring(ref GetBone(2, 0, 2), ref GetBone(2, 1, 1));
        Softbody.AddSpring(ref GetBone(2, 1, 2), ref GetBone(2, 1, 1));
        Softbody.AddSpring(ref GetBone(2, 2, 2), ref GetBone(2, 1, 1));
        Softbody.AddSpring(ref GetBone(2, 2, 1), ref GetBone(2, 1, 1));
        Softbody.AddSpring(ref GetBone(2, 2, 0), ref GetBone(2, 1, 1));
        Softbody.AddSpring(ref GetBone(2, 1, 0), ref GetBone(2, 1, 1));


        Softbody.AddSpring(ref GetBone(0, 0, 0), ref GetBone(1, 0, 1));
        Softbody.AddSpring(ref GetBone(1, 0, 0), ref GetBone(1, 0, 1));
        Softbody.AddSpring(ref GetBone(2, 0, 0), ref GetBone(1, 0, 1));
        Softbody.AddSpring(ref GetBone(2, 0, 1), ref GetBone(1, 0, 1));
        Softbody.AddSpring(ref GetBone(2, 0, 2), ref GetBone(1, 0, 1));
        Softbody.AddSpring(ref GetBone(1, 0, 2), ref GetBone(1, 0, 1));
        Softbody.AddSpring(ref GetBone(0, 0, 2), ref GetBone(1, 0, 1));
        Softbody.AddSpring(ref GetBone(0, 0, 1), ref GetBone(1, 0, 1));

        Softbody.AddSpring(ref GetBone(0, 2, 0), ref GetBone(1, 2, 1));
        Softbody.AddSpring(ref GetBone(1, 2, 0), ref GetBone(1, 2, 1));
        Softbody.AddSpring(ref GetBone(2, 2, 0), ref GetBone(1, 2, 1));
        Softbody.AddSpring(ref GetBone(2, 2, 1), ref GetBone(1, 2, 1));
        Softbody.AddSpring(ref GetBone(2, 2, 2), ref GetBone(1, 2, 1));
        Softbody.AddSpring(ref GetBone(1, 2, 2), ref GetBone(1, 2, 1));
        Softbody.AddSpring(ref GetBone(0, 2, 2), ref GetBone(1, 2, 1));
        Softbody.AddSpring(ref GetBone(0, 2, 1), ref GetBone(1, 2, 1));

        /*
        for (int x = 0; x < Resolution; x++)
        {
            for (int y = 0; y < Resolution; y++)
            {
                for (int z = 0; z < Resolution; z++)
                {
                    bool xLess = x < Resolution - 1;
                    bool yLess = y < Resolution - 1;
                    bool zLess = z < Resolution - 1;
                    bool xGreat = x > 0;
                    bool yGreat = y > 0;
                    bool zGreat = z > 0;

                    if (xLess)
                        Softbody.AddSpring(ref GetBone(x, y, z), ref GetBone(x + 1, y, z));
                    if (yLess)
                        Softbody.AddSpring(ref GetBone(x, y, z), ref GetBone(x, y + 1, z));
                    if (zLess)
                        Softbody.AddSpring(ref GetBone(x, y, z), ref GetBone(x, y, z + 1));

                    if (xLess && yLess && zLess)
                        Softbody.AddSpring(ref GetBone(x, y, z), ref GetBone(x + 1, y + 1, z + 1));
                    if (xGreat && yLess && zGreat)
                        Softbody.AddSpring(ref GetBone(x, y, z), ref GetBone(x - 1, y + 1, z - 1));
                    if (xGreat && yLess && zLess)
                        Softbody.AddSpring(ref GetBone(x, y, z), ref GetBone(x - 1, y + 1, z + 1));
                    if (xLess && yLess && zGreat)
                        Softbody.AddSpring(ref GetBone(x, y, z), ref GetBone(x + 1, y + 1, z - 1));
                }
            }
        }*/
    }
}