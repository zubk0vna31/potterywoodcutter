using UnityEngine;

public static class Softbody
{
    #region --- helpers ---
    public enum ColliderShape
    {
        Box,
        Sphere,
    }
    #endregion

    public static float Spring;
    public static float Damper;
    public static LineRenderer PrefabLine;
    public static bool ViewLines;

    public static void Init(float spring, float damper)
    {
        Spring = spring;
        Damper = damper;
        ViewLines = false;
    }
    public static void Init(float spring, float damper, RigidbodyConstraints constraints, LineRenderer prefabline, bool viewlines)
    {
        Spring = spring;
        Damper = damper;
        PrefabLine = prefabline;
        ViewLines = viewlines;
    }

    public static SpringJoint AddSpring(ref GameObject go1, ref GameObject go2)
    {
        SpringJoint sp = AddSpring(ref go1, ref go2, Spring, Damper);

        if (ViewLines == true)
            AddLine(ref go1, ref go2);

        return sp;
    }

    public static LineRenderer AddLine(ref GameObject go1, ref GameObject go2)
    {
        return AddLine(ref go1, ref go2, ref PrefabLine);
    }

    public static SpringJoint AddSpring(ref GameObject go1, ref GameObject go2, float spring, float damper)
    {
        SpringJoint sp = go1.AddComponent<SpringJoint>();
        sp.connectedBody = go2.GetComponent<Rigidbody>();
        sp.spring = spring;
        sp.damper = damper;
        return sp;
    }
    public static LineRenderer AddLine(ref GameObject go1, ref GameObject go2, ref LineRenderer prefab)
    {
        LineRenderer line = Object.Instantiate(prefab);
        line.positionCount = 2;
        line.SetPosition(0, go1.transform.position);
        line.SetPosition(1, go2.transform.position);
        return line;
    }
}