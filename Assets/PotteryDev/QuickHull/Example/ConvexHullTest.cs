﻿/**
 * Copyright 2019 Oskar Sigvardsson
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GK {
	public class ConvexHullTest : MonoBehaviour
    {
        [SerializeField] private Transform[] _points;
        [SerializeField] private float _radius = 0.03f;
        private List<Vector3> _positions = new List<Vector3>();
        private List<Vector3> _spherePoints = new List<Vector3>();

        private ConvexHullCalculator _calc = new ConvexHullCalculator();
        private List<Vector3> _verts = new List<Vector3>();
        private List<int> _tris = new List<int>();
        private List<Vector3> _normals = new List<Vector3>();

        private MeshFilter _meshFilter;

        private Mesh _mesh;

        void Start() {
            _mesh = new Mesh();

            _meshFilter = GetComponent<MeshFilter>();
            _meshFilter.mesh = _mesh;

            _mesh.MarkDynamic();

            _spherePoints = PointsOnSphere(16);

        }

        void Update()
        {
            _positions.Clear();
            _mesh.Clear();

            foreach (Transform tr in _points)
            {
                foreach (Vector3 pt in _spherePoints)
                {
                    _positions.Add(tr.position + pt * _radius);
                }
                //_positions.Add(tr.position);
            }

            _calc.GenerateHull(_positions, false, ref _verts, ref _tris, ref _normals);

            _mesh.SetVertices(_verts);
            _mesh.SetTriangles(_tris, 0);
            //mesh.SetNormals(_normals);
            _mesh.RecalculateNormals();
        }

        List<Vector3> PointsOnSphere(int n)
        {
            List<Vector3> upts = new List<Vector3>();
            float inc = Mathf.PI * (3 - Mathf.Sqrt(5));
            float off = 2.0f / n;
            float x = 0;
            float y = 0;
            float z = 0;
            float r = 0;
            float phi = 0;

            for (var k = 0; k < n; k++)
            {
                y = k * off - 1 + (off / 2);
                r = Mathf.Sqrt(1 - y * y);
                phi = k * inc;
                x = Mathf.Cos(phi) * r;
                z = Mathf.Sin(phi) * r;

                upts.Add(new Vector3(x, y, z));
            }
            return upts;
        }
    }
}
