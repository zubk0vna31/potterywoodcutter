Shader "Custom/PlankPaintable-Color"
{
    Properties
    {
        _DefaultColor ("Default Color",  Color) = (1,1,1,1)
        _CustomColor1 ("Custom Color 1 ",Color) = (1,1,1,1)
        _CustomColor2 ("Custom Color 2 ",Color) = (1,1,1,1)

        [HideInInspector]
        _CustomMask ("", 2D) = "black" {}

        _CustomAmount1("Custom Amount 1",Range(0,1)) = 0
        _CustomAmount2("Custom Amount 2",Range(0,1)) = 0

        _Glossiness ("Smoothness", Range(0,1)) = 0.5
        _Metallic ("Metallic", Range(0,1)) = 0.0
    }
    SubShader
    {
        Tags {"Queue" = "Transparent" "RenderType"="Transparent" } 
          Blend SrcAlpha OneMinusSrcAlpha
        LOD 200

        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf Standard fullforwardshadows alpha:fade

        // Use shader model 3.0 target, to get nicer looking lighting
        #pragma target 3.0

        sampler2D _CustomMask;

        struct Input
        {
            float2 uv_CustomMask;
        };

        fixed _CustomAmount1;
        fixed _CustomAmount2;
        fixed4 _DefaultColor,_CustomColor1,_CustomColor2;

        half _Glossiness;
        half _Metallic;

        // Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
        // See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
        // #pragma instancing_options assumeuniformscaling
        UNITY_INSTANCING_BUFFER_START(Props)
            // put more per-instance properties here
        UNITY_INSTANCING_BUFFER_END(Props)

        void surf (Input IN, inout SurfaceOutputStandard o)
        {
            fixed customMask = tex2D(_CustomMask,IN.uv_CustomMask).r;

            fixed customMask1 = lerp(customMask,1,_CustomAmount1);
            fixed customMask2 = lerp(0,1,_CustomAmount2);

            fixed4 c = lerp(_DefaultColor,_CustomColor1,customMask1);
            c = lerp(c,_CustomColor2,customMask2);

            o.Albedo = c.rgb;
            // Metallic and smoothness come from slider variables
            o.Metallic = _Metallic;
            o.Smoothness = _Glossiness;
            o.Alpha = c.a;
        }
        ENDCG
    }
    FallBack "Diffuse"
}
