Shader "Custom/PlankPaintable"
{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
        _DefaultTexture ("Default Texture", 2D) = "white" {}
        _CustomTexture ("Custom Texture", 2D) = "white" {}

        [HideInInspector]
        _CustomMask ("", 2D) = "black" {}

        _CustomAmount("Custom Amount",Range(0,1)) = 0

        _DefaultGlossiness ("Default Smoothness", Range(0,1)) = 0.5
        _CustomGlossiness ("Custom Smoothness", Range(0,1)) = 0.5
        _Metallic ("Metallic", Range(0,1)) = 0.0
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 200

        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf Standard fullforwardshadows

        // Use shader model 3.0 target, to get nicer looking lighting
        #pragma target 3.0

        sampler2D _DefaultTexture;
        sampler2D _CustomTexture;
        sampler2D _CustomMask;

        struct Input
        {
            float2 uv_DefaultTexture;
            float2 uv_CustomTexture;
            float2 uv_CustomMask;
        };

        fixed _CustomAmount;

        half _DefaultGlossiness,_CustomGlossiness;
        half _Metallic;
        fixed4 _Color;

        // Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
        // See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
        // #pragma instancing_options assumeuniformscaling
        UNITY_INSTANCING_BUFFER_START(Props)
            // put more per-instance properties here
        UNITY_INSTANCING_BUFFER_END(Props)

        void surf (Input IN, inout SurfaceOutputStandard o)
        {
            fixed4 defaultTexture = tex2D(_DefaultTexture,IN.uv_DefaultTexture);
            fixed4 customTexture = tex2D(_CustomTexture,IN.uv_CustomTexture);

            fixed customMask = lerp(tex2D(_CustomMask,IN.uv_CustomMask).r,1,_CustomAmount);

            fixed4 c = lerp(defaultTexture,customTexture,customMask);

            o.Albedo = c.rgb;
            // Metallic and smoothness come from slider variables
            o.Metallic = _Metallic;
            o.Smoothness = lerp(_DefaultGlossiness,_CustomGlossiness,customMask);
            o.Alpha = c.a;
        }
        ENDCG
    }
    FallBack "Diffuse"
}
