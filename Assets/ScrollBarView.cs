using UnityEngine;
using UnityEngine.UI;

public class ScrollBarView : MonoBehaviour
{
    [SerializeField]
    private ScrollRect scrollRect;

    [SerializeField]
    private RectTransform handle;

    [SerializeField]
    private RectTransform slidingArea;

    public void ChangePosition(Vector2 pos)
    {
        float y = Mathf.Lerp(0, slidingArea.sizeDelta.y-handle.sizeDelta.y, 1f-scrollRect.verticalNormalizedPosition);
        handle.anchoredPosition = new Vector2(0f, -y);
    }
}
