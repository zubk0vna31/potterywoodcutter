// Unity built-in shader source. Copyright (c) 2016 Unity Technologies. MIT license (see license.txt)

// Simplified Bumped Specular shader. Differences from regular Bumped Specular one:
// - no Main Color nor Specular Color
// - specular lighting directions are approximated per vertex
// - writes zero to alpha channel
// - Normalmap uses Tiling/Offset of the Base texture
// - no Deferred Lighting support
// - no Lightmap support
// - fully supports only 1 directional light. Other lights can affect it, but it will be per-vertex/SH.

Shader "Custom/ClayCull" {
	Properties{
		_MainTex("Base (RGB) Gloss (A)", 2D) = "white" {}
		_InsideColor("Inside Color",Color) = (0.5,0.5,0.5,1.0)
	}
	SubShader{
		Tags { "RenderType" = "Opaque" }
		LOD 250

		Cull Front

		CGPROGRAM
		#pragma surface surf NoLighting noambient finalcolor:myColor
		sampler2D _MainTex;
		struct Input {
			float2 uv_MainTex;
		};

		fixed4 LightingNoLighting(SurfaceOutput s, fixed3 lightDir, fixed atten) {
			return fixed4(s.Albedo, s.Alpha);
		}

		fixed4 _InsideColor;
		void myColor(Input IN, SurfaceOutput o, inout fixed4 color)
		{
			color *= _InsideColor;
		}
		void surf(Input IN, inout SurfaceOutput o) {
			o.Albedo = 1;
			o.Alpha = 1;
		}
		ENDCG
	}

	FallBack "Mobile/VertexLit"
}