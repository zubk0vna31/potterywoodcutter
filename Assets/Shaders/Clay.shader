Shader "Custom/Clay"
{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
		_OutsideColor("Outside Color",Color) = (0.5,0.5,0.5,1.0)
        _MainTex ("Albedo (RGB)", 2D) = "white" {}
		[NoScaleOffset] _BumpMap("Normalmap", 2D) = "bump" {}

        _Glossiness ("Smoothness", Range(0,1)) = 0.5
        _Metallic ("Metallic", Range(0,1)) = 0.0

		[Header(Wetness)]
		_Wetness("Wetness", Range(0, 1)) = 0
		[PowerSlider(5.0)] _Shininess("Shininess", Range(0.03, 1)) = 0.05
		[PowerSlider(5.0)] _ShininessWet("Shininess Wet", Range(0.03, 1)) = 0.5
		_Brightness("Brightness", Range(0, 1)) = 0.8
		_BrightnessWet("Brightness Wet", Range(0, 1)) = 0.4

		[Header(Drying)]
		_Dryness("Dryness", Range(0, 1)) = 0

		[Header(Firing)]
		_Firing("Firing", Range(0, 1)) = 0
		_FiringSpecular("Firing Specular", Range(0, 1)) = 0

		[Header(Decoration Sculpting)]
		_DecSculptTex("Decoration Sculpting Tex", 2D) = "white" {}
		[NoScaleOffset]
		_DecSculptBumpMap("Decoration Sculpting Bump Map", 2D) = "bump" {}
		_DecSculptPower("Decoration Sculpting Power", Range(0, 1)) = 0.4
		[NoScaleOffset]
		_DecSculptMask("Decoration Sculpting Mask", 2D) = "black" {}
		_DecSculptLineColor("Decoration Sculpting Line Color", Color) = (1,0,0,1.0)

		[Header(Decoration Painting)]
		_DecPaintTex("Decoration Painting Tex", 2D) = "white" {}
		[NoScaleOffset]
		_DecPaintMask("Decoration Painting Mask", 2D) = "black" {}
		_DecPaintColor("Decoration Painting Color", Color) = (1,0,0,1.0)

		[Header(Glazing)]
		_GlazingMask("Glazing Mask", 2D) = "black" {}
		_GlazingColor("Glazing Color", Color) = (1,0,0,1.0)
		_GlazingAlpha("Glazing Alpha", Range(0, 1)) = 0.5
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 200

        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf Standard fullforwardshadows

        // Use shader model 3.0 target, to get nicer looking lighting
        #pragma target 3.0

        sampler2D _MainTex;
		sampler2D _BumpMap;

		//wetness
		half _Shininess;
		half _ShininessWet;
		half _Brightness;
		half _BrightnessWet;
		half _Wetness;

		//dryness
		half _Dryness;

		//firing
		half _Firing;
		half _FiringSpecular;

		//dec sculpt
		sampler2D _DecSculptTex;
		sampler2D _DecSculptBumpMap;
		half _DecSculptPower;
		sampler2D _DecSculptMask;

		//dec paint
		sampler2D _DecPaintTex;
		sampler2D _DecPaintMask;

		//glazing
		sampler2D _GlazingMask;
		half _GlazingAlpha;

        struct Input
        {
			float2 uv_MainTex;
			float2 uv4_DecSculptTex;
			float2 uv3_DecPaintTex;
			float2 uv2_GlazingMask;
			float4 color: Color;
        };


        half _Glossiness;
        half _Metallic;

        fixed4 _Color;
		fixed4 _OutsideColor;
		fixed4 _DecSculptLineColor;
		fixed4 _DecPaintColor;
		fixed4 _GlazingColor;

        // Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
        // See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
        // #pragma instancing_options assumeuniformscaling
        UNITY_INSTANCING_BUFFER_START(Props)
            // put more per-instance properties here
        UNITY_INSTANCING_BUFFER_END(Props)

        void surf (Input IN, inout SurfaceOutputStandard o)
        {
			fixed4 texColor = tex2D(_MainTex, IN.uv_MainTex) * _Color * _OutsideColor;

			//wetness
			fixed wetnessLerp = _Wetness * IN.color.r;
			fixed4 wetnessColor = lerp(_Brightness, _BrightnessWet, wetnessLerp);
			fixed wetShineness = lerp(_Shininess, _ShininessWet, wetnessLerp);
			fixed4 result = texColor * wetnessColor;

			//dryness
			result = lerp(result, 1, _Dryness);

			//dec sculpt
			fixed4 decTex = tex2D(_DecSculptTex, IN.uv4_DecSculptTex);
			fixed4 decMask = tex2D(_DecSculptMask, IN.uv4_DecSculptTex);
			fixed decSculpt = decTex.r * _DecSculptPower + (1 - _DecSculptPower);
			fixed sculpLerp = (1 - decTex.r) * (1 - decMask.r);

			//dec paint
			fixed4 decPaintTex = tex2D(_DecPaintTex, IN.uv3_DecPaintTex);
			fixed4 decPaintMask = tex2D(_DecPaintMask, IN.uv3_DecPaintTex);
			fixed decPaintLerp = (1 - decPaintTex.r) * (1 - decPaintMask.r);
			fixed3 decPaintColor = lerp(result * decSculpt, _DecPaintColor, (1 - decPaintTex.r));

			fixed3 decLineColor = lerp(decPaintColor, _DecSculptLineColor, max(sculpLerp, decPaintLerp));

			//firing
			fixed3 firing = lerp(decLineColor, 0, _Firing);

			//glazing
			fixed4 glazingMask = tex2D(_GlazingMask, IN.uv2_GlazingMask);
			fixed firingSpec10 = clamp(_FiringSpecular * 10, 0, 1);
			fixed3 glazingMasking = lerp(firing, 1, glazingMask.r * (1 - firingSpec10) * 0.7f);
			fixed3 glazingColor = lerp(glazingMasking, _GlazingColor, firingSpec10 * _GlazingAlpha * decPaintTex.r) * decSculpt;
			//fixed3 glazingColor = glazingMasking * lerp(1, _GlazingColor, firingSpec10 * _GlazingAlpha);

            fixed4 c = result;
            o.Albedo = glazingColor.rgb;

			//normals
			fixed3 bumpMap = UnpackScaleNormal(tex2D(_BumpMap, IN.uv_MainTex), IN.color.r);
			fixed3 decSculptBumpMap = UnpackNormal(tex2D(_DecSculptBumpMap, IN.uv4_DecSculptTex));
			o.Normal = lerp(bumpMap, decSculptBumpMap, (1 - decTex.r) * decMask.r);

            // Metallic and smoothness come from slider variables
            o.Metallic = _Metallic;
            o.Smoothness = max(wetShineness, _FiringSpecular);
            o.Alpha = c.a;

        }
        ENDCG
    }
    FallBack "Diffuse"
}
