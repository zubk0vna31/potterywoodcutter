// Unity built-in shader source. Copyright (c) 2016 Unity Technologies. MIT license (see license.txt)

// Simplified Bumped Specular shader. Differences from regular Bumped Specular one:
// - no Main Color nor Specular Color
// - specular lighting directions are approximated per vertex
// - writes zero to alpha channel
// - Normalmap uses Tiling/Offset of the Base texture
// - no Deferred Lighting support
// - no Lightmap support
// - fully supports only 1 directional light. Other lights can affect it, but it will be per-vertex/SH.

Shader "Mobile/Bumped Specular Double Cull" {
	Properties{
		_MainTex("Base (RGB) Gloss (A)", 2D) = "white" {}
		[NoScaleOffset] _BumpMap("Normalmap", 2D) = "bump" {}
		_Color("Color",Color) = (0.5,0.5,0.5,1.0)
		_OutsideColor("Outside Color",Color) = (0.5,0.5,0.5,1.0)
		_InsideColor("Inside Color",Color) = (0.5,0.5,0.5,1.0)

		[Header(Wetness)]
		_Wetness("Wetness", Range(0, 1)) = 0
		[PowerSlider(5.0)] _Shininess("Shininess", Range(0.03, 1)) = 0.05
		[PowerSlider(5.0)] _ShininessWet("Shininess Wet", Range(0.03, 1)) = 0.5
		_Brightness("Brightness", Range(0, 1)) = 0.8
		_BrightnessWet("Brightness Wet", Range(0, 1)) = 0.4

		[Header(Drying)]
		_Dryness("Dryness", Range(0, 1)) = 0
		_LerpWhiteDry("Lerp White Dry", Range(0, 1)) = 0.1

		[Header(Firing)]
		_Firing("Firing", Range(0, 1)) = 0
		_FiringSpecular("Firing Specular", Range(0, 1)) = 0

		[Header(Decoration Sculpting)]
		_DecSculptTex("Decoration Sculpting Tex", 2D) = "white" {}
		[NoScaleOffset] _DecSculptBumpMap("Decoration Sculpting Bump Map", 2D) = "bump" {}
		_DecSculptPower("Decoration Sculpting Power", Range(0, 1)) = 0.4
		[NoScaleOffset] _DecSculptMask("Decoration Sculpting Mask", 2D) = "black" {}
		_DecSculptLineColor("Decoration Sculpting Line Color", Color) = (1,0,0,1.0)

		[Header(Decoration Painting)]
		_DecPaintTex("Decoration Painting Tex", 2D) = "white" {}
		[NoScaleOffset] _DecPaintMask("Decoration Painting Mask", 2D) = "black" {}
		_DecPaintColor("Decoration Painting Color", Color) = (1,0,0,1.0)

		[Header(Glazing)]
		_GlazingMask("Glazing Mask", 2D) = "black" {}
		_GlazingColor("Glazing Color", Color) = (1,0,0,1.0)
		_GlazingAlpha("Glazing Alpha", Range(0, 1)) = 0.5
	}
		SubShader{
			Tags { "RenderType" = "Opaque" }
			LOD 250

		Cull Back

		CGPROGRAM
		#pragma target 3.0
		#pragma surface surf MobileBlinnPhong exclude_path:prepass nolightmap noforwardadd halfasview interpolateview finalcolor:myColor

		inline fixed4 LightingMobileBlinnPhong(SurfaceOutput s, fixed3 lightDir, fixed3 halfDir, fixed atten)
		{
			fixed diff = max(0, dot(s.Normal, lightDir));
			fixed nh = max(0, dot(s.Normal, halfDir));
			fixed spec = pow(nh, s.Specular * 128) * s.Gloss;

			fixed4 c;
			c.rgb = (s.Albedo * _LightColor0.rgb * diff + _LightColor0.rgb * spec) * atten;
			UNITY_OPAQUE_ALPHA(c.a);
			return c;
		}

		sampler2D _MainTex;
		sampler2D _BumpMap;
		half _Shininess;
		half _ShininessWet;
		half _Brightness;
		half _BrightnessWet;
		half _Wetness;

		half _Dryness;
		half _LerpWhiteDry;
		half _Firing;
		half _FiringSpecular;

		sampler2D _DecSculptTex;
		sampler2D _DecSculptBumpMap;
		half _DecSculptPower;
		sampler2D _DecSculptMask;

		sampler2D _DecPaintTex;
		sampler2D _DecPaintMask;

		sampler2D _GlazingMask;
		half _GlazingAlpha;

		struct Input {
			float2 uv_MainTex;
			float2 uv4_DecSculptTex;
			float2 uv4_DecPaintTex;
			float2 uv2_GlazingMask;
			float4 color: Color;
		};

		fixed4 _Color;
		fixed4 _OutsideColor;
		fixed4 _DecSculptLineColor;
		fixed4 _DecPaintColor;
		fixed4 _GlazingColor;

		void myColor(Input IN, SurfaceOutput o, inout fixed4 color)
		{
			fixed4 newColor = lerp(_OutsideColor, 1, _LerpWhiteDry * _Dryness);
			color *= newColor * _Color * lerp(_Brightness, _BrightnessWet, _Wetness * (1 - IN.color.r));
			//color = IN.color;
		}

		void surf(Input IN, inout SurfaceOutput o) {
			fixed4 tex = tex2D(_MainTex, IN.uv_MainTex);

			fixed4 decTex = tex2D(_DecSculptTex, IN.uv4_DecSculptTex);
			fixed4 decMask = tex2D(_DecSculptMask, IN.uv4_DecSculptTex);
			fixed decSculpt = decTex.r * _DecSculptPower + (1 - _DecSculptPower);

			fixed4 decPaintTex = tex2D(_DecPaintTex, IN.uv4_DecPaintTex);
			fixed4 decPaintMask = tex2D(_DecPaintMask, IN.uv4_DecPaintTex);

			fixed4 glazingMask = tex2D(_GlazingMask, IN.uv2_GlazingMask);

			fixed3 texturedColor = lerp(tex.rgb * decSculpt, _DecPaintColor, (1 - decPaintTex.r));

			fixed sculpLerp = (1 - decTex.r) * (1 - decMask.r);
			fixed paintLerp = (1 - decPaintTex.r) * (1 - decPaintMask.r);
			fixed3 newColor = lerp(texturedColor, _DecSculptLineColor, max(sculpLerp, paintLerp));
			fixed3 firing = lerp(newColor, 0, _Firing);
			fixed3 glazingMasking = lerp(firing, 1, glazingMask.r * (1 - _FiringSpecular));
			fixed3 glazingColor = lerp(glazingMasking, _GlazingColor, _FiringSpecular * _GlazingAlpha);
			o.Albedo = glazingColor;
			o.Gloss = _FiringSpecular;
			o.Alpha = tex.a;

			fixed wetShineness = lerp(_Shininess, _ShininessWet, _Wetness * (1 - IN.color.r));
			o.Specular = max(wetShineness, _FiringSpecular);

			float3 fn1 = UnpackScaleNormal(tex2D(_BumpMap, IN.uv_MainTex), (1 - IN.color.r));
			float3 fn2 = UnpackNormal(tex2D(_DecSculptBumpMap, IN.uv4_DecSculptTex)); 
			//o.Normal = fn1 * (1 - decTex.r) + fn2 * decTex.r;
			o.Normal = lerp(fn1, fn2, (1 - decTex.r) * decMask.r);
		}
		ENDCG

		Cull Front

		CGPROGRAM
		#pragma surface surf NoLighting noambient finalcolor:myColor
		sampler2D _MainTex;
		struct Input {
			float2 uv_MainTex;
		};

		fixed4 LightingNoLighting(SurfaceOutput s, fixed3 lightDir, fixed atten) {
			return fixed4(s.Albedo, s.Alpha);
		}

		fixed4 _Color;
		fixed4 _InsideColor;
		void myColor(Input IN, SurfaceOutput o, inout fixed4 color)
		{
			color *= _InsideColor * _Color;
		}
		void surf(Input IN, inout SurfaceOutput o) {
			o.Albedo = 1;
			o.Alpha = 1;
		}
		ENDCG
	}

		FallBack "Mobile/VertexLit"
}