Shader "Unlit/Plank"
{
    Properties
    {
         _TintColor("Tint Color", Color) = (1,1,1,1)

        _NotPolishedTexture ("Not Polished", 2D) = "white" {}
        _PolishedTexture ("Polished", 2D) = "white" {}

        [HideInInspector]
        _PolishMask ("Polish Mask", 2D) = "black" {}

        [HDR]
        _AmbientColor("Ambient Color", Color) = (0.4,0.4,0.4,1)

    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"
            #include "Lighting.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                float3 normal : NORMAL;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
                float3 worldNormal : NORMAL;
            };

            sampler2D _NotPolishedTexture;
            sampler2D _PolishedTexture;

            float4 _NotPolishedTexture_ST;
            float4 _PolishedTexture_ST;

            half4 _AmbientColor,_TintColor;

            sampler2D _PolishMask;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.worldNormal = UnityObjectToWorldNormal(v.normal);
                o.uv = v.uv;
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                float3 normal = normalize(i.worldNormal);
                float NdotL = dot(_WorldSpaceLightPos0, normal);

                float4 light = NdotL * _LightColor0;

                fixed4 notPolished = tex2D(_NotPolishedTexture,i.uv);
                fixed4 polished = tex2D(_PolishedTexture,i.uv);
                fixed4 mask = tex2D(_PolishMask,i.uv);

                float maskValue = (mask.r+mask.g+mask.b)*0.333;

                fixed4 lerpTexture = lerp(notPolished,polished,maskValue);

                // sample the texture
                fixed4 col = lerpTexture*_TintColor*(_AmbientColor+light);
                // apply fog
                UNITY_APPLY_FOG(i.fogCoord, col);
                return col;
            }
            ENDCG
        }
    }
}
