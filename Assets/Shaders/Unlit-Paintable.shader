Shader "Unlit/Paintable_Diffuse"
{
    Properties
    {
        _Color1("Color 1", Color) = (1,1,1,1) 
        _Color2 ("Color 2", Color) = (0,0,1,1) 
        _FinalColor ("Paint With Duration Color", Color) = (0,0,1,1) 
        _Opacity ("Opacity", Range(0,1)) = 0.5

        //[HideInInspector]
        _MaskTexture("Mask",2D) = "black"{}
    }
    SubShader
    {
         Tags {"Queue" = "Transparent" "RenderType"="Transparent" }
       
        Cull Back
        Blend SrcAlpha OneMinusSrcAlpha

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"
            #include "Lighting.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                float3 normal : NORMAL;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
                float3 worldNormal : NORMAL;
            };

            sampler2D _MaskTexture;

            int _PaintAll=0;
            fixed _PaintAllValue; 

            fixed4 _Color1,_Color2,_FinalColor;
            fixed _Opacity;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.worldNormal = UnityObjectToWorldNormal(v.normal);
                o.uv = v.uv;
                return o;
            }

            fixed2 GetUV(fixed2 uv,float4 st)
            {
                return fixed2(uv.xy * st.xy + st.zw);
            }

            fixed4 frag (v2f i) : SV_Target
            {
                float mask = tex2D(_MaskTexture,i.uv).r;

                fixed4 col = lerp(_Color1,_Color2,mask);

                if(_PaintAll>0)
                {
                    col = lerp(col,_FinalColor,_PaintAllValue);
                }

                col.a = _Opacity;

                return col;
            }
            ENDCG
        }
    }
}
