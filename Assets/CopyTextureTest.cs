using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CopyTextureTest : MonoBehaviour
{
    public MeshRenderer source, target;

    public Vector4 copyRect;
    public Vector2 copyTo;

    private Texture2D texture;
    private Texture2D toTexture;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Copy();
        }
    }

    private void Copy()
    {
        texture = source.material.GetTexture("_MainTex") as Texture2D;

        if (toTexture == null)
        {
            toTexture = new Texture2D(texture.width, texture.height,
                TextureFormat.ETC2_RGBA8 ,false);
        }

        int x = (int)(texture.width * copyRect.x);
        int y = (int)(texture.height * copyRect.y);
        int z = (int)(texture.width * copyRect.z);
        int w = (int)(texture.height * copyRect.w);

        int toX = (int)(texture.width * copyRect.x);
        int toY = (int)(texture.height * copyRect.y);

        Debug.Log($"Copy from:" +
            $"{x},{y},{z},{w}");

        Graphics.CopyTexture(texture, 0, 0,
            x, y, z, w,
            toTexture, 0, 0,
            toX, toY);

        target.material.SetTexture("_MainTex", toTexture);

    }

}
