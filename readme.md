# LS-CPR/PotteryWoodcutterVR

---

#### Requirements
- Unity 2020.3.3f1
- git lfs should be installed to pull/push content

--- 

#### Template

- custom

---

### Notes

- empty repo for now

---

#### Included sw-components

Unity official components
- tba

External components
- tba

LS-Internal: Root layer components
- tba

LS-Internal: Extension layer components
- tba

LS-Internal: Feauture layer components
- tba

---