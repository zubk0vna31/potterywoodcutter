using UnityEngine;

namespace Modules.Root.ContainerComponentModel
{
    // use to just add some gameobject from resources as child to the AppContainer 
    public class EmptyMonoInstaller : AMonoInstaller
    {
        public override void Install(IContainer container)
        {
            
        }
    }
}