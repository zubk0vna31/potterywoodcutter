using UnityEngine;

namespace Modules.Root.ContainerComponentModel
{
    /// <summary>
    /// abstract realisation of MonoInstaller
    /// used to wrap installer into MonoBehaviour
    /// </summary>
    public abstract class AMonoInstaller : MonoBehaviour, IInstaller
    {
        public abstract void Install(IContainer container);
    }
}