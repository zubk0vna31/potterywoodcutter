using System;
using System.Collections.Generic;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Modules.Root.ContainerComponentModel
{
    /// <summary>
    /// scope - whole project
    /// </summary>
    public static class AppContainer
    {
        private const string _resourcesPath = "AppContainer";
        
        private static IContainer _instance;
        public static IContainer Instance => _initialized ? _instance : GetInstance();

        private static bool _initialized;
        
        private static IContainer GetInstance()
        {
            Initialize();
            return _instance;
        }

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
        public static void Initialize()
        {
            if (_initialized)
            {
                return;
            }
            
            UnityEngine.Debug.Log("AppContainer: Initializing");

            GameObject containerRoot = new GameObject("--- [AppContainer]");
            UnityEngine.Object.DontDestroyOnLoad(containerRoot);
            containerRoot.AddComponent<OnDestroyDisposeHandler>().SetContainer(_instance);
            
            List<IInstaller> installers = new List<IInstaller>();
            installers.AddRange(Resources.LoadAll<ASOInstaller>(_resourcesPath));
            foreach (var installer in Resources.LoadAll<AMonoInstaller>(_resourcesPath))
            {
                installers.Add(Object.Instantiate(installer, containerRoot.transform));
            }
            UnityEngine.Debug.Log($"AppContainer: {installers.Count } installers detected");
            
            _instance = new Container(installers);
            _instance.Init();
            _initialized = true;
            
            UnityEngine.Debug.Log("AppContainer: Initialized");
        }

        // to reset static members when domain reload disabled in unity
        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.SubsystemRegistration)]
        public static void OnDomainReload()
        {
            _instance = null;
            _initialized = false;
        }


        public static T Component<T>()
        {
            return Instance.Component<T>();
        }

        public static bool SafeGetComponent<T>(out T component)
        {
            return Instance.SafeGetComponent(out component);
        }
        
        public static void Inject<T>(T obj)
        {
            Instance.Inject<T>(obj);
        }

        public static void Bind<T>(T obj)
        {
            Instance.Bind<T>(obj);
        }
    }
}