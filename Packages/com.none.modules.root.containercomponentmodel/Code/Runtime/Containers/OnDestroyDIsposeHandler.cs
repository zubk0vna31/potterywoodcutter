using System;
using UnityEngine;

namespace Modules.Root.ContainerComponentModel
{
    public class OnDestroyDisposeHandler : MonoBehaviour
    {
        private IContainer _container;

        public void SetContainer(IContainer container)
        {
            _container = container;
        }

        private void OnDestroy()
        {
            if(_container != null)
                _container.Dispose();
        }
    }
}