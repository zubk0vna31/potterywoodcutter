namespace Modules.Root.ContainerComponentModel
{
    public interface IInitializable
    {
        void Init();
    }
}