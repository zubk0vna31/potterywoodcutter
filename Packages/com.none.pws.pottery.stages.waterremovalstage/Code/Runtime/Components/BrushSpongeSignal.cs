namespace PWS.Pottery.Stages.WaterRemovalStage
{
    public struct BrushSpongeSignal
    {
        public bool Wet;
    }
}
