using DG.Tweening;
using Leopotam.Ecs;
using Modules.StateGroup.Components;
using Modules.ViewHub;
using PWS.Pottery.ItemDataHolderService;
using PWS.Pottery.MeshGeneration;
using UnityEngine;

namespace PWS.Pottery.Stages.WaterRemovalStage
{
    public class WaterRemovalInteractorHandler : IEcsRunSystem
    {
        // auto injected fields
        readonly EcsFilter<StateEnter, WaterRemovalState> _entering;
        readonly EcsFilter<WaterRemovalState> _inState;
        
        readonly EcsFilter<WaterMesh> _water;
        readonly EcsFilter<BrushSpongeSignal, WaterRemovalSponge> _signal;

        private ISculptingDataHolder _data;
        private WaterRemovalStateConfig _config;

        //runtimeData
        private bool _spongeWet;

        public WaterRemovalInteractorHandler(ISculptingDataHolder data, WaterRemovalStateConfig config)
        {
            _data = data;
            _config = config;
        }

        public void Run()
        {
            if (!_entering.IsEmpty())
            {
                _spongeWet = false;
            }

            if (_inState.IsEmpty())
                return;

            foreach (var signal in _signal)
            {
                if(_spongeWet && !_signal.Get1(signal).Wet)
                {
                    _spongeWet = false;
                } else if (!_spongeWet && _signal.Get1(signal).Wet)
                {
                    foreach (var water in _water)
                    {
                        float newLevel = Mathf.Clamp01(_water.Get1(water).View.WaterLevel - _config.WaterReduceStep);
                        DOTween.To(() => _water.Get1(water).View.WaterLevel, x => _water.Get1(water).View.WaterLevel = x, newLevel, 0.1f)
                            .OnUpdate(() =>
                            {
                                _water.Get1(water).View.UpdateMesh(_data);
                            });
                    }
                    _spongeWet = true;
                }
            }
        }
    }
}