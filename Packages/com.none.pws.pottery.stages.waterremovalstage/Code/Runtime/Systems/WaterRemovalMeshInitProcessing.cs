using Leopotam.Ecs;
using Modules.StateGroup.Components;
using Modules.ViewHub;
using PWS.Pottery.ItemDataHolderService;
using PWS.Pottery.MeshGeneration;

namespace PWS.Pottery.Stages.WaterRemovalStage
{
    public class WaterRemovalMeshInitProcessing : IEcsRunSystem
    {
        // auto injected fields
        readonly EcsFilter<StateEnter, WaterRemovalState> _entering;
        
        readonly EcsFilter<WaterMesh> _water;
        readonly EcsFilter<UnityView, SculptingProductTag> _product;

        private ISculptingDataHolder _data;
        private WaterRemovalStateConfig _config;

        public WaterRemovalMeshInitProcessing(ISculptingDataHolder data, WaterRemovalStateConfig config)
        {
            _data = data;
            _config = config;
        }

        public void Run()
        {
            if (_entering.IsEmpty())
                return;

            foreach (var water in _water)
            {
                _water.Get1(water).View.WaterLevel = _config.InitWaterLevel;
                _water.Get1(water).View.UpdateMesh(_data);
            }
        }

    }
}