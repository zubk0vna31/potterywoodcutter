using Leopotam.Ecs;
using Modules.StateGroup.Core;
using PWS.Common.Messages;
using PWS.Common.UI;

namespace PWS.Pottery.Stages.WaterRemovalStage
{
    public class WaterRemovalSkipProcessing : IEcsRunSystem
    {
        // auto injected fields
        protected readonly EcsFilter<WaterRemovalState> _inState;
        private readonly EcsFilter<SkipStageMessage> _skipPerformed;
        protected readonly StateFactory _stateFactory;

        protected readonly SetStateSOCommand _nextState;

        public WaterRemovalSkipProcessing(SetStateSOCommand nextState)
        {
            _nextState = nextState;
        }

        public virtual void Run()
        {
            if (_inState.IsEmpty()) return;
            if (_skipPerformed.IsEmpty()) return;

            foreach (var i in _skipPerformed)
            {
                _skipPerformed.GetEntity(i).Del<NextStateSignal>();
            }

            _nextState.Execute(_stateFactory);
        }
    }
}