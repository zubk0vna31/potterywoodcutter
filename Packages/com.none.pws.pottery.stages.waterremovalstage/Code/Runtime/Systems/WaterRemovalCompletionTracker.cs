using DG.Tweening;
using Leopotam.Ecs;
using Modules.StateGroup.Components;
using Modules.ViewHub;
using PWS.Common.Messages;
using PWS.Common.UI;
using PWS.Pottery.ItemDataHolderService;
using PWS.Pottery.MeshGeneration;
using UnityEngine;

namespace PWS.Pottery.Stages.WaterRemovalStage
{
    public class WaterRemovalCompletionTracker : IEcsRunSystem
    {
        // auto injected fields
        readonly EcsFilter<StateEnter, WaterRemovalState> _entering;
        readonly EcsFilter<WaterRemovalState>.Exclude<StateExit> _inState;
        private readonly EcsFilter<SkipStageMessage> _skipPerformed;

        readonly EcsFilter<StateWindow, StateProgress> _stateWindowWithProgress;

        readonly EcsFilter<WaterMesh> _water;
        readonly EcsFilter<UnityView, SculptingProductTag> _product;
        readonly EcsFilter<BrushSpongeSignal, WaterRemovalSponge> _signal;

        private ISculptingDataHolder _data;
        private WaterRemovalStateConfig _config;

        //runtimeData
        private bool _spongeWet;

        public WaterRemovalCompletionTracker(ISculptingDataHolder data, WaterRemovalStateConfig config)
        {
            _data = data;
            _config = config;
        }

        public void Run()
        {
            if (!_entering.IsEmpty())
            {
                foreach (var i in _stateWindowWithProgress)
                {
                    _stateWindowWithProgress.Get2(i).Value = 0;
                    _stateWindowWithProgress.Get2(i).CompletionThreshold = _config.CompletionThreshold;
                    _stateWindowWithProgress.Get2(i).SkipThreshold = _config.SkipThreshold;
                }
            }

            if (_inState.IsEmpty())
                return;

            if (!_skipPerformed.IsEmpty())
                return;

            foreach (var water in _water)
            {
                float completion = 1 - _water.Get1(water).View.WaterLevel / _config.InitWaterLevel;
                foreach (var i in _stateWindowWithProgress)
                {
                    _stateWindowWithProgress.Get2(i).Value = completion;
                }
            }
        }
    }
}