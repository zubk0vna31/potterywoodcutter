using Leopotam.Ecs;
using Modules.Root.ContainerComponentModel;
using Modules.Root.ECS;
using Modules.StateGroup.Core;
using PWS.Common.Audio;
using PWS.Pottery.Common;
using PWS.Pottery.ItemDataHolderService;
using PWS.Pottery.MeshGeneration;
using UnityEngine;
using PWS.Common.Messages;
using PWS.Common.RestartStage;
using PWS.Common.UI;
using PWS.Common.ResultsEvaluation;
using PWS.Common.TravelPoints;

namespace PWS.Pottery.Stages.WaterRemovalStage
{
    public class PotteryWaterRemovalSystemsProvider : MonoBehaviour, ISystemsProvider
    {
        internal class SystemsDependencies
        {
            [Inject] public ISculptingDataHolder SculptingData;
            [Inject] public IResultsEvaluationDataHolder ResultsEvaluationData;
        }

        [SerializeField] private SetStateSOCommand _nextState;
        [SerializeField] private WaterRemovalStateConfig _config;
        [SerializeField] private StateInfoData _stateInfo;
        
        [Header("Voice config")] 
        [SerializeField] private VoiceConfig _voiceConfig;
        public EcsSystems GetSystems(EcsWorld world, EcsSystems endFrame, EcsSystems mainSystems)
        {
            SystemsDependencies dependencies = new SystemsDependencies();
            SceneContainer.Instance.Inject(dependencies);

            EcsSystems systems = new EcsSystems(world);
            systems
                .Add(new TransitToStateOnUIActionSystem<WaterRemovalState>(_nextState))
                .Add(new WaterRemovalSkipProcessing(_nextState))

                //restart
                .Add(new CurrentStateRestoreDataProcessing<WaterRemovalState>())

                //show sculpting inside
                .Add(new ProductMeshAngleRangeProcessing<WaterRemovalState, SculptingProductMesherTag>(_config.OpenMeshAngleRange, _config.Duration, dependencies.SculptingData))

                //state systems
                .Add(new GameObjectVisibilityProcessing<WaterRemovalState, WaterRemovalContainer>())
                .Add(new WaterRemovalMeshInitProcessing(dependencies.SculptingData, _config))
                .Add(new WaterRemovalInteractorHandler(dependencies.SculptingData, _config))

                .Add(new WaterRemovalCompletionTracker(dependencies.SculptingData, _config))

                //Outline
                .Add(new OutlineByStateSystem<WaterRemovalState>(2))

                .Add(new SetGradeByProgress<WaterRemovalState>(dependencies.ResultsEvaluationData, _stateInfo))

                //ui
                .Add(new StateInfoWindowUIProcessing<WaterRemovalState>(_stateInfo))

                .Add(new StateWindowPlacementProcessing<WaterRemovalState>("Wheel"))
                .Add(new StateWindowProgressUIProcessing<WaterRemovalState>())
                .Add(new StateWindowButtonUIProcessing<WaterRemovalState>())
                .Add(new SkipStageButtonUIProcessing<WaterRemovalState>())

                .Add(new MeshVisibilityProcessing<WaterRemovalState, SculptingProductTag>())
                
                //voice
                .Add(new VoiceAudioSystem<WaterRemovalState>(_voiceConfig))

                //travel point
                .Add(new TravelPointPositioningSystem<WaterRemovalState>("Wheel"))

                ;

            endFrame
                //sculpting product
                .Add(new SculptingDataProcessingSystem<WaterRemovalState>(dependencies.SculptingData))
                .Add(new MeshRingsDataProcessingSystem<WaterRemovalState>(dependencies.SculptingData))
                .Add(new MeshUpdateSystem<WaterRemovalState, SculptingProductMesherTag>(dependencies.SculptingData))

                .OneFrame<RestartCurrentStepMessage>()
                .OneFrame<NextStateSignal>()
                .OneFrame<SkipStageMessage>()
                .OneFrame<BrushSpongeSignal>()
                ;

            return systems;
        }
    }
}
