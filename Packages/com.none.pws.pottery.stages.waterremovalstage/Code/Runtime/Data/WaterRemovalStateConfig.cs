using UnityEngine;

namespace PWS.Pottery.Stages.WaterRemovalStage
{
    [CreateAssetMenu(fileName = "WaterRemovalStageConfig", menuName = "PWS/Pottery/Stages/WaterRemovalStage/Config", order = 0)]
    public class WaterRemovalStateConfig : ScriptableObject
    {
        public float CompletionThreshold = 0.9f;
        public float SkipThreshold = 0.75f;

        [Range(0, 1)]
        public float InitWaterLevel = 0.1f;
        public float WaterReduceStep = 0.02f;

        [Header("Pottery Product Mesh Angle Range")]
        public float OpenMeshAngleRange = 270;
        public float Duration = 1;
    }
}
