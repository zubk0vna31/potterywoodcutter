using Modules.StateGroup.Core;
using UnityEngine;

namespace PWS.Pottery.Stages.WaterRemovalStage
{
    [CreateAssetMenu(fileName = "SetWaterRemovalStateSOCommand", menuName = "PWS/Pottery/Stages/WaterRemovalStage/SetStateSOCommand")]
    public class SetWaterRemovalStateSOCommand : SetStateSOCommand<WaterRemovalState>
    {

    }
}