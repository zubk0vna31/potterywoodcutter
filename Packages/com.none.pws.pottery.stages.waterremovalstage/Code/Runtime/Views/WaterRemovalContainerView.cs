using Leopotam.Ecs;
using Modules.ViewHub;

namespace PWS.Pottery.Stages.WaterRemovalStage
{
    public class WaterRemovalContainerView : ViewComponent
    {
        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            ecsEntity.Get<WaterRemovalContainer>();
        }
    }
}
