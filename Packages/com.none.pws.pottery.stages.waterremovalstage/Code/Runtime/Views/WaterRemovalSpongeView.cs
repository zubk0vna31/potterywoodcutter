using Leopotam.Ecs;
using Modules.ViewHub;
using UnityEngine;

namespace PWS.Pottery.Stages.WaterRemovalStage
{
    public class WaterRemovalSpongeView : ViewComponent
    {
        private EcsEntity _ecsEntity;

        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            _ecsEntity = ecsEntity;
            ecsEntity.Get<WaterRemovalSponge>();
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.tag.Equals("SculptingProductWater"))
            {
                _ecsEntity.Get<BrushSpongeSignal>().Wet = true;
            }
            else if (other.tag.Equals("Water"))
            {
                _ecsEntity.Get<BrushSpongeSignal>().Wet = false;
            }
        }
    }
}
