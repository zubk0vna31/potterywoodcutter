using Leopotam.Ecs;
using Modules.ViewHub;
namespace PWS.Pottery.Stages.WaterRemovalStage
{
    public class WaterMeshView : ViewComponent
    {
        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            ecsEntity.Get<WaterMesh>().View = GetComponent<SculptingProductWaterMesher>();
        }
    }
}
