﻿using PWS.Pottery.ItemDataHolderService;
using PWS.Pottery.MeshGeneration;
using System.Collections.Generic;
using UnityEngine;

namespace PWS.Pottery.Stages.WaterRemovalStage
{
    public class SculptingProductWaterMesher : SculptingVisualizer
    {
        [Header("Water Settings")]
        [Range(0, 1)]
        public float WaterLevel = 0.2f;

        private List<Vector3> _vertices = new List<Vector3>();
        private List<int> _triangles = new List<int>();

        private MeshFilter _meshFilter;
        private MeshRenderer _meshRenderer;
        private BoxCollider _boxCollider;
        private Mesh _mesh;

        public override void Init()
        {
            base.Init();

            _meshRenderer = GetComponent<MeshRenderer>();
            _boxCollider = GetComponent<BoxCollider>();
            _meshFilter = GetComponent<MeshFilter>();
            _mesh = new Mesh();
            _mesh.MarkDynamic();
            _meshFilter.mesh = _mesh;
        }

        public override void Hide()
        {
            if (gameObject.activeInHierarchy && _meshRenderer && _meshRenderer.enabled)
                _meshRenderer.enabled = false;
        }

        public override void Show()
        {
            if (gameObject.activeInHierarchy && _meshRenderer && !_meshRenderer.enabled)
                _meshRenderer.enabled = true;
        }

        public override void VisualizeSculpting()
        {
            int bottomRing = Data.MeshRings.Count - 1;
            if (Data.GeneralParameters.FullHeight <= Data.MeshRings[bottomRing].Position.y)
                return;

            _vertices.Clear();
            _triangles.Clear();

            float balanceAngle = (360 - MeshVisualizeAngleRange) / 2;
            Quaternion rot0 = Quaternion.Euler(0, -balanceAngle - FacingRotation, 0);
            Quaternion rot1 = Quaternion.Euler(0, balanceAngle - FacingRotation, 0);

            float realWaterLevel = Mathf.Lerp(Data.MeshRings[bottomRing].Position.y, Data.GeneralParameters.FullHeight, WaterLevel);
            //Debug.Log(realWaterLevel);

            //walls vertices
            for (int i = 0; i < Data.MeshRings.Count; i++)
            {
                int id = (Data.MeshRings.Count - 1) - i;

                if(Data.MeshRings[id].Position.y < realWaterLevel)
                {
                    AddVertices(Data.MeshRings[id].Position, rot0, rot1);
                }
                else
                {
                    break;
                }
            }

            //top wall vertices
            for (int i = 0; i < Data.MeshRings.Count - 1; i++)
            {
                int id = (Data.MeshRings.Count - 1) - i;

                if (Data.MeshRings[id].Position.y < realWaterLevel)
                {
                    if (Data.MeshRings[id - 1].Position.y >= realWaterLevel)
                    {
                        AddLerpedVertices(realWaterLevel, Data.MeshRings[id], Data.MeshRings[id - 1], rot0, rot1);
                        break;
                    }
                }
            }

            //walls triangles
            for (int i = 0; i < _vertices.Count - 2; i += 2)
            {
                _triangles.Add(i);
                _triangles.Add(i + 1);
                _triangles.Add(i + 2);

                _triangles.Add(i + 1);
                _triangles.Add(i + 3);
                _triangles.Add(i + 2);
            }

            //top circle
            int realVertices = Data.SmoothParamaters.Vertices + 1;
            if (_vertices.Count >= 4)
            {
                Vector2 pos = Vector2.zero;
                Vector3 vertPos = _vertices[_vertices.Count - 1];
                pos.y = vertPos.y;
                vertPos.y = 0;
                pos.x = vertPos.magnitude;

                int originVert = _vertices.Count;
                int centralVert = _vertices.Count + realVertices;

                for (int j = 0; j < realVertices; j++)
                {
                    _vertices.Add(GetPosition(pos, j));

                    int next = (j + 1) % realVertices;

                    _triangles.Add(originVert + j);
                    _triangles.Add(originVert + next);
                    _triangles.Add(centralVert);
                }

                _vertices.Add(Vector3.up * pos.y);
            }

            _mesh.Clear();
            _mesh.vertices = _vertices.ToArray();
            _mesh.triangles = _triangles.ToArray();
            _mesh.RecalculateNormals();
            _meshFilter.mesh = _mesh;
            _boxCollider.center = _mesh.bounds.center;
            _boxCollider.size = _mesh.bounds.size;
        }

        Vector3 GetPosition(Vector2 pos, int i)
        {
            float angleStep = MeshVisualizeAngleRange / Data.SmoothParamaters.Vertices;
            float balanceAngle = (360 - MeshVisualizeAngleRange) / 2;

            Quaternion rotation = Quaternion.Euler(0, angleStep * i + balanceAngle - FacingRotation, 0);
            return rotation * pos;
        }

        void AddLerpedVertices(float height, MeshRing ringLow, MeshRing ringUp, Quaternion rot0, Quaternion rot1)
        {
            float value = (height - ringLow.Position.y) / (ringUp.Position.y - ringLow.Position.y);
            Vector2 pos = Vector2.Lerp(ringLow.Position, ringUp.Position, value);
            AddVertices(pos, rot0, rot1);
        }

        void AddVertices(Vector2 pos, Quaternion rot0, Quaternion rot1)
        {
            _vertices.Add(rot0 * pos);
            _vertices.Add(rot1 * pos);
        }
    }
}