using UnityEngine;
using Leopotam.Ecs;
using Modules.Root.ECS;
using Modules.StateGroup.Components;
using Modules.StateGroup.Core;
using Modules.StateGroup.Systems;


namespace Modules.StateGroup
{
    [CreateAssetMenu(menuName = "Modules/StateGroup/Provider")]
    public class StateGroupSystemsProvider : ScriptableObject, ISystemsProvider
    {   
        public EcsSystems GetSystems(EcsWorld world, EcsSystems endFrame, EcsSystems mainSystems)
        {
            mainSystems.Inject(new StateFactory(world));
            
            EcsSystems systems = new EcsSystems(world);

            systems

                .OneFrame<StateEnter>()
                .Add(new StateCleanupSystem())
                .Add(new StateSwitchSystem())

                ;

            return systems;
        }
    }
}