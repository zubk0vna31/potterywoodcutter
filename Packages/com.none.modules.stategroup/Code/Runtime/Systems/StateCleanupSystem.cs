using Leopotam.Ecs;
using Modules.StateGroup.Components;

namespace Modules.StateGroup.Systems
{
    public class StateCleanupSystem : IEcsRunSystem
    {
        private readonly EcsFilter<SimulationState, StateExit> _filter;

        public void Run()
        {
            foreach (var i in _filter)
            {
                _filter.GetEntity(i).Destroy();
            }
        }
    }
}