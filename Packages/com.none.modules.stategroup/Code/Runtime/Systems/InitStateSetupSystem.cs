using Leopotam.Ecs;
using Modules.StateGroup.Core;

namespace Modules.StateGroup.Systems
{
    public class InitStateSetupSystem : IEcsInitSystem
    {
        // auto injected fields
        private readonly StateFactory _stateFactory;
            
        private readonly SetStateSOCommand _setStateSoCommand;

        public InitStateSetupSystem(SetStateSOCommand setStateSoCommand)
        {
            _setStateSoCommand = setStateSoCommand;
        }

        public void Init()
        {
            _setStateSoCommand.Execute(_stateFactory);
        }
    }
}