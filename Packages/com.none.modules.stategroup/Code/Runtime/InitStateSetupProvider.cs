using Leopotam.Ecs;
using Modules.Root.ECS;
using Modules.StateGroup.Core;
using Modules.StateGroup.Systems;
using UnityEngine;

namespace Modules.StateGroup
{
    public class InitStateSetupProvider : MonoBehaviour, ISystemsProvider
    {
        [SerializeField] private SetStateSOCommand _initState;

        public EcsSystems GetSystems(EcsWorld world, EcsSystems endFrame, EcsSystems mainSystems)
        {
            return new EcsSystems(world, "InitStateSetup").Add(new InitStateSetupSystem(_initState));
        }
    }
    
}