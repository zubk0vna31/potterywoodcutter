namespace Modules.StateGroup.Core
{
    public interface ISetStateListener
    {
        void OnStateEnter<T>(ref T state);
        void OnStateExit<T>(ref T state);
    }
}