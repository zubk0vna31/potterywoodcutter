using UnityEngine;

namespace Modules.StateGroup.Core
{
    public abstract class SetStateSOCommand : ScriptableObject
    {
        public abstract void Execute(StateFactory stateFactory);
    }
    
    // used as an option to configure state switch process with serializable new state
    public abstract class SetStateSOCommand<T> : SetStateSOCommand where T : struct
    {
        public override void Execute(StateFactory stateFactory)
        {
            stateFactory.SetState<T>();
        }
    }
}