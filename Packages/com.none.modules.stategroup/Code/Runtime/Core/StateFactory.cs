using System.Collections.Generic;
using Leopotam.Ecs;
using Modules.StateGroup.Components;
using UnityEngine;

namespace Modules.StateGroup.Core
{
    public class StateFactory
    {
        public EcsEntity StateEntity;
        private EcsWorld _world;
        private List<ISetStateListener> _setStateListeners;
        private ISetStateListenerExitHandler _stateExitHandler;

        private ISetNextStateCommand _setNextStateCommand;
        private bool _hasCommand;

        public StateFactory(EcsWorld world)
        {
            _world = world;
            StateEntity = EcsEntity.Null;
            _setStateListeners = new List<ISetStateListener>();
        }

        public void SetState<T>() where T : struct
        {
            SetState<T>(new T());
        }

        public void SetState<T>(ref T state) where T : struct
        {
            _setNextStateCommand = new SetNextStateCommand<T>(state);
            _hasCommand = true;
        }

        public void ApplyState<T>(ref T state) where T : struct
        {
            if (StateEntity != EcsEntity.Null)
            {
                StateEntity.Get<StateExit>();
                _stateExitHandler?.OnExit(_setStateListeners);
            }

            StateEntity = _world.NewEntity();
            StateEntity.Get<SimulationState>();
            StateEntity.Get<StateEnter>();
            StateEntity.Get<T>() = state;

            foreach (var setStateListener in _setStateListeners)
            {
                setStateListener.OnStateEnter<T>(ref state);
            }

            _stateExitHandler = new SetStateListenerExitHandler<T>(StateEntity.Ref<T>());
        }

        public void TryApplyState()
        {
            if (_hasCommand)
            {
                _setNextStateCommand.Execute(this);
                _hasCommand = false;
            }
        }

        public void SetState<T>(T state) where T : struct
        {
            SetState<T>(ref state);
        }

        public void AddSetStateListener(ISetStateListener setStateListener)
        {
            _setStateListeners.Add(setStateListener);
        }
    }

    internal interface ISetStateListenerExitHandler
    {
        public void OnExit(IEnumerable<ISetStateListener> _listeners);
    }

    internal class SetStateListenerExitHandler<T> : ISetStateListenerExitHandler where T : struct 
    {
        private EcsComponentRef<T> _state;

        public SetStateListenerExitHandler(EcsComponentRef<T> state)
        {
            _state = state;
        }

        public void OnExit(IEnumerable<ISetStateListener> _listeners)
        {
            foreach (var setStateListener in _listeners)
            {
                setStateListener.OnStateExit<T>(ref _state.Unref());
            }
        }
    }

    internal interface ISetNextStateCommand
    {
        public void Execute(StateFactory stateFactory);
    }

    internal class SetNextStateCommand<T> : ISetNextStateCommand where T : struct
    {
        private T _state;

        public SetNextStateCommand(T state)
        {
            _state = state;
        }

        public void Execute(StateFactory stateFactory)
        {
            stateFactory.ApplyState(ref _state);
        }
    }

    public class StateSwitchSystem : IEcsRunSystem
    {
        private readonly StateFactory _stateFactory;

        public void Run()
        {
            _stateFactory.TryApplyState();
        }
    }
}