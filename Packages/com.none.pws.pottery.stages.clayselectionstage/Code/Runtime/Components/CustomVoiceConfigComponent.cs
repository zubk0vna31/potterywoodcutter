﻿using Modules.Audio;
using UnityEngine;

namespace PWS.Pottery.Stages.ClaySelectionStage
{
    public struct CustomVoiceConfigComponent
    {
        public string[] EnterClipsTerms;
        public string[] WrongChoiceClipsTerms;
        public string[] RightChoiceClipsTerms;
        public string[] ExitClipsTerms;

        public int EnterClipsIndex;
        public int WrongChoiceClipsIndex;
        public int RightChoiceClipsIndex;
        public int ExitClipsIndex;
        
        public OneShotSoundTemplate GrandDadPrefab;
    }
}