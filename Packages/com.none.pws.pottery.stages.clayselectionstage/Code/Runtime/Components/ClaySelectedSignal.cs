﻿using UnityEngine;

namespace PWS.Pottery.Stages.ClaySelectionStage
{
    public struct ClaySelectedSignal
    {
        public Material Material;
    }
}