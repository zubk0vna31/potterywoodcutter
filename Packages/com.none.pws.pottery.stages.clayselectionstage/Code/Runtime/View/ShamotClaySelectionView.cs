using Leopotam.Ecs;
using Modules.ViewHub;
using PWS.Pottery.Stages.ClaySelectionStage;
using UnityEngine.XR.Interaction.Toolkit;

public class ShamotClaySelectionView : ViewComponent
{
    public XRBaseInteractable interactable;

    private EcsEntity ecsEntity;

    public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
    {
        this.ecsEntity = ecsEntity;
        interactable.selectEntered.AddListener(Selected);
    }

    private void Selected(SelectEnterEventArgs arg0)
    {
        ecsEntity.Get<ShamotClaySelectedSignal>();
    }
}
