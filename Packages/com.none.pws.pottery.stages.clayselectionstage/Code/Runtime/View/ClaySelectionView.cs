using Leopotam.Ecs;
using Modules.ViewHub;
using PWS.Common.UI;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace PWS.Pottery.Stages.ClaySelectionStage
{
    public class ClaySelectionView : ViewComponent
    {
        [SerializeField] private GameObject _itemContainer;
        [SerializeField] private SculptingItem[] _references;

        private SculptingItem _selectedReference;
        private EcsEntity _ecsEntity;

        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            _ecsEntity = ecsEntity;
            ecsEntity.Get<ClaySelection>().View = this;
            foreach (var r in _references)
            {
                SculptingItem reference = r;
                r.Item.selectEntered.AddListener((SelectEnterEventArgs args) =>
                {
                    if (_selectedReference == null)
                    {
                        _selectedReference = reference;
                        SelectionCompleted();

                    }
                });
            }

            Hide();
        }

        void SelectionCompleted()
        {
            if (_selectedReference == null) return;

            _ecsEntity.Get<ClaySelectedSignal>().Material = _selectedReference.Material;
            _ecsEntity.Get<NextStateSignal>();

            _selectedReference = null;
        }

        public void Show()
        {
            if (!_itemContainer.activeSelf)
                _itemContainer.SetActive(true);
        }
        public void Hide()
        {
            if (_itemContainer.activeSelf)
                _itemContainer.SetActive(false);
        }

        [System.Serializable]
        public class SculptingItem
        {
            public Material Material;
            public XRBaseInteractable Item;
        }
    }
}
