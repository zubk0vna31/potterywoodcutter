﻿using Leopotam.Ecs;
using Modules.Root.ContainerComponentModel;
using Modules.Root.ECS;
using Modules.StateGroup.Core;
using PWS.Common.Audio;
using PWS.Common.ResultsEvaluation;
using PWS.Common.TravelPoints;
using PWS.Common.UI;
using PWS.Features.Achievements;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace PWS.Pottery.Stages.ClaySelectionStage
{
    public class PotteryClaySelectionSystemsProvider : MonoBehaviour, ISystemsProvider
    {
        internal class SystemsDependencies
        {
            [Inject] public IResultsEvaluationDataHolder ResultsEvaluationData;
            [Inject] public IAchievementsService AchievementsService;
        }

        [SerializeField] private SetStateSOCommand _nextState;
        [SerializeField] private StateInfoData _stateInfo;
        
        [Header("Voice config")] 
        [SerializeField] private VoiceConfig _voiceConfig;
        [SerializeField] private XRBaseInteractable _wrongClay;

        public EcsSystems GetSystems(EcsWorld world, EcsSystems endFrame, EcsSystems mainSystems)
        {
            SystemsDependencies dependencies = new SystemsDependencies();
            SceneContainer.Instance.Inject(dependencies);

            EcsSystems systems = new EcsSystems(world);
            systems
                .Add(new TransitToStateOnUIActionSystem<ClaySelectionState>(_nextState))
                //.Add(new StateCurrentRestartProcessing<TargetItemSelectionState, SculptingDataHolder>(dependencies.SculptingReferenceData.Data))

                .Add(new ClaySelectionProcessing())
                .Add(new SetCustomGrade<ClaySelectionState>(1, dependencies.ResultsEvaluationData, _stateInfo))

                //ui
                .Add(new StateInfoWindowUIProcessing<ClaySelectionState>(_stateInfo))

                .Add(new StateWindowPlacementProcessing<ClaySelectionState>("Table"))

                //voice
                .Add(new VoiceCustomAudioSystem(_voiceConfig, _wrongClay))

                //travel point
                .Add(new TravelPointPositioningSystem<ClaySelectionState>("Start"))

                // Achievement
                .Add(new ShamotClayProcessingSystem<ClaySelectionState,ShamotClaySelectedSignal>(dependencies.AchievementsService))

                ;

            endFrame
                .OneFrame<NextStateSignal>()
                .OneFrame<ClaySelectedSignal>()
                .OneFrame<ShamotClaySelectedSignal>()
                ;

            return systems;
        }
    }
}