﻿using UnityEngine;
using Modules.StateGroup.Core;

namespace PWS.Pottery.Stages.ClaySelectionStage
{
    [CreateAssetMenu(menuName = "PWS/Pottery/Stages/ClaySelectionStage/SetStateSOCommand")]
    public class SetClaySelectionStateSOCommand : SetStateSOCommand<ClaySelectionState>
    {

    }
}