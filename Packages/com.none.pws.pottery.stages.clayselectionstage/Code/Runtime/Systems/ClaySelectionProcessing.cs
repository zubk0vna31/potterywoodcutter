﻿using Leopotam.Ecs;
using Modules.StateGroup.Components;
using PWS.Pottery.Common;
using UnityEngine;

namespace PWS.Pottery.Stages.ClaySelectionStage
{
    public class ClaySelectionProcessing : IEcsRunSystem
    {
        // auto injected fields
        readonly EcsFilter<StateEnter, ClaySelectionState> _entering;
        readonly EcsFilter<StateExit, ClaySelectionState> _exiting;
        readonly EcsFilter<ClaySelectionState> _inState;

        readonly EcsFilter<ClaySelection> _view;
        readonly EcsFilter<ClaySelectedSignal> _signal;

        readonly EcsFilter<ClayMeshRenderer> _product;

        public void Run()
        {
            if (!_entering.IsEmpty())
            {
                foreach (var window in _view)
                {
                    _view.Get1(window).View.Show();
                }
            }
            if (!_exiting.IsEmpty())
            {
                foreach (var window in _view)
                {
                    _view.Get1(window).View.Hide();
                }
            }

            if (_inState.IsEmpty()) return;

            foreach (var signal in _signal)
            {
                foreach (var idx in _product)
                {
                    _product.Get1(idx).MeshRenderer.material = _signal.Get1(signal).Material;
                }
            }
        }
    }
}