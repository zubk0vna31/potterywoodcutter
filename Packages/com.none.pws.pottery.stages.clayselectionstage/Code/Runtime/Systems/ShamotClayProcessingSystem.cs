using Leopotam.Ecs;
using PWS.Features.Achievements;

namespace PWS.Pottery.Stages.ClaySelectionStage
{
    public class ShamotClayProcessingSystem<T,S> : IEcsRunSystem where T : struct where S :struct
    {
        private readonly EcsFilter<T> _state;
        private readonly EcsFilter<S> _signal;

        private readonly IAchievementsService _achievementsService;

        public ShamotClayProcessingSystem(IAchievementsService achievementsService)
        {
            _achievementsService = achievementsService;
        }

        public void Run()
        {
            if (_state.IsEmpty()) return;

            if (_signal.IsEmpty()) return;

            foreach (var i in _signal)
            {
                _achievementsService.Unlock(AchievementsNames.it_happens);
            }
        }
    }
}
