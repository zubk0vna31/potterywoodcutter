﻿using Leopotam.Ecs;
using Modules.Audio;
using Modules.StateGroup.Components;
using PWS.Common.Audio;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace PWS.Pottery.Stages.ClaySelectionStage
{
    public class VoiceCustomAudioSystem : IEcsInitSystem, IEcsRunSystem
    {
        private readonly EcsFilter<StateEnter, ClaySelectionState> _enterState;
        private readonly EcsFilter<StateExit, ClaySelectionState> _exitState;
        private readonly EcsFilter<ClaySelectionState>.Exclude<StateEnter, StateExit> _inState;
        private readonly EcsFilter<AudioSourceRef, GrandDadTag, OneShotSource> _grandDad;
        private readonly EcsFilter<CustomVoiceConfigComponent> _customVoiceConfigComponent;
        private readonly EcsWorld _ecsWorld;

        private bool _enterStateFlag;
        private bool _wrongСhoiceFlag;
        private XRBaseInteractable _interactable;

        private VoiceConfig _voiceConfig;
        
        public VoiceCustomAudioSystem(VoiceConfig voiceConfig, XRBaseInteractable interactable)
        {
            _voiceConfig = voiceConfig;
            _interactable = interactable;
        }
        
        public void Init()
        {
            _interactable.selectEntered.AddListener(arg =>
                {
                    if (!_inState.IsEmpty())
                        _wrongСhoiceFlag = true;
                });
        }
        
        public void Run()
        {
            if (!_enterState.IsEmpty())
            {
                _enterStateFlag = true;
                SetVoiceConfig();
                return;
            }

            if (!_inState.IsEmpty())
            {
                foreach (var dad in _grandDad)
                {
                    if (_grandDad.Get1(dad).AudioSource.isPlaying)
                        return;

                    foreach (var i in _customVoiceConfigComponent)
                    {
                        ref var config = ref _customVoiceConfigComponent.Get1(i);
                        
                        if (_enterStateFlag)
                        {
                            PlayClip(config.EnterClipsTerms, ref config.EnterClipsIndex);
                            _enterStateFlag = false;
                            return;
                        }

                        if (_wrongСhoiceFlag && config.WrongChoiceClipsIndex < config.WrongChoiceClipsTerms.Length)
                        {
                            PlayClip(config.WrongChoiceClipsTerms, ref config.WrongChoiceClipsIndex);
                        }
                    }
                }
            }
            
            if (!_exitState.IsEmpty())
            {
                foreach (var i in _grandDad)
                {
                    _grandDad.Get1(i).AudioSource.Stop();
                }

                foreach (var i in _customVoiceConfigComponent)
                {
                    ref var config = ref _customVoiceConfigComponent.Get1(i);
                    
                    if (_wrongСhoiceFlag)
                    {
                        PlayClip(config.ExitClipsTerms, ref config.ExitClipsIndex);
                    }
                    else
                    {
                        PlayClip(config.RightChoiceClipsTerms, ref config.RightChoiceClipsIndex);
                    } 
                }
            }
        }

        private void PlayClip(string[] clips, ref int idx)
        {
            foreach (var dad in _grandDad)
            {
                ref var sourceRef = ref _grandDad.Get1(dad);
                if (sourceRef.AudioSource.isPlaying)
                    return;
                
                ref var signal = ref _grandDad.GetEntity(dad).Get<PlayLocalizedVoiceAtPointSignal>();
                signal.PlayPosition = sourceRef.Transform.position;
                signal.SoundPrefab = _voiceConfig.GrandDadPrefab;
                signal.Term = clips[idx];
                idx++;
            }
        }

        private void SetVoiceConfig()
        {
            foreach (var component in _customVoiceConfigComponent)
            {
                _customVoiceConfigComponent.GetEntity(component).Destroy();
            }

            ref var entity = ref _ecsWorld.NewEntity().Get<CustomVoiceConfigComponent>();

            entity.EnterClipsTerms = _voiceConfig.StateEnterClipsTerms;
            entity.RightChoiceClipsTerms = new [] {_voiceConfig.StateExitClipsTerms[0]};
            entity.WrongChoiceClipsTerms = new [] {_voiceConfig.StateExitClipsTerms[1]};
            entity.ExitClipsTerms = new [] {_voiceConfig.StateExitClipsTerms[2]};

            entity.EnterClipsIndex = 0;
            entity.WrongChoiceClipsIndex = 0;
            entity.RightChoiceClipsIndex = 0;
            entity.ExitClipsIndex = 0;

            entity.GrandDadPrefab = _voiceConfig.GrandDadPrefab;
        }
    }
}