using Modules.Root.ContainerComponentModel;
using UnityEngine;

namespace PWS.Common.UserInfoService
{

    [CreateAssetMenu(fileName = "UserInfoServiceInstaller", menuName = "PWS/Common/UserInfoService/Installer", order = 0)]
    public class UserInfoServiceInstaller : ASOInstaller
    {
        public override void Install(IContainer container)
        {
            container.Bind<IUserInfoService>( new UserInfoService());
        }
    }
}
