
namespace PWS.Common.UserInfoService
{
    public interface IUserInfoService
    {
        string UserName { get; set; }
        bool Registered { get; }
        void ResetRegistration();
    }
}
