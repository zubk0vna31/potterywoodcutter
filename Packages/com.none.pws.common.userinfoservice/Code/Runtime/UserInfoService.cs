
namespace PWS.Common.UserInfoService
{
    public class UserInfoService : IUserInfoService
    {
        private string _userName = "User";
        private bool _registered;

        public string UserName
        {
            get => _userName;
            set {
                _userName = value;
                _registered = true;
            }
        }

        public bool Registered => _registered;

        public void ResetRegistration() 
        {
            _registered = false;
        }
    }
}