using UnityEngine;

namespace PWS.Common.ResultsSubmission.EmailSubmitter
{
    [CreateAssetMenu(menuName = "PWS/Common/ResultsSubmission/EmailSubmitter/EmailConfigurationSO")]
    public class EMailConfigurationSO : ScriptableObject
    {
        [SerializeField] public EMailConfiguration EMailConfiguration;
    }
}