using System;

namespace PWS.Common.ResultsSubmission.EmailSubmitter
{
    public class FakeEMailSubmitter : IEmailSubmitter
    {
        public void SubmitResults(ref string[] imagesPaths, string username, DateTime currentDateTime, string currentLocationName)
        {
            UnityEngine.Debug.Log("Results sent over fake email submitter");
            return;
        }
    }
}