using System;

namespace PWS.Common.ResultsSubmission.EmailSubmitter
{
    public interface IEmailSubmitter
    {
        public void SubmitResults(ref string[] imagesPaths, string username, DateTime currentDateTime, string currentLocationName);
    }
}