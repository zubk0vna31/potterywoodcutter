namespace PWS.Common.ResultsSubmission.EmailSubmitter
{
    [System.Serializable]
    public struct EMailConfiguration
    {
        public string FromAddress;
        public string FromAddressPassword;
        public string ToAddress;
        public string SMTPServer;
        public int SMTPPort;
    }
}