using System.IO;
using UnityEngine;

namespace PWS.Common.ResultsSubmission.EmailSubmitter
{
    public class EMailConfigurationProvider : IEmailConfigurationProvider
    {
        public const string ONRESOURCES_CONFIGURATION_LOCATION = "ResultsSubmission/EMailSubmitter/DefaultConfiguration";
        public const string ONDEVICE_CONFIGURATION_FOLDER = "Configuration";
        public const string ONDEVICE_CONFIGURATION_FILENAME = "EmailConfiguration.json";

        public EMailConfigurationProvider()
        {
            TryCreateDefaultConfigurationOnDeviceInNotExists();
        }

        public void TryCreateDefaultConfigurationOnDeviceInNotExists()
        {
            EMailConfiguration result = new EMailConfiguration();
            if (!TryGetConfigurationFromDevice(ref result) && TryGetDefaultConfigurationFromResources(ref result))
            {
                SaveConfigurationToDevice(ref result);
            }
        }
        
        public EMailConfiguration GetConfiguration()
        {
            EMailConfiguration result = new EMailConfiguration();
            if (TryGetConfigurationFromDevice(ref result))
                return result;

            if (TryGetDefaultConfigurationFromResources(ref result))
            {
                SaveConfigurationToDevice(ref result);
                return result;
            }

            return result;
        }

        private bool TryGetConfigurationFromDevice(ref EMailConfiguration eMailConfiguration)
        {
            string filePath = Path.Combine(Application.persistentDataPath, ONDEVICE_CONFIGURATION_FOLDER,
                ONDEVICE_CONFIGURATION_FILENAME);
            
            if (!System.IO.File.Exists(filePath))
                return false;

            eMailConfiguration= JsonUtility.FromJson<EMailConfiguration>(File.ReadAllText(filePath));
            return true;
        }

        private bool TryGetDefaultConfigurationFromResources(ref EMailConfiguration eMailConfiguration)
        {
            EMailConfigurationSO eMailConfigurationSO =
                Resources.Load<EMailConfigurationSO>(ONRESOURCES_CONFIGURATION_LOCATION);

            if (eMailConfigurationSO == null)
                return false;

            eMailConfiguration = eMailConfigurationSO.EMailConfiguration;
            return true;
        }

        private void SaveConfigurationToDevice(ref EMailConfiguration configuration)
        {
            string savePath = Path.Combine(Application.persistentDataPath, ONDEVICE_CONFIGURATION_FOLDER);
            
            if (!Directory.Exists(savePath))
                Directory.CreateDirectory(savePath);

            savePath = Path.Combine(savePath, ONDEVICE_CONFIGURATION_FILENAME);
            
            File.WriteAllText(savePath, JsonUtility.ToJson(configuration, true));
        }
    }
}