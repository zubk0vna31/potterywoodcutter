namespace PWS.Common.ResultsSubmission.EmailSubmitter
{
    public interface IEmailConfigurationProvider
    {
        public EMailConfiguration GetConfiguration();
    }
}