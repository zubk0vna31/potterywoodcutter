using System;
using System.ComponentModel;
using System.Net.Mail;
using Modules.Root.ContainerComponentModel;
using PWS.Common.UserInfoService;
using UnityEngine;

namespace PWS.Common.ResultsSubmission.EmailSubmitter
{
    public class EmailSubmitter : IEmailSubmitter
    {
        private IEmailConfigurationProvider _configurationProvider;

        public EmailSubmitter()
        {
            _configurationProvider = new EMailConfigurationProvider();
        }
        
        public EmailSubmitter(EMailConfigurationProvider configurationProvider)
        {
            _configurationProvider = configurationProvider;
        }

        public void SubmitResults(ref string[] imagesPaths, string username, DateTime currentDateTime, string currentLocationName)
        {
            EMailConfiguration configuration = _configurationProvider.GetConfiguration();
            // Command-line argument must be the SMTP host.
            SmtpClient client = new SmtpClient(configuration.SMTPServer, configuration.SMTPPort);
            client.Credentials = new System.Net.NetworkCredential(
                configuration.FromAddress, 
                configuration.FromAddressPassword);
            client.EnableSsl = true;
            // Specify the email sender.
            // Create a mailing address that includes a UTF8 character
            // in the display name.
            MailAddress from = new MailAddress(
                configuration.FromAddress,
                configuration.FromAddress,
                System.Text.Encoding.UTF8);
            // Set destinations for the email message.
            MailAddress to = new MailAddress(configuration.ToAddress);
            // Specify the message content.
            MailMessage message = new MailMessage(from, to);
            message.Body = GenerateMessageBody(username, currentLocationName, DateTime.Now);;
            message.BodyEncoding = System.Text.Encoding.UTF8;
            message.Subject = message.Body;
            message.SubjectEncoding = System.Text.Encoding.UTF8;


            foreach (var imagePath in imagesPaths)
            {
                message.Attachments.Add(new Attachment(imagePath));
            }

            // Set the method that is called back when the send operation ends.
            client.SendCompleted += new SendCompletedEventHandler(SendCompletedCallback);
            // The userState can be any object that allows your callback
            // method to identify this send operation.
            // For this example, the userToken is a string constant.
            string userState = "email results submission";
            client.SendAsync(message, userState);
        }

        private string GenerateMessageBody(string userName, string locationName,DateTime time)
        {
            return $"Результаты пользователя {userName} в локации {locationName} в {time}";
        }
        
        private static void SendCompletedCallback(object sender, AsyncCompletedEventArgs e)
        {
            // Get the unique identifier for this asynchronous operation.
            string token = (string)e.UserState;

            if (e.Cancelled)
            {
                Debug.Log("Send canceled "+ token);
            }
            if (e.Error != null)
            {
                Debug.Log("[ "+token+" ] " + " " + e.Error.ToString());
            }
            else
            {
                Debug.Log("Message sent.");
            }
        }
    }
}