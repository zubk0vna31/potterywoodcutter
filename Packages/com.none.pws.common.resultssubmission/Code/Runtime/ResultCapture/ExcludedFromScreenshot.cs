
using UnityEngine;

namespace PWS.Common.ResultsSubmission.ResultCapture
{
    public struct ExcludedFromScreenshot
    {
        public GameObject RenderersParent;
    }
}