using System.Collections.Generic;
using System.IO;
using System.Linq;
using Leopotam.Ecs;
using UnityEngine;

namespace PWS.Common.ResultsSubmission.ResultCapture
{
    public class ResultsSubmissionSystem : IEcsInitSystem, IEcsRunSystem, IEcsDestroySystem
    {
        internal struct RenderTexturePair
        {
            public RenderTexture ActiveRenderTexture;
            public RenderTexture ScreenshotRenderTexture;
        }

        private const string RESULT_CAPTURE_CONFIG_RESOURCES_SUBPATH = "ResultCaptureConfig";
        private const string SCREENSHOT_FOLDER_RESOURCES_SUBPATH = "ResultCapture";
        
        // auto injected fields
        private readonly EcsFilter<SubmitResultsSignal> _signal;
        private readonly EcsFilter<ExcludedFromScreenshot> _excludeFromScreenshot;

        private readonly string _locationResourcesPath;
        private readonly IResultsSubmissionService _resultsSubmissionService;

        // runtime data
        private ResultCaptureConfig _resultCaptureConfig;

        public ResultsSubmissionSystem(IResultsSubmissionService resultsSubmissionService, string locationResourcesPath)
        {
            _locationResourcesPath = locationResourcesPath;
            _resultsSubmissionService = resultsSubmissionService;
        }

        public void Init()
        {
            _resultCaptureConfig = Resources.Load<ResultCaptureConfig>(Path.Combine(_locationResourcesPath, SCREENSHOT_FOLDER_RESOURCES_SUBPATH, RESULT_CAPTURE_CONFIG_RESOURCES_SUBPATH));

            _resultsSubmissionService.ClearResultImages();
            _resultsSubmissionService.SetCurrentLocationName(_resultCaptureConfig.LocationName);
        }

        public void Run()
        {
            if(_signal.IsEmpty())
                return;

            RenderTexturePair renderTextures = PrepareRenderTextures();

            List<Renderer> disbaledRenderers = DisableExcludedRenderers();

            Camera captureCamera = new GameObject().AddComponent<Camera>();

            foreach (var position in _resultCaptureConfig.ResultingTransforms)
            {
                UpdateCameraPosition(captureCamera, position);
                SubmitScreenshot(CaptureScreenshot(renderTextures.ScreenshotRenderTexture, captureCamera));
            }
            GameObject.Destroy(captureCamera.gameObject);

            EnableRenderers(disbaledRenderers);
            
            ReleaseScreenshotTexture(ref renderTextures);

            _resultsSubmissionService.SubmitResult();
        }

        private List<Renderer> DisableExcludedRenderers()
        {
            List<Renderer> renderers = new List<Renderer>();

            foreach (var excluded in _excludeFromScreenshot)
            {
                if(_excludeFromScreenshot.Get1(excluded).RenderersParent is null)
                    continue;

                renderers.AddRange(_excludeFromScreenshot.Get1(excluded).RenderersParent.GetComponentsInChildren<Renderer>().Where(r => r.enabled));
            }
            foreach (var renderer in renderers)
            {
                renderer.enabled = false;
            }

            return renderers;
        }

        private void EnableRenderers(List<Renderer> renderers)
        {
            foreach (var renderer in renderers)
            {
                renderer.enabled = true;
            }
        }

        private RenderTexturePair PrepareRenderTextures()
        {
            RenderTexturePair result = new RenderTexturePair();
            result.ScreenshotRenderTexture = new RenderTexture(1920, 1080, 32, RenderTextureFormat.ARGB32, RenderTextureReadWrite.sRGB)
            {
                name = "Results texture",
                enableRandomWrite = true
            };
            result.ScreenshotRenderTexture.Create();
            result.ActiveRenderTexture = RenderTexture.active;
            RenderTexture.active = result.ScreenshotRenderTexture;
            return result;
        }

        private void ReleaseScreenshotTexture(ref RenderTexturePair renderTexturePair)
        {
            RenderTexture.active = renderTexturePair.ActiveRenderTexture;
            renderTexturePair.ScreenshotRenderTexture.Release();
            renderTexturePair.ScreenshotRenderTexture = null;
        }

        public void SubmitScreenshot(Texture2D screenshot)
        {
            _resultsSubmissionService.AddResultImage(screenshot);
        }

        public void UpdateCameraPosition(Camera camera, ResultCameraEntry resultCameraEntry)
        {
            camera.transform.position = resultCameraEntry.Position;
            camera.transform.rotation = Quaternion.Euler(resultCameraEntry.Rotation);
        }

        public Texture2D CaptureScreenshot(RenderTexture rt, Camera camera)
        {
            camera.targetTexture = rt;
            GL.Clear(true, true, Color.black);
            camera.Render();
            
            Texture2D tex = new Texture2D(rt.width, rt.height, TextureFormat.ARGB32 , false);
            tex.ReadPixels(new Rect(0, 0, rt.width, rt.height), 0, 0);
            tex.Apply();
            return tex;
        }

        public void Destroy()
        {
            _resultCaptureConfig = null;
            Resources.UnloadUnusedAssets();
        }
    }
}