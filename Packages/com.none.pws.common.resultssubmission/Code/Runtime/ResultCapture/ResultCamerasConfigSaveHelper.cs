using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace PWS.Common.ResultsSubmission.ResultCapture
{
    public class ResultCamerasConfigSaveHelper : MonoBehaviour
    {
        [SerializeField] private ResultCaptureConfig targetConfig;
        
        [ContextMenu("Update Config")]
        public void UpdateConfig()
        {
            Camera[] cameras = this.gameObject.GetComponentsInChildren<Camera>();
            targetConfig.ResultingTransforms = new List<ResultCameraEntry>();
            foreach (var camera in cameras)
            {
                targetConfig.ResultingTransforms.Add(
                    new ResultCameraEntry()
                    {
                        Position = camera.transform.position, 
                        Rotation = camera.transform.rotation.eulerAngles
                        
                    });
            }

#if UNITY_EDITOR
            EditorUtility.SetDirty(targetConfig);
#endif
        }
    }
}