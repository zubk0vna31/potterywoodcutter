using System.Collections.Generic;
using UnityEngine;

namespace PWS.Common.ResultsSubmission.ResultCapture
{
    [System.Serializable]
    public struct ResultCameraEntry
    {
        public Vector3 Position;
        public Vector3 Rotation;
    }
    
    [CreateAssetMenu(menuName = "PWS/Common/ResultsSubmission/ScreenshotsCapture/ResultCaptureConfig", fileName = "ResultCaptureConfig")]
    public class ResultCaptureConfig : ScriptableObject
    {
        public List<ResultCameraEntry> ResultingTransforms;
        public string LocationName;
    }
}