using System;
using System.Collections.Generic;
using UnityEngine;

namespace PWS.Common.ResultsSubmission.OnDeviceResultsSubmitter
{
    public interface IOnDeviceResultsSubmitter
    {
        /// <summary>
        /// saves results on device
        /// </summary>
        /// <returns>paths of saved images</returns>
        public string[] SubmitResults(List<Texture2D> images, string username, DateTime dateTime, string currentLocationName);
    }
}