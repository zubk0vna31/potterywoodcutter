using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

namespace PWS.Common.ResultsSubmission.OnDeviceResultsSubmitter
{
    public class PNGOnDeviceResultSubmitter : IOnDeviceResultsSubmitter
    {
        private const string RESULTS_SAVE_DIRECTORY_NAME = "Results";

        private readonly int _maxScreenshotsInSaveDirectory;

        public PNGOnDeviceResultSubmitter(int maxScreenshotsInSaveDirectory)
        {
            _maxScreenshotsInSaveDirectory = maxScreenshotsInSaveDirectory;
        }

        public PNGOnDeviceResultSubmitter()
        {
            _maxScreenshotsInSaveDirectory = 100;
        }
        
        public string[] SubmitResults(List<Texture2D> images, string username, DateTime dateTime, string currentLocationName)
        {
            string[] resultPath = new string[images.Count];
            string saveDir = Path.Combine(Application.persistentDataPath, RESULTS_SAVE_DIRECTORY_NAME);
            if (!System.IO.Directory.Exists(Path.Combine(Application.persistentDataPath, RESULTS_SAVE_DIRECTORY_NAME)))
            {
                System.IO.Directory.CreateDirectory(saveDir);
            }

            List<string> storedFiles = System.IO.Directory.GetFiles(saveDir).ToList();
            if (storedFiles.Count + images.Count >= _maxScreenshotsInSaveDirectory)
            {
                storedFiles = storedFiles.Where(s => s.EndsWith(".png")).OrderBy(System.IO.File.GetCreationTime).ToList();
                for (int i = storedFiles.Count + images.Count-_maxScreenshotsInSaveDirectory; i >=0; i--)
                {
                    System.IO.File.Delete(storedFiles[i]);
                }
            }
            
            DateTime currentTime = DateTime.Now;
            for (int i = 0; i < images.Count; i++)
            {
                resultPath[i] =
                    Path.Combine(saveDir, GenerateFileName(username, currentTime, currentLocationName, i, ".png"));
                System.IO.File.WriteAllBytes(resultPath[i], images[i].EncodeToPNG());
            }
            
            return resultPath;
        }
        
        private static string GenerateFileName(string username, DateTime dateTime, string currentLocationName, int fileID, string fileExtension)
        {
            string result = String.IsNullOrEmpty(username) ? "User" : username;
            result = result.Replace(' ', '_');
            result = result.Replace('.','_');
            result += $"_{currentLocationName}_{dateTime.Year}_{dateTime.Month}_{dateTime.Day}_{dateTime.Hour:00}_{dateTime.Minute:00}_№{fileID:00}{fileExtension}";
            return result;
        }


    }
}