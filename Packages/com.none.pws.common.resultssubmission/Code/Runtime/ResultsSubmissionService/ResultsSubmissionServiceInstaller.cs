using Modules.Root.ContainerComponentModel;
using PWS.Common.ResultsSubmission.EmailSubmitter;
using PWS.Common.ResultsSubmission.OnDeviceResultsSubmitter;
using UnityEngine;

namespace PWS.Common.ResultsSubmission
{
    [CreateAssetMenu(menuName = "PWS/Common/ResultsSubmission/Installer")]
    public class ResultsSubmissionServiceInstaller : ASOInstaller
    {
        [SerializeField] private bool _submitToEmailInBuild = true;
        [SerializeField] private bool _submitToEmailInEditor = false;
        [SerializeField] private int _maxFilesInDirectory = 100;
        
        public override void Install(IContainer container)
        {
            container.Bind<IResultsSubmissionService>(new ResultsSubmissionService(CreateEmailSubmitter(), new PNGOnDeviceResultSubmitter(_maxFilesInDirectory)));
        }

        private IEmailSubmitter CreateEmailSubmitter()
        {
            #if UNITY_EDITOR
            if (_submitToEmailInEditor)
                return new EmailSubmitter.EmailSubmitter();
            #endif
            
            #if !UNITY_EDITOR
            if (_submitToEmailInBuild)
                return new EmailSubmitter.EmailSubmitter();
            #endif
            
            return new FakeEMailSubmitter();
        }
    }
}