using System;
using System.Collections.Generic;
using Modules.Root.ContainerComponentModel;
using PWS.Common.ResultsSubmission.EmailSubmitter;
using PWS.Common.ResultsSubmission.OnDeviceResultsSubmitter;
using PWS.Common.UserInfoService;
using UnityEngine;

namespace PWS.Common.ResultsSubmission
{
    public class ResultsSubmissionService : IResultsSubmissionService
    {
        [Inject] private IUserInfoService _userInfoService;

        private List<Texture2D> _resultImages;
        private string _currentLocationName;
        private IEmailSubmitter _emailSubmitter;
        private IOnDeviceResultsSubmitter _onDeviceResultsSubmitter;

        public ResultsSubmissionService(IEmailSubmitter emailSubmitter, IOnDeviceResultsSubmitter onDeviceResultsSubmitter)
        {
            _emailSubmitter = emailSubmitter;
            _onDeviceResultsSubmitter = onDeviceResultsSubmitter;
            _resultImages = new List<Texture2D>();
        }
        
        public ResultsSubmissionService()
        {
            _emailSubmitter = new EmailSubmitter.EmailSubmitter();
            _onDeviceResultsSubmitter = new PNGOnDeviceResultSubmitter();
            _resultImages = new List<Texture2D>();
        }
        
        public void SubmitResult()
        {
            // to safely avoid filesystem related exceptions like out of memory exception
            try
            {
                string[] savedImages = _onDeviceResultsSubmitter.SubmitResults(_resultImages, _userInfoService.UserName, DateTime.Now, _currentLocationName);
                _emailSubmitter.SubmitResults(ref savedImages, _userInfoService.UserName, DateTime.Now, _currentLocationName);
                ClearResultImages();
            }
            catch (Exception e)
            {
                ClearResultImages();
                UnityEngine.Debug.Log(e);
            }

        }

        public void AddResultImage(Texture2D resultImage)
        {
            _resultImages.Add(resultImage);
        }

        public void SetCurrentLocationName(string location)
        {
            _currentLocationName = location;
        }

        public void ClearResultImages()
        {
            for (int i = 0; i < _resultImages.Count; i++)
            {
                UnityEngine.Object.Destroy(_resultImages[i]);
            }
            _resultImages.Clear();
        }
    }
}