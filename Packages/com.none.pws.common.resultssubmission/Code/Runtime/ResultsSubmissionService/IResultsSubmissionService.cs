using UnityEngine;

namespace PWS.Common.ResultsSubmission
{
    public interface IResultsSubmissionService
    {
        void AddResultImage(Texture2D resultImage);
        
        void SetCurrentLocationName(string location);
        
        /// <summary>
        /// forced clear of saved result images in memory 
        /// </summary>
        void ClearResultImages();
        
        /// <summary>
        /// submits result
        /// Result images will be cleared from memory after submission
        /// </summary>
        void SubmitResult();
    }
}