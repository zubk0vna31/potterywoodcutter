using UnityEngine;

namespace PWS.WoodCutting
{
    public class TagAttribute : PropertyAttribute
    {
        public bool UseDefaultTagFieldDrawer = false;
    }
}
