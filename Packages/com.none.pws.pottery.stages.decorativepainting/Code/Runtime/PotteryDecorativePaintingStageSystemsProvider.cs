using Leopotam.Ecs;
using Modules.Root.ContainerComponentModel;
using Modules.Root.ECS;
using Modules.StateGroup.Core;
using Modules.VRFeatures;
using PWS.Common.Audio;
using PWS.Common.Messages;
using PWS.Common.RestartStage;
using PWS.Common.ResultsEvaluation;
using PWS.Common.TravelPoints;
using PWS.Common.UI;
using PWS.Features.Achievements;
using PWS.Pottery.Common;
using PWS.Pottery.ItemDataHolderService;
using PWS.Pottery.MeshGeneration;
using PWS.Pottery.Stages.DecorativeSculptingStage;
using UnityEngine;

namespace PWS.Pottery.Stages.DecorativePainting
{
    public class PotteryDecorativePaintingStageSystemsProvider : MonoBehaviour, ISystemsProvider
    {
        internal class SystemsDependencies
        {
            [Inject] public ISculptingDataHolder SculptingData;
            [Inject] public IResultsEvaluationDataHolder ResultsEvaluationData;
            [Inject] public IAchievementsService AchievementsService;
        }

        [SerializeField] private SetStateSOCommand _nextState;
        [SerializeField] private DecoratingStateConfig _config;
        [SerializeField] private StateInfoData _stateInfo;

        [Header("Voice config")] 
        [SerializeField] private VoiceConfig _itemPlacingState;
        [SerializeField] private VoiceConfig _decorSelectionState;
        [SerializeField] private VoiceConfig _decorativePaintingState;

        public EcsSystems GetSystems(EcsWorld world, EcsSystems endFrame, EcsSystems mainSystems)
        {
            SystemsDependencies dependencies = new SystemsDependencies();
            SceneContainer.Instance.Inject(dependencies);
            EcsSystems systems = new EcsSystems(world);
            systems
                .Add(new TransitToStateOnUIActionSystem<DecorativePaintingState>(_nextState))

                .Add(new CurrentSubStateRestoreDataProcessing<DecorativePaintingDecorSelectionState, DecorativePaintingState>())
                .Add(new CurrentSubStateRestoreDataProcessing<DecorativePaintingDecorSelectionState, DecorativePaintingColorSelectionState>())
                .Add(new DecorativeSculptingRestartProcessing<DecorativePaintingDecorSelectionState,
                DecorativePaintingDrawableComponent, DrawableQuad>(_config.TextureID))

                //item placing state systems
                .Add(new SnapSocketActivityProcessing<DecorativePaintingItemPlacingState, DecorativeSculptingSnapSocket>())
                .Add(new GameObjectVisibilityProcessing<DecorativePaintingItemPlacingState, DecorativeSculptingContainer>())

                .Add(new DecoratingItemPlacingProcessing<DecorativePaintingItemPlacingState, DecorativePaintingDecorSelectionState>())

                .Add(new MeshVisibilityProcessing<DecorativePaintingItemPlacingState, SculptingProductMesherTag>())
                .Add(new XRInteractableProcessing<DecorativePaintingItemPlacingState, SculptingProductTag>())

                .Add(new StateInfoWindowUIProcessing<DecorativePaintingItemPlacingState>(_stateInfo))

                //decor selection state systems
                .Add(new GameObjectVisibilityProcessing<DecorativePaintingDecorSelectionState, DecorativeSculptingContainer>())
                .Add(new GameObjectVisibilityProcessing<DecorativePaintingDecorSelectionState, DecoratingHeightHandle>())
                .Add(new GameObjectVisibilityProcessing<DecorativePaintingDecorSelectionState, DecorativeSculptingMenu>())

                .Add(new DecoratingMenuItemProcessing<DecorativePaintingDecorSelectionState, DecorativePaintingDrawableComponent,
                DecorativePaintingColorSelectionState, DecorativePaintingUpdateSignal>(_config.TextureID))
                .Add(new HeightHandlePlacingProcessing<DecorativePaintingDecorSelectionState, DecorativePaintingUpdateSignal>(dependencies.SculptingData, _config, false))

                .Add(new MeshVisibilityProcessing<DecorativePaintingDecorSelectionState, SculptingProductMesherTag>())

                .Add(new StateInfoWindowUIProcessing<DecorativePaintingDecorSelectionState>(_stateInfo))

                //color selection state systems
                .Add(new GameObjectVisibilityProcessing<DecorativePaintingColorSelectionState, DecorativePaintingColorMenu>())

                .Add(new DecorativePaintingColorMenuItemProcessing<DecorativePaintingColorSelectionState, DecorativePaintingDrawableComponent, DecorativePaintingState>("_DecPaintColor"))

                .Add(new MeshVisibilityDoubleStateProcessing<DecorativePaintingColorSelectionState, SculptingProductMesherTag, DecorativePaintingDecorSelectionState>())

                .Add(new StateInfoWindowUIProcessing<DecorativePaintingColorSelectionState>(_stateInfo))

                //paint state systems

                .Add(new DecoratingSkipProcessing<DecorativePaintingState, DecorativePaintingDrawableComponent, DrawableQuad>(_nextState))

                .Add(new GameObjectVisibilityDoubleStateProcessing<DecorativePaintingState, DecorativeSculptingContainer, DecorativePaintingDecorSelectionState>())
                .Add(new GameObjectVisibilityProcessing<DecorativePaintingState, BrushTool>())
                .Add(new GameObjectVisibilityProcessing<DecorativePaintingState, DrawableQuad>())

                .Add(new DecoratingPainterProcessing<DecorativePaintingState, DecorativePaintingDrawableComponent, DrawableQuad>(_config))
                .Add(new DecoratingCompletionTracker<DecorativePaintingState, DecorativePaintingDrawableComponent, DrawableQuad>(_config))

                //Outline
                .Add(new OutlineByStateSystem<DecorativePaintingState>(5))

                .Add(new SetGradeByProgress<DecorativePaintingState>(dependencies.ResultsEvaluationData, _stateInfo))

                // Achievement
                .Add(new JewelerAchievementSender<DecorativePaintingState>(dependencies.ResultsEvaluationData,dependencies.AchievementsService))

                .Add(new MeshVisibilityDoubleStateProcessing<DecorativePaintingState, SculptingProductMesherTag, DecorativePaintingDecorSelectionState>())

                .Add(new StateInfoWindowUIProcessing<DecorativePaintingState>(_stateInfo))
                .Add(new StateWindowPlacementProcessing<DecorativePaintingState>("Table"))
                .Add(new StateWindowProgressUIProcessing<DecorativePaintingState>())
                .Add(new StateWindowButtonUIProcessing<DecorativePaintingState>())
                .Add(new SkipStageButtonUIProcessing<DecorativePaintingState>())
                
                //voice systems 
                .Add(new VoiceAudioSystem<DecorativePaintingItemPlacingState>(_itemPlacingState))
                .Add(new VoiceAudioSystem<DecorativePaintingDecorSelectionState>(_decorSelectionState))
                .Add(new VoiceAudioSystem<DecorativePaintingState>(_decorativePaintingState))

                //travel point
                .Add(new TravelPointPositioningSystem<DecorativePaintingItemPlacingState>("Start"))
                .Add(new TravelPointPositioningSystem<DecorativePaintingDecorSelectionState>("Start"))
                .Add(new TravelPointPositioningSystem<DecorativePaintingState>("Start"))

                ;

            endFrame
                //sculpting product
                .Add(new SculptingDataProcessingSystem<DecorativePaintingItemPlacingState>(dependencies.SculptingData))
                .Add(new MeshRingsDataProcessingSystem<DecorativePaintingItemPlacingState>(dependencies.SculptingData))
                .Add(new MeshUpdateSystem<DecorativePaintingItemPlacingState, SculptingProductMesherTag>(dependencies.SculptingData))

                .Add(new DecoratingUV2Processing<DecorativePaintingDecorSelectionState, DecorativePaintingUpdateSignal>(dependencies.SculptingData, _config, false))
                .Add(new DecoratingTilingProcessing<DecorativePaintingDecorSelectionState, DecorativePaintingUpdateSignal>(dependencies.SculptingData, _config))
                .Add(new DrawableQuadUV2Processing<DecorativePaintingDecorSelectionState, DecorativePaintingUpdateSignal>(_config, false))

                .OneFrame<RestartCurrentStepMessage>()
                .OneFrame<NextStateSignal>()
                .OneFrame<SkipStageMessage>()
                .OneFrame<NextSubStateSignal>()

                .OneFrame<DecorativePaintingUpdateSignal>()
                .OneFrame<StackCutterHitSignal>()
                .OneFrame<DecorMenuItemSelected>()
                ;
            return systems;
        }
    }
}
