using Leopotam.Ecs;
using Modules.StateGroup.Components;
using Modules.StateGroup.Core;
using PWS.Common.UI;
using PWS.Pottery.Common;
using PWS.Pottery.MeshGeneration;
using PWS.Pottery.Stages.DecorativeSculptingStage;

namespace PWS.Pottery.Stages.DecorativePainting
{
    public class DecorativePaintingColorMenuItemProcessing<StateT, DrawableT, NextStateT> : IEcsRunSystem where StateT : struct where DrawableT : struct where NextStateT : struct
    {
        // auto injected fields
        readonly EcsFilter<StateEnter, StateT> _entering;
        readonly EcsFilter<StateT> _inState;
        readonly EcsFilter<StateT>.Exclude<StateExit> _inStateNotExit;

        readonly EcsFilter<NextSubStateSignal, DecorativePaintingColorMenu> _nextSubStateSignal;
        readonly EcsFilter<ColorMenuItemSelected, DecorativePaintingColorMenu> _selectedSignal;
        readonly EcsFilter<ClayMeshRenderer, SculptingProductTag> _meshRenderer;
        readonly EcsFilter<DecorativePaintingColorMenu> _menu;

        readonly EcsFilter<DrawableComponent<DrawableT>, SculptingProductTag> _drawable;

        private StateFactory _stateFactory;

        private string COL_ID;

        public DecorativePaintingColorMenuItemProcessing(string colId) {
            COL_ID = colId;
        }

        public void Run()
        {
            if (!_entering.IsEmpty())
            {
                foreach (var menu in _menu)
                {
                    _menu.Get1(menu).View.DisableColorGroupBySelected(COL_ID);
                    _menu.Get1(menu).View.SelectFirstItem();
                }
            }

            if (!_inState.IsEmpty())
            {
                foreach (var signal in _selectedSignal)
                {
                    foreach (var mr in _meshRenderer)
                    {
                        _meshRenderer.Get1(mr).MeshRenderer.sharedMaterial.SetColor(COL_ID, _selectedSignal.Get1(signal).Color);
                    }
                }
            }

            if (!_inStateNotExit.IsEmpty())
            {
                if (!_nextSubStateSignal.IsEmpty())
                {
                    _stateFactory.SetState<NextStateT>();
                }
            }
        }
    }
}
