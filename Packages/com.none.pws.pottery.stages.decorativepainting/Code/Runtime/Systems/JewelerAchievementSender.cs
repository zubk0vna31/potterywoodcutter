using Leopotam.Ecs;
using Modules.StateGroup.Components;
using PWS.Common.ResultsEvaluation;
using PWS.Common.UI;
using PWS.Features.Achievements;

namespace PWS.Pottery.Stages.DecorativePainting
{
    public class JewelerAchievementSender<T> : IEcsRunSystem where T : struct
    {
        private readonly EcsFilter<StateExit, T> _inState;

        private readonly IResultsEvaluationDataHolder _dataHolder;
        private readonly IAchievementsService _achievementsService;

        private readonly EcsFilter<StateWindow, StateProgress> _progress;

        public JewelerAchievementSender(IResultsEvaluationDataHolder dataHolder, IAchievementsService achievementsService)
        {
            _dataHolder = dataHolder;
            _achievementsService = achievementsService;
        }

        public void Run()
        {
            if (_inState.IsEmpty()) return;

            foreach (var i in _progress)
            {
                float value = _progress.Get2(i).Value;

                if (value > 0.99f)
                {
                    value = 1f;
                }

                _achievementsService.AppendProgressFloat(AchievementsNames.jeweler, value * 100f);
            }
        }
    }
}
