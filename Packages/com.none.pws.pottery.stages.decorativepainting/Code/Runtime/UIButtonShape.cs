using UnityEngine;
using UnityEngine.UI;

namespace PWS.Pottery.Stages.DecorativePainting
{
    public class UIButtonShape : MonoBehaviour
    {
        [SerializeField]
        private float _alphaHitTestMinimumThreshold = 0.1f;

        void Start()
        {
            GetComponent<Image>().alphaHitTestMinimumThreshold = _alphaHitTestMinimumThreshold;
        }
    }
}
