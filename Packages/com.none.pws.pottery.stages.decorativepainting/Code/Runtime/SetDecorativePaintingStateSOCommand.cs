using Modules.StateGroup.Core;
using UnityEngine;

namespace PWS.Pottery.Stages.DecorativePainting
{
    [CreateAssetMenu(fileName = "SetDecorativePaintingStateSOCommand", menuName = "PWS/Pottery/Stages/DecorativePaintingStage/SetStateSOCommand")]
    public class SetDecorativePaintingStateSOCommand : SetStateSOCommand<DecorativePaintingItemPlacingState>
    {
    }
}