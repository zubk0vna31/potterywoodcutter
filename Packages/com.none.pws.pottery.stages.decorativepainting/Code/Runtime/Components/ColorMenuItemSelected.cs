﻿using UnityEngine;

namespace PWS.Pottery.Stages.DecorativePainting
{
    public struct ColorMenuItemSelected
    {
        public Color Color;
    }
}