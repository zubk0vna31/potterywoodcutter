using Leopotam.Ecs;

namespace PWS.Pottery.Stages.DecorativePainting
{
    public struct DecorativePaintingItemPlacingState : IEcsIgnoreInFilter
    {
    }
    public struct DecorativePaintingDecorSelectionState : IEcsIgnoreInFilter
    {
    }
    public struct DecorativePaintingColorSelectionState : IEcsIgnoreInFilter
    {
    }
    public struct DecorativePaintingState : IEcsIgnoreInFilter
    {
    }
}
