﻿using Leopotam.Ecs;

namespace PWS.Pottery.Stages.DecorativePainting
{
    public struct DecorativePaintingUpdateSignal : IEcsIgnoreInFilter
    {
    }
}