﻿using Modules.ViewHub;
using Leopotam.Ecs;
using UnityEngine.UI;
using UnityEngine;

namespace PWS.Pottery.Stages.DecorativePainting
{
    public class DecorativePaintingColorMenuView : ViewComponent
    {
        [SerializeField]
        private bool dissableAfterInit;
        [SerializeField]
        private Toggle[] _items;
        [SerializeField]
        private int[] _itemsGroups;
        private int _selectedItem;

        [HideInInspector]
        public string KeyForState;

        private Color[] _colors;

        private EcsEntity _ecsEntity;

        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            _ecsEntity = ecsEntity;
            ecsEntity.Get<DecorativePaintingColorMenu>().View = this;

            AddColors();
            AddToggleListeners();

            if (dissableAfterInit)
            {
                gameObject.SetActive(false);
                SetItem(0);
            }
        }

        public void SetItem(int i)
        {
            Toggle curItem = _items[i];
            curItem.isOn = true;
            _selectedItem = i;
            _ecsEntity.Get<ColorMenuItemSelected>().Color = _colors[i];
        }
        public void SelectFirstItem()
        {
            for (int i = 0; i < _items.Length; i++)
            {
                if (_items[i].interactable)
                {
                    SetItem(i);
                    return;
                }
            }
        }

        public void DisableColorGroupBySelected(string key)
        {
            bool emptyKey = KeyForState == null || KeyForState.Length == 0;
            if (!emptyKey && !KeyForState.Equals(key))
            {
                int group = _itemsGroups[_selectedItem];
                for (int i = 0; i < _items.Length; i++)
                {
                    if (_itemsGroups[i] == group)
                    {
                        _items[i].interactable = false;
                    }
                }
            }
            KeyForState = key;
        }

        void AddColors()
        {
            _colors = new Color[_items.Length];

            for (int i = 0; i < _items.Length; i++)
            {
                Color col = _items[i].GetComponent<Image>().color;
                _colors[i] = col;
            }
        }

        void AddToggleListeners()
        {
            for (int i = 0; i < _items.Length; i++)
            {
                int id = i;
                _items[id].onValueChanged.AddListener((bool on) =>
                {
                    if (on)
                    {
                        _selectedItem = id;
                        _ecsEntity.Get<ColorMenuItemSelected>().Color = _colors[id];
                    }
                });
            }
        }
    }
}