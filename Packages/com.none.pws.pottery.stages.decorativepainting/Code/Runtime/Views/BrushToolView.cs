using Modules.ViewHub;
using Leopotam.Ecs;

namespace PWS.Pottery.Stages.DecorativePainting
{
    public class BrushToolView : ViewComponent
    {
        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            ecsEntity.Get<BrushTool>();
        }
    }
}