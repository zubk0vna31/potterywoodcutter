﻿using Modules.ViewHub;
using Leopotam.Ecs;
using UnityEngine;
using PWS.Pottery.Stages.DecorativeSculptingStage;

namespace PWS.Pottery.Stages.DecorativePainting
{
    public class DecorativePaintingDrawableRef : ViewComponent
    {
        [SerializeField]
        private Drawable _drawable;

        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            ecsEntity.Get<DrawableComponent<DecorativePaintingDrawableComponent>>().View = _drawable;
        }
    }
}