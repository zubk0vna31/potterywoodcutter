using Modules.Root.ContainerComponentModel;
using UnityEngine;

namespace PWS.Common.GameModeInfoService
{
    [CreateAssetMenu(fileName = "GameModeInfoServiceInstaller", menuName = "PWS/Common/GameModeInfoService/Installer", order = 0)]
    public class GameModeInfoServiceInstaller : ASOInstaller
    {
        [SerializeField]
        private GameMode _defaultGameMode;

        public override void Install(IContainer container)
        {
            container.Bind<IGameModeInfoService>(new GameModeInfoService(_defaultGameMode));
        }
    }
}