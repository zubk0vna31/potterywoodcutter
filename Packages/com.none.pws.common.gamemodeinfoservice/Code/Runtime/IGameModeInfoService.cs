namespace PWS.Common.GameModeInfoService
{
    public interface IGameModeInfoService
    {
        GameMode CurrentMode { get; set; }
    }

    public enum GameMode {
        Tutorial,
        Practice,
        Free
    }
}