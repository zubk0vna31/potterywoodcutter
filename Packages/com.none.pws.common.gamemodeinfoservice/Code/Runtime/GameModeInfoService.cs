namespace PWS.Common.GameModeInfoService {
    public class GameModeInfoService : IGameModeInfoService {
        private GameMode _currentMode;

        public GameMode CurrentMode {
            get => _currentMode;
            set => _currentMode = value;
        }

        public GameModeInfoService(GameMode gameMode)
        {
            _currentMode = gameMode;
        }
    }
}