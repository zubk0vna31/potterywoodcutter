using Modules.StateGroup.Core;
using UnityEngine;

namespace PWS.Pottery.Stages.FinalFiringStage
{
    [CreateAssetMenu(fileName = "SetFinalFiringStateSOCommand", menuName = "PWS/Pottery/Stages/FinalFiringStage/SetStateSOCommand")]
    public class SetFinalFiringStateSOCommand : SetStateSOCommand<FinalFiringItemPlacingState>
    {
    }
}
