using Leopotam.Ecs;
using Modules.Root.ContainerComponentModel;
using Modules.Root.ECS;
using Modules.StateGroup.Core;
using Modules.VRFeatures;
using PWS.Common.Audio;
using PWS.Common.Messages;
using PWS.Common.RestartStage;
using PWS.Common.ResultsEvaluation;
using PWS.Common.SceneSwitchService;
using PWS.Common.TravelPoints;
using PWS.Common.UI;
using PWS.Common.ValueRegulator;
using PWS.Pottery.Common;
using PWS.Pottery.ItemDataHolderService;
using PWS.Pottery.MeshGeneration;
using PWS.Pottery.Stages.InitialFiringStage;
using UnityEngine;

namespace PWS.Pottery.Stages.FinalFiringStage
{
    public class PotteryFinalFiringStageSystemsProvider : MonoBehaviour, ISystemsProvider
    {
        internal class SystemsDependencies
        {
            [Inject] public IValueRegulatorDataHolder<HoursRegulator> HoursData;
            [Inject] public IValueRegulatorDataHolder<TemperatureRegulator> TemperatureData;
            [Inject] public IResultsEvaluationDataHolder ResultsEvaluationData;
            [Inject] public ISceneSwitchService SceneSwitchService;
        }

        [SerializeField] private SetStateSOCommand _nextState;
        [SerializeField] private InitialFiringStateConfig _config;
        [SerializeField] private ValueRegulatorConfig _hoursRegulatorConfig;
        [SerializeField] private ValueRegulatorConfig _temperatureRegulatorConfig;
        [SerializeField] private StateInfoData _stateInfo;
        
        [Header("Voice config")] 
        [SerializeField] private VoiceConfig _itemPlacingVoiceConfig;
        [SerializeField] private VoiceConfig _firingStateVoiceConfig;

        public EcsSystems GetSystems(EcsWorld world, EcsSystems endFrame, EcsSystems mainSystems)
        {
            SystemsDependencies dependencies = new SystemsDependencies();
            SceneContainer.Instance.Inject(dependencies);

            EcsSystems systems = new EcsSystems(world);
            systems
                .Add(new FadeToStateOnUIActionSystem<FinalFiringState>(_nextState, dependencies.SceneSwitchService))
                .Add(new CurrentStateRestoreDataProcessing<FinalFiringState, ValueRegulatorDataHolder<HoursRegulator>>(dependencies.HoursData))
                .Add(new CurrentStateRestoreDataProcessing<FinalFiringState, ValueRegulatorDataHolder<TemperatureRegulator>>(dependencies.TemperatureData))

                //Outline
                .Add(new OutlineByStateSystem<FinalFiringItemPlacingState>(0))

                //item placing state systems
                .Add(new SnapSocketActivityProcessing<FinalFiringItemPlacingState, FiringSnapSocket>())

                .Add(new FiringItemPlacingProcessing<FinalFiringItemPlacingState, FinalFiringState>())

                .Add(new MeshVisibilityProcessing<FinalFiringItemPlacingState, SculptingProductMesherTag>())
                .Add(new XRInteractableProcessing<FinalFiringItemPlacingState, SculptingProductTag>())

                .Add(new StateInfoWindowUIProcessing<FinalFiringItemPlacingState>(_stateInfo))

                //state systems
                .Add(new StoveDoorProcessing<FinalFiringState>(_config))
                .Add(new GameObjectVisibilityProcessing<FinalFiringState, FiringContainer>())

                .Add(new GameObjectVisibilityProcessing<FinalFiringState, HoursRegulator>())
                .Add(new ValueRegulatorSetupProcessing<FinalFiringState, HoursRegulator>(_hoursRegulatorConfig, dependencies.HoursData, true))
                .Add(new GameObjectVisibilityProcessing<FinalFiringState, TemperatureRegulator>())
                .Add(new ValueRegulatorSetupProcessing<FinalFiringState, TemperatureRegulator>(_temperatureRegulatorConfig, dependencies.TemperatureData, true))

                .Add(new FiringCompletionTracker<FinalFiringState>(_hoursRegulatorConfig, _temperatureRegulatorConfig, dependencies.HoursData, dependencies.TemperatureData))
                .Add(new FinalFiringSpecularProcessing(_config))

                .Add(new SetGradeByValueRegulator<FinalFiringState, HoursRegulator>(dependencies.ResultsEvaluationData,
                dependencies.HoursData, _stateInfo, _config.TargetHours))
                .Add(new SetGradeByValueRegulator<FinalFiringState, TemperatureRegulator>(dependencies.ResultsEvaluationData,
                dependencies.TemperatureData, _stateInfo, _config.MinTemperature, _config.MaxTemperature, true))

                .Add(new StateInfoWindowUIProcessing<FinalFiringState>(_stateInfo))
                .Add(new StateWindowPlacementProcessing<FinalFiringState>("Firing"))
                .Add(new StateWindowButtonUIProcessing<FinalFiringState>())

                .Add(new XRInteractableProcessing<FinalFiringState, SculptingProductTag>())

                .Add(new MeshVisibilityProcessing<FinalFiringState, SculptingProductTag>())

                //voice
                .Add(new VoiceAudioSystem<FinalFiringItemPlacingState>(_itemPlacingVoiceConfig))
                .Add(new VoiceAudioSystem<FinalFiringState>(_firingStateVoiceConfig))
                
                //sound
                .Add(new StoveDoorSoundSystem<FinalFiringState>())
                .Add(new StoveSoundSystem<FinalFiringState>(dependencies.TemperatureData))

                //travel point
                .Add(new TravelPointPositioningSystem<FinalFiringItemPlacingState>("Stelaj"))
                .Add(new TravelPointPositioningSystem<FinalFiringState>("Stelaj"))

                ;

            endFrame
                .Add(new ValueRegulatorUpdateProcessing<FinalFiringState, HoursRegulator>(dependencies.HoursData))
                .Add(new ValueRegulatorUpdateProcessing<FinalFiringState, TemperatureRegulator>(dependencies.TemperatureData))

                .OneFrame<RestartCurrentStepMessage>()
                .OneFrame<NextStateSignal>()
                .OneFrame<ValueRegulatorChangedSignal>()
                ;

            return systems;
        }
    }
}
