﻿using Leopotam.Ecs;
using Modules.StateGroup.Components;
using PWS.Pottery.Common;
using PWS.Pottery.MeshGeneration;
using PWS.Pottery.Stages.InitialFiringStage;

namespace PWS.Pottery.Stages.FinalFiringStage
{
    public class FinalFiringSpecularProcessing : IEcsRunSystem
    {
        // auto injected fields
        readonly EcsFilter<StateExit, FinalFiringState> _exiting;

        readonly EcsFilter<ClayMeshRenderer, SculptingProductTag> _meshRenderer;

        private InitialFiringStateConfig _config;
        private const string FIRING_ID = "_FiringSpecular";

        public FinalFiringSpecularProcessing(InitialFiringStateConfig config)
        {
            _config = config;
        }

        public void Run()
        {
            if (!_exiting.IsEmpty())
            {
                foreach (var mr in _meshRenderer)
                {
                    _meshRenderer.Get1(mr).MeshRenderer.sharedMaterial.SetFloat(FIRING_ID, _config.FiringSpecular);
                }
            }
        }
    }
}