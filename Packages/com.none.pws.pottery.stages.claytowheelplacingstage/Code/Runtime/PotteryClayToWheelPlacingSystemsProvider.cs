
using Leopotam.Ecs;
using Modules.Root.ContainerComponentModel;
using Modules.Root.ECS;
using Modules.StateGroup.Core;
using Modules.VRFeatures;
using Modules.Audio;
using PWS.Common.UI;
using PWS.Pottery.Common;
using PWS.Common.Audio;
using UnityEngine;
using PWS.Pottery.Stages.ClayPreparationStage;
using PWS.Common.TravelPoints;

namespace PWS.Pottery.Stages.ClayToWheelPlacingStage
{
    public class PotteryClayToWheelPlacingSystemsProvider : MonoBehaviour, ISystemsProvider
    {
        internal class SystemsDependencies
        {
        }

        [SerializeField] private SetStateSOCommand _nextState;
        [SerializeField] private StateInfoData _stateInfo;

        [Header("Voice config")] 
        [SerializeField] private VoiceConfig _voiceConfig;
        
        public EcsSystems GetSystems(EcsWorld world, EcsSystems endFrame, EcsSystems mainSystems)
        {
            SystemsDependencies dependencies = new SystemsDependencies();
            SceneContainer.Instance.Inject(dependencies);

            EcsSystems systems = new EcsSystems(world);
            systems

                //state systems
                .Add(new GameObjectVisibilityProcessing<ClayToWheelPlacingState, ClayCubeContainer>())
                .Add(new SnapSocketActivityProcessing<ClayToWheelPlacingState, WheelSnapSocket>())
                .Add(new ClayToWheelPlacingProcessing<ClayToWheelPlacingState>(_nextState))

                //ui
                .Add(new StateInfoWindowUIProcessing<ClayToWheelPlacingState>(_stateInfo))

                //voice
                .Add(new VoiceAudioSystem<ClayToWheelPlacingState>(_voiceConfig))

                //travel point
                .Add(new TravelPointPositioningSystem<ClayToWheelPlacingState>("Wheel"))

                ;

            endFrame
                .OneFrame<NextStateSignal>()
                .OneFrame<SelectEntered>()
                ;

            return systems;
        }
    }
}
