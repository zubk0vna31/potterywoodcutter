using Leopotam.Ecs;
using Modules.ViewHub;

namespace PWS.Pottery.Stages.ClayToWheelPlacingStage
{
    public class WheelSnapSocketView : ViewComponent
    {
        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            ecsEntity.Get<WheelSnapSocket>();
        }
    }
}
