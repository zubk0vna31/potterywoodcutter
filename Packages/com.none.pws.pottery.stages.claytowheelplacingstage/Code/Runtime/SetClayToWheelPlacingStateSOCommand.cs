using Modules.StateGroup.Core;
using UnityEngine;

namespace PWS.Pottery.Stages.ClayToWheelPlacingStage
{
    [CreateAssetMenu(fileName = "SetClayToWheelPlacingStateSOCommand", menuName = "PWS/Pottery/Stages/ClayToWheelPlacingStage/SetStateSOCommand")]
    public class SetClayToWheelPlacingStateSOCommand : SetStateSOCommand<ClayToWheelPlacingState>
    {

    }
}