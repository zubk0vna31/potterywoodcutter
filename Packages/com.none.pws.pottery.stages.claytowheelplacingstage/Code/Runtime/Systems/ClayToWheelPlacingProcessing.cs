using Leopotam.Ecs;
using Modules.StateGroup.Components;
using Modules.StateGroup.Core;
using Modules.VRFeatures;
using UnityEngine;

namespace PWS.Pottery.Stages.ClayToWheelPlacingStage
{
    public class ClayToWheelPlacingProcessing<StateT> : IEcsRunSystem where StateT : struct
    {
        // auto injected fields
        readonly EcsFilter<StateEnter, StateT> _entering;
        readonly EcsFilter<StateT>.Exclude<StateExit> _inState;


        readonly EcsFilter<WheelSnapSocket, SelectEntered> _placingSignal;

        private StateFactory _stateFactory;
        protected readonly SetStateSOCommand _nextState;

        //runtime data
        private const float _waitTime = 0.3f;
        private float _timer;
        private bool _isSelected;
        public ClayToWheelPlacingProcessing(SetStateSOCommand nextState)
        {
            _nextState = nextState;
        }

        public void Run()
        {
            if (!_entering.IsEmpty())
            {
                _timer = 0;
                _isSelected = false;
            }

            if (_inState.IsEmpty())
                return;

            foreach (var signal in _placingSignal)
            {
                /*foreach (var product in _product)
                {
                    _product.Get1(product).View.EnableCollider(false);
                }*/
                _isSelected = true;
            }

            if (_isSelected)
            {
                if (_timer < _waitTime)
                {
                    _timer += Time.deltaTime;
                }
                else
                {
                    _nextState.Execute(_stateFactory);
                }
            }
        }
    }
}
