﻿using UnityEngine;
using Leopotam.Ecs;
using System;

namespace Modules.UPhysics
{
    public struct Triggered 
    {
        public Collider Collider;
        public EcsEntity Other;
    }

    public struct TriggeredExited
    {
        public Collider Collider;
        public EcsEntity Other;
    }
}
