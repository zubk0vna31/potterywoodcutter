﻿using Leopotam.Ecs;

namespace Modules.UPhysics
{
    public struct Collidied 
    {
        public UnityEngine.Collision Collision;
        public EcsEntity Other;
    }
}
