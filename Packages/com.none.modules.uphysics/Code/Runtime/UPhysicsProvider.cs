﻿using Leopotam.Ecs;
using Modules.Root.ECS;
using UnityEngine;

namespace Modules.UPhysics
{
    [CreateAssetMenu(menuName = "Modules/UPhysics/Provider")]
    public class UPhysicsProvider : ScriptableObject, ISystemsProvider
    {
        public EcsSystems GetSystems(EcsWorld world, EcsSystems endFrame, EcsSystems ecsSystems)
        {
            EcsSystems systems = new EcsSystems(world, this.name);

            endFrame.OneFrame<Collidied>();
            endFrame.OneFrame<Triggered>();

            //Custom Trigger Exited
            endFrame.OneFrame<TriggeredExited>();

            return systems;
        }
    }
}
