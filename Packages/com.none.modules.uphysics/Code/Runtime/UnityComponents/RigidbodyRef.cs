﻿using UnityEngine;
using Modules.ViewHub;
using Leopotam.Ecs;

namespace Modules.UPhysics
{
    [RequireComponent(typeof(Rigidbody))]
    public class RigidbodyRef : ViewComponent
    {
        [SerializeField] private Rigidbody _rb;

        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            ecsEntity.Get<RigidbodyComponent>().Rigidbody = _rb;
        }

#if UNITY_EDITOR

        private void Reset()
        {
            _rb = GetComponent<Rigidbody>();
        }

#endif
    }
}

