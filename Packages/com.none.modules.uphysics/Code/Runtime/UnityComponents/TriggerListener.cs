﻿using UnityEngine;
using Leopotam.Ecs;
using Modules.ViewHub;

namespace Modules.UPhysics
{
    public class TriggerListener : ViewComponent
    {
        public EcsEntity Entity;

        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            Entity = ecsEntity;

        }

        private void OnTriggerEnter(Collider other)
        {
            EntityRef listener;

            ref var triggered = ref Entity.Get<Triggered>();
            triggered.Collider = other;

            if (other.TryGetComponent<EntityRef>(out listener))
            {
                triggered.Other = listener.Entity;
            }
            else
            {
                listener = other.GetComponentInParent<EntityRef>();
                if (listener == null)
                {
                    triggered.Other = EcsEntity.Null;
                }
                else
                {
                    triggered.Other = listener.Entity;
                }
            }
        }


        private void OnTriggerExit(Collider other)
        {
            EntityRef listener;

            ref var triggered = ref Entity.Get<TriggeredExited>();
            triggered.Collider = other;

            if (other.TryGetComponent<EntityRef>(out listener))
            {
                triggered.Other = listener.Entity;
            }
            else
            {
                listener = other.GetComponentInParent<EntityRef>();
                if (listener == null)
                {
                    triggered.Other = EcsEntity.Null;
                }
                else
                {
                    triggered.Other = listener.Entity;
                }
            }
        }
    }
}
