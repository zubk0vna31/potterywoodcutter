﻿using Leopotam.Ecs;
using Modules.ViewHub;
using UnityEngine;

namespace Modules.UPhysics
{
    public class CollisionListener : ViewComponent
    {
        public EcsEntity Entity;

        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            Entity = ecsEntity;
        }

        private void OnCollisionEnter(Collision collision)
        {
           
            EntityRef listener;

            ref var collided = ref Entity.Get<Collidied>();
            collided.Collision = collision;

            if (collision.transform.TryGetComponent<EntityRef>(out listener))
            {
                collided.Other = listener.Entity;
            }
            else 
            {
                listener = collision.transform.GetComponentInParent<EntityRef>();
                if (listener == null) 
                {
                    collided.Other = EcsEntity.Null;
                }
                else 
                {
                    collided.Other = listener.Entity;
                }
            } 
        }
    }
}
