using System.Collections.Generic;
using Leopotam.Ecs;
using UnityEngine;

namespace Modules.ViewHub
{
    public class EntityTemplate : MonoBehaviour
    {
        public List<ViewComponent> _components = new List<ViewComponent>();
        public bool Initialized => _initialized;
        protected bool _initialized = false;

        // use with custom spawn logic
        public virtual void Init(EcsEntity entity, EcsWorld world)
        {
            if (_initialized)
            {
               UnityEngine.Debug.LogWarning("Entity was initialized previously", this);
               return;
            }
                
            SetupEntity(entity, world);
            _components.ForEach((component => component.EntityInit(entity, world)));
            _initialized = true;
        }

        // override to setup custom initialization logic
        protected virtual void SetupEntity(EcsEntity entity, EcsWorld world)
        {
            this.gameObject.AddComponent<EntityRef>().Entity = entity;
            ref UnityView view = ref entity.Get<UnityView>();
            view.GameObject = this.gameObject;
            view.Transform = transform;
        }
        
        // for specific cases, generally with pooling
        public virtual void ResetInitializedStatus()
        {
            _initialized = false;
        }

#if UNITY_EDITOR

        [ContextMenu("Collect components")]
        private void CollectComponents()
        {
            _components = new List<ViewComponent>(transform.GetComponentsInChildren<ViewComponent>());
        }

#endif
    }
}