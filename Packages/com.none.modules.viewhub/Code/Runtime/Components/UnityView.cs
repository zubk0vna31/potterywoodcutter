using Leopotam.Ecs;
using UnityEngine;

namespace Modules.ViewHub
{

    public struct UnityView : IEcsAutoReset<UnityView>
    {
        public GameObject GameObject;
        public Transform Transform;

        public void AutoReset(ref UnityView c)
        {
            if (c.GameObject != null)
                Object.Destroy(c.GameObject);
            c.GameObject = null;
            c.Transform = null;
        }
    }
}