using UnityEngine;
using Leopotam.Ecs;

namespace Modules.ViewHub.Systems
{
    public class SceneEntitiesInitSystem : IEcsInitSystem
    {
        private readonly EcsWorld _world;

        public void Init()
        {
            foreach (var entityTemplate in GameObject.FindObjectsOfType<EntityTemplate>())
            {
                if(!entityTemplate.Initialized)
                    entityTemplate.Init(_world.NewEntity(), _world);
            }
        }
    }
}