﻿using Leopotam.Ecs;
using Modules.Root.ECS;
using Modules.ViewHub.Systems;
using UnityEngine;

namespace Modules.ViewHub
{
    [CreateAssetMenu(menuName ="Modules/ViewHub/Provider")]
    public class ViewHubProvider : ScriptableObject, ISystemsProvider
    {
        public EcsSystems GetSystems(EcsWorld world, EcsSystems endFrame, EcsSystems ecsSystems)
        {
            EcsSystems systems = new EcsSystems(world, this.name);
            
            systems
                .Add(new SceneEntitiesInitSystem());

            return systems;
        }
    }
}
