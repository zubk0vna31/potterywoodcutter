using Leopotam.Ecs;
using Modules.StateGroup.Core;

namespace PWS.Common.UI
{
    public class TransitToStateOnUIActionSystem<StateT> : IEcsRunSystem where StateT : struct
    {
        // auto injected fields
        protected readonly EcsFilter<StateT> _inState;
        protected readonly EcsFilter<NextStateSignal> _actionPerformed;
        protected readonly StateFactory _stateFactory;

        protected readonly SetStateSOCommand _nextState;

        public TransitToStateOnUIActionSystem(SetStateSOCommand nextState)
        {
            _nextState = nextState;
        }

        public virtual void Run()
        {
            if (_inState.IsEmpty()) return;
            if (_actionPerformed.IsEmpty()) return;

            foreach (var i in _actionPerformed)
            {
                _actionPerformed.GetEntity(i).Del<NextStateSignal>();
            }

            _nextState.Execute(_stateFactory);
        }
    }
}
