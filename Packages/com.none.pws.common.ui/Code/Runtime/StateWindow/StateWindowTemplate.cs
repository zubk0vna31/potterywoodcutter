using Leopotam.Ecs;
using Modules.ViewHub;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Modules.Root.ContainerComponentModel;
using PWS.Common.GameModeInfoService;
using I2.Loc;
using PWS.FeatureToggles;

namespace PWS.Common.UI
{
    public class StateWindowTemplate : EntityTemplate
    {
        [Inject] IGameModeInfoService _gameModeData;

        [Header("Data")]
        [SerializeField] private string _defaultButtonTitleTerm;
        [SerializeField] private Localize _buttonLocalize;
        [SerializeField] private string _defaultButtonTitle;
        [SerializeField] private TMP_Text _buttonLabel;

        [Header("View")]
        [SerializeField] private GameObject _canvas;
        [SerializeField] private TMP_Text _label;
        [SerializeField] private Button _nextButton;
        [SerializeField] private Button _completeButton;



        [Header("Progress")]
        [SerializeField] private Slider _completionSlider;
        [SerializeField] private Slider _completionSliderWithStars;
        [SerializeField] private TMP_Text _completionLabel;
        [SerializeField] private Image _completionFill;
        [SerializeField] private Color _completedColor = Color.green;
        [SerializeField] private Color _notCompletedColor = Color.white;

        private EcsEntity _entity;
        private GameMode _gameMode;
        private bool _useSliderWithStars;

        protected override void SetupEntity(EcsEntity entity, EcsWorld world)
        {
            base.SetupEntity(entity, world);

            _entity = entity;
            entity.Get<StateWindow>().Template = this;
            entity.Get<StateProgress>();

            HideButtons();
            ShowButton(false);
            ShowLabel(false);
            ShowSlider(false);

            SceneContainer.Instance.Inject(this);
            _gameMode = _gameModeData.CurrentMode;
        }

        private void Start()
        {
            _nextButton.onClick.AddListener(OnClickNext);
            _completeButton.onClick.AddListener(OnClickComplete);

            if (FeatureManagerFacade.FeatureEnabled(StateWindowWithStarsFeatures.PWS_Common_UI_WindowWithStars))
            {
                _completionSlider = _completionSliderWithStars;
                _useSliderWithStars = true;
            }
        }
        private void OnDestroy()
        {
            _nextButton.onClick.RemoveListener(OnClickNext);
        }
        public void Show()
        {
            _canvas.SetActive(true);
        }
        public void Hide()
        {
            _canvas.SetActive(false);
        }
        public void SetSliderValue(float value)
        {
            if (_useSliderWithStars)
            {
                _completionSlider.value = 0.9f * value + 1.6f * Mathf.Abs(value - 0.75f) - 1.5f * Mathf.Abs(value - 0.8f); // formula is needed to fill 3 stars at 75% progress, 4 stars at 80% progress
            }
            else
            {
                _completionSlider.value = value;
            }
        }

        public void SetSliderValue(float value, bool useCustomFormula)
        {
            _completionSlider.value = 0.3335f * value + 0.5001f + 1.333f * Mathf.Abs(value - 0.75f) - 1.6665f * Mathf.Abs(value - 0.9f); // formula is needed to fill 3 stars at 75% progress, 4 stars at 80% progress
        }
        public void SetSliderLabel(float value)
        {
            _completionLabel.text = value.ToString("0") + " %";
        }
        public void SetSliderColor(bool completed)
        {
            _completionFill.color = completed ? _completedColor : _notCompletedColor;
        }

        public void ShowSlider(bool value)
        {
            if (_gameMode == GameMode.Free)
                return;

            _completionSlider.gameObject.SetActive(value);
        }

        public void SetLabel(string text)
        {
            _label.text = text;
        }
        public void ShowLabel(bool value)
        {
            _label.gameObject.SetActive(value);
        }

        private void OnClickNext()
        {
            _entity.Get<NextStateSignal>().button = _nextButton;
        }

        private void OnClickComplete()
        {
            _entity.Get<CompleteStateSignal>().button = _completeButton;
        }

        public void MakeSignal()
        {
            _entity.Get<NextStateSignal>().button = _nextButton;
        }

        public void ShowButton(bool value)
        {
            if (_nextButton.gameObject.activeSelf == value)
                return;

            _nextButton.gameObject.SetActive(value);
        }

        public void ShowCompleteButton()
        {
            _completeButton.gameObject.SetActive(true);
            _nextButton.gameObject.SetActive(false);
        }

        public void ShowSkipButton()
        {
            _nextButton.gameObject.SetActive(true);
            _completeButton.gameObject.SetActive(false);
        }

        public void HideButtons()
        {
            _nextButton.gameObject.SetActive(false);
            _completeButton.gameObject.SetActive(false);
        }

        public void ToggleButtonInteraction(bool value)
        {
            if (_nextButton.interactable == value)
                return;

            _nextButton.interactable = value;
        }

        public void ChangeButtonTitle(string title)
        {
            _buttonLocalize.SetTerm(title);
            //_buttonLabel.text = title;
        }

        public void ResetButtonTitle()
        {
            _buttonLocalize.SetTerm(_defaultButtonTitleTerm);
            //_buttonLabel.text = _defaultButtonTitle;
        }
    }
}
