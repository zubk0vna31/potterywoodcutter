﻿using Leopotam.Ecs;
using Modules.StateGroup.Components;
using UnityEngine;

namespace PWS.Common.UI
{
    public class StateWindowPlacementProcessing<StateT> : IEcsRunSystem where StateT : struct
    {
        // auto injected fields
        readonly EcsFilter<StateT>.Exclude<StateEnter> _inState;
        readonly EcsFilter<StateWindowPlacement> _stateWindow;

        private string _placementName;

        public StateWindowPlacementProcessing(string placementName)
        {
            _placementName = placementName;
        }

        public void Run()
        {
            if (!_inState.IsEmpty())
            {

                foreach (var i in _stateWindow)
                {
                    _stateWindow.Get1(i).View.ChangePlacement(_placementName);
                }
            }
        }
    }
}