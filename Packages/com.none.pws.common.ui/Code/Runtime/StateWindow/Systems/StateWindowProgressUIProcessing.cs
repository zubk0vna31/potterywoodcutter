﻿using UnityEngine;
using Leopotam.Ecs;
using Modules.StateGroup.Components;

namespace PWS.Common.UI
{
    public class StateWindowProgressUIProcessing<StateT> : IEcsRunSystem where StateT : struct
    {
        // auto injected fields
        readonly EcsFilter<StateEnter, StateT> _entering;
        readonly EcsFilter<StateExit, StateT> _exiting;
        readonly EcsFilter<StateT> _inState;
        readonly EcsFilter<StateWindow, StateProgress> _stateWindowWithProgress;

        private bool _useCustomFormula;
        public StateWindowProgressUIProcessing(bool useCustomFormula = false)
        {
            _useCustomFormula = useCustomFormula;
        }
        public void Run()
        {
            if (_inState.IsEmpty()) return;

            foreach (var i in _stateWindowWithProgress)
            {
                if (!_entering.IsEmpty())
                {
                    _stateWindowWithProgress.Get1(i).Template.ShowSlider(true);
                }
                if (!_inState.IsEmpty())
                {
                    float value = _stateWindowWithProgress.Get2(i).Value;
                    float thresold = _stateWindowWithProgress.Get2(i).CompletionThreshold;
                    
                    if (_useCustomFormula) _stateWindowWithProgress.Get1(i).Template.SetSliderValue(value, true);
                    else _stateWindowWithProgress.Get1(i).Template.SetSliderValue(value);
                    _stateWindowWithProgress.Get1(i).Template.SetSliderLabel(Mathf.Floor(value * 1000) / 10);
                    //_stateWindowWithProgress.Get1(i).Instance.SetSliderColor(value >= thresold);
                }
                if (!_exiting.IsEmpty())
                {
                    _stateWindowWithProgress.Get1(i).Template.SetSliderValue(0);
                    _stateWindowWithProgress.Get1(i).Template.SetSliderLabel(0);
                    //_stateWindowWithProgress.Get1(i).Instance.SetSliderColor(false);
                    _stateWindowWithProgress.Get1(i).Template.ShowSlider(false);
                }
            }
        }
    }
}