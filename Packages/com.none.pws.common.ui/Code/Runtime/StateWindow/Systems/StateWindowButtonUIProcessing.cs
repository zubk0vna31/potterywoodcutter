﻿using Leopotam.Ecs;
using Modules.StateGroup.Components;
using UnityEngine;

namespace PWS.Common.UI
{
    public class StateWindowButtonUIProcessing<StateT> : IEcsRunSystem where StateT : struct
    {
        // auto injected fields
        readonly EcsFilter<StateExit, StateT> _exiting;
        readonly EcsFilter<StateEnter, StateT> _entering;
        readonly EcsFilter<StateT> _inState;
        readonly EcsFilter<StateWindow, StateProgress>.Exclude<Ignore> _stateWindowWithProgress;
        readonly EcsFilter<StateWindow, Ignore> _stateWindowIgnored;

        private readonly bool potteryUsecase;

        public StateWindowButtonUIProcessing(bool potteryUsecase=true)
        {
            this.potteryUsecase = potteryUsecase;
        }

        public void Run()
        {
            if (potteryUsecase)
            {
                PotteryRun();
            }
            else
            {
                WoodCuttingRun();
            }
        }

        private void WoodCuttingRun()
        {
            if (_inState.IsEmpty()) return;

            foreach (var i in _stateWindowWithProgress)
            {
                ref var stateWindow = ref _stateWindowWithProgress.Get1(i);
                ref var stateProgress = ref _stateWindowWithProgress.Get2(i);

                if (!_entering.IsEmpty())
                {

                }

                if (!_inState.IsEmpty())
                {
                    //if (stateProgress.Value >= stateProgress.CompletionThreshold)
                    //{
                    //    stateWindow.Template.ShowCompleteButton();
                    //}
                    //else
                    if (stateProgress.Value >= stateProgress.SkipThreshold)
                    {
                        stateWindow.Template.ShowSkipButton();
                    }
                    else
                    {
                        stateWindow.Template.HideButtons();
                    }

                }

                if (!_exiting.IsEmpty())
                {
                    stateWindow.Template.HideButtons();
                }
            }

            if (!_entering.IsEmpty() || !_exiting.IsEmpty())
            {
                foreach (var i in _stateWindowIgnored)
                {
                    _stateWindowIgnored.GetEntity(i).Del<Ignore>();
                }
            }
        }

        private void PotteryRun()
        {
            if (_inState.IsEmpty()) return;

            foreach (var i in _stateWindowWithProgress)
            {
                if (!_entering.IsEmpty())
                {
                    _stateWindowWithProgress.GetEntity(i).Del<Ignore>();
                }

                if (!_inState.IsEmpty())
                {
                    _stateWindowWithProgress.Get1(i).Template.ShowButton(
                        _stateWindowWithProgress.Get2(i).Value >= _stateWindowWithProgress.Get2(i).CompletionThreshold);
                }

                if (!_exiting.IsEmpty())
                {
                    _stateWindowWithProgress.Get1(i).Template.ShowButton(false);
                }
            }

            if (!_entering.IsEmpty() || !_exiting.IsEmpty())
            {
                foreach (var i in _stateWindowIgnored)
                {
                    _stateWindowIgnored.GetEntity(i).Del<Ignore>();
                }
            }
        }
    }
}