﻿using Leopotam.Ecs;
using Modules.StateGroup.Components;

namespace PWS.Common.UI
{
    public class StateWindowLabelUIProcessing<StateT> : IEcsRunSystem where StateT : struct
    {
        // auto injected fields
        readonly EcsFilter<StateEnter, StateT> _entering;
        readonly EcsFilter<StateExit, StateT> _exiting;
        readonly EcsFilter<StateWindow> _stateWindow;

        private string _labelText;

        public StateWindowLabelUIProcessing(string labelText)
        {
            _labelText = labelText;
        }

        public void Run()
        {
            foreach (var i in _stateWindow)
            {
                if (!_entering.IsEmpty())
                {
                    _stateWindow.Get1(i).Template.SetLabel(_labelText);
                    _stateWindow.Get1(i).Template.ShowLabel(true);
                }

                if (!_exiting.IsEmpty())
                {
                    _stateWindow.Get1(i).Template.SetLabel("");
                    _stateWindow.Get1(i).Template.ShowLabel(false);
                }
            }
        }
    }
}