using Leopotam.Ecs;
using Modules.ViewHub;
using System.Collections.Generic;
using UnityEngine;

namespace PWS.Common.UI
{
    public class StateWindowPlacementView : ViewComponent
    {
        [SerializeField]
        private List<WindowPlacement> _placements = new List<WindowPlacement>();
        private Transform _tr;

        private string _currentPlacement = "";

        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            ecsEntity.Get<StateWindowPlacement>().View = this;
            _tr = transform;
        }

        [ContextMenu("Add Current Placement")]
        void AddCurrentPlacement()
        {
            WindowPlacement placement = new WindowPlacement();
            placement.Name = "new";
            placement.Position = transform.position;
            placement.Rotation = transform.eulerAngles;
            placement.Scale = transform.localScale;
            _placements.Add(placement);
        }

        public void ChangePlacement(string name)
        {
            if (_currentPlacement.Equals(name))
                return;

            foreach (WindowPlacement placement in _placements)
            {
                if (placement.Name.Equals(name))
                {
                    _tr.position = placement.Position;
                    _tr.rotation = Quaternion.Euler(placement.Rotation);
                    _tr.localScale = placement.Scale;
                    _currentPlacement = name;

                    return;
                }
            }
        }
    }

    [System.Serializable]
    public class WindowPlacement
    {
        public string Name;
        public Vector3 Position;
        public Vector3 Rotation;
        public Vector3 Scale;
    }
}
