﻿using UnityEngine;

namespace PWS.Common.UI
{
    public struct StateProgress
    {
        public float Value;
        public float CompletionThreshold;
        public float SkipThreshold;

        public float ProgressValue => Mathf.RoundToInt(Mathf.Floor(Value * 1000f) / 10f);
    }
}