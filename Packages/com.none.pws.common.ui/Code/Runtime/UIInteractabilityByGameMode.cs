using Modules.Root.ContainerComponentModel;
using PWS.Common.GameModeInfoService;
using UnityEngine;
using UnityEngine.UI;

namespace PWS.Common.UI
{
    public class UIInteractabilityByGameMode : MonoBehaviour
    {
        [Inject] IGameModeInfoService _gameModeData;

        [SerializeField]
        private Selectable _selectable;

        public void Start()
        {
            SceneContainer.Instance.Inject(this);
            _selectable.interactable = _gameModeData.CurrentMode != GameMode.Free;
        }
    }
}
