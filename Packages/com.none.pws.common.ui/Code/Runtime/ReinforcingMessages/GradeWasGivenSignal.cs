namespace PWS.Common
{
    public struct GradeWasGivenSignal
    {
        public float Grade;
    }
}
