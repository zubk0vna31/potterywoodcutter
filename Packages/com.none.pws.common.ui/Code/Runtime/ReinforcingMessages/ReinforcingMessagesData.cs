using UnityEngine;

namespace PWS.Common.UI
{
    [CreateAssetMenu(fileName = "ReinforcingMessagesData", menuName = "PWS/Stages/ReinforcingMessagesData/Data", order = 0)]
    public class ReinforcingMessagesData : ScriptableObject
    {
        public ReinforcingMessagePair[] PositiveTerms;
        public ReinforcingMessagePair[] NegativeTerms;
        public float Threshold = 0.85f;
    }

    [System.Serializable]
    public class ReinforcingMessagePair
    {
        public string TitleTerm;
        public string DescTerm;
    }
}
