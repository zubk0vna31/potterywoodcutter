using I2.Loc;
using Leopotam.Ecs;
using Modules.ViewHub;
using System.Collections;
using UnityEngine;
using DG.Tweening;

namespace PWS.Common.UI
{
    public class ReinforcingMessageView : ViewComponent
    {
        [SerializeField] private GameObject _showHideTarget;

        [SerializeField] private Localize _titleLocalize;
        [SerializeField] private Localize _descLocalize;
        [SerializeField] private GameObject _fingerUpImage;
        [SerializeField] private GameObject _fingerDownImage;

        [SerializeField] private ReinforcingMessagesData _data;

        private EcsEntity _entity;

        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            _entity = ecsEntity;
            ecsEntity.Get<ReinforcingMessage>().View = this;
            Hide();
        }
        public void Show()
        {
            /*if (_showHideTarget.activeSelf)
                return;*/

            _showHideTarget.gameObject.SetActive(true);
            _showHideTarget.transform.DOShakeScale(0.5f, 0.0001f);

            StopCoroutine("HideTimer");
            //StartCoroutine(UpdateCanvas());
            StartCoroutine("HideTimer");
        }

        IEnumerator UpdateCanvas()
        {
            _showHideTarget.gameObject.SetActive(true);
            yield return null;
            _showHideTarget.gameObject.SetActive(false);
            _showHideTarget.gameObject.SetActive(true);
        }

        IEnumerator HideTimer()
        {
            yield return new WaitForSeconds(5);
            Hide();
        }

        public void Hide()
        {
            if (!_showHideTarget.activeSelf)
                return;

            _showHideTarget.gameObject.SetActive(false);
        }

        public void SetData(float grade)
        {
            bool positive = grade >= _data.Threshold;

            ReinforcingMessagePair pair;
            if (positive)
                pair = _data.PositiveTerms[Random.Range(0, _data.PositiveTerms.Length)];
            else
                pair = _data.NegativeTerms[Random.Range(0, _data.NegativeTerms.Length)];

            _titleLocalize.SetTerm(pair.TitleTerm);
            _descLocalize.SetTerm(pair.DescTerm);

            _fingerUpImage.SetActive(positive);
            _fingerDownImage.SetActive(!positive);
        }
    }
}
