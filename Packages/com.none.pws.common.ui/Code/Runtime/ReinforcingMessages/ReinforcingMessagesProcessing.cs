using Leopotam.Ecs;
using Modules.ViewHub;
using PWS.FeatureToggles;
using PWS.HUB.Simulation;
using UnityEngine;

namespace PWS.Common.UI
{
    public class ReinforcingMessagesProcessing : IEcsRunSystem
    {
        // auto injected fields
        readonly EcsFilter<GradeWasGivenSignal> _signal;
        readonly EcsFilter<ReinforcingMessage, UnityView> _reinforcingMessage;
        readonly EcsFilter<StateWindowPlacement, UnityView> _stateWindow;

        public void Run()
        {
            if (!FeatureManagerFacade.FeatureEnabled(Features.ReinforcingMessages))
                return;

            foreach (var signal in _signal)
            {
                foreach (var rm in _reinforcingMessage)
                {
                    foreach (var sw in _stateWindow)
                    {
                        _reinforcingMessage.Get2(sw).Transform.position = _stateWindow.Get2(sw).Transform.position;
                        _reinforcingMessage.Get2(sw).Transform.rotation = _stateWindow.Get2(sw).Transform.rotation;
                    }
                    Debug.Log("_reinforcingMessage: " + _signal.Get1(signal).Grade);
                    _reinforcingMessage.Get1(rm).View.SetData(_signal.Get1(signal).Grade);
                    _reinforcingMessage.Get1(rm).View.Show();
                }
            }       
        }
    }
}
