using Leopotam.Ecs;
using Modules.Root.ContainerComponentModel;
using Modules.Root.ECS;
using UnityEngine;

namespace PWS.Common.UI
{
    public class ReinforcingMessagesProvider : MonoBehaviour, ISystemsProvider
    {
        internal class SystemsDependencies
        {

        }

        public EcsSystems GetSystems(EcsWorld world, EcsSystems endFrame, EcsSystems mainSystems)
        {
            SystemsDependencies dependencies = new SystemsDependencies();
            SceneContainer.Instance.Inject(dependencies);

            EcsSystems systems = new EcsSystems(world);

            systems
                .Add(new ReinforcingMessagesProcessing())
            ;

            endFrame
                .OneFrame<GradeWasGivenSignal>()
            ;

            return systems;

        }
    }
}