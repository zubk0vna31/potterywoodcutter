
namespace PWS.Common.UI
{
    public struct SubstageProgress 
    {
        public float Value;
        public float SkipValue;
        public float CompleteValue;

        public bool CanBeSkipped => (Value >= SkipValue || Value >= CompleteValue);
    }
}
