using Leopotam.Ecs;
using Modules.ViewHub;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace PWS.Common.UI
{
    public class SubstageButtonView : ViewComponent
    {
        [SerializeField]
        private GameObject _showHideObject;
        [SerializeField]
        private Button _button;

        private EcsEntity _entity;

        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            _entity = ecsEntity;
            _entity.Get<SubstageButton>().View = this;
            _button.onClick.AddListener(Click);

            Toggle(false);
        }

        public void Toggle(bool value)
        {
            if (_showHideObject.activeSelf == value) return;
            _showHideObject.gameObject.SetActive(value);
        }

        public void Click()
        {
            _entity.Get<SubstageSignal>();
        }


    }
}
