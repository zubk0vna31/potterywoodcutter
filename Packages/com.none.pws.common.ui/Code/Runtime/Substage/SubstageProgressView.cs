using Leopotam.Ecs;
using Modules.ViewHub;

namespace PWS.Common.UI
{
    public class SubstageProgressView : ViewComponent
    {
        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            ecsEntity.Get<SubstageProgress>();
        }
    }
}
