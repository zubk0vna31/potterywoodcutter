using Leopotam.Ecs;
using Modules.StateGroup.Components;

namespace PWS.Common.UI
{
    public class SubstageButtonUIProcessing<T> : IEcsRunSystem where T : struct
    {
        // auto injected fields
        readonly EcsFilter<StateExit, T> _stateExit;
        readonly EcsFilter<T> _state;
        readonly EcsFilter<SubstageProgress, SubstageButton> _substage;

        public void Run()
        {
            if (_state.IsEmpty()) return;

            foreach (var i in _substage)
            {
                if (_stateExit.IsEmpty())
                {
                    _substage.Get2(i).View.Toggle(_substage.Get1(i).CanBeSkipped);
                }
                else
                {
                    _substage.Get2(i).View.Toggle(false);
                }
            }
        }
    }
}
