﻿using Leopotam.Ecs;
using Modules.StateGroup.Components;
namespace PWS.Common.UI
{
    public class SkipStageButtonUIProcessing<StateT> : IEcsRunSystem where StateT : struct
    {
        // auto injected fields
        readonly EcsFilter<StateExit, StateT> _exiting;
        readonly EcsFilter<StateEnter, StateT> _entering;
        readonly EcsFilter<StateT> _inState;

        readonly EcsFilter<StateProgress> _stateProgress;
        readonly EcsFilter<SkipStageButton>.Exclude<Ignore> _skipStageButton;
        readonly EcsFilter<SkipStageButton,Ignore> _skipStageButtonIgnored;

        public void Run()
        {
            if (_inState.IsEmpty()) return;

            foreach (var j in _skipStageButton)
            {
                foreach (var i in _stateProgress)
                {
                    if (!_inState.IsEmpty())
                    {
                        bool show = _stateProgress.Get1(i).Value > _stateProgress.Get1(i).SkipThreshold;
                        show &= _stateProgress.Get1(i).Value <= _stateProgress.Get1(i).CompletionThreshold;

                        if (show)
                            _skipStageButton.Get1(j).View.Show();
                        else
                            _skipStageButton.Get1(j).View.Hide();
                    }
                }

                if (!_exiting.IsEmpty())
                {
                    _skipStageButton.Get1(j).View.Hide();
                }
            }

            if (!_entering.IsEmpty() || !_exiting.IsEmpty())
            {
                foreach (var i in _skipStageButtonIgnored)
                {
                    _skipStageButton.GetEntity(i).Del<Ignore>();
                }
            }
        }
    }
}