using Leopotam.Ecs;
using Modules.ViewHub;
using UnityEngine;
using UnityEngine.UI;

namespace PWS.Common.UI
{
    public class SkipStageButtonView : ViewComponent
    {
        [SerializeField] private Button _button;
        [SerializeField] private GameObject _showHideTarget;

        private EcsEntity _entity;
        
        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            _entity = ecsEntity;
            ecsEntity.Get<SkipStageButton>().View = this;
            _button.onClick.AddListener(OnButtonClick);

            Hide();
        }

        public virtual void Show()
        {
            if(_showHideTarget.activeSelf)
                return;
            
            _showHideTarget.gameObject.SetActive(true);
        }

        public virtual void Hide()
        {
            if(!_showHideTarget.activeSelf)
                return;
            
            _showHideTarget.gameObject.SetActive(false);
        }

        public void OnButtonClick()
        {
            _entity.Get<PWS.Common.Messages.SkipStageMessage>();
        }
    }
}