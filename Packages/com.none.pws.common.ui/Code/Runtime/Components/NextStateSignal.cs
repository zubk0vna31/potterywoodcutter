using Leopotam.Ecs;
using UnityEngine.UI;

namespace PWS.Common.UI
{
    public struct NextStateSignal 
    {
        public Button button;
    }
}
