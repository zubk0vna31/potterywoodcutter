
using UnityEngine;
using UnityEngine.UI;

namespace PWS.Common.UI
{
    public struct CompleteStateSignal 
    {
        public Button button;
    }
}
