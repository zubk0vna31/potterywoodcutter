using Leopotam.Ecs;
using Modules.ViewHub;
using UnityEngine;
using UnityEngine.UI;

namespace PWS.Common.UI
{
    public class NextSubStateButtonView : ViewComponent
    {
        [SerializeField] private Button _button;

        private EcsEntity _entity;
        
        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            _entity = ecsEntity;
            _button.onClick.AddListener(OnButtonClick);
        }
        
        public void OnButtonClick()
        {
            _entity.Get<NextSubStateSignal>();
        }
    }
}