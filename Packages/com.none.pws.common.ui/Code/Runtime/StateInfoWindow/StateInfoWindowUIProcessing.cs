using Leopotam.Ecs;
using Modules.StateGroup.Components;

namespace PWS.Common.UI
{
    public class StateInfoWindowUIProcessing<StateT> : IEcsRunSystem where StateT : struct
    {
        // auto injected fields
        readonly EcsFilter<StateEnter, StateT> _entering;
        readonly EcsFilter<StateExit, StateT> _exiting;

        readonly EcsFilter<StateInfoWindow> _view;

        private StateInfoData _data;

        public StateInfoWindowUIProcessing(StateInfoData data)
        {
            _data = data;
        }

        public void Run()
        {
            foreach (var i in _view)
            {
                if (!_entering.IsEmpty())
                {
                    _view.Get1(i).View.SetData(_data);
                    _view.Get1(i).View.Show(_data.AndSecondary);
                }

                if (!_exiting.IsEmpty())
                {
                    _view.Get1(i).View.Hide();
                }
            }
        }
    }
}
