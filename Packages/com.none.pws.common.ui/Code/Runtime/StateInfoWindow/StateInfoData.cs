using UnityEngine;

namespace PWS.Common.UI
{
    [CreateAssetMenu(fileName = "StateInfoData", menuName = "PWS/Stages/StateInfoWindow/Data", order = 0)]
    public class StateInfoData : ScriptableObject
    {
        [Header("Localize Terms")]
        public string HeaderTerm;
        public string MainTextTerm;
        public string BottomTerm;

        [Space]
        public bool AndSecondary;


        [Header("Deprecated Parameters")]
        public string Header;
        [TextArea]
        public string MainText;
        public string Bottom;
    }
}
