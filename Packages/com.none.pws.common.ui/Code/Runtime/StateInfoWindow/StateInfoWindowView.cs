using I2.Loc;
using Leopotam.Ecs;
using Modules.Root.ContainerComponentModel;
using Modules.ViewHub;
using PWS.Common.GameModeInfoService;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace PWS.Common.UI
{
    public class StateInfoWindowView : ViewComponent
    {
        [Inject] IGameModeInfoService _gameModeData;

        [SerializeField] private GameObject _showHideTarget;

        [SerializeField] private TMP_Text _header;
        [SerializeField] private TMP_Text _mainText;
        [SerializeField] private TMP_Text _bottom;
        [SerializeField] private Toggle _toggle;
        [SerializeField] private bool _secondary;

        [SerializeField] private Localize _headerLocalize;
        [SerializeField] private Localize _mainTextLocalize;
        [SerializeField] private Localize _bottomLocalize;

        private EcsEntity _entity;
        private GameMode _gameMode;

        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            _entity = ecsEntity;
            ecsEntity.Get<StateInfoWindow>().View = this;
            Hide();

            SceneContainer.Instance.Inject(this);
            _gameMode = _gameModeData.CurrentMode;
        }

        public virtual void Show(bool andSecondary)
        {
            if (_secondary && !andSecondary)
                return;

            if (_gameMode == GameMode.Free)
                return;

            if (_showHideTarget.activeSelf)
                return;

            StartCoroutine(UpdateCanvas());
        }

        IEnumerator UpdateCanvas()
        {
            _showHideTarget.gameObject.SetActive(true);
            yield return null;
            _showHideTarget.gameObject.SetActive(false);
            _showHideTarget.gameObject.SetActive(true);
        }

        public virtual void Hide()
        {
            if (_gameMode == GameMode.Free)
                return;

            if (!_showHideTarget.activeSelf)
                return;

            _showHideTarget.gameObject.SetActive(false);
        }

        public void SetData(StateInfoData data)
        {
            /*_header.text = data.Header;
            _mainText.text = data.MainText;
            _bottom.text = data.Bottom;*/
            _headerLocalize.SetTerm(data.HeaderTerm);
            _mainTextLocalize.SetTerm(data.MainTextTerm);
            _bottomLocalize.SetTerm(data.BottomTerm);
        }

        public void SetToggle(bool value)
        {
            _toggle.isOn = value;
        }
    }
}
