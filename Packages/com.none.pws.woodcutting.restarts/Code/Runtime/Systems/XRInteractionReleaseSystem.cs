﻿using Leopotam.Ecs;
using PWS.WoodCutting.Common;

namespace PWS.WoodCutting.Restarts
{
    public class XRInteractionReleaseSystem<SignalT> : IEcsRunSystem where SignalT: struct
    {
        private readonly EcsFilter<SignalT> _signal;
        private readonly EcsFilter<XRInteractable> _interactables;
        private readonly EcsFilter<XRInteractor> _interactors;

        public void Run()
        {
            if (_signal.IsEmpty()) return;

            foreach (var i in _interactors)
            {
                _interactors.Get1(i).interactor.ForceDeselect();
            }
            //foreach (var i in _interactables)
            //{
            //    _interactables.Get1(i).interactable.ForceDeselect();
            //}

        }
    }
}
