using Leopotam.Ecs;
using Modules.StateGroup.Components;
using Modules.StateGroup.Core;

namespace PWS.WoodCutting.Restarts.Systems
{
    public class StateGroupRestartHandler<RestoreSignalT> : IEcsRunSystem where RestoreSignalT:struct
    {
        // auto injected fields
        private readonly EcsFilter<SimulationState>.Exclude<StateExit> _state;
        private readonly EcsFilter<RestoreSignalT> _restoreSignal;
        private readonly StateFactory _stateFactory;

        public void Run()
        {
            if(_restoreSignal.IsEmpty())
                return;


            foreach (var i in _state)
            {
                _stateFactory.StateEntity = _state.GetEntity(i);
            }
        }
    }
}