using Leopotam.Ecs;
using PWS.Common.WorldRollbacking;
using UnityEngine;

namespace PWS.WoodCutting.Restarts.Systems
{
    public class RestartsProcessingSystem<DumpSignalT, RestoreSignalT> : IEcsRunSystem where DumpSignalT:struct where RestoreSignalT:struct
    {
        // auto injected fields
        private readonly EcsFilter<DumpSignalT> _dumpSignal;
        private readonly EcsFilter<RestoreSignalT> _restoreSignal;
        private EcsWorld _world;

        private WorldDumper _dumper;
        private bool _listenForDebugKeys;
        private KeyCode _saveDumpDebuKey;
        private KeyCode _restoreFromDumpDebugKey;

        public RestartsProcessingSystem(WorldDumper dumper)
        {
            _dumper = dumper;
            _listenForDebugKeys = false;
        }

        public RestartsProcessingSystem(WorldDumper dumper, bool listenForDebugKeys, KeyCode saveDumpDebuKey, KeyCode restoreFromDumpDebugKey)
        {
            _dumper = dumper;
            _listenForDebugKeys = listenForDebugKeys;
            _saveDumpDebuKey = saveDumpDebuKey;
            _restoreFromDumpDebugKey = restoreFromDumpDebugKey;
        }

        public void Run()
        {
            if (!_restoreSignal.IsEmpty() || (_listenForDebugKeys && UnityEngine.Input.GetKeyDown(_restoreFromDumpDebugKey)))
            {
                RestoreSignalT signalSave = new RestoreSignalT();
                foreach (var i in _restoreSignal)
                {
                    signalSave = _restoreSignal.Get1(i);
                }
                
                _dumper.RestoreFromDump();
                
                // recreate restore signal, because it may be deleted during rollback
                _world.NewEntity().Get<RestoreSignalT>() = signalSave;

            }else if (!_dumpSignal.IsEmpty() || (_listenForDebugKeys && UnityEngine.Input.GetKeyDown(_saveDumpDebuKey)))
            {
                _dumper.SaveDump();
            }
        }
    }
}