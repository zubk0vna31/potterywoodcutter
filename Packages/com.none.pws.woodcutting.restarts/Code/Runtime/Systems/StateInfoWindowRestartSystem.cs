﻿using Leopotam.Ecs;
using PWS.Common.UI;

namespace PWS.WoodCutting.Restarts
{
    public class StateInfoWindowRestartSystem<T> : IEcsRunSystem where T :struct
    {
        private readonly EcsFilter<T> _signal;
        private readonly EcsFilter<StateWindow> _stateWindow;


        public void Run()
        {
            if (_signal.IsEmpty()) return;

            foreach (var i in _stateWindow)
            {
                _stateWindow.Get1(i).Template.ShowSlider(false);
                _stateWindow.Get1(i).Template.SetSliderValue(0);
                _stateWindow.Get1(i).Template.SetSliderLabel(0);

                _stateWindow.Get1(i).Template.HideButtons();
            }
        }
    }
}
