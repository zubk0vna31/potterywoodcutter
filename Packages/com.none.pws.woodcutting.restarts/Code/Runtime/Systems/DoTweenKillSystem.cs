using Leopotam.Ecs;
using DG.Tweening;

namespace PWS.WoodCutting.Restarts
{
    public class DoTweenKillSystem<Signal> : IEcsRunSystem where Signal : struct
    {
        private readonly EcsFilter<Signal> _signal;

        public void Run()
        {
            if (_signal.IsEmpty()) return;

            DOTween.KillAll();
        }
    }
}
