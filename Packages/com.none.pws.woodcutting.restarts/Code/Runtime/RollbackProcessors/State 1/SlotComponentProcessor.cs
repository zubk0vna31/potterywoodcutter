using Leopotam.Ecs;
using PWS.Common.WorldRollbacking;
using UnityEngine;

namespace PWS.WoodCutting.Restarts
{
    struct SlotComponentRollbackData
    {
        public bool colliderEnabled;
        public Transform target;
        public Color meshRendererColor;
        public bool previewEnabled, wrongEnabled;
        public bool meshRenderEnabled;
    }

    public class SlotComponentProcessor : DefaultRollbackProcessor<SlotComponent>
    {
        public override void OnSaveDump(EcsEntity currentEntity, EcsEntity dumpEntity)
        {
            base.OnSaveDump(currentEntity, dumpEntity);

            if (!currentEntity.Has<SlotComponent>()) return;

            ref var data = ref dumpEntity.Get<SlotComponentRollbackData>();
            ref var currentData = ref currentEntity.Get<SlotComponent>();

            data.colliderEnabled = currentData.collider.enabled;
            data.target = currentData.target;
            data.meshRendererColor = currentData.meshRenderer.material.GetColor("_TintColor");
            data.meshRenderEnabled = currentData.meshRenderer.enabled;
            data.previewEnabled = currentData.preview.gameObject.activeSelf;
            data.wrongEnabled = currentData.wrong.gameObject.activeSelf;
        }

        public override void OnRestoreFromDump(EcsEntity currentEntity, EcsEntity dumpEntity)
        {
            base.OnRestoreFromDump(currentEntity, dumpEntity);

            if (!dumpEntity.Has<SlotComponentRollbackData>() || !currentEntity.Has<SlotComponent>()) return;

            ref var data = ref dumpEntity.Get<SlotComponentRollbackData>();
            ref var currentData = ref currentEntity.Get<SlotComponent>();

            currentData.collider.enabled = data.colliderEnabled;
            currentData.meshRenderer.material.SetColor("_TintColor", data.meshRendererColor);
            currentData.target = data.target;
            currentData.meshRenderer.enabled = data.meshRenderEnabled;
            currentData.preview.gameObject.SetActive(data.previewEnabled);
            currentData.wrong.gameObject.SetActive(data.wrongEnabled);
        }
    }
}
