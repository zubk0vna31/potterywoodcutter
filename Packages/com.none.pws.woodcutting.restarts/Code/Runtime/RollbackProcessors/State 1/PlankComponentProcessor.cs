using Leopotam.Ecs;
using PWS.Common.WorldRollbacking;

namespace PWS.WoodCutting.Restarts
{
    struct PlankDumpRollbackData
    {
        public bool colliderEnabled;
        public bool triggerColliderEnabled;
        public bool rigidBodyIsKinematic;
        public bool grabbableEnabled;
    }

    public class PlankComponentProcessor : DefaultRollbackProcessor<PlankComponent>
    {
        public override void OnSaveDump(EcsEntity currentEntity, EcsEntity dumpEntity)
        {
            base.OnSaveDump(currentEntity, dumpEntity);

            if (!currentEntity.Has<PlankComponent>()) return;

            ref var data = ref dumpEntity.Get<PlankDumpRollbackData>();
            ref var currentData = ref currentEntity.Get<PlankComponent>();

            data.colliderEnabled = currentData.collider.enabled;
            data.triggerColliderEnabled = currentData.triggerCollider.enabled;
            data.rigidBodyIsKinematic = currentData.rigidbody.isKinematic;
            data.grabbableEnabled = currentData.grabbable.enabled;
        }
        public override void OnRestoreFromDump(EcsEntity currentEntity, EcsEntity dumpEntity)
        {
            base.OnRestoreFromDump(currentEntity, dumpEntity);

            if (!dumpEntity.Has<PlankDumpRollbackData>() || !currentEntity.Has<PlankComponent>()) return;

            ref var data = ref dumpEntity.Get<PlankDumpRollbackData>();
            ref var currentData = ref currentEntity.Get<PlankComponent>();

            currentData.collider.enabled = data.colliderEnabled;
            currentData.triggerCollider.enabled = data.triggerColliderEnabled;
            currentData.rigidbody.isKinematic = data.rigidBodyIsKinematic;
            currentData.grabbable.enabled = data.grabbableEnabled;
        }
    }
}
