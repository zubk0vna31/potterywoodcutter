﻿using Leopotam.Ecs;
using PWS.Common.WorldRollbacking;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace PWS.WoodCutting.Restarts
{
    struct HolesDrillingRollbackData
    {
        public Dictionary<Transform, RigidbodyData> drillDataMap;
    }


    public class HolesDrillingRollbackProcessor : DefaultRollbackProcessor<PlankFinal>
    {
        public override void OnSaveDump(EcsEntity currentEntity, EcsEntity dumpEntity)
        {
            base.OnSaveDump(currentEntity, dumpEntity);

            if (!currentEntity.Has<PlankFinal>()) return;

            ref var data = ref dumpEntity.Get<HolesDrillingRollbackData>();
            ref var currentData = ref currentEntity.Get<PlankFinal>();

            var drillObjects = currentData.view.State7.GetComponentsInChildren<DrillObject>();

            data.drillDataMap = drillObjects.ToDictionary(x => x.transform, y => y.GetDrillData());
        }

        public override void OnRestoreFromDump(EcsEntity currentEntity, EcsEntity dumpEntity)
        {
            base.OnRestoreFromDump(currentEntity, dumpEntity);

            if (!dumpEntity.Has<HolesDrillingRollbackData>() || !currentEntity.Has<PlankFinal>()) return;

            ref var data = ref dumpEntity.Get<HolesDrillingRollbackData>();
            ref var currentData = ref currentEntity.Get<PlankFinal>();

            var drillObjects = currentData.view.State7.GetComponentsInChildren<DrillObject>();

            foreach (var dr in drillObjects)
            {
                if (data.drillDataMap.ContainsKey(dr.transform))
                {
                    dr.ResetComponent(data.drillDataMap[dr.transform]);
                }
            }
        }
    }
}


