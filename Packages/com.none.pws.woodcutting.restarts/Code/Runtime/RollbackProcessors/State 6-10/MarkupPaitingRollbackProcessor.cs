﻿using Leopotam.Ecs;
using PWS.Common.WorldRollbacking;
using UnityEngine;

namespace PWS.WoodCutting.Restarts
{
    struct MarkupPaintingRollbackData
    {

    }


    public class MarkupPaitingRollbackProcessor : DefaultRollbackProcessor<PlankFinal>
    {
        public override void OnSaveDump(EcsEntity currentEntity, EcsEntity dumpEntity)
        {
            base.OnSaveDump(currentEntity, dumpEntity);

            if (!currentEntity.Has<PlankFinal>()) return;

            ref var data = ref dumpEntity.Get<MarkupPaintingRollbackData>();
            ref var currentData = ref currentEntity.Get<PlankFinal>();
        }

        public override void OnRestoreFromDump(EcsEntity currentEntity, EcsEntity dumpEntity)
        {
            base.OnRestoreFromDump(currentEntity, dumpEntity);

            if (!dumpEntity.Has<MarkupPaintingRollbackData>() || !currentEntity.Has<PlankFinal>()) return;

            ref var data = ref dumpEntity.Get<MarkupPaintingRollbackData>();
            ref var currentData = ref currentEntity.Get<PlankFinal>();

            currentData.markupPaintingPattern.ResetComponent();
        }
    }
}
