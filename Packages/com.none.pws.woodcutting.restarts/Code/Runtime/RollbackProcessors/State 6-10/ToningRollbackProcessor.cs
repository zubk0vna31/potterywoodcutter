﻿using Leopotam.Ecs;
using PWS.Common.WorldRollbacking;
using PWS.WoodCutting.Common;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace PWS.WoodCutting.Restarts
{

    public struct ToningRollbackData
    {
        public Queue<ToningSubstateData> editorQueueData;
        public Quaternion rot;
    }

    public class ToningRollbackProcessor : DefaultRollbackProcessor<PlankFinal>
    {
        public override void OnSaveDump(EcsEntity currentEntity, EcsEntity dumpEntity)
        {
            base.OnSaveDump(currentEntity, dumpEntity);

            if (!currentEntity.Has<PlankFinal>()) return;

            ref var data = ref dumpEntity.Get<ToningRollbackData>();
            ref var currentData = ref currentEntity.Get<PlankFinal>();

            data.editorQueueData = new Queue<ToningSubstateData>();

            foreach (var i in currentData.queue)
            {
                data.editorQueueData.Enqueue(i);
            }

            data.rot = currentData.view.ChamferedFinalObject.rotation;
        }

        public override void OnRestoreFromDump(EcsEntity currentEntity, EcsEntity dumpEntity)
        {
            base.OnRestoreFromDump(currentEntity, dumpEntity);

            if (!dumpEntity.Has<ToningRollbackData>() || !currentEntity.Has<PlankFinal>()) return;

            ref var data = ref dumpEntity.Get<ToningRollbackData>();
            ref var currentData = ref currentEntity.Get<PlankFinal>();

            currentData.view.ChamferedFinalObject.rotation = data.rot;

            currentData.queue = new Queue<ToningSubstateData>();

            currentData.view.StickyObject.Toggle(false);

            var renderers = new List<Renderer>();
            foreach (var i in data.editorQueueData)
            {
                currentData.queue.Enqueue(i);
                renderers.Add(i.renderer);
            }

            currentData.toningPaintable.ResetComponent(renderers);
        }
    }
}
