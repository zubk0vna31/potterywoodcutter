﻿using Leopotam.Ecs;
using PWS.Common.WorldRollbacking;
using PWS.WoodCutting.Common;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace PWS.WoodCutting.Restarts
{
     struct ChamferingRollbackData
    {
        public VertexPathData[] sawData;
        public Dictionary<Transform, RigidbodyData> triggerZoneMap;
    }

    public class ChamferingRollbackProcessor : DefaultRollbackProcessor<PlankFinal>
    {
        public override void OnSaveDump(EcsEntity currentEntity, EcsEntity dumpEntity)
        {
            base.OnSaveDump(currentEntity, dumpEntity);

            if (!currentEntity.Has<PlankFinal>()) return;

            ref var data = ref dumpEntity.Get<ChamferingRollbackData>();
            ref var currentData = ref currentEntity.Get<PlankFinal>();

            data.sawData = currentData.chamferingPattern.GetSawData();
            data.triggerZoneMap = currentData.chamferingPattern.GetTriggerZones().ToDictionary(
                x => x, y => GetData(y));
        }

        public override void OnRestoreFromDump(EcsEntity currentEntity, EcsEntity dumpEntity)
        {
            base.OnRestoreFromDump(currentEntity, dumpEntity);

            if (!dumpEntity.Has<ChamferingRollbackData>() || !currentEntity.Has<PlankFinal>()) return;

            ref var data = ref dumpEntity.Get<ChamferingRollbackData>();
            ref var currentData = ref currentEntity.Get<PlankFinal>();

            currentData.chamferingPattern.ResetComponent(data.sawData);
            currentData.chamferPaintable.ResetComponent();

            foreach (var key in data.triggerZoneMap.Keys)
            {
                var rbData = data.triggerZoneMap[key];

                key.position = rbData.pos;
                key.rotation = rbData.rot;
                key.GetComponent<Collider>().enabled = rbData.colliderEnabled;
                key.GetComponent<Rigidbody>().isKinematic = rbData.isKinematic;
                key.gameObject.SetActive(true);
            }
        }

        private RigidbodyData GetData(Transform target)
        {
            return new RigidbodyData()
            {
                pos = target.position,
                rot = target.rotation,
                colliderEnabled = target.GetComponentInChildren<Collider>().enabled,
                isKinematic = target.GetComponentInChildren<Rigidbody>().isKinematic
            };
        }
    }
}
