using Leopotam.Ecs;
using PWS.Common.WorldRollbacking;
using UnityEngine;

namespace PWS.WoodCutting.Restarts
{
    struct PlankGluedRollbackData
    {
        
    }

    public class PlankGluedProcessor : DefaultRollbackProcessor<PlankGlued>
    {
        public override void OnSaveDump(EcsEntity currentEntity, EcsEntity dumpEntity)
        {
            base.OnSaveDump(currentEntity, dumpEntity);

            if (!currentEntity.Has<PlankGlued>()) return;

            ref var data = ref dumpEntity.Get<PlankGluedRollbackData>();
            ref var currentData = ref currentEntity.Get<PlankGlued>();
        }

        public override void OnRestoreFromDump(EcsEntity currentEntity, EcsEntity dumpEntity)
        {
            base.OnRestoreFromDump(currentEntity, dumpEntity);

            if (!dumpEntity.Has<PlankGluedRollbackData>() || !currentEntity.Has<PlankGlued>()) return;

            ref var data = ref dumpEntity.Get<PlankGluedRollbackData>();
            ref var currentData = ref currentEntity.Get<PlankGlued>();


            var transform = currentData.glueHolder;

            GameObject[] delete = new GameObject[transform.childCount];

            for (int i = 0; i < delete.Length; i++)
            {
                delete[i] = transform.GetChild(i).gameObject;
            }

            for (int i = 0; i < delete.Length; i++)
            {
                UnityEngine.GameObject.DestroyImmediate(delete[i]);
            }
        }
    }
}
