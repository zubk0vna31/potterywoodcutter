using Leopotam.Ecs;
using PWS.Common.WorldRollbacking;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PWS.WoodCutting.Restarts
{
    struct PlankDrawSideRollbackData
    {
        public bool colliderEnabled;
    }

    public class PlankDrawSideProcessor : DefaultRollbackProcessor<PlankDrawSide>
    {
        public override void OnSaveDump(EcsEntity currentEntity, EcsEntity dumpEntity)
        {
            base.OnSaveDump(currentEntity, dumpEntity);

            if(!currentEntity.Has<PlankDrawSide>()) return;

            ref var data = ref dumpEntity.Get<PlankDrawSideRollbackData>();
            ref var currentData = ref currentEntity.Get<PlankDrawSide>();

            data.colliderEnabled = currentData.collider.enabled;
        }

        public override void OnRestoreFromDump(EcsEntity currentEntity, EcsEntity dumpEntity)
        {
            base.OnRestoreFromDump(currentEntity, dumpEntity);

            if (!dumpEntity.Has<PlankDrawSideRollbackData>() || !currentEntity.Has<PlankDrawSide>()) return;

            ref var data = ref dumpEntity.Get<PlankDrawSideRollbackData>();
            ref var currentData = ref currentEntity.Get<PlankDrawSide>();

            currentData.collider.enabled = data.colliderEnabled;

            currentData.ResetUnityComponents();
        }
    }
}
