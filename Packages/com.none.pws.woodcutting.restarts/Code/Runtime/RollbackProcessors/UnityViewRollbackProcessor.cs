using Leopotam.Ecs;
using Modules.ViewHub;
using PWS.Common.WorldRollbacking;
using UnityEngine;

namespace PWS.WoodCutting.Restarts.RoolbackProcessors
{
    public struct DumpTransformSaveData
    {
        public Vector3 LocalPosition;
        public Quaternion LocalRotation;
        public Vector3 LocalScale;
        public bool GameObjectActive;

        public Transform Parent;
    }
    
    public class UnityViewRollbackProcessor : IDumpRollbackProcessor
    {
        public void OnSaveDump(EcsEntity currentEntity, EcsEntity dumpEntity)
        {
            if(!currentEntity.Has<UnityView>())
                return;
            
            dumpEntity.Get<UnityView>() = currentEntity.Get<UnityView>();
            
            ref var view = ref dumpEntity.Get<UnityView>();
            ref var saveData = ref dumpEntity.Get<DumpTransformSaveData>();

            saveData.Parent = view.Transform.parent;
            saveData.LocalPosition = view.Transform.localPosition;
            saveData.LocalRotation = view.Transform.localRotation;
            saveData.LocalScale = view.Transform.localScale;
            saveData.GameObjectActive = view.GameObject.activeSelf;
        }

        public void OnRestoreFromDump(EcsEntity currentEntity, EcsEntity dumpEntity)
        {
            if (dumpEntity.Has<UnityView>())
            {
                if (dumpEntity.Get<UnityView>().GameObject == null)
                {
                    UnityEngine.Debug.LogError("Unimplemented case: UnityView object was destroyed");
                    return;
                }
                
                currentEntity.Get<UnityView>() = dumpEntity.Get<UnityView>();

                if (dumpEntity.Has<DumpTransformSaveData>())
                {
                    ref var view = ref currentEntity.Get<UnityView>();
                    ref var saveData = ref dumpEntity.Get<DumpTransformSaveData>();

                    view.Transform.SetParent(saveData.Parent);
                    view.Transform.localPosition = saveData.LocalPosition;
                    view.Transform.localRotation = saveData.LocalRotation;
                    view.Transform.localScale = saveData.LocalScale;
                    view.GameObject.SetActive(saveData.GameObjectActive);
                }
            }
            else if (currentEntity.Has<UnityView>())
            {
                currentEntity.Del<UnityView>();
            }
        }
    }
}