using Leopotam.Ecs;
using PWS.Common.WorldRollbacking;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PWS.WoodCutting.Restarts
{
    struct GlueToRemoveRollbackData
    {

    }

    public class GlueToRemoveProcessor : DefaultRollbackProcessor<GlueToRemove>
    {
        public override void OnSaveDump(EcsEntity currentEntity, EcsEntity dumpEntity)
        {
            base.OnSaveDump(currentEntity, dumpEntity);

            if (!currentEntity.Has<GlueToRemove>()) return;

            ref var data = ref dumpEntity.Get<GlueToRemoveRollbackData>();
            ref var currentData = ref currentEntity.Get<GlueToRemove>();
        }

        public override void OnRestoreFromDump(EcsEntity currentEntity, EcsEntity dumpEntity)
        {
            base.OnRestoreFromDump(currentEntity, dumpEntity);

            if (!dumpEntity.Has<GlueToRemoveRollbackData>() || !currentEntity.Has<GlueToRemove>()) return;

            ref var data = ref dumpEntity.Get<GlueToRemoveRollbackData>();
            ref var currentData = ref currentEntity.Get<GlueToRemove>();

            currentData.ResetUnityComponents();
        }
    }
}
