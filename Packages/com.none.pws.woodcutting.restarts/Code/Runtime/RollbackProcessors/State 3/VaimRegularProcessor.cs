﻿using Leopotam.Ecs;
using PWS.Common.WorldRollbacking;
using UnityEngine;

namespace PWS.WoodCutting.Restarts
{
    struct VaimRegularRollbackData
    {
        public bool isKinematic;
        public bool isGrabbable;
        public bool isCircularGrabbable;
        public bool isAxisMoveable;
    }

    public class VaimRegularProcessor : DefaultRollbackProcessor<VaimRegular>
    {
        public override void OnSaveDump(EcsEntity currentEntity, EcsEntity dumpEntity)
        {
            base.OnSaveDump(currentEntity, dumpEntity);

            if (!currentEntity.Has<VaimRegular>()) return;

            ref var data = ref dumpEntity.Get<VaimRegularRollbackData>();
            ref var currentData = ref currentEntity.Get<VaimRegular>();

            data.isKinematic = currentData.rigidbody.isKinematic;
            data.isGrabbable = currentData.grabInteractable.enabled;
            data.isCircularGrabbable = currentData.circularGrabbable.enabled;
            data.isAxisMoveable = currentData.axisMover.enabled;
        }

        public override void OnRestoreFromDump(EcsEntity currentEntity, EcsEntity dumpEntity)
        {
            base.OnRestoreFromDump(currentEntity, dumpEntity);

            if (!dumpEntity.Has<VaimRegularRollbackData>() || !currentEntity.Has<VaimRegular>()) return;

            ref var data = ref dumpEntity.Get<VaimRegularRollbackData>();
            ref var currentData = ref currentEntity.Get<VaimRegular>();

            Debug.Log(data.isGrabbable);

            currentData.rigidbody.isKinematic = data.isKinematic;
            currentData.grabInteractable.enabled = data.isGrabbable;
            currentData.circularGrabbable.enabled = data.isCircularGrabbable;
            currentData.axisMover.enabled = data.isAxisMoveable;

            currentData.ResetUnityComponents();
        }
    }
}