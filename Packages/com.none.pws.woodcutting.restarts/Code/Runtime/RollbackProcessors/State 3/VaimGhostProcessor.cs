using Leopotam.Ecs;
using PWS.Common.WorldRollbacking;
using UnityEngine;

namespace PWS.WoodCutting.Restarts
{
    struct VaimGhostRollbackData
    {
        public Color defaultColor;
        public bool colliderEnabled;
    }

    public class VaimGhostProcessor : DefaultRollbackProcessor<VaimGhost>
    {
        public override void OnSaveDump(EcsEntity currentEntity, EcsEntity dumpEntity)
        {
            base.OnSaveDump(currentEntity, dumpEntity);

            if (!currentEntity.Has<VaimGhost>()) return;

            ref var data = ref dumpEntity.Get<VaimGhostRollbackData>();
            ref var currentData = ref currentEntity.Get<VaimGhost>();

            data.defaultColor = currentData.meshRenderer[0].material.GetColor("_Color");
            data.colliderEnabled = currentData.collider.enabled;
        }
        public override void OnRestoreFromDump(EcsEntity currentEntity, EcsEntity dumpEntity)
        {
            base.OnRestoreFromDump(currentEntity, dumpEntity);

            if (!dumpEntity.Has<VaimGhostRollbackData>() || !currentEntity.Has<VaimGhost>()) return;

            ref var data = ref dumpEntity.Get<VaimGhostRollbackData>();
            ref var currentData = ref currentEntity.Get<VaimGhost>();

            currentData.collider.enabled = data.colliderEnabled;

            foreach (var mr in currentData.meshRenderer)
            {
                mr.material.SetColor("_Color",data.defaultColor);
            }

            currentData.animator.Rebind();
            currentData.animator.SetTrigger("Ghost");

        }

    }
}
