using Leopotam.Ecs;
using PWS.Common.WorldRollbacking;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PWS.WoodCutting.Restarts
{
    struct PlankPaintableRollbackData
    {

    }

    public class PlankPaintableProcessor : DefaultRollbackProcessor<PlankPaintable>
    {
        public override void OnSaveDump(EcsEntity currentEntity, EcsEntity dumpEntity)
        {
            base.OnSaveDump(currentEntity, dumpEntity);

            if (!currentEntity.Has<PlankPaintable>()) return;

            ref var data = ref dumpEntity.Get<PlankPaintableRollbackData>();
            ref var currentData = ref currentEntity.Get<PlankPaintable>();
        }

        public override void OnRestoreFromDump(EcsEntity currentEntity, EcsEntity dumpEntity)
        {
            base.OnRestoreFromDump(currentEntity, dumpEntity);

            if (!dumpEntity.Has<PlankPaintableRollbackData>() || !currentEntity.Has<PlankPaintable>()) return;

            ref var data = ref dumpEntity.Get<PlankPaintableRollbackData>();
            ref var currentData = ref currentEntity.Get<PlankPaintable>();

            currentData.ResetUnityComponents();
        }

    }
}
