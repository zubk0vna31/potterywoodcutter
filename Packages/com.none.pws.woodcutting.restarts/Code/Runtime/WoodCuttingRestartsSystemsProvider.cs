using UnityEngine;
using Leopotam.Ecs;
using Modules.Root.ECS;
using Modules.StateGroup.Components;
using PWS.Common.Messages;
using PWS.Common.UI;
using PWS.Common.WorldRollbacking;
using PWS.WoodCutting.Restarts.RoolbackProcessors;
using PWS.WoodCutting.Restarts.Systems;
using PWS.WoodCutting.Stages.GlueRemoval;
using PWS.WoodCutting.Stages.ItemPolishing;
using PWS.WoodCutting.Common;
using PWS.WoodCutting.MarkupPainting;

namespace PWS.WoodCutting.Restarts
{
    public class WoodCuttingRestartsSystemsProvider : MonoBehaviour, ISystemsProvider
    {
        [SerializeField] private bool _listenForDebugKeys = false;
        [SerializeField] private KeyCode _debugSaveDumpKeycode = KeyCode.A;
        [SerializeField] private KeyCode _debugRestoreFromDumpKeyCode = KeyCode.Z;


        public EcsSystems GetSystems(EcsWorld world, EcsSystems endFrame, EcsSystems mainSystems)
        {
            WorldDumper dumper = new WorldDumper(world);
            EcsSystems systems = new EcsSystems(world, "Restarts");

            systems
                .Add(new RestartsProcessingSystem<StateEnter, RestartCurrentStepMessage>(dumper, _listenForDebugKeys, _debugSaveDumpKeycode, _debugRestoreFromDumpKeyCode))
                .Add(new DoTweenKillSystem<RestartCurrentStepMessage>())
                .Add(new StateInfoWindowRestartSystem<RestartCurrentStepMessage>())
                .Add(new XRInteractionReleaseSystem<RestartCurrentStepMessage>())
                .Add(new StateGroupRestartHandler<RestartCurrentStepMessage>())
                ;

            dumper
                // Stategroup
                .AddProcessor(new DefaultRollbackProcessor<Modules.StateGroup.Components.SimulationState>())
                .AddProcessor(new DefaultRollbackProcessor<Modules.StateGroup.Components.StateEnter>())
                .AddProcessor(new DefaultRollbackProcessor<Modules.StateGroup.Components.StateExit>())

                // States
                .AddProcessor(new DefaultRollbackProcessor<PWS.WoodCutting.InitialPlanksPlacementState>())
                .AddProcessor(new DefaultRollbackProcessor<PWS.WoodCutting.GlueingState>())
                .AddProcessor(new DefaultRollbackProcessor<PWS.WoodCutting.ClampsPlacementState>())
                .AddProcessor(new DefaultRollbackProcessor<PWS.WoodCutting.GlueRemovalState>())
                .AddProcessor(new DefaultRollbackProcessor<PWS.WoodCutting.ItemPolishingState>())

                // Viewhub
                .AddProcessor(new UnityViewRollbackProcessor())

                // Common UI
                .AddProcessor(new DefaultRollbackProcessor<SkipStageButton>())
                .AddProcessor(new DefaultRollbackProcessor<StateInfoWindow>())
                .AddProcessor(new DefaultRollbackProcessor<StateProgress>())
                .AddProcessor(new DefaultRollbackProcessor<StateWindow>())
                .AddProcessor(new DefaultRollbackProcessor<StateWindowPlacement>())
                .AddProcessor(new DefaultRollbackProcessor<Ignore>())

                // Common
                .AddProcessor(new DefaultRollbackProcessor<PWS.WoodCutting.PlankComponent>())
                .AddProcessor(new DefaultRollbackProcessor<PWS.WoodCutting.PlankParent>())
                .AddProcessor(new DefaultRollbackProcessor<PWS.WoodCutting.Table>())
                .AddProcessor(new DefaultRollbackProcessor<PWS.WoodCutting.SelectEnter>())
                .AddProcessor(new DefaultRollbackProcessor<PWS.WoodCutting.SelectExit>())

                // Common: Custom
                .AddProcessor(new PlankComponentProcessor())

                // State 1 - Initial Planks Placement : Default
                .AddProcessor(new DefaultRollbackProcessor<PWS.WoodCutting.HoverSlot>())
                .AddProcessor(new DefaultRollbackProcessor<PWS.WoodCutting.Moveable>())
                .AddProcessor(new DefaultRollbackProcessor<PWS.WoodCutting.PlankCorrecting>())
                .AddProcessor(new DefaultRollbackProcessor<PWS.WoodCutting.PlankPlaced>())
                .AddProcessor(new DefaultRollbackProcessor<PWS.WoodCutting.PlankWrongPlacement>())
                .AddProcessor(new DefaultRollbackProcessor<PWS.WoodCutting.RemoveTargetTag>())
                .AddProcessor(new DefaultRollbackProcessor<PWS.WoodCutting.SlotComponent>())
                .AddProcessor(new DefaultRollbackProcessor<PWS.WoodCutting.WaitThenPerform>())

                // State 1 - Initial Planks Placement : Custom
                .AddProcessor(new SlotComponentProcessor())

                // State 2 - Glueing : Default
                .AddProcessor(new DefaultRollbackProcessor<Drawable>())
                .AddProcessor(new DefaultRollbackProcessor<GlueContainter>())
                .AddProcessor(new DefaultRollbackProcessor<GluePressed>())
                .AddProcessor(new DefaultRollbackProcessor<MovePlankToDrawPosition>())
                .AddProcessor(new DefaultRollbackProcessor<PlankDrawSide>())
                .AddProcessor(new DefaultRollbackProcessor<PlankGlued>())
                .AddProcessor(new DefaultRollbackProcessor<ResetPlankTransform>())
                .AddProcessor(new DefaultRollbackProcessor<RotatePlankToDrawablePart>())
                .AddProcessor(new DefaultRollbackProcessor<ShowRateVisual>())
                .AddProcessor(new DefaultRollbackProcessor<ShowRatingTag>())

                // State 2 - Glueing : Custom
                .AddProcessor(new PlankGluedProcessor())
                .AddProcessor(new PlankDrawSideProcessor())

                // State 3 - Clamps placement : Default
                .AddProcessor(new DefaultRollbackProcessor<NearGhost>())
                .AddProcessor(new DefaultRollbackProcessor<PlankMoveUp>())
                .AddProcessor(new DefaultRollbackProcessor<PlankParentRotate>())
                .AddProcessor(new DefaultRollbackProcessor<VaimGhost>())
                .AddProcessor(new DefaultRollbackProcessor<VaimGhostAnimate>())
                .AddProcessor(new DefaultRollbackProcessor<VaimGhostAppear>())
                .AddProcessor(new DefaultRollbackProcessor<VaimPlaced>())
                .AddProcessor(new DefaultRollbackProcessor<VaimRegular>())
                .AddProcessor(new DefaultRollbackProcessor<VaimRegularAnimate>())
                .AddProcessor(new DefaultRollbackProcessor<VaimTwisted>())

                // State 3 - Clamps placement : Custom
                .AddProcessor(new VaimRegularProcessor())
                .AddProcessor(new VaimGhostProcessor())

                // State 4 - Glue Removal : Default
                .AddProcessor(new DefaultRollbackProcessor<GluePusher>())
                .AddProcessor(new DefaultRollbackProcessor<GlueToRemove>())
                .AddProcessor(new DefaultRollbackProcessor<GlueSideToRemove>())
                .AddProcessor(new DefaultRollbackProcessor<RotatePlankToSideWithGlue>())

                // State 4 - Glue Removal : Custom
                .AddProcessor(new GlueToRemoveProcessor())

                // State 5 - Item Polishing : Default
                .AddProcessor(new DefaultRollbackProcessor<PlankPaintable>())

                // State 5 - Item Polishing : Custom
                .AddProcessor(new PlankPaintableProcessor())

                // State (6-10) - All : Default
                .AddProcessor(new DefaultRollbackProcessor<PlankFinal>())

                // State 6 - Markup Painting : Default
                .AddProcessor(new DefaultRollbackProcessor<PatternSelectMenu>())

                // State 6 - Markup Painting : Custom
                .AddProcessor(new MarkupPaitingRollbackProcessor())

                // State 7 - Holes Drilling : Default

                // State 7 - Holes Drilling : Custom
                .AddProcessor(new HolesDrillingRollbackProcessor())

                // State 8 - Sawing : Default

                // State 8 - Sawing : Custom
                .AddProcessor(new SawingRollbackProcessor())

                // State 9 - Chamfering : Default
                         
                // State 9 - Chamfering : Custom
                .AddProcessor(new ChamferingRollbackProcessor())

                // State 10 - Toning : Default

                // State 10 - Toning : Custom
                .AddProcessor(new ToningRollbackProcessor())


            // State N - Name : Default

            // State N - Name : Custom

            ;

            return systems;
        }
    }
}