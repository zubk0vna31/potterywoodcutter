using Modules.StateGroup.Core;
using UnityEngine;

namespace PWS.Pottery.Stages.HandlePlacingStage
{
    [CreateAssetMenu(fileName = "SetHandlePlacingStateSOCommand", menuName = "PWS/Pottery/Stages/HandlePlacingStage/SetStateSOCommand")]
    public class SetHandlePlacingStateSOCommand : SetStateSOCommand<HandlePlacingState>
    {

    }
}