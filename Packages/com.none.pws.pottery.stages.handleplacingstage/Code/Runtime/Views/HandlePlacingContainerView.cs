using Leopotam.Ecs;
using Modules.ViewHub;

namespace PWS.Pottery.Stages.HandlePlacingStage
{
    public class HandlePlacingContainerView : ViewComponent
    {
        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            ecsEntity.Get<HandlePlacingContainer>().View = this;
        }
    }
}
