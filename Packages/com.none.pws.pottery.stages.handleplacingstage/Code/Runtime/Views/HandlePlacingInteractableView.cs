using Leopotam.Ecs;
using Modules.ViewHub;
using UnityEngine;
using UnityEngine.UI;

namespace PWS.Pottery.Stages.HandlePlacingStage
{
    public class HandlePlacingInteractableView : ViewComponent
    {
        [SerializeField]
        private HandleTogglePair[] _handles;
        public Transform SelectedHandle;
        [HideInInspector] public Transform LeftHandModel;
        [HideInInspector] public Transform RightHandModel;

        private EcsEntity _ecsEntity;

        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            _ecsEntity = ecsEntity;
            ecsEntity.Get<HandlePlacingInteractable>().View = this;

            AddToggleListeners();
        }

        void AddToggleListeners()
        {
            foreach (HandleTogglePair handle in _handles)
            {
                handle.Handle.gameObject.SetActive(false);

                Transform tr = handle.Handle;
                handle.Toggle.onValueChanged.AddListener((bool on) =>
                {
                    if (on)
                    {
                        if (SelectedHandle)
                            SelectedHandle.gameObject.SetActive(false);

                        SelectedHandle = tr;
                        SelectedHandle.gameObject.SetActive(true);
                        LeftHandModel = handle.LeftHandModel;
                        RightHandModel = handle.RightHandModel;
                        _ecsEntity.Get<HandleSelectedSignal>();
                    }
                });
            }
        }
    }

    [System.Serializable]
    public class HandleTogglePair
    {
        public Transform Handle;
        public Toggle Toggle;
        public Transform LeftHandModel;
        public Transform RightHandModel;
    }
}
