using Leopotam.Ecs;
using Modules.StateGroup.Components;
using Modules.ViewHub;
using PWS.Pottery.ItemDataHolderService;
using PWS.Pottery.MeshGeneration;
using UnityEngine;

namespace PWS.Pottery.Stages.HandlePlacingStage
{
    public class HandlePlacingInteractablePositionProcessing : IEcsRunSystem
    {
        // auto injected fields
        readonly EcsFilter<StateEnter, HandlePlacingState> _entering;
        readonly EcsFilter<StateExit, HandlePlacingState> _exiting;
        readonly EcsFilter<HandlePlacingState> _inState;
        readonly EcsFilter<UnityView, HandlePlacingInteractable> _interactable;
        readonly EcsFilter<UnityView, SculptingProductTag> _product;
        readonly EcsFilter<HandleSelectedSignal> _selected;

        private ISculptingDataHolder _data;

        //runtime data
        private float _distance;
        private float _angle;

        private float _oldHeight;

        public HandlePlacingInteractablePositionProcessing(ISculptingDataHolder data)
        {
            _data = data;
        }

        public void Run()
        {
            if (!_inState.IsEmpty())
            {
                if (_data.Rings.Count < 2) return;

                foreach (var interactable in _interactable)
                {
                    /*if (_interactable.Get1(interactable).Transform.position.y == _oldHeight && _selected.IsEmpty())
                        return;

                    _oldHeight = _interactable.Get1(interactable).Transform.position.y;*/

                    if (_interactable.Get2(interactable).View.SelectedHandle == null)
                        return;

                    Transform point0 = _interactable.Get1(interactable).Transform;
                    Transform handle = _interactable.Get2(interactable).View.SelectedHandle;
                    Transform point1 = handle.GetChild(0);

                    //if (!_entering.IsEmpty())
                    //{
                    Vector3 vec = point1.position - point0.position;
                    _distance = vec.magnitude;
                    _angle = Vector3.SignedAngle(vec, point0.up, point0.forward);
                    //}

                    foreach (var productIdx in _product)
                    {
                        Vector2 localPos0 = _product.Get1(productIdx).Transform.InverseTransformPoint(point0.position);

                        int iNext0 = 2;
                        int iPrev0 = 1;
                        float heightClamp = _data.Rings[_data.Rings.Count - 2].Position.y - _distance;
                        for (int i = iPrev0; i < _data.Rings.Count; i++)
                        {
                            if (_data.Rings[i].Position.y < localPos0.y && _data.Rings[i].Position.y < heightClamp)
                            {
                                iNext0 = i + 1;
                                iPrev0 = i;
                            }
                        }

                        float value0 = (localPos0.y - _data.Rings[iPrev0].Position.y) / (_data.Rings[iNext0].Position.y - _data.Rings[iPrev0].Position.y);
                        localPos0 = Vector2.Lerp(_data.Rings[iPrev0].Position, _data.Rings[iNext0].Position, value0);
                        localPos0.x += Mathf.Lerp(_data.Rings[iPrev0].Width / 2, _data.Rings[iNext0].Width / 2, value0);

                        int iNext1 = iNext0;
                        int iPrev1 = iPrev0;
                        float distNext1 = 0;
                        float distPrev1 = 0;
                        for (int i = iNext0; i < _data.Rings.Count; i++)
                        {
                            distPrev1 = distNext1;
                            distNext1 = Vector3.Distance(localPos0, _data.Rings[i].Position);
                            if (distNext1 > _distance)
                            {
                                iNext1 = i;
                                iPrev1 = i - 1;
                                break;
                            }
                        }

                        float value1 = (_distance - distPrev1) / (distNext1 - distPrev1);

                        Vector2 localPos1 = Vector2.Lerp(_data.Rings[iPrev1].Position, _data.Rings[iNext1].Position, value1);
                        localPos1.x += Mathf.Lerp(_data.Rings[iPrev1].Width / 2, _data.Rings[iNext1].Width / 2, value1);

                        localPos0.x *= -1;
                        localPos1.x *= -1;
                        Vector3 globalPos0 = _product.Get1(productIdx).Transform.TransformPoint(localPos0);
                        Vector3 globalPos1 = _product.Get1(productIdx).Transform.TransformPoint(localPos1);
                        Vector3 dir = Quaternion.Euler(0, 0, _angle) * (localPos1 - localPos0);

                        _interactable.Get1(interactable).Transform.position = globalPos0;
                        _interactable.Get1(interactable).Transform.rotation = _product.Get1(productIdx).Transform.rotation * Quaternion.LookRotation(Vector3.forward, dir);
                    }
                }
            }

            if (!_exiting.IsEmpty())
            {
                foreach (var interactable in _interactable)
                {
                    if (_interactable.Get2(interactable).View.SelectedHandle == null)
                        return;

                    Transform handle = _interactable.Get2(interactable).View.SelectedHandle;

                    foreach (var productIdx in _product)
                    {
                        handle.parent = _product.Get1(productIdx).Transform;
                    }
                }
            }
        }
    }
}
