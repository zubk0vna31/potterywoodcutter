using Leopotam.Ecs;
using Modules.StateGroup.Components;
using Modules.ViewHub;
using PWS.Pottery.ItemDataHolderService;
using PWS.Pottery.MeshGeneration;
using UnityEngine;

namespace PWS.Pottery.Stages.HandlePlacingStage
{
    public class HandlePlacingInteractableInitProcessing : IEcsRunSystem
    {
        // auto injected fields
        readonly EcsFilter<StateEnter, HandlePlacingState> _entering;
        readonly EcsFilter<UnityView, HandlePlacingInteractable> _interactable;
        readonly EcsFilter<UnityView, SculptingProductTag> _product;

        private ISculptingDataHolder _data;

        public HandlePlacingInteractableInitProcessing(ISculptingDataHolder data)
        {
            _data = data;
        }

        public void Run()
        {
            if (!_entering.IsEmpty())
            {
                foreach (var productIdx in _product)
                {
                    foreach (var idx in _interactable)
                    {
                        float height = _data.GeneralParameters.FullHeight / 2;
                        Quaternion rotation = _product.Get1(productIdx).Transform.rotation;
                        Vector3 pos = _product.Get1(productIdx).Transform.TransformPoint(Vector3.forward * 0.2f + Vector3.up * height);

                        _interactable.Get1(idx).Transform.position = pos;
                        _interactable.Get1(idx).Transform.rotation = Quaternion.LookRotation(_product.Get1(productIdx).Transform.up, -_product.Get1(productIdx).Transform.right);
                    }
                }
            }
        }
    }
}
