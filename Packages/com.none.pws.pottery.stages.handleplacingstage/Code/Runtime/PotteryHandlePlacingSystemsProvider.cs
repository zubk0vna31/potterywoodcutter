using Leopotam.Ecs;
using Modules.Root.ContainerComponentModel;
using Modules.Root.ECS;
using Modules.StateGroup.Core;
using PWS.Common.Messages;
using PWS.Common.RestartStage;
using PWS.Common.UI;
using PWS.Common.ValueRegulator;
using PWS.Pottery.Common;
using PWS.Pottery.ItemDataHolderService;
using PWS.Pottery.MeshGeneration;
using UnityEngine;
using PWS.Common.Audio;
using PWS.Common.TravelPoints;

namespace PWS.Pottery.Stages.HandlePlacingStage
{
    public class PotteryHandlePlacingSystemsProvider : MonoBehaviour, ISystemsProvider
    {
        internal class SystemsDependencies
        {
            [Inject] public ISculptingDataHolder SculptingData;
        }

        [SerializeField] private SetStateSOCommand _nextState;
        [SerializeField] private StateInfoData _stateInfo;
        
        [Header("Voice config")] 
        [SerializeField] private VoiceConfig _voiceConfig;

        public EcsSystems GetSystems(EcsWorld world, EcsSystems endFrame, EcsSystems mainSystems)
        {
            SystemsDependencies dependencies = new SystemsDependencies();
            SceneContainer.Instance.Inject(dependencies);

            EcsSystems systems = new EcsSystems(world);
            systems
                .Add(new TransitToStateOnUIActionSystem<HandlePlacingState>(_nextState))

                //restart
                //.Add(new CurrentStateRestoreDataProcessing<ItemFormingState, SculptingDataHolder>(dependencies.SculptingData))

                //show sculpting inside
                .Add(new ProductMeshAngleRangeProcessing<HandlePlacingState, SculptingProductMesherTag>(360, 1, dependencies.SculptingData))

                //state systems
                .Add(new GameObjectVisibilityProcessing<HandlePlacingState, HandlePlacingContainer>())
                .Add(new HandlePlacingInteractableInitProcessing(dependencies.SculptingData))
                .Add(new HandlePlacingInteractablePositionProcessing(dependencies.SculptingData))

                //Outline
                .Add(new OutlineByStateSystem<HandlePlacingState>(0))

                //ui
                .Add(new StateInfoWindowUIProcessing<HandlePlacingState>(_stateInfo))


                //.Add(new XRInteractableProcessing<HandlePlacingSetupState, SculptingProductTag>())
                .Add(new MeshVisibilityProcessing<HandlePlacingState, SculptingProductTag>())
                
                //voice
                .Add(new VoiceAudioSystem<HandlePlacingState>(_voiceConfig))

                //travel point
                .Add(new TravelPointPositioningSystem<HandlePlacingState>("Wheel"))

                ;

            endFrame
                //sculpting product
                .Add(new SculptingDataProcessingSystem<HandlePlacingState>(dependencies.SculptingData))
                .Add(new MeshRingsDataProcessingSystem<HandlePlacingState>(dependencies.SculptingData))
                .Add(new MeshUpdateSystem<HandlePlacingState, SculptingProductMesherTag>(dependencies.SculptingData))

                .OneFrame<HandleSelectedSignal>()

                .OneFrame<RestartCurrentStepMessage>()
                .OneFrame<NextStateSignal>()
                ;

            return systems;
        }
    }
}
