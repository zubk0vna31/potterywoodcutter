using UnityEngine;

namespace PWS.Pottery.Stages.DecorativeSculptingStage
{
    [CreateAssetMenu(fileName = "DecoratingStateConfig", menuName = "PWS/Pottery/Stages/DecorativeSculptingStage/Config", order = 0)]
    public class DecoratingStateConfig : ScriptableObject
    {
        public float CompletionThreshold = 0.9f;
        public float SkipThreshold = 0.75f;

        public float MaxCentralDecorSize = 0.05f;
        public float MaxLinearDecorSize = 0.1f;
        public int InteractorRadiusPixel = 10;

        public string TextureID = "_DecSculptTex";
        public string DrawMaskID = "_DecSculptMask";
    }
}
