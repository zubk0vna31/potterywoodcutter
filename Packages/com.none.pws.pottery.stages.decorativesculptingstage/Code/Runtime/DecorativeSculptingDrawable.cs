﻿using System.Linq;
using UnityEngine;

namespace PWS.Pottery.Stages.DecorativeSculptingStage
{
    public class DecorativeSculptingDrawable : Drawable
    {
        [Header("Core")]
        [SerializeField, Range(0f, 1f)]
        private float m_PixelThreshold = 0.5f;

        private Texture2D m_Reference;
        private int m_ReferenceAmount;
        private int m_PaintedAmount;
        
        public override float Percentage => Mathf.Clamp01((m_PaintedAmount * 1.0f) / m_ReferenceAmount);

        public override void SetReference(Texture2D reference)
        {
            m_Reference = reference;
        }

        public override void Init(string texId)
        {
            m_PaintedAmount = 0;
            m_ReferenceAmount = 0;

            //Material setup
            m_MaskID = Shader.PropertyToID(texId);
            m_Material = m_MeshRenderer.sharedMaterial;

            //Create mask texture

            if (!m_Mask) {
                m_Mask = new Texture2D(m_Reference.width, m_Reference.height, TextureFormat.R8, false);
                m_Mask.filterMode = FilterMode.Bilinear;
            }
            
            m_Mask.SetPixels(Enumerable.Repeat(
                Color.black, m_Reference.width * m_Reference.height).ToArray());
            m_Mask.Apply();
            
            //Set mask texture to material
            m_Material.SetTexture(m_MaskID, m_Mask);
            
            //Calculate reference amount
            var colors = m_Reference.GetPixels();
            for (int i = 0; i < colors.Length; i++)
            {
                var threshold = CalculateThreshold(colors[i]);
                var success = threshold < m_PixelThreshold;
                
                if (success) m_ReferenceAmount++;
            }
        }
        
        public override void Paint(Vector2 pos, int radius = 0, bool useCircle = false)
        {
            if (pos.y < 0 || pos.y > 1)
                return;

            if (m_Reference.wrapModeU == TextureWrapMode.Clamp)
            {
                if (pos.x < 0 || pos.x > 1)
                    return;
            }
            else if (m_Reference.wrapModeU == TextureWrapMode.Repeat)
            {
                pos.x = Mathf.Repeat(pos.x, 1);
            }

            Vector2Int pixel = new Vector2Int((int)(pos.x * m_Reference.width), (int)(pos.y * m_Reference.height));

            pixel.x = Mathf.Clamp(pixel.x, 0, m_Reference.width);
            pixel.y = Mathf.Clamp(pixel.y, 0, m_Reference.height);
            
            if (radius < 1)
            {
                useCircle = false;
            }
            
            bool needApplyTexture = false;
            
            for (int i = pixel.x - radius; i <= pixel.x + radius; i++)
            {
                for (int j = pixel.y - radius; j <= pixel.y + radius; j++)
                {
                    if (InBounds(i, j, m_Reference.width, m_Reference.height))
                    {
                        var current = new Vector2Int(i, j);
                        var threshold = CalculateThreshold(m_Reference, current);
                        var success = threshold < m_PixelThreshold;


                        var hasPixel = m_Mask.GetPixel(current.x, current.y).r > 0;
                        if (hasPixel) continue;

                        if (useCircle)
                        {
                            if (InCircle(new Vector2Int(i, j), pixel, radius))
                            {
                                m_Mask.SetPixel(i, j, Color.red);
                                needApplyTexture = true;

                                if (success)
                                    m_PaintedAmount++;
                            }
                        }
                        else
                        {
                            m_Mask.SetPixel(i, j, Color.red);
                            needApplyTexture = true;

                            if (success)
                                m_PaintedAmount++;
                        }
                    }
                }
            }

            if (needApplyTexture)
            {
                m_Mask.Apply();
                m_Material.SetTexture(m_MaskID, m_Mask);
            }
        }

        public override void PaintAllPixels()
        {
            m_Mask.SetPixels(Enumerable.Repeat(
                   Color.red, m_Reference.width * m_Reference.height).ToArray());
            m_Mask.Apply();

            //Set mask texture to material
            m_Material.SetTexture(m_MaskID, m_Mask);
        }

        public override void ClearAll()
        {
            if (!m_Mask)
                return;
            if (!m_Material)
                return;

            m_Mask.SetPixels(Enumerable.Repeat(
                Color.black, m_Reference.width * m_Reference.height).ToArray());
            m_Mask.Apply();

            m_Material.SetTexture(m_MaskID, m_Mask);
        }
        
        private float CalculateThreshold(Texture2D reference, Vector2Int p)
        {
            return reference.GetPixel(p.x, p.y).r;
        }

        private float CalculateThreshold(Color color)
        {
            return color.r;
        }
    }
}
