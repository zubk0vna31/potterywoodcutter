﻿using Modules.ViewHub;
using Leopotam.Ecs;

namespace PWS.Pottery.Stages.DecorativeSculptingStage
{
    public class DecorativeSculptingHeightHandleView : ViewComponent
    {
        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            ecsEntity.Get<DecoratingHeightHandle>();
        }
    }
}