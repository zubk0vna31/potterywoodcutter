﻿using Modules.ViewHub;
using Leopotam.Ecs;
using UnityEngine;

namespace PWS.Pottery.Stages.DecorativeSculptingStage
{
    public class DecorativeSculptingDrawableRef : ViewComponent
    {
        [SerializeField]
        private DecorativeSculptingDrawable _drawable;

        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            ecsEntity.Get<DrawableComponent<DecorativeSculptingDrawableComponent>>().View = _drawable;
        }
    }
}