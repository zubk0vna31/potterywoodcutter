﻿using Modules.ViewHub;
using Leopotam.Ecs;

namespace PWS.Pottery.Stages.DecorativeSculptingStage
{
    public class DecorativeSculptingContainerView : ViewComponent
    {
        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            ecsEntity.Get<DecorativeSculptingContainer>();
        }
    }
}