﻿using Modules.ViewHub;
using Leopotam.Ecs;
using UnityEngine.XR.Interaction.Toolkit;
using System.Collections.Generic;
using UnityEngine;

namespace PWS.Pottery.Stages.DecorativeSculptingStage
{
    public class DecorativeSculptingSnapSocketView : ViewComponent
    {
        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            ecsEntity.Get<DecorativeSculptingSnapSocket>().Socket = GetComponent<XRSocketInteractor>();
        }
    }
}