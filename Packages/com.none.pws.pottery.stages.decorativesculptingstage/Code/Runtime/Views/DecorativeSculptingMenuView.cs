﻿using Modules.ViewHub;
using Leopotam.Ecs;
using UnityEngine.UI;
using UnityEngine;

namespace PWS.Pottery.Stages.DecorativeSculptingStage
{
    public class DecorativeSculptingMenuView : ViewComponent
    {
        [SerializeField]
        private DecorMenuItem[] _items;
        private DecorMenuItem _selectedItem;

        private EcsEntity _ecsEntity;

        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            _ecsEntity = ecsEntity;
            ecsEntity.Get<DecorativeSculptingMenu>().View = this;

            AddToggleListeners();
        }

        public void SetItem(int i)
        {
            DecorMenuItem curItem = _items[i];
            curItem.Toggle.isOn = true;
            _selectedItem = curItem;
            _ecsEntity.Get<DecorMenuItemSelected>().Item = curItem;
        }

        void AddToggleListeners()
        {
            foreach (DecorMenuItem item in _items)
            {
                DecorMenuItem curItem = item;
                item.Toggle.onValueChanged.AddListener((bool on) =>
                {
                    if (on)
                    {
                        _selectedItem = curItem;
                        _ecsEntity.Get<DecorMenuItemSelected>().Item = curItem;
                    }
                });
            }
        }
    }

    [System.Serializable]
    public class DecorMenuItem
    {
        public Toggle Toggle;
        public Texture2D Texture;
        public Texture2D Normals;
    }
}