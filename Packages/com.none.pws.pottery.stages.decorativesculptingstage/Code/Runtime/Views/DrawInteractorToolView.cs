using Modules.ViewHub;
using Leopotam.Ecs;
using UnityEngine;

namespace PWS.Pottery.Stages.DecorativeSculptingStage
{
    public class DrawInteractorToolView : ViewComponent
    {
        [SerializeField]
        private float _hitDistance = 0.105f;
        [SerializeField]
        private Transform _painter;
        [SerializeField]
        private Transform _meshHolder;

        private EcsEntity _ecsEntity;
        private Vector3 _initLocalPos;

        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            _initLocalPos = _meshHolder.localPosition;
            _ecsEntity = ecsEntity;

            ecsEntity.Get<DrawInteractor>();
            //ecsEntity.Get<StackCutterTool>();
        }

        private void Update()
        {
            _meshHolder.localPosition = _initLocalPos;
            RaycastHit[] hits = Physics.RaycastAll(_painter.position, _painter.forward, _hitDistance);
            foreach (RaycastHit hit in hits)
            {
                if (!hit.collider)
                    continue;

                if (!hit.collider.tag.Equals("DrawableQuad"))
                    continue;

                _ecsEntity.Get<StackCutterHitSignal>().Position = hit.textureCoord2;
                _meshHolder.position = hit.point;
                //_tr.rotation = Quaternion.LookRotation(-hit.normal, Vector3.up);
            }
        }
    }
}