using Leopotam.Ecs;
using Modules.Root.ContainerComponentModel;
using Modules.Root.ECS;
using Modules.StateGroup.Core;
using Modules.VRFeatures;
using PWS.Common.Audio;
using PWS.Common.Messages;
using PWS.Common.RestartStage;
using PWS.Common.ResultsEvaluation;
using PWS.Common.TravelPoints;
using PWS.Common.UI;
using PWS.Pottery.Common;
using PWS.Pottery.ItemDataHolderService;
using PWS.Pottery.MeshGeneration;
using UnityEngine;

namespace PWS.Pottery.Stages.DecorativeSculptingStage
{
    public class PotteryDecorativeSculptingStageSystemsProvider : MonoBehaviour, ISystemsProvider
    {
        internal class SystemsDependencies
        {
            [Inject] public ISculptingDataHolder SculptingData;
            [Inject] public IResultsEvaluationDataHolder ResultsEvaluationData;
        }

        [SerializeField] private SetStateSOCommand _nextState;
        [SerializeField] private DecoratingStateConfig _config;
        [SerializeField] private StateInfoData _stateInfo;

        [Header("Voice config")]
        [SerializeField] private VoiceConfig _itemPlacingVoiceConfig;
        [SerializeField] private VoiceConfig _decorSelectionVoiceConfig;
        [SerializeField] private VoiceConfig _paintStateVoiceConfig;

        public EcsSystems GetSystems(EcsWorld world, EcsSystems endFrame, EcsSystems mainSystems)
        {
            SystemsDependencies dependencies = new SystemsDependencies();
            SceneContainer.Instance.Inject(dependencies);

            EcsSystems systems = new EcsSystems(world);
            systems
                .Add(new TransitToStateOnUIActionSystem<DecorativeSculptingPaintState>(_nextState))

                .Add(new CurrentSubStateRestoreDataProcessing<DecorativeSculptingDecorSelectionState, DecorativeSculptingPaintState>())
                .Add(new DecorativeSculptingRestartProcessing<DecorativeSculptingDecorSelectionState,
                DecorativeSculptingDrawableComponent,DrawableQuad>(_config.TextureID, "_DecSculptBumpMap"))

                //item placing state systems
                .Add(new SnapSocketActivityProcessing<DecorativeSculptingItemPlacingState, DecorativeSculptingSnapSocket>())
                .Add(new GameObjectVisibilityProcessing<DecorativeSculptingItemPlacingState, DecorativeSculptingContainer>())

                .Add(new DecoratingItemPlacingProcessing<DecorativeSculptingItemPlacingState, DecorativeSculptingDecorSelectionState>())

                .Add(new MeshVisibilityProcessing<DecorativeSculptingItemPlacingState, SculptingProductMesherTag>())
                .Add(new XRInteractableProcessing<DecorativeSculptingItemPlacingState, SculptingProductTag>())

                .Add(new StateInfoWindowUIProcessing<DecorativeSculptingItemPlacingState>(_stateInfo))

                //decor selection state systems
                .Add(new GameObjectVisibilityProcessing<DecorativeSculptingDecorSelectionState, DecorativeSculptingContainer>())
                .Add(new GameObjectVisibilityProcessing<DecorativeSculptingDecorSelectionState, DecoratingHeightHandle>())
                .Add(new GameObjectVisibilityProcessing<DecorativeSculptingDecorSelectionState, DecorativeSculptingMenu>())

                .Add(new DecoratingMenuItemProcessing<DecorativeSculptingDecorSelectionState, DecorativeSculptingDrawableComponent,
                DecorativeSculptingPaintState, DecorativeSculptingUpdateSignal>(_config.TextureID, "_DecSculptBumpMap"))
                .Add(new HeightHandlePlacingProcessing<DecorativeSculptingDecorSelectionState, DecorativeSculptingUpdateSignal>(dependencies.SculptingData, _config))
                //.Add(new SculptingProductMeshChangedTracker<DecorativeSculptingDecorSelectionState, SculptingProductDecorationUpdatedSignal>(dependencies.SculptingData))

                .Add(new MeshVisibilityProcessing<DecorativeSculptingDecorSelectionState, SculptingProductMesherTag>())

                .Add(new StateInfoWindowUIProcessing<DecorativeSculptingDecorSelectionState>(_stateInfo))

                //paint state systems

                .Add(new DecoratingSkipProcessing<DecorativeSculptingPaintState, DecorativeSculptingDrawableComponent, DrawableQuad>(_nextState))

                .Add(new GameObjectVisibilityDoubleStateProcessing<DecorativeSculptingPaintState, DecorativeSculptingContainer, DecorativeSculptingDecorSelectionState>())
                .Add(new GameObjectVisibilityProcessing<DecorativeSculptingPaintState, StackCutterTool>())
                .Add(new GameObjectVisibilityProcessing<DecorativeSculptingPaintState, DrawableQuad>())

                .Add(new DecoratingPainterProcessing<DecorativeSculptingPaintState, DecorativeSculptingDrawableComponent, DrawableQuad>(_config))
                .Add(new DecoratingCompletionTracker<DecorativeSculptingPaintState, DecorativeSculptingDrawableComponent, DrawableQuad>(_config))

                //Outline
                .Add(new OutlineByStateSystem<DecorativeSculptingPaintState>(4))

                .Add(new SetGradeByProgress<DecorativeSculptingPaintState>(dependencies.ResultsEvaluationData, _stateInfo))

                .Add(new MeshVisibilityDoubleStateProcessing<DecorativeSculptingPaintState, SculptingProductMesherTag, DecorativeSculptingDecorSelectionState>())

                .Add(new StateInfoWindowUIProcessing<DecorativeSculptingPaintState>(_stateInfo))
                .Add(new StateWindowPlacementProcessing<DecorativeSculptingPaintState>("Table"))
                .Add(new StateWindowProgressUIProcessing<DecorativeSculptingPaintState>())
                .Add(new StateWindowButtonUIProcessing<DecorativeSculptingPaintState>())
                .Add(new SkipStageButtonUIProcessing<DecorativeSculptingPaintState>())

                //voice systems
                .Add(new VoiceAudioSystem<DecorativeSculptingItemPlacingState>(_itemPlacingVoiceConfig))
                .Add(new VoiceAudioSystem<DecorativeSculptingDecorSelectionState>(_decorSelectionVoiceConfig))
                .Add(new VoiceAudioSystem<DecorativeSculptingPaintState>(_paintStateVoiceConfig))

                //travel point
                .Add(new TravelPointPositioningSystem<DecorativeSculptingItemPlacingState>("Start"))
                .Add(new TravelPointPositioningSystem<DecorativeSculptingDecorSelectionState>("Start"))
                .Add(new TravelPointPositioningSystem<DecorativeSculptingPaintState>("Start"))

                ;

            endFrame
                //sculpting product
                .Add(new SculptingDataProcessingSystem<DecorativeSculptingItemPlacingState>(dependencies.SculptingData))
                .Add(new MeshRingsDataProcessingSystem<DecorativeSculptingItemPlacingState>(dependencies.SculptingData))
                .Add(new MeshUpdateSystem<DecorativeSculptingItemPlacingState, SculptingProductMesherTag>(dependencies.SculptingData))

                .Add(new DecoratingUV2Processing<DecorativeSculptingDecorSelectionState, DecorativeSculptingUpdateSignal>(dependencies.SculptingData, _config))
                .Add(new DecoratingTilingProcessing<DecorativeSculptingDecorSelectionState, DecorativeSculptingUpdateSignal>(dependencies.SculptingData, _config))
                .Add(new DrawableQuadUV2Processing<DecorativeSculptingDecorSelectionState, DecorativeSculptingUpdateSignal>(_config))

                .OneFrame<RestartCurrentStepMessage>()
                .OneFrame<NextStateSignal>()
                .OneFrame<SkipStageMessage>()
                .OneFrame<NextSubStateSignal>()

                .OneFrame<DecorativeSculptingUpdateSignal>()
                //.OneFrame<StackCutterHitSignal>()
                .OneFrame<DecorMenuItemSelected>()
                ;

            return systems;
        }
    }
}
