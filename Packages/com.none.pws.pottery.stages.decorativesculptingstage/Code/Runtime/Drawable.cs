﻿using UnityEngine;

namespace PWS.Pottery.Stages.DecorativeSculptingStage
{
    public abstract class Drawable : MonoBehaviour
    {
        [SerializeField]
        protected MeshRenderer m_MeshRenderer;
        
        protected Texture2D m_Mask;
        protected Material m_Material;
        protected int m_MaskID;
        
        public abstract float Percentage { get; }

        public abstract void SetReference(Texture2D reference);

        public abstract void Init(string texId);

        public abstract void Paint(Vector2 pos, int radius = 0, bool useCircle = false);

        public abstract void PaintAllPixels();

        public abstract void ClearAll();

        protected bool InBounds(int x, int y, int w, int h)
        {
            return x >= 0 && x < w && y >= 0 && y < h;
        }

        protected bool InCircle(Vector2Int p, Vector2Int o, int radius)
        {
            return (Mathf.Pow(p.x - o.x, 2) + Mathf.Pow(p.y - o.y, 2) <= radius * radius);
        }
    }
}
