using Modules.StateGroup.Core;
using UnityEngine;

namespace PWS.Pottery.Stages.DecorativeSculptingStage
{
    [CreateAssetMenu(fileName = "SetDecorativeSculptingStateSOCommand", menuName = "PWS/Pottery/Stages/DecorativeSculptingStage/SetStateSOCommand")]
    public class SetDecorativeSculptingStateSOCommand : SetStateSOCommand<DecorativeSculptingItemPlacingState>
    {

    }
}