﻿using Leopotam.Ecs;

namespace PWS.Pottery.Stages.DecorativeSculptingStage
{
    public struct DecorativeSculptingUpdateSignal : IEcsIgnoreInFilter
    {
    }
}