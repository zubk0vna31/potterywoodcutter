﻿using Leopotam.Ecs;

namespace PWS.Pottery.Stages.DecorativeSculptingStage
{
    public struct DecorativeSculptingMenu
    {
        public DecorativeSculptingMenuView View;
    }
}