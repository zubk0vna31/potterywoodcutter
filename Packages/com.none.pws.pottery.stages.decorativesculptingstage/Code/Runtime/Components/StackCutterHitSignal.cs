﻿using UnityEngine;

namespace PWS.Pottery.Stages.DecorativeSculptingStage
{
    public struct StackCutterHitSignal
    {
        public Vector2 Position;
    }
}