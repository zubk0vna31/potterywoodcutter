﻿using Leopotam.Ecs;

namespace PWS.Pottery.Stages.DecorativeSculptingStage
{
    public struct DecorativeSculptingItemPlacingState : IEcsIgnoreInFilter
    {
    }
    public struct DecorativeSculptingDecorSelectionState : IEcsIgnoreInFilter
    {
    }
    public struct DecorativeSculptingPaintState : IEcsIgnoreInFilter
    {
    }
}