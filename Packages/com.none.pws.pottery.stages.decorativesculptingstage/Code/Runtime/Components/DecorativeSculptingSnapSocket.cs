﻿using Leopotam.Ecs;
using UnityEngine.XR.Interaction.Toolkit;

namespace PWS.Pottery.Stages.DecorativeSculptingStage
{
    public struct DecorativeSculptingSnapSocket
    {
        public XRSocketInteractor Socket;
    }
}