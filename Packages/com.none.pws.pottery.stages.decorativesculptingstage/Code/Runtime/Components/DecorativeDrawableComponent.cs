﻿namespace PWS.Pottery.Stages.DecorativeSculptingStage
{
    public struct DrawableComponent<DrawableT> where DrawableT : struct
    {
        public Drawable View;
    }
}