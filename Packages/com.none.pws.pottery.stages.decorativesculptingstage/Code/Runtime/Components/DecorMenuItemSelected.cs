﻿using Leopotam.Ecs;

namespace PWS.Pottery.Stages.DecorativeSculptingStage
{
    public struct DecorMenuItemSelected
    {
        public DecorMenuItem Item;
    }
}