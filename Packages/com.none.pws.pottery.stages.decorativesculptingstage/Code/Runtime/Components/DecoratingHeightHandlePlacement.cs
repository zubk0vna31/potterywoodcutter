﻿using System;

namespace PWS.Pottery.Stages.DecorativeSculptingStage
{
    public struct DecoratingHeightHandlePlacement
    {
        public float Height;
        public float Size;
    }
}