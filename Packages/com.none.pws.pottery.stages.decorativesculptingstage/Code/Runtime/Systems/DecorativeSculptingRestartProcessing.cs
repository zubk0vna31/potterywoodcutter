using Leopotam.Ecs;
using Modules.StateGroup.Components;
using PWS.Pottery.Common;

namespace PWS.Pottery.Stages.DecorativeSculptingStage
{
    public class DecorativeSculptingRestartProcessing<StateT, DrawableT, SurfaceT> : IEcsRunSystem where StateT : struct where DrawableT : struct where SurfaceT : struct
    {
        // auto injected fields
        readonly EcsFilter<StateEnter, StateT> _entering;

        //readonly EcsFilter<DrawableComponent<DecorativeSculptingDrawableComponent>, SculptingProductTag> _drawable;
        readonly EcsFilter<DrawableComponent<DrawableT>, SurfaceT> _drawable;
        readonly EcsFilter<ClayMeshRenderer, SurfaceT> _meshRenderer;

        private string TEXTURE_ID;
        private string NORMALS_ID;

        public DecorativeSculptingRestartProcessing(string textureId, string normalsId = null)
        {
            TEXTURE_ID = textureId;
            NORMALS_ID = normalsId;
        }

        public void Run()
        {
            if (!_entering.IsEmpty())
            {
                foreach (var drawable in _drawable)
                {
                    _drawable.Get1(drawable).View.ClearAll();
                }
                foreach (var mr in _meshRenderer)
                {
                    _meshRenderer.Get1(mr).MeshRenderer.sharedMaterial.SetTexture(TEXTURE_ID, null);
                    if (NORMALS_ID != null)
                        _meshRenderer.Get1(mr).MeshRenderer.sharedMaterial.SetTexture(NORMALS_ID, null);
                }
            }

        }
    }
}
