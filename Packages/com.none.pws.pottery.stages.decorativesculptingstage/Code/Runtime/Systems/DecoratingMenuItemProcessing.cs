using Leopotam.Ecs;
using Modules.StateGroup.Components;
using Modules.StateGroup.Core;
using PWS.Common.UI;
using PWS.Pottery.Common;
using PWS.Pottery.MeshGeneration;
using UnityEngine;

namespace PWS.Pottery.Stages.DecorativeSculptingStage
{
    public class DecoratingMenuItemProcessing<StateT, DrawableT, NextStateT, SignalT> : IEcsRunSystem
        where StateT : struct where DrawableT : struct where NextStateT : struct where SignalT : struct
    {
        // auto injected fields
        readonly EcsFilter<StateEnter, StateT> _entering;
        readonly EcsFilter<StateT> _inState;
        readonly EcsFilter<StateT>.Exclude<StateExit> _inStateNotExit;

        readonly EcsFilter<NextSubStateSignal, DecorativeSculptingMenu> _nextSubStateSignal;
        readonly EcsFilter<DecorMenuItemSelected, DecorativeSculptingMenu> _selectedSignal;
        readonly EcsFilter<ClayMeshRenderer, DrawableQuad> _meshRenderer;
        readonly EcsFilter<SculptingProductTag> _product;
        readonly EcsFilter<DecorativeSculptingMenu> _menu;

        readonly EcsFilter<DrawableComponent<DrawableT>, DrawableQuad> _drawable;

        private StateFactory _stateFactory;

        private string TEXTURE_ID;
        private string NORMALS_ID;

        public DecoratingMenuItemProcessing(string textureId, string normalsId = null) {
            TEXTURE_ID = textureId;
            NORMALS_ID = normalsId;
        }

        public void Run()
        {
            if (!_entering.IsEmpty())
            {
                foreach (var menu in _menu)
                {
                    _menu.Get1(menu).View.SetItem(0);
                }
            }

            if (!_inState.IsEmpty())
            {
                foreach (var signal in _selectedSignal)
                {
                    foreach (var mr in _meshRenderer)
                    {
                        _meshRenderer.Get1(mr).MeshRenderer.sharedMaterial.SetTexture(TEXTURE_ID, _selectedSignal.Get1(signal).Item.Texture);
                        if(NORMALS_ID != null)
                            _meshRenderer.Get1(mr).MeshRenderer.sharedMaterial.SetTexture(NORMALS_ID, _selectedSignal.Get1(signal).Item.Normals);
                    }
                    foreach (var drawable in _drawable)
                    {
                        _drawable.Get1(drawable).View.SetReference(_selectedSignal.Get1(signal).Item.Texture);
                    }
                    foreach (var product in _product)
                    {
                        _product.GetEntity(product).Get<SignalT>();
                    }
                }
            }

            if (!_inStateNotExit.IsEmpty())
            {
                if (!_nextSubStateSignal.IsEmpty())
                {
                    _stateFactory.SetState<NextStateT>();
                }
            }
        }
    }
}
