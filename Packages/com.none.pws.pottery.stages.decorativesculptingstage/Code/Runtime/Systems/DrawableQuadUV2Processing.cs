using Leopotam.Ecs;
using Modules.ViewHub;
using PWS.Pottery.Common;
using PWS.Pottery.ItemDataHolderService;
using PWS.Pottery.MeshGeneration;
using UnityEngine;

namespace PWS.Pottery.Stages.DecorativeSculptingStage
{
    public class DrawableQuadUV2Processing<StateT, SignalT> : IEcsRunSystem where StateT : struct where SignalT : struct
    {
        // auto injected fields
        readonly EcsFilter<StateT> _inState;
        readonly EcsFilter<MeshFilterComponent, DrawableQuad> _meshFilter;
        readonly EcsFilter<ClayMeshRenderer, SculptingProductTag> _meshRenderer;

        readonly EcsFilter<SignalT, SculptingProductTag> _signal;

        private DecoratingStateConfig _config;
        private bool _isSculpting;

        //runtime data
        private Mesh mesh;

        public DrawableQuadUV2Processing(DecoratingStateConfig config, bool isSculpting = true)
        {
            _config = config;
            _isSculpting = isSculpting;
        }

        public void Run()
        {
            if (!_inState.IsEmpty())
            {
                if (_signal.IsEmpty())
                    return;

                foreach (var meshFilter in _meshFilter)
                {
                    foreach (var mr in _meshRenderer)
                    {
                        Vector2 offset = _meshRenderer.Get1(mr).MeshRenderer.sharedMaterial.GetTextureOffset(_config.TextureID);
                        Vector2 scale = _meshRenderer.Get1(mr).MeshRenderer.sharedMaterial.GetTextureScale(_config.TextureID);
                        UpdateUV2(_meshFilter.Get1(meshFilter).MeshFilter, offset, scale);
                    }
                }
            }
        }

        void UpdateUV2(MeshFilter meshFilter, Vector2 offset, Vector2 scale)
        {
            if (!mesh)
            {
                mesh = meshFilter.sharedMesh;
                mesh.Clear();
                mesh.name = "DrawableQuad";

                Vector3[] vertices = new Vector3[]
                {
                    new Vector3(1, 1, 0) * 0.5f,
                    new Vector3(1, -1, 0) * 0.5f,
                    new Vector3(-1, -1, 0) * 0.5f,
                    new Vector3(-1, 1, 0) * 0.5f
                };

                int[] triangles = new int[]
                {
                    0, 1, 2,
                    0, 2, 3
                };

                mesh.vertices = vertices;
                mesh.triangles = triangles;

                mesh.RecalculateNormals();
                mesh.RecalculateBounds();
                mesh.RecalculateTangents();
            }

            Vector2[] uvs = new Vector2[]
            {
               (new Vector2(0, 1) - offset) / scale,
               (new Vector2(0, 0) - offset) / scale,
               (new Vector2(1, 0) - offset) / scale,
               (new Vector2(1, 1) - offset) / scale
            };

            mesh.uv = uvs;
            mesh.uv2 = uvs;

            if (!_isSculpting)
            {
                mesh.uv3 = uvs;
                mesh.uv4 = new Vector2[] { Vector2.zero, Vector2.zero, Vector2.zero, Vector2.zero };
            }
            else
            {
                mesh.uv3 = new Vector2[] { Vector2.zero, Vector2.zero, Vector2.zero, Vector2.zero };
                mesh.uv4 = uvs;
            }

            meshFilter.sharedMesh = mesh;
        }
    }
}
