using Leopotam.Ecs;
using Modules.StateGroup.Components;
using Modules.ViewHub;
using PWS.Pottery.Common;
using PWS.Pottery.ItemDataHolderService;
using PWS.Pottery.MeshGeneration;
using UnityEngine;

namespace PWS.Pottery.Stages.DecorativeSculptingStage
{
    public class HeightHandlePlacingProcessing<StateT, SignalT> : IEcsRunSystem where StateT : struct where SignalT : struct
    {
        // auto injected fields
        readonly EcsFilter<StateEnter, StateT> _entering;
        readonly EcsFilter<StateT> _inState;

        readonly EcsFilter<UnityView, DecoratingHeightHandle> _handle;
        readonly EcsFilter<DecoratingHeightHandlePlacement, DecoratingHeightHandle> _handleOldPlacement;
        readonly EcsFilter<UnityView, SculptingProductTag> _product;

        private ISculptingDataHolder _data;
        private DecoratingStateConfig _config;
        private bool _isSculpting;

        //runtime data
        private float _prevHeight;

        public HeightHandlePlacingProcessing(ISculptingDataHolder data, DecoratingStateConfig config, bool isSculpting = true)
        {
            _data = data;
            _config = config;
            _isSculpting = isSculpting;
        }

        public void Run()
        {
            if (!_entering.IsEmpty())
            {
                foreach (var productIdx in _product)
                {
                    foreach (var idx in _handle)
                    {
                        float height = _data.GeneralParameters.FullHeight / 2;
                        Vector3 pos = _product.Get1(productIdx).Transform.TransformPoint(Vector3.up * height);

                        _prevHeight = 0;
                        _handle.Get1(idx).Transform.position = pos;
                    }
                }
            }
            if (!_inState.IsEmpty())
            {
                float size = Mathf.Min(_config.MaxCentralDecorSize, _data.GeneralParameters.FullHeight - _data.BottomRing.Width);
                float halfSize = size / 2;

                foreach (var handle in _handle)
                {
                    foreach (var product in _product)
                    {
                        Vector3 localPos = _handle.Get1(handle).Transform.localPosition;

                        if(_data.GeneralParameters.FullHeight >= _config.MaxCentralDecorSize * 2)
                            localPos.y = ClampedHeight(localPos.y);
                        else
                            localPos.y = Mathf.Clamp(localPos.y, halfSize + _data.BottomRing.Width, _data.GeneralParameters.FullHeight - halfSize);

                        _handle.Get1(handle).Transform.localPosition = localPos;

                        if (_isSculpting)
                        {
                            ref var placement = ref _handle.GetEntity(handle).Get<DecoratingHeightHandlePlacement>();
                            placement.Height = localPos.y;
                            placement.Size = size;
                        }

                        if (_prevHeight != _handle.Get1(handle).Transform.localPosition.y)
                        {
                            _product.GetEntity(product).Get<SignalT>();
                            _prevHeight = _handle.Get1(handle).Transform.localPosition.y;
                        }
                    }
                }
            }
        }
        float ClampedHeight(float height)
        {
            float size = Mathf.Min(_config.MaxCentralDecorSize, _data.GeneralParameters.FullHeight);
            float halfSize = size / 2;
            float heightOld = 0;
            float sizeOld = 0;
            foreach (var placementOld in _handleOldPlacement)
            {
                if (_isSculpting)
                    continue;

                heightOld = _handleOldPlacement.Get1(placementOld).Height;
                sizeOld = _handleOldPlacement.Get1(placementOld).Size;
            }

            height = Mathf.Clamp(height, halfSize, _data.GeneralParameters.FullHeight - halfSize);

            if (sizeOld > 0)
            {
                float halfSizeOld = sizeOld / 2;
                float resultSize = halfSize + halfSizeOld;
                float deltaHeight = Mathf.Abs(height - heightOld);

                bool up = height >= heightOld;
                if (deltaHeight < resultSize)
                    height = up ? heightOld + resultSize : heightOld - resultSize;

                if (height + halfSize > _data.GeneralParameters.FullHeight || height < halfSize)
                {
                    height = !up ? heightOld + resultSize : heightOld - resultSize;
                }
            }

            return height;
        }
    }
}
