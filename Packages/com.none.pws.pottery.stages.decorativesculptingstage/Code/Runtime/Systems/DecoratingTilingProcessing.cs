using Leopotam.Ecs;
using Modules.ViewHub;
using PWS.Pottery.Common;
using PWS.Pottery.ItemDataHolderService;
using PWS.Pottery.MeshGeneration;
using UnityEngine;

namespace PWS.Pottery.Stages.DecorativeSculptingStage
{
    public class DecoratingTilingProcessing<StateT, SignalT> : IEcsRunSystem where StateT : struct where SignalT : struct
    {
        // auto injected fields
        readonly EcsFilter<StateT> _inState;
        readonly EcsFilter<ClayMeshRenderer, SculptingProductTag> _meshRenderer;

        readonly EcsFilter<SignalT, SculptingProductTag> _signal;
        readonly EcsFilter<UnityView, DecoratingHeightHandle> _heightHandle;

        private ISculptingDataHolder _data;
        private DecoratingStateConfig _config;

        public DecoratingTilingProcessing(ISculptingDataHolder data, DecoratingStateConfig config)
        {
            _data = data;
            _config = config;
        }

        public void Run()
        {
            if (!_inState.IsEmpty())
            {
                if (_signal.IsEmpty())
                    return;

                UpdateTiling();
            }
        }

        void UpdateTiling()
        {
            foreach (var handle in _heightHandle)
            {
                foreach (var mr in _meshRenderer)
                {
                    float maxDecorSize = _config.MaxCentralDecorSize;
                    if(_meshRenderer.Get1(mr).MeshRenderer.sharedMaterial.GetTexture(_config.TextureID).wrapModeU == TextureWrapMode.Repeat)
                        maxDecorSize = _config.MaxLinearDecorSize;

                    float fullHeight = _data.GeneralParameters.FullHeight;
                    float maxSize = Mathf.Min(maxDecorSize, fullHeight);
                    float height = _heightHandle.Get1(handle).Transform.localPosition.y;
                    float radius = GetRadius(height);
                    float circleLength = 2 * Mathf.PI * radius;
                    int scaleX = (int)(circleLength / maxSize);
                    //float scaleX = circleLength / maxSize;
                    float aspectRatio = fullHeight / circleLength;
                    float scaleY = scaleX * aspectRatio;

                    _meshRenderer.Get1(mr).MeshRenderer.sharedMaterial.SetTextureScale(_config.TextureID, new Vector2(scaleX, scaleY));
                    _meshRenderer.Get1(mr).MeshRenderer.sharedMaterial.SetTextureOffset(_config.TextureID, new Vector2(0, -(height * scaleY / fullHeight) + 0.5f));
                }
            }
        }

        float GetRadius(float height)
        {
            float width = 0;
            for (int i = 0; i < _data.Rings.Count - 1; i++)
            {
                if (height >= _data.Rings[i].Position.y && height < _data.Rings[i + 1].Position.y)
                {
                    float value = (height - _data.Rings[i].Position.y) / (_data.Rings[i + 1].Position.y - _data.Rings[i].Position.y);
                    width = Mathf.Lerp(_data.Rings[i].Position.x, _data.Rings[i + 1].Position.x, value);
                    break;
                }
            }
            return width;
        }
    }
}
