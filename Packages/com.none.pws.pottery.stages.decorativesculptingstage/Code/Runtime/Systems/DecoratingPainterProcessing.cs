using Leopotam.Ecs;
using Modules.StateGroup.Components;
using PWS.Pottery.Common;
using UnityEngine;

namespace PWS.Pottery.Stages.DecorativeSculptingStage
{
    public class DecoratingPainterProcessing<StateT, DrawableT, SurfaceT> : IEcsRunSystem where StateT : struct where DrawableT : struct where SurfaceT : struct
    {
        // auto injected fields
        readonly EcsFilter<StateEnter, StateT> _entering;
        readonly EcsFilter<StateT> _inState;

        readonly EcsFilter<StackCutterHitSignal, DrawInteractor> _hitSignal;
        readonly EcsFilter<DrawableComponent<DrawableT>, SurfaceT> _drawable;
        readonly EcsFilter<ClayMeshRenderer, SurfaceT> _meshRenderer;

        private DecoratingStateConfig _config;

        public DecoratingPainterProcessing(DecoratingStateConfig config)
        {
            _config = config;
        }

        public void Run()
        {
            if (!_entering.IsEmpty())
            {
                foreach (var drawable in _drawable)
                {
                    _drawable.Get1(drawable).View.Init(_config.DrawMaskID);
                }
            }
            if (!_inState.IsEmpty())
            {
                foreach (var hit in _hitSignal)
                {
                    foreach (var drawable in _drawable)
                    {
                        foreach (var mr in _meshRenderer)
                        {
                            Vector2 scale = _meshRenderer.Get1(mr).MeshRenderer.sharedMaterial.GetTextureScale(_config.TextureID);
                            Vector2 offset = _meshRenderer.Get1(mr).MeshRenderer.sharedMaterial.GetTextureOffset(_config.TextureID);

                            Vector2 scaledPos = new Vector2(_hitSignal.Get1(hit).Position.x * scale.x + offset.x, _hitSignal.Get1(hit).Position.y * scale.y + offset.y);
                            
                            _drawable.Get1(drawable).View.Paint(scaledPos, _config.InteractorRadiusPixel, true);
                        }
                    }
                }
            }
        }
    }
}
