using Leopotam.Ecs;
using Modules.ViewHub;
using PWS.Pottery.Common;
using PWS.Pottery.ItemDataHolderService;
using PWS.Pottery.MeshGeneration;
using UnityEngine;

namespace PWS.Pottery.Stages.DecorativeSculptingStage
{
    public class DecoratingUV2Processing<StateT, SignalT> : IEcsRunSystem where StateT : struct where SignalT : struct
    {
        // auto injected fields
        readonly EcsFilter<StateT> _inState;
        readonly EcsFilter<MeshFilterComponent, SculptingProductTag> _meshFilter;

        readonly EcsFilter<SignalT, SculptingProductTag> _signal;

        private ISculptingDataHolder _data;
        private DecoratingStateConfig _config;
        private bool _isSculpting;

        //runtime data
        private Vector2[] _uvs2;

        public DecoratingUV2Processing(ISculptingDataHolder data, DecoratingStateConfig config, bool isSculpting = true)
        {
            _data = data;
            _config = config;
            _isSculpting = isSculpting;
        }

        public void Run()
        {
            if (!_inState.IsEmpty())
            {
                if (_signal.IsEmpty())
                    return;

                foreach (var meshFilter in _meshFilter)
                {
                    if (_data.GeneralParameters.FullHeight < _config.MaxCentralDecorSize * 2 && !_isSculpting)
                        UpdateUV2Inside(_meshFilter.Get1(meshFilter).MeshFilter);
                    else
                        UpdateUV2Outside(_meshFilter.Get1(meshFilter).MeshFilter);
                }
            }
        }

        void UpdateUV2Outside(MeshFilter meshFilter)
        {
            Mesh mesh = meshFilter.sharedMesh;

            int realVertices = _data.SmoothParamaters.Vertices + 1;

            _uvs2 = new Vector2[_data.MeshRings.Count * realVertices];

            float fullHeight = _data.GeneralParameters.FullHeight;
            bool isOutside = true;
            for (int i = 0; i < _data.MeshRings.Count; i++)
            {
                /*if (_data.MeshRings[i].Position.y >= fullHeight)
                    isOutside = false;*/
                if (_data.MeshRings[i].TopWallRing)
                    isOutside = false;

                for (int j = 0; j < realVertices; j++)
                {
                    float uvX = ((float)j / _data.SmoothParamaters.Vertices);

                    if (isOutside)
                    {
                        float uvY = _data.MeshRings[i].Position.y / fullHeight;
                        _uvs2[i * realVertices + j] = new Vector2(uvX, uvY);
                    }
                    else
                    {
                        _uvs2[i * realVertices + j] = new Vector2(uvX, 2);
                    }
                }
            }

            //mesh.uv2 = _uvs2;
            if(_isSculpting)
                mesh.uv4 = _uvs2;
            else
                mesh.uv3 = _uvs2;
            meshFilter.sharedMesh = mesh;
        }

        void UpdateUV2Inside(MeshFilter meshFilter)
        {
            Mesh mesh = meshFilter.sharedMesh;

            int realVertices = _data.SmoothParamaters.Vertices + 1;

            _uvs2 = new Vector2[_data.MeshRings.Count * realVertices];

            float fullHeight = _data.GeneralParameters.FullHeight;
            bool isInside = true;
            for (int ringId = 0; ringId < _data.MeshRings.Count; ringId++)
            {
                int i = _data.MeshRings.Count - (ringId + 1);
                /*if (_data.MeshRings[i].Position.y >= fullHeight)
                     isInside = false;*/
                if (_data.MeshRings[i].TopWallRing)
                    isInside = false;

                for (int j = 0; j < realVertices; j++)
                {
                    float uvX = ((float)j / _data.SmoothParamaters.Vertices);

                    if (ringId > 0)
                    {
                        if (isInside)
                            {
                                float uvY = _data.MeshRings[i].Position.y / fullHeight;
                                _uvs2[i * realVertices + j] = new Vector2(uvX, uvY);
                        }
                        else
                        _uvs2[i * realVertices + j] = new Vector2(uvX, 2);
                    }
                    else
                        _uvs2[i * realVertices + j] = new Vector2(uvX, 0);
                }
            }

            //mesh.uv2 = _uvs2;
            mesh.uv3 = _uvs2;
            meshFilter.sharedMesh = mesh;
        }
    }
}
