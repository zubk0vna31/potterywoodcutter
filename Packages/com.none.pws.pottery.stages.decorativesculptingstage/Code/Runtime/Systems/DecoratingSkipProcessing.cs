using Leopotam.Ecs;
using Modules.StateGroup.Core;
using PWS.Common.Messages;
using PWS.Pottery.MeshGeneration;

namespace PWS.Pottery.Stages.DecorativeSculptingStage
{
    public class DecoratingSkipProcessing<StateT, DrawableT, SurfaceT> : IEcsRunSystem where StateT : struct where DrawableT : struct where SurfaceT : struct
    {
        // auto injected fields
        readonly EcsFilter<StateT> _inState;

        private readonly EcsFilter<SkipStageMessage> _skipPerformed;

        readonly EcsFilter<DrawableComponent<DrawableT>, SurfaceT> _drawable;

        private readonly StateFactory _stateFactory;

        private SetStateSOCommand _nextState;

        public DecoratingSkipProcessing(SetStateSOCommand nextState)
        {
            _nextState = nextState;
        }

        public void Run()
        {
            if (_inState.IsEmpty())
                return;
            if (_skipPerformed.IsEmpty())
                return;

            foreach (var i in _drawable)
            {
                _drawable.Get1(i).View.PaintAllPixels();
            }

            /*foreach (var i in _skipPerformed)
            {
                _skipPerformed.GetEntity(i).Del<SkipStageMessage>();
            }*/

            _nextState.Execute(_stateFactory);
        }
    }
}
