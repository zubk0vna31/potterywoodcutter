using Leopotam.Ecs;
using Modules.StateGroup.Components;
using PWS.Common.Messages;
using PWS.Common.UI;
using PWS.Pottery.MeshGeneration;

namespace PWS.Pottery.Stages.DecorativeSculptingStage
{
    public class DecoratingCompletionTracker<StateT, DrawableT, SurfaceT> : IEcsRunSystem where StateT : struct where DrawableT : struct where SurfaceT : struct
    {
        // auto injected fields
        readonly EcsFilter<StateEnter, StateT> _entering;
        readonly EcsFilter<StateT>.Exclude<StateExit> _inState;
        private readonly EcsFilter<SkipStageMessage> _skipPerformed;

        readonly EcsFilter<DrawableComponent<DrawableT>, SurfaceT> _drawable;
        readonly EcsFilter<StateWindow, StateProgress> _stateWindowWithProgress;

        private DecoratingStateConfig _config;

        public DecoratingCompletionTracker(DecoratingStateConfig config)
        {
            _config = config;
        }

        public void Run()
        {
            if (!_entering.IsEmpty())
            {
                foreach (var i in _stateWindowWithProgress)
                {
                    _stateWindowWithProgress.Get2(i).Value = 0;
                    _stateWindowWithProgress.Get2(i).SkipThreshold = _config.SkipThreshold;
                    _stateWindowWithProgress.Get2(i).CompletionThreshold = _config.CompletionThreshold;
                }
            }

            if (!_inState.IsEmpty())
            {
                if (!_skipPerformed.IsEmpty())
                    return;

                foreach (var drawable in _drawable)
                {
                    foreach (var i in _stateWindowWithProgress)
                    {
                        _stateWindowWithProgress.Get2(i).Value = _drawable.Get1(drawable).View.Percentage;
                    }
                }
            }
        }
    }
}
