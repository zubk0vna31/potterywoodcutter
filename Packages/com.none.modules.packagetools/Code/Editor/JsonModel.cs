#if UNITY_EDITOR
namespace Modules.PackageTools {
    public class Manifest {
        public string name;
        public string displayName;
        public string version;
        public string unity;
        public string description;
    }

    public class AssemblyDefinition {
        public string name;
        public string rootNamespace;
        public string[] references;
        public string[] includePlatforms;
    }
}
#endif