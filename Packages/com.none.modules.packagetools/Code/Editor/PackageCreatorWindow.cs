#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using UnityEditor.PackageManager;
using UnityEditor.PackageManager.Requests;
using System.IO;
using System.Text;

namespace Modules.PackageTools
{
    public class PackageCreatorWindow : EditorWindow
    {
        private string _packageName = "Package.Name";
        private string _organization = "none";
        private string _version = "1.0.0";
        private string _description;

        private string _folderName;
        private bool _isCheckingForExisting;
        private bool _isCreating;

        private ListRequest _checkPackageExistsRequest;

        // Add menu named "My Window" to the Window menu
        [MenuItem("Tools/Package Creator")]
        static void OpenWindow() {
            // Get existing open window or if none, make a new one:
            PackageCreatorWindow window = GetWindow<PackageCreatorWindow>();

            window.titleContent = new GUIContent("Package Creator");
            window.minSize = new Vector2(600, 200);
            window.Show();
        }

        void OnGUI() {
            EditorGUI.BeginDisabledGroup(_isCheckingForExisting || _isCreating);

            _packageName = EditorGUILayout.TextField("Name", _packageName);
            _organization = EditorGUILayout.TextField("Organization Name", _organization);
            _version = EditorGUILayout.TextField("Version", _version);
            _description = EditorGUILayout.TextField("Description", _description, GUILayout.Height(50));
            _folderName = "com." + _organization + "." + _packageName;
            _folderName = _folderName.ToLower();

            if (GUILayout.Button("Create")) {
                CheckPackageExists();
            }

            EditorGUILayout.LabelField(_folderName);

            EditorGUI.EndDisabledGroup();

            if (_isCheckingForExisting)
                EditorGUILayout.LabelField("Checking packages list... ");
        }

        void CheckPackageExists() {
            _isCheckingForExisting = true;
            _checkPackageExistsRequest = Client.List();
            EditorApplication.update += CheckPackageExistsProgress;
            Repaint();
        }

        void CheckPackageExistsProgress() {
            string error = "";
            if (_checkPackageExistsRequest.IsCompleted) {
                if (_checkPackageExistsRequest.Status == StatusCode.Success) {
                    foreach (var package in _checkPackageExistsRequest.Result) {
                        if (package.name.Equals(_folderName)) {
                            error = "Package with the same name already exists: " + _folderName;
                            break;
                        }
                        if (package.displayName.Equals(_packageName)) {
                            error = "Package with the same namespace already exists: " + _packageName;
                            break;
                        }
                    }

                } else if (_checkPackageExistsRequest.Status >= StatusCode.Failure) {
                    error = _checkPackageExistsRequest.Error.message;
                }

                if (error.Length == 0)
                    CreatePackage();
                else
                    Debug.LogError(error);

                _isCheckingForExisting = false;
                EditorApplication.update -= CheckPackageExistsProgress;
                Repaint();
            }
        }

        void CreatePackage() {
            _isCreating = true;

            DirectoryInfo directoryInfo = new DirectoryInfo(Application.dataPath);
            string allPackagesPath = Path.Combine(directoryInfo.Parent.ToString(), "Packages");
            string packagePath = Path.Combine(allPackagesPath, _folderName);
            string codePath = Path.Combine(packagePath, "Code");
            string runtimePath = Path.Combine(codePath, "Runtime");
            string editorPath = Path.Combine(codePath, "Editor");

            Directory.CreateDirectory(packagePath);
            Directory.CreateDirectory(codePath);
            Directory.CreateDirectory(runtimePath);
            Directory.CreateDirectory(editorPath);

            CreateManifest(packagePath);
            CreateAssemblyDefinition(_packageName, runtimePath, false);
            CreateAssemblyDefinition(_packageName, editorPath, true);

            Client.Resolve();

            Debug.Log("Package Created");

            _isCreating = false;
        }

        void CreateManifest(string packagePath) {
            string jsonPath = Path.Combine(packagePath, "package.json");
            using (FileStream fileStream = File.Create(jsonPath)) {
                Manifest manifest = new Manifest();
                manifest.name = _folderName;
                manifest.displayName = _packageName;
                manifest.version = _version;
                manifest.unity = UnityVersion();
                manifest.description = _description;
                string json = JsonUtility.ToJson(manifest);
                byte[] jsonBytes = Encoding.UTF8.GetBytes(json);
                fileStream.Write(jsonBytes, 0, jsonBytes.Length);
            }
        }

        void CreateAssemblyDefinition(string name, string codeFolderPath, bool editor) {
            string rightName = name;
            if (editor)
                rightName += ".Editor";

            string jsonPath = Path.Combine(codeFolderPath, rightName + ".asmdef");
            using (FileStream fileStream = File.Create(jsonPath)) {
                AssemblyDefinition assemblyDefinition = new AssemblyDefinition();
                assemblyDefinition.name = rightName;
                assemblyDefinition.rootNamespace = rightName;
                if (editor) {
                    assemblyDefinition.references = new string[] { name };
                    assemblyDefinition.includePlatforms = new string[] { "Editor" };
                }
                string json = JsonUtility.ToJson(assemblyDefinition);
                byte[] jsonBytes = Encoding.UTF8.GetBytes(json);
                fileStream.Write(jsonBytes, 0, jsonBytes.Length);
            }
        }

        string UnityVersion() {
            string version = Application.unityVersion;
            string rightVersion = "";
            int dots = 0;
            foreach (char c in version) {
                if (c.Equals('.'))
                    dots++;

                if (dots >= 2)
                    return rightVersion;

                rightVersion += c;
            }
            return "";
        }
    }
}
#endif