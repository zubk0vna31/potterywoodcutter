﻿using UnityEditor;
using UnityEngine;

namespace PWS.Common.Audio
{
    [CustomEditor(typeof(ToolSerialSoundViewComponent))]
    public class ToolSoundFaidConfigGUIButtons : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            ToolSerialSoundViewComponent tr = (ToolSerialSoundViewComponent) target;
           
            if (GUILayout.Button("play"))
            {
                tr.SetPlaySignalFromInspector();
            }

            if (GUILayout.Button("stop"))
            {
                tr.SetStopSignalFromInspector();    
            }
        }
    }
}