﻿using UnityEditor;
using UnityEngine;

namespace PWS.Common.Audio
{
    [CustomEditor(typeof(MusicViewComponent))]
    public class MusicViewComponentGUIButton : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            MusicViewComponent tr = (MusicViewComponent) target;
           
            if (GUILayout.Button("Play next clip"))
            {
                tr.PlayNextClip();
            }
        }
    }
}