﻿using System.Collections.Generic;
using Leopotam.Ecs;
using Modules.Audio;
using Modules.Root.ECS;
using UnityEngine;

namespace PWS.Common.Audio
{
    public class CommonAudioSystemsProvider : MonoBehaviour, ISystemsProvider
    {
        [SerializeField] private List<OneShotSoundTemplate> _footStepPrefabs;
        public EcsSystems GetSystems(EcsWorld world, EcsSystems endFrame, EcsSystems mainSystems)
        {
            EcsSystems systems = new EcsSystems(world);
            
            systems
                .Add(new FootStepsAudioSystem(_footStepPrefabs))
                .Add(new MusicAudioSystem())
                .Add(new FoleyAudioSystem())
                .Add(new SetProgressBasedSignal())
                .Add(new ToolSerialSoundAudioSystem())
                .Add(new ToolRandomSoundAudioSystem<ProgressBasedSoundSignal>())
                .Add(new QuiqMenuSoundSystem())
                .Add(new ButtonsSoundSystem())
                ;

            endFrame
                .OneFrame<VoiceIntroExitSignal>()
                .OneFrame<ToolSoundStartStopSignal>()
                .OneFrame<ProgressBasedSoundSignal>()
                .OneFrame<PickUpSignal>()
                .OneFrame<QuiqMenuSoundSignal>()
                .OneFrame<ButtonsSoundSignal>()
                ;
            return systems;
        }
    }
}