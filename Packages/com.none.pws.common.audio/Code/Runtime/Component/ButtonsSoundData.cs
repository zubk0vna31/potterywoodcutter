﻿using Modules.Audio;

namespace PWS.Common.Audio
{
    public struct ButtonsSoundData
    {
        public OneShotSoundTemplate SoundPrefab;
        public RandomSoundClipsData ButtonsClips;
    }
}