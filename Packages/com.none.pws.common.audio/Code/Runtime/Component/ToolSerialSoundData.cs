﻿using Modules.Audio;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace PWS.Common.Audio
{
    public struct ToolSerialSoundData
    {
        public XRGrabInteractable Interactable;
        public AudioClip LoopClip;
        public OneShotSoundTemplate OnOffClipPrefab;

        public float VolumeFadeInTime;
        public float VolumeFadeOutTime;
        public float MinVolume;
        public float MaxVolume;

        public float PitchFadeInTime;
        public float PitchFadeOutTime;
        public float MinPitch;
        public float MaxPitch;

        public bool Started;
        public bool Playing;

        public bool UseOnOffClips;
        public bool UseProgressBasedSignal;
    }
}