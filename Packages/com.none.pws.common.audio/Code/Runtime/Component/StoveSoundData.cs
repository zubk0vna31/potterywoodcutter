﻿using Modules.Audio;
using UnityEngine;

namespace PWS.Common.Audio
{
    public struct StoveSoundData
    {
        public StoveSoundClipsData StoveClips;
        public OneShotSoundTemplate RegulatorOn;
        public OneShotSoundTemplate RegulatorOff;
        public AudioSource StoveIntencitySource;
        public float IntencityTreshhold;
    }
}