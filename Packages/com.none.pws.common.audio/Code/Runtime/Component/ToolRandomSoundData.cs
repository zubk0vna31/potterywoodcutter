﻿using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace PWS.Common.Audio
{
    public struct ToolRandomSoundData
    {
        public XRGrabInteractable Interactable;
        public AudioClip[] Clips;
    }
}