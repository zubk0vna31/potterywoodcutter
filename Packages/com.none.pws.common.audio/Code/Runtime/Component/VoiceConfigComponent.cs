﻿using Modules.Audio;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace PWS.Common.Audio
{
    public struct VoiceConfigComponent
    {
        public string[] EnterClipsTerms;
        public string[] InClipsTerms;
        public string[] ExitClipsTerms;
        public string[] WrongClipsTerms;

        public int EnterClipsIndex;
        public int InClipsIndex;
        public int ExitClipsIndex;
        public int WrongClipsIndex;
        
        public OneShotSoundTemplate GrandDadPrefab;
        
        public float DelayTime;
        public float PassedTime;

        public XRBaseInteractable Interactable;
    }
}