﻿using UnityEngine.XR.Interaction.Toolkit;

namespace PWS.Common.Audio
{
    public struct StickableSoundComponent
    {
        public XRGrabInteractable Interctable;
        public Stickable Stickable;
        public bool Activated;
    }
}