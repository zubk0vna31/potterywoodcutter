﻿
namespace PWS.Common.Audio
{
    public struct StoveDoorSoundData
    {
        public RandomSoundClipsData OpenClips;
        public RandomSoundClipsData CloseClips;
    }
}