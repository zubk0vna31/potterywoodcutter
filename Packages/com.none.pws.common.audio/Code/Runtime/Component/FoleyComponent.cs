﻿
using Modules.Audio;
using UnityEngine.XR.Interaction.Toolkit;

namespace PWS.Common.Audio
{
    public struct FoleyComponent
    {
        public XRGrabInteractable Interactable;

        public OneShotSoundTemplate PickUpSoundPrefab;
        public OneShotSoundTemplate DropSoundPrefab;

        public RandomSoundClipsData PickUpClips;
        public RandomSoundClipsData DropClips;
    }
}