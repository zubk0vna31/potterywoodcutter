﻿namespace PWS.Common.Audio
{
    public struct QuiqMenuSoundData
    {
        public RandomSoundClipsData OpenClips;
        public RandomSoundClipsData ClosedClips;
    }
}