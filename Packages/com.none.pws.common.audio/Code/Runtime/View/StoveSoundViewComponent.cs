﻿using Leopotam.Ecs;
using Modules.Audio;
using Modules.ViewHub;
using UnityEngine;

namespace PWS.Common.Audio
{
    public class StoveSoundViewComponent : ViewComponent
    {
        private const float INTENCITY_TRESHHOLD = 0.2f;
        
        [SerializeField] private StoveSoundClipsData _stoveClips;
        [SerializeField] private OneShotSoundTemplate _regulatorOn;
        [SerializeField] private OneShotSoundTemplate _regulatorOff;
        [SerializeField] private AudioSource _stoveIntencitySource;
        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            ref var entity = ref ecsEntity.Get<StoveSoundData>();
            entity.StoveClips = _stoveClips;
            entity.RegulatorOff = _regulatorOff;
            entity.RegulatorOn = _regulatorOn;
            entity.StoveIntencitySource = _stoveIntencitySource;
            entity.IntencityTreshhold = INTENCITY_TRESHHOLD;
        }
    }
}