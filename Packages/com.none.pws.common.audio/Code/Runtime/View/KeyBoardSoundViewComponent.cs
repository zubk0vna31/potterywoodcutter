﻿using Leopotam.Ecs;
using Modules.Audio;
using Modules.ViewHub;
using Modules.VRKeyboard;
using UnityEngine;

namespace PWS.Common.Audio
{
    public class KeyBoardSoundViewComponent : ViewComponent
    {
        private EcsEntity _entity;
        [SerializeField] private CanvasKeyboardASCII _event;
        [SerializeField] private OneShotSoundTemplate _buttonSoundPrefab;
        [SerializeField] private RandomSoundClipsData _buttonClips;
        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            _entity = ecsEntity;
            ref var entity = ref _entity.Get<ButtonsSoundData>();
            entity.SoundPrefab = _buttonSoundPrefab;
            entity.ButtonsClips = _buttonClips;
            
            _event.onKewDownEvent.AddListener(SetSignal);
        }

        private void SetSignal()
        {
            _entity.Get<ButtonsSoundSignal>();
        }

        private void OnDestroy()
        {
            _event.onKewDownEvent.RemoveListener(SetSignal);
        }
    }
}