﻿using Leopotam.Ecs;
using Modules.ViewHub;

namespace PWS.Common.Audio
{
    public class TriggerViewComponent : ViewComponent
    {
        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            ecsEntity.Get<TriggerAreaTag>();
        }
    }
}