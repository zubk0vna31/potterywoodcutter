﻿using Leopotam.Ecs;
using Modules.ViewHub;
using UnityEngine;

namespace PWS.Common.Audio
{
    public class MusicViewComponent : ViewComponent
    {
        [SerializeField] private MusicData _musicData;
        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            ecsEntity.Get<MusicComponent>().MusicData = _musicData;
        }

#if UNITY_EDITOR
        public void PlayNextClip()
        {
            var source = GetComponent<AudioSource>();
            var clipLength = source.clip.length;
            source.time = clipLength - 5;
        }
#endif
    }
}