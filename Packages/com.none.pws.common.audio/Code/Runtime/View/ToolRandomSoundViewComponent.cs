﻿using Leopotam.Ecs;
using Modules.ViewHub;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace PWS.Common.Audio
{
    public class ToolRandomSoundViewComponent : ViewComponent
    {
        [SerializeField] private XRGrabInteractable _interactable;
        [SerializeField] private RandomSoundClipsData _clipsData;
        
        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            ref var entity = ref ecsEntity.Get<ToolRandomSoundData>();
            entity.Interactable = _interactable;
            entity.Clips = _clipsData.Clips.ToArray();
        }
    }
}