﻿using Leopotam.Ecs;
using Modules.ViewHub;
using PWS.WoodCutting.Common;

namespace PWS.Common.Audio
{
    public class GranDadViewComponent : ViewComponent
    {
        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            ecsEntity.Get<GrandDadTag>();
            ecsEntity.Get<WoodCuttingGrandDadTag>();
        }
    }
}