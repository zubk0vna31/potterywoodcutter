﻿using Leopotam.Ecs;
using Modules.ViewHub;
using UnityEngine;

namespace PWS.Common.Audio
{
    public class StoveDoorSoundViewComponent : ViewComponent
    {
        [SerializeField] private RandomSoundClipsData _openClips;
        [SerializeField] private RandomSoundClipsData _closeClips;
        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            ref var entity = ref ecsEntity.Get<StoveDoorSoundData>();
            entity.OpenClips = _openClips;
            entity.CloseClips = _closeClips;
        }
    }
}