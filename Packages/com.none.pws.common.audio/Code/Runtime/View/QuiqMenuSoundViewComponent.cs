﻿using Leopotam.Ecs;
using Modules.ViewHub;
using UnityEngine;

namespace PWS.Common.Audio
{
    public class QuiqMenuSoundViewComponent : ViewComponent
    {
        private EcsEntity _entity;
        
        [SerializeField] private QuickMenu.QuickMenu _quickMenu;
        [SerializeField] private RandomSoundClipsData _openClips;
        [SerializeField] private RandomSoundClipsData _closedClips;
        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            _entity = ecsEntity;
            ref var clipsData = ref _entity.Get<QuiqMenuSoundData>();
            clipsData.OpenClips = _openClips;
            clipsData.ClosedClips = _closedClips;
            
            _quickMenu.OnMenuOpenedEvent.AddListener(() =>
            {
                _entity.Get<QuiqMenuSoundSignal>().Opened = true;
            });
            
            _quickMenu.OnMenuClosedEvent.AddListener(() =>
            {
                _entity.Get<QuiqMenuSoundSignal>().Opened = false;
            });
        }
    }
}