﻿using System;
using Leopotam.Ecs;
using Modules.Audio;
using Modules.ViewHub;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace PWS.Common.Audio
{
    public class ToolSerialSoundViewComponent : ViewComponent
    {
        private EcsEntity _entity;

        [SerializeField] private XRGrabInteractable _interactable;
        [SerializeField] private SerialSoundClipsData _clipsData;
        [SerializeField] private OneShotSoundTemplate _onOffClipPrefab;
        [SerializeField] private bool _useOnOffClips;
        [SerializeField] private bool _useProgressBasedSignal;
        [Range(0, 3)]
        [SerializeField] private float _volumeFadeInTime;
        [Range(0, 3)]
        [SerializeField] private float _volumeFadeOutTime;
        [Range(0, 1)]
        [SerializeField] private float _minVolume;
        [Range(0, 1)]
        [SerializeField] private float _maxVolume;
        [Range(0, 3)] 
        [SerializeField] private float _pitchFadeInTime;
        [Range(0, 3)]
        [SerializeField] private float _pitchFadeOutTime;
        [Range(-3, 3)]
        [SerializeField] private float _minPitch;
        [Range(-3, 3)] 
        [SerializeField] private float _maxPitch;
        
#if UNITY_EDITOR
        private bool _sendSignal;
#endif

        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            ref var entity = ref ecsEntity.Get<ToolSerialSoundData>();
            entity.Interactable = _interactable;
            entity.LoopClip = _clipsData.LoopClip;
            entity.OnOffClipPrefab = _onOffClipPrefab;
            entity.VolumeFadeInTime = _volumeFadeInTime;
            entity.VolumeFadeOutTime = _volumeFadeOutTime;
            entity.MinVolume = _minVolume;
            entity.MaxVolume = _maxVolume;
            entity.PitchFadeInTime = _pitchFadeInTime;
            entity.PitchFadeOutTime = _pitchFadeOutTime;
            entity.MaxPitch = _maxPitch;
            entity.MinPitch = _minPitch;
            entity.UseOnOffClips = _useOnOffClips;
            entity.UseProgressBasedSignal = _useProgressBasedSignal;
            _entity = ecsEntity;

            if (!_useProgressBasedSignal)
            {
                _interactable.activated.AddListener(SetStartSignal);
                _interactable.deactivated.AddListener(SetStopSignal);
            }
            _interactable.selectExited.AddListener(SetStopSignalOnDeselect);
        }
        
        private void SetStartSignal(ActivateEventArgs arg)
        {
            _entity.Get<ToolSoundStartStopSignal>();
            _entity.Get<ToolSerialSoundData>().Started = true;
        }

        private void SetStopSignal(DeactivateEventArgs arg)
        {
            _entity.Get<ToolSoundStartStopSignal>();
            _entity.Get<ToolSerialSoundData>().Started = false;
        }

        private void SetStopSignalOnDeselect(SelectExitEventArgs arg)
        {
            _entity.Get<ToolSoundStartStopSignal>();
            _entity.Get<ToolSerialSoundData>().Started = false;
        }
        
#if UNITY_EDITOR

        private void Update()
        {
            if (_sendSignal)
                _entity.Get<ProgressBasedSoundSignal>();
        }

        public void SetPlaySignalFromInspector()
        {
            ref var entity = ref _entity.Get<ToolSerialSoundData>();
            entity.VolumeFadeInTime = _volumeFadeInTime;
            entity.VolumeFadeOutTime = _volumeFadeOutTime;
            entity.MinVolume = _minVolume;
            entity.MaxVolume = _maxVolume;
            entity.PitchFadeInTime = _pitchFadeInTime;
            entity.PitchFadeOutTime = _pitchFadeOutTime;
            entity.MaxPitch = _maxPitch;
            entity.MinPitch = _minPitch;
            
            _entity.Get<ToolSoundStartStopSignal>();
            _entity.Get<ToolSerialSoundData>().Started = true;

            if (_useProgressBasedSignal)
                _sendSignal = true;
        }

        public void SetStopSignalFromInspector()
        {
            _entity.Get<ToolSoundStartStopSignal>();
            _entity.Get<ToolSerialSoundData>().Started = false;
            _sendSignal = false;
        }
        
#endif
    }
}