﻿using Leopotam.Ecs;
using Modules.Audio;
using Modules.ViewHub;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace PWS.Common.Audio
{
    public class FoleyViewComponent : ViewComponent
    {
        private EcsEntity _entity;
        
        [SerializeField] private XRGrabInteractable _interactable;
        [SerializeField] private OneShotSoundTemplate _pickUpSoundPrefab;
        [SerializeField] private OneShotSoundTemplate _dropSoundPrefab;
        [SerializeField] private RandomSoundClipsData _pickUpClips;
        [SerializeField] private RandomSoundClipsData _dropClips;
        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            _entity = ecsEntity;
            ref var entity = ref _entity.Get<FoleyComponent>();
            entity.Interactable = _interactable;
            entity.PickUpSoundPrefab = _pickUpSoundPrefab;
            entity.DropSoundPrefab = _dropSoundPrefab;
            entity.PickUpClips = _pickUpClips;
            entity.DropClips = _dropClips;
            _interactable.selectEntered.AddListener(SetSignal);
        }

        private void SetSignal(SelectEnterEventArgs arg0)
        {
            _entity.Get<PickUpSignal>();
        }
    }
}