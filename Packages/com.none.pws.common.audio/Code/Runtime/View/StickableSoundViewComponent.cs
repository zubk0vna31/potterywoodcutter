﻿using Leopotam.Ecs;
using Modules.ViewHub;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace PWS.Common.Audio
{
    public class StickableSoundViewComponent : ViewComponent
    {
        private EcsEntity _entity;
        
        [SerializeField] private XRGrabInteractable _interactable;
        [SerializeField] private Stickable _stickable;
        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            _entity = ecsEntity;
            ref var entity = ref _entity.Get<StickableSoundComponent>();
            entity.Stickable = _stickable;
            entity.Interctable = _interactable;
            
            _interactable.activated.AddListener(arg => _entity.Get<StickableSoundComponent>().Activated = true);
            _interactable.deactivated.AddListener(arg =>  _entity.Get<StickableSoundComponent>().Activated = false);
            _interactable.selectExited.AddListener(arg =>  _entity.Get<StickableSoundComponent>().Activated = false);

        }
    }
}