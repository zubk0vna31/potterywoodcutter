﻿using Leopotam.Ecs;
using Modules.Audio;
using Modules.ViewHub;
using UnityEngine;
using UnityEngine.UI;

namespace PWS.Common.Audio
{
    public class ButtonsSoundViewComponent : ViewComponent
    {
        private EcsEntity _entity;
        
        [SerializeField] private OneShotSoundTemplate _buttonSoundPrefab;
        [SerializeField] private RandomSoundClipsData _buttonClips;
        private Button[] _buttons;
        private Toggle[] _toggles;
        
        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            FindUIElements();

            _entity = ecsEntity;
            ref var entity = ref _entity.Get<ButtonsSoundData>();
            entity.SoundPrefab = _buttonSoundPrefab;
            entity.ButtonsClips = _buttonClips;
        }

        private void SetSignal()
        {
            _entity.Get<ButtonsSoundSignal>();
        }
        
        private void FindUIElements()
        {
            _buttons = FindObjectsOfType<Button>(true);

            foreach (var button in _buttons)
            {
                if (!button.transform.parent.name.Contains("row"))
                {
                    button.onClick.AddListener(SetSignal);
                }
            }
            
            _toggles = FindObjectsOfType<Toggle>(true);
            foreach (var toggle in _toggles)
            {
                toggle.onValueChanged.AddListener(arg => SetSignal());
            }

            Debug.Log("total buttons: " + _buttons.Length);
            Debug.Log("total Toggle: " + _toggles.Length);
        }
    }
}