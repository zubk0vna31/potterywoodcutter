﻿using System.Collections.Generic;
using UnityEngine;

namespace PWS.Common.Audio
{
    [CreateAssetMenu(fileName = "FoleyData", menuName = "PWS/Common/Audio/FoleyData", order = 3)]
    public class FoleyData : ScriptableObject
    {
        public List<AudioClip> PickUpClips;
        public List<AudioClip> DropClips;
    }
}