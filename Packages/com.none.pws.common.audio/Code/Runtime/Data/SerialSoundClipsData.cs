﻿using UnityEngine;

namespace PWS.Common.Audio
{
    [CreateAssetMenu(fileName = "SerialSoundClipsData", menuName = "PWS/Common/Audio/SerialSoundConfig", order = 4)]
    public class SerialSoundClipsData : ScriptableObject
    {
        public AudioClip LoopClip;
    }
}