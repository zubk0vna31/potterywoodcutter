﻿using UnityEngine;

namespace PWS.Common.Audio
{
    [CreateAssetMenu(fileName = "StoveSoundClipsData", menuName = "PWS/Common/Audio/StoveSoundConfig", order = 5)]
    public class StoveSoundClipsData : ScriptableObject
    {
        public AudioClip LoopClip;
        public AudioClip IntencityLoopClip;
    }
}