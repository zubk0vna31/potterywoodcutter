﻿using System.Collections.Generic;
using UnityEngine;

namespace PWS.Common.Audio
{
    [CreateAssetMenu(fileName = "RandomSoundClipsData", menuName = "PWS/Common/Audio/RandomSoundConfig", order = 2)]
    public class RandomSoundClipsData : ScriptableObject
    {
        public List<AudioClip> Clips;
    }
}