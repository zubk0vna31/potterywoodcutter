﻿using System.Collections.Generic;
using Modules.Audio;
using UnityEngine;

namespace PWS.Common.Audio
{
    [CreateAssetMenu(fileName = "VoiceConfig", menuName = "PWS/Common/Audio/VoiceConfig", order = 0)]
    public class VoiceConfig : ScriptableObject
    {

        [Header("Localize Terms")]
        public string[] StateEnterClipsTerms;
        public string[] StateInClipsTerms;
        public string[] StateExitClipsTerms;
        public string[] WrongClipsTerms;

        [Space]
        public OneShotSoundTemplate GrandDadPrefab;


        [Header("Deprecated Parameters")]
        public AudioClip[] StateEnterClips;
        public AudioClip[] StateInClips;
        public AudioClip[] StateExitClips;
        public AudioClip[] WrongClips;

    }
}