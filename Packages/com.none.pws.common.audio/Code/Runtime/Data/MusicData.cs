﻿using System.Collections.Generic;
using UnityEngine;

namespace PWS.Common.Audio
{
    [CreateAssetMenu(fileName = "MusicConfig", menuName = "PWS/Common/Audio/MusicConfig", order = 1)]
    public class MusicData : ScriptableObject
    {
        public List<AudioClip> MusicClips;
    }
}