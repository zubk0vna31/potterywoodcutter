﻿using Leopotam.Ecs;
using Modules.Audio;
using UnityEngine;

namespace PWS.Common.Audio
{
    public class QuiqMenuSoundSystem : IEcsRunSystem
    {
        private readonly EcsFilter<AudioSourceRef, QuiqMenuSoundData, QuiqMenuSoundSignal> _signal;
        public void Run()
        {
            if (!_signal.IsEmpty())
            {
                foreach (var signal in _signal)
                {
                    ref var play = ref _signal.GetEntity(signal).Get<PlayGlobalAuidioSignal>();
                    ref var data = ref _signal.Get2(signal);
                    play.AudioSource = _signal.Get1(signal).AudioSource;
                    
                    if (_signal.Get3(signal).Opened)
                    {
                        var id = Random.Range(0, data.OpenClips.Clips.Count);
                        play.AudioClip = data.OpenClips.Clips[id];
                    }
                    else
                    {
                        var id = Random.Range(0, data.ClosedClips.Clips.Count);
                        play.AudioClip = data.ClosedClips.Clips[id];
                    }
                }
            }
        }
    }
}