﻿using Leopotam.Ecs;
using Modules.Audio;
using Modules.StateGroup.Components;
using UnityEngine;

namespace PWS.Common.Audio
{
    public class StoveDoorSoundSystem<TStage> : IEcsRunSystem where TStage : struct
    {
        private readonly EcsFilter<StateEnter, TStage> _enterState;
        private readonly EcsFilter<StateExit, TStage> _exitState;
        private readonly EcsFilter<AudioSourceRef, StoveDoorSoundData> _door;
        public void Run()
        {
            if (!_enterState.IsEmpty())
            {
                foreach (var i in _door)
                {
                    ref var door = ref _door.Get2(i);
                    ref var play = ref _door.GetEntity(i).Get<PlayGlobalAuidioSignal>();
                    play.AudioSource = _door.Get1(i).AudioSource;
                    var id = Random.Range(0, door.CloseClips.Clips.Count);
                    play.AudioClip = door.CloseClips.Clips[id];
                }
            }
            
            if (!_exitState.IsEmpty())
            {
                foreach (var i in _door)
                {
                    ref var door = ref _door.Get2(i);
                    ref var play = ref _door.GetEntity(i).Get<PlayGlobalAuidioSignal>();
                    play.AudioSource = _door.Get1(i).AudioSource;
                    var id = Random.Range(0, door.OpenClips.Clips.Count);
                    play.AudioClip = door.OpenClips.Clips[id];
                }
            }
        }
    }
}