﻿using Leopotam.Ecs;
using Modules.Audio;
using Modules.UPhysics;
using Modules.ViewHub;
using UnityEngine;

namespace PWS.Common.Audio
{
    public class FoleyAudioSystem : IEcsRunSystem
    {
        private readonly EcsFilter<UnityView, FoleyComponent, Collidied> _triggered;
        private readonly EcsFilter<UnityView, FoleyComponent, PickUpSignal> _pickUped;
        
        public void Run()
        {
            if (!_triggered.IsEmpty())
            {
                foreach (var triggered in _triggered)
                {
                    ref var clips = ref _triggered.Get2(triggered);
                    if (clips.DropClips.Clips.Count == 0)
                        return;
                    
                    ref var signal = ref _triggered.Get3(triggered);
                    if (!signal.Other.IsNull() && signal.Other.Has<FoleyComponent>())
                        return;
                    
                    ref var play = ref _triggered.GetEntity(triggered).Get<PlaySoundAtPointSignal>();
                    var id = Random.Range(0, clips.DropClips.Clips.Count);
                    play.SoundPrefab = clips.DropSoundPrefab;
                    play.PlayPosition = _triggered.Get1(triggered).Transform.position;
                    play.AudioClip = clips.DropClips.Clips[id];
                }
            }
            
            if (!_pickUped.IsEmpty())
            {
                foreach (var pickUped in _pickUped)
                {
                    ref var tool = ref _pickUped.Get2(pickUped);
                    if( tool.PickUpClips.Clips.Count == 0) 
                        return;

                    if (tool.Interactable.isSelected)
                    {
                        ref var play = ref _pickUped.GetEntity(pickUped).Get<PlaySoundAtPointSignal>();
                        var id = Random.Range(0, tool.PickUpClips.Clips.Count);
                        play.SoundPrefab = tool.PickUpSoundPrefab;
                        play.PlayPosition = _pickUped.Get1(pickUped).Transform.position;
                        play.AudioClip = tool.PickUpClips.Clips[id];
                    }
                }
            }
        }
    }
}