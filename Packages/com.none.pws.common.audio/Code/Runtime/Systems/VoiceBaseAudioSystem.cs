﻿using Leopotam.Ecs;
using Modules.Audio;
using Modules.StateGroup.Components;
using UnityEngine;

namespace PWS.Common.Audio
{
    public class VoiceBaseAudioSystem<TStage> : IEcsRunSystem where TStage : struct
    {
        private readonly EcsFilter<VoiceIntroExitSignal> _enterState;
        private readonly EcsFilter<StateExit, TStage> _exitState;
        private readonly EcsFilter<TStage>.Exclude<StateEnter> _inState;
        private readonly EcsFilter<VoiceIntroTag> _introTag;
        private readonly EcsFilter<AudioSourceRef, GrandDadTag> _grandDad;
        private readonly EcsFilter<VoiceConfigComponent> _voiceConfigComponent;
        private readonly EcsWorld _ecsWorld;

        private bool _enterStateFlag;
        
        private VoiceConfig _voiceConfig;
        public VoiceBaseAudioSystem(VoiceConfig voiceConfig)
        {
            _voiceConfig = voiceConfig;
        }
        public void Run()
        {
            if (!_enterState.IsEmpty() && _introTag.IsEmpty())
            {
                _enterStateFlag = true;
                SetVoiceConfig();
                return;
            }

            if (!_inState.IsEmpty() && _introTag.IsEmpty())
            {
                foreach (var i in _voiceConfigComponent)
                {
                    ref var config = ref _voiceConfigComponent.Get1(i);

                    if (_enterStateFlag && config.EnterClipsIndex < config.EnterClipsTerms.Length)
                    {
                        PlayClip(config.EnterClipsTerms, ref config.EnterClipsIndex);

                        if (config.EnterClipsIndex >= config.EnterClipsTerms.Length)
                            _enterStateFlag = false;

                        return;
                    }

                    if (config.InClipsIndex < config.InClipsTerms.Length)
                        PlayClip(config.InClipsTerms, ref config.InClipsIndex);
                }
            }
            
            if (!_exitState.IsEmpty() && _introTag.IsEmpty())
            {
                foreach (var i in _grandDad)
                {
                    _grandDad.Get1(i).AudioSource.Stop();
                }

                foreach (var i in _voiceConfigComponent)
                {
                    ref var config = ref _voiceConfigComponent.Get1(i);
                    PlayClip(config.ExitClipsTerms, ref config.ExitClipsIndex);
                }
            }
        }

        private void PlayClip(string[] clips, ref int idx)
        {
            foreach (var dad in _grandDad)
            {
                ref var sourceRef = ref _grandDad.Get1(dad);
                if (sourceRef.AudioSource.isPlaying)
                    return;

                ref var signal = ref _grandDad.GetEntity(dad).Get<PlayGlobalLocalizedVoiceSignal>();
                signal.VoiceLocalize = _grandDad.Get1(dad).VoiceLocalize;
                signal.AudioSource = _grandDad.Get1(dad).AudioSource;
                signal.Term = clips[idx];
                idx++;
            }
        }
        private void SetVoiceConfig()
        {
            foreach (var component in _voiceConfigComponent)
            {
                _voiceConfigComponent.GetEntity(component).Destroy();
            }

            ref var entity = ref _ecsWorld.NewEntity().Get<VoiceConfigComponent>();

            entity.EnterClipsTerms = _voiceConfig.StateEnterClipsTerms;
            entity.InClipsTerms = _voiceConfig.StateInClipsTerms;
            entity.ExitClipsTerms = _voiceConfig.StateExitClipsTerms;
            entity.WrongClipsTerms = _voiceConfig.WrongClipsTerms;

            entity.EnterClipsIndex = 0;
            entity.InClipsIndex = 0;
            entity.ExitClipsIndex = 0;
            entity.WrongClipsIndex = 0;

            entity.GrandDadPrefab = _voiceConfig.GrandDadPrefab;
        }
    }
}