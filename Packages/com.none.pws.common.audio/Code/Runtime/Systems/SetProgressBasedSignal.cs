﻿using Leopotam.Ecs;
using PWS.Common.UI;
using UnityEngine;

namespace PWS.Common.Audio
{
    public class SetProgressBasedSignal : IEcsRunSystem 
    {
        private readonly EcsFilter<StateProgress> _progress;

        private float _newProgress;

        public void Run()
        {
            if (!_progress.IsEmpty())
            {
                foreach (var progress in _progress)
                {
                    if (_progress.Get1(progress).Value != _newProgress)
                    {
                        _progress.GetEntity(progress).Get<ProgressBasedSoundSignal>();
                        _newProgress = _progress.Get1(progress).Value;
                    }
                }
            }
        }
    }
}