﻿using System.Collections.Generic;
using Leopotam.Ecs;
using Modules.Audio;
using UnityEngine;
using PWS.Tutorial;

namespace PWS.Common.Audio
{
    public class FootStepsAudioSystem : IEcsRunSystem, IEcsInitSystem
    {
        private readonly EcsFilter<AudioSourceRef, GlobalAudioSourceTag, PlayerTag> _player;
        
        private const float FOOT_STEP_DISTANCE = 0.2f;
        private const float TELEPORT_DISTANCE = 1f;

        private Vector3 _oldPosition;
        private Vector3 _newPosition;

        private List<OneShotSoundTemplate> _footStepPrefabs;

        public FootStepsAudioSystem(List<OneShotSoundTemplate> prefabs)
        {
            _footStepPrefabs = prefabs;
        }
        
        public void Init()
        {
            if (_player.IsEmpty())
                return;
            
            foreach (var i in _player)
            {
                ref var cameraStartPosition = ref _player.Get1(i).Transform;
                _oldPosition = cameraStartPosition.position;
                _oldPosition.y = 0f;
                _newPosition = cameraStartPosition.position;
                _newPosition.y = 0f;
            }
        }
        
        public void Run()
        {
            if (_player.IsEmpty())
                return;

            foreach (var source in _player)
            {
                _newPosition = _player.Get1(source).Transform.position;
                _newPosition.y = 0f; 
                var currentDistance = Vector3.Distance(_oldPosition, _newPosition);

                if (currentDistance <= TELEPORT_DISTANCE && currentDistance >= FOOT_STEP_DISTANCE)
                {
                    SetPlaySoundAtPointSignal(source);
                }
                else if (currentDistance > TELEPORT_DISTANCE)
                {
                    SetPlaySoundAtPointSignal(source);
                }
            }
        }

        private void SetPlaySoundAtPointSignal(int source)
        {
            _oldPosition = _newPosition;
            var id = Random.Range(0, _footStepPrefabs.Count);
            ref var entity = ref _player.GetEntity(source).Get<PlaySoundAtPointSignal>();
            entity.PlayPosition = _newPosition;
            entity.SoundPrefab = _footStepPrefabs[id];
        }
    }
}