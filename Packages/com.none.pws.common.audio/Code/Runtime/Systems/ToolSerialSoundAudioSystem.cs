﻿using Leopotam.Ecs;
using Modules.Audio;
using Modules.Utils;
using Modules.ViewHub;
using UnityEngine;

namespace PWS.Common.Audio
{
    public class ToolSerialSoundAudioSystem: IEcsRunSystem
    {
        private readonly EcsFilter<UnityView, AudioSourceRef, ToolSerialSoundData, ToolSoundStartStopSignal> _oneShotSignal;
        private readonly EcsFilter<ProgressBasedSoundSignal> _progressSignal;
        private readonly EcsFilter<AudioSourceRef, ToolSerialSoundData> _tool;
        private readonly TimeService _timeService;

        public void Run()
        {
            if (!_oneShotSignal.IsEmpty())
            {
                foreach (var signal in _oneShotSignal)
                {
                    ref var toolData = ref _oneShotSignal.Get3(signal);
                    if (toolData.UseOnOffClips)
                    {
                        if (toolData.Playing || toolData.Started)
                        {
                            ref var playSound = ref _oneShotSignal.GetEntity(signal).Get<PlaySoundAtPointSignal>();
                            playSound.SoundPrefab = toolData.OnOffClipPrefab;
                            playSound.PlayPosition = _oneShotSignal.Get1(signal).Transform.position;
                        }  
                    }
                }
            }

            if (!_tool.IsEmpty())
            {
                foreach (var tool in _tool)
                {
                    ref var selectedTool = ref _tool.Get2(tool);
                    if (selectedTool.UseProgressBasedSignal)
                    {
                        if (!_progressSignal.IsEmpty() && selectedTool.Interactable.isSelected)
                            selectedTool.Started = true;
                        else
                            selectedTool.Started = false;
#if UNITY_EDITOR
                        if (!_progressSignal.IsEmpty())
                            selectedTool.Started = true;
                        else
                            selectedTool.Started = false;          
#endif
                    }

                    if (!selectedTool.Playing && selectedTool.Started)
                    {
                        ref var sourceRef = ref _tool.Get1(tool);
                        ref var playSound = ref _tool.GetEntity(tool).Get<PlayGlobalAuidioSignal>();
                        playSound.AudioSource = sourceRef.AudioSource;
                        playSound.AudioSource.loop = true;
                        playSound.AudioClip = selectedTool.LoopClip;
                        playSound.AudioSource.volume = selectedTool.MinVolume;
                        playSound.AudioSource.pitch = selectedTool.MinPitch;
                        selectedTool.Playing = true;
                    }

                    if (selectedTool.Playing && selectedTool.Started)
                    {
                        ref var sourceRef = ref _tool.Get1(tool);
                        if (sourceRef.AudioSource.volume < selectedTool.MaxVolume)
                        {
                            if (selectedTool.VolumeFadeInTime != 0f)
                                sourceRef.AudioSource.volume += _timeService.DeltaTime / selectedTool.VolumeFadeInTime;
                            else
                                sourceRef.AudioSource.volume += _timeService.DeltaTime;
                        }

                        if (sourceRef.AudioSource.pitch < selectedTool.MaxPitch)
                        {
                            if (selectedTool.PitchFadeInTime != 0)
                                sourceRef.AudioSource.pitch += _timeService.DeltaTime / selectedTool.PitchFadeInTime;
                            else
                                sourceRef.AudioSource.pitch += _timeService.DeltaTime;

                            if (sourceRef.AudioSource.pitch > selectedTool.MaxPitch)
                                sourceRef.AudioSource.pitch = selectedTool.MaxPitch;

                        }
                        
                    }

                    else if (selectedTool.Playing && !selectedTool.Started)
                    {
                        ref var sourceRef = ref _tool.Get1(tool);
                        if (sourceRef.AudioSource.volume > selectedTool.MinVolume)
                        {
                            if (selectedTool.VolumeFadeOutTime != 0f)
                                sourceRef.AudioSource.volume -= _timeService.DeltaTime / selectedTool.VolumeFadeOutTime;
                            else
                                sourceRef.AudioSource.volume -= _timeService.DeltaTime;
                            
                        }
                        
                        if (sourceRef.AudioSource.pitch > selectedTool.MinPitch)
                        {
                            if (selectedTool.PitchFadeInTime != 0)
                                sourceRef.AudioSource.pitch -= _timeService.DeltaTime / selectedTool.PitchFadeOutTime;
                            else
                                sourceRef.AudioSource.pitch -= _timeService.DeltaTime;
                            
                            if (sourceRef.AudioSource.pitch < selectedTool.MinPitch)
                                sourceRef.AudioSource.pitch = selectedTool.MinPitch;
                        }

                        if (sourceRef.AudioSource.volume <= selectedTool.MinVolume && sourceRef.AudioSource.pitch <=selectedTool.MinPitch)
                        {
                            sourceRef.AudioSource.Stop();
                            selectedTool.Playing = false;
                        }
                    }
                }
            }
        }
    }
}