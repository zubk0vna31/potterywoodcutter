﻿using Leopotam.Ecs;
using Modules.Audio;
using UnityEngine;

namespace PWS.Common.Audio
{
    public class ToolRandomSoundAudioSystem<TSignal> : IEcsRunSystem where TSignal : struct
    {
        private readonly EcsFilter<ToolRandomSoundData, AudioSourceRef> _tool;
        private readonly EcsFilter<TSignal> _signal;
        
        public void Run()
        {
            if (!_signal.IsEmpty())
            {
                foreach (var tool in _tool)
                {
                    ref var clips = ref _tool.Get1(tool);

                    if (clips.Interactable != null && clips.Interactable.isSelected)
                    {
                        ref var sourceRef = ref _tool.Get2(tool);
                        if (sourceRef.AudioSource.isPlaying)
                            return;
                    
                        var id = Random.Range(0, clips.Clips.Length);
                        ref var playSound = ref _tool.GetEntity(tool).Get<PlayGlobalAuidioSignal>();
                        playSound.AudioSource = sourceRef.AudioSource;
                        playSound.AudioClip = clips.Clips[id];
                    }
                }
            }
        }
    }
}