﻿using Leopotam.Ecs;
using Modules.Audio;
using Modules.ViewHub;
using UnityEngine;

namespace PWS.Common.Audio
{
    public class ButtonsSoundSystem : IEcsRunSystem
    {
        private readonly EcsFilter<UnityView, ButtonsSoundData, ButtonsSoundSignal> _soundData;

        public void Run()
        {
            if (_soundData.IsEmpty())
                return;

            foreach (var signal in _soundData)
            {
                foreach (var data in _soundData)
                {
                    ref var play = ref _soundData.GetEntity(data).Get<PlaySoundAtPointSignal>();
                    ref var soundData = ref _soundData.Get2(data);
                    play.SoundPrefab = soundData.SoundPrefab;
                    var id = Random.Range(0, soundData.ButtonsClips.Clips.Count);
                    play.AudioClip = soundData.ButtonsClips.Clips[id];
                    play.PlayPosition = _soundData.Get1(data).Transform.position;
                }
            }
        }
    }
}