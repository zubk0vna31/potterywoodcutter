﻿using Leopotam.Ecs;
using Modules.Audio;

namespace PWS.Common.Audio
{
    public class StickableBasedSoundSystem<TState> : IEcsRunSystem where TState : struct
    {
        private readonly EcsFilter<TState> _inState;
        private readonly EcsFilter<AudioSourceRef, StickableSoundComponent> _stickable;
        public void Run()
        {
            if (!_inState.IsEmpty())
            {
                foreach (var idx in _stickable)
                {
                    if (_stickable.Get2(idx).Activated && _stickable.Get2(idx).Stickable.IsSticked)
                    {
                        if (!_stickable.Get1(idx).AudioSource.isPlaying)
                            _stickable.Get1(idx).AudioSource.Play();
                    }
                    else
                    {
                        _stickable.Get1(idx).AudioSource.Stop();
                    }
                }
            }
        }
    }
}