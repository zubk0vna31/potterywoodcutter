﻿using Leopotam.Ecs;
using Modules.Audio;
using Modules.StateGroup.Components;
using UnityEngine;

namespace PWS.Common.Audio
{
    public class VoiceAudioSystem<TStage> : IEcsRunSystem where TStage : struct
    {

        private readonly EcsFilter<StateEnter, TStage> _enterState;
        private readonly EcsFilter<StateExit, TStage> _exitState;
        private readonly EcsFilter<TStage>.Exclude<StateEnter> _inState;
        private readonly EcsFilter<AudioSourceRef, GrandDadTag> _grandDad;
        private readonly EcsFilter<VoiceConfigComponent> _voiceConfigComponent;
        private readonly EcsWorld _ecsWorld;

        private bool _enterStateFlag;

        private VoiceConfig _voiceConfig;
        
        public VoiceAudioSystem(VoiceConfig voiceConfig)
        {
            _voiceConfig = voiceConfig;
        }
        public void Run()
        {
            if (!_enterState.IsEmpty())
            {
                _enterStateFlag = true;
                SetVoiceConfig();

                return;
            }

            if (!_inState.IsEmpty())
            {
                if (_enterStateFlag)
                {
                    foreach (var i in _voiceConfigComponent)
                    {
                        ref var voiceConfig = ref _voiceConfigComponent.Get1(i);
                        
                        if (voiceConfig.EnterClipsIndex < voiceConfig.EnterClipsTerms.Length)
                        {
                            PlayClip(voiceConfig.EnterClipsTerms, ref voiceConfig.EnterClipsIndex);
                        }
                        else
                        {
                            _enterStateFlag = false;
                        }

                        return;
                    }
                }

                foreach (var i in _voiceConfigComponent)
                {
                    ref var voiceConfig = ref _voiceConfigComponent.Get1(i);
                    if (voiceConfig.InClipsIndex < voiceConfig.InClipsTerms.Length)
                        PlayClip(voiceConfig.InClipsTerms, ref voiceConfig.InClipsIndex);
                }
            }
            
            if (!_exitState.IsEmpty())
            {
                foreach (var i in _grandDad)
                {
                    _grandDad.Get1(i).AudioSource.Stop();
                }
                
                foreach (var i in _voiceConfigComponent)
                {
                    ref var voiceConfig = ref _voiceConfigComponent.Get1(i);
                    if (voiceConfig.ExitClipsIndex < voiceConfig.ExitClipsTerms.Length)
                        PlayClip(voiceConfig.ExitClipsTerms, ref voiceConfig.ExitClipsIndex);
                }
            }
        }
        
        private void PlayClip(string[] clips, ref int idx)
        {
            foreach (var dad in _grandDad)
            {
                ref var sourceRef = ref _grandDad.Get1(dad);
                if (sourceRef.AudioSource.isPlaying)
                    return;

                ref var signal = ref _grandDad.GetEntity(dad).Get<PlayGlobalLocalizedVoiceSignal>();
                signal.VoiceLocalize = _grandDad.Get1(dad).VoiceLocalize;
                signal.AudioSource = _grandDad.Get1(dad).AudioSource;
                signal.Term = clips[idx];
                idx++;
            }
        }

        private void SetVoiceConfig()
        {
            foreach (var component in _voiceConfigComponent)
            {
                _voiceConfigComponent.GetEntity(component).Destroy();
            }

            ref var entity = ref _ecsWorld.NewEntity().Get<VoiceConfigComponent>();

            entity.EnterClipsTerms = _voiceConfig.StateEnterClipsTerms;
            entity.InClipsTerms = _voiceConfig.StateInClipsTerms;
            entity.ExitClipsTerms = _voiceConfig.StateExitClipsTerms;
            entity.WrongClipsTerms = _voiceConfig.WrongClipsTerms;

            entity.EnterClipsIndex = 0;
            entity.InClipsIndex = 0;
            entity.ExitClipsIndex = 0;
            entity.WrongClipsIndex = 0;

            entity.GrandDadPrefab = _voiceConfig.GrandDadPrefab;

            entity.DelayTime = 60f;
            entity.PassedTime = 0f;
        }
    }
}