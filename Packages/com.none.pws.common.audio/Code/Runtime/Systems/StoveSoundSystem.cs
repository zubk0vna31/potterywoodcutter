﻿using Leopotam.Ecs;
using Modules.Audio;
using Modules.StateGroup.Components;
using Modules.Utils;
using Modules.ViewHub;
using PWS.Pottery.ItemDataHolderService;

namespace PWS.Common.Audio
{
    public class StoveSoundSystem<TStage> : IEcsRunSystem where TStage : struct
    {
        private readonly EcsFilter<TStage> _inState;
        private readonly EcsFilter<StateExit, TStage> _exitState;
        private readonly EcsFilter<UnityView, AudioSourceRef, StoveSoundData> _stove;
        private readonly TimeService _timeService;

        private IValueRegulatorDataHolder<TemperatureRegulator> _data;

        private bool _stoveOn;

        public StoveSoundSystem(IValueRegulatorDataHolder<TemperatureRegulator> data)
        {
            _data = data;
        }
        public void Run()
        {
            if (_inState.IsEmpty())
                return;

            if (!_stoveOn && _data.Value > 0)
            {
                foreach (var i in _stove)
                {
                    ref var playOneShot = ref _stove.GetEntity(i).Get<PlaySoundAtPointSignal>();
                    ref var stoveData = ref _stove.Get3(i);
                    playOneShot.SoundPrefab = stoveData.RegulatorOn;
                    playOneShot.PlayPosition = _stove.Get1(i).Transform.position;
                    
                    ref var sourceRef = ref _stove.Get2(i);

                    if (!sourceRef.AudioSource.isPlaying)
                    {
                        ref var playLoop = ref _stove.GetEntity(i).Get<PlayGlobalAuidioSignal>();
                        playLoop.AudioSource = sourceRef.AudioSource;
                        playLoop.AudioSource.loop = true;
                        playLoop.AudioClip = stoveData.StoveClips.LoopClip;
                    }

                    if (!stoveData.StoveIntencitySource.isPlaying)
                    {
                        stoveData.StoveIntencitySource.clip = stoveData.StoveClips.IntencityLoopClip;
                        stoveData.StoveIntencitySource.loop = true;
                        stoveData.StoveIntencitySource.volume = 0f;
                        stoveData.StoveIntencitySource.Play();
                    }
                    _stoveOn = true;
                }
            }
            
            if (_stoveOn && _data.Value <= 0)
            {
                foreach (var i in _stove)
                {
                    ref var playOneShot = ref _stove.GetEntity(i).Get<PlaySoundAtPointSignal>();
                    ref var stoveData = ref _stove.Get3(i);
                    playOneShot.SoundPrefab = stoveData.RegulatorOff;
                    playOneShot.PlayPosition = _stove.Get1(i).Transform.position;
                    _stove.Get2(i).AudioSource.Stop();
                    stoveData.StoveIntencitySource.Stop();
                    _stoveOn = false;
                }
            }

            if (_stoveOn)
            {
                foreach (var i in _stove)
                {
                    ref var stoveData = ref _stove.Get3(i);

                    if (_data.Value >= stoveData.IntencityTreshhold && stoveData.StoveIntencitySource.volume <= _data.Value)
                    {
                        stoveData.StoveIntencitySource.volume += _timeService.DeltaTime;
                    }

                    if (stoveData.StoveIntencitySource.volume > _data.Value)
                    {
                        stoveData.StoveIntencitySource.volume -= _timeService.DeltaTime;
                        if (stoveData.StoveIntencitySource.volume <= stoveData.IntencityTreshhold)
                        {
                            stoveData.StoveIntencitySource.volume = 0f;
                        }
                    }
                }
            }

            if (!_exitState.IsEmpty())
            {
                foreach (var i in _stove)
                {
                    _stove.Get3(i).StoveIntencitySource.Stop();
                    _stove.Get2(i).AudioSource.Stop();
                }
            }
        }
    }
}