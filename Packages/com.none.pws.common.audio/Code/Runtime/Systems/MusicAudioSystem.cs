﻿using Leopotam.Ecs;
using Modules.Audio;
using Modules.StateGroup.Components;
using UnityEngine;

namespace PWS.Common.Audio
{
    public class MusicAudioSystem : IEcsRunSystem
    {
        private readonly EcsFilter<StateEnter> _stateEnter;
        private readonly EcsFilter<AudioSourceRef ,GrandDadTag> _dad;
        private readonly EcsFilter<AudioSourceRef, MusicComponent> _musicSource;

        private const float MAX_VOLUME = 0.5f;
        private const float MIN_VOLUME = 0.15f;
        private const float VOLUME_STEP = 0.01f;
        
        public void Run()
        {
            if (!_stateEnter.IsEmpty())
                return;
            
            foreach (var music in _musicSource)
            {
                ref var source = ref _musicSource.Get1(music);
                if (!source.AudioSource.isPlaying || source.AudioSource.clip == null)
                {
                    ref var data = ref _musicSource.Get2(music);
                    var id = Random.Range(0, data.MusicData.MusicClips.Count);
                    source.AudioSource.clip = data.MusicData.MusicClips[id];
#if UNITY_EDITOR
                    source.AudioSource.time = 0;                
#endif
                    source.AudioSource.Play();
                }
            }

            foreach (var dad in _dad)
            {
                ref var dadSource = ref _dad.Get1(dad);
                foreach (var music in _musicSource)
                {
                    ref var musicSource = ref _musicSource.Get1(music);
                    
                    if (dadSource.AudioSource.isPlaying)
                    {
                        if (musicSource.AudioSource.volume > MIN_VOLUME)
                        {
                            musicSource.AudioSource.volume -= VOLUME_STEP;
                        }
                        return;
                    }

                    if (!dadSource.AudioSource.isPlaying)
                    {
                        if (musicSource.AudioSource.volume < MAX_VOLUME)
                        {
                            musicSource.AudioSource.volume += VOLUME_STEP;
                        }
                        return;
                    }
                }
            }
        }
    }
}