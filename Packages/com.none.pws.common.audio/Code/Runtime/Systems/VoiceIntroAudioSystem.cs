﻿using Leopotam.Ecs;
using Modules.Audio;
using Modules.StateGroup.Components;
using Modules.Utils;
using Modules.UPhysics;
using Modules.ViewHub;
using PWS.Tutorial;
using UnityEngine;

namespace PWS.Common.Audio
{
    public class VoiceIntroAudioSystem<TStage> : IEcsRunSystem where TStage : struct
    {
        private readonly EcsFilter<StateEnter, TStage> _enterState;
        private readonly EcsFilter<TStage> _inState;
        private readonly EcsFilter<AudioSourceRef, GrandDadTag> _grandDad;
        private readonly EcsFilter<VoiceIntroTag> _introTag;
        private readonly EcsFilter<UnityView, TriggerAreaTag, Triggered> _triggered;
        private readonly EcsFilter<VoiceConfigComponent> _voiceConfigComponent;
        private readonly EcsWorld _ecsWorld;
        private readonly TimeService _timeService;

        private VoiceConfig _voiceConfig;
        
        public VoiceIntroAudioSystem(VoiceConfig voiceConfig)
        {
            _voiceConfig = voiceConfig;
        }
        public void Run()
        {
            if (!_enterState.IsEmpty())
            {
                foreach (var i in _grandDad)
                {
                    _grandDad.GetEntity(i).Get<VoiceIntroTag>();
                }
                
                SetVoiceConfig();
            }

            if (!_inState.IsEmpty() && !_introTag.IsEmpty())
            {
                foreach (var i in _voiceConfigComponent)
                {
                    ref var config = ref _voiceConfigComponent.Get1(i);
                    
                    config.PassedTime += _timeService.DeltaTime;
                
                    if (config.PassedTime >= config.DelayTime && config.ExitClipsIndex < config.ExitClipsTerms.Length)
                    {
                        PlayClip(config.ExitClipsTerms, ref config.ExitClipsIndex);
                    }
                    else if (config.EnterClipsIndex < config.EnterClipsTerms.Length)
                    {
                        PlayClip(config.EnterClipsTerms, ref config.EnterClipsIndex);
                    }
                }
               
            }
            
            if (!_triggered.IsEmpty() && !_introTag.IsEmpty())
            {
                foreach (var trigger in _triggered)
                {
                    ref var entity  = ref _triggered.Get3(trigger);
                    if (!entity.Other.IsNull())
                    {
                        if (entity.Other.Has<PlayerTag>())
                        {
                            foreach (var i in _introTag)
                            {
                                ref var intro = ref _introTag.GetEntity(i);
                                intro.Del<VoiceIntroTag>();
                                intro.Get<VoiceIntroExitSignal>();
                                _triggered.Get1(trigger).GameObject.SetActive(false);
                            }
                        
                            foreach (var dad in _grandDad)
                            {
                                _grandDad.Get1(dad).AudioSource.Stop();
                            }
                        }
                    }
                }
            }
        }

        private void PlayClip(string[] clips, ref int idx)
        {
            foreach (var dad in _grandDad)
            {
                ref var sourceRef = ref _grandDad.Get1(dad);
                if (sourceRef.AudioSource.isPlaying)
                    return;

                ref var signal = ref _grandDad.GetEntity(dad).Get<PlayGlobalLocalizedVoiceSignal>();
                signal.VoiceLocalize = _grandDad.Get1(dad).VoiceLocalize;
                signal.AudioSource = _grandDad.Get1(dad).AudioSource;
                signal.Term = clips[idx];
                idx++;
            }
        }

        private void SetVoiceConfig()
        {
            foreach (var component in _voiceConfigComponent)
            {
                _voiceConfigComponent.GetEntity(component).Destroy();
            }

            ref var entity = ref _ecsWorld.NewEntity().Get<VoiceConfigComponent>();

            entity.EnterClipsTerms = _voiceConfig.StateEnterClipsTerms;
            entity.InClipsTerms = _voiceConfig.StateInClipsTerms;
            entity.ExitClipsTerms = _voiceConfig.StateExitClipsTerms;
            entity.WrongClipsTerms = _voiceConfig.WrongClipsTerms;

            entity.EnterClipsIndex = 0;
            entity.InClipsIndex = 0;
            entity.ExitClipsIndex = 0;
            entity.WrongClipsIndex = 0;

            entity.GrandDadPrefab = _voiceConfig.GrandDadPrefab;

            entity.DelayTime = 60f;
            entity.PassedTime = 0f;
        }
    }
}