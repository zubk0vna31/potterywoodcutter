﻿using Leopotam.Ecs;
using Modules.Audio;
using Modules.StateGroup.Components;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace PWS.Common.Audio
{
    public class VoiceInteractableAudioSystem<TStage> : IEcsRunSystem where TStage : struct
    {
        private readonly EcsFilter<StateEnter, TStage> _enterState;
        private readonly EcsFilter<StateExit, TStage> _exitState;
        private readonly EcsFilter<TStage>.Exclude<StateEnter> _inState;
        private readonly EcsFilter<AudioSourceRef, GrandDadTag> _grandDad;
        private readonly EcsFilter<VoiceConfigComponent> _voiceConfigComponent;
        private readonly EcsWorld _ecsWorld;

        private XRBaseInteractable _interactable;
        private VoiceConfig _voiceConfig;
        private bool _enterStateFlag;
        private bool _inStateFlag;
        
        public VoiceInteractableAudioSystem(VoiceConfig voiceConfig, XRBaseInteractable interactable)
        {
            _voiceConfig = voiceConfig;
            _interactable = interactable;
        }

        public void Run()
        {
            if (!_enterState.IsEmpty())
            {
                _enterStateFlag = true;
                SetVoiceConfig();
                return;
            }

            if (!_inState.IsEmpty())
            {
                foreach (var i in _voiceConfigComponent)
                {
                    ref var config = ref _voiceConfigComponent.Get1(i);
                    
                    if (config.Interactable != null && config.Interactable.isSelected)
                        _inStateFlag = true;
                    
                    if (_enterStateFlag)
                    {
                        PlayClip(config.EnterClipsTerms, ref config.EnterClipsIndex, config.GrandDadPrefab);
                        if (config.EnterClipsIndex >= config.EnterClipsTerms.Length)
                            _enterStateFlag = false;
                        
                        return;
                    }

                    if (_inStateFlag)
                    {
                        if (config.InClipsIndex >= config.InClipsTerms.Length)
                        {
                            _inStateFlag = false;
                            return;
                        }
                        PlayClip(config.InClipsTerms, ref config.InClipsIndex, config.GrandDadPrefab);
                    }
                }
            }
            
            if (!_exitState.IsEmpty())
            {
                foreach (var i in _grandDad)
                {
                    _grandDad.Get1(i).AudioSource.Stop();
                }
                
                foreach (var i in _voiceConfigComponent)
                {
                    ref var config = ref _voiceConfigComponent.Get1(i);
                    PlayClip(config.ExitClipsTerms, ref config.ExitClipsIndex, config.GrandDadPrefab);
                }
            }

        }
        private void PlayClip(string[] clips, ref int idx, OneShotSoundTemplate grandDad)
        {
            foreach (var dad in _grandDad)
            {
                ref var sourceRef = ref _grandDad.Get1(dad);
                if (sourceRef.AudioSource.isPlaying)
                    return;

                ref var signal = ref _grandDad.GetEntity(dad).Get<PlayGlobalLocalizedVoiceSignal>();
                signal.VoiceLocalize = _grandDad.Get1(dad).VoiceLocalize;
                signal.AudioSource = _grandDad.Get1(dad).AudioSource;
                signal.Term = clips[idx];
                idx++;
            }
        }

        private void SetVoiceConfig()
        {
            foreach (var component in _voiceConfigComponent)
            {
                _voiceConfigComponent.GetEntity(component).Destroy();
            }

            ref var entity = ref _ecsWorld.NewEntity().Get<VoiceConfigComponent>();

            entity.EnterClipsTerms = _voiceConfig.StateEnterClipsTerms;
            entity.InClipsTerms = _voiceConfig.StateInClipsTerms;
            entity.ExitClipsTerms = _voiceConfig.StateExitClipsTerms;

            entity.EnterClipsIndex = 0;
            entity.InClipsIndex = 0;
            entity.ExitClipsIndex = 0;

            entity.GrandDadPrefab = _voiceConfig.GrandDadPrefab;

            entity.DelayTime = 60f;
            entity.PassedTime = 0f;

            entity.Interactable = _interactable;
        }
    }
}