using PWS.WoodCutting.Common;
using UnityEngine;

namespace PWS.WoodCutting.Stages.HolesDrilling
{
    [CreateAssetMenu(menuName = "PWS/WoodCutting/Stages/Holes Drilling/State Config")]
    public class HolesDrillingStateConfig : StateConfig
    {
        [Range(0f,1f)]
        public float skipThreshold=0.7f;

        [Range(0f, 5f)]
        public float skipDuration = 0.7f;
    }
}
