using Modules.StateGroup.Core;
using UnityEngine;

namespace PWS.WoodCutting.Stages.HolesDrilling
{
    [CreateAssetMenu(menuName = "PWS/WoodCutting/Stages/Holes Drilling/SetStateCommand")]
    public class SetHolesDrillingStateSOCommand : SetStateSOCommand<HolesDrillingState>
    {
        
    }
}
