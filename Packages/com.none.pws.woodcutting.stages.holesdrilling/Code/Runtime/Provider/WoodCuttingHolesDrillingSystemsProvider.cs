using Leopotam.Ecs;
using Modules.Root.ContainerComponentModel;
using Modules.Root.ECS;
using PWS.Common.Audio;
using PWS.Common.GameModeInfoService;
using PWS.Common.ResultsEvaluation;
using PWS.Common.UI;
using PWS.WoodCutting.Common;
using UnityEngine;
using PWS.Common.TravelPoints;


namespace PWS.WoodCutting.Stages.HolesDrilling
{

    internal class StateDependecies
    {
        [Inject] public IGameModeInfoService gameModeInfoService;
        [Inject] public IResultsEvaluationDataHolder resultsEvaluation;
    }

    public class WoodCuttingHolesDrillingSystemsProvider : MonoBehaviour,ISystemsProvider
    {
        [Header("Core")]
        [SerializeField]
        private StateConfig _stateConfig;
        [SerializeField]
        private StateInfoData _stateInfo;
        
        //[Header("Scene Related")]
        
        [Header("Voice config")]
        [SerializeField] private VoiceConfig _voiceConfig;

        public EcsSystems GetSystems(EcsWorld world, EcsSystems endFrame, EcsSystems mainSystems)
        {
            StateDependecies dependecies = new StateDependecies();
            SceneContainer.Instance.Inject(dependecies);

            EcsSystems systems = new EcsSystems(world, "WoodCutting Holes Drilling");

            systems

                .Add(new SetStateDelayedSystem<HolesDrillingState>())


                .Add(new OutlineByStateSystem<HolesDrillingState>(_stateConfig))

                .Add(new HolesDrillingInitializeSystem(_stateConfig))
                .Add(new HolesDrillingSkipStateSystem(_stateConfig))

                //Tracker
                .Add(new HolesDrillingTrackerSystem(_stateConfig,dependecies.gameModeInfoService))

                .Add(new WoodCuttingSetGradeByProgress<HolesDrillingState>(dependecies.resultsEvaluation,
                _stateInfo))

                .Add(new StateInfoWindowUIProcessing<HolesDrillingState>(_stateInfo))
                .Add(new StateWindowPlacementProcessing<HolesDrillingState>("Main"))

                .Add(new TravelPointPositioningSystem<HolesDrillingState>("Default"))


                .Add(new StateWindowProgressUIProcessing<HolesDrillingState>())
                .Add(new StateWindowButtonUIProcessing<HolesDrillingState>(false))
                
                //Voice
                .Add(new VoiceAudioSystem<HolesDrillingState>(_voiceConfig))
                
                //Sound
                .Add(new StickableBasedSoundSystem<HolesDrillingState>())

                ;

            return systems;
        }

        
    }
}
