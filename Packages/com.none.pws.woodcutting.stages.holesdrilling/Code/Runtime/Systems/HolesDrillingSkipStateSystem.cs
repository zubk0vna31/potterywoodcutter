using Leopotam.Ecs;
using Modules.StateGroup.Core;
using PWS.Common;
using PWS.Common.UI;
using PWS.WoodCutting.Common;

namespace PWS.WoodCutting.Stages.HolesDrilling
{
    public class HolesDrillingSkipStateSystem : RunSystem<HolesDrillingState,HolesDrillingStateConfig>
    {
        private readonly EcsFilter<NextStateSignal> _nextStateSignal;
        private readonly EcsFilter<CompleteStateSignal> _completeStateSignal;
        private readonly EcsFilter<DrillZone> _drillHoles;

        private readonly EcsFilter<StateProgress> _stateProgress;
        private readonly EcsWorld _ecsWorld;

        private readonly StateFactory _stateFactory;

        public HolesDrillingSkipStateSystem(StateConfig config) : base(config)
        {
        }

        protected override void OnStateUpdate()
        {
            base.OnStateUpdate();

            if (!_completeStateSignal.IsEmpty())
            {
                foreach (var i in _completeStateSignal)
                {
                    _completeStateSignal.Get1(i).button.gameObject.SetActive(false);
                    _completeStateSignal.GetEntity(i).Get<Ignore>();
                }

                foreach (var i in _drillHoles)
                {
                    _drillHoles.Get1(i).drillObject.Deactivate();
                }

                _config.nextState.Execute(_stateFactory);
                return;
            }

            if (_nextStateSignal.IsEmpty()) return;

            float grade = 0f;
            foreach (var i in _stateProgress)
            {
                grade = _stateProgress.Get1(i).ProgressValue;
            }
            _ecsWorld.NewEntity().Get<SetGradeSignal>().grade = grade;

            foreach (var i in _nextStateSignal)
            {
                _nextStateSignal.Get1(i).button.gameObject.SetActive(false);
                _nextStateSignal.GetEntity(i).Get<Ignore>();
            }

            foreach (var i in _drillHoles)
            {
                _drillHoles.Get1(i).drillObject.DrillWithDuration(_config.skipDuration);
            }

            ref var  delayState = ref _ecsWorld.NewEntity().Get<SetStateDelayed>();

            delayState.delay = _config.skipDuration;
            delayState.state = _config.nextState;
        }

    }
}
