using Leopotam.Ecs;
using PWS.Common.GameModeInfoService;
using PWS.Common.UI;
using PWS.WoodCutting.Common;

namespace PWS.WoodCutting.Stages.HolesDrilling
{
    public class HolesDrillingTrackerSystem : RunSystem<HolesDrillingState, HolesDrillingStateConfig>
    {
        private readonly EcsFilter<DrillZone> _drillHoles;
        private readonly EcsFilter<StateProgress> _stateProgress;
        private readonly IGameModeInfoService _gameModeInfoService;

        public HolesDrillingTrackerSystem(StateConfig config,IGameModeInfoService gameModeInfoService) : base(config)
        {
            _gameModeInfoService = gameModeInfoService;
        }

        protected override void OnStateEnter()
        {
            base.OnStateEnter();

            foreach (var i in _stateProgress)
            {
                ref var progress = ref _stateProgress.Get1(i);

                progress.Value = 0f;
                progress.CompletionThreshold = 1f;
                progress.SkipThreshold = _gameModeInfoService.CurrentMode.Equals(GameMode.Free)?
                    0.0f:_config.skipThreshold;
            }
        }

        protected override void OnStateExit()
        {
            base.OnStateExit();
        }

        protected override void OnStateUpdate()
        {
            base.OnStateUpdate();

            foreach (var i in _stateProgress)
            {
                float value = 0f;

                foreach (var j in _drillHoles)
                {
                    value+=_drillHoles.Get1(j).drillObject.Percentage;
                }

                value = (value * 1.0f) / _drillHoles.GetEntitiesCount();

                _stateProgress.Get1(i).Value = value;
            }
        }
    }
}
