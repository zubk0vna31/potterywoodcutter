using Leopotam.Ecs;
using Modules.ViewHub;
using PWS.WoodCutting.Common;

namespace PWS.WoodCutting.Stages.HolesDrilling
{
    public class HolesDrillingInitializeSystem : RunSystem<HolesDrillingState, HolesDrillingStateConfig>
    {
        private readonly EcsFilter<PlankFinal> _planks;

        private readonly EcsWorld _ecsWorld;

        public HolesDrillingInitializeSystem(StateConfig config) : base(config)
        {
        }

        protected override void OnStateEnter()
        {
            base.OnStateEnter();

            foreach (var i in _planks)
            {
                //_planks.Get1(i).view.State6.gameObject.SetActive(false);
                _planks.Get1(i).markupPaintingPattern.Deactivate();
                _planks.Get1(i).view.State7.gameObject.SetActive(true);
            }
        }
    }
}
