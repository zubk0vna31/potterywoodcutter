using Leopotam.Ecs;
using Modules.ViewHub;

namespace PWS.WoodCutting.Stages.HolesDrilling
{
    public class DrilZoneViewComponent : ViewComponent
    {
        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            var drillObject = GetComponent<DrillObject>();

            drillObject.Initialize();
            drillObject.Activate();

            ecsEntity.Get<DrillZone>().drillObject = drillObject;
        }
    }
}
