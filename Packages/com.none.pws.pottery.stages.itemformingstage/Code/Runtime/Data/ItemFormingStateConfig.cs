using PWS.Pottery.Sculpting.Interactor;
using UnityEngine;

namespace PWS.Pottery.Stages.ItemFormingStage
{
    [CreateAssetMenu(fileName = "ItemFormingStageConfig", menuName = "PWS/Pottery/Stages/ItemFormingStage/Config", order = 0)]
    public class ItemFormingStateConfig : ScriptableObject
    {
        public float CompletionThresold = 0.9f;
        public float SkipThreshold = 0.4f;
        [Range(0.1f, 1)]
        public float Difficulty = 0.4f;

        [Header("Hands")]
        public InteractableParameters HandMoveAxes;
        public float HandSmoothSpeed = 5;
        public float HandRadius = 0.1f;
        public AnimationCurve HandInteractionCurve = AnimationCurve.Linear(0, 0, 1, 1);
    }
}
