using Leopotam.Ecs;
using Modules.Root.ContainerComponentModel;
using Modules.Root.ECS;
using Modules.StateGroup.Core;
using PWS.Common.Audio;
using PWS.Common.Messages;
using PWS.Common.RestartStage;
using PWS.Common.UI;
using PWS.Pottery.ItemDataHolderService;
using PWS.Pottery.MeshGeneration;
using PWS.Pottery.Sculpting.Interactor;
using UnityEngine;
using PWS.Common.ValueRegulator;
using PWS.Pottery.WheelSpeedSetupStage;
using PWS.Pottery.Common;
using PWS.Common.ResultsEvaluation;
using PWS.Common.TravelPoints;
using PWS.Features.Achievements;

namespace PWS.Pottery.Stages.ItemFormingStage
{
    public class PotteryItemFormingSystemsProvider : MonoBehaviour, ISystemsProvider
    {
        internal class SystemsDependencies
        {
            [Inject] public ISculptingDataHolder SculptingData;
            [Inject] public SculptingReferenceDataHolder SculptingReferenceData;
            [Inject] public IValueRegulatorDataHolder<WheelSpeedRegulator> WheelSpeedData;
            [Inject] public IResultsEvaluationDataHolder ResultsEvaluationData;
            [Inject] public IAchievementsService AchievementsService;
        }

        [SerializeField] private SetStateSOCommand _nextState;

        [SerializeField] private ItemFormingStateConfig _config;
        [SerializeField] private ValueRegulatorConfig _wheelSpeedConfig;
        [SerializeField] private StateInfoData _stateInfo;
        
        [Header("Voice config")] 
        [SerializeField] private VoiceConfig _voiceConfig;

        public EcsSystems GetSystems(EcsWorld world, EcsSystems endFrame, EcsSystems mainSystems)
        {
            SystemsDependencies dependencies = new SystemsDependencies();
            SceneContainer.Instance.Inject(dependencies);

            EcsSystems systems = new EcsSystems(world);
            systems
                .Add(new TransitToStateOnUIActionSystem<ItemFormingState>(_nextState))
                .Add(new ItemFormingSkipProcessing(dependencies.SculptingData, dependencies.SculptingReferenceData.Data, _nextState))

                //restart
                .Add(new CurrentStateRestoreDataProcessing<ItemFormingState, SculptingDataHolder>(dependencies.SculptingData))
                .Add(new CurrentStateRestoreDataProcessing<ItemFormingState, ValueRegulatorDataHolder<WheelSpeedRegulator>>(dependencies.WheelSpeedData))

                //wheel speed systems
                .Add(new GameObjectVisibilityProcessing<ItemFormingState, WheelSpeedRegulator>())
                .Add(new ValueRegulatorSetupProcessing<ItemFormingState, WheelSpeedRegulator>(_wheelSpeedConfig, dependencies.WheelSpeedData))
                .Add(new ValueRegulatorInRangeProcessing<ItemFormingState, WheelSpeedRegulator, HandTag>(_wheelSpeedConfig, dependencies.WheelSpeedData))

                //state systems
                .Add(new HandInteractorVisibiltySystem<ItemFormingState>(SculptingHand.ItemForming))
                .Add(new ItemFormingHandsInitPosition(dependencies.SculptingData, _config))
                .Add(new HandInteractorPositionSystem<ItemFormingState>(_config.HandMoveAxes, _config.HandSmoothSpeed))
                .Add(new ItemFormingInteractorsHandler(dependencies.SculptingData, dependencies.SculptingReferenceData.Data, _config))
                .Add(new ItemFormingCompletionTracker(dependencies.SculptingData, dependencies.SculptingReferenceData.Data, _config))

                .Add(new SetGradeByProgress<ItemFormingState>(dependencies.ResultsEvaluationData, _stateInfo))
                
                .Add(new GoldenHandsAchievementSender<ItemFormingState>(
                    dependencies.ResultsEvaluationData,dependencies.AchievementsService))

                //ui
                .Add(new StateInfoWindowUIProcessing<ItemFormingState>(_stateInfo))

                .Add(new StateWindowPlacementProcessing<ItemFormingState>("Wheel"))
                .Add(new StateWindowProgressUIProcessing<ItemFormingState>(true))
                .Add(new StateWindowButtonUIProcessing<ItemFormingState>())
                .Add(new SkipStageButtonUIProcessing<ItemFormingState>())

                .Add(new MeshVisibilityProcessing<ItemFormingState, SculptingReferenceMesherTag>())
                .Add(new MeshVisibilityProcessing<ItemFormingState, SculptingProductTag>())
                
                //voice
                .Add(new VoiceAudioSystem<ItemFormingState>(_voiceConfig))

                //travel point
                .Add(new TravelPointPositioningSystem<ItemFormingState>("Wheel"))

                ;

            endFrame
                //wheel Speed
                .Add(new ValueRegulatorUpdateProcessing<ItemFormingState, WheelSpeedRegulator>(dependencies.WheelSpeedData))

                //sculpting reference
                .Add(new SculptingDataProcessingSystem<ItemFormingState>(dependencies.SculptingReferenceData.Data))
                .Add(new MeshRingsDataProcessingSystem<ItemFormingState>(dependencies.SculptingReferenceData.Data))
                .Add(new MeshUpdateSystem<ItemFormingState, SculptingReferenceMesherTag>(dependencies.SculptingReferenceData.Data))

                //sculpting product
                .Add(new SculptingDataProcessingSystem<ItemFormingState>(dependencies.SculptingData))
                .Add(new MeshRingsDataProcessingSystem<ItemFormingState>(dependencies.SculptingData))
                .Add(new MeshUpdateSystem<ItemFormingState, SculptingProductMesherTag>(dependencies.SculptingData))

                .OneFrame<RestartCurrentStepMessage>()
                .OneFrame<NextStateSignal>()
                .OneFrame<SkipStageMessage>()

                .OneFrame<ValueRegulatorChangedSignal>()
                .OneFrame<VisibilitySignal>()
                ;

            return systems;
        }
    }
}
