using Leopotam.Ecs;
using Modules.StateGroup.Components;
using PWS.Common.Messages;
using PWS.Common.UI;
using PWS.Pottery.ItemDataHolderService;
using UnityEngine;

namespace PWS.Pottery.Stages.ItemFormingStage
{
    public class ItemFormingCompletionTracker : IEcsRunSystem
    {
        // auto injected fields
        readonly EcsFilter<StateEnter, ItemFormingState> _entering;
        readonly EcsFilter<ItemFormingState>.Exclude<StateExit> _inState;
        private readonly EcsFilter<SkipStageMessage> _skipPerformed;

        readonly EcsFilter<StateWindow, StateProgress> _stateWindowWithProgress;

        private ItemFormingStateConfig _config;
        private ISculptingDataHolder _referenceData;

        //runtime data
        private ISculptingDataHolder _data;
        private float[] InitDistances;

        public ItemFormingCompletionTracker(ISculptingDataHolder data, ISculptingDataHolder referenceData, ItemFormingStateConfig config)
        {
            _data = data;
            _referenceData = referenceData;
            _config = config;
        }

        public void Run()
        {
            if (!_entering.IsEmpty())
            {
                InitDistances = new float[_data.Rings.Count];
                for (int i = 0; i < InitDistances.Length; i++)
                {
                    int refId = Mathf.Min(i, _referenceData.Rings.Count - 1);

                    InitDistances[i] = Mathf.Abs(_referenceData.Rings[refId].Position.x - _data.Rings[i].Position.x);
                }

                foreach (var i in _stateWindowWithProgress)
                {
                    _stateWindowWithProgress.Get2(i).Value = 0;
                    _stateWindowWithProgress.Get2(i).CompletionThreshold = _config.CompletionThresold;
                    _stateWindowWithProgress.Get2(i).SkipThreshold = _config.SkipThreshold;
                }
            }

            if (!_inState.IsEmpty())
            {
                if (!_skipPerformed.IsEmpty())
                    return;

                float fullCompletion = 0;
                for (int i = 0; i < InitDistances.Length; i++)
                {
                    int refId = Mathf.Min(i, _referenceData.Rings.Count - 1);

                    float currentDistance = Mathf.Abs(_referenceData.Rings[refId].Position.x - _data.Rings[i].Position.x);
                    float completion = 1 - Mathf.Clamp01(currentDistance / InitDistances[i]);

                    fullCompletion += completion;
                }
                fullCompletion /= InitDistances.Length;
                fullCompletion = Mathf.Clamp01(fullCompletion / _config.Difficulty);

                foreach (var i in _stateWindowWithProgress)
                {
                    _stateWindowWithProgress.Get2(i).Value = fullCompletion;
                }
            }
        }
    }
}
