using Leopotam.Ecs;
using Modules.StateGroup.Core;
using PWS.Common.Messages;
using PWS.Pottery.Common;
using PWS.Pottery.ItemDataHolderService;

namespace PWS.Pottery.Stages.ItemFormingStage
{
    public class ItemFormingSkipProcessing : IEcsRunSystem
    {
        // auto injected fields
        private readonly EcsFilter<SkipStageMessage> _skipPerformed;
        readonly EcsFilter<ItemFormingState> _inState;
        private readonly StateFactory _stateFactory;

        private ISculptingDataHolder _referenceData;
        private SetStateSOCommand _nextState;

        //runtime data
        private ISculptingDataHolder _data;

        public ItemFormingSkipProcessing(ISculptingDataHolder data, ISculptingDataHolder referenceData, SetStateSOCommand nextState)
        {
            _data = data;
            _referenceData = referenceData;
            _nextState = nextState;
        }

        public void Run()
        {
            if (_inState.IsEmpty())
                return;
            if (_skipPerformed.IsEmpty())
                return;

            _data.Rings.Clear();
            foreach (Ring ring in _referenceData.Rings)
            {
                _data.Rings.Add(new Ring(ring.Position, ring.Width));
            }
            _data.SetDirty();

            /*foreach (var i in _skipPerformed)
            {
                _skipPerformed.GetEntity(i).Del<SkipStageMessage>();
            }*/

            _nextState.Execute(_stateFactory);
        }
    }
}
