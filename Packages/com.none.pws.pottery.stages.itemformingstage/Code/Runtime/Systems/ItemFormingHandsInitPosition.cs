using Leopotam.Ecs;
using Modules.StateGroup.Components;
using Modules.ViewHub;
using PWS.Pottery.ItemDataHolderService;
using PWS.Pottery.MeshGeneration;
using PWS.Pottery.Sculpting.Interactor;
using UnityEngine;

namespace PWS.Pottery.Stages.ItemFormingStage
{
    public class ItemFormingHandsInitPosition : IEcsRunSystem
    {

        // auto injected fields
        readonly EcsFilter<StateEnter, ItemFormingState> _entering;

        readonly EcsFilter<UnityView, HandTag> _hands;
        readonly EcsFilter<UnityView, SculptingProductTag> _product;

        private ItemFormingStateConfig _config;

        //runtime data
        private ISculptingDataHolder _data;

        public ItemFormingHandsInitPosition(ISculptingDataHolder data, ItemFormingStateConfig config)
        {
            _data = data;
            _config = config;
        }

        public void Run()
        {
            if (!_entering.IsEmpty())
            {
                foreach (var product in _product)
                {
                    foreach (var idx in _hands)
                    {
                        ref var handsView = ref _hands.Get1(idx);
                        ref var handsTag = ref _hands.Get2(idx);
                        handsView.Transform.localPosition = Vector3.zero;
                        var pos = handsTag.View.ItemFormingHandVisual.transform.position;
                        //float height = _product.Get1(product).Transform.position.y + _data.GeneralParameters.FullHeight;
                        float height = _product.Get1(product).Transform.position.y;
                        pos.y = height;
                        handsView.Transform.position = pos;
                        handsTag.View.ItemFormingHandVisual.transform.position = pos;
                        handsTag.OriginLocalPosition = handsView.Transform.localPosition;
                    }
                }
            }
        }
    }
}