using Leopotam.Ecs;
using Modules.ViewHub;
using PWS.Pottery.ItemDataHolderService;
using PWS.Pottery.MeshGeneration;
using PWS.Pottery.Sculpting.Interactor;
using UnityEngine;

namespace PWS.Pottery.Stages.ItemFormingStage
{
    public class ItemFormingInteractorsHandler : IEcsRunSystem
    {

        // auto injected fields
        readonly EcsFilter<ItemFormingState> _inState;

        readonly EcsFilter<UnityView, SculptingInteractorTag> _generalInteractor;
        readonly EcsFilter<UnityView, SculptingProductTag> _product;

        private ItemFormingStateConfig _config;

        //runtime data
        private ISculptingDataHolder _data;
        private ISculptingDataHolder _referenceData;
        private Vector3 _oldHand;

        public ItemFormingInteractorsHandler(ISculptingDataHolder data, ISculptingDataHolder referenceData, ItemFormingStateConfig config)
        {
            _data = data;
            _referenceData = referenceData;
            _config = config;
        }

        public void Run()
        {
            if (_inState.IsEmpty())
                return;

            foreach (var productIdx in _product)
            {
                Transform product = _product.Get1(productIdx).Transform;
                foreach (var interactorIdx in _generalInteractor)
                {
                    Interaction(_generalInteractor.Get2(interactorIdx).Position, _product.Get1(productIdx).Transform.position);
                }
            }
        }

        void Interaction(Vector3 handPosition, Vector3 productPosition)
        {
            Vector2 moveVec = handPosition - _oldHand;
            //Debug.Log(moveVec);
            float moveDirV = moveVec.y;
            float moveDirVSign = Mathf.Sign(moveDirV);
            if (moveDirV == 0)
                moveDirVSign = 0;
            _oldHand = handPosition;

            float valueByHeight = 0;
            if (_referenceData.GeneralParameters.FullHeight > _data.DefaultParameters.Height)
            {
                float itemHeight = _data.GeneralParameters.FullHeight - _data.DefaultParameters.Height;
                float refItemHeight = _referenceData.GeneralParameters.FullHeight - _data.DefaultParameters.Height;
                valueByHeight = Mathf.Clamp01(itemHeight / refItemHeight);
            }

            float itemRadius = _data.Rings[_data.Rings.Count - 1].Position.x - _data.DefaultParameters.Radius;
            float refItemRadius = _referenceData.Rings[_referenceData.Rings.Count - 1].Position.x - _data.DefaultParameters.Radius;
            float valueByRadius = Mathf.Clamp01(itemRadius / refItemRadius);
            float wallWidth = Mathf.Lerp(_data.DefaultParameters.WallWidthMax, _data.DefaultParameters.WallWidthMin, Mathf.Max(valueByHeight, valueByRadius));
            //wallWidth = Mathf.Min(wallWidth, _data.Rings[_data.Rings.Count - 1].Width);
            
            foreach (Ring ring in _data.Rings)
            {
                Vector2 localHandPosition = handPosition - productPosition;
                Vector2 vectorToRing = ring.Position - localHandPosition;
                float dirToRing = Mathf.Sign(vectorToRing.y);
                float verticalDistance = Mathf.Abs(vectorToRing.y);
                float radius = _config.HandRadius;
                if (dirToRing == moveDirVSign && verticalDistance <= radius)
                {
                    float multiplier = 1 - verticalDistance / radius;
                    multiplier = _config.HandInteractionCurve.Evaluate(multiplier);

                    float offsetX = moveVec.normalized.x * 0.1f;
                    float offset = Mathf.Lerp(ring.Position.x, Mathf.Abs(localHandPosition.x), multiplier);
                    ring.SetOffset(Mathf.Max(ring.Width, offset));
                }

                ring.Width = wallWidth;
            }
            _data.SetDirty();
        }

        float HorizontalDistance(Vector3 vectorToRing)
        {
            return Mathf.Max(Mathf.Abs(vectorToRing.x), Mathf.Abs(vectorToRing.z));
        }
    }
}