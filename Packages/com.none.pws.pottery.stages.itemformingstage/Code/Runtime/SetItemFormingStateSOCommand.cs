using Modules.StateGroup.Core;
using UnityEngine;

namespace PWS.Pottery.Stages.ItemFormingStage
{
    [CreateAssetMenu(menuName = "PWS/Pottery/Stages/ItemFormingStage/SetStateSOCommand")]
    public class SetItemFormingStateSOCommand : SetStateSOCommand<ItemFormingState>
    {

    }
}