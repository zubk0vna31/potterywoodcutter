using Modules.StateGroup.Core;
using UnityEngine;

namespace PWS.WoodCutting.Stages.Toning
{
    [CreateAssetMenu(menuName = "PWS/WoodCutting/Stages/Toning/SetStateSOCommand")]

    public class SetToningStateSOCommand : SetStateSOCommand<ToningState>
    {
    }
}
