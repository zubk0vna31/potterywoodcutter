using PWS.WoodCutting.Common;
using UnityEngine;

namespace PWS.WoodCutting.Stages.Toning
{
    [CreateAssetMenu(fileName = "ToningStateConfig",
        menuName = "PWS/WoodCutting/Stages/Toning/Config")]
    public class ToningStateConfig : StateConfig
    {
        [Range(0f, 1f)]
        public float skipThreshold = 0.5f;
        [Range(0.1f, 1f)]
        public float paintableUpdateInterval = 0.5f;
        [Range(0.1f, 1f)]
        public float paintAllDuration = 0.5f;

        [Range(0.0f, 5f)]
        public float finalObjectRotationDuration = 1f;
    }
}
