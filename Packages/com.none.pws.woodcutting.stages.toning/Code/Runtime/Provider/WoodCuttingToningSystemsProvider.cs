using Leopotam.Ecs;
using Modules.Root.ContainerComponentModel;
using Modules.Root.ECS;
using PWS.Common.Audio;
using PWS.Common.GameModeInfoService;
using PWS.Common.ResultsEvaluation;
using PWS.Common.UI;
using PWS.WoodCutting.Common;
using UnityEngine;
using PWS.Common.TravelPoints;


namespace PWS.WoodCutting.Stages.Toning
{
    internal class StateDependecies
    {
        [Inject] public IGameModeInfoService gameModeInfoService;
        [Inject] public IResultsEvaluationDataHolder resultsEvaluation;
    }

    public class WoodCuttingToningSystemsProvider : MonoBehaviour, ISystemsProvider
    {
        [Header("Core")]
        [SerializeField]
        private StateConfig _stateConfig;
        [SerializeField]
        private StateInfoData _stateInfo;
        [SerializeField, Range(0.001f, 2f)]
        private float _uvMargin = 0.15f;
        [SerializeField, Range(0.001f, 0.5f)]
        private float _stickyZoneMainSqrDst = 0.15f;
        [SerializeField, Range(0.001f, 0.5f)]
        private float _stickyZoneSubSqrDst = 0.15f;

        [Header("Scene Related")]
        [SerializeField]
        private GameObject _selectMenu;
        [SerializeField]
        private RollerTool _rollerTool;


        [Header("Voice config")]
        [SerializeField] private VoiceConfig _voiceConfig;

        public EcsSystems GetSystems(EcsWorld world, EcsSystems endFrame, EcsSystems mainSystems)
        {
            StateDependecies dependecies = new StateDependecies();
            SceneContainer.Instance.Inject(dependecies);


            EcsSystems systems = new EcsSystems(world, "WoodCutting Toning");

            systems

                .Add(new SetStateDelayedSystem<ToningState>())

                .Add(new OutlineByStateSystem<ToningState>(_stateConfig))


                .Add(new SubstateSystem<ToningState,ToningSubstates,ToningStateConfig>(_stateConfig,
                ToningSubstates.SelectColor,
                ToningSubstates.Transition,
                ToningSubstates.None))

                .Add(new ToningSelectColorSubstateSystem(_stateConfig, ToningSubstates.SelectColor,_selectMenu))
                .Add(new ToningTopSubstateSystem(_stateConfig, ToningSubstates.Top,_rollerTool, dependecies.gameModeInfoService,
                _uvMargin, _stickyZoneMainSqrDst, _stickyZoneSubSqrDst))
                .Add(new ToningBotSubstateSystem(_stateConfig, ToningSubstates.Bot, dependecies.gameModeInfoService, _uvMargin))

                .Add(new ToningStatePaintableUpdateSystem(_stateConfig))

                //Tracker
                .Add(new ToningStateTrackerSystem(_stateConfig))

                .Add(new WoodCuttingSetGradeByProgress<ToningState>(dependecies.resultsEvaluation,
                _stateInfo))

                .Add(new StateInfoWindowUIProcessing<ToningState>(_stateInfo))
                .Add(new StateWindowPlacementProcessing<ToningState>("Main"))

                .Add(new TravelPointPositioningSystem<ToningState>("Default"))


                .Add(new SubstateUIProcessingSystem<ToningState,ToningSubstates>(ToningSubstates.Top))
                .Add(new SubstateUIProcessingSystem<ToningState,ToningSubstates>(ToningSubstates.Bot))

                .Add(new SubstateButtonUIProcessing<ToningState,ToningSubstates>(ToningSubstates.Top))
                .Add(new SubstateButtonUIProcessing<ToningState,ToningSubstates>(ToningSubstates.Bot))

                //Voice
                .Add(new VoiceAudioSystem<ToningState>(_voiceConfig))

                ;


            return systems;
        }
    }
}
