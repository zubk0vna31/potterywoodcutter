using DG.Tweening;
using Leopotam.Ecs;
using PWS.Common.GameModeInfoService;
using PWS.Common.UI;
using PWS.Pottery.Stages.DecorativePainting;
using PWS.WoodCutting.Common;
using UnityEngine;

namespace PWS.WoodCutting.Stages.Toning
{
    public class ToningTopSubstateSystem : RunSubstateSystem<ToningState, ToningSubstates, ToningStateConfig>
    {
        private readonly EcsFilter<NextStateSignal> _nextStateSignal;
        private readonly EcsFilter<StateProgress> _stateProgress;
        private readonly EcsFilter<ColorMenuItemSelected> _colorMenu;

        private readonly EcsFilter<PlankFinal> _planks;

        private readonly IGameModeInfoService _gameModeInfoService;

        private readonly RollerTool _rollerTool;
        private readonly float _uvMargin;
        private readonly float _stickyZoneMainSqrDst, _stickyZoneSubSqrDst;



        public ToningTopSubstateSystem(StateConfig stateConfig, ToningSubstates currentSubstate,
            RollerTool rollerTool, IGameModeInfoService gameModeInfoService,
            float uvMargin, float stickyZoneMainSqrDst, float stickyZoneSubSqrDst) : base(stateConfig, currentSubstate)
        {
            _rollerTool = rollerTool;
            _gameModeInfoService = gameModeInfoService;
            _uvMargin = uvMargin;
            _stickyZoneMainSqrDst = stickyZoneMainSqrDst;
            _stickyZoneSubSqrDst = stickyZoneSubSqrDst;
        }




        protected override void OnSubstateEnter()
        {
            base.OnSubstateEnter();

            Color paintColor = Color.white;

            foreach (var i in _colorMenu)
            {
                paintColor = _colorMenu.Get1(i).Color;
            }

            foreach (var i in _planks)
            {
                ref var plankFinal = ref _planks.Get1(i);

                plankFinal.toningPaintable.InitializeCustomRenderer(plankFinal.Last.renderer,
                plankFinal.Last.uvArea, _config.skipThreshold, true, _uvMargin);
                plankFinal.toningPaintable.SetPaintedColor(paintColor);
                plankFinal.view.StickyObject.Toggle(true);
                plankFinal.view.StickyObject.ChangeSqrDst(plankFinal.pattern.main ? _stickyZoneMainSqrDst : _stickyZoneSubSqrDst);
            }

            foreach (var i in _stateProgress)
            {
                _stateProgress.Get1(i).SkipThreshold = _gameModeInfoService.CurrentMode.Equals(GameMode.Free) ?
                    0.0f : _config.skipThreshold * 0.5f;
                _stateProgress.GetEntity(i).Del<Ignore>();
            }
        }

        protected override void OnSubstateUpdate()
        {
            base.OnSubstateUpdate();
            OnNextSubstateSignal();
        }

        private void OnNextSubstateSignal()
        {
            if (_nextStateSignal.IsEmpty()) return;

            foreach (var i in _planks)
            {
                if (!_planks.Get1(i).IsEmpty)
                {
                    _planks.Get1(i).Complete();
                    _planks.Get1(i).view.StickyObject.Toggle(false);
                    _planks.Get1(i).view.ChamferedFinalObject
                        .DOLocalRotate(Vector3.right * 180f, _config.finalObjectRotationDuration)
                        .SetRelative(true);
                }
            }

            foreach (var i in _stateProgress)
            {
                _stateProgress.Get1(i).SkipThreshold = _gameModeInfoService.CurrentMode.Equals(GameMode.Free) ?
                        0.0f : Mathf.Clamp(
                            _stateProgress.Get1(i).Value + _config.skipThreshold * 0.5f, 0f, _config.skipThreshold);
            }

            foreach (var i in _substate)
            {
                ref var signal = ref _substate.GetEntity(i).Get<ChangeSubstateSignal<ToningSubstates>>();
                signal.nextSubstate = ToningSubstates.Bot;
                signal.delay = _config.finalObjectRotationDuration;
            }
        }
    }
}
