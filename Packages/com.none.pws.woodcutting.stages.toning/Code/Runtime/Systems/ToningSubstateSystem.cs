using Leopotam.Ecs;
using Modules.Utils;
using PWS.WoodCutting.Common;
using PWS.WoodCutting.MarkupPainting;

namespace PWS.WoodCutting.Stages.Toning
{
    public enum ToningSubstates
    {
        None=0,
        Transition,
        SelectColor,
        Top,
        Bot,
    }

    public class ToningSubstateSystem : RunSystem<ToningState, ToningStateConfig>
    {
        // auto-injected fields
        private readonly EcsWorld _ecsWorld;
        private readonly EcsFilter<ChangeSubstateSignal<ToningSubstates>> _changeSubstateSignal;
        private readonly TimeService _timeService;
        private readonly ToningSubstates initialSubstate = ToningSubstates.SelectColor;

        // runtime data
        private EcsEntity _substateEntity;
        private float _timer;
        private bool _transitionSubstateEntered;

        public ToningSubstateSystem(StateConfig stateConfig) : base(stateConfig)
        {

        }

        protected override void OnStateEnter()
        {
            base.OnStateEnter();

            _substateEntity = _ecsWorld.NewEntity();
            _substateEntity.Get<Substate<ToningSubstates>>().substate = initialSubstate;
            _substateEntity.Get<Substate<ToningSubstates>>().nextSubstate = ToningSubstates.None;
            _substateEntity.Get<SubstateEnter>();

        }

        protected override void OnStateUpdate()
        {
            base.OnStateUpdate();

            if (_timer <= 0.0f)
            {
                ChangeSubstate();
            }
            else
            {
                if (!_transitionSubstateEntered)
                {
                    _transitionSubstateEntered = true;
                    _substateEntity.Get<Substate<ToningSubstates>>().substate = ToningSubstates.Transition;
                }

                _timer -= _timeService.DeltaTime;

                if (_timer <= 0.0f)
                {
                    ChangeSubstate();
                }
            }

            if (_changeSubstateSignal.IsEmpty()) return;


            foreach (var i in _changeSubstateSignal)
            {
                _timer = _changeSubstateSignal.Get1(i).delay;

                _substateEntity.Get<SubstateExit>();
                _substateEntity.Get<Substate<ToningSubstates>>().nextSubstate = 
                    _changeSubstateSignal.Get1(i).nextSubstate;
                _changeSubstateSignal.GetEntity(i).Del<ChangeSubstateSignal<ToningSubstates>>();
            }
        }

        protected override void OnStateExit()
        {
            base.OnStateExit();


            _substateEntity.Destroy();
        }

        private void ChangeSubstate()
        {
            ref var substate = ref _substateEntity.Get<Substate<ToningSubstates>>();

            if (substate.nextSubstate != ToningSubstates.None)
            {
                substate.substate = substate.nextSubstate;
                _substateEntity.Get<SubstateEnter>();
                substate.nextSubstate = ToningSubstates.None;
                _transitionSubstateEntered = false;
            }
        }

    }
}
