using Leopotam.Ecs;
using PWS.Common.UI;
using PWS.WoodCutting.Common;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PWS.WoodCutting.Stages.Toning
{
    public class ToningSkipStateSystem : RunSystem<ToningState, ToningStateConfig>
    {
        private readonly EcsFilter<NextStateSignal> _nextStateSignal;

        private readonly EcsFilter<PlankFinal> _plankFinal;

        private readonly EcsWorld _ecsWorld;

        public ToningSkipStateSystem(StateConfig config) : base(config)
        {
        }

        protected override void OnStateUpdate()
        {
            base.OnStateUpdate();

            if (_nextStateSignal.IsEmpty()) return;

            //foreach (var i in _nextStateSignal)
            //{
            //    _nextStateSignal.Get1(i).button.gameObject.SetActive(false);
            //    _nextStateSignal.GetEntity(i).Get<Ignore>();
            //}

            //foreach (var i in _plankFinal)
            //{
            //    ref var plank = ref _plankFinal.Get1(i);
            //    plank.chamferPaintable.PaintAllWithDuration(_config.paintAllDuration);
            //    plank.chamferingPattern.SetAllLoopsTraveled();
            //    plank.view.ShowChamferedObject(_config.paintAllDuration);
            //}

            //var entity = _ecsWorld.NewEntity();
            //entity.Get<SetStateDelayed>().state = _config.nextState;
            //entity.Get<SetStateDelayed>().delay = _config.paintAllDuration;
        }
    }
}
