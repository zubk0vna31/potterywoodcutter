using Leopotam.Ecs;
using PWS.Common.UI;
using PWS.WoodCutting.Common;
using UnityEngine;

namespace PWS.WoodCutting.Stages.Toning
{
    public class ToningSelectColorSubstateSystem : RunSubstateSystem<ToningState, ToningSubstates, ToningStateConfig>
    {
        private readonly EcsFilter<NextStateSignal> _nextStateSignal;

        private readonly EcsFilter<PlankFinal> _planks;
        private readonly GameObject _selectMenu;

        public ToningSelectColorSubstateSystem(StateConfig stateConfig, ToningSubstates currentSubstate,
            GameObject selectMenu) : base(stateConfig, currentSubstate)
        {
            _selectMenu = selectMenu;

        }

        protected override void OnStateEnter()
        {
            base.OnStateEnter();

            foreach (var i in _planks)
            {
                ref var plankFinal = ref _planks.Get1(i);

                plankFinal.view.State9.gameObject.SetActive(false);
                plankFinal.view.State10.gameObject.SetActive(true);
            }
        }

        protected override  void OnSubstateEnter()
        {
            base.OnSubstateEnter();

            _selectMenu.gameObject.SetActive(true);
        }

        protected override void OnSubstateUpdate()
        {
            base.OnSubstateUpdate();
            OnNextSubstateSignal();
        }

        private void OnNextSubstateSignal()
        {
            if (_nextStateSignal.IsEmpty()) return;

            foreach (var i in _substate)
            {
                ref var signal = ref _substate.GetEntity(i).Get<ChangeSubstateSignal<ToningSubstates>>();
                signal.nextSubstate = ToningSubstates.Top;
            }

            _selectMenu.gameObject.SetActive(false);
        }
    }
}
