using Leopotam.Ecs;
using PWS.Common.GameModeInfoService;
using PWS.Common.UI;
using PWS.WoodCutting.Common;
using UnityEngine;

namespace PWS.WoodCutting.Stages.Toning
{
    public class ToningStateTrackerSystem : RunSystem<ToningState, ToningStateConfig>
    {
        private readonly EcsFilter<StateProgress> _stateProgress;
        private readonly EcsFilter<PlankFinal> _planks;

        public ToningStateTrackerSystem(StateConfig config) : base(config)
        {
        }

        protected override void OnStateEnter()
        {
            base.OnStateEnter();

            foreach (var i in _stateProgress)
            {
                ref var progress = ref _stateProgress.Get1(i);

                progress.Value = 0f;
                progress.CompletionThreshold = 2f;
            }
        }

        protected override void OnStateUpdate()
        {
            base.OnStateUpdate();

            foreach (var i in _stateProgress)
            {
                float value = 0f;

                foreach (var plank in _planks)
                {
                    value += _planks.Get1(plank).Percentage;
                }

                Debug.Log("HERE2");

                value = Mathf.Clamp01(value / (_planks.GetEntitiesCount()*2f));

                _stateProgress.Get1(i).Value = value;
            }
        }
    }
}
