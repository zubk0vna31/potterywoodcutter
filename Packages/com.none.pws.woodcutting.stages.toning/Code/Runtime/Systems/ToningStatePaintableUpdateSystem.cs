using Leopotam.Ecs;
using Modules.Utils;
using PWS.WoodCutting.Common;
using UnityEngine;

namespace PWS.WoodCutting.Stages.Toning
{
    public class ToningStatePaintableUpdateSystem : RunSystem<ToningState, ToningStateConfig>
    {
        private readonly EcsFilter<Substate<ToningSubstates>> _substate;
        private readonly EcsFilter<Substate<ToningSubstates>, SubstateEnter> _substateEnter;
        private readonly EcsFilter<PlankFinal> _planks;
        private readonly TimeService _timeService;

        private float _timer;

        public ToningStatePaintableUpdateSystem(StateConfig config) : base(config)
        {
        }

        protected override void OnStateEnter()
        {
            base.OnStateEnter();

        }

        protected override void OnStateUpdate()
        {
            base.OnStateUpdate();

            if (!InSubstate()) return;

            OnSubstateEnter();
            OnSubstate();
        }

        private bool InSubstate()
        {
            if (_substate.IsEmpty())
            {
                return false;
            }

            foreach (var i in _substate)
            {
                if (_substate.Get1(i).substate == ToningSubstates.SelectColor)
                {
                    return false;
                }
            }

            return true;
        }

        private void OnSubstateEnter()
        {
            if (_substateEnter.IsEmpty()) return;

            _timer = _config.paintableUpdateInterval;
        }

        private void OnSubstate()
        {
            if (_timer > 0)
            {
                _timer -= _timeService.DeltaTime;

                if (_timer <= 0f)
                {
                    foreach (var i in _planks)
                    {
                        _planks.Get1(i).toningPaintable.CalculatePercentage();
                    }

                    _timer = _config.paintableUpdateInterval;
                }
            }
        }
    }
}
