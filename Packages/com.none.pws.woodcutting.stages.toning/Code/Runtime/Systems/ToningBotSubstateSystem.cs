using Leopotam.Ecs;
using PWS.Common;
using PWS.Common.GameModeInfoService;
using PWS.Common.UI;
using PWS.Pottery.Stages.DecorativePainting;
using PWS.WoodCutting.Common;
using UnityEngine;

namespace PWS.WoodCutting.Stages.Toning
{
    public class ToningBotSubstateSystem : RunSubstateSystem<ToningState, ToningSubstates, ToningStateConfig>
    {
        private readonly EcsFilter<PlankFinal> _planks;
        private readonly EcsFilter<ColorMenuItemSelected> _colorMenu;

        private readonly EcsFilter<NextStateSignal> _nextStateSignal;
        private readonly EcsFilter<StateProgress> _stateProgress;

        private readonly IGameModeInfoService _gameModeInfoService;
        private readonly float _uvMargin;



        public ToningBotSubstateSystem(StateConfig stateConfig, ToningSubstates currentSubstate,
            IGameModeInfoService gameModeInfoService,float uvMargin) : base(stateConfig, currentSubstate)
        {
            _gameModeInfoService = gameModeInfoService;
            _uvMargin = uvMargin;
        }


        protected override void OnSubstateEnter()
        {
            base.OnSubstateEnter();


            Color paintColor = Color.white;

            foreach (var i in _colorMenu)
            {
                paintColor = _colorMenu.Get1(i).Color;
            }

            foreach (var i in _planks)
            {
                ref var plankFinal = ref _planks.Get1(i);

                plankFinal.toningPaintable.InitializeCustomRenderer(plankFinal.Last.renderer,
                plankFinal.Last.uvArea, _config.skipThreshold,true, _uvMargin);
                plankFinal.toningPaintable.SetPaintedColor(paintColor);

                plankFinal.view.StickyObject.Toggle(true);
            }
        }

        protected override void OnSubstateUpdate()
        {
            base.OnSubstateUpdate();
            OnNextStateSignal();
        }

        private void OnNextStateSignal()
        {
            if (_nextStateSignal.IsEmpty()) return;

            float grade = 0f;
            foreach (var i in _stateProgress)
            {
                grade = _stateProgress.Get1(i).ProgressValue;
            }
            _ecsWorld.NewEntity().Get<SetGradeSignal>().grade = grade;

            foreach (var i in _nextStateSignal)
            {
                _nextStateSignal.Get1(i).button.gameObject.SetActive(false);
                _nextStateSignal.GetEntity(i).Get<Ignore>();
            }


            foreach (var i in _planks)
            {
                if (!_planks.Get1(i).IsEmpty)
                {
                    _planks.Get1(i).Complete(_config.paintAllDuration);
                    _planks.Get1(i).view.StickyObject.Toggle(false);
                }
            }

            var entity = _ecsWorld.NewEntity();
            entity.Get<SetStateDelayed>().state = _config.nextState;
            entity.Get<SetStateDelayed>().delay = _config.paintAllDuration;
        }
    }
}

