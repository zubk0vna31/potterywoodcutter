using Leopotam.Ecs;
using Modules.ViewHub;
using PWS.Common.UI;
using UnityEngine;
using UnityEngine.UI;

namespace PWS.WoodCutting.Stages.Toning
{
    public class NextStateButtonView : ViewComponent
    {
        [SerializeField]
        private Button _button;

        private EcsEntity ecsEntity;

        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            this.ecsEntity = ecsEntity;

            _button.onClick.AddListener(ButtonPressed);
        }

        private void ButtonPressed()
        {
            ecsEntity.Get<NextStateSignal>().button=_button;
        }
    }
}
