#if UNITY_EDITOR
using PWS.Pottery.ItemDataHolderService;
using UnityEditor;
using UnityEngine;

namespace PWS.Pottery.MeshGeneration
{
    public class SculptingTemplateDataCreator : MonoBehaviour
    {
        [ContextMenu("Create")]
        void Create()
        {
            SculptingMesher mesher = GetComponent<SculptingMesher>();

            SculptingTemplateData asset = ScriptableObject.CreateInstance<SculptingTemplateData>();
            asset.Data = new SculptingDataHolder();
            asset.Data.CopyFrom(mesher.Data);

            AssetDatabase.CreateAsset(asset, "Assets/Data/Pottery/SculptingTemplateData.asset");
            AssetDatabase.SaveAssets();

        }
    }
}
#endif