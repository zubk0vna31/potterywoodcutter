using UnityEngine;

namespace PWS.Pottery.MeshGeneration
{
    public class SculptingLineVisualizer : SculptingVisualizer
    {
        private LineRenderer _lineRenderer;

        public override void Init()
        {
            base.Init();

            _lineRenderer = GetComponent<LineRenderer>();
        }

        public override void Show()
        {
            if (!_lineRenderer.enabled)
                _lineRenderer.enabled = true;
        }
        public override void Hide()
        {
            if (_lineRenderer.enabled)
            {
                _lineRenderer.enabled = false;
            }
        }

        public override void VisualizeSculpting()
        {
            Vector3[] points = new Vector3[Data.MeshRings.Count * 2 - 2];

            float balanceAngle = (360 - MeshVisualizeAngleRange) / 2;
            Quaternion rotation = Quaternion.Euler(0, balanceAngle + FacingRotation, 0);
            for (int i = 0; i < Data.MeshRings.Count; i++)
            {
                points[i] = rotation * Data.MeshRings[i].Position;
            }

            rotation = Quaternion.Euler(0, -balanceAngle + FacingRotation, 0);
            for (int i = 0; i < Data.MeshRings.Count - 2; i++)
            {
                int idRing = Data.MeshRings.Count - (i + 1);
                int idArray = i + Data.MeshRings.Count;
                points[idArray] = rotation * Data.MeshRings[idRing].Position;
            }

            _lineRenderer.positionCount = points.Length;
            _lineRenderer.SetPositions(points);
        }
    }
}
