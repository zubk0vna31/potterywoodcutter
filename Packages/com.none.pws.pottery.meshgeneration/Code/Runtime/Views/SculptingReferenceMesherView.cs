using Leopotam.Ecs;
using Modules.ViewHub;

namespace PWS.Pottery.MeshGeneration
{
    public class SculptingReferenceMesherView : ViewComponent
    {
        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            ecsEntity.Get<SculptingReferenceMesherTag>();
        }
    }
}
