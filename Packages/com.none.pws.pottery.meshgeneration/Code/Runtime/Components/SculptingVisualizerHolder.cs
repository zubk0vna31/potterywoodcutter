using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PWS.Pottery.MeshGeneration
{
    public struct SculptingVisualizerHolder
    {
        public SculptingVisualizer SculptingVisualizer;
    }
}
