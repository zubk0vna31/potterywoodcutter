﻿using PWS.Pottery.ItemDataHolderService;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace PWS.Pottery.MeshGeneration
{
    public class SculptingMesher : SculptingVisualizer
    {
        public UnityEvent OnVisualUpdated;

        private List<Vector3> _vertices = new List<Vector3>();
        private List<Vector3> _normals = new List<Vector3>();
        private List<Vector2> _uvs = new List<Vector2>();
        private List<int> _triangles = new List<int>();
        private List<Color> _colors = new List<Color>();

        private MeshFilter _meshFilter;
        private MeshRenderer _meshRenderer;
        private MeshCollider _meshCollider;
        private Mesh _mesh;

        public override void Init()
        {
            base.Init();

            _meshRenderer = GetComponent<MeshRenderer>();
            _meshCollider = GetComponent<MeshCollider>();
            _meshFilter = GetComponent<MeshFilter>();
            _mesh = new Mesh();
            _mesh.MarkDynamic();
            _meshFilter.mesh = _mesh;
            _meshCollider.sharedMesh = _mesh;
        }

        public override void Show()
        {
            if (!_meshRenderer.enabled)
                _meshRenderer.enabled = true;
        }

        public override void Hide()
        {
            if (_meshRenderer.enabled)
                _meshRenderer.enabled = false;
        }

        public override void VisualizeSculpting()
        {
            int realVertices = Data.SmoothParamaters.Vertices + 1;
            int verticesCount = Data.MeshRings.Count * realVertices;

            _vertices.Clear();
            _normals.Clear();
            _uvs.Clear();
            _triangles.Clear();
            _colors.Clear();

            float angleRangeScale = MeshVisualizeAngleRange / 360;
            float angleRangeOffset = (360 - angleRangeScale) / 2;

            for (int i = 0; i < Data.MeshRings.Count; i++)
            {
                Vector2 normal = CalculateNormal(i);

                int ring = _vertices.Count;

                int nextMeshRingId = (i + 1) % Data.MeshRings.Count;
                MeshRing nextMeshRing = Data.MeshRings[nextMeshRingId];

                float uvOffset = CalculateUvOffset(i);

                for (int j = 0; j < realVertices; j++)
                {
                    _vertices.Add(GetPosition(Data.MeshRings[i], j));
                    _normals.Add(GetNormal(normal, j));
                    _colors.Add(Data.MeshRings[i].Color);

                    float uvY = Data.MeshRings[i].Position.y;
                    uvY += uvOffset;

                    _uvs.Add(new Vector2((float)j / Data.SmoothParamaters.Vertices * angleRangeScale + angleRangeOffset + FacingRotation, uvY));

                    if (j >= Data.SmoothParamaters.Vertices)
                        continue;

                    if (Data.MeshRings[i].Type == RingType.ClosingPoint)
                        continue;

                    if (Data.MeshRings[i].Type == RingType.OpeningPoint && nextMeshRing.Type != RingType.Default)
                        continue;

                    int outA = ring + j;
                    int outB = ring + (j + 1);
                    int outC = ring + j + realVertices;
                    int outD = ring + (j + 1) + realVertices;

                    outA %= verticesCount;
                    outB %= verticesCount;
                    outC %= verticesCount;
                    outD %= verticesCount;

                    _triangles.Add(outA);
                    _triangles.Add(outB);
                    _triangles.Add(outC);

                    _triangles.Add(outB);
                    _triangles.Add(outD);
                    _triangles.Add(outC);
                }
            }

            _mesh.Clear();
            _mesh.vertices = _vertices.ToArray();
            _mesh.normals = _normals.ToArray();
            _mesh.uv = _uvs.ToArray();
            _mesh.triangles = _triangles.ToArray();
            _mesh.colors = _colors.ToArray();
            _meshFilter.mesh = _mesh;
            _meshCollider.sharedMesh = _mesh;

            OnVisualUpdated.Invoke();
        }

        Vector3 GetPosition(MeshRing meshRing, int i)
        {
            float angleStep = MeshVisualizeAngleRange / Data.SmoothParamaters.Vertices;
            float balanceAngle = (360 - MeshVisualizeAngleRange) / 2;

            Quaternion rotation = Quaternion.Euler(0, angleStep * i + balanceAngle + FacingRotation, 0);
            return rotation * meshRing.Position;
        }

        Vector3 GetNormal(Vector2 normal, int i)
        {
            float angleStep = MeshVisualizeAngleRange / Data.SmoothParamaters.Vertices;
            float balanceAngle = (360 - MeshVisualizeAngleRange) / 2;

            Quaternion rotation = Quaternion.Euler(0, angleStep * i + balanceAngle + FacingRotation, 0);
            return rotation * normal;
        }

        Vector2 CalculateNormal(int i)
        {
            if (Data.MeshRings[i].Type == RingType.OpeningPoint)
                return -Vector2.up;
            else if (Data.MeshRings[i].Type == RingType.ClosingPoint)
                return Vector2.up;
            else
            {
                int next = (int)Mathf.Repeat(i + 1, Data.MeshRings.Count);
                int prev = (int)Mathf.Repeat(i - 1, Data.MeshRings.Count);

                Vector2 vectorToNext = (Data.MeshRings[next].Position - Data.MeshRings[i].Position);
                Vector2 vectorToPrev = (Data.MeshRings[prev].Position - Data.MeshRings[i].Position);

                Vector2 vectorToNextPerp = -Vector2.Perpendicular(vectorToNext);
                Vector2 vectorToPrevPerp = Vector2.Perpendicular(vectorToPrev);
                return (vectorToNextPerp + vectorToPrevPerp) / 2;
            }
        }

        float CalculateUvOffset(int i)
        {
            float offset = 0;

            if (Data.MeshRings[i].Type != RingType.Default)
                return offset;

            int next = (int)Mathf.Repeat(i + 1, Data.MeshRings.Count);
            int prev = (int)Mathf.Repeat(i - 1, Data.MeshRings.Count);

            float uvClosureOffset = Mathf.Max(Data.MeshRings[next].UvClosureOffset, Data.MeshRings[prev].UvClosureOffset);
            if (uvClosureOffset > 0)
                offset = uvClosureOffset - Data.MeshRings[i].Position.x;

            return offset;
        }
    }
}