using Leopotam.Ecs;
using Modules.StateGroup.Components;
using DG.Tweening;
using PWS.Pottery.ItemDataHolderService;

namespace PWS.Pottery.MeshGeneration
{
    public class ProductMeshAngleRangeProcessing<StateT, SculptingType> : IEcsRunSystem where StateT : struct where SculptingType : struct
    {
        // auto injected fields
        readonly EcsFilter<StateEnter, StateT> _entering;
        readonly EcsFilter<StateExit, StateT> _exiting;
        readonly EcsFilter<SculptingVisualizerHolder, SculptingType> _mesher;

        private float _openMeshAngleRange;
        private float _duration;

        //runtime data
        private ISculptingDataHolder _data;

        public ProductMeshAngleRangeProcessing(float openMeshAngleRange, float duration, ISculptingDataHolder data)
        {
            _openMeshAngleRange = openMeshAngleRange;
            _duration = duration;
            _data = data;
        }

        public void Run()
        {
            if (!_entering.IsEmpty())
            {
                foreach (var idx in _mesher) {
                    float angle = _mesher.Get1(idx).SculptingVisualizer.MeshVisualizeAngleRange;
                    DOTween.To(() => angle, x => angle = x, _openMeshAngleRange, _duration)
                        .OnUpdate(() => {
                            _mesher.Get1(idx).SculptingVisualizer.MeshVisualizeAngleRange = angle;
                            _data.SetDirty();
                        });

                }
            }
        }
    }
}
