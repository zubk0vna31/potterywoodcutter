using Leopotam.Ecs;
using PWS.Pottery.ItemDataHolderService;
using UnityEngine;

namespace PWS.Pottery.MeshGeneration
{
    public class MeshUpdateSystem<StateT, SculptingType> : IEcsRunSystem where StateT : struct where SculptingType : struct
    {
        // auto injected fields
        readonly EcsFilter<StateT> _inState;
        readonly EcsFilter<SculptingVisualizerHolder, SculptingType> _sculptingMesherHolder;

        private ISculptingDataHolder _data;

        public MeshUpdateSystem(ISculptingDataHolder data)
        {
            _data = data;
        }

        public void Run()
        {
            if (_inState.IsEmpty())
                return;

            if (_data == null)
                return;

            Update();

        }

        void Update()
        {
            if (!_data.IsDataUpdated)
                return;
            if (!_data.IsRingsUpdated)
                return;

            foreach (var idx in _sculptingMesherHolder)
            {
                _sculptingMesherHolder.Get1(idx).SculptingVisualizer.UpdateMesh(_data);
            }

            _data.IsDataUpdated = false;
            _data.IsRingsUpdated = false;
        }
    }
}
