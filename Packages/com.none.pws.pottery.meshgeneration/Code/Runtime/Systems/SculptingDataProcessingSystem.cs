using Leopotam.Ecs;
using PWS.Pottery.ItemDataHolderService;
using UnityEngine;
using Modules.StateGroup.Components;

namespace PWS.Pottery.MeshGeneration
{
    public class SculptingDataProcessingSystem<StateT> : IEcsRunSystem where StateT : struct
    {
        // auto injected fields
        readonly EcsFilter<StateEnter, StateT> _entering;
        readonly EcsFilter<StateT> _inState;

        private ISculptingDataHolder _data;

        public SculptingDataProcessingSystem(ISculptingDataHolder data)
        {
            _data = data;
        }

        public void Run()
        {
            if (_inState.IsEmpty())
                return;

            if (_data == null)
                return;

            if (!_data.IsDataUpdated)
                return;

            CreateRings(_data);

            for (int i = 0; i < _data.Rings.Count; i++)
            {
                _data.Rings[i].TopWallRing = i == _data.Rings.Count - 1;
            }
        }

        void CreateRings(ISculptingDataHolder data)
        {
            float nextRingHeight = 0;
            int i = 0;

            Vector2 removedTopRingPosition = data.TopRing.Position;

            /*float valueByHeight = -(data.DefaultParameters.Height - data.GeneralParameters.FullHeight) * 10f;
            Debug.Log(valueByHeight);
            float wallWidthLerped = Mathf.Lerp(data.DefaultParameters.WallWidthMax, data.DefaultParameters.WallWidthMin, valueByHeight);
            float wallWidth = Mathf.Clamp01(wallWidthLerped);*/

            while (nextRingHeight < data.GeneralParameters.FullHeight || i < data.GetRingsCount())
            {
                if (i >= data.GetRingsCount())
                {
                    int prevId = data.GetRingsCount() - 1;
                    data.GetRing(prevId).SetHeight(nextRingHeight - data.DefaultParameters.RingsOffset);

                    data.Rings.Add(new Ring(new Vector2(data.GetRing(prevId).Position.x, nextRingHeight), data.Rings[i - 1].Width));
                }
                else
                {
                    if (data.GetRing(i).Position.y > data.GeneralParameters.FullHeight && data.GetRingsCount() > 2)
                    {
                        if (i == data.GetRingsCount() - 1)
                            removedTopRingPosition = data.TopRing.Position;

                        data.Rings.RemoveAt(i);
                        nextRingHeight -= data.DefaultParameters.RingsOffset;
                        i--;
                    }
                }
                nextRingHeight += data.DefaultParameters.RingsOffset;
                i++;

                //if (i < data.Rings.Count)
                //    data.Rings[i].Width = wallWidth;
            }

            if (removedTopRingPosition.y <= data.TopRing.Position.y)
            {
                data.TopRing.SetHeight(data.GeneralParameters.FullHeight);
                data.GeneralParameters.TopClosureHeight = Mathf.Min(data.GeneralParameters.TopClosureHeight, data.GeneralParameters.FullHeight);
            }
            else
            {
                Vector2 vectorToRemovedTopRing = removedTopRingPosition - data.TopRing.Position;
                float lerp = (data.GeneralParameters.FullHeight - data.TopRing.Position.y) / vectorToRemovedTopRing.y;
                Vector2 lerpPosition = Vector2.Lerp(data.TopRing.Position, removedTopRingPosition, lerp);
                data.TopRing.Position = lerpPosition;
                data.GeneralParameters.TopClosureHeight = Mathf.Min(data.GeneralParameters.TopClosureHeight, lerpPosition.y);
            }
        }
    }
}
