using Leopotam.Ecs;
using PWS.Pottery.ItemDataHolderService;
using UnityEngine;
using Modules.StateGroup.Components;

namespace PWS.Pottery.MeshGeneration
{
    public class MeshRingsDataProcessingSystem<StateT> : IEcsRunSystem where StateT : struct
    {
        // auto injected fields
        readonly EcsFilter<StateEnter, StateT> _entering;
        readonly EcsFilter<StateT> _inState;

        private ISculptingDataHolder _data;

        public MeshRingsDataProcessingSystem(ISculptingDataHolder data)
        {
            _data = data;
        }

        public void Run()
        {
            if (_inState.IsEmpty())
                return;

            if (_data == null)
                return;

            if (!_data.IsRingsUpdated)
                return;

            CreateMeshRings();

        }

        void CreateMeshRings()
        {
            _data.MeshRings.Clear();

            bool hasHole = _data.GeneralParameters.BottomClosureHeight > _data.GeneralParameters.TopClosureHeight;

            if (!hasHole)
            {
                //bottom central point
                AddMeshRing(new Vector2(_data.BottomRing.Position.x, _data.GeneralParameters.BottomClosureHeight), _data.BottomRing, RingType.OpeningPoint);
                //inside surface from central point height to bottom ring
                AddInsidePoints(true);

            }

            //bottom edge mesh
            AddClosureMeshRing(0, true);

            //outside surface
            AddOutsideRings();

            //top edge mesh
            AddClosureMeshRing(_data.GetRingsCount() - 1, false);

            if (!hasHole)
            {
                //inside surface from top ring to central point height
                AddInsidePoints(false);

                //top central point
                AddMeshRing(new Vector2(_data.TopRing.Position.x, _data.GeneralParameters.TopClosureHeight), _data.BottomRing, RingType.ClosingPoint);
            }

            if (hasHole)
            {
                int firstRing = 0;
                int lastRing = _data.GetRingsCount() - 1;
                for (int i = lastRing; i >= firstRing; i--)
                {
                    AddSurfaceMeshRing(i, false, firstRing, lastRing);
                }
            }

            SmoothMeshRings();
        }

        void AddOutsideRings()
        {
            int firstRing = 0;
            int lastRing = _data.GetRingsCount() - 1;
            for (int i = firstRing; i <= lastRing; i++)
            {
                AddSurfaceMeshRing(i, true, firstRing, lastRing);
            }
        }

        void AddInsidePoints(bool isBottom)
        {
            int firstRing = 0;
            int lastRing = _data.GetRingsCount() - 1;

            for (int i = lastRing; i >= firstRing; i--)
            {
                bool canAddRing = isBottom
                    ? _data.GetRing(i).Position.y + _data.DefaultParameters.RingsOffset < _data.GeneralParameters.BottomClosureHeight
                    : _data.GetRing(i).Position.y - _data.DefaultParameters.RingsOffset > _data.GeneralParameters.TopClosureHeight;
                if (canAddRing)
                {
                    AddSurfaceMeshRing(i, false, firstRing, lastRing);
                }

                if (!isBottom && i < lastRing)
                {
                    Vector2 position0 = _data.GetRing(i).Position;
                    Vector2 position1 = _data.GetRing(i + 1).Position;

                    if (_data.GeneralParameters.TopClosureHeight >= position0.y && _data.GeneralParameters.TopClosureHeight < position1.y)
                    {
                        Vector2 vectorToTopRing = position1 - position0;
                        float lerp = (_data.GeneralParameters.TopClosureHeight - position0.y) / vectorToTopRing.y;
                        Vector2 lerpPosition = Vector2.Lerp(position0, position1, lerp);
                        float lerpWidth = Mathf.Lerp(_data.GetRing(i).Width, _data.GetRing(i + 1).Width, lerp);
                        AddMeshRing(lerpPosition - Vector2.right * lerpWidth / 2, _data.GetRing(i));
                    }
                }
            }

        }

        private void AddClosureMeshRing(int ringId, bool isBottomClosure)
        {
            int bottomSign = isBottomClosure ? 1 : -1;
            float halfWidth = _data.GetRing(ringId).Width / 2;

            Vector2 curPosition = _data.GetRing(ringId).Position;
            Vector2 nextPosition = _data.GetRing(ringId + bottomSign).Position;
            Vector2 vectorToNextPoint = (nextPosition - curPosition).normalized;

            Vector2 perpendicularVectorToNextPoint = Vector2.Perpendicular(vectorToNextPoint);

            Vector2 surfacePoint0 = curPosition + perpendicularVectorToNextPoint * halfWidth;
            Vector2 surfacePoint1 = curPosition - perpendicularVectorToNextPoint * halfWidth;

            if (isBottomClosure && _data.GetRing(ringId).Position.y >= _data.GeneralParameters.BottomClosureHeight)
                surfacePoint0.y = surfacePoint1.y = _data.GeneralParameters.BottomClosureHeight;

            if (!isBottomClosure && _data.GetRing(ringId).Position.y <= _data.GeneralParameters.TopClosureHeight)
                surfacePoint0.y = surfacePoint1.y = _data.GeneralParameters.TopClosureHeight;

            AddMeshRing(surfacePoint0, _data.GetRing(ringId));
            AddMeshRing(surfacePoint1, _data.GetRing(ringId));

        }

        private void AddSurfaceMeshRing(int ringId, bool outside, int firstRing, int lastRing)
        {
            Vector2 intersection = FindIntersection(ringId, outside, firstRing, lastRing);
            if (intersection == Vector2.zero)
                return;

            AddMeshRing(intersection, _data.GetRing(ringId));
        }

        Vector2 FindIntersection(int ringId, bool outside, int firstRing, int lastRing)
        {
            int outsideSign = outside ? 1 : -1;
            float halfWidth = _data.GetRing(ringId).Width / 2;

            if (ringId > firstRing && ringId < lastRing)
            {
                int prev = ringId - outsideSign;
                int next = ringId + outsideSign;

                Vector2 vectorToPrevPoint = (_data.GetRing(prev).Position - _data.GetRing(ringId).Position).normalized;
                Vector2 vectorToNextPoint = (_data.GetRing(next).Position - _data.GetRing(ringId).Position).normalized;

                float angleBetweenVectors = Vector2.SignedAngle(vectorToPrevPoint, vectorToNextPoint);
                float angleBetweenVectorsAbs = Mathf.Abs(angleBetweenVectors);

                if (angleBetweenVectorsAbs != 180)
                {
                    Vector2 perpendicularVectorToPrevPoint = Vector2.Perpendicular(vectorToPrevPoint);
                    Vector2 perpendicularVectorToNextPoint = -Vector2.Perpendicular(vectorToNextPoint);

                    Vector2 surfacePointPrev = _data.GetRing(prev).Position + perpendicularVectorToPrevPoint * halfWidth;
                    Vector2 surfacePointNext = _data.GetRing(next).Position + perpendicularVectorToNextPoint * halfWidth;

                    Vector2 surfaceIntersectionPoint = VectorHelper.LineLineIntersection(surfacePointPrev, -vectorToPrevPoint * 100, surfacePointNext, -vectorToNextPoint * 100);

                    return surfaceIntersectionPoint;
                }
                else
                {
                    Vector2 position = _data.GetRing(ringId).Position + Vector2.right * halfWidth * outsideSign;

                    return position;
                }
            }
            else return Vector2.zero;

        }

        private void AddMeshRing(Vector2 point, Ring ring, RingType pointType = RingType.Default)
        {
            MeshRing meshRing = new MeshRing();
            meshRing.CreateRing(point, pointType, ring.Color);
            meshRing.TopWallRing = ring.TopWallRing;
            _data.MeshRings.Add(meshRing);
        }

        private void InsertMeshRing(int i, Vector2 point, Color color, bool topWallRing = false)
        {
            MeshRing meshRing = new MeshRing();
            meshRing.CreateRing(point, RingType.Default, color);
            meshRing.TopWallRing = topWallRing;
            _data.MeshRings.Insert(i, meshRing);
        }

        void RemoveMeshRing(int i)
        {
            _data.MeshRings.RemoveAt(i);
        }

        void SmoothMeshRings()
        {
            if (_data.MeshRings.Count < 1)
                return;
            if (_data.SmoothParamaters.Segments <= 0)
                return;

            int cur = 0;
            while (cur < _data.MeshRings.Count)
            {
                if (_data.MeshRings[cur].Type != RingType.Default)
                {
                    cur++;
                    continue;
                }

                int next = (int)Mathf.Repeat(cur + 1, _data.MeshRings.Count);
                int prev = (int)Mathf.Repeat(cur - 1, _data.MeshRings.Count);

                Vector2 position = _data.MeshRings[cur].Position;
                Vector2 vectorToNext = _data.MeshRings[next].Position - _data.MeshRings[cur].Position;
                Vector2 vectorToPrev = _data.MeshRings[prev].Position - _data.MeshRings[cur].Position;

                float angle = Vector2.SignedAngle(vectorToPrev, vectorToNext);
                float angleAbs = Mathf.Abs(angle);

                if (angleAbs >= _data.SmoothParamaters.AngleThreshold)
                {
                    Vector2 vectorToPrevLerp = vectorToPrev * _data.SmoothParamaters.LerpValue;
                    vectorToPrevLerp = Vector2.ClampMagnitude(vectorToPrevLerp, _data.SmoothParamaters.MaxOffset);
                    Vector2 vectorToNextLerp = vectorToNext * _data.SmoothParamaters.LerpValue;
                    vectorToNextLerp = Vector2.ClampMagnitude(vectorToNextLerp, _data.SmoothParamaters.MaxOffset);

                    Vector2 smoothVector = (vectorToPrevLerp + vectorToNextLerp) / 2;
                    Vector2 smoothPosition = position + smoothVector;
                    _data.MeshRings[cur].Position = smoothPosition;
                    cur++;
                }
                else
                {
                    MeshRing meshRing = _data.MeshRings[cur];
                    bool topWall = _data.MeshRings[cur].TopWallRing;
                    bool topWallNext = _data.MeshRings[next].TopWallRing;
                    bool topWallPrev = _data.MeshRings[prev].TopWallRing;
                    int topSeg = 0;
                    if (topWall && topWallNext)
                        topSeg = _data.SmoothParamaters.Segments;
                    else if (topWall && topWallPrev)
                        topSeg = 1;

                    RemoveMeshRing(cur);

                    int segments = _data.SmoothParamaters.Segments + 1;

                    for (int seg = 1; seg < segments; seg++)
                    {
                        float lerp = (float)seg / segments;
                        float lerpInverse = 1 - lerp;

                        Vector2 vectorToPrevLerp = vectorToPrev * _data.SmoothParamaters.LerpValue;
                        vectorToPrevLerp = Vector2.ClampMagnitude(vectorToPrevLerp, _data.SmoothParamaters.MaxOffset);
                        Vector2 vectorToNextLerp = vectorToNext * _data.SmoothParamaters.LerpValue;
                        vectorToNextLerp = Vector2.ClampMagnitude(vectorToNextLerp, _data.SmoothParamaters.MaxOffset);

                        Vector2 prevPosition = position + vectorToPrevLerp;
                        Vector2 nextPosition = position + vectorToNextLerp;

                        Vector2 position1 = Vector2.Lerp(prevPosition, position, lerp);
                        Vector2 position2 = Vector2.Lerp(position, nextPosition, lerp);
                        Vector2 smoothPosition = Vector2.Lerp(position1, position2, lerp);


                        InsertMeshRing(cur, smoothPosition, meshRing.Color, seg == topSeg);
                        cur++;
                    }
                }
            }
        }
    }
}
