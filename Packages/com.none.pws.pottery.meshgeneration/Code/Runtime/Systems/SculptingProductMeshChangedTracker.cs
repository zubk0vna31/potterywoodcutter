using Leopotam.Ecs;
using PWS.Pottery.ItemDataHolderService;

namespace PWS.Pottery.MeshGeneration
{
    public class SculptingProductMeshChangedTracker<StateT, SignalT> : IEcsRunSystem where StateT : struct where SignalT : struct
    {
        // auto injected fields
        readonly EcsFilter<StateT> _inState;
        readonly EcsFilter<SculptingProductTag> _sculptingProduct;

        private ISculptingDataHolder _data;

        public SculptingProductMeshChangedTracker(ISculptingDataHolder data)
        {
            _data = data;
        }

        public void Run()
        {
            if (!_inState.IsEmpty())
            {
                if (!_data.IsDataUpdated)
                    return;
                if (!_data.IsRingsUpdated)
                    return;

                foreach (var product in _sculptingProduct)
                {
                    _sculptingProduct.GetEntity(product).Get<SignalT>();
                }
            }
        }
    }
}
