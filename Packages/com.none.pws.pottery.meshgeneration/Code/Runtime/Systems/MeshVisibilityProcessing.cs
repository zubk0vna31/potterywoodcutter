using Leopotam.Ecs;
using Modules.StateGroup.Components;
using UnityEngine;

namespace PWS.Pottery.MeshGeneration
{
    public class MeshVisibilityProcessing<StateT, SculptingType> : IEcsRunSystem where StateT : struct where SculptingType : struct
    {
        // auto injected fields
        protected readonly EcsFilter<StateEnter, StateT> _entering;
        protected readonly EcsFilter<StateExit, StateT> _exiting;
        protected readonly EcsFilter<SculptingVisualizerHolder, SculptingType> _sculptingMesherHolder;

        public virtual void Run()
        {
            if (!_entering.IsEmpty())
            {
                foreach (var idx in _sculptingMesherHolder)
                {
                    _sculptingMesherHolder.Get1(idx).SculptingVisualizer.Show();
                }
            }
            if (!_exiting.IsEmpty())
            {
                foreach (var idx in _sculptingMesherHolder)
                {
                    _sculptingMesherHolder.Get1(idx).SculptingVisualizer.Hide();
                }
            }
        }
    }
}
