using Leopotam.Ecs;
using Modules.StateGroup.Components;

namespace PWS.Pottery.MeshGeneration
{
    public class MeshVisibilityDoubleStateProcessing<StateT, SculptingType, SecondStateT> : MeshVisibilityProcessing<StateT, SculptingType>
        where StateT : struct where SculptingType : struct where SecondStateT : struct
    {
        // auto injected fields
        readonly EcsFilter<StateEnter, SecondStateT> _enteringSecond;

        public override void Run()
        {
            if (!_exiting.IsEmpty() && !_enteringSecond.IsEmpty())
                return;

            base.Run();
        }
    }
}
