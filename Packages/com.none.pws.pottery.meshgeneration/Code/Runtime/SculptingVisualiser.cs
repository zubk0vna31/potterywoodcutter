﻿using PWS.Pottery.ItemDataHolderService;
using System.Collections.Generic;
using UnityEngine;

namespace PWS.Pottery.MeshGeneration
{
    public abstract class SculptingVisualizer : MonoBehaviour
    {
        private ISculptingDataHolder _data;
        public ISculptingDataHolder Data
        {
            get => _data;
        }

        public bool UpdateOnlyEnabled;
        public const float FacingRotation = -90;
        [Range(0, 360)]
        public float MeshVisualizeAngleRange = 360;
        
        [HideInInspector]
        public Transform _tr;

        void Awake()
        {
            Init();
        }

        public virtual void Init()
        {
            _tr = transform;
        }

        public virtual void UpdateMesh(ISculptingDataHolder data)
        {
            if (UpdateOnlyEnabled && !gameObject.activeInHierarchy) return;
            if (UpdateOnlyEnabled && !enabled) return;
            if (data == null) return;
            if (data.MeshRings.Count == 0) return;

            _data = data;
            VisualizeSculpting();
        }

        public abstract void VisualizeSculpting();

        public abstract void Show();
        public abstract void Hide();

    }
}