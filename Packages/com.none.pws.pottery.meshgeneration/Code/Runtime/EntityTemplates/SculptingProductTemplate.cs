using Leopotam.Ecs;
using Modules.ViewHub;
using UnityEngine;

namespace PWS.Pottery.MeshGeneration
{
    public class SculptingProductTemplate : EntityTemplate
    {
        protected override void SetupEntity(EcsEntity entity, EcsWorld world)
        {
            base.SetupEntity(entity, world);

            ref var sculptingMesherHolder = ref entity.Get<SculptingVisualizerHolder>();
            sculptingMesherHolder.SculptingVisualizer = GetComponent<SculptingVisualizer>();

        }
    }
}
