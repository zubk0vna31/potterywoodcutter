using PWS.WoodCutting.Common;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace PWS.WoodCutting
{
    public struct VaimRegular : ITask
    {
        public Rigidbody rigidbody;
        public XRGrabInteractable grabInteractable;
        public CircularGrabbable circularGrabbable;
        public AxisMover axisMover;
        public Animator animator;

        public Collider[] dissableCollider;

        public Vector3 initPosition, initRotation;

        public bool Completed()
        {
            return axisMover.enabled;
        }

        public void ResetUnityComponents()
        {
            //Reset animator
            animator.Rebind();

            //Enable all colliders
            foreach (var col in dissableCollider)
            {
                if (!col.enabled) col.enabled = true;
            }

            circularGrabbable.ResetComponent();
            axisMover.ResetComponent();

            circularGrabbable.enabled = false;
            axisMover.enabled = false;
        }
    }
}
