using UnityEngine;

namespace PWS.WoodCutting
{
    public struct NearGhost 
    {
        public Transform ghostTransform;
        public Collider ghostCollider;
    }
}
