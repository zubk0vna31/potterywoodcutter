using UnityEngine;

namespace PWS.WoodCutting
{
    public struct VaimGhost
    {
        public bool canStoreTarget;

        public MeshRenderer [] meshRenderer;
        public Collider collider;
        public Animator animator;
    }
}
