﻿using UnityEngine;

namespace PWS.WoodCutting
{
    public struct VaimSoundData
    {
        public CircularGrabbable Interactable;
        public float MinRequiredAngle;
        public float MaxRequiredAngle;
        public float StartAngle;
        public float RequiredAngle;
        public float CurrentAngle;
        public AudioClip[] TwistingClips;
        public AudioClip[] PlacedClips;
        public bool Placed;
    }
}