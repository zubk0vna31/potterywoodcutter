using PWS.WoodCutting.Common;
using UnityEngine;

namespace PWS.WoodCutting
{
    [CreateAssetMenu(fileName = "ClampsPlacementStateConfig", menuName = "PWS/WoodCutting/Stages/Clamps Placement/Config")]
    public class ClampsPlacementStateConfig : StateConfig
    {
        [Range(0f,1f)]
        public float succsessVaimTwistAmount = 0.7f;
        [Range(0f, 1f)]
        public float plankGoUpHeight = 0.7f;

        public Color plankActivatedColor;


        [Header("Durations")]
        [Range(0f, 5f)]
        public float planksMovesUpDuration = 1f;
        [Range(0f,5f)]
        public float rotateTowardPlayerDuration = 1f;
        [Range(0f, 5f)]
        public float vaimGhostAppearDuration = 1f;
        [Range(0f, 5f)]
        public float vaimGhostAnimationDuration = 1f;
        [Range(0f, 5f)]
        public float vaimRegularAnimationDuration = 1f;
        [Range(0f,5f)]
        public float clampsMovesToGhostDuration = 1f;
        [Range(0f, 5f)]
        public float finalAnimationDuration = 1f;
        [Range(0f, 5f)]
        public float skipDuration= 1f;

        [Header("Shader Settings")]
        [Range(0f, 1f)]
        public float ghostFinalOpacity = 0.33f;


    }
}
