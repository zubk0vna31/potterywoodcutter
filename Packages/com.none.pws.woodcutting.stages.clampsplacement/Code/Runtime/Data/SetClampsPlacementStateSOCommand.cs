using UnityEngine;
using Modules.StateGroup.Core;

namespace PWS.WoodCutting
{
    [CreateAssetMenu(menuName = "PWS/WoodCutting/Stages/Clamps Placement/SetStateCommand")]
    public class SetClampsPlacementStateSOCommand : SetStateSOCommand<ClampsPlacementState>
    {

    }
}
