﻿using Leopotam.Ecs;
using Modules.ViewHub;
using PWS.Common.Audio;
using UnityEngine;

namespace PWS.WoodCutting
{
    public class VaimSoundViewComponent : ViewComponent
    {
        private const float MIN_REQUIRED_ANGLE = 30f;
        private const float MAX_REQUIRED_ANGLE = 170f;
        [SerializeField] private CircularGrabbable _interactable;
        [Range(MIN_REQUIRED_ANGLE, MAX_REQUIRED_ANGLE)]
        [SerializeField] private float _requiredAngle;
        [SerializeField] private RandomSoundClipsData _twistingClipsData;
        [SerializeField] private RandomSoundClipsData _placedClipsData;
        
        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            ref var entity = ref ecsEntity.Get<VaimSoundData>();
            entity.Interactable = _interactable;
            entity.StartAngle = _interactable.transform.localRotation.eulerAngles.y;
            entity.RequiredAngle = _requiredAngle;
            entity.MinRequiredAngle = MIN_REQUIRED_ANGLE;
            entity.MaxRequiredAngle = MAX_REQUIRED_ANGLE;
            entity.TwistingClips = _twistingClipsData.Clips.ToArray();
            entity.PlacedClips = _placedClipsData.Clips.ToArray();
            entity.Placed = false;
        }
    }
}