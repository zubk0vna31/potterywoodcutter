using Leopotam.Ecs;
using PWS.WoodCutting.Common;

namespace PWS.WoodCutting
{
    public class VaimGhostAnimationSystem : IEcsRunSystem
    {
        // auto injected fields
        private readonly EcsFilter<ClampsPlacementState> _state;
        private readonly EcsFilter<VaimGhost, VaimGhostAnimate> _filter;

        private readonly ClampsPlacementStateConfig _stateConfig;
        private readonly Modules.Utils.TimeService _time;

        public VaimGhostAnimationSystem(StateConfig stateConfig)
        {
            _stateConfig = stateConfig as ClampsPlacementStateConfig;
        }

        public void Run()
        {
            if (_state.IsEmpty()) return;

            foreach (var i in _filter)
            {
                _filter.Get2(i).duration -= _time.DeltaTime;

                if (_filter.Get2(i).duration <= 0)
                {
                    _filter.Get1(i).animator.speed = 1f;
                    _filter.GetEntity(i).Del<VaimGhostAnimate>();
                    _filter.Get1(i).collider.enabled=true;
                }
            }
        }
    }
}
