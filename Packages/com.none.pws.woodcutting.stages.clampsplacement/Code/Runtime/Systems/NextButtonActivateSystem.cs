using DG.Tweening;
using Leopotam.Ecs;
using Modules.StateGroup.Components;
using Modules.StateGroup.Core;
using Modules.ViewHub;
using PWS.Common;
using PWS.Common.GameModeInfoService;
using PWS.Common.Messages;
using PWS.Common.UI;
using PWS.Features.Achievements;
using PWS.Features.Achivements;
using PWS.WoodCutting.Common;
using UnityEngine;

namespace PWS.WoodCutting
{
    public class NextButtonActivateSystem : IEcsRunSystem
    {
        // auto injected fields
        private readonly EcsFilter<ClampsPlacementState> _state;

        private readonly EcsFilter<StateWindow,StateProgress> _stateWindowWithProgress;
        private readonly EcsFilter<NextStateSignal> _nextStateSignal;

        private readonly EcsFilter<VaimRegular, UnityView> _vaimsRegular;
        private readonly EcsFilter<PlankParent> _plankParent;

        private readonly ClampsPlacementStateConfig _stateConfig;

        private readonly IAchievementsService _achievementsService;

        private readonly EcsWorld _ecsWorld;

        public NextButtonActivateSystem(StateConfig stateConfig,IAchievementsService achievementsService)
        {
            _stateConfig = stateConfig as ClampsPlacementStateConfig;
            _achievementsService = achievementsService;
        }

        private void Next()
        {
            foreach (var i in _vaimsRegular)
            {
                var sequence = DOTween.Sequence();
                sequence.Pause();

                var transform = _vaimsRegular.Get2(i).Transform;
                var animator = _vaimsRegular.Get1(i).animator;

                Vector3 initPos = _vaimsRegular.Get1(i).initPosition;
                Vector3 initRot = _vaimsRegular.Get1(i).initRotation;

                var rigidBody = _vaimsRegular.Get1(i).rigidbody;
                var grabInteractable = _vaimsRegular.Get1(i).grabInteractable;

                float animationTime1 = _stateConfig.finalAnimationDuration * 0.15f;
                float moveUpTime = _stateConfig.finalAnimationDuration * 0.15f;
                float animationTime2 = _stateConfig.finalAnimationDuration * 0.3f;
                float moveToInitTime = _stateConfig.finalAnimationDuration * 0.4f;


                animator.speed = 1f / animationTime1;
                sequence.AppendInterval(animationTime1);

                sequence.Append(transform.DOMoveY(0.185f, moveUpTime).SetRelative(true));

                animator.speed = 1f / animationTime2;
                sequence.AppendCallback(() => { animator.SetTrigger("Sbor"); });

                sequence.AppendInterval(animationTime2);

                sequence.Append(transform.DOMove(initPos, moveToInitTime));
                sequence.Insert(animationTime1 + moveUpTime + animationTime2, transform.DORotate(initRot, moveToInitTime));

                sequence.AppendCallback(() =>
                {
                    grabInteractable.enabled = true;
                    rigidBody.isKinematic = false;
                });


                animator.SetTrigger("Razbor");
                sequence.Play();

                _vaimsRegular.Get1(i).circularGrabbable.enabled = false;

                foreach (var j in _vaimsRegular.Get1(i).dissableCollider)
                {
                    j.enabled = false;
                }

            }

            foreach (var i in _plankParent)
            {
                _plankParent.Get1(i).view.Switch();
            }

            var entity = _ecsWorld.NewEntity();
            entity.Get<SetStateDelayed>().state = _stateConfig.nextState;
            entity.Get<SetStateDelayed>().delay = _stateConfig.finalAnimationDuration;

        }

        private void Skip()
        {
            foreach (var i in _nextStateSignal)
            {
                foreach (var v in _vaimsRegular)
                {
                    var circularGrabbable = _vaimsRegular.Get1(v).circularGrabbable;
                    circularGrabbable.enabled = false;

                    float time = Mathf.Lerp(_stateConfig.skipDuration, 0f, circularGrabbable.TwistAmount);

                    circularGrabbable.ContiniousUpdateAngle(time);
                }
            }


            float delay = 0;
            DOTween.To(() => delay, x => delay = x, 1f, _stateConfig.skipDuration)
                        .OnComplete(() =>
                        {
                            Next();
                        });

            foreach (var i in _nextStateSignal)
            {
                _nextStateSignal.GetEntity(i).Get<Ignore>();
            }


            float grade = 0f;
            foreach (var i in _stateWindowWithProgress)
            {
                _stateWindowWithProgress.Get1(i).Template.ShowButton(false);
                _stateWindowWithProgress.GetEntity(i).Get<Ignore>();

                grade = _stateWindowWithProgress.Get2(i).ProgressValue;
            }
            _ecsWorld.NewEntity().Get<SetGradeSignal>().grade = grade;
        }
        public void Run()
        {
            if (_state.IsEmpty()) return;

            if (!_nextStateSignal.IsEmpty())
            {
                Skip();
            }

        }
    }
}
