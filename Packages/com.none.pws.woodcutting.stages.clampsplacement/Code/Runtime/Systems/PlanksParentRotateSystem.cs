using DG.Tweening;
using Leopotam.Ecs;
using Modules.ViewHub;
using PWS.WoodCutting.Common;
using UnityEngine;

namespace PWS.WoodCutting
{
    public class PlanksParentRotateSystem : IEcsRunSystem
    {
        // auto injected fields
        private readonly EcsFilter<ClampsPlacementState> _state;
        private readonly EcsFilter<PlankParent,PlankParentRotate> _filter;
        private readonly EcsFilter<VaimGhost> _vaims;

        private readonly ClampsPlacementStateConfig _stateConfig;
        private readonly Modules.Utils.TimeService _time;
        private readonly Transform _vaimsOrigin;

        public PlanksParentRotateSystem(StateConfig stateConfig,Transform vaimsOrigin)
        {
            _stateConfig = stateConfig as ClampsPlacementStateConfig;
            _vaimsOrigin = vaimsOrigin;
        }

        public void Run()
        {
            if (_state.IsEmpty()) return;

            foreach (var i in _filter)
            {
                _filter.Get2(i).duration -= _time.DeltaTime;

                if (_filter.Get2(i).duration <= 0)
                {
                    _filter.GetEntity(i).Del<PlankParentRotate>();

                    int index = 0;
                    foreach (var v in _vaims)
                    {
                        if (index == 0)
                        {
                            _vaims.GetEntity(i).Get<UnityView>().Transform.parent.position =
                               _vaimsOrigin.position;
                        }

                        _vaims.GetEntity(v).Get<VaimGhostAppear>().duration = _stateConfig.vaimGhostAppearDuration;

                        foreach (var renderer in _vaims.Get1(v).meshRenderer)
                        {
                            renderer.material.DOFade(_stateConfig.ghostFinalOpacity, _stateConfig.vaimGhostAppearDuration);
                        }

                        index++;
                    }
                }
            }
        }
    }
}
