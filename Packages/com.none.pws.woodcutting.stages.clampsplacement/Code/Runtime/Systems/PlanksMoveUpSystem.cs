using DG.Tweening;
using Leopotam.Ecs;
using Modules.ViewHub;
using PWS.WoodCutting.Common;
using UnityEngine;

namespace PWS.WoodCutting
{
    public class PlanksMoveUpSystem : IEcsRunSystem
    {
        // auto injected fields
        private readonly EcsFilter<ClampsPlacementState> _state;
        private readonly EcsFilter<PlankComponent, PlankMoveUp> _filter;
        private readonly EcsFilter<PlankParent, UnityView> _plankParent;

        private readonly EcsFilter<PlankComponent> _planks;

        private readonly ClampsPlacementStateConfig _stateConfig;
        private readonly Modules.Utils.TimeService _time;

        private readonly Transform _rotateTowardPlayer;

        public PlanksMoveUpSystem(StateConfig stateConfig, Transform rotateTowardPlayer)
        {
            _stateConfig = stateConfig as ClampsPlacementStateConfig;
            _rotateTowardPlayer = rotateTowardPlayer;
        }

        public void Run()
        {
            if (_state.IsEmpty()) return;

            int amount = MoveUp();

            if (_planks.GetEntitiesCount() > amount) return;

            foreach (var i in _filter)
            {
                _filter.GetEntity(i).Del<PlankMoveUp>();
            }

            //Rotate plank parent toward player face
            foreach (var i in _plankParent)
            {
                var planksParentTransform = _plankParent.Get2(i).Transform;

                Quaternion desiredRotation = Quaternion.LookRotation(_rotateTowardPlayer.forward, _rotateTowardPlayer.up);

                var sequence = DOTween.Sequence();
                sequence.Pause();

                sequence.Append(planksParentTransform.DORotateQuaternion(desiredRotation, _stateConfig.rotateTowardPlayerDuration));

                sequence.Play();


                _plankParent.GetEntity(i).Get<PlankParentRotate>().duration = _stateConfig.rotateTowardPlayerDuration;
            }
        }

        private int MoveUp()
        {
            int amount = 0;
            foreach (var i in _filter)
            {
                if (_filter.Get2(i).duration <= 0)
                {
                    amount++;
                    continue;
                }
                _filter.Get2(i).duration -= _time.DeltaTime;
            }

            return amount;
        }
    }
}
