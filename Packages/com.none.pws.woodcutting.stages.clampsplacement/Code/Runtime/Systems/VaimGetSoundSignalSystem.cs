﻿using Leopotam.Ecs;
using UnityEngine;

namespace PWS.WoodCutting
{
    public class VaimGetSoundSignalSystem : IEcsRunSystem
    {
        private readonly EcsFilter<ClampsPlacementState> _inState;
        private readonly EcsFilter<VaimSoundData> _vaim;

        public void Run()
        {
            if (_inState.IsEmpty())
                return;

            foreach (var i in _vaim)
            {
                ref var vaim = ref _vaim.Get1(i);

                if (vaim.Interactable.isSelected)
                {
                    vaim.CurrentAngle = vaim.Interactable.transform.localRotation.eulerAngles.y;
                    var angle = Mathf.Abs(vaim.CurrentAngle - vaim.StartAngle);
                    if (angle > vaim.RequiredAngle)
                    {
                        _vaim.GetEntity(i).Get<ClampsTwistingSoundSignal>();
                        vaim.StartAngle = vaim.CurrentAngle;
                    }
                }
            }
        }
    }
}
