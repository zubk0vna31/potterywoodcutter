﻿using Leopotam.Ecs;
using Modules.Audio;
using UnityEngine;

namespace PWS.WoodCutting
{
    public class VaimSoundAudioSystem : IEcsRunSystem
    {
        private readonly EcsFilter<ClampsPlacementState> _inState;
        private readonly EcsFilter<VaimRegular, VaimSoundData, AudioSourceRef> _vaim;
        private readonly EcsFilter<ClampsTwistingSoundSignal> _signal;

        private bool _allVaimPlaced = false;
        
        public void Run()
        {
            if (_inState.IsEmpty())
                return;

            if (!_signal.IsEmpty())
            {
                foreach (var i in _vaim)
                {
                    ref var vaimData = ref _vaim.Get2(i);
                    ref var vaimAudioSource = ref _vaim.Get3(i);

                    if (vaimData.Interactable.isSelected)
                    {
                        if (vaimAudioSource.AudioSource.isPlaying)
                            return;
                    
                        var id = Random.Range(0, vaimData.TwistingClips.Length);
                        ref var playSignal = ref _vaim.GetEntity(i).Get<PlayGlobalAuidioSignal>();
                        playSignal.AudioSource = vaimAudioSource.AudioSource;
                        playSignal.AudioClip = vaimData.TwistingClips[id];
                    }
                }
            }

            if (!_allVaimPlaced)
            {
                _allVaimPlaced = true;
                
                foreach (var i in _vaim)
                {
                    ref var vaimData = ref _vaim.Get2(i);
                    ref var vaimAudioSource = ref _vaim.Get3(i);
                
                    if (_vaim.GetEntity(i).Has<VaimPlaced>() && !vaimData.Placed)
                    {
                        var id = Random.Range(0, vaimData.PlacedClips.Length);
                        ref var playSignal = ref _vaim.GetEntity(i).Get<PlayGlobalAuidioSignal>();
                        playSignal.AudioSource = vaimAudioSource.AudioSource;
                        playSignal.AudioClip = vaimData.PlacedClips[id];
                        vaimData.Placed = true;
                    }
                    
                    _allVaimPlaced &= vaimData.Placed;
                }
            }
        }
    }
}