using DG.Tweening;
using Leopotam.Ecs;
using Modules.ViewHub;
using PWS.WoodCutting.Common;
using UnityEngine;

namespace PWS.WoodCutting
{
    public class VaimMoveSystem : IEcsRunSystem
    {
        // auto injected fields
        private readonly EcsFilter<ClampsPlacementState> _state;
        private readonly EcsFilter<VaimRegular, NearGhost,SelectExit> _filter;
        private readonly EcsFilter<Table,UnityView> _table;

        private readonly ClampsPlacementStateConfig _stateConfig;
        private readonly Transform _tableDirection;

        public VaimMoveSystem(StateConfig stateConfig, Transform tableDirection)
        {
            _stateConfig = stateConfig as ClampsPlacementStateConfig;
            _tableDirection = tableDirection;
        }


        public void Run()
        {
            if (_state.IsEmpty()) return;

            GrabEndProcessing();
        }
            
        private void GrabEndProcessing()
        {
            foreach (var i in _filter)
            {
                _filter.Get1(i).grabInteractable.enabled = false;
                _filter.Get1(i).rigidbody.isKinematic = true;
                var transform = _filter.GetEntity(i).Get<UnityView>().Transform;

                _filter.Get2(i).ghostCollider.enabled = false;

                var ghostEntity = _filter.Get2(i).ghostTransform.GetComponent<EntityRef>().Entity;
                var vaimEntity = _filter.GetEntity(i);

                //Make Ghost Vaim dissapear
                foreach(var mr in ghostEntity.Get<VaimGhost>().meshRenderer)
                {
                    mr.material.DOFade(0f, _stateConfig.clampsMovesToGhostDuration * 0.5f);
                }

                transform.DOMove(_filter.Get2(i).ghostTransform.position,_stateConfig.clampsMovesToGhostDuration);

                Quaternion rotation = Quaternion.identity;

                foreach (var t in _table)
                {
                    rotation = Quaternion.LookRotation(-_tableDirection.forward, _tableDirection.up);
                    break;
                }

                transform.DORotateQuaternion(rotation, _stateConfig.clampsMovesToGhostDuration)
                    .OnComplete(()=>
                    {
                        vaimEntity.Get<VaimRegularAnimate>().duration = _stateConfig.vaimRegularAnimationDuration;

                        vaimEntity.Get<VaimRegular>().animator.speed = 1f / _stateConfig.vaimRegularAnimationDuration;
                        vaimEntity.Get<VaimRegular>().animator.SetTrigger("Activate");

                        vaimEntity.Get<VaimPlaced>();
                    });
            }
        }
    }
}

