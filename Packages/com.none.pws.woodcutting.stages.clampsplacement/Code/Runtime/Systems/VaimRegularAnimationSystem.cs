using Leopotam.Ecs;

namespace PWS.WoodCutting
{
    public class VaimRegularAnimationSystem : IEcsRunSystem
    {
        // auto injected fields
        private readonly EcsFilter<ClampsPlacementState> _state;
        private readonly EcsFilter<VaimRegular, VaimRegularAnimate> _filter;

        private readonly Modules.Utils.TimeService _time;

        public void Run()
        {
            if (_state.IsEmpty()) return;

            foreach (var i in _filter)
            {
                _filter.Get2(i).duration -= _time.DeltaTime;

                if (_filter.Get2(i).duration <= 0)
                {
                    _filter.GetEntity(i).Del<VaimRegularAnimate>();

                    _filter.Get1(i).circularGrabbable.enabled = true;
                    _filter.Get1(i).axisMover.enabled = true;
                }
            }
        }
    }
}
