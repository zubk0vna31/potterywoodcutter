using Leopotam.Ecs;
using Modules.Root.ContainerComponentModel;
using Modules.Root.ECS;
using PWS.Common.GameModeInfoService;
using PWS.Common.UI;
using UnityEngine;
using PWS.Common.Audio;
using PWS.WoodCutting.Common;
using PWS.Common.ResultsEvaluation;
using PWS.Common.TravelPoints;
using PWS.Features.Achievements;

namespace PWS.WoodCutting
{
    internal class StateDependecies
    {
        [Inject] public IGameModeInfoService gameModeInfoService;
        [Inject] public IResultsEvaluationDataHolder resultsEvaluation;
        [Inject] public IAchievementsService achievementsService;
    }

    public enum ClampsPlacementSubstates
    {
        None=0,
        Transition,
        Place,
        Twist
    }

    public class WoodCuttingClampsPlacementSystemsProvider : MonoBehaviour, ISystemsProvider
    {
        [Header("Core")]
        [SerializeField]
        private StateConfig _stateConfig;
        [SerializeField]
        private StateInfoData _stateInfo;

        [Header("Scene Related")]
        [SerializeField]
        private Transform _rotateTowardPlayer;
        [SerializeField]
        private Transform _tableDirection;
        [SerializeField]
        private Transform _vaimsOrigin;
        
        [Header("Voice config")] 
        [SerializeField] private VoiceConfig _voiceConfig;

        public EcsSystems GetSystems(EcsWorld world, EcsSystems endFrame, EcsSystems mainSystems)
        {
            StateDependecies dependecies = new StateDependecies();
            SceneContainer.Instance.Inject(dependecies);


            EcsSystems systems = new EcsSystems(world, "WoodCutting Clamps Placement");

            systems

                .Add(new SubstateSystem
                <ClampsPlacementState, ClampsPlacementSubstates, ClampsPlacementStateConfig>
                (_stateConfig, ClampsPlacementSubstates.Place, 
                ClampsPlacementSubstates.Transition, ClampsPlacementSubstates.None))

                .Add(new EntityTaskTrackerSubstateSignal
                <ClampsPlacementState,VaimRegular,ClampsPlacementSubstates>
                (ClampsPlacementSubstates.Twist))


                .Add(new OutlineByStateSystem<ClampsPlacementState>(_stateConfig))



                .Add(new ClampsPlacementStateEnterSystem(_stateConfig))
                .Add(new PlanksMoveUpSystem(_stateConfig, _rotateTowardPlayer))
                .Add(new NextButtonActivateSystem(_stateConfig,dependecies.achievementsService))
                .Add(new PlanksParentRotateSystem(_stateConfig, _vaimsOrigin))
                .Add(new VaimGhostAppearSystem(_stateConfig))
                .Add(new VaimGhostAnimationSystem(_stateConfig))
                .Add(new VaimMoveSystem(_stateConfig, _tableDirection))
                .Add(new VaimRegularAnimationSystem())

                .Add(new SetStateDelayedSystem<ClampsPlacementState>())

                //Tracker
                .Add(new ClampsPlacementProcessTracker(_stateConfig,dependecies.gameModeInfoService))

                .Add(new StateInfoWindowUIProcessing<ClampsPlacementState>(_stateInfo))

                 .Add(new WoodCuttingSetGradeByProgress<ClampsPlacementState>(dependecies.resultsEvaluation,
                _stateInfo))

                .Add(new StateWindowPlacementProcessing<ClampsPlacementState>("Main"))
                .Add(new StateWindowProgressUIProcessing<ClampsPlacementState>())

                .Add(new TravelPointPositioningSystem<ClampsPlacementState>("Default"))


                .Add(new SubstateButtonUIProcessing<ClampsPlacementState,ClampsPlacementSubstates>
                (ClampsPlacementSubstates.Twist))

                //.Add(new StateWindowButtonUIProcessing<ClampsPlacementState>(false))

                .OneFrame<GrabBegin>()
                .OneFrame<GrabEnd>()
                
                //Voice
                .Add(new VoiceAudioSystem<ClampsPlacementState>(_voiceConfig))
                
                //Sound
                .Add(new VaimGetSoundSignalSystem())
                .Add(new VaimSoundAudioSystem())
            ;

            endFrame
                .OneFrame<ClampsTwistingSoundSignal>()
                
                ;
            

            return systems;
        }
    }
}
