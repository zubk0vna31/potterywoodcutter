using Leopotam.Ecs;
using PWS.Common.GameModeInfoService;
using PWS.Common.UI;
using PWS.WoodCutting.Common;

namespace PWS.WoodCutting
{
    public class ClampsPlacementProcessTracker : RunSystem<ClampsPlacementState,ClampsPlacementStateConfig>
    {
        private readonly EcsFilter<StateWindow, StateProgress> _stateWindowWithProgress;
        private readonly EcsFilter<VaimRegular> _vaims;

        private readonly IGameModeInfoService _gameModeInfoService;


        public ClampsPlacementProcessTracker(StateConfig config,IGameModeInfoService gameModeInfoService) : base(config)
        {
            _gameModeInfoService = gameModeInfoService;
        }

        protected override void OnStateEnter()
        {
            base.OnStateEnter();

            foreach (var i in _stateWindowWithProgress)
            {
                ref var progress = ref _stateWindowWithProgress.Get2(i);

                progress.Value = 0;
                progress.CompletionThreshold = 2f;
                progress.SkipThreshold = _gameModeInfoService.CurrentMode.Equals(GameMode.Free)? 
                    0.0f: _config.succsessVaimTwistAmount;
            }
        }

        protected override void OnStateUpdate()
        {
            base.OnStateUpdate();

            foreach (var i in _stateWindowWithProgress)
            {
                float twistProgress = 0f;

                foreach (var j in _vaims)
                {
                    twistProgress += _vaims.Get1(j).circularGrabbable.TwistAmount;
                }

                twistProgress /= _vaims.GetEntitiesCount();


                _stateWindowWithProgress.Get2(i).Value = twistProgress;
            }
        }

    }
}
