using DG.Tweening;
using Leopotam.Ecs;
using PWS.WoodCutting.Common;
using UnityEngine;

namespace PWS.WoodCutting
{
    public class VaimGhostAppearSystem : IEcsRunSystem
    {
        // auto injected fields
        private readonly EcsFilter<ClampsPlacementState> _state;
        private readonly EcsFilter<VaimGhost,VaimGhostAppear> _filter;

        private readonly ClampsPlacementStateConfig _stateConfig;
        private readonly Modules.Utils.TimeService _time;

        public VaimGhostAppearSystem(StateConfig stateConfig)
        {
            _stateConfig = stateConfig as ClampsPlacementStateConfig;
        }

        public void Run()
        {
            if (_state.IsEmpty()) return;

            foreach (var i  in _filter)
            {
                _filter.Get2(i).duration -= _time.DeltaTime;

                if (_filter.Get2(i).duration <= 0)
                {
                    _filter.GetEntity(i).Del<VaimGhostAppear>();

                    _filter.GetEntity(i).Get<VaimGhostAnimate>().duration = _stateConfig.vaimGhostAnimationDuration;

                    Color color = _stateConfig.plankActivatedColor;
                    color.a = _stateConfig.ghostFinalOpacity;

                    foreach (var rend in _filter.Get1(i).meshRenderer)
                    {
                        rend.material.DOColor(color, _stateConfig.vaimGhostAnimationDuration);
                    }

                    _filter.Get1(i).animator.speed = 1f / _stateConfig.vaimGhostAnimationDuration;
                    _filter.Get1(i).animator.SetTrigger("Appear");
                }
            }
        }
    }
}
