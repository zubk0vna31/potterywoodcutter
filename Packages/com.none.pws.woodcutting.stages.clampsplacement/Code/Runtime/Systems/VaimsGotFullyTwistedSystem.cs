using Leopotam.Ecs;
using Modules.StateGroup.Core;
using Modules.ViewHub;
using UnityEngine;

namespace PWS.WoodCutting
{
    public class VaimsGotFullyTwistedSystem : IEcsRunSystem
    {
        // auto injected fields
        private readonly EcsFilter<ClampsPlacementState> _state;
        private readonly EcsFilter<VaimRegular, VaimTwisted> _filter;
        private readonly EcsFilter<VaimRegular> _vaimsRegular;

        private readonly ClampsPlacementStateConfig _config;

        private readonly StateFactory _stateFactory;

        // runtime data
        private bool stageCompleted;

        public VaimsGotFullyTwistedSystem(ClampsPlacementStateConfig config)
        {
            _config = config;
        }

        public void Run()
        {
            if (stageCompleted || _state.IsEmpty()) return;

            if (_vaimsRegular.GetEntitiesCount() > _filter.GetEntitiesCount()) return;

            stageCompleted = true;

            Vector3 spawnPoint = Vector3.zero;

            foreach (var i in _filter)
            {
                spawnPoint += _filter.GetEntity(i).Get<UnityView>().Transform.position;
            }

            if (_filter.GetEntitiesCount() != 0)
                spawnPoint /= _filter.GetEntitiesCount();


            _config.nextState.Execute(_stateFactory);
        }

    }
}
