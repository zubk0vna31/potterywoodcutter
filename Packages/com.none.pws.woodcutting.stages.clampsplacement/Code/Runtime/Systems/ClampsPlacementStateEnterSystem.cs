using DG.Tweening;
using Leopotam.Ecs;
using Modules.StateGroup.Components;
using Modules.ViewHub;
using PWS.WoodCutting.Common;
using UnityEngine;

namespace PWS.WoodCutting
{
    public class ClampsPlacementStateEnterSystem : IEcsRunSystem
    {
        // auto injected fields
        private readonly EcsFilter<StateEnter, ClampsPlacementState> _stateEnter;
        private readonly EcsFilter<PlankComponent,UnityView> _planks;
        private readonly EcsFilter<PlankParent,UnityView> _plankParent;

        private readonly ClampsPlacementStateConfig _stateConfig;

        public ClampsPlacementStateEnterSystem(StateConfig stateConfig)
        {
            _stateConfig = stateConfig as ClampsPlacementStateConfig;
        }

        public void Run()
        {
            if (_stateEnter.IsEmpty()) return;

            Debug.Log("Enter Clamps Placement State");

            //Dissable all planks collider, rigidbody and grabbable
            foreach (var i in _planks)
            {
                int slotValue = _planks.Get1(i).slotID - (Mathf.FloorToInt(_planks.GetEntitiesCount() * 0.5f));
                _planks.Get1(i).collider.enabled = false;
                _planks.Get1(i).rigidbody.isKinematic = true;
                _planks.Get1(i).grabbable.enabled = false;

                //Enable trigger collider
                _planks.Get1(i).triggerCollider.enabled = true;

                _planks.Get2(i).Transform.DOLocalMoveX(-slotValue * 0.1f, _stateConfig.planksMovesUpDuration * 0.25f);
                _planks.Get2(i).Transform.DOLocalMoveY(0f, _stateConfig.planksMovesUpDuration * 0.05f);

                _planks.GetEntity(i).Get<PlankMoveUp>().duration = _stateConfig.planksMovesUpDuration;
            }

            foreach (var i in _plankParent)
            {
                _plankParent.Get2(i).Transform.DOLocalMoveY(0.15f, _stateConfig.planksMovesUpDuration);
            }
        }

    }
}
