using Leopotam.Ecs;
using Modules.ViewHub;
using UnityEngine;

namespace PWS.WoodCutting
{
    public class VaimGhostTemplate : EntityTemplate
    {
        [SerializeField]
        private string interactionTag;

        [SerializeField]
        private Animator animator;
        [SerializeField]
        private Collider _collider;

        private EcsEntity entity;

        protected override void SetupEntity(EcsEntity entity, EcsWorld world)
        {
            base.SetupEntity(entity, world);
            this.entity = entity;

            ref var ghost = ref entity.Get<VaimGhost>();

            ghost.collider = _collider;
            ghost.canStoreTarget = true;
            ghost.meshRenderer = GetComponentsInChildren<MeshRenderer>();
            ghost.animator = animator;

            //Change alpha to zero on start
            var color = ghost.meshRenderer[0].material.GetColor("_Color");
            color.a = 0f;

            foreach (var meshRenderer in ghost.meshRenderer)
            {
                meshRenderer.material.SetColor("_Color", color);
            }

            //Change animation trigger to "Ghost"
            ghost.animator.SetTrigger("Ghost");
        }

        public void OnTriggerEnter(Collider other)
        {
            if (!other.CompareTag(interactionTag)) return;

            var entityRef = other.GetComponentInParent<EntityRef>();

            if (!entityRef) return;

            if (!entity.Get<VaimGhost>().canStoreTarget ||  entityRef.Entity.Has<NearGhost>()) return;

            ref var ghost = ref entityRef.Entity.Get<NearGhost>();
            ghost.ghostTransform = transform;
            ghost.ghostCollider = transform.GetComponent<Collider>();

            entity.Get<VaimGhost>().canStoreTarget = false;
        }

        public void OnTriggerExit(Collider other)
        {
            if (!other.CompareTag(interactionTag)) return;

            var entityRef = other.GetComponentInParent<EntityRef>();

            if (!entityRef) return;

            if (!entityRef.Entity.Has<NearGhost>()) return;

            entityRef.Entity.Del<NearGhost>();

            entity.Get<VaimGhost>().canStoreTarget = true;
        }
    }
}
