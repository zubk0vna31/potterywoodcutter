using Leopotam.Ecs;
using Modules.ViewHub;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace PWS.WoodCutting
{
    public class VaimRegularTemplate : EntityTemplate
    {
        [SerializeField]
        private ClampsPlacementStateConfig config;

        [SerializeField]
        private Collider[] dissableColliders;

        private EcsEntity entity;

        protected override void SetupEntity(EcsEntity entity, EcsWorld world)
        {
            base.SetupEntity(entity, world);
            this.entity = entity;

            ref var vaim = ref entity.Get<VaimRegular>();
            
            vaim.grabInteractable = GetComponent<XRGrabInteractable>();
            vaim.rigidbody = GetComponent<Rigidbody>();
            vaim.circularGrabbable = GetComponentInChildren<CircularGrabbable>();
            vaim.axisMover = GetComponentInChildren<AxisMover>();
            vaim.animator = GetComponentInChildren<Animator>();
            vaim.dissableCollider = dissableColliders;

            vaim.initPosition = transform.position;
            vaim.initRotation = transform.eulerAngles;

            vaim.circularGrabbable.AddMaxAngleReachedEvent(MaxAngleReached);
            vaim.circularGrabbable.AddSpecificValueReachedEvent(config.succsessVaimTwistAmount, DesiredAngleReached);
        }

        private void DesiredAngleReached()
        {
            entity.Get<VaimTwisted>();
        }

        private void MaxAngleReached()
        {
            entity.Get<VaimRegular>().circularGrabbable.enabled = false;
        }

        public void SelectEnter(SelectEnterEventArgs args)
        {
            entity.Get<SelectEnter>();
        }

        public void SelectExit(SelectExitEventArgs args)
        {
            entity.Get<SelectExit>();
        }
    }
}
