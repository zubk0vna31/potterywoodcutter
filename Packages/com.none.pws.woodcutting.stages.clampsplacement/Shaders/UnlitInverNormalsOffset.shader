Shader "Unlit/UnlitInverNormalsOffset"
{
    Properties
    {
        _Color ("Color", Color) = (.5,.5,.5,1)
		_Outline ("Outline width", Range (.002, 2)) = .005
		_MaxWidth ("Max Width", Range (.002, 1)) = .005
    }

    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Cull Front
		ZWrite On
		Blend SrcAlpha OneMinusSrcAlpha


        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
	            float3 normal : NORMAL;
            };

            struct v2f
            {
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;

            half4  _Color;
            half _Outline,_MaxWidth;

            v2f vert (appdata v)
            {
                v2f o;

	            o.vertex = UnityObjectToClipPos(v.vertex);
 
	            float3 norm   = mul ((float3x3)UNITY_MATRIX_IT_MV, v.normal);
	            float2 offset = TransformViewToProjection(norm.xy);

                half clip = clamp(o.vertex.w*_Outline,0,_MaxWidth);
 
	            o.vertex.xy += offset * o.vertex.z * clip;
	            return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                return _Color;
            }
            ENDCG
        }
    }
}
