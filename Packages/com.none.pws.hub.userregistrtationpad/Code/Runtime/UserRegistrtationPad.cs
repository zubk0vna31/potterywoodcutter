using Modules.Root.ContainerComponentModel;
using PWS.Common.UserInfoService;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using TMPro;

namespace PWS.HUB.UserRegistrtationPad
{
    public class UserRegistrtationPad : MonoBehaviour
    {
        // dependencies
        [Inject] private IUserInfoService _userInfoService;

        [Header("View")]
        [SerializeField] private GameObject _canvas;
        [SerializeField] private TMP_InputField _nameInputField;
        [SerializeField] private Button _confirmButton;

        [SerializeField] private UnityEvent _onConfirmEvent;

        private void Awake() {
            AppContainer.Inject(this);
        }

        private void Start() {
            Hide();
            _confirmButton.onClick.AddListener(OnConfirm);
        }

        public void OnConfirm() {
            if (_nameInputField.text.Length == 0)
                return;

            _userInfoService.UserName = _nameInputField.text;
            Hide();

            _onConfirmEvent.Invoke();
        }

        public void Show() {
            _canvas.SetActive(true);
        }

        public void Hide() {
            _canvas.SetActive(false);
        }
    }
}
