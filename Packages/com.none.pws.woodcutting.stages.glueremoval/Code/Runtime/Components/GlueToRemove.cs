using UnityEngine;

namespace PWS.WoodCutting
{
    public struct GlueToRemove 
    {
        public int glueID;
        public Vector3 normal;
        public GlueMeshCreator glueMeshCreatorMain, glueMeshCreatorLinked;

        public void ResetUnityComponents()
        {
            glueMeshCreatorMain.ResetComponent();
            glueMeshCreatorLinked.ResetComponent();
        }
    }
}
