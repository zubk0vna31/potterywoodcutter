using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace PWS.WoodCutting
{
    public struct GlueSideToRemove 
    {
        public int order;
        public Vector3 normal;
        public List<GlueMeshRenderer> glueMeshRenderer;

        public float Percentage
        {
            get
            {
                return glueMeshRenderer.Select(x=>x.PushAmount).Aggregate((x, y) => x + y)/glueMeshRenderer.Count;
            }
        }
    }
}
