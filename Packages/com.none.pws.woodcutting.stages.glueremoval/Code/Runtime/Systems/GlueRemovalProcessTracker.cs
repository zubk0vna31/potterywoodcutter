using Leopotam.Ecs;
using PWS.Common.UI;
using PWS.WoodCutting.Common;
using UnityEngine;

namespace PWS.WoodCutting
{
    public class GlueRemovalProcessTracker : RunSystem<GlueRemovalState,GlueRemovalStateConfig>
    {
        private readonly EcsFilter<StateWindow, StateProgress> _stateWindowWithProgress;
        private readonly EcsFilter<GlueSideToRemove> _glueSides;

        public GlueRemovalProcessTracker(StateConfig config) : base(config)
        {
        }


        protected override void OnStateEnter()
        {
            base.OnStateEnter();

            foreach (var i in _stateWindowWithProgress)
            {
                ref var progress = ref _stateWindowWithProgress.Get2(i);

                progress.Value = 0;
                progress.CompletionThreshold = 2f;
            }
        }

        protected override void OnStateUpdate()
        {
            foreach (var i in _stateWindowWithProgress)
            {
                float progress = 0f;

                foreach (var j in _glueSides)
                {
                    Debug.Log(_glueSides.Get1(j).Percentage);
                    progress += _glueSides.Get1(j).Percentage;
                }

                progress /= _glueSides.GetEntitiesCount();

                _stateWindowWithProgress.Get2(i).Value = progress;
            }
        }
    }
}
