using DG.Tweening;
using Leopotam.Ecs;
using Modules.ViewHub;
using PWS.Common;
using PWS.Common.UI;
using PWS.WoodCutting.Common;
using System.Diagnostics;

namespace PWS.WoodCutting.Stages.GlueRemoval
{
    public class GlueRemovalBotSubstateSystem : RunSubstateSystem<GlueRemovalState,
        GlueRemovalSubstates, GlueRemovalStateConfig>
    {
        // auto-inject fields
        private readonly EcsFilter<GlueSideToRemove, UnityView> _glueSides;
        private readonly EcsFilter<StateWindow, StateProgress> _stateWindowWithProgress;
        private readonly EcsFilter<NextStateSignal> _nextStateSignal;
        private readonly EcsFilter<GluePusher> _gluePusher;

        private readonly IGlueRemovalDataHolder _glueRemovalDataHolder;

        private readonly int _order;

        public GlueRemovalBotSubstateSystem(StateConfig stateConfig, GlueRemovalSubstates currentSubstate,
            IGlueRemovalDataHolder glueRemovalDataHolder) : base(stateConfig, currentSubstate)
        {
            _glueRemovalDataHolder = glueRemovalDataHolder;
            _order = 1;
        }

        protected override void OnSubstateEnter()
        {
            base.OnSubstateEnter();
            ToggleStickyObjects(true);
            ToggleGlueSides(true);
        }

        protected override void OnSubstateUpdate()
        {
            base.OnSubstateUpdate();

            if (!_nextStateSignal.IsEmpty())
            {
                ToggleStickyObjects(false);
                ToggleGlueSides(false);

                // Grade 
                float grade = 0f;
                foreach (var i in _stateWindowWithProgress)
                {
                    grade = _stateWindowWithProgress.Get2(i).ProgressValue;
                }
                _ecsWorld.NewEntity().Get<SetGradeSignal>().grade = grade;

                // Push
                PushClayWithDuration(_config.skipDuration);

                // Change substate to transition to hide buttons
                ChangeSubstate(GlueRemovalSubstates.None);

                // Change state
                ChangeState(_config.skipDuration);
            }
        }

        private void ToggleStickyObjects(bool value)
        {
            var stickyObjects = _glueRemovalDataHolder.GetStickyObjectsByOrder(_order);

            foreach (var sticky in stickyObjects)
            {
                sticky.Toggle(value);
            }
        }

        private void ToggleGlueSides(bool value)
        {
            ref var current = ref _glueRemovalDataHolder.GetEntityByOrder(_order).Get<GlueSideToRemove>();
            var transform = _glueRemovalDataHolder.GetEntityByOrder(_order).Get<UnityView>().Transform;

            foreach (var glue in current.glueMeshRenderer)
            {
                glue.ToggleCollider(value);
                if (value)
                {
                    glue.PushDirection = transform.up;
                }
            }
        }

        private void PushClayWithDuration(float duration)
        {
            float pushAmount = 0f;
            GluePushSystem pushSystem = null;

            foreach (var i in _gluePusher)
            {
                pushAmount = _gluePusher.Get1(i).gluePushSystem.PushStrength;
                pushSystem = _gluePusher.Get1(i).gluePushSystem;
            }

            foreach (var i in _glueSides)
            {
                ref var side = ref _glueSides.Get1(i);

                foreach (var glueMesh in side.glueMeshRenderer)
                {
                    glueMesh.PushAll(pushAmount,duration);
                    glueMesh.ToggleCollider(false);
                }
            }

            pushSystem.Clear();
            foreach (var i in _glueSides)
            {
                var transform = _glueSides.Get2(i).Transform;
                float baseRadius = _glueSides.Get1(i).glueMeshRenderer[0].BaseRadius;

                transform.DOLocalMove(transform.up * (baseRadius * 2.1f), duration).SetRelative(true)
                    .OnComplete(() =>
                    {
                        transform.gameObject.SetActive(false);
                    });
            }
        }
    }
}
