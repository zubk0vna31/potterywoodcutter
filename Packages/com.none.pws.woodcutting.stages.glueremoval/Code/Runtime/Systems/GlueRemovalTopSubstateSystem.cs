using DG.Tweening;
using Leopotam.Ecs;
using Modules.ViewHub;
using PWS.Common.GameModeInfoService;
using PWS.Common.UI;
using PWS.WoodCutting.Common;
using System.Collections.Generic;
using UnityEngine;

namespace PWS.WoodCutting.Stages.GlueRemoval
{
    public class GlueRemovalTopSubstateSystem : RunSubstateSystem<GlueRemovalState,
        GlueRemovalSubstates, GlueRemovalStateConfig>
    {
        // auto-inject fields
        private readonly EcsFilter<GlueSideToRemove, UnityView> _glueSides;
        private readonly EcsFilter<StateWindow,StateProgress> _stateWindowWithProgress;
        private readonly EcsFilter<NextStateSignal> _nextStateSignal;
        private readonly EcsFilter<PlankParent,UnityView> _plankParent;
        private readonly EcsFilter<GluePusher> _gluePusher;

        private readonly Transform _planksTransform, _stateTransform;
        private readonly IGlueRemovalDataHolder _glueRemovalDataHolder;
        private readonly IGameModeInfoService _gameModeInfoService;

        private readonly int _order;

        public GlueRemovalTopSubstateSystem(StateConfig stateConfig, GlueRemovalSubstates currentSubstate,
            Transform plankTransform,Transform stateTransform,
            IGlueRemovalDataHolder glueRemovalDataHolder,IGameModeInfoService gameModeInfoService) : base(stateConfig, currentSubstate)
        {
            _planksTransform = plankTransform;
            _stateTransform = stateTransform;
            _glueRemovalDataHolder = glueRemovalDataHolder;
            _gameModeInfoService = gameModeInfoService;

            _order = 0;
        }

        protected override void OnSubstateEnter()
        {
            base.OnSubstateEnter();

            // Change button title
            foreach (var i in _stateWindowWithProgress)
            {
                _stateWindowWithProgress.Get1(i).Template.ChangeButtonTitle(_config.buttonSubstageTitleTerm);
                _stateWindowWithProgress.Get2(i).SkipThreshold =
                    _gameModeInfoService.CurrentMode.Equals(GameMode.Free)? 0.0f: _config.skipTreshold*0.5f;
            }

            // Initialize glue data holder
            List<EcsEntity> _glueSidesToRemove = new List<EcsEntity>();

            foreach (var i in _glueSides)
            {
                if (Vector3.Dot(_planksTransform.up, _glueSides.Get2(i).Transform.up) < 0f)
                {
                    _glueSides.Get1(i).order = 1;
                }
                else
                {
                    _glueSides.Get1(i).order = 0;
                }

                _glueRemovalDataHolder.AddEntity(_glueSides.GetEntity(i));
            }


            StickyObject[] stickyObjects = new StickyObject[2];
            for (int i = 0, j = 0; i < 4; i += 2, j++)
            {
                stickyObjects = new StickyObject[2];
                stickyObjects[0] = _stateTransform.GetChild(i).GetComponent<StickyObject>();
                stickyObjects[1] = _stateTransform.GetChild(i + 1).GetComponent<StickyObject>();
                _glueRemovalDataHolder.AddStickyObjects(j, stickyObjects);
            }

            ToggleStickyObjects(true);
            ToggleGlueSides(true);
        }

        protected override void OnSubstateUpdate()
        {
            base.OnSubstateUpdate();

            if (!_nextStateSignal.IsEmpty())
            {
                ToggleStickyObjects(false);
                ToggleGlueSides(false);

                // Reset button title and increase skip threshold
                foreach (var i in _stateWindowWithProgress)
                {
                    _stateWindowWithProgress.Get1(i).Template.HideButtons();
                    _stateWindowWithProgress.GetEntity(i).Get<Ignore>();
                    _stateWindowWithProgress.Get1(i).Template.ResetButtonTitle();
                    _stateWindowWithProgress.Get2(i).SkipThreshold = _gameModeInfoService.CurrentMode.Equals(GameMode.Free)?
                        0f:
                        Mathf.Clamp01(_stateWindowWithProgress.Get2(i).Value + _config.skipTreshold * 0.5f);
                }

                // Reset glue pusher
                foreach (var i in _gluePusher)
                {
                    _gluePusher.Get1(i).gluePushSystem.Clear();
                }

                // Rotate plank parent
                foreach (var i in _plankParent)
                {
                    _plankParent.Get2(i).Transform.DORotateQuaternion(Quaternion.AngleAxis(
                        180f, _planksTransform.right) * _plankParent.Get2(i).Transform.rotation,
                        _config.switchSideDuration);
                }

                // Change substate
                ChangeSubstate(GlueRemovalSubstates.Bot, _config.switchSideDuration);
            }
        }

        private void ToggleStickyObjects(bool value)
        {
            var stickyObjects = _glueRemovalDataHolder.GetStickyObjectsByOrder(_order);

            foreach (var sticky in stickyObjects)
            {
                sticky.Toggle(value);
            }
        }

        private void ToggleGlueSides(bool value)
        {
            ref var current = ref _glueRemovalDataHolder.GetEntityByOrder(_order).Get<GlueSideToRemove>();
            var transform = _glueRemovalDataHolder.GetEntityByOrder(_order).Get<UnityView>().Transform;

            foreach (var glue in current.glueMeshRenderer)
            {
                glue.ToggleCollider(value);
                if (value)
                {
                    glue.PushDirection = transform.up;
                }
            }
        }
    }
}
