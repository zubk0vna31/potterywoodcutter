using Leopotam.Ecs;
using Modules.StateGroup.Components;
using Modules.ViewHub;
using System.Collections.Generic;
using UnityEngine;

namespace PWS.WoodCutting
{
    public class WoodCuttingGlueRemovalStateEnterSystem<T> : IEcsRunSystem where T : struct
    {
        private readonly EcsFilter<StateEnter,T> _stateEnter;

        private readonly EcsFilter<PlankComponent, PlankGlued, UnityView> _planks;
        private readonly EcsFilter<GlueToRemove, UnityView> _glueToRemove;
        private readonly EcsFilter<GlueSideToRemove, UnityView> _glueSideToRemove;

        private readonly Transform _stageTransform;

        // runtime data
        private HashSet<int> checkedGluesId;

        public WoodCuttingGlueRemovalStateEnterSystem(Transform stageTransform)
        {
            _stageTransform = stageTransform;
        }

        public void Run()
        {
            if (_stateEnter.IsEmpty()) return;

            checkedGluesId = new HashSet<int>();

            foreach (var i in _planks)
            {
                _planks.Get2(i).glueHolder.gameObject.SetActive(false);
            }

            foreach (var i in _glueSideToRemove)
            {
                Transform transform = _glueSideToRemove.Get2(i).Transform;

                //Update parent
                transform.SetParent(_stageTransform);
                transform.rotation = Quaternion.LookRotation(_stageTransform.forward, transform.up);

                //Update locas pos
                transform.localPosition = Vector3.up * ((0.015f -
                    _glueSideToRemove.Get1(i).glueMeshRenderer[0].BaseRadius * 2f)*Mathf.Sign(Vector3.Dot(
                        _stageTransform.up,transform.up)));

                for (int j = 0; j < transform.childCount; j++)
                {
                    var child = transform.GetChild(j);

                    if (j % 2 == 0)
                    {
                        child.transform.localPosition = Vector3.right * (0.05f);
                    }
                    else
                    {
                        child.transform.localPosition = Vector3.left * (0.05f);
                    }
                }
            }

            foreach (var i in _glueToRemove)
            {
                if (checkedGluesId.Contains(_glueToRemove.Get1(i).glueID)) continue;

                ref var glue = ref _glueToRemove.Get1(i);
                ref var transform = ref _glueToRemove.Get2(i).Transform;

                Vector3 start = transform.position + transform.forward * 0.75f;
                Vector3 end = transform.position - transform.forward * 0.75f;

                float glueBaseRadius = glue.glueMeshCreatorMain.GlueMeshRenderer.BaseRadius;

                Vector3 offset = transform.parent.up * (glueBaseRadius);

                glue.glueMeshCreatorMain.GlueMeshRenderer.useLocalSpace = true;
                glue.glueMeshCreatorLinked.GlueMeshRenderer.useLocalSpace = true;

                glue.glueMeshCreatorMain.Create(transform.InverseTransformPoint(start + offset),
                    transform.InverseTransformPoint(end + offset));

                float angleOffset = Mathf.Sin(Mathf.Deg2Rad * 45f);
                offset = -transform.parent.up * (glueBaseRadius *angleOffset+0.001f);
                glue.glueMeshCreatorLinked.Create(transform.InverseTransformPoint(start + offset),
                    transform.InverseTransformPoint(end + offset));

                //Dissable each for now
                glue.glueMeshCreatorMain.GlueMeshRenderer.ShouldUpdateCollider = false;
                glue.glueMeshCreatorLinked.GlueMeshRenderer.ShouldUpdateCollider = false;

                glue.glueMeshCreatorMain.GlueMeshRenderer.ToggleCollider(false);
                glue.glueMeshCreatorLinked.GlueMeshRenderer.ToggleCollider(false);

                checkedGluesId.Add(glue.glueID);
            }
        }
    }
}
