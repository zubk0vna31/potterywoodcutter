using DG.Tweening;
using Leopotam.Ecs;
using Modules.StateGroup.Components;
using Modules.StateGroup.Core;
using Modules.ViewHub;
using PWS.Common;
using PWS.Common.GameModeInfoService;
using PWS.Common.UI;
using PWS.WoodCutting.Common;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace PWS.WoodCutting
{
    public class GlueRemovalSideChangerSystem<T> : IEcsRunSystem where T : struct
    {
        private readonly EcsFilter<T> _state;
        private readonly EcsFilter<StateEnter, T> _stateEnter;

        private readonly EcsFilter<GlueSideToRemove, UnityView> _glueSides;
        private readonly EcsFilter<PlankParent, UnityView> _plankParent;
        private readonly EcsFilter<NextStateSignal> _nextStateSignal;
        private readonly EcsFilter<GluePusher> _gluePusher;
        private readonly EcsFilter<StateWindow,StateProgress> _stateWindowWithProgress;

        private readonly Modules.Utils.TimeService _timeService;
        private readonly StateFactory _stateFactory;
        private readonly EcsWorld _ecsWorld;

        private readonly IGameModeInfoService _gameModeInfoService;

        private readonly GlueRemovalStateConfig _stateConfig;
        private readonly Transform _planksTransform,_stateTransform;
        private Dictionary<int, StickyObject[]> stickyObjects;

        // runtime data
        private EcsEntity current;
        private float _timer;
        private bool _allCompleted;
        private bool _initialized;
        private bool _active;
        private int _currentItteration;

        public GlueRemovalSideChangerSystem(StateConfig stateConfig, Transform planksTransform,
            Transform stateTransform, IGameModeInfoService gameModeInfoService)
        {
            _stateConfig = stateConfig as GlueRemovalStateConfig;
            _planksTransform = planksTransform;
            _stateTransform = stateTransform;
            _gameModeInfoService = gameModeInfoService;

            stickyObjects = new Dictionary<int, StickyObject[]>();
        }

        public void Run()
        {
            //if (_state.IsEmpty() || _allCompleted) return;

            //OnStateEnter();
            //HandleTimer();

            //if (!_nextStateSignal.IsEmpty())
            //{
            //    Next();
            //}
        }

        //private void OnStateEnter()
        //{
        //    if (!_stateEnter.IsEmpty())
        //    {
        //        _initialized = false;
        //        _currentItteration = 0;
        //        _allCompleted = false;
        //        _active = true;
        //        _timer = _stateConfig.checkSuccessInterval;

        //        foreach (var i in _stateWindowWithProgress)
        //        {
        //            _stateWindowWithProgress.Get1(i).Template.ChangeButtonTitle(_stateConfig.buttonSubstageTitle);
        //        }

        //        ToggleButtonVI(true, false);

        //        List<EcsEntity> _glueSidesToRemove = new List<EcsEntity>();

        //        foreach (var i in _glueSides)
        //        {
        //            if (Vector3.Dot(_planksTransform.up, _glueSides.Get2(i).Transform.up) < 0f)
        //            {
        //                _glueSides.Get1(i).order = 1;
        //            }
        //            else
        //            {
        //                _glueSides.Get1(i).order = 0;
        //            }

        //            _glueSidesToRemove.Add(_glueSides.GetEntity(i));
        //        }

        //        _glueSidesToRemove = _glueSidesToRemove.OrderBy(x => x.Get<GlueSideToRemove>().order).ToList();

        //        int counter = 0;
        //        EcsEntity entity = EcsEntity.Null;
        //        foreach (var side in _glueSidesToRemove)
        //        {
        //            if (counter == 0)
        //            {
        //                current = entity = side;
        //                counter++;
        //                continue;
        //            }

        //            entity.Get<GlueSideToRemove>().next = side;
        //            entity = side;
        //            counter++;
        //        }

        //        Next();
        //    }
        //}

        //private void HandleTimer()
        //{
        //    if (!_active) return;

        //    if (_timer > 0f)
        //    {
        //        _timer -= _timeService.DeltaTime;

        //        if (_timer <= 0f)
        //        {
        //            if (current != EcsEntity.Null && IsCompleted())
        //            {
        //                ToggleButtonVI(true, true);
        //            }
        //            else _timer = _stateConfig.checkSuccessInterval;
        //        }
        //    }
        //}

        //private void Next()
        //{
        //    if (current == EcsEntity.Null)
        //    {
        //        // Grade 
        //        float grade = 0f;
        //        foreach (var i in _stateWindowWithProgress)
        //        {
        //            grade = _stateWindowWithProgress.Get2(i).ProgressValue;
        //        }
        //        _ecsWorld.NewEntity().Get<SetGradeSignal>().grade = grade;

        //        ToggleButtonVI(false, true);
        //        PushClayWithDuration();
        //        return;
        //    }

        //    ref var glueSide = ref current.Get<GlueSideToRemove>();

        //    if (IsCompleted(true))
        //    {
        //        Toggle(ref glueSide, false);

        //        foreach (var i in _gluePusher)
        //        {
        //            _gluePusher.Get1(i).gluePushSystem.Clear();
        //        }

        //        Rotate(glueSide.order);
        //    }
        //    else
        //    {
        //        if (!_initialized)
        //        {
        //            _initialized = true;

        //            bool callbackAdded = false;

        //            foreach (var i in _glueSides)
        //            {
        //                var transform = _glueSides.Get2(i).Transform;
        //                var baseRadius = _glueSides.Get1(i).glueMeshRenderer[0].BaseRadius;

        //                Vector3 offset = transform.up * baseRadius * 2;

        //                if (!callbackAdded)
        //                {
        //                    callbackAdded = true;
        //                    transform.DOMove(offset, 1f).SetRelative(true).OnComplete(() =>
        //                     {
        //                         Next();
        //                     });
        //                }
        //                else
        //                {
        //                    transform.DOMove(offset, 1f).SetRelative(true);
        //                }
        //            }


        //            StickyObject[] stickyObjects = new StickyObject[2];
        //            for (int i = 0,j=0; i < 4; i+=2,j++)
        //            {
        //                stickyObjects = new StickyObject[2];
        //                stickyObjects[0] = _stateTransform.GetChild(i).GetComponent<StickyObject>();
        //                stickyObjects[1] = _stateTransform.GetChild(i+1).GetComponent<StickyObject>();
        //                this.stickyObjects.Add(j, stickyObjects);
        //            }
        //            return;
        //        }
        //        else Toggle(ref glueSide, true);
        //    }
        //}

        //private void PushClayWithDuration()
        //{
        //    ToggleButtonVI(false, false);

        //    float pushAmount = 0f;

        //    GluePushSystem pushSystem = null;

        //    foreach (var i in _gluePusher)
        //    {
        //        pushAmount = _gluePusher.Get1(i).gluePushSystem.PushStrength;
        //        pushSystem = _gluePusher.Get1(i).gluePushSystem;
        //        break;
        //    }

        //    foreach (var i in _glueSides)
        //    {
        //        ref var side = ref _glueSides.Get1(i);

        //        foreach (var mesh in side.glueMeshRenderer)
        //        {
        //            mesh.PushAll(pushAmount);
        //            mesh.ToggleCollider(false);
        //        }
        //    }

        //    pushSystem.Clear();

        //    bool callbackAdded = false;

        //    foreach (var i in _glueSides)
        //    {
        //        var transform = _glueSides.Get2(i).Transform;
        //        float baseRadius = _glueSides.Get1(i).glueMeshRenderer[0].BaseRadius;

        //        var entity = _glueSides.GetEntity(i);

        //        if (!callbackAdded)
        //        {
        //            callbackAdded = true;
        //            transform.DOLocalMove(transform.up * (baseRadius * 2.1f), 0.75f).SetRelative(true)
        //                .OnComplete(() =>
        //                {
        //                    entity.Get<UnityView>().GameObject.SetActive(false);
        //                    _stateConfig.nextState.Execute(_stateFactory);
        //                });
        //        }
        //        else
        //        {
        //            transform.DOLocalMove(transform.up * (baseRadius * 2.1f), 0.75f).SetRelative(true)
        //                .OnComplete(() => entity.Get<UnityView>().GameObject.SetActive(false));
        //        }
        //    }
        //}

        //private void ToggleButtonVI(bool v,bool i)
        //{
        //    foreach (var j in _stateWindowWithProgress)
        //    {
        //        _stateWindowWithProgress.Get1(j).Template.ShowButton(v);
        //        _stateWindowWithProgress.Get1(j).Template.ToggleButtonInteraction(i);
        //    }
        //}

        //private bool IsCompleted(bool updateItteration=false)
        //{
        //    if (!_initialized) return false;

        //    float p = 0;

        //    foreach (var i in _glueSides)
        //    {
        //        p += _glueSides.Get1(i).Percentage;
        //    }

        //    p /= _glueSides.GetEntitiesCount();

        //    float refPercentage = _stateConfig.skipTreshold/
        //        (_glueSides.GetEntitiesCount()-_currentItteration);

        //    if (p >= refPercentage || _gameModeInfoService.Equals(GameMode.Free))
        //    {
        //        if (_initialized && updateItteration)
        //            _currentItteration++;

        //        return true;
        //    }
        //    else return false;

        //}

        //private void Toggle(ref GlueSideToRemove side, bool value)
        //{
        //    ToggleButtonVI(true, false);

        //    foreach (var mesh in side.glueMeshRenderer)
        //    {
        //        mesh.ToggleCollider(value);
        //        if (value)
        //            mesh.PushDirection = current.Get<UnityView>().Transform.up;
        //    }

        //    if (_currentItteration < 2)
        //    {
        //        foreach (var s in stickyObjects[Mathf.Clamp(_currentItteration, 0, 1)])
        //        {
        //            s.Toggle(value);
        //        }
        //    }

        //    if (_currentItteration > 0)
        //    {
        //        foreach (var s in stickyObjects[Mathf.Clamp(_currentItteration - 1, 0, 1)])
        //        {
        //            s.Toggle(false);
        //        }
        //    }

        //    if (value)
        //    {
        //        _timer = _stateConfig.checkSuccessInterval;
        //    }
        //}

        //private void Rotate(int order)
        //{
        //    if (order == 1)
        //    {
        //        _allCompleted = true;
        //        current = EcsEntity.Null;
        //        Next();
        //        return;
        //    }

        //    foreach (var i in _stateWindowWithProgress)
        //    {
        //        _stateWindowWithProgress.Get1(i).Template.ResetButtonTitle();
        //    }

        //    ToggleButtonVI(true, false);

        //    current = current.Get<GlueSideToRemove>().next;

        //    foreach (var i in _plankParent)
        //    {
        //        _plankParent.Get2(i).Transform.DORotateQuaternion(Quaternion.AngleAxis(
        //            180f, _planksTransform.right) * _plankParent.Get2(i).Transform.rotation,
        //            _stateConfig.switchSideDuration)
        //            .OnComplete(() => Next());
        //    }

        //}
    }
}
