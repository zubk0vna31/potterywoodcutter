using DG.Tweening;
using Leopotam.Ecs;
using Modules.ViewHub;
using PWS.WoodCutting.Common;
using System.Collections.Generic;
using UnityEngine;

namespace PWS.WoodCutting.Stages.GlueRemoval
{
    public class GlueRemovalPrepareSubstateSystem : RunSubstateSystem<GlueRemovalState,
        GlueRemovalSubstates,GlueRemovalStateConfig>
    {
        private readonly EcsFilter<PlankComponent, PlankGlued, UnityView> _planks;

        private readonly EcsFilter<GlueSideToRemove, UnityView> _glueSides;
        private readonly EcsFilter<GlueToRemove, UnityView> _glueToRemove;

        private readonly Transform _stateTransform;

        // runtime data
        private HashSet<int> checkedGluesId;

        public GlueRemovalPrepareSubstateSystem(StateConfig stateConfig, GlueRemovalSubstates currentSubstate,
            Transform stateTransform) : base(stateConfig, currentSubstate)
        {
            _stateTransform = stateTransform;
        }

        protected override void OnSubstateEnter()
        {
            base.OnSubstateEnter();

            // Generate Mesh
            GenerateMesh();

            // Move mesh onto plank surface
            foreach (var i in _glueSides)
            {
                var transform = _glueSides.Get2(i).Transform;
                var baseRadius = _glueSides.Get1(i).glueMeshRenderer[0].BaseRadius;

                Vector3 offset = transform.up * baseRadius * 2;

                transform.DOMove(offset, 1f).SetRelative(true);
            }

            ChangeSubstate(GlueRemovalSubstates.Top, _config.glueMoveOntoSurfaceDuration);
        }

        private void GenerateMesh()
        {
            checkedGluesId = new HashSet<int>();

            foreach (var i in _planks)
            {
                _planks.Get2(i).glueHolder.gameObject.SetActive(false);
            }

            foreach (var i in _glueSides)
            {
                Transform transform = _glueSides.Get2(i).Transform;

                //Update parent
                transform.SetParent(_stateTransform);
                transform.rotation = Quaternion.LookRotation(_stateTransform.forward, transform.up);

                //Update locas pos
                transform.localPosition = Vector3.up * ((0.015f -
                    _glueSides.Get1(i).glueMeshRenderer[0].BaseRadius * 2f) * Mathf.Sign(Vector3.Dot(
                        _stateTransform.up, transform.up)));

                for (int j = 0; j < transform.childCount; j++)
                {
                    var child = transform.GetChild(j);

                    if (j % 2 == 0)
                    {
                        child.transform.localPosition = Vector3.right * (0.05f);
                    }
                    else
                    {
                        child.transform.localPosition = Vector3.left * (0.05f);
                    }
                }
            }

            foreach (var i in _glueToRemove)
            {
                if (checkedGluesId.Contains(_glueToRemove.Get1(i).glueID)) continue;

                ref var glue = ref _glueToRemove.Get1(i);
                ref var transform = ref _glueToRemove.Get2(i).Transform;

                Vector3 start = transform.position + transform.forward * 0.75f;
                Vector3 end = transform.position - transform.forward * 0.75f;

                float glueBaseRadius = glue.glueMeshCreatorMain.GlueMeshRenderer.BaseRadius;

                Vector3 offset = transform.parent.up * (glueBaseRadius);

                glue.glueMeshCreatorMain.GlueMeshRenderer.useLocalSpace = true;
                glue.glueMeshCreatorLinked.GlueMeshRenderer.useLocalSpace = true;

                glue.glueMeshCreatorMain.Create(transform.InverseTransformPoint(start + offset),
                    transform.InverseTransformPoint(end + offset));

                float angleOffset = Mathf.Sin(Mathf.Deg2Rad * 45f);
                offset = -transform.parent.up * (glueBaseRadius * angleOffset + 0.001f);
                glue.glueMeshCreatorLinked.Create(transform.InverseTransformPoint(start + offset),
                    transform.InverseTransformPoint(end + offset));

                //Dissable each for now
                glue.glueMeshCreatorMain.GlueMeshRenderer.ShouldUpdateCollider = false;
                glue.glueMeshCreatorLinked.GlueMeshRenderer.ShouldUpdateCollider = false;

                glue.glueMeshCreatorMain.GlueMeshRenderer.ToggleCollider(false);
                glue.glueMeshCreatorLinked.GlueMeshRenderer.ToggleCollider(false);

                checkedGluesId.Add(glue.glueID);
            }
        }

    }
}
