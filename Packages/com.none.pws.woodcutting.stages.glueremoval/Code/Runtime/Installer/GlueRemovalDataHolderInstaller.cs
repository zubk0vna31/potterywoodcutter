using Modules.Root.ContainerComponentModel;

namespace PWS.WoodCutting.Stages.GlueRemoval
{
    public class GlueRemovalDataHolderInstaller : AMonoInstaller
    {
        public override void Install(IContainer container)
        {
            IGlueRemovalDataHolder itemDataHolder = new GlueRemovalDataHolder();

            container.Bind(itemDataHolder);
        }

       
    }
}
