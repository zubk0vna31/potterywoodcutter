using Leopotam.Ecs;
using System.Collections.Generic;
using UnityEngine;

namespace PWS.WoodCutting.Stages.GlueRemoval
{
    public interface IGlueRemovalDataHolder 
    {
        List<EcsEntity> entities { get; }
        Dictionary<int,StickyObject [] > stickyObjects { get; }

        public void AddEntity(EcsEntity entity);

        public void AddStickyObjects(int key, StickyObject [] value);

        public EcsEntity GetEntityByOrder(int order);

        public StickyObject[] GetStickyObjectsByOrder(int order);
    }
}
