using Leopotam.Ecs;
using System.Collections.Generic;

namespace PWS.WoodCutting.Stages.GlueRemoval
{
    public class GlueRemovalDataHolder : IGlueRemovalDataHolder
    {
        private List<EcsEntity> _entities = new List<EcsEntity>(10);
        public List<EcsEntity> entities => _entities;

        private Dictionary<int, StickyObject []> _stickyObjects = new Dictionary<int, StickyObject[]>(10);

        public Dictionary<int, StickyObject[]> stickyObjects => _stickyObjects;

        public void AddEntity(EcsEntity entity)
        {
            _entities.Add(entity);
        }

        public void AddStickyObjects(int key, StickyObject[] value)
        {
            _stickyObjects[key] = value;
        }

        public EcsEntity GetEntityByOrder(int order)
        {
            foreach (var entity in _entities)
            {
                if (entity.Get<GlueSideToRemove>().order == order) return entity;
            }

            return EcsEntity.Null;
        }

        public StickyObject[] GetStickyObjectsByOrder(int order)
        {
            return _stickyObjects[order];
        }
    }
}
