using Modules.StateGroup.Core;
using PWS.WoodCutting.Common;
using UnityEngine;

namespace PWS.WoodCutting
{
    [CreateAssetMenu(fileName = "Glue Removal Stage Config",
        menuName = "PWS/WoodCutting/Stages/Glue Removal/State Config")]
    public class GlueRemovalStateConfig : StateConfig
    {
        [Header("String")]
        public string buttonSubstageTitleTerm;
        public string buttonSubstageTitle;

        [Range(0f,1f)]
        public float skipTreshold = 0.75f;
        [Range(0f, 5f)]
        public float skipDuration = 0.75f;
        [Range(0f, 5f)]
        public float glueMoveOntoSurfaceDuration = 1f;

        [Range(0f, 5f)]
        public float checkSuccessInterval = 1f;

        [Range(0f, 5f)]
        public float switchSideDuration = 0.5f;
    }
}
