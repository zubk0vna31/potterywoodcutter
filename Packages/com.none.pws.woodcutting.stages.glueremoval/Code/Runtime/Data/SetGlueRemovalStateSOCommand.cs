using Modules.StateGroup.Core;
using UnityEngine;

namespace PWS.WoodCutting
{
    [CreateAssetMenu(menuName = "PWS/WoodCutting/Stages/Glue Removal/SetStateSOCommand")]
    public class SetGlueRemovalStateSOCommand : SetStateSOCommand<GlueRemovalState>
    {

    }
}
