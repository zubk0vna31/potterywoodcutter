﻿using System;
using Leopotam.Ecs;
using Modules.ViewHub;
using UnityEngine;

namespace PWS.WoodCutting
{
    public class GlueRemovalViewComponent : ViewComponent
    {
        private EcsEntity _entity;
        
        [SerializeField] private TriggerListenerActionBased _triggerListener;

        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            _entity = ecsEntity;
            _triggerListener.triggerEnter += SetSignal;
        }

        private void SetSignal(Transform self, Transform other)
        {
            if (other.CompareTag("Glue"))
                _entity.Get<GlueRemovalSignal>();
        }
    }
}