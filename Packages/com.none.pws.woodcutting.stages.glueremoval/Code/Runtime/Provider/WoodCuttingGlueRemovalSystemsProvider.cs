using Leopotam.Ecs;
using Modules.Root.ContainerComponentModel;
using Modules.Root.ECS;
using PWS.Common.Audio;
using PWS.Common.GameModeInfoService;
using PWS.Common.ResultsEvaluation;
using PWS.Common.TravelPoints;
using PWS.Common.UI;
using PWS.WoodCutting.Common;
using PWS.WoodCutting.Stages.Toning;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace PWS.WoodCutting.Stages.GlueRemoval
{
    public enum GlueRemovalSubstates
    {
        None = 0,
        Transition,
        Prepare,
        Top,
        Bot
    }

    public class WoodCuttingGlueRemovalSystemsProvider : MonoBehaviour, ISystemsProvider
    {

        internal class StateDependencies
        {
            [Inject] public IGameModeInfoService gameModeInfoService;
            [Inject] public IGlueRemovalDataHolder glueRemovalDataHolder;
            [Inject] public IResultsEvaluationDataHolder resultsEvaluation;
        }


        [Header("Config")]
        [SerializeField]
        private StateConfig _stateConfig;
        [SerializeField]
        private StateInfoData _stateInfo;

        [Header("Scene Related")]
        [SerializeField]
        private Transform _plankTransform;
        [SerializeField]
        private Transform _stateTransform;

        [Header("Voice config")]
        [SerializeField] private VoiceConfig _voiceConfig;
        [SerializeField] private XRBaseInteractable _tsikla;

        public EcsSystems GetSystems(EcsWorld world, EcsSystems endFrame, EcsSystems mainSystems)
        {
            StateDependencies dependecies = new StateDependencies();
            SceneContainer.Instance.Inject(dependecies);

            EcsSystems systems = new EcsSystems(world, "WoodCutting Glue Removal");

            systems

                .Add(new SetStateDelayedSystem<GlueRemovalState>())

                .Add(new OutlineByStateSystem<GlueRemovalState>(_stateConfig))


                 .Add(new SubstateSystem
                <GlueRemovalState, GlueRemovalSubstates, GlueRemovalStateConfig>
                (_stateConfig,
                GlueRemovalSubstates.Prepare,
                GlueRemovalSubstates.Transition,
                GlueRemovalSubstates.None))

                .Add(new GlueRemovalPrepareSubstateSystem(_stateConfig, GlueRemovalSubstates.Prepare,
                _stateTransform))

                 .Add(new GlueRemovalTopSubstateSystem(_stateConfig, GlueRemovalSubstates.Top,
                 _plankTransform, _stateTransform, dependecies.glueRemovalDataHolder,
                    dependecies.gameModeInfoService))

                 .Add(new GlueRemovalBotSubstateSystem(_stateConfig, GlueRemovalSubstates.Bot,
                    dependecies.glueRemovalDataHolder))

                //.Add(new GlueRemovalSideChangerSystem<GlueRemovalState>(_stateConfig,
                //_plankTransform, _stateTransform, dependecies.gameModeInfoService))

                //Tracker
                .Add(new GlueRemovalProcessTracker(_stateConfig))
                .Add(new StateInfoWindowUIProcessing<GlueRemovalState>(_stateInfo))

                 .Add(new WoodCuttingSetGradeByProgress<GlueRemovalState>(dependecies.resultsEvaluation,
                _stateInfo))


                .Add(new StateWindowPlacementProcessing<GlueRemovalState>("Main"))


                .Add(new TravelPointPositioningSystem<GlueRemovalState>("Default"))


                .Add(new SubstateUIProcessingSystem<GlueRemovalState, GlueRemovalSubstates>
                (GlueRemovalSubstates.Top))
                .Add(new SubstateUIProcessingSystem<GlueRemovalState, GlueRemovalSubstates>
                (GlueRemovalSubstates.Bot))
                .Add(new SubstateUIProcessingSystem<GlueRemovalState, GlueRemovalSubstates>
                (GlueRemovalSubstates.None))

                .Add(new SubstateButtonUIProcessing<GlueRemovalState, GlueRemovalSubstates>
                (GlueRemovalSubstates.Top))
                .Add(new SubstateButtonUIProcessing<GlueRemovalState, GlueRemovalSubstates>
                (GlueRemovalSubstates.Bot))


                //Voice
                .Add(new VoiceInteractableAudioSystem<GlueRemovalState>(_voiceConfig, _tsikla))

                ;

            return systems;
        }


    }
}
