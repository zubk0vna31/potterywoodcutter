using Leopotam.Ecs;
using Modules.ViewHub;
using UnityEngine;

namespace PWS.WoodCutting
{
    public class GlueToRemoveTemplate : EntityTemplate
    {
        [SerializeField, Range(0, 128)]
        private int glueID;

        [SerializeField,Range(0,128)]
        private int lineDivisionCount = 64;

        [SerializeField, Range(0f, 1f)]
        private float linkedPushAmountPower = 0.1f;

        [SerializeField]
        private GlueMeshCreator glueMeshCreatorMain,glueMeshCreatorLinked;

        [SerializeField]
        private GlueMeshRenderer glueMeshRendererMain, glueMeshRendererLinked;

        [SerializeField]
        private SplineCreator glueSplineCreatorMain, glueSplineCreatorLinked;

        protected override void SetupEntity(EcsEntity entity, EcsWorld world)
        {
            base.SetupEntity(entity, world);

            ref var glue = ref entity.Get<GlueToRemove>();


            glueMeshCreatorMain.Initialize(glueSplineCreatorMain, glueMeshRendererMain,lineDivisionCount);
            glueMeshCreatorLinked.Initialize(glueSplineCreatorLinked, glueMeshRendererLinked,lineDivisionCount);

            glue.glueMeshCreatorMain = glueMeshCreatorMain;
            glue.glueMeshCreatorLinked = glueMeshCreatorLinked;
            glue.glueID = glueID;

            Link(glueMeshRendererMain, glueMeshRendererLinked);
        }

        private void Link(GlueMeshRenderer main, GlueMeshRenderer linked)
        {
            main.OnGlueMeshModified += ModifyLinked;
        }

        private void ModifyLinked(int index, Vector3 direction, float strength)
        {
            glueMeshRendererLinked.Push(index, direction, strength* linkedPushAmountPower);

            if (glueMeshRendererLinked.FullyPushed)
            {
                glueMeshRendererLinked.OnGlueMeshModified -= ModifyLinked;

                glueMeshRendererMain.gameObject.SetActive(false);
            }
        }
    }
}
