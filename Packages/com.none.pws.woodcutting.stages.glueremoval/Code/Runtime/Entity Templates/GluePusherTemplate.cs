using Leopotam.Ecs;
using Modules.ViewHub;
using UnityEngine;

namespace PWS.WoodCutting
{
    public class GluePusherTemplate : EntityTemplate
    {
        [SerializeField]
        private GluePushSystem gluePushSystem;

        protected override void SetupEntity(EcsEntity entity, EcsWorld world)
        {
            base.SetupEntity(entity, world);
            entity.Get<GluePusher>().gluePushSystem = gluePushSystem;
        }
    }
}
