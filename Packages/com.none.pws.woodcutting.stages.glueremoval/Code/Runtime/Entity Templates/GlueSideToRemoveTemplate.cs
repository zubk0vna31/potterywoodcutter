using Leopotam.Ecs;
using Modules.ViewHub;
using System.Collections.Generic;
using UnityEngine;

namespace PWS.WoodCutting
{
    public class GlueSideToRemoveTemplate : EntityTemplate
    {
        [SerializeField]
        private List<GlueMeshRenderer> glueMeshRenderers;

        protected override void SetupEntity(EcsEntity entity, EcsWorld world)
        {
            base.SetupEntity(entity, world);

            ref var side = ref entity.Get<GlueSideToRemove>();

            side.glueMeshRenderer = glueMeshRenderers;
        }
    }
}
