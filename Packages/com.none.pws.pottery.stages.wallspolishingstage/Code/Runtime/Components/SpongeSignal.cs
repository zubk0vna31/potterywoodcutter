using UnityEngine;

namespace PWS.Pottery.Stages.WallsPolishingStage
{
    public struct SpongeSignal
    {
        public Vector3 CollisionPoint;
        public Vector3 SpongePoint;
    }
}
