using Modules.StateGroup.Core;
using UnityEngine;

namespace PWS.Pottery.Stages.WallsPolishingStage
{
    [CreateAssetMenu(fileName = "SetWallsPolishingStateSOCommand", menuName = "PWS/Pottery/Stages/WallsPolishingStage/SetStateSOCommand")]
    public class SetWallsPolishingStateSOCommand : SetStateSOCommand<WallsPolishingState>
    {

    }
}