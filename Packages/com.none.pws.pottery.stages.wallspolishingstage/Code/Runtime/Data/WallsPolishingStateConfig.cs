using UnityEngine;

namespace PWS.Pottery.Stages.WallsPolishingStage
{
    [CreateAssetMenu(fileName = "WallsPolishingStateConfig", menuName = "PWS/Pottery/Stages/WallsPolishingStage/Config", order = 0)]
    public class WallsPolishingStateConfig : ScriptableObject
    {
        public float CompletionThresold = 0.9f;
        public float SkipThreshold = 0.75f;

        public float SpongeRadius = 0.1f;
        public float PolishingSpeed = 2;
    }
}
