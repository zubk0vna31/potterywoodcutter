using Leopotam.Ecs;
using Modules.Root.ContainerComponentModel;
using Modules.Root.ECS;
using Modules.StateGroup.Core;
using PWS.Common.Audio;
using PWS.Pottery.Common;
using PWS.Pottery.ItemDataHolderService;
using PWS.Pottery.MeshGeneration;
using UnityEngine;
using PWS.Common.Messages;
using PWS.Common.RestartStage;
using PWS.Common.UI;
using PWS.Common.ResultsEvaluation;
using PWS.Common.TravelPoints;

namespace PWS.Pottery.Stages.WallsPolishingStage
{
    public class PotteryWallsPolishingSystemsProvider : MonoBehaviour, ISystemsProvider
    {
        internal class SystemsDependencies
        {
            [Inject] public ISculptingDataHolder SculptingData;
            [Inject] public IResultsEvaluationDataHolder ResultsEvaluationData;
        }

        [SerializeField] private SetStateSOCommand _nextState;
        [SerializeField] private WallsPolishingStateConfig _config;
        [SerializeField] private StateInfoData _stateInfo;
        
        [Header("Voice config")] 
        [SerializeField] private VoiceConfig _voiceConfig;

        public EcsSystems GetSystems(EcsWorld world, EcsSystems endFrame, EcsSystems mainSystems)
        {
            SystemsDependencies dependencies = new SystemsDependencies();
            SceneContainer.Instance.Inject(dependencies);

            EcsSystems systems = new EcsSystems(world);
            systems
                .Add(new TransitToStateOnUIActionSystem<WallsPolishingState>(_nextState))
                .Add(new WallsPolishingSkipProcessing(dependencies.SculptingData, _nextState))

                //restart
                .Add(new CurrentStateRestoreDataProcessing<WallsPolishingState, SculptingDataHolder>(dependencies.SculptingData))

                //show sculpting inside
                .Add(new ProductMeshAngleRangeProcessing<WallsPolishingState, SculptingProductTag>(360, 1, dependencies.SculptingData))

                //state systems
                .Add(new WallsPolishingInteractorHandler(dependencies.SculptingData, _config))
                .Add(new WallsPolishingCompletionTracker(dependencies.SculptingData, _config))

                //Outline
                .Add(new OutlineByStateSystem<WallsPolishingState>(1))

                .Add(new SetGradeByProgress<WallsPolishingState>(dependencies.ResultsEvaluationData, _stateInfo))

                //ui
                .Add(new StateInfoWindowUIProcessing<WallsPolishingState>(_stateInfo))

                .Add(new StateWindowPlacementProcessing<WallsPolishingState>("Wheel"))
                .Add(new StateWindowProgressUIProcessing<WallsPolishingState>())
                .Add(new StateWindowButtonUIProcessing<WallsPolishingState>())
                .Add(new SkipStageButtonUIProcessing<WallsPolishingState>())

                .Add(new MeshVisibilityProcessing<WallsPolishingState, SculptingProductMesherTag>())
                
                //voice
                .Add(new VoiceAudioSystem<WallsPolishingState>(_voiceConfig))

                //travel point
                .Add(new TravelPointPositioningSystem<WallsPolishingState>("Wheel"))

                ;

            endFrame
                //sculpting product
                .Add(new SculptingDataProcessingSystem<WallsPolishingState>(dependencies.SculptingData))
                .Add(new MeshRingsDataProcessingSystem<WallsPolishingState>(dependencies.SculptingData))
                .Add(new MeshUpdateSystem<WallsPolishingState, SculptingProductMesherTag>(dependencies.SculptingData))

                .OneFrame<RestartCurrentStepMessage>()
                .OneFrame<NextStateSignal>()
                .OneFrame<SkipStageMessage>()
                .OneFrame<SpongeSignal>()
                ;

            return systems;
        }
    }
}
