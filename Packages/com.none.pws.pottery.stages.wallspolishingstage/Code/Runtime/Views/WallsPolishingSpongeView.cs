using Leopotam.Ecs;
using Modules.ViewHub;
using UnityEngine;

namespace PWS.Pottery.Stages.WallsPolishingStage
{
    public class WallsPolishingSpongeView : ViewComponent
    {
        private EcsEntity _ecsEntity;

        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            _ecsEntity = ecsEntity;
            ecsEntity.Get<WallsPolishingSponge>();
        }

        private void OnCollisionStay(Collision collision)
        {
            if (!collision.collider.tag.Equals("SculptingProduct"))
                return;

            ref var signal = ref _ecsEntity.Get<SpongeSignal>();
            signal.CollisionPoint = collision.contacts[0].point;
            signal.SpongePoint = transform.position;
        }
    }
}
