using Leopotam.Ecs;
using Modules.ViewHub;
using PWS.Pottery.ItemDataHolderService;
using PWS.Pottery.MeshGeneration;
using UnityEngine;

namespace PWS.Pottery.Stages.WallsPolishingStage
{
    public class WallsPolishingInteractorHandler : IEcsRunSystem
    {
        // auto injected fields
        readonly EcsFilter<WallsPolishingState> _inState;

        readonly EcsFilter<UnityView, WallsPolishingSponge> _sponge;
        readonly EcsFilter<SpongeSignal, WallsPolishingSponge> _spongeSignal;
        readonly EcsFilter<UnityView, SculptingProductTag> _product;

        private ISculptingDataHolder _data;
        private WallsPolishingStateConfig _config;

        public WallsPolishingInteractorHandler(ISculptingDataHolder data, WallsPolishingStateConfig config)
        {
            _data = data;
            _config = config;
        }

        public void Run()
        {
            if (_inState.IsEmpty())
                return;

            foreach (var signal in _spongeSignal)
            {
                foreach (var productIdx in _product)
                {
                    Transform product = _product.Get1(productIdx).Transform;

                    Interaction(_spongeSignal.Get1(signal).CollisionPoint, _product.Get1(productIdx).Transform.position);
                }
            }

            /*foreach (var productIdx in _product)
            {
                Transform product = _product.Get1(productIdx).Transform;
                foreach (var interactorIdx in _sponge)
                {
                    Interaction(_sponge.Get1(interactorIdx).Transform.position, _product.Get1(productIdx).Transform.position);
                }
            }*/
        }

        void Interaction(Vector3 handPosition, Vector3 productPosition)
        {
            foreach (Ring ring in _data.Rings)
            {
                Vector3 localHandPosition = handPosition - productPosition;
                float verticalDistance = Mathf.Abs(ring.Position.y - localHandPosition.y);
                float radius = _config.SpongeRadius;
                if (verticalDistance <= radius && ring.Color.r > 0)
                {
                    float multiplier = 1 - verticalDistance / radius;
                    //multiplier = _config.HandInteractionCurve.Evaluate(multiplier);

                    ring.Color.r -= multiplier * Time.deltaTime * _config.PolishingSpeed;
                    ring.Color.r = Mathf.Clamp01(ring.Color.r);
                }
            }
            _data.SetDirty();
        }

    }
}