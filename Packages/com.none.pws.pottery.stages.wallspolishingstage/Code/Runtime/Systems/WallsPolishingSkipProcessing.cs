using Leopotam.Ecs;
using Modules.StateGroup.Core;
using PWS.Common.Messages;
using PWS.Pottery.Common;
using PWS.Pottery.ItemDataHolderService;
using PWS.Pottery.MeshGeneration;

namespace PWS.Pottery.Stages.WallsPolishingStage
{
    public class WallsPolishingSkipProcessing : IEcsRunSystem
    {
        // auto injected fields
        private readonly EcsFilter<SkipStageMessage> _skipPerformed;
        readonly EcsFilter<WallsPolishingState> _inState;
        //readonly EcsFilter<ClayMeshRenderer, SculptingProductTag> _product;
        private readonly StateFactory _stateFactory;

        private ISculptingDataHolder _referenceData;
        private SetStateSOCommand _nextState;

        //runtime data
        private ISculptingDataHolder _data;

        public WallsPolishingSkipProcessing(ISculptingDataHolder data, SetStateSOCommand nextState)
        {
            _data = data;
            _nextState = nextState;
        }

        public void Run()
        {
            if (_inState.IsEmpty())
                return;
            if (_skipPerformed.IsEmpty())
                return;

            foreach (Ring ring in _data.Rings)
            {
                ring.Color.r = 0;
            }

            /*foreach (var i in _skipPerformed)
            {
                _skipPerformed.GetEntity(i).Del<SkipStageMessage>();
            }*/

            _nextState.Execute(_stateFactory);
        }
    }
}
