using Leopotam.Ecs;
using Modules.StateGroup.Components;
using PWS.Common.Messages;
using PWS.Common.UI;
using PWS.Pottery.ItemDataHolderService;
using UnityEngine;

namespace PWS.Pottery.Stages.WallsPolishingStage
{
    public class WallsPolishingCompletionTracker : IEcsRunSystem
    {
        // auto injected fields
        readonly EcsFilter<StateEnter, WallsPolishingState> _entering;
        readonly EcsFilter<WallsPolishingState>.Exclude<StateExit> _inState;
        private readonly EcsFilter<SkipStageMessage> _skipPerformed;

        readonly EcsFilter<StateWindow, StateProgress> _stateWindowWithProgress;

        private ISculptingDataHolder _data;
        private WallsPolishingStateConfig _config;

        public WallsPolishingCompletionTracker(ISculptingDataHolder data, WallsPolishingStateConfig config)
        {
            _data = data;
            _config = config;
        }

        public void Run()
        {
            if (!_entering.IsEmpty())
            {
                foreach (var i in _stateWindowWithProgress)
                {
                    _stateWindowWithProgress.Get2(i).Value = 0;
                    _stateWindowWithProgress.Get2(i).CompletionThreshold = _config.CompletionThresold;
                    _stateWindowWithProgress.Get2(i).SkipThreshold = _config.SkipThreshold;
                }
            }

            if (!_inState.IsEmpty())
            {
                if (!_skipPerformed.IsEmpty())
                    return;

                float fullCompletion = 0;
                foreach (var ring in _data.Rings)
                {
                    fullCompletion += (1 - ring.Color.r);
                }
                fullCompletion /= _data.Rings.Count;

                foreach (var i in _stateWindowWithProgress)
                {
                    _stateWindowWithProgress.Get2(i).Value = fullCompletion;
                }
            }
        }
    }
}
