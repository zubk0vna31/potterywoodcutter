using Modules.Root.ContainerComponentModel;
using UnityEngine;

namespace PWS.Common.SceneSwitchService
{
    public class SceneSwitchServiceInstaller : AMonoInstaller
    {
        [SerializeField] private SceneSwitchService _sceneSwitchService;
        
        public override void Install(IContainer container)
        {
            container.Bind<ISceneSwitchService>(_sceneSwitchService);
        }
    }
}