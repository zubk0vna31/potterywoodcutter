namespace PWS.Common.SceneSwitchService {

    public interface ISceneSwitchService {
        void LoadScene(int buildIndex);
        void LoadScene(string name);

        void FadeIn(float duration);
        void FadeOut(float duration);

        void NextState(System.Action onFadeCompleted);
    }
}