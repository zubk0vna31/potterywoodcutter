using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Rendering;
using UnityEngine.SceneManagement;

namespace PWS.Common.SceneSwitchService
{

    public class SceneSwitchService : MonoBehaviour, ISceneSwitchService
    {
        [SerializeField] private float _fadeTime = 0.5f;
        [SerializeField] private Material _fadeMaterial;
        [SerializeField] private Color _initialColor = new Color(0, 0, 0, 0);
        [SerializeField] private Color _fadeColor = new Color(0, 0, 0, 1);

        private bool _isLoading;
        private bool _isFading;

        void Start()
        {
            Camera.onPostRender += OnPostRenderCallback;
        }

        public void LoadScene(int buildIndex)
        {
            if (_isLoading)
                return;

            StartCoroutine(FadeTaskCoroutine(() => SceneManager.LoadScene(buildIndex)));
        }

        public void LoadScene(string name)
        {
            if (_isLoading)
                return;

            StartCoroutine(FadeTaskCoroutine(() => SceneManager.LoadScene(name)));
        }

        public void FadeIn(float duration)
        {
            if (_isFading) return;

            StartCoroutine(FadeCoroutine(true, duration));
        }

        public void FadeOut(float duration)
        {
            if (!_isFading) return;

            StartCoroutine(FadeCoroutine(false, duration));
        }

        public void NextState(System.Action onFadeCompleted)
        {
            StartCoroutine(FadeTaskCoroutine(onFadeCompleted));
        }

        IEnumerator FadeTaskCoroutine(System.Action onFadeCompleted)
        {
            EventSystem eventSystem = EventSystem.current;
            if (eventSystem)
                eventSystem.enabled = false;

            _isLoading = true;
            yield return StartCoroutine(FadeCoroutine(true));
            onFadeCompleted.Invoke();
            yield return StartCoroutine(FadeCoroutine(false));
            _isLoading = false;

            if (eventSystem)
                eventSystem.enabled = true;
        }

        IEnumerator FadeCoroutine (bool fadeIn)
        {
            float timer = 0;
            _fadeMaterial.color = fadeIn ? _initialColor : _fadeColor;

            if (fadeIn)
            {
                _isFading = true;
            }

            while (timer < _fadeTime) {
                yield return null;
                timer += Time.deltaTime;

                float value = Mathf.Clamp01(timer / _fadeTime);

                if(fadeIn)
                    _fadeMaterial.color = Color.Lerp(_initialColor, _fadeColor, value);
                else
                    _fadeMaterial.color = Color.Lerp(_fadeColor, _initialColor, value);
            }

            if (!fadeIn)
            {
                _isFading = false;
            }
        }

        IEnumerator FadeCoroutine(bool fadeIn,float duration=1)
        {
            float timer = 0;
            _fadeMaterial.color = fadeIn ? _initialColor : _fadeColor;

            _isFading = true;

            while (timer < duration)
            {
                yield return null;
                timer += Time.deltaTime;

                float value = Mathf.Clamp01(timer / duration);

                if (fadeIn)
                    _fadeMaterial.color = Color.Lerp(_initialColor, _fadeColor, value);
                else
                    _fadeMaterial.color = Color.Lerp(_fadeColor, _initialColor, value);
            }

        }

        void OnPostRenderCallback(Camera cam)
        {
            DrawQuad();
        }

        void DrawQuad()
        {
            if (_isLoading || _isFading)
            {
                _fadeMaterial.SetPass(0);
                GL.PushMatrix();
                GL.LoadOrtho();
                GL.Color(_fadeMaterial.color);
                GL.Begin(GL.QUADS);
                GL.Vertex3(0f, 0f, -1f);
                GL.Vertex3(0f, 1f, -1f);
                GL.Vertex3(1f, 1f, -1f);
                GL.Vertex3(1f, 0f, -1f);
                GL.End();
                GL.PopMatrix();
            }
        }

        void OnDestroy()
        {
            Camera.onPostRender -= OnPostRenderCallback;
        }
    }

}