using Leopotam.Ecs;
using Modules.Root.ECS;
using UnityEngine;

namespace Modules.SGMBLinker
{
    [CreateAssetMenu(menuName = "Modules/SGMBLinker/Provider")]
    public class SGMBLinkerSystemsProvider : ScriptableObject, ISystemsProvider
    {
        public EcsSystems GetSystems(EcsWorld world, EcsSystems endFrame, EcsSystems mainSystems)
        {
            EcsSystems systems = new EcsSystems(world, "SGMBLinkerSystems");
            systems.Add(new SGMBLinkerInitSystem());
            return systems;
        }
    }
}