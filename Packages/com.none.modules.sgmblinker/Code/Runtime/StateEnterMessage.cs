namespace Modules.SGMBLinker
{
    public class StateEnterMessage<T>
    {
        public readonly T State;

        public StateEnterMessage(T state)
        {
            State = state;
        }
    }
}