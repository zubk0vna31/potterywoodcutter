using System.Collections.Generic;
using Leopotam.Ecs;
using UniRx;

namespace Modules.SGMBLinker
{
    // propagates message from unirx messagebroker to ecs
    public class RXMessagePropagator<T> : IEcsInitSystem, IEcsDestroySystem, IEcsRunSystem where T:struct
    {
        // auto injected fields
        private readonly EcsFilter<T> _propagatedMessages;
        private readonly EcsWorld _world;
        
        private readonly IMessageBroker _messageBroker;
        private readonly Queue<T> _messageQueue;

        private System.IDisposable _disposable;
        
        public RXMessagePropagator(IMessageBroker messageBroker)
        {
            _messageBroker = messageBroker;
            _messageQueue = new Queue<T>();
        }

        public RXMessagePropagator()
        {
            _messageBroker = UniRx.MessageBroker.Default;
            _messageQueue = new Queue<T>();
        }

        public void Init()
        {
            _disposable = _messageBroker.Receive<T>().Subscribe(message => _messageQueue.Enqueue(message));
        }
        
        public void Run()
        {
            foreach (var i in _propagatedMessages)
            {
                _propagatedMessages.GetEntity(i).Destroy();
            }

            while (_messageQueue.Count > 0)
            {
                Propagate(_messageQueue.Dequeue());
            }
        }
        
        public void Destroy()
        {
            _disposable?.Dispose();
        }

        private void Propagate(T message)
        {
            EcsEntity entity = _world.NewEntity();
            entity.Get<T>() = message;
            entity.Get<PropagatedMessageTag>();
        }
    }
}