using Modules.StateGroup.Core;
using UniRx;

namespace Modules.SGMBLinker
{
    public class StateFactoryListener : ISetStateListener
    {
        private IMessageBroker _targetBroker;

        public StateFactoryListener(IMessageBroker targetBroker)
        {
            _targetBroker = targetBroker;
        }

        public void OnStateEnter<T>(ref T state)
        {
            _targetBroker.Publish(new StateEnterMessage<T>(state));
        }

        public void OnStateExit<T>(ref T state)
        {
            _targetBroker.Publish(new StateExitMessage<T>(state));
        }
    }
}