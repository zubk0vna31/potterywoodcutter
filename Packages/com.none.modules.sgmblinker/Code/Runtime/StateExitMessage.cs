namespace Modules.SGMBLinker
{
    public class StateExitMessage<T>
    {
        public readonly T State;

        public StateExitMessage(T state)
        {
            State = state;
        }
    }
}