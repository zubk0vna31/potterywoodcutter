using Leopotam.Ecs;
using UniRx;
using Modules.StateGroup;
using Modules.StateGroup.Core;

namespace Modules.SGMBLinker
{
    public class SGMBLinkerInitSystem : IEcsInitSystem
    {
        private readonly StateFactory _stateFactory;
        
        public void Init()
        {
            _stateFactory.AddSetStateListener(new StateFactoryListener(MessageBroker.Default));
        }
    }
}