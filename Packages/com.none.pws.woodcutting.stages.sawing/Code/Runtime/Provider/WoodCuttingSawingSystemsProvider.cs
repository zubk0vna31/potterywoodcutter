using Leopotam.Ecs;
using Modules.Root.ContainerComponentModel;
using Modules.Root.ECS;
using PWS.Common.Audio;
using PWS.Common.GameModeInfoService;
using PWS.Common.ResultsEvaluation;
using PWS.Common.UI;
using PWS.WoodCutting.Common;
using UnityEngine;
using PWS.Common.TravelPoints;


namespace PWS.WoodCutting.Stages.Sawing
{

    internal class StateDependecies
    {
        [Inject] public IGameModeInfoService gameModeInfoService;
        [Inject] public IResultsEvaluationDataHolder resultsEvaluation;
    }
    public class WoodCuttingSawingSystemsProvider : MonoBehaviour, ISystemsProvider
    {
        [Header("Core")]
        [SerializeField]
        private StateConfig _stateConfig;
        [SerializeField]
        private StateInfoData _stateInfo;
        [SerializeField,Range(0.001f,2f)]
        private float uvMargin = 0.15f;

        //[Header("Scene Related")]
        
        [Header("Voice config")]
        [SerializeField] private VoiceConfig _voiceConfig;

        public EcsSystems GetSystems(EcsWorld world, EcsSystems endFrame, EcsSystems mainSystems)
        {
            StateDependecies dependecies = new StateDependecies();
            SceneContainer.Instance.Inject(dependecies);

            EcsSystems systems = new EcsSystems(world, "WoodCutting Sawing");

            systems

                .Add(new SetStateDelayedSystem<SawingState>())

                .Add(new OutlineByStateSystem<SawingState>(_stateConfig))


                .Add(new SawingInitializeSystem(_stateConfig, uvMargin))
                .Add(new SawingSkipStateSystem(_stateConfig))

                .Add(new SawingRegionUpdateSystem(_stateConfig))

                //Tracker
                .Add(new SawingStateTrackerSystem(_stateConfig,dependecies.gameModeInfoService))

                .Add(new WoodCuttingSetGradeByProgress<SawingState>(dependecies.resultsEvaluation,
                _stateInfo))

                .Add(new StateInfoWindowUIProcessing<SawingState>(_stateInfo))
                .Add(new StateWindowPlacementProcessing<SawingState>("Main"))

                .Add(new TravelPointPositioningSystem<SawingState>("Default"))


                .Add(new StateWindowProgressUIProcessing<SawingState>())
                .Add(new StateWindowButtonUIProcessing<SawingState>(false))
                
                //Voice
                .Add(new VoiceAudioSystem<SawingState>(_voiceConfig))
                
                //Sound
                .Add(new StickableBasedSoundSystem<SawingState>())

                ;


            return systems;
        }
    }
}
