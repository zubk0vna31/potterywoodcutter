using Leopotam.Ecs;
using Modules.Utils;
using PWS.WoodCutting.Common;
using UnityEngine;

namespace PWS.WoodCutting.Stages.Sawing
{
    public class SawingPartsTrackerSystem : RunSystem<SawingState, SawingStateConfig>
    {
        private readonly EcsFilter<PlankFinal> _planks;
        private readonly TimeService _timeService;

        private float timer = 0f;

        public SawingPartsTrackerSystem(StateConfig config) : base(config)
        {
        }

        protected override void OnStateEnter()
        {
            base.OnStateEnter();

            timer = _config.paintableUpdateInterval;
        }

        protected override void OnStateUpdate()
        {
            base.OnStateUpdate();

            if (timer > 0)
            {
                timer -= _timeService.DeltaTime;

                if (timer <= 0f)
                {
                    foreach (var i in _planks)
                    {
                        foreach (var region in _planks.Get1(i).sawingPattern.Regions)
                        {
                            _planks.Get1(i).sawingPaintable.CalculatePercentage(region);

                            float percentage = _planks.Get1(i).sawingPaintable.GetPercentageByRegion(region);

                            if (percentage >= 1f)
                            {
                                _planks.Get1(i).sawingPattern.Drop(region);
                            }
                        }
                    }

                    timer = _config.paintableUpdateInterval;
                }
            }
        }

    }
}
