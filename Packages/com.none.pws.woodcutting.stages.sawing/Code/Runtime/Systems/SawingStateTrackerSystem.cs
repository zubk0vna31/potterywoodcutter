using Leopotam.Ecs;
using PWS.Common.GameModeInfoService;
using PWS.Common.UI;
using PWS.WoodCutting.Common;
using UnityEngine;

namespace PWS.WoodCutting.Stages.Sawing
{
    public class SawingStateTrackerSystem : RunSystem<SawingState, SawingStateConfig>
    {
        private readonly EcsFilter<StateProgress> _stateProgress;
        private readonly EcsFilter<PlankFinal> _planks;
        private readonly IGameModeInfoService _gameModeInfoService;

        public SawingStateTrackerSystem(StateConfig config,IGameModeInfoService gameModeInfoService) : base(config)
        {
            _gameModeInfoService = gameModeInfoService;
        }

        protected override void OnStateEnter()
        {
            base.OnStateEnter();

            foreach (var i in _stateProgress)
            {
                ref var progress = ref _stateProgress.Get1(i);

                progress.Value = 0f;
                progress.CompletionThreshold = 1f;
                progress.SkipThreshold = _gameModeInfoService.CurrentMode.Equals(GameMode.Free)?
                    0.0f:_config.skipThreshold;
            }
        }


        protected override void OnStateUpdate()
        {
            base.OnStateUpdate();

            foreach (var i in _stateProgress)
            {
                float value = 0f;

                foreach (var plank in _planks)
                {
                    value += _planks.Get1(plank).sawingPattern.FullTime;
                }

                value = Mathf.Clamp01(value / _planks.GetEntitiesCount());

                _stateProgress.Get1(i).Value = value;
            }
        }
    }
}
