using Leopotam.Ecs;
using PWS.WoodCutting.Common;

namespace PWS.WoodCutting.Stages.Sawing
{
    public class SawingRegionUpdateSystem : RunSystem<SawingState,SawingStateConfig>
    {
        private readonly EcsFilter<PlankFinal> _planks;

        public SawingRegionUpdateSystem(StateConfig config) : base(config)
        {
        }

        protected override void OnStateUpdate()
        {
            base.OnStateUpdate();

            foreach (var i in _planks)
            {
                foreach (var region in _planks.Get1(i).sawingPattern.Regions)
                {
                    float percentage = _planks.Get1(i).sawingPattern.CurrentTime(region);

                    if (percentage >= 1f)
                    {
                        _planks.Get1(i).sawingPattern.Drop(region);
                    }
                }
            }
        }

    }
}
