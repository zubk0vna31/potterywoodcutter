using DG.Tweening;
using Leopotam.Ecs;
using Modules.StateGroup.Core;
using PWS.Common;
using PWS.Common.UI;
using PWS.WoodCutting.Common;
using UnityEngine;

namespace PWS.WoodCutting.Stages.Sawing
{
    public class SawingSkipStateSystem : RunSystem<SawingState, SawingStateConfig>
    {
        private readonly EcsFilter<NextStateSignal> _nextStateSignal;
        private readonly EcsFilter<CompleteStateSignal> _completeStateSignal;

        private readonly EcsFilter<PlankFinal> _plankFinal;

        private readonly EcsFilter<StateProgress> _stateProgress;

        public SawingSkipStateSystem(StateConfig config) : base(config)
        {
        }

        protected override void OnStateUpdate()
        {
            base.OnStateUpdate();

            //if (!_completeStateSignal.IsEmpty())
            //{
            //    foreach (var i in _completeStateSignal)
            //    {
            //        _completeStateSignal.Get1(i).button.gameObject.SetActive(false);
            //        _completeStateSignal.GetEntity(i).Get<Ignore>();
            //    }

            //    _config.nextState.Execute(_stateFactory);
            //    return;
            //}

            if (_nextStateSignal.IsEmpty() ) return;

            float grade = 0f;
            foreach (var i in _stateProgress)
            {
                grade = _stateProgress.Get1(i).ProgressValue;
            }
            _ecsWorld.NewEntity().Get<SetGradeSignal>().grade = grade;

            foreach (var i in _nextStateSignal)
            {
                _nextStateSignal.Get1(i).button.gameObject.SetActive(false);
                _nextStateSignal.GetEntity(i).Get<Ignore>();
            }

            foreach (var i in _plankFinal)
            {
                ref var plank = ref _plankFinal.Get1(i);
                plank.sawingPaintable.PaintAllWithDuration(_config.paintAllDuration);
                plank.sawingPattern.DropAll(_config.sawedPartShrinkDuration, _config.sawedPartShrinkStartDelay);
                plank.sawingPattern.Dissable();
                plank.view.EnableOtherSawingParts(_config.sawedPartShrinkDuration, _config.sawedPartShrinkStartDelay); ;

            }

            var entity = _ecsWorld.NewEntity();
            entity.Get<SetStateDelayed>().state = _config.nextState;
            entity.Get<SetStateDelayed>().delay = _config.sawedPartShrinkDuration+_config.sawedPartShrinkStartDelay;
        }
    }
}
