using Leopotam.Ecs;
using PWS.WoodCutting.Common;
using UnityEngine;

namespace PWS.WoodCutting.Stages.Sawing
{
    public class SawingInitializeSystem : RunSystem<SawingState, SawingStateConfig>
    {
        private readonly EcsFilter<PlankFinal> _planks;
        private readonly float _uvMargin;

        public SawingInitializeSystem(StateConfig config,float uvMargin) : base(config)
        {
            _uvMargin = uvMargin;
        }

        protected override void OnStateEnter()
        {
            base.OnStateEnter();

            foreach (var plank in _planks)
            {
                ref var plankFinal  = ref _planks.Get1(plank);
                plankFinal.view.State6.gameObject.SetActive(false);
                plankFinal.view.State7.gameObject.SetActive(false);
                plankFinal.view.State8.gameObject.SetActive(true);

                plankFinal.sawingPattern.Initialize(plankFinal.pattern.vertexPathFollowConfig,
                    plankFinal.pattern.jigsawToolConfig);
                plankFinal.sawingPattern.SetMaxTime(1f);


                var regions = plankFinal.sawingPattern.Regions;
                var checkInfos = new Paintable.CheckInfo[regions.Length];

                for (int i = 0; i < checkInfos.Length; i++)
                {
                    checkInfos[i] = new Paintable.CheckInfo();

                    checkInfos[i].percentage = 0f;
                    checkInfos[i].uvArea = plankFinal.sawingPattern.GetUVArea(regions[i]);
                    checkInfos[i].uvSize = plankFinal.sawingPattern.GetUVSize(regions[i]);

                }

                plankFinal.sawingPaintable.Initialize(regions,checkInfos,
                    _config.skipThreshold,false, _uvMargin);

                plankFinal.sawingPaintable.SetMaterialColors(_config.normalColor,
                    _config.sawedColor,_config.sawedColor);
            }
        }
    }
}
