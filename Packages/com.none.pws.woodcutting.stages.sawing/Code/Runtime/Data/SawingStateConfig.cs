using PWS.WoodCutting.Common;
using UnityEngine;

namespace PWS.WoodCutting.Stages.Sawing
{
    [CreateAssetMenu(fileName = "W_SawingStateConfig",
        menuName = "PWS/WoodCutting/Stages/Sawing/State Config")]

    public class SawingStateConfig : StateConfig
    {
        [Range(0f, 1f)]
        public float skipThreshold = 0.75f;
        [Range(0f, 1f)]
        public float sawedPartShrinkDuration=0.5f;
        [Range(0f, 1f)]
        public float sawedPartShrinkStartDelay=1f;
        [Range(0f, 1f)]
        public float paintableUpdateInterval = 0.25f;
        [Range(0f, 1f)]
        public float paintAllDuration = 0.25f; 

        public Color normalColor;
        public Color sawedColor;
    }
}
