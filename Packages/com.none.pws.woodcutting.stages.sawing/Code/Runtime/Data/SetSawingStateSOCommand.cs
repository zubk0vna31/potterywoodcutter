using Modules.StateGroup.Core;
using UnityEngine;

namespace PWS.WoodCutting.Stages.Sawing
{
    [CreateAssetMenu(menuName = "PWS/WoodCutting/Stages/Sawing/SetStateCommand")]
    public class SetSawingStateSOCommand : SetStateSOCommand<SawingState>
    {
       
    }
}
