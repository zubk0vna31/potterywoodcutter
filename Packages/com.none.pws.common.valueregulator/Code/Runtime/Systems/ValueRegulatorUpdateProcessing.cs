﻿using Leopotam.Ecs;
using PWS.Pottery.ItemDataHolderService;
using UnityEngine;

namespace PWS.Common.ValueRegulator
{
    public class ValueRegulatorUpdateProcessing<StateT, MachineT> : IEcsRunSystem where StateT : struct where MachineT : struct
    {
        // auto injected fields
        readonly EcsFilter<StateT> _inState;
        readonly EcsFilter<ValueRegulator, MachineT> _regulator;
        readonly EcsFilter<ValueRegulatorVisualizable<MachineT>> _view;

        private IValueRegulatorDataHolder<MachineT> _data;

        public ValueRegulatorUpdateProcessing(IValueRegulatorDataHolder<MachineT> data)
        {
            _data = data;
        }

        public void Run()
        {
            if (_inState.IsEmpty()) return;

            if (_data.IsUpdated)
            {
                foreach (var idx in _regulator)
                {
                    _regulator.Get1(idx).View.SetValue(_data.Value);
                }
                foreach (var idx in _view)
                {
                    _view.Get1(idx).View.UpdateValue(_data.ConvertToCustom());
                }
                _data.IsUpdated = false;
            }
        }
    }
}