﻿using Leopotam.Ecs;
using Modules.StateGroup.Components;

namespace PWS.Common.ValueRegulator
{
    public class ValueRegulatorUIProcessing<StateT, ViewT> : IEcsRunSystem where StateT : struct where ViewT : struct
    {
        // auto injected fields
        readonly EcsFilter<StateEnter, StateT> _entering;
        readonly EcsFilter<StateExit, StateT> _exiting;
        readonly EcsFilter<StateT> _inState;

        readonly EcsFilter<ValueRegulator, ViewT> _view;

        public void Run()
        {
            if (!_entering.IsEmpty())
            {
                foreach (var idx in _view)
                {
                    _view.Get1(idx).View.Show();
                }
            }

            if (!_exiting.IsEmpty())
            {
                foreach (var idx in _view)
                {
                    _view.Get1(idx).View.Hide();
                }
            }
        }
    }
}