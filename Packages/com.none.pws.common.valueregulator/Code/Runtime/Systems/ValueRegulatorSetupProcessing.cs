﻿using Leopotam.Ecs;
using Modules.StateGroup.Components;
using PWS.Common.ValueRegulator;
using PWS.Pottery.ItemDataHolderService;
using UnityEngine;

namespace PWS.Common.ValueRegulator
{
    public class ValueRegulatorSetupProcessing<StateT, MachineT> : IEcsRunSystem where StateT : struct where MachineT : struct
    {
        // auto injected fields
        readonly EcsFilter<StateT> _inState;
        readonly EcsFilter<StateEnter, StateT> _entering;
        readonly EcsFilter<ValueRegulatorChangedSignal, MachineT> _changedSignal;

        private ValueRegulatorConfig _config;
        private IValueRegulatorDataHolder<MachineT> _data;
        private bool _resetValue;

        public ValueRegulatorSetupProcessing(ValueRegulatorConfig config, IValueRegulatorDataHolder<MachineT> data, bool resetValue = false)
        {
            _config = config;
            _data = data;
            _data.KeyCustomValues = _config.KeyCustomValues;
            _data.Value = _data.ConvertFromCustom(_config.InitCustomValue);
            _data.SetDirty();

            _resetValue = resetValue;
        }

        public void Run()
        {
            if (!_entering.IsEmpty())
            {
                if (_resetValue)
                {
                    _data.Value = 0;
                    _data.SetDirty();
                }
            }

            if (_inState.IsEmpty()) return;

            foreach (var idx in _changedSignal)
            {
                _data.Value = _changedSignal.Get1(idx).Value;
                _data.SetDirty();
            }
        }
    }
}