﻿using Leopotam.Ecs;
using Modules.StateGroup.Components;
using PWS.Pottery.Common;
using PWS.Pottery.ItemDataHolderService;
using UnityEngine;

namespace PWS.Common.ValueRegulator
{
    public class ValueRegulatorInRangeProcessing<StateT, MachineT, ViewT> : IEcsRunSystem where StateT : struct where MachineT : struct where ViewT : struct
    {
        // auto injected fields
        readonly EcsFilter<StateEnter, StateT> _entering;
        readonly EcsFilter<StateExit, StateT> _exiting;
        readonly EcsFilter<StateT> _inState;

        readonly EcsFilter<MachineT> _machine;
        readonly EcsFilter<ViewT> _view;
        readonly EcsFilter<ValueRegulatorChangedSignal, MachineT> _changedSignal;

        private ValueRegulatorConfig _config;
        private IValueRegulatorDataHolder<MachineT> _data;

        //runtime data
        private float _minValue;
        private float _maxValue;

        public ValueRegulatorInRangeProcessing(ValueRegulatorConfig config, IValueRegulatorDataHolder<MachineT> data)
        {
            _config = config;
            _data = data;

            _minValue = _data.ConvertFromCustom(_config.CompletionCustomValueMin);
            _maxValue = _data.ConvertFromCustom(_config.CompletionCustomValueMax);
        }

        public void Run()
        {
            if (!_entering.IsEmpty())
            {
                foreach (var view in _view)
                {
                    bool show = _data.Value >= _minValue && _data.Value <= _maxValue;
                    _view.GetEntity(view).Get<VisibilitySignal>().Show = show;
                }
            }

            if (_inState.IsEmpty()) return;

            foreach (var idx in _changedSignal)
            {
                bool show = _changedSignal.Get1(idx).Value >= _minValue && _changedSignal.Get1(idx).Value <= _maxValue;

                foreach (var view in _view)
                {
                    _view.GetEntity(view).Get<VisibilitySignal>().Show = show;
                }
            }
        }
    }
}