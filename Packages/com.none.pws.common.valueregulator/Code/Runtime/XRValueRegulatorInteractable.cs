using UnityEngine;
using UnityEngine.Events;
using UnityEngine.XR.Interaction.Toolkit;

namespace PWS.Common.ValueRegulator
{
    [System.Serializable]
    public enum InteractableAxes
    {
        XAxis,
        YAxis,
        ZAxis
    }

    public class XRValueRegulatorInteractable : XRBaseInteractable
    {
        [SerializeField] private InteractableAxes _axis = InteractableAxes.ZAxis;
        [SerializeField] private float _minAngle = -90;
        [SerializeField] private float _maxAngle = 90;
        [SerializeField] private float _angleStep = 15;
        [SerializeField] private Transform _handleVisual;

        public UnityEvent<float> OnUpdate;
        private Transform _tr;
        private Vector3 _initLocalRotation;

        private Vector3 _interactorOldRot;
        private bool _editedByHand;

        override protected void Awake()
        {
            base.Awake();

            _tr = transform;
            _initLocalRotation = _tr.localEulerAngles;

            selectEntered.AddListener((SelectEnterEventArgs arg0) => {
                _interactorOldRot = selectingInteractor.transform.eulerAngles;
            });
        }

        public void SetRotation(float rot)
        {
            if (_editedByHand)
            {
                _editedByHand = false;
                return;
            }

            Vector3 rotation = Vector3.zero;
            switch (_axis)
            {
                case InteractableAxes.XAxis:
                    rotation = Vector3.right * rot;
                    break;
                case InteractableAxes.YAxis:
                    rotation = Vector3.up * rot;
                    break;
                case InteractableAxes.ZAxis:
                    rotation = Vector3.forward * rot;
                    break;
            }
            _tr.localEulerAngles = rotation;
            _handleVisual.localEulerAngles = rotation;
        }

        public override void ProcessInteractable(XRInteractionUpdateOrder.UpdatePhase updatePhase)
        {
            base.ProcessInteractable(updatePhase);

            if (updatePhase == XRInteractionUpdateOrder.UpdatePhase.Dynamic)
            {
                if (isSelected)
                {
                    Vector3 delta = selectingInteractor.transform.eulerAngles - _interactorOldRot;
                    _interactorOldRot = selectingInteractor.transform.eulerAngles;

                    _tr.eulerAngles += delta;

                    Vector3 eulerRot = _initLocalRotation;
                    Vector3 eulerRotVisual = _initLocalRotation;
                    float rot = 0;
                    float rotVisual = 0;
                    switch (_axis)
                    {
                        case InteractableAxes.XAxis:
                            rot = ClampAxis(_tr.localEulerAngles.x);
                            eulerRot.x = rot;

                            rotVisual = Mathf.Round(rot / _angleStep) * _angleStep;
                            eulerRotVisual.x = rotVisual;
                            break;
                        case InteractableAxes.YAxis:
                            rot = ClampAxis(_tr.localEulerAngles.y);
                            eulerRot.y = rot;

                            rotVisual = Mathf.Round(rot / _angleStep) * _angleStep;
                            eulerRotVisual.y = rotVisual;
                            break;
                        case InteractableAxes.ZAxis:
                            rot = ClampAxis(_tr.localEulerAngles.z);
                            eulerRot.z = rot;

                            rotVisual = Mathf.Round(rot / _angleStep) * _angleStep;
                            eulerRotVisual.z = rotVisual;
                            break;
                    }

                    _tr.localEulerAngles = eulerRot;
                    _handleVisual.localEulerAngles = eulerRotVisual;

                    _editedByHand = true;

                    OnUpdate.Invoke(rotVisual);
                }
            }
        }

        float ClampAxis(float inRot)
        {
            float outRot = inRot;

            if (outRot > 180)
                outRot = -(360 - outRot);

            outRot = Mathf.Clamp(outRot, _minAngle, _maxAngle);

            return outRot;
        }
    }
}
