﻿using UnityEngine;
using UnityEditor;

namespace PWS.Common.ValueRegulator
{
    public interface IValueRegulatorVizualizableView
    {
        void UpdateValue(float value);
    }
}