﻿using Leopotam.Ecs;

namespace PWS.Common.ValueRegulator
{
    public struct ValueRegulatorVisualizable<MachineT>
    {
        public IValueRegulatorVizualizableView View;
    }
}