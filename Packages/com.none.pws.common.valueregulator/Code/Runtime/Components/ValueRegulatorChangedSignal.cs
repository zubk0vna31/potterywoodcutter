﻿
namespace PWS.Common.ValueRegulator
{
    public struct ValueRegulatorChangedSignal
    {
        public float Value;
    }
}