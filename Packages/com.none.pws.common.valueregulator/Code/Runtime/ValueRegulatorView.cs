using UnityEngine;
using Modules.ViewHub;
using Leopotam.Ecs;

namespace PWS.Common.ValueRegulator
{
    public class ValueRegulatorView : ViewComponent
    {
        private float _value;
        public float Value => _value;

        [SerializeField] private GameObject _showHideTarget;
        [SerializeField] private XRValueRegulatorInteractable _handleInteractable;
        [SerializeField] private float _range = 180;
        [SerializeField] private bool _inverse = true;

        private EcsEntity _entity;

        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            _entity = ecsEntity;
            ecsEntity.Get<ValueRegulator>().View = this;

            _handleInteractable.OnUpdate.AddListener(OnUpdateInteractable);
            //SetValue(0, true);

            //Hide();
        }

        public void OnUpdateInteractable(float rot)
        {
            if (_inverse)
                rot *= -1;

            _value = Mathf.Clamp01(rot / _range + 0.5f);

            _entity.Get<ValueRegulatorChangedSignal>().Value = _value;
        }

        public void SetValue(float value, bool invokeSignal = false)
        {
            _value = Mathf.Clamp01(value);

            float rot = (_value - 0.5f) * _range;
            if (_inverse)
                rot *= -1;

            _handleInteractable.SetRotation(rot);

            if(invokeSignal)
                _entity.Get<ValueRegulatorChangedSignal>().Value = _value;
        }

        public virtual void Show()
        {
            if (_showHideTarget.activeSelf)
                return;

            _showHideTarget.gameObject.SetActive(true);
        }

        public virtual void Hide()
        {
            if (!_showHideTarget.activeSelf)
                return;

            _showHideTarget.gameObject.SetActive(false);
        }
    }
}
