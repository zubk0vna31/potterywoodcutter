﻿using UnityEngine;

namespace PWS.Common.ValueRegulator
{
    [CreateAssetMenu(fileName = "ValueRegulatorConfig", menuName = "PWS/Stages/ValueRegulator/Config", order = 0)]
    public class ValueRegulatorConfig : ScriptableObject
    {
        public float[] KeyCustomValues = new float[] {0, 100, 200, 225, 250};
        public float InitCustomValue = 0;

        public float CompletionCustomValueMin = 225;
        public float CompletionCustomValueMax = 250;
    }

}