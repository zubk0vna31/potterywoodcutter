using UnityEngine;
using TMPro;
using UnityEngine.UI;
using PWS.Common.GameModeInfoService;

namespace PWS.HUB.GameModeSelector
{
    public class GameModeToggle : MonoBehaviour
    {
        public TMP_Text Label;
        public Toggle Toggle;

        private GameMode _gameMode;
        public GameMode GameMode
        {
            set => _gameMode = value;
            get => _gameMode;
        }

        public bool IsOn
        {
            get => Toggle.isOn;
        }

        public void SetData(GameMode gameMode, string text)
        {
            GameMode = gameMode;
            Label.text = text;

            gameObject.SetActive(true);
        }

    }
}