using Modules.Root.ContainerComponentModel;
using PWS.Common.GameModeInfoService;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace PWS.HUB.GameModeSelector
{
    public class GameModeSelector : MonoBehaviour
    {
        // dependencies
        [Inject] private IGameModeInfoService _gameModeInfoService;

        [Header("View")]
        [SerializeField] private GameObject _canvas;
        [SerializeField] private GameModeToggle[] _toggles;
        [SerializeField] private Button _potteryButton;
        [SerializeField] private Button _woodcutterButton;

        [Header("Configs")]
        [SerializeField] private GameModeData _gameModeData;

        //[SerializeField] private UnityEvent _potteryClickEvent;
        //[SerializeField] private UnityEvent _woodcutterClickEvent;

        private void Awake()
        {
            AppContainer.Inject(this);
        }

        // Start is called before the first frame update
        void Start()
        {
            Hide();
            Init();
        }

        void Init()
        {
            //_potteryButton.onClick.AddListener(_potteryClickEvent.Invoke);
            //_woodcutterButton.onClick.AddListener(_woodcutterClickEvent.Invoke);

            for (int i = 0; i < _gameModeData.pairs.Length; i++) 
            {
                _toggles[i].SetData(_gameModeData.pairs[i].gameMode, _gameModeData.pairs[i].text);
                _toggles[i].Toggle.onValueChanged.AddListener(OnToggleChanged);
            }

            OnToggleChanged(true);
        }

        void OnToggleChanged(bool value)
        {
            if (!value) return;

            _gameModeInfoService.CurrentMode = GetSelectedGameMode();
        }

        public GameMode GetSelectedGameMode()
        {
            foreach (GameModeToggle toggle in _toggles) {
                if (toggle.IsOn) {
                    return toggle.GameMode;
                }
            }

            return GameMode.Free;
        }

        public void Show()
        {
            _canvas.SetActive(true);
            _potteryButton.gameObject.SetActive(true);
            _woodcutterButton.gameObject.SetActive(true);
        }

        public void Hide()
        {
            _canvas.SetActive(false);
            _potteryButton.gameObject.SetActive(false);
            _woodcutterButton.gameObject.SetActive(false);
        }

        //private void //OnDestroy()
        //{
        //    _potteryButton.onClick.RemoveListener(_potteryClickEvent.Invoke);
        //    _woodcutterButton.onClick.RemoveListener(_woodcutterClickEvent.Invoke);
        //}
    }
}