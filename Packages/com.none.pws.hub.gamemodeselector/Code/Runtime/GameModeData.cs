using PWS.Common.GameModeInfoService;
using UnityEngine;

namespace PWS.HUB.GameModeSelector {
    [CreateAssetMenu(fileName = "GameModeData", menuName = "PWS/Common/GameModeSelector/GameModeData", order = 0)]
    public class GameModeData : ScriptableObject
    {
        public GameModeTextPair[] pairs;
    }

    [System.Serializable]
    public class GameModeTextPair
    {
        public GameMode gameMode;
        public string text;
    }
}