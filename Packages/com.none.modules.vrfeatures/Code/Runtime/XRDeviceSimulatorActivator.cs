using System;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit.Inputs;
using Modules.VRFeatures;

namespace PWS.Common.UnityComponents
{
    public class XRDeviceSimulatorActivator : MonoBehaviour
    {
        [SerializeField] private XRDeviceSimulator _simulator;
        [SerializeField] private InputActionManager _inputActionManager;
        
        private void Awake()
        {
            #if UNITY_EDITOR
            Activate();
            #endif
        }

        private void Activate()
        {
            _simulator.gameObject.SetActive(true);
            _simulator.enabled = true;
            _inputActionManager.enabled = true;
        }
    }
}