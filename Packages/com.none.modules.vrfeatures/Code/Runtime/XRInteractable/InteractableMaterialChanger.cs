﻿using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace Modules.VRFeatures
{
    [RequireComponent(typeof(XRBaseInteractable))]
    public class InteractableMaterialChanger : MonoBehaviour
    {
        public Renderer[] Renderers;
        public Material DefaultMaterial;
        public Material SelectedMaterial;

        private XRBaseInteractable m_GrabInteractable;

        protected void OnEnable()
        {
            m_GrabInteractable = GetComponent<XRBaseInteractable>();

            //m_GrabInteractable.firstHoverEntered.AddListener(OnFirstHoverEntered);
            //m_GrabInteractable.lastHoverExited.AddListener(OnLastHoverExited);
            m_GrabInteractable.selectEntered.AddListener(OnSelectEntered);
            m_GrabInteractable.selectExited.AddListener(OnSelectExited);

            SetMaterial(DefaultMaterial);
        }

        protected void OnDisable()
        {
            //m_GrabInteractable.firstHoverEntered.RemoveListener(OnFirstHoverEntered);
            //m_GrabInteractable.lastHoverExited.RemoveListener(OnLastHoverExited);
            m_GrabInteractable.selectEntered.RemoveListener(OnSelectEntered);
            m_GrabInteractable.selectExited.RemoveListener(OnSelectExited);
        }

        public virtual void SetMaterial(Material mat)
        {
            foreach (Renderer rend in Renderers)
            {
                rend.material = mat;
            }
        }

        protected virtual void OnSelectEntered(SelectEnterEventArgs args)
        {
            SetMaterial(SelectedMaterial);
        }

        protected virtual void OnSelectExited(SelectExitEventArgs args)
        {
            SetMaterial(DefaultMaterial);
        }
        /*
        protected virtual void OnLastHoverExited(HoverExitEventArgs args)
        {
            

        }

        protected virtual void OnFirstHoverEntered(HoverEnterEventArgs args)
        {
            

        }*/
    }
}