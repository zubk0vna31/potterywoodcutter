
using UnityEngine.XR.Interaction.Toolkit;

namespace Modules.VRFeatures
{
    public struct XRKinematicGrabInteractableComponent
    {
        public XRKinematicGrabInteractable Interactable;
    }
}
