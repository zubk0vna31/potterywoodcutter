
using UnityEngine.XR.Interaction.Toolkit;

namespace Modules.VRFeatures
{
    public struct XRInteractableComponent
    {
        public XRBaseInteractable Interactable;
    }
}
