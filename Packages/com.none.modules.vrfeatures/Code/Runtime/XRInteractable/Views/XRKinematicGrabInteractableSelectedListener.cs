using Leopotam.Ecs;
using Modules.ViewHub;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace Modules.VRFeatures
{
    public class XRKinematicGrabInteractableSelectedListener : ViewComponent
    {
        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            XRKinematicGrabInteractable interactable = GetComponent<XRKinematicGrabInteractable>();

            interactable.selectEntered.AddListener((SelectEnterEventArgs args) =>
            {
                if(interactable.UseSystem)
                    ecsEntity.Get<SelectEntered>();
            });

            interactable.selectExited.AddListener((SelectExitEventArgs args) =>
            {
                if (interactable.UseSystem)
                    ecsEntity.Get<SelectExited>();
            });
        }
    }
}
