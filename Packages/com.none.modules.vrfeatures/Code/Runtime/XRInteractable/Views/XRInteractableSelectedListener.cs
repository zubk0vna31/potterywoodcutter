using Leopotam.Ecs;
using Modules.ViewHub;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace Modules.VRFeatures
{
    public class XRInteractableSelectedListener : ViewComponent
    {
        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            XRBaseInteractable interactable = GetComponent<XRBaseInteractable>();

            interactable.selectEntered.AddListener((SelectEnterEventArgs args) =>
            {
                ecsEntity.Get<SelectEntered>();
            });

            interactable.selectExited.AddListener((SelectExitEventArgs args) =>
            {
                ecsEntity.Get<SelectExited>();
            });
        }
    }
}
