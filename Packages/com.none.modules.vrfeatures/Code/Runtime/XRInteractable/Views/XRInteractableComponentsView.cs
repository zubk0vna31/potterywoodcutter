using Leopotam.Ecs;
using Modules.ViewHub;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace Modules.VRFeatures
{
    public class XRInteractableComponentsView : ViewComponent
    {
        [SerializeField]
        private bool _canDisableCollider;

        private XRBaseInteractable _interactable;
        private Rigidbody _rb;
        private Collider _col;
        private MeshCollider _mCol;

        private bool _initIsKinematic;
        private bool _convex;

        //runtime data
        public bool IsKinematic
        {
            get => _rb.isKinematic;
        }
        public bool IsSelected
        {
            get => _interactable.isSelected;
        }

        public float GetDistanceSqrToInteractor(XRBaseInteractor interactor)
        {
            return _interactable.GetDistanceSqrToInteractor(interactor);
        }

        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            ecsEntity.Get<XRInteractableComponents>().View = this;
            _interactable = GetComponent<XRBaseInteractable>();
            _rb = GetComponent<Rigidbody>();
            _col = GetComponent<Collider>();
            _mCol = GetComponent<MeshCollider>();
            _initIsKinematic = _rb.isKinematic;

            if(_mCol)
                _convex = _mCol.convex;
        }

        public void EnableCollider(bool value)
        {
            _col.enabled = value;
        }

        public void MakeInteractable(bool value)
        {
            _interactable.enabled = value;
            if(_initIsKinematic)
                _rb.isKinematic = true;

            if(_canDisableCollider)
                _col.enabled = value;

            if (_mCol)
                _mCol.convex = value ? true : _convex;
        }
    }
}
