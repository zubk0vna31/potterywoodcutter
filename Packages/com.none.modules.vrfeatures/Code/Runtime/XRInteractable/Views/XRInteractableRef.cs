using Leopotam.Ecs;
using Modules.ViewHub;
using UnityEngine.XR.Interaction.Toolkit;

namespace Modules.VRFeatures
{
    public class XRInteractableRef : ViewComponent
    {
        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            ecsEntity.Get<XRInteractableComponent>().Interactable = GetComponent<XRBaseInteractable>();
        }
    }
}
