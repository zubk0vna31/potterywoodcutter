using Leopotam.Ecs;
using Modules.ViewHub;

namespace Modules.VRFeatures
{
    public class XRKinematicGrabInteractableRef : ViewComponent
    {
        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            ecsEntity.Get<XRKinematicGrabInteractableComponent>().Interactable = GetComponent<XRKinematicGrabInteractable>();
        }
    }
}
