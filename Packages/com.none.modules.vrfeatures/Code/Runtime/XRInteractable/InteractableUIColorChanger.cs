using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.Interaction.Toolkit;

namespace Modules.VRFeatures
{
    [RequireComponent(typeof(XRBaseInteractable))]
    public class InteractableUIColorChanger : MonoBehaviour
    {
        public Graphic Graphic;
        public Color Hover = new Color(0.929f, 0.094f, 0.278f);
        public Color Select = new Color(1, 1, 1);

        XRBaseInteractable m_GrabInteractable;

        bool m_Held;

        protected void OnEnable()
        {
            m_GrabInteractable = GetComponent<XRBaseInteractable>();

            m_GrabInteractable.firstHoverEntered.AddListener(OnFirstHoverEntered);
            m_GrabInteractable.lastHoverExited.AddListener(OnLastHoverExited);
            m_GrabInteractable.selectEntered.AddListener(OnSelectEntered);
            m_GrabInteractable.selectExited.AddListener(OnSelectExited);
        }


        protected void OnDisable()
        {
            m_GrabInteractable.firstHoverEntered.RemoveListener(OnFirstHoverEntered);
            m_GrabInteractable.lastHoverExited.RemoveListener(OnLastHoverExited);
            m_GrabInteractable.selectEntered.RemoveListener(OnSelectEntered);
            m_GrabInteractable.selectExited.RemoveListener(OnSelectExited);
        }

        protected virtual void OnSelectEntered(SelectEnterEventArgs args)
        {
            Graphic.color = Select;
            m_Held = true;
        }

        protected virtual void OnSelectExited(SelectExitEventArgs args)
        {
            Graphic.color = Color.white;
            m_Held = false;
        }

        protected virtual void OnLastHoverExited(HoverExitEventArgs args)
        {
            if (!m_Held)
            {
                Graphic.color = Color.white;
            }
        }

        protected virtual void OnFirstHoverEntered(HoverEnterEventArgs args)
        {
            if (!m_Held)
            {
                Graphic.color = Hover;
            }
        }
    }
}
