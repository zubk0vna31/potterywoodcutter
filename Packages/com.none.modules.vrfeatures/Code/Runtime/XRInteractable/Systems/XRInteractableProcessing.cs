using Leopotam.Ecs;
using Modules.StateGroup.Components;

namespace Modules.VRFeatures
{
    public class XRInteractableProcessing<StateT, ViewT> : IEcsRunSystem, IEcsInitSystem where StateT : struct where ViewT : struct
    {
        // auto injected fields
        readonly EcsFilter<StateEnter, StateT> _entering;
        readonly EcsFilter<StateExit, StateT> _exiting;
        readonly EcsFilter<XRInteractableComponents, ViewT> _view;

        public void Init()
        {
            foreach (var i in _view)
            {
                _view.Get1(i).View.MakeInteractable(false);
            }
        }

        public void Run()
        {
            if (!_entering.IsEmpty())
            {
                foreach (var i in _view)
                {
                    _view.Get1(i).View.MakeInteractable(true);
                }
            }
            if (!_exiting.IsEmpty())
            {
                foreach (var i in _view)
                {
                    _view.Get1(i).View.MakeInteractable(false);
                }
            }
        }
    }
}
