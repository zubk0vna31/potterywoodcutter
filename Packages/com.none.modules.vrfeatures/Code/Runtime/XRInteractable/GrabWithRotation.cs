using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace Modules.VRFeatures
{
    public class GrabWithRotation : MonoBehaviour
    {
        private XRBaseInteractable _interactble;

        void Start()
        {
            _interactble = GetComponentInParent<XRBaseInteractable>();
            _interactble.selectEntered.AddListener(OnSelected);
        }

        private void OnSelected(SelectEnterEventArgs arg0)
        {
            transform.rotation = arg0.interactor.transform.rotation;
        }
    }
}
