using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace Modules.VRFeatures
{
    public class ForceIsKinematicOnDetach : MonoBehaviour
    {
        [SerializeField]
        private bool _isKinematic;

        private XRBaseInteractable _interactble;
        private Rigidbody _rb;

        void Start()
        {
            _rb = GetComponent<Rigidbody>();
            _interactble = GetComponent<XRBaseInteractable>();
            _interactble.selectExited.AddListener(OnSelectedExit);
        }

        private void OnSelectedExit(SelectExitEventArgs arg0)
        {
            _rb.isKinematic = _isKinematic;
        }
    }
}
