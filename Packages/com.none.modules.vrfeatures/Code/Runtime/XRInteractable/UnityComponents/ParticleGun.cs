﻿using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

[RequireComponent(typeof(XRGrabInteractable))]
public class ParticleGun : MonoBehaviour
{
    [SerializeField] private ParticleSystem _particles;

    private XRGrabInteractable _interactable;

    void Start() {
        _interactable = GetComponent<XRGrabInteractable>();
        _interactable.selectExited.AddListener(DroppedGun);
        _interactable.activated.AddListener(TriggerPulled);
        _interactable.deactivated.AddListener(TriggerReleased);
    }

    void TriggerReleased(DeactivateEventArgs args)
    {
        _particles.Stop();
    }

    void TriggerPulled(ActivateEventArgs args)
    {
        _particles.Play();
    }

    void DroppedGun(SelectExitEventArgs args)
    {
        _particles.Stop();
    }
}