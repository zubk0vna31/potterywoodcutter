
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace Modules.VRFeatures
{
    public class XRKinematicGrabInteractable : XRBaseInteractable
    {
        public bool UseSystem;

        private Rigidbody _rb;

        protected override void Awake()
        {
            base.Awake();

            _rb = GetComponent<Rigidbody>();
        }

        /// <inheritdoc />
        protected override void OnSelectEntering(SelectEnterEventArgs args)
        {
            base.OnSelectEntering(args);

            if (UseSystem)
                return;

            _rb.isKinematic = true;
        }

        /// <inheritdoc />
        protected override void OnSelectExiting(SelectExitEventArgs args)
        {
            base.OnSelectExiting(args);

            if (UseSystem)
                return;

            _rb.isKinematic = false;
        }

        public override void ProcessInteractable(XRInteractionUpdateOrder.UpdatePhase updatePhase)
        {
            base.ProcessInteractable(updatePhase);

            if (UseSystem)
                return;

            if (updatePhase != XRInteractionUpdateOrder.UpdatePhase.Fixed)
                return;

            if (!isSelected)
                return;

            Vector3 pos = selectingInteractor.transform.position;

            _rb.position = pos;
            _rb.rotation = selectingInteractor.transform.rotation;
        }

    }
}
