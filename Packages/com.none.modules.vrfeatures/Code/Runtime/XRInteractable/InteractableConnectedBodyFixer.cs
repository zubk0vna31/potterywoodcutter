using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace Modules.VRFeatures
{
    [RequireComponent(typeof(XRBaseInteractable))]
    public class InteractableConnectedBodyFixer : MonoBehaviour
    {
        [SerializeField]
        private Rigidbody _connectedBody;

        private Vector3 _pos;
        //private Quaternion _rot;
        private Transform _tr;

        XRBaseInteractable m_GrabInteractable;

        private void Start()
        {
            _tr = transform;
            _pos = _tr.InverseTransformPoint(_connectedBody.position);
        }

        protected void OnEnable()
        {
            m_GrabInteractable = GetComponent<XRBaseInteractable>();
            
            m_GrabInteractable.selectEntered.AddListener(OnSelectEntered);
            m_GrabInteractable.selectExited.AddListener(OnSelectExited);

            _tr = transform;
            _pos = _tr.InverseTransformPoint(_connectedBody.position);
        }


        protected void OnDisable()
        {
            m_GrabInteractable.selectEntered.RemoveListener(OnSelectEntered);
            m_GrabInteractable.selectExited.RemoveListener(OnSelectExited);
        }

        protected virtual void OnSelectEntered(SelectEnterEventArgs args)
        {
            _connectedBody.velocity = Vector3.zero;
            _connectedBody.position = _tr.TransformPoint(_pos);
            _connectedBody.rotation = _tr.rotation;
        }

        protected virtual void OnSelectExited(SelectExitEventArgs args)
        {
            
        }
    }
}