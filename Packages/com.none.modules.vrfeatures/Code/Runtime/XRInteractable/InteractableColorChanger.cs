using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace Modules.VRFeatures
{
    [RequireComponent(typeof(XRBaseInteractable))]
    public class InteractableColorChanger : MonoBehaviour
    {
        public string property = "";
        public SkinnedMeshRenderer SkinnedMeshRenderer;
        public MeshRenderer MeshRenderer;
        [ColorUsage(false, true)]
        public Color Hover = new Color(0.929f, 0.094f, 0.278f);
        [ColorUsage(false, true)]
        public Color Select = new Color(1, 1, 1);

        XRBaseInteractable m_GrabInteractable;

        bool m_Held;

        protected void OnEnable()
        {
            m_GrabInteractable = GetComponent<XRBaseInteractable>();

            m_GrabInteractable.firstHoverEntered.AddListener(OnFirstHoverEntered);
            m_GrabInteractable.lastHoverExited.AddListener(OnLastHoverExited);
            m_GrabInteractable.selectEntered.AddListener(OnSelectEntered);
            m_GrabInteractable.selectExited.AddListener(OnSelectExited);
        }


        protected void OnDisable()
        {
            m_GrabInteractable.firstHoverEntered.RemoveListener(OnFirstHoverEntered);
            m_GrabInteractable.lastHoverExited.RemoveListener(OnLastHoverExited);
            m_GrabInteractable.selectEntered.RemoveListener(OnSelectEntered);
            m_GrabInteractable.selectExited.RemoveListener(OnSelectExited);
        }

        protected virtual void OnSelectEntered(SelectEnterEventArgs args)
        {
            if (property.Length > 0)
            {
                MeshRenderer.material.EnableKeyword("_EMISSION");
                MeshRenderer.material.SetColor(property, Select);
            }
            else if (SkinnedMeshRenderer == null)
            {
                MeshRenderer.material.color = Select;
            }
            else
            {
                SkinnedMeshRenderer.material.color = Select;
            }

            m_Held = true;
        }

        protected virtual void OnSelectExited(SelectExitEventArgs args)
        {
            if (property.Length > 0)
            {
                MeshRenderer.material.DisableKeyword("_EMISSION");
                MeshRenderer.material.SetColor(property, Color.black);
            }
            else if (SkinnedMeshRenderer == null)
            {
                MeshRenderer.material.color = Color.white;
            }
            else
            {
                SkinnedMeshRenderer.material.color = Color.white;
            }

            m_Held = false;
        }

        protected virtual void OnLastHoverExited(HoverExitEventArgs args)
        {
            if (!m_Held)
            {
                if (property.Length > 0)
                {
                    MeshRenderer.material.DisableKeyword("_EMISSION");
                    MeshRenderer.material.SetColor(property, Color.black);
                }
                else if (SkinnedMeshRenderer == null)
                {
                    MeshRenderer.material.color = Color.white;
                }
                else
                {
                    SkinnedMeshRenderer.material.color = Color.white;
                }
            }
        }

        protected virtual void OnFirstHoverEntered(HoverEnterEventArgs args)
        {
            if (!m_Held)
            {
                if (property.Length > 0)
                {
                    MeshRenderer.material.EnableKeyword("_EMISSION");
                    MeshRenderer.material.SetColor(property, Hover);
                }
                else if (SkinnedMeshRenderer == null)
                {
                    MeshRenderer.material.color = Hover;
                }
                else
                {
                    SkinnedMeshRenderer.material.color = Hover;
                }
            }
        }
    }
}