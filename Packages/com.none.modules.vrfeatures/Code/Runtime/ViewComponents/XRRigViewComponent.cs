using Leopotam.Ecs;
using Modules.ViewHub;
using Modules.VRFeatures.Components;
using UnityEngine;

namespace Modules.VRFeatures.ViewComponents
{
    public class XRRigViewComponent : ViewComponent
    {
        [SerializeField] private XRRigComponent _xrRigComponent;
        
        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            ecsEntity.Get<XRRigComponent>() = _xrRigComponent;
        }
    }
}