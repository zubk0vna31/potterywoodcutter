using Leopotam.Ecs;
using Modules.ViewHub;
using UnityEngine.XR.Interaction.Toolkit;

namespace Modules.VRFeatures
{
    public class XRInteractorSelectListener : ViewComponent
    {
        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            XRBaseInteractor interactor = GetComponent<XRBaseInteractor>();

            interactor.selectEntered.AddListener((SelectEnterEventArgs args) =>
            {
                ecsEntity.Get<SelectEntered>();
            });

            interactor.selectExited.AddListener((SelectExitEventArgs args) =>
            {
                ecsEntity.Get<SelectExited>();
            });
        }
    }
}
