using Leopotam.Ecs;
using Modules.ViewHub;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace Modules.VRFeatures
{
    public class SnapSocketView : ViewComponent
    {
        [SerializeField]
        private GameObject _visual;
        [SerializeField]
        private XRSocketInteractor _socket;

        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            ecsEntity.Get<SnapSocket>().View = this;
        }

        public void SetActive(bool value)
        {
            _socket.socketActive = value;
            _visual.SetActive(value);
        }
    }
}
