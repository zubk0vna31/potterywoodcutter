using Leopotam.Ecs;
using Modules.StateGroup.Components;

namespace Modules.VRFeatures
{
    public class SnapSocketActivityProcessing<StateT, ViewT> : IEcsRunSystem, IEcsInitSystem where StateT : struct where ViewT : struct
    {
        // auto injected fields
        protected readonly EcsFilter<StateEnter, StateT> _entering;
        protected readonly EcsFilter<StateExit, StateT> _exiting;

        protected readonly EcsFilter<SnapSocket, ViewT> _socket;

        public void Init()
        {
            foreach (var i in _socket)
            {
                _socket.Get1(i).View.SetActive(false);
            }
        }

        public virtual void Run()
        {
            if (!_entering.IsEmpty())
            {
                foreach (var i in _socket)
                {
                    _socket.Get1(i).View.SetActive(true);
                }
            }
            if (!_exiting.IsEmpty())
            {
                foreach (var i in _socket)
                {
                    _socket.Get1(i).View.SetActive(false);
                }
            }
        }
    }
}
