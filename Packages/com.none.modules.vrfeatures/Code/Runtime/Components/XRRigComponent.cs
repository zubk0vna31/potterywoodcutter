using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace Modules.VRFeatures.Components
{
    [System.Serializable]
    public struct XRRigComponent
    {
        public XRRig XRRig;
        public Transform CameraOffset;
        public Camera MainCamera;
    }
}