using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.XR.Interaction.Toolkit;

namespace Modules.VRFeatures
{
    public class LeapMoveProvider : LocomotionProvider
    {
        public enum Direction
        {
            All,
            Forward,
            Horizontal
        }

        [SerializeField]
        private float _moveAmount = 0.75f;
        [SerializeField]
        Direction _direction;

        [SerializeField]
        InputActionProperty _leapMoveInputAction;

        [SerializeField]
        [Tooltip("The source Transform to define the forward direction.")]
        private Transform _forwardSource;

        private CharacterController _characterController;
        private bool _attemptedGetCharacterController;

        protected void OnEnable()
        {
            _leapMoveInputAction.action.started += Action_started;
        }

        private void Action_started(InputAction.CallbackContext obj)
        {

            var xrRig = system.xrRig;
            if (xrRig == null)
                return;

            var input = ReadInput();
            var translationInWorldSpace = ComputeDesiredMove(input);

            MoveRig(translationInWorldSpace);
        }

        private Vector2 ReadInput()
        {
            return _leapMoveInputAction.action?.ReadValue<Vector2>() ?? Vector2.zero;
        }

        protected virtual void MoveRig(Vector3 translationInWorldSpace)
        {
            var xrRig = system.xrRig;
            if (xrRig == null)
                return;

            FindCharacterController();

            var motion = translationInWorldSpace;

            if (_characterController != null && _characterController.enabled)
            {

                if (CanBeginLocomotion() && BeginLocomotion())
                {
                    _characterController.Move(motion);

                    EndLocomotion();
                }
            }
            else
            {
                if (CanBeginLocomotion() && BeginLocomotion())
                {
                    xrRig.rig.transform.position += motion;

                    EndLocomotion();
                }
            }
        }

        protected virtual Vector3 ComputeDesiredMove(Vector2 input)
        {
            if (input == Vector2.zero)
                return Vector3.zero;

            var xrRig = system.xrRig;
            if (xrRig == null)
                return Vector3.zero;

            Vector3 inputMove = Vector3.zero;
            switch (_direction)
            {
                case Direction.All:
                    inputMove = Vector3.ClampMagnitude(new Vector3(input.x, 0f, input.y), 1f);
                    break;
                case Direction.Forward:
                    inputMove.z = Mathf.Ceil(Mathf.Abs(input.y)) * Mathf.Sign(input.y);
                    break;
                case Direction.Horizontal:
                    inputMove.x = Mathf.Ceil(Mathf.Abs(input.x)) * Mathf.Sign(input.x);
                    break;
            }

            var rigTransform = xrRig.rig.transform;
            var rigUp = rigTransform.up;

            // Determine frame of reference for what the input direction is relative to
            var forwardSourceTransform = _forwardSource == null ? xrRig.cameraGameObject.transform : _forwardSource;
            var inputForwardInWorldSpace = forwardSourceTransform.forward;
            if (Mathf.Approximately(Mathf.Abs(Vector3.Dot(inputForwardInWorldSpace, rigUp)), 1f))
            {
                // When the input forward direction is parallel with the rig normal,
                // it will probably feel better for the player to move along the same direction
                // as if they tilted forward or up some rather than moving in the rig forward direction.
                // It also will probably be a better experience to at least move in a direction
                // rather than stopping if the head/controller is oriented such that it is perpendicular with the rig.
                inputForwardInWorldSpace = -forwardSourceTransform.up;
            }

            var inputForwardProjectedInWorldSpace = Vector3.ProjectOnPlane(inputForwardInWorldSpace, rigUp);
            var forwardRotation = Quaternion.FromToRotation(rigTransform.forward, inputForwardProjectedInWorldSpace);

            var translationInRigSpace = forwardRotation * inputMove * _moveAmount;
            var translationInWorldSpace = rigTransform.TransformDirection(translationInRigSpace);

            return translationInWorldSpace;
        }

        void FindCharacterController()
        {
            var xrRig = system.xrRig;
            if (xrRig == null)
                return;

            // Save a reference to the optional CharacterController on the rig GameObject
            // that will be used to move instead of modifying the Transform directly.
            if (_characterController == null && !_attemptedGetCharacterController)
            {
                _characterController = xrRig.rig.GetComponent<CharacterController>();
                _attemptedGetCharacterController = true;
            }
        }

        protected void OnDisable()
        {
            _leapMoveInputAction.action.started -= Action_started;
        }

    }
}
