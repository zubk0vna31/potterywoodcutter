using Leopotam.Ecs;
using Modules.Root.ECS;
using Modules.VRFeatures.InitialRigReset;
using UnityEngine;

namespace Modules.VRFeatures.SystemsProviders
{
    public class VRFeaturesSystemsProvider : MonoBehaviour, ISystemsProvider
    {
        [SerializeField] private bool _tryResetPositionOnInit = true;
        
        public EcsSystems GetSystems(EcsWorld world, EcsSystems endFrame, EcsSystems mainSystems)
        {
            EcsSystems systems = new EcsSystems(world, "VRFeatures");
            
            if (_tryResetPositionOnInit)
                systems.Add(new ResetPositionOnInitSystem());

            return systems;
        }
    }
}