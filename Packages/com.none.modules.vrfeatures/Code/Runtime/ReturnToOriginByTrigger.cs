using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.XR.Interaction.Toolkit;

namespace Modules.VRFeatures
{
    public class ReturnToOriginByTrigger : MonoBehaviour
    {
        public string TriggerTag = "ReturnFallingObjects";
        public float Time = 0.5f;
        public float LiftHeight = 1;
        public float IgnoreDistance = 0.1f;

        private Transform _tr;
        private Vector3 _originPosition;
        private Vector3 _originRotation;
        private float _ignoreDistanceSqr;

        private XRBaseInteractable m_Interactable;
        private Rigidbody m_Rigidbody;
        private bool m_Held;

        protected void OnEnable()
        {
            m_Rigidbody = GetComponent<Rigidbody>();
            m_Interactable = GetComponent<XRBaseInteractable>();
            if (m_Interactable)
            {
                m_Interactable.selectEntered.AddListener(OnSelectEntered);
                m_Interactable.selectExited.AddListener(OnSelectExited);
            }
        }
        
        protected void OnDisable()
        {
            if (m_Interactable)
            {
                m_Interactable.selectEntered.RemoveListener(OnSelectEntered);
                m_Interactable.selectExited.RemoveListener(OnSelectExited);
            }
        }
        private void Start()
        {
            _tr = transform;
            _originPosition = _tr.position;
            _originRotation = _tr.eulerAngles;
            _ignoreDistanceSqr = IgnoreDistance * IgnoreDistance;
        }

        private void OnTriggerEnter(Collider other)
        {
            if ((_tr.position - _originPosition).sqrMagnitude < _ignoreDistanceSqr) return;
            if (m_Held) return;
            if (!other.tag.Equals(TriggerTag)) return;

            //Debug.Log(gameObject.name + " falled");
            ReturnToOrigin();
        }

        void ReturnToOrigin()
        {
            m_Rigidbody.angularVelocity = m_Rigidbody.velocity = Vector3.zero;

            _tr.DOMoveY(_tr.position.y + LiftHeight, Time / 2).OnComplete(()=> {
                _tr.DOMove(_originPosition, Time / 2).OnComplete(() => {
                    m_Rigidbody.angularVelocity = m_Rigidbody.velocity = Vector3.zero;
                }); ;
            });
            _tr.DORotate(_originRotation, Time)
                .OnComplete(() =>
                {
                })
                .OnKill(() =>
                {
                }) ;
        }

        protected virtual void OnSelectEntered(SelectEnterEventArgs args)
        {
            _tr.DOKill();
            m_Held = true;
        }

        protected virtual void OnSelectExited(SelectExitEventArgs args)
        {
            m_Held = false;
        }
    }
}
