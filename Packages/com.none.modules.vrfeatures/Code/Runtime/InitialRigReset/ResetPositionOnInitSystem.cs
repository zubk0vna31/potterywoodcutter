using System.Collections.Generic;
using Leopotam.Ecs;
using Modules.ViewHub;
using Modules.VRFeatures.Components;
using UnityEngine.XR;

namespace Modules.VRFeatures.InitialRigReset
{
    public class ResetPositionOnInitSystem : IEcsInitSystem, IEcsRunSystem
    {
        // auto injected fields
        private readonly EcsFilter<InitialRigTransformTag, UnityView> _initialRigTransform;
        private readonly EcsFilter<XRRigComponent> _xrRig;

        private bool _resetAtNextFrame;
        
        public void Init()
        {
            _resetAtNextFrame = true;
            ResetPositionAndRotation();
        }

        public void Run()
        {
            if (_resetAtNextFrame)
            {
                ResetPositionAndRotation();
                _resetAtNextFrame = false;
            }
        }

        private void ResetPositionAndRotation()
        {
            List<InputDevice> inputDevices = new List<InputDevice>();
            InputDevices.GetDevices(inputDevices);
            foreach (var device in inputDevices)
            {
                device.subsystem.TryRecenter();
            }

            foreach (var initialPosIdx in _initialRigTransform)
            {
                foreach (var xrRigIdx in _xrRig)
                {
                    var xrRig = _xrRig.Get1(xrRigIdx).XRRig;

                    var cameraDestination = _initialRigTransform.Get2(initialPosIdx).Transform.position +
                                            (xrRig.rig.transform.up * xrRig.cameraInRigSpaceHeight);
                    xrRig.MoveCameraToWorldLocation(cameraDestination);
                    xrRig.RotateAroundCameraUsingRigUp(
                        _initialRigTransform.Get2((initialPosIdx)).Transform.rotation.eulerAngles.y -
                        xrRig.cameraGameObject.transform.rotation.eulerAngles.y);
                }
            }
        }
    }
}