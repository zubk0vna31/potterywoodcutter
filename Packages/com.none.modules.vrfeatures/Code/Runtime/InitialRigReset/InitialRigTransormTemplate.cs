using Leopotam.Ecs;
using Modules.ViewHub;

namespace Modules.VRFeatures.InitialRigReset
{
    public class InitialRigTransormTemplate : EntityTemplate
    {
        protected override void SetupEntity(EcsEntity entity, EcsWorld world)
        {
            base.SetupEntity(entity, world);
            entity.Get<InitialRigTransformTag>();
        }
    }
}