using Modules.StateGroup.Core;
using UnityEngine;

namespace PWS.WoodCutting.Stages.InitialPlanksPlacementStage
{
    [CreateAssetMenu(menuName = "PWS/WoodCutting/Stages/Initial Planks Placement/SetStateSOCommand")]
    public class SetInitialPlanksPlacementStateSOCommand : SetStateSOCommand<InitialPlanksPlacementState>
    {
        
    }
}