using PWS.WoodCutting.Common;
using UnityEngine;

namespace PWS.WoodCutting
{
    [CreateAssetMenu(fileName = "Initial Planks Placement Stage Config",
        menuName = "PWS/WoodCutting/Stages/Initial Planks Placement/Config")]
    public class InitialPlanksPlacementConfig : StateConfig
    {
        [Header("Durations")]
        [Range(0f, 5f)]
        public float plankHoverUpDuration = 0.15f;
        [Range(0f, 5f)]
        public float moveIntoSlotDuration = 1f;
        [Range(0f, 5f)]
        public float plankCorrectingDuration = 1f;

        [Header("Placement")]
        [Range(1, 5)]
        public int plankAmount;

        public LayerMask mask;

        [Range(0f, 1f)]
        public float plankSelectedHeight = 0.075f;
        [Range(0f, 1f)]
        public float plankCorrectingHeight = 0.15f;
        [Range(0f, 10f)]
        public float plankFollowSpeed = 1f;
    }
}
