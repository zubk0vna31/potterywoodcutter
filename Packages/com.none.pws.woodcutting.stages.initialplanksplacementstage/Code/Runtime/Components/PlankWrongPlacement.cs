using UnityEngine;

namespace PWS.WoodCutting
{
    public struct PlankWrongPlacement 
    {
        public Transform wrongPreview;
        public Transform plankTransform;
    }
}
