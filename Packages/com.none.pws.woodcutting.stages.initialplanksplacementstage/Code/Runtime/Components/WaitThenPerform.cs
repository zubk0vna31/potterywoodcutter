using System;
using System.Diagnostics;

namespace PWS.WoodCutting
{
    public struct WaitThenPerform 
    {
        public float Timer;
        private Action _task;
        public Action Task
        {
            private get => _task;

            set
            {
                if (_task != null)
                {
                    _task = null;
                }

                _task = value;
            }
        }
        public bool Update(float dt)
        {
            Timer -= dt;

            if (Timer <= 0f)
            {
                Task?.Invoke();
                return true;
            }

            return false;
        }
    }
}
