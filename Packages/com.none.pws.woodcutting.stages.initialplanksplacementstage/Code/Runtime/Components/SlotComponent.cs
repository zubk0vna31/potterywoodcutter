using System;
using UnityEngine;

namespace PWS.WoodCutting
{
    [Serializable]
    public struct SlotComponent
    {
        public int slotID;

        public Transform origin,preview,wrong;
        public Transform target;

        public MeshRenderer meshRenderer;

        public Collider collider;

        public static bool operator ==(SlotComponent a,SlotComponent b)
        {
            return a.slotID == b.slotID;
        }

        public static bool operator !=(SlotComponent a, SlotComponent b)
        {
            return a.slotID != b.slotID;
        }

        public override bool Equals(System.Object other)
        {
            if(other==null || !(other.GetType() == typeof(SlotComponent)))
            {
                return false;
            }
            else
            {
                var slot = (SlotComponent)other;

                return this.slotID == slot.slotID;
            }
        }

        public override int GetHashCode()
        {
            return (origin.name + slotID.ToString()).GetHashCode();
        }

        public override string ToString()
        {
            return $"Slot {slotID}";
        }
    }
}
