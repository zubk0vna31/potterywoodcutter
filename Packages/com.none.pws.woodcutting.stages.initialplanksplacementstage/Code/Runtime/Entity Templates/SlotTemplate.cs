using UnityEngine;
using Leopotam.Ecs;
using Modules.ViewHub;
using PWS.WoodCutting;
using Modules.UPhysics;

public class SlotTemplate : EntityTemplate
{
    [SerializeField, Range(0, 4)]
    private int slotID;

    [SerializeField]
    private Transform origin, preview, wrong;

    [SerializeField]
    private MeshRenderer meshRenderer;

    protected override void SetupEntity(EcsEntity entity, EcsWorld world)
    {
        base.SetupEntity(entity, world);

        ref var slot = ref entity.Get<SlotComponent>();

        slot.slotID = slotID;
        slot.origin = origin;
        slot.preview = preview;
        slot.wrong = wrong;

        slot.meshRenderer = meshRenderer;

        slot.collider = GetComponentInChildren<Collider>();


        _components.Add(gameObject.AddComponent<TriggerListener>());
    }
}
