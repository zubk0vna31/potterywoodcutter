using UnityEngine;
using Leopotam.Ecs;
using Modules.ViewHub;
using UnityEngine.XR.Interaction.Toolkit;
using PWS.WoodCutting;

public class PlankTemplate : EntityTemplate
{
    [SerializeField]
    private int plankID;

    [SerializeField]
    private Collider _collider;

    [SerializeField]
    private Collider _triggerCollider;

    [SerializeField]
    private Rigidbody _rigidbody;

    [SerializeField]
    private XRGrabInteractable _grabbable;

    private EcsEntity entity;

    protected override void SetupEntity(EcsEntity entity, EcsWorld world)
    {
        base.SetupEntity(entity, world);

        ref var plank = ref entity.Get<PlankComponent>();

        plank.collider = _collider;
        plank.triggerCollider = _triggerCollider;
        plank.rigidbody = _rigidbody;
        plank.grabbable = _grabbable;
        plank.plankID = plankID;

        this.entity = entity;
    }

    public void HoverEnter(HoverEnterEventArgs args)
    {
       
    }

    public void HoverExit(HoverExitEventArgs args)
    {
        
    }

    public void SelectEnter(SelectEnterEventArgs args)
    {
        entity.Get<Moveable>().hand = args.interactor.transform;

        ref var selected = ref entity.Get<SelectEnter>();

        selected.interactable = args.interactable;
        selected.interactor = args.interactor;
    }

    public void SelectExit(SelectExitEventArgs args)
    {
        //if(entity.Has<SelectStay>())
        //    entity.Del<SelectStay>();

        entity.Del<Moveable>();

        ref var selected = ref entity.Get<SelectExit>();

        selected.interactable = args.interactable;
        selected.interactor = args.interactor;
    }
}
