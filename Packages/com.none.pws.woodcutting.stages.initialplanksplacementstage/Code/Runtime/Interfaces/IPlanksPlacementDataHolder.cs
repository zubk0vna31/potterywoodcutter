using PWS.Common.ItemDataHolder;
using System.Collections.Generic;
using UnityEngine;

namespace PWS.WoodCutting
{
    public interface IPlanksPlacementDataHolder : IItemDataHolder
    {
        Dictionary<int, Vector3> PlanksPositions { get; }

        void PassData(PlankComponent[] planks);

        bool IsDirty { get; set; }
    }
}
