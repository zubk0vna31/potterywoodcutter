using Leopotam.Ecs;
using Modules.StateGroup.Components;
using PWS.Common.UI;
using UnityEngine;

namespace PWS.WoodCutting
{
    public class InitialPlanksPlacementProcessTracker<T> : IEcsRunSystem where T: struct
    {
        private readonly EcsFilter<T> _state;
        private readonly EcsFilter<StateEnter,T> _stateEnter;

        private readonly EcsFilter<StateWindow, StateProgress> _stateWindowWithProgress;
        private readonly EcsFilter<PlankPlaced>.Exclude<PlankWrongPlacement> _placed;


        public void Run()
        {
            if (_state.IsEmpty()) return;

            StateEnter();
            State();
        }

        private void StateEnter()
        {
            if (_stateEnter.IsEmpty()) return;

            foreach (var i in _stateWindowWithProgress)
            {
                ref var progress = ref _stateWindowWithProgress.Get2(i);

                progress.Value = 0;
                progress.CompletionThreshold = 2f;
                progress.SkipThreshold = 0.9f;
            }
        }

        private void State()
        {
            foreach (var i in _stateWindowWithProgress)
            {
                _stateWindowWithProgress.Get2(i).Value = Mathf.Clamp01(_placed.GetEntitiesCount() * 0.334f);
            }
        }

    }
}
