using Leopotam.Ecs;

namespace PWS.WoodCutting
{
    public class WaitThenPerformSystem : IEcsRunSystem
    {
        private readonly EcsFilter<WaitThenPerform> _filter;
        private readonly Modules.Utils.TimeService _time;


        public void Run()
        {
            foreach (var i in _filter)
            {
                if (_filter.Get1(i).Update(_time.DeltaTime))
                {
                    _filter.GetEntity(i).Del<WaitThenPerform>();
                }
            }
        }

       
    }
}
