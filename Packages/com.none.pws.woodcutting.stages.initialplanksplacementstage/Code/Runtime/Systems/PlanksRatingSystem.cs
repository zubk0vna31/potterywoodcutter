using Leopotam.Ecs;
using PWS.WoodCutting.Common;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace PWS.WoodCutting
{
    public class PlanksRatingSystem<T> : IEcsRunSystem where T :struct
    {
        // auto injected fields
        private readonly EcsFilter<PlankPlaced>.Exclude<PlankWrongPlacement> _filter;
        private readonly EcsFilter<T> _state;

        private readonly InitialPlanksPlacementConfig _stateConfig;

        // runtime data
        private bool enabled=true;

        public PlanksRatingSystem(StateConfig stateConfig)
        {
            _stateConfig = stateConfig as InitialPlanksPlacementConfig;
        }

        public void Run()
        {
            if (!enabled || _state.IsEmpty()) return;

            if (_filter.GetEntitiesCount() < _stateConfig.plankAmount) return;

            var slots = new Dictionary<int, SlotComponent>();

            foreach (var i in _filter)
            {
                slots.Add(i, _filter.Get1(i).entity.Get<SlotComponent>());
            }

            slots = slots.OrderBy(x => x.Value.slotID).ToDictionary(x => x.Key, y => y.Value);


            Vector3 startNormal = slots.First().Value.target.up;

            bool succsess = true;
            int index = 1;
            foreach (var i in slots.Keys.Skip(1))
            {
                float dotProduct = Vector3.Dot(startNormal, slots[i].target.up);

                if (index % 2 == 0)
                {
                    if (dotProduct <= 0)
                    {
                        succsess = false;

                        ProccessWrongPlank(_filter.GetEntity(i), slots[i]);
                    }
                }
                else
                {
                    if (dotProduct > 0)
                    {
                        succsess = false;

                        ProccessWrongPlank(_filter.GetEntity(i), slots[i]);
                    }
                }

                index++;
            }

            if (succsess)
            {

                enabled = false;
               
            }
           

            void ProccessWrongPlank(EcsEntity entity,SlotComponent slot)
            {
                ref var wrong = ref entity.Get<PlankWrongPlacement>();
                wrong.wrongPreview = slot.wrong;
                wrong.plankTransform = slot.target;

                wrong.wrongPreview.localEulerAngles = wrong.plankTransform.localEulerAngles;

                slot.wrong.gameObject.SetActive(true);

                slot.target.GetComponent<Collider>().enabled = true;
            }
        }

    }
}
