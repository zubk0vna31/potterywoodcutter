using DG.Tweening;
using Leopotam.Ecs;
using PWS.WoodCutting.Common;

namespace PWS.WoodCutting
{
    public class SlotInteractionSystem : IEcsRunSystem
    {
        // auto injected fields
        private readonly EcsFilter<PlankComponent, HoverSlot, SelectExit> _exit;
        private readonly EcsFilter<InitialPlanksPlacementState> _state;

        private readonly InitialPlanksPlacementConfig _stateConfig;

        public SlotInteractionSystem(StateConfig stateConfig)
        {
            _stateConfig = stateConfig as InitialPlanksPlacementConfig;
        }

        public void Run()
        {
            if (_state.IsEmpty()) return;

            foreach (var i in _exit)
            {
                var slotEntity = _exit.Get2(i).entity;
                var entity = _exit.GetEntity(i);

                ref var slot = ref slotEntity.Get<SlotComponent>();

                slot.collider.enabled = false;
                slot.preview.gameObject.SetActive(false);

                _exit.Get1(i).collider.enabled = false;
                _exit.Get1(i).rigidbody.isKinematic = true;

                slot.target.DOMove(slot.preview.position, _stateConfig.moveIntoSlotDuration);
                slot.target.DORotateQuaternion(slot.preview.rotation, _stateConfig.moveIntoSlotDuration);

                var meshRenderer = slot.meshRenderer;
                meshRenderer.material.DOFade(0f, "_TintColor", _stateConfig.moveIntoSlotDuration).OnComplete(() => meshRenderer.enabled = false);

                ref var waiter = ref entity.Get<WaitThenPerform>();

                waiter.Timer = _stateConfig.moveIntoSlotDuration;
                waiter.Task = () =>
                {
                    entity.Get<PlankPlaced>().entity = slotEntity;
                    entity.Get<PlankComponent>().slotID = slotEntity.Get<SlotComponent>().slotID;
                    entity.Del<HoverSlot>();
                };
            }
        }
    }
}
