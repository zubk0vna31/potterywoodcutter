﻿using Leopotam.Ecs;
using Modules.Audio;
using PWS.Common.Audio;
using UnityEngine;

namespace PWS.WoodCutting
{
    public class WrongPlanksPlacmentVoiceSystem : IEcsRunSystem
    {
        private readonly EcsFilter<InitialPlanksPlacementState> _inState;
        private readonly EcsFilter<PlankWrongPlacement> _signal;
        private readonly EcsFilter<AudioSourceRef, GrandDadTag> _grandDad;
        private readonly EcsFilter<VoiceConfigComponent> _voiceConfigComponent;

        private bool _played;
        
        public void Run()
        {
            if (_inState.IsEmpty())
                return;
            
            if (!_signal.IsEmpty())
            {
                if (_played)
                    return;
                
                foreach (var i in _voiceConfigComponent)
                {
                    ref var config = ref _voiceConfigComponent.Get1(i);
                    var id = Random.Range(0, config.WrongClipsTerms.Length);
                    PlayClip(config.WrongClipsTerms, ref id);
                    _played = true;
                }
            }
        }

        private void PlayClip(string[] clips, ref int idx)
        {
            foreach (var dad in _grandDad)
            {
                ref var sourceRef = ref _grandDad.Get1(dad);
                if (sourceRef.AudioSource.isPlaying)
                    return;

                ref var signal = ref _grandDad.GetEntity(dad).Get<PlayGlobalLocalizedVoiceSignal>();
                signal.VoiceLocalize = _grandDad.Get1(dad).VoiceLocalize;
                signal.AudioSource = _grandDad.Get1(dad).AudioSource;
                signal.Term = clips[idx];
                idx++;
            }
        }
    }
}