using DG.Tweening;
using Leopotam.Ecs;
using PWS.WoodCutting.Common;
using UnityEngine;

namespace PWS.WoodCutting
{
    public class PlanksCorrectingSystem : IEcsRunSystem
    {
        // auto injected fields
        private readonly EcsFilter<PlankWrongPlacement,SelectEnter>.Exclude<PlankCorrecting> _enter;
        private readonly EcsFilter<PlankWrongPlacement,SelectExit> _exit;
        private readonly EcsFilter<InitialPlanksPlacementState> _state;

        private readonly InitialPlanksPlacementConfig _stateConfig;

        public PlanksCorrectingSystem(StateConfig stateConfig)
        {
            _stateConfig = stateConfig as InitialPlanksPlacementConfig;
        }

        public void Run()
        {
            if (_state.IsEmpty()) return;

            SelectEnterProcessing();
            SelectExitProcessing();
        }

        private void SelectEnterProcessing()
        {
            foreach (var i in _enter)
            {
                var entity = _enter.GetEntity(i);
                ref var wrong = ref _enter.Get1(i);

                wrong.plankTransform.GetComponent<Collider>().enabled = false;
                wrong.wrongPreview.gameObject.SetActive(false);

                entity.Get<PlankCorrecting>();
            }
        }

        private void SelectExitProcessing()
        {
            foreach (var i in _exit)
            {
                var entity = _exit.GetEntity(i);
                ref var wrong = ref _exit.Get1(i);

                ref var waiter = ref entity.Get<WaitThenPerform>();

                waiter.Timer = _stateConfig.plankCorrectingDuration;
                waiter.Task = () =>
                {
                    entity.Del<PlankWrongPlacement>();
                    entity.Del<PlankCorrecting>();
                };

                var sequence = DOTween.Sequence();
                sequence.Pause();

                Vector3 initPosition = wrong.plankTransform.position;

                Debug.Log(initPosition.y);
                Debug.Log((initPosition + Vector3.up * _stateConfig.plankCorrectingHeight).y);

                sequence.Append(wrong.plankTransform.
                    DOMove(initPosition+Vector3.up*_stateConfig.plankCorrectingHeight, _stateConfig.plankCorrectingDuration * 0.35f).SetRelative(false));
                sequence.Append(wrong.plankTransform.
                    DOLocalRotate(Vector3.forward * 180f, _stateConfig.plankCorrectingDuration * 0.3f).SetRelative(true));
                sequence.Append(wrong.plankTransform.
                    DOMove(initPosition, _stateConfig.plankCorrectingDuration * 0.345f).SetRelative(false));

                sequence.Play();
            }
        }

    }
}
