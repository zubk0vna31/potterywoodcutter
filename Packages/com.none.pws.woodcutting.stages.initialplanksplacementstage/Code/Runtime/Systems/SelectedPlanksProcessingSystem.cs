using Leopotam.Ecs;
using Modules.ViewHub;
using PWS.WoodCutting.Common;
using UnityEngine;

namespace PWS.WoodCutting
{
    public class SelectedPlanksProcessingSystem : IEcsRunSystem
    {
        // auto injected fields
        private readonly EcsFilter<PlankComponent, Moveable,UnityView>.Exclude<PlankWrongPlacement> _moveable;

        private readonly EcsFilter<InitialPlanksPlacementState> _state;

        private readonly InitialPlanksPlacementConfig _stateConfig;
        private readonly Transform _slotOrigin;

        public SelectedPlanksProcessingSystem(Transform slotOrigin,StateConfig stateConfig)
        {
            _slotOrigin = slotOrigin;
            _stateConfig = stateConfig as InitialPlanksPlacementConfig;
        }

        public void Run()
        {
            if (_state.IsEmpty()) return;
            Move();
        }

        private void Move()
        {
            foreach (var i in _moveable)
            {
                ref var hand = ref _moveable.Get2(i).hand;
                float rayDistance = 30f;

                Vector3 lastPoint = hand.position + hand.forward * rayDistance;

                if (Physics.Raycast(hand.position, hand.forward,
                    out var hit, rayDistance, _stateConfig.mask,QueryTriggerInteraction.Collide))
                {
                    lastPoint = hit.point;
                }

                lastPoint.y = _slotOrigin.position.y;


                Vector3 alignedX = Vector3.Project(lastPoint - _slotOrigin.position, _slotOrigin.right) + _slotOrigin.position;
                float x = Vector3.Distance(_slotOrigin.position, alignedX);
                Vector3 dirX = -(_slotOrigin.position - alignedX).normalized;

                Vector3 position = _slotOrigin.position + dirX * x + Vector3.up * _stateConfig.plankSelectedHeight;

                ref var unityView = ref _moveable.Get3(i);

                unityView.Transform.position = Vector3.Lerp(unityView.Transform.position, position,
                    Time.deltaTime * _stateConfig.plankFollowSpeed);

                //Rotate

                float dot = Vector3.Dot(_slotOrigin.up, unityView.Transform.up);

                unityView.Transform.rotation = Quaternion.Slerp(unityView.Transform.rotation,
                    Quaternion.LookRotation(_slotOrigin.transform.forward, _slotOrigin.up * Mathf.Sign(dot)),
                    Time.deltaTime*_stateConfig.plankFollowSpeed*2f);
            }
        }
    }
}
