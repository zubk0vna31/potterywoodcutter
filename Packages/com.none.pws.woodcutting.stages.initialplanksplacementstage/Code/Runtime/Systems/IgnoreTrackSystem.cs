using Leopotam.Ecs;
using PWS.Common.UI;
using System.Collections;

namespace PWS.WoodCutting
{
    public class IgnoreTrackSystem : IEcsRunSystem
    {
        private readonly EcsFilter<Ignore> _filter;
        private Modules.Utils.TimeService _timeService;

        public void Run()
        {
            foreach (var i in _filter)
            {
                _filter.Get1(i).ignoreTime -= _timeService.DeltaTime;

                if (_filter.Get1(i).ignoreTime <= 0f)
                {
                    _filter.GetEntity(i).Del<Ignore>();
                }
            }
        }

    }
}
