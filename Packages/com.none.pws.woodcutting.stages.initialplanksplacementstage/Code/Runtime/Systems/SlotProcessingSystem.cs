using Leopotam.Ecs;
using Modules.UPhysics;
using Modules.ViewHub;
using System.Collections.Generic;
using UnityEngine;

namespace PWS.WoodCutting
{
    public class SlotProcessingSystem : IEcsRunSystem
    {
        // auto injected fields
        private readonly EcsFilter<SlotComponent, Triggered,UnityView> _triggerEnter;
        private readonly EcsFilter<HoverSlot> _triggerStay;
        private readonly EcsFilter<SlotComponent, TriggeredExited> _triggerExit;

        private readonly EcsFilter<InitialPlanksPlacementState> _state;

        public void Run()
        {
            if (_state.IsEmpty()) return;

            TriggerEnterProccessing();
            TriggerStayProcessing();
            TriggerExitProccessing();
        }

        private void TriggerEnterProccessing()
        {
            foreach (var i in _triggerEnter)
            {
                ref var triggered = ref _triggerEnter.Get2(i);

                if (triggered.Other.IsNull() || !triggered.Other.Has<PlankComponent>()) continue;

                if (triggered.Other.Get<UnityView>().Transform != triggered.Collider.transform) continue;

                ref var slot = ref _triggerEnter.Get1(i);

                if (slot.target) continue;

                if (triggered.Other.Has<HoverSlot>())
                {
                    ref var otherSlot = ref triggered.Other.Get<HoverSlot>().entity.Get<SlotComponent>();

                    otherSlot.target = null;
                    otherSlot.preview.gameObject.SetActive(false);
                    otherSlot.preview.transform.localEulerAngles = Vector3.zero;
                }

                slot.target = triggered.Collider.transform;
                triggered.Other.Get<HoverSlot>().entity = _triggerEnter.GetEntity(i);

                slot.preview.gameObject.SetActive(true);

                Transform other = triggered.Collider.transform;
                Vector3 rotation = slot.preview.localEulerAngles;

                if (Vector3.Dot(slot.preview.up, other.up) <= 0f)
                {
                    rotation.y = 180f - rotation.y;
                }

                if (Vector3.Dot(slot.preview.right, other.right) <= 0f)
                {
                    rotation.x = 180f - rotation.x;
                }

                if (Vector3.Dot(slot.preview.forward, other.forward) <= 0f)
                {
                    rotation.z = 180f - rotation.z;
                }

                slot.preview.transform.localEulerAngles = rotation;
            }
        }

        private void TriggerStayProcessing()
        {
            foreach (var i in _triggerStay)
            {
                ref var slot = ref _triggerStay.Get1(i).entity.Get<SlotComponent>();

                Transform other = slot.target;

                Vector3 rotation = slot.preview.localEulerAngles;

                if (Vector3.Dot(slot.preview.up, other.up) <= 0f)
                {
                    rotation.y = 180f - rotation.y;
                }

                if (Vector3.Dot(slot.preview.right, other.right) <= 0f)
                {
                    rotation.x = 180f - rotation.x;
                }

                if (Vector3.Dot(slot.preview.forward, other.forward) <= 0f)
                {
                    rotation.z = 180f - rotation.z;
                }

                slot.preview.transform.localEulerAngles = rotation;
            }
        }

        private void TriggerExitProccessing()
        {
            foreach (var i in _triggerExit)
            {
                ref var triggered = ref _triggerExit.Get2(i);
                ref var slot = ref _triggerExit.Get1(i);

                if (triggered.Other.IsNull() || !triggered.Other.Has<HoverSlot>()) continue;

                if (triggered.Other.Get<UnityView>().Transform != triggered.Collider.transform) continue;

                if (triggered.Other.Get<HoverSlot>().entity.Get<SlotComponent>() != slot) continue;

                triggered.Other.Del<HoverSlot>();

                slot.target = null;
                slot.preview.gameObject.SetActive(false);
                slot.preview.transform.localEulerAngles = Vector3.zero;
            }
        }
    }
}

