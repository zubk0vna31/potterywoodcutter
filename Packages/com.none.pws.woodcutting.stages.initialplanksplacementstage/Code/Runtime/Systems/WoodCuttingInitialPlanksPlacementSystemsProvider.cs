using Leopotam.Ecs;
using Modules.Root.ContainerComponentModel;
using Modules.Root.ECS;
using PWS.Common.Audio;
using PWS.Common.ResultsEvaluation;
using PWS.Common.TravelPoints;
using PWS.Common.UI;
using PWS.Features.Achievements;
using PWS.WoodCutting.Common;
using UnityEngine;

namespace PWS.WoodCutting
{
    public class WoodCuttingInitialPlanksPlacementSystemsProvider : MonoBehaviour, ISystemsProvider
    {
        internal class ResultsEvaluationDependecies
        {
            [Inject] public IResultsEvaluationDataHolder resultsEvaluation;
            [Inject] public IAchievementsService achivementSenderService;
        }

        [Header("Core")]
        [SerializeField]
        private StateConfig _stateConfig;
        [SerializeField]
        private StateInfoData _stateInfo;

        [Header("Scene Related")]
        [SerializeField]
        private Transform _slotOrigin;
        [SerializeField]
        private MeshRenderer _placeAreaRenderer;
        
        [Header("Voice config")] 
        [SerializeField] private VoiceConfig _voiceIntroConfig;
        [SerializeField] private VoiceConfig _voiceBaseConfig;

        public EcsSystems GetSystems(EcsWorld world, EcsSystems endFrame, EcsSystems mainSystems)
        {
            ResultsEvaluationDependecies dependecies = new ResultsEvaluationDependecies();
            SceneContainer.Instance.Inject(dependecies);

            EcsSystems systems = new EcsSystems(world, "WoodCutting Initial Planks Placement");
            systems

                // Utils
                .Add(new WaitThenPerformSystem())

                // Outline
                .Add(new OutlineByStateSystem<InitialPlanksPlacementState>(_stateConfig))

                // Systems
                .Add(new InitialPlanksStateRelatedSystem<InitialPlanksPlacementState>(_stateConfig, _placeAreaRenderer,
                dependecies.achivementSenderService))
                .Add(new SlotProcessingSystem())
                .Add(new SlotInteractionSystem(_stateConfig))
                .Add(new SelectedPlanksProcessingSystem(_slotOrigin, _stateConfig))
                .Add(new PlanksRatingSystem<InitialPlanksPlacementState>(_stateConfig))
                .Add(new PlanksCorrectingSystem(_stateConfig))

                //Tracker
                .Add(new InitialPlanksPlacementProcessTracker<InitialPlanksPlacementState>())
                .Add(new StateInfoWindowUIProcessing<InitialPlanksPlacementState>(_stateInfo))

                .Add(new StateWindowPlacementProcessing<InitialPlanksPlacementState>("Main"))
                .Add(new StateWindowProgressUIProcessing<InitialPlanksPlacementState>())
                .Add(new StateWindowButtonUIProcessing<InitialPlanksPlacementState>(false))
                //.Add(new SkipStageButtonUIProcessing<InitialPlanksPlacementState>())

                // Travel point
                .Add(new TravelPointPositioningSystem<InitialPlanksPlacementState>("Default"))

                .Add(new WoodCuttingSetGradeByProgress<InitialPlanksPlacementState>(dependecies.resultsEvaluation,
                _stateInfo))
                
                //Voice
                .Add(new VoiceIntroAudioSystem<InitialPlanksPlacementState>(_voiceIntroConfig))
                .Add(new VoiceBaseAudioSystem<InitialPlanksPlacementState>(_voiceBaseConfig))
                .Add(new WrongPlanksPlacmentVoiceSystem())
                

            ;

            endFrame
                .OneFrame<RemoveTargetTag>();

            return systems;
        }
    }
}