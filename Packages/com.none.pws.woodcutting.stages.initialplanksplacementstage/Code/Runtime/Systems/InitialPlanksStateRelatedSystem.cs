using DG.Tweening;
using Leopotam.Ecs;
using Modules.StateGroup.Components;
using Modules.StateGroup.Core;
using PWS.Common;
using PWS.Common.UI;
using PWS.Features.Achievements;
using PWS.WoodCutting.Common;
using UnityEngine;

namespace PWS.WoodCutting
{
    public class InitialPlanksStateRelatedSystem<T> : IEcsRunSystem where T : struct
    {
        // auto injected fields
        private readonly EcsFilter<StateEnter, T> _stateEnter;
        private readonly EcsFilter<T> _state;

        private readonly EcsFilter<NextStateSignal> _nextStateSignal;

        private readonly EcsFilter<PlankComponent> _filter;

        private readonly EcsWorld _ecsWorld;

        private readonly StateFactory _stateFactory;
        private readonly InitialPlanksPlacementConfig _stateConfig;

        private readonly MeshRenderer _placeAreaRenderer;
        private readonly IAchievementsService _achievementsService;

        public InitialPlanksStateRelatedSystem( StateConfig stateConfig, MeshRenderer placeAreaRenderer,
            IAchievementsService achivementSenderService)
        {
            _stateConfig = stateConfig as InitialPlanksPlacementConfig;
            _placeAreaRenderer = placeAreaRenderer;

            _achievementsService = achivementSenderService;
        }

        public void Run()
        {
            if (_state.IsEmpty()) return;

            StateEnter();
            NextState();
        }

        private void StateEnter()
        {
            if (_stateEnter.IsEmpty()) return;

            foreach (var i in _filter)
            {
                _filter.Get1(i).triggerCollider.enabled = false;
            }
        }


        private void NextState()
        {
            if (_nextStateSignal.IsEmpty()) return;

            // Grade
            _ecsWorld.NewEntity().Get<SetGradeSignal>().grade = 100f;

            foreach (var i in _nextStateSignal)
            {
                _nextStateSignal.GetEntity(i).Get<Ignore>().ignoreTime = 0.5f;
                _nextStateSignal.Get1(i).button.gameObject.SetActive(false);

                _placeAreaRenderer.material.DOFade(0f, "_TintColor", 0.5f)
                       .OnComplete(() =>
                       {
                           _placeAreaRenderer.gameObject.SetActive(false);
                           _stateConfig.nextState.Execute(_stateFactory);
                       });
            }
        }
    }
}
