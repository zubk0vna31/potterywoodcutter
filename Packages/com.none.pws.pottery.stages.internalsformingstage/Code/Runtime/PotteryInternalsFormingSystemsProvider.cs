using Leopotam.Ecs;
using Modules.Root.ContainerComponentModel;
using Modules.Root.ECS;
using Modules.StateGroup.Core;
using PWS.Common.Audio;
using PWS.Pottery.Common;
using PWS.Pottery.ItemDataHolderService;
using PWS.Pottery.MeshGeneration;
using PWS.Pottery.Sculpting.Interactor;
using UnityEngine;
using PWS.Common.Messages;
using PWS.Common.RestartStage;
using PWS.Common.UI;
using PWS.Common.ValueRegulator;
using PWS.Pottery.WheelSpeedSetupStage;
using PWS.Common.ResultsEvaluation;
using PWS.Common.TravelPoints;

namespace PWS.Pottery.Stages.InternalsFormingStage
{
    public class PotteryInternalsFormingSystemsProvider : MonoBehaviour, ISystemsProvider
    {
        internal class SystemsDependencies
        {
            [Inject] public ISculptingDataHolder SculptingData;
            [Inject] public SculptingReferenceDataHolder SculptingReferenceData;
            [Inject] public IValueRegulatorDataHolder<WheelSpeedRegulator> WheelSpeedData;
            [Inject] public IResultsEvaluationDataHolder ResultsEvaluationData;
        }

        [SerializeField] private SetStateSOCommand _nextState;
        [SerializeField] private IntenalsFormingStateConfig _config;
        [SerializeField] private ValueRegulatorConfig _wheelSpeedConfig;
        [SerializeField] private StateInfoData _stateInfo;
        
        [Header("Voice config")] 
        [SerializeField] private VoiceConfig _voiceConfig;

        public EcsSystems GetSystems(EcsWorld world, EcsSystems endFrame, EcsSystems mainSystems)
        {
            SystemsDependencies dependencies = new SystemsDependencies();
            SceneContainer.Instance.Inject(dependencies);

            EcsSystems systems = new EcsSystems(world);
            systems
                .Add(new TransitToStateOnUIActionSystem<InternalsFormingState>(_nextState))
                .Add(new InternalsFormingSkipProcessing(dependencies.SculptingData, dependencies.SculptingReferenceData.Data, _nextState))

                //restart
                .Add(new CurrentStateRestoreDataProcessing<InternalsFormingState, SculptingDataHolder>(dependencies.SculptingData))
                .Add(new CurrentStateRestoreDataProcessing<InternalsFormingState, ValueRegulatorDataHolder<WheelSpeedRegulator>>(dependencies.WheelSpeedData))

                //show sculpting inside
                .Add(new ProductMeshAngleRangeProcessing<InternalsFormingState, SculptingReferenceMesherTag>(_config.OpenMeshAngleRange, _config.Duration, dependencies.SculptingReferenceData.Data))
                .Add(new ProductMeshAngleRangeProcessing<InternalsFormingState, SculptingProductMesherTag>(_config.OpenMeshAngleRange, _config.Duration, dependencies.SculptingData))

                //wheel speed systems
                .Add(new GameObjectVisibilityProcessing<InternalsFormingState, WheelSpeedRegulator>())
                .Add(new ValueRegulatorSetupProcessing<InternalsFormingState, WheelSpeedRegulator>(_wheelSpeedConfig, dependencies.WheelSpeedData))
                .Add(new ValueRegulatorInRangeProcessing<InternalsFormingState, WheelSpeedRegulator, HandTag>(_wheelSpeedConfig, dependencies.WheelSpeedData))

                //state systems
                .Add(new HandInteractorVisibiltySystem<InternalsFormingState>(SculptingHand.InternalsForming))
                .Add(new InternalsFormingHandsInitPosition(dependencies.SculptingData, _config))
                .Add(new HandInteractorPositionSystem<InternalsFormingState>(_config.HandMoveAxes, _config.HandSmoothSpeed))
                .Add(new InternalsFormingInteractorsHandler(dependencies.SculptingData, _config))

                .Add(new InternalsFormingCompletionTracker(dependencies.SculptingData, dependencies.SculptingReferenceData.Data, _config))

                .Add(new SetGradeByProgress<InternalsFormingState>(dependencies.ResultsEvaluationData, _stateInfo))

                //ui
                .Add(new StateInfoWindowUIProcessing<InternalsFormingState>(_stateInfo))

                .Add(new StateWindowPlacementProcessing<InternalsFormingState>("Wheel"))
                .Add(new StateWindowProgressUIProcessing<InternalsFormingState>())
                .Add(new StateWindowButtonUIProcessing<InternalsFormingState>())
                .Add(new SkipStageButtonUIProcessing<InternalsFormingState>())

                .Add(new MeshVisibilityProcessing<InternalsFormingState, SculptingReferenceMesherTag>())
                .Add(new MeshVisibilityProcessing<InternalsFormingState, SculptingProductMesherTag>())
                
                //voice
                .Add(new VoiceCustomAudioSystem<InternalsFormingState>(_voiceConfig))

                //travel point
                .Add(new TravelPointPositioningSystem<InternalsFormingState>("Wheel"))

                ;

            endFrame
                //wheel Speed
                .Add(new ValueRegulatorUpdateProcessing<InternalsFormingState, WheelSpeedRegulator>(dependencies.WheelSpeedData))

                //sculpting reference
                .Add(new SculptingDataProcessingSystem<InternalsFormingState>(dependencies.SculptingReferenceData.Data))
                .Add(new MeshRingsDataProcessingSystem<InternalsFormingState>(dependencies.SculptingReferenceData.Data))
                .Add(new MeshUpdateSystem<InternalsFormingState, SculptingReferenceMesherTag>(dependencies.SculptingReferenceData.Data))
                //sculpting product
                .Add(new SculptingDataProcessingSystem<InternalsFormingState>(dependencies.SculptingData))
                .Add(new MeshRingsDataProcessingSystem<InternalsFormingState>(dependencies.SculptingData))
                .Add(new MeshUpdateSystem<InternalsFormingState, SculptingProductMesherTag>(dependencies.SculptingData))

                .OneFrame<RestartCurrentStepMessage>()
                .OneFrame<NextStateSignal>()
                .OneFrame<SkipStageMessage>()
                .OneFrame<SelectEnteredSignal>()
                .OneFrame<SelectExitedSignal>()

                .OneFrame<ValueRegulatorChangedSignal>()
                .OneFrame<VisibilitySignal>()
                ;

            return systems;
        }
    }
}
