using PWS.Pottery.Sculpting.Interactor;
using UnityEngine;

namespace PWS.Pottery.Stages.InternalsFormingStage
{
    [CreateAssetMenu(fileName = "InternalsFormingStageConfig", menuName = "PWS/Pottery/Stages/InternalsFormingStage/Config", order = 0)]
    public class IntenalsFormingStateConfig : ScriptableObject
    {
        public float CompletionThreshold = 0.9f;
        public float SkipThreshold = 0.4f;

        [Header("Hands")]
        public InteractableParameters HandMoveAxes;
        public float HandSmoothSpeed = 5;
        public float HandRadius = 0.1f;

        [Header("Pottery Product Mesh Angle Range")]
        public float OpenMeshAngleRange = 270;
        public float Duration = 1;
    }
}
