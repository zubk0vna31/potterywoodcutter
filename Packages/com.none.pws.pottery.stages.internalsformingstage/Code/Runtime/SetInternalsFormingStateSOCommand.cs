using Modules.StateGroup.Core;
using UnityEngine;

namespace PWS.Pottery.Stages.InternalsFormingStage
{
    [CreateAssetMenu(menuName = "PWS/Pottery/Stages/InternalsFormingStage/SetStateSOCommand")]
    public class SetInternalsFormingStateSOCommand : SetStateSOCommand<InternalsFormingState>
    {

    }
}