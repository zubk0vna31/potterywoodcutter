using Leopotam.Ecs;
using Modules.StateGroup.Components;
using Modules.ViewHub;
using PWS.Pottery.ItemDataHolderService;
using PWS.Pottery.MeshGeneration;
using PWS.Pottery.Sculpting.Interactor;
using UnityEngine;

namespace PWS.Pottery.Stages.InternalsFormingStage
{
    public class InternalsFormingHandsInitPosition : IEcsRunSystem
    {
        // auto injected fields
        readonly EcsFilter<StateEnter, InternalsFormingState> _entering;

        readonly EcsFilter<UnityView, HandTag> _hands;
        readonly EcsFilter<UnityView, SculptingProductTag> _product;

        private IntenalsFormingStateConfig _config;

        //runtime data
        private ISculptingDataHolder _data;

        public InternalsFormingHandsInitPosition(ISculptingDataHolder data, IntenalsFormingStateConfig config)
        {
            _data = data;
            _config = config;
        }

        public void Run()
        {
            if (!_entering.IsEmpty())
            {
                foreach (var product in _product)
                {
                    foreach (var idx in _hands)
                    {
                        ref var handsView = ref _hands.Get1(idx);
                        ref var handsTag = ref _hands.Get2(idx);
                        handsView.Transform.localPosition = Vector3.zero;
                        var pos = handsTag.View.InternalsFormingHandVisual.transform.position;
                        float height = _product.Get1(product).Transform.position.y + _data.GeneralParameters.FullHeight + _config.HandRadius;
                        pos.y = height;
                        handsView.Transform.position = pos;
                        handsTag.View.InternalsFormingHandVisual.transform.position = pos;
                        handsTag.OriginLocalPosition = handsView.Transform.localPosition;
                    }
                }
            }
        }
    }
}