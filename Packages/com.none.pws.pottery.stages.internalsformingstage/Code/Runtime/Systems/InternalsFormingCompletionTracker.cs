using Leopotam.Ecs;
using Modules.StateGroup.Components;
using PWS.Common.Messages;
using PWS.Common.UI;
using PWS.Pottery.ItemDataHolderService;
using UnityEngine;

namespace PWS.Pottery.Stages.InternalsFormingStage
{
    public class InternalsFormingCompletionTracker : IEcsRunSystem
    {
        // auto injected fields
        readonly EcsFilter<StateEnter, InternalsFormingState> _entering;
        readonly EcsFilter<InternalsFormingState>.Exclude<StateExit> _inState;
        private readonly EcsFilter<SkipStageMessage> _skipPerformed;

        readonly EcsFilter<StateWindow, StateProgress> _stateWindowWithProgress;

        private ISculptingDataHolder _referenceData;
        private IntenalsFormingStateConfig _config;

        //runtime data
        private ISculptingDataHolder _data;
        private float _distanceBetweenMaxPoints;

        public InternalsFormingCompletionTracker(ISculptingDataHolder data, ISculptingDataHolder referenceData, IntenalsFormingStateConfig config)
        {
            _data = data;
            _referenceData = referenceData;
            _config = config;
        }

        public void Run()
        {
            float targetHeight = _referenceData.GeneralParameters.TopClosureHeight;

            if (!_entering.IsEmpty())
            {
                _distanceBetweenMaxPoints = Mathf.Abs(targetHeight - _data.GeneralParameters.TopClosureHeight);

                foreach (var i in _stateWindowWithProgress)
                {
                    _stateWindowWithProgress.Get2(i).Value = 0;
                    _stateWindowWithProgress.Get2(i).CompletionThreshold = _config.CompletionThreshold;
                    _stateWindowWithProgress.Get2(i).SkipThreshold = _config.SkipThreshold;
                }
            }

            if (!_inState.IsEmpty())
            {
                if (!_skipPerformed.IsEmpty())
                    return;

                float currentHeight = Mathf.Abs(targetHeight - _data.GeneralParameters.TopClosureHeight);
                float completion = 1 - Mathf.Clamp01(currentHeight / _distanceBetweenMaxPoints);

                foreach (var i in _stateWindowWithProgress)
                {
                    _stateWindowWithProgress.Get2(i).Value = completion;
                }
            }
        }
    }
}
