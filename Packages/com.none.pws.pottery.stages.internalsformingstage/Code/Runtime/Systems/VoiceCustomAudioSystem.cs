﻿using Leopotam.Ecs;
using Modules.Audio;
using Modules.StateGroup.Components;
using PWS.Common.Audio;
using PWS.Pottery.Common;
using PWS.Pottery.Sculpting.Interactor;
using UnityEngine;

namespace PWS.Pottery.Stages.InternalsFormingStage
{
    public class VoiceCustomAudioSystem<TStage> : IEcsRunSystem where TStage : struct
    {

        private readonly EcsFilter<StateEnter, TStage> _enterState;
        private readonly EcsFilter<StateExit, TStage> _exitState;
        private readonly EcsFilter<TStage>.Exclude<StateEnter> _inState;
        private readonly EcsFilter<AudioSourceRef, GrandDadTag> _grandDad;
        private readonly EcsFilter<VisibilitySignal, HandTag> _signal;
        private readonly EcsFilter<VoiceConfigComponent> _voiceConfigComponent;
        private readonly EcsWorld _ecsWorld;

        private bool _enterStateFlag;
        private bool _visibilitySignalFlag;
        
        private VoiceConfig _voiceConfig;
        
        public VoiceCustomAudioSystem(VoiceConfig voiceConfig)
        {
            _voiceConfig = voiceConfig;
        }
        public void Run()
        {
            if (!_enterState.IsEmpty())
            {
                _enterStateFlag = true;
                SetVoiceConfig();
                return;
            }

            if (!_inState.IsEmpty())
            {
                foreach (var i in _voiceConfigComponent)
                {
                    ref var config = ref _voiceConfigComponent.Get1(i);

                    if (_enterStateFlag)
                    {
                        if (config.EnterClipsIndex < config.EnterClipsTerms.Length)
                        {
                            PlayClip(config.EnterClipsTerms, ref config.EnterClipsIndex);
                        }
                        else
                        {
                            _enterStateFlag = false;
                        }
                    }

                    if (!_signal.IsEmpty())
                    {
                        foreach (var idx in _signal)
                        {
                            if (_signal.Get1(idx).Show)
                                _visibilitySignalFlag = true;
                        }
                    }

                    if (_visibilitySignalFlag)
                    {
                        if (config.InClipsIndex < config.InClipsTerms.Length)
                        {
                            PlayClip(config.InClipsTerms, ref config.InClipsIndex);
                        }
                        else
                        {
                            _visibilitySignalFlag = false;
                        }
                    }
                }
            }
            
            if (!_exitState.IsEmpty())
            {
                foreach (var i in _grandDad)
                {
                    _grandDad.Get1(i).AudioSource.Stop();
                }
                
                foreach (var i in _voiceConfigComponent)
                {
                    ref var voiceConfig = ref _voiceConfigComponent.Get1(i);
                    PlayClip(voiceConfig.ExitClipsTerms, ref voiceConfig.ExitClipsIndex);
                }
            }

        }

        private void PlayClip(string[] clips, ref int idx)
        {
            foreach (var dad in _grandDad)
            {
                ref var sourceRef = ref _grandDad.Get1(dad);
                if (sourceRef.AudioSource.isPlaying)
                    return;
                
                ref var signal = ref _grandDad.GetEntity(dad).Get<PlayGlobalLocalizedVoiceSignal>();
                signal.VoiceLocalize = _grandDad.Get1(dad).VoiceLocalize;
                signal.AudioSource = _grandDad.Get1(dad).AudioSource;
                signal.Term = clips[idx];
                idx++;
            }
        }
        
        private void SetVoiceConfig()
        {
            foreach (var component in _voiceConfigComponent)
            {
                _voiceConfigComponent.GetEntity(component).Destroy();
            }

            ref var entity = ref _ecsWorld.NewEntity().Get<VoiceConfigComponent>();

            entity.EnterClipsTerms = _voiceConfig.StateEnterClipsTerms;
            entity.InClipsTerms = _voiceConfig.StateInClipsTerms;
            entity.ExitClipsTerms = _voiceConfig.StateExitClipsTerms;

            entity.EnterClipsIndex = 0;
            entity.InClipsIndex = 0;
            entity.ExitClipsIndex = 0;

            entity.GrandDadPrefab = _voiceConfig.GrandDadPrefab;
        }
    }
}