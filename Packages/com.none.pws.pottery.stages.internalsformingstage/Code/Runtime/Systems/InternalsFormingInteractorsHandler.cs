using Leopotam.Ecs;
using Modules.StateGroup.Components;
using Modules.ViewHub;
using PWS.Pottery.ItemDataHolderService;
using PWS.Pottery.MeshGeneration;
using PWS.Pottery.Sculpting.Interactor;
using UnityEngine;

namespace PWS.Pottery.Stages.InternalsFormingStage
{
    public class InternalsFormingInteractorsHandler : IEcsRunSystem
    {
        // auto injected fields
        readonly EcsFilter<InternalsFormingState> _inState;

        readonly EcsFilter<UnityView, SculptingInteractorTag> _generalInteractor;
        readonly EcsFilter<UnityView, SculptingProductTag> _product;

        private IntenalsFormingStateConfig _config;

        //runtime data
        private ISculptingDataHolder _data;

        public InternalsFormingInteractorsHandler(ISculptingDataHolder data, IntenalsFormingStateConfig config)
        {
            _data = data;
            _config = config;
        }

        public void Run()
        {
            if (_inState.IsEmpty())
                return;
            
            foreach (var productIdx in _product)
            {
                Transform product = _product.Get1(productIdx).Transform;
                foreach (var interactorIdx in _generalInteractor)
                {
                    Interaction(_generalInteractor.Get2(interactorIdx).Position, _product.Get1(productIdx).Transform.position);
                }
            }

        }

        void Interaction(Vector3 handPosition, Vector3 productPosition)
        {
            bool canMakeHole = handPosition.y - _config.HandRadius < productPosition.y + _data.GeneralParameters.TopClosureHeight;

            if (!canMakeHole) return;

            float height = handPosition.y - productPosition.y;
            if (height > _data.GeneralParameters.TopClosureHeight)
            {
                if (height - _config.HandRadius < _data.GeneralParameters.TopClosureHeight)
                {
                    float newHeight = height - _config.HandRadius;
                    newHeight = Mathf.Max(_data.GeneralParameters.BottomClosureHeight + _data.DefaultParameters.WallWidthMin, newHeight);
                    _data.GeneralParameters.TopClosureHeight = newHeight;
                }
            }

            _data.SetDirty();
        }
    }
}