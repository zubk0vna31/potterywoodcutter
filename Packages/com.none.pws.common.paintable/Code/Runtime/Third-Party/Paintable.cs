using DG.Tweening;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class Paintable : MonoBehaviour
{
    [SerializeField]
    private Renderer customRenderer;
    [Serializable]
    public enum TextureResolution
    {
        _32,
        _64,
        _128,
        _256,
        _512,
        _1024
    }

    public struct CheckInfo
    {
        public Vector4 uvSize;
        public float uvArea;
        public float uvPixelSize;
        public float percentage;

        public void SetPixelSize(float uvPixelSize)
        {
            this.uvPixelSize = uvPixelSize;
        }
    }

    [SerializeField]
    private TextureResolution textureResolution;
    public float extendsIslandOffset = 1;

    private const RenderTextureFormat renderTextureFormat = RenderTextureFormat.R8;

    private RenderTexture _extendIslandsRenderTexture;
    private RenderTexture _uvIslandsRenderTexture;
    private RenderTexture _maskRenderTexture;
    private RenderTexture _supportTexture;

    private Renderer _renderer;

    private Texture2D _result;

    private bool _useErrorCorrection;
    private bool _useCheckInfo;
    private bool _paintingWithDurationCurrentRenderer;
    private bool _active, _initialized;
    private bool firstTouch;
    private float _uvPixelSize;
    private float _uvArea;
    private float _paintedPercentage;
    private float _backupedPercentage;
    private float _maxPercentage = 1f;
    private float _minimumPaintedPercentage;

    private RawImage _rawImage;

    private Dictionary<int, CheckInfo> _checkInfo;

    private int _maskTextureID = Shader.PropertyToID("_CustomMask");
    private int _normalColorID = Shader.PropertyToID("_DefaultColor");
    private int _maskedColorID = Shader.PropertyToID("_CustomColor1");
    private int _finalColorID = Shader.PropertyToID("_CustomColor2");
    private int TextureSize => (int)Mathf.Pow(2, 5 + (int)textureResolution);

    public RenderTexture GetMask() => _maskRenderTexture;
    public RenderTexture GetUVIslands() => _uvIslandsRenderTexture;
    public RenderTexture GetExtend() => _extendIslandsRenderTexture;
    public RenderTexture GetSupport() => _supportTexture;
    public Renderer GetRenderer() => _renderer;
    public bool Active => _active;

    public Action OnPainted;
    public float PaintedPercentage => _paintedPercentage + _backupedPercentage;

    public float AllRegionsPercentage
    {
        get
        {
            float value = 0f;

            foreach (var i in _checkInfo.Keys)
            {
                value += _checkInfo[i].percentage;
            }

            value /= _checkInfo.Count;

            return value;
        }
    }

    public bool Painted
    {
        get => PaintedPercentage >= _minimumPaintedPercentage;
    }
    public bool FullPainted
    {
        get => PaintedPercentage >= _maxPercentage;
    }

    public bool Initialized => _initialized;
    public bool FirstTouch
    {
        get => firstTouch;
        set => firstTouch = value;
    }

    public void InitializeCustomRenderer(Renderer renderer, float uvArea,
        float minimumPaintedPercentage = 0.7f, bool useErrorCorrection = true, float uvMargin = -1f)
    {
        _initialized = true;
        FirstTouch = false;

        this._minimumPaintedPercentage = minimumPaintedPercentage;

        int textureSize = TextureSize;

        if (uvMargin > 0)
        {
            extendsIslandOffset = uvMargin;
        }

        _maskRenderTexture = new RenderTexture(textureSize, textureSize, 0, renderTextureFormat);
        _maskRenderTexture.filterMode = FilterMode.Bilinear;

        _extendIslandsRenderTexture = new RenderTexture(textureSize, textureSize, 0, renderTextureFormat);
        _extendIslandsRenderTexture.filterMode = FilterMode.Bilinear;

        _uvIslandsRenderTexture = new RenderTexture(textureSize, textureSize, 0, renderTextureFormat);
        _uvIslandsRenderTexture.filterMode = FilterMode.Bilinear;

        _supportTexture = new RenderTexture(textureSize, textureSize, 0, renderTextureFormat);
        _supportTexture.filterMode = FilterMode.Bilinear;

        _renderer = renderer;
        _renderer.material.SetTexture(_maskTextureID, _extendIslandsRenderTexture);




        PaintManager.instance.initTextures(this);

        _result = new Texture2D(textureSize, textureSize, TextureFormat.R8, false);
        _result.filterMode = FilterMode.Bilinear;

        _uvArea = Mathf.Clamp01(uvArea);

        _useErrorCorrection = useErrorCorrection;

        CalculateUVSpacePercentage();

#if UNITY_ANDROID
        Clear();
#endif

        _active = true;
    }

    public void Initialize(int[] regions, CheckInfo[] info, float minimumPaintedPercentage = 0.7f,
        bool useErrorCorrection = true, float uvMargin = -1f)
    {
        _useCheckInfo = true;
        _checkInfo = GetCheckInfo(regions, info);
        InitializeCustomRenderer(customRenderer, 0f, minimumPaintedPercentage, useErrorCorrection, uvMargin);
    }

    public void Initialize(float uvArea, float minimumPaintedPercentage = 0.7f,
        bool useErrorCorrection = true, float uvMargin = -1f)
    {
        _useCheckInfo = false;
        InitializeCustomRenderer(customRenderer, uvArea, minimumPaintedPercentage, useErrorCorrection, uvMargin);
    }

    public static Dictionary<int, CheckInfo> GetCheckInfo(int[] regions, CheckInfo[] info)
    {
        var checkInfo = new Dictionary<int, CheckInfo>(regions.Length);

        for (int i = 0; i < regions.Length; i++)
        {
            checkInfo.Add(regions[i], info[i]);
        }

        return checkInfo;
    }

    //public void Start()
    //{
    //    Debug.Log("Paintable initialized");

    //    int textureSize = TextureSize;

    //    _maskRenderTexture = new RenderTexture(textureSize, textureSize, 0, renderTextureFormat);
    //    _maskRenderTexture.filterMode = FilterMode.Bilinear;

    //    _extendIslandsRenderTexture = new RenderTexture(textureSize, textureSize, 0, renderTextureFormat);
    //    _extendIslandsRenderTexture.filterMode = FilterMode.Bilinear;

    //    _uvIslandsRenderTexture = new RenderTexture(textureSize, textureSize, 0, renderTextureFormat);
    //    _uvIslandsRenderTexture.filterMode = FilterMode.Bilinear;

    //    _supportTexture = new RenderTexture(textureSize, textureSize, 0, renderTextureFormat);
    //    _supportTexture.filterMode = FilterMode.Bilinear;

    //    _renderer = GetComponent<Renderer>();
    //    _renderer.material.SetTexture(_maskTextureID, _extendIslandsRenderTexture);

    //    PaintManager.instance.initTextures(this);

    //    _result = new Texture2D(textureSize, textureSize, TextureFormat.R8, false);
    //    _result.filterMode = FilterMode.Bilinear;

    //    CalculateUVSpacePercentage();
    //}

    public void SetNewMaxPercentage(float maxPercentage)
    {
        _maxPercentage = maxPercentage;
    }

    public void SetRenderTarget(Renderer renderer, float uvArea)
    {
        _renderer.material.SetTexture(_maskTextureID, GetTexture());

        _renderer = renderer;
        Clear();

        _uvArea = uvArea;

        _renderer.material.SetTexture(_maskTextureID, _extendIslandsRenderTexture);
        PaintManager.instance.initTextures(this);

        CalculateUVSpacePercentage();
    }

    private Texture2D GetTexture()
    {
        var tex = new Texture2D(_extendIslandsRenderTexture.width, _extendIslandsRenderTexture.height, TextureFormat.R8, false);
        tex.filterMode = FilterMode.Bilinear;
        Graphics.CopyTexture(_extendIslandsRenderTexture, tex);
        return tex;
    }

    public void SetMaterialColors(Color normal, Color masked, Color final)
    {
        _renderer.material.SetColor(_normalColorID, normal);
        _renderer.material.SetColor(_maskedColorID, masked);
        _renderer.material.SetColor(_finalColorID, final);
    }

    public void SetPaintedColor(Color paintedColor)
    {
        _renderer.material.SetColor("_PaintedColor", paintedColor);
    }

    public void SetDebugRenderTarget(RawImage rawImage)
    {
        _rawImage = rawImage;

        _rawImage.texture = _result;
    }

    public void BackupPercentage()
    {
        _backupedPercentage += 1f;
        Clear();
    }
    public void BackupCurrentPercentage()
    {
        _backupedPercentage += _paintedPercentage;
        _paintedPercentage = 0f;

    }

    private void Clear()
    {
        _paintedPercentage = 0.0f;


        RenderTexture tmp = RenderTexture.active;

        RenderTexture.active = _maskRenderTexture;
        GL.Clear(true, true, Color.black);

        RenderTexture.active = _extendIslandsRenderTexture;
        GL.Clear(true, true, Color.black);

        RenderTexture.active = _uvIslandsRenderTexture;
        GL.Clear(true, true, Color.black);

        RenderTexture.active = _supportTexture;
        GL.Clear(true, true, Color.black);

        RenderTexture.active = tmp;

        _result.SetPixels(Enumerable.Repeat(Color.black, _result.width * _result.height).ToArray());
        _result.Apply();
    }

    private void CalculateUVSpacePercentage()
    {
        float error = 0f;

        if (!_useCheckInfo)
        {

            if (_useErrorCorrection)
            {
                //NEED MORE ACCURATE AND SMART SOLUTION!-----------------------------------------------------
                float t = 1f - ((float)textureResolution) / Enum.GetValues(typeof(TextureResolution)).Length;

                t = Mathf.Pow(t, 3);

                error = Mathf.Lerp(0.0f, 0.6f, t);
                //-------------------------------------------------------------------------------------------

            }
            _uvPixelSize = _uvArea * Mathf.Pow(TextureSize, 2) * (1f + error);
        }
        else
        {

            if (_useErrorCorrection)
            {
                //NEED MORE ACCURATE AND SMART SOLUTION!-----------------------------------------------------
                float t = 1f - ((float)textureResolution) / Enum.GetValues(typeof(TextureResolution)).Length;

                t = Mathf.Pow(t, 3);

                error = Mathf.Lerp(0.0f, 0.6f, t);
                //-------------------------------------------------------------------------------------------

            }

            for (int i = 0; i < _checkInfo.Count; i++)
            {
                var key = _checkInfo.ElementAt(i).Key;
                var value = _checkInfo[key];

                value.uvPixelSize = _checkInfo[key].uvArea * Mathf.Pow(TextureSize, 2)
                * (1f + error);

                _checkInfo[key] = value;
            }
        }
    }

    private void OnDisable()
    {
        if (_renderer)
        {
            _maskRenderTexture.Release();
            _uvIslandsRenderTexture.Release();
            _extendIslandsRenderTexture.Release();
            _supportTexture.Release();
        }

        _initialized = false;
    }

    public float GetPercentageByRegion(int region)
    {
        if (!_checkInfo.ContainsKey(region)) return 0f;

        return _checkInfo[region].percentage;
    }

    public void CalculatePercentage(int region = -1)
    {
        if (_paintingWithDurationCurrentRenderer || FullPainted) return;

        if (!_useCheckInfo)
        {
            Rect rectReadPicture = new Rect(0, 0, TextureSize, TextureSize);

            var current = RenderTexture.active;

            RenderTexture.active = _extendIslandsRenderTexture;

            _result.ReadPixels(rectReadPicture, 0, 0);
            _result.Apply();

            RenderTexture.active = current;

            int pixelCount = 0;

            var pixels = _result.GetPixels();
            for (int i = 0; i < pixels.Length; i++)
            {
                if (pixels[i].r >= 1f - Mathf.Epsilon)
                {
                    pixelCount++;
                }
            }

            _paintedPercentage = Mathf.Clamp01(pixelCount * 1.0f / _uvPixelSize);

            if (Painted)
            {
                OnPainted?.Invoke();
            }
        }
        else
        {
            if (!_checkInfo.ContainsKey(region)) return;

            var info = _checkInfo[region];


            int x = (int)(_result.width * info.uvSize.x);
            int y = (int)(_result.height * info.uvSize.y);
            int z = (int)(_result.width * info.uvSize.z);
            int w = (int)(_result.height * info.uvSize.w);

            var current = RenderTexture.active;

            RenderTexture.active = _extendIslandsRenderTexture;

            _result.ReadPixels(new Rect(0, 0, _result.width, _result.height), 0, 0);
            _result.Apply();

            RenderTexture.active = current;

            int pixelCount = 0;

            var pixels = _result.GetPixels(x, y, z, w, 0);
            for (int i = 0; i < pixels.Length; i++)
            {
                if (pixels[i].r >= 1f - Mathf.Epsilon)
                {
                    pixelCount++;
                }
            }

            info.percentage = Mathf.Clamp01(pixelCount * 1.0f / info.uvPixelSize);

            _checkInfo[region] = info;
        }
    }

    public void ColorAllWithDuration(Renderer[] renderers, float duration = 1f,
        Action callback = null, bool useCorrectedDuration = true, bool dissableAtEnd = true)
    {
        _paintingWithDurationCurrentRenderer = true;

        if (useCorrectedDuration)
        {
            duration = duration * (Mathf.InverseLerp(0f, _maxPercentage, _maxPercentage - PaintedPercentage));
        }

        float value = 0f;
        float currentPercentage = _paintedPercentage;
        float backup = _backupedPercentage;

        DOTween.To(() => value, x => value = x, 1f, duration)
            .OnUpdate(() =>
            {
                _paintedPercentage = Mathf.Lerp(currentPercentage, 1f, value);
                _backupedPercentage = Mathf.Lerp(backup, _maxPercentage - 1f, value);

                foreach (var r in renderers)
                {
                    r.material.SetFloat("_CustomAmount", value);
                }
            })
            .OnComplete(() =>
            {
                callback?.Invoke();
                if (dissableAtEnd)
                    Dissable();
            });
    }

    public void PaintAllWithDuration(float duration)
    {
        if (_paintingWithDurationCurrentRenderer) return;

        _paintingWithDurationCurrentRenderer = true;

        duration = duration * (Mathf.InverseLerp(0f, _maxPercentage, _maxPercentage - PaintedPercentage));

        if (!_useCheckInfo)
        {
            float value = 0f;
            float currentPercentage = _paintedPercentage;
            float backup = _backupedPercentage;
            DOTween.To(() => value, x => value = x, 1f, duration)
                .OnUpdate(() =>
                {
                    _paintedPercentage = Mathf.Lerp(currentPercentage, 1f, value);
                    _backupedPercentage = Mathf.Lerp(backup, 1f, value);
                    _renderer.material.SetFloat("_CustomAmount2", value);
                })
                .OnComplete(() =>
                {
                    Dissable();
                });
        }
        else
        {
            int[] index = _checkInfo.Keys.ToArray();
            float[] value = _checkInfo.Values.Select(x => x.percentage).ToArray();

            float lerp = 0f;
            DOTween.To(() => lerp, x => lerp = x, 1f, duration)
                .OnUpdate(() =>
                {
                    for (int i = 0; i < index.Length; i++)
                    {
                        SetPercentage(index[i], Mathf.Lerp(value[i], 1f, lerp));
                    }

                    _renderer.material.SetFloat("_CustomAmount2", lerp);
                })
                .OnComplete(() =>
                {
                    Dissable();
                });
        }
    }

    private void SetPercentage(int region, float value)
    {
        if (!_checkInfo.ContainsKey(region)) return;

        var info = _checkInfo[region];
        info.percentage = value;
        _checkInfo[region] = info;
    }

    public void ResetComponent(List<Renderer> mr = null)
    {
        if (_renderer)
        {
            _renderer.material.SetFloat("_CustomAmount", 0);
            _renderer.material.SetFloat("_CustomAmount2", 0);
            _initialized = false;
            _renderer = null;
            _uvPixelSize = _minimumPaintedPercentage = 0.0f;
            _backupedPercentage = 0.0f;
            Clear();
        }

        _paintingWithDurationCurrentRenderer = false;

        if (mr != null)
        {
            foreach (var item in mr)
            {
                item.material.SetTexture(_maskTextureID, Texture2D.blackTexture);
                item.material.SetFloat("_CustomAmount", 0);
                item.material.SetFloat("_CustomAmount2", 0);
            }
        }
    }

    public void Dissable()
    {
        _paintingWithDurationCurrentRenderer = false;
        _initialized = false;
        _active = false;
        _paintedPercentage = 0f;
        _backupedPercentage = 0f;
        _uvArea = 0f;
    }

    public void Dissable(float time, float overrideMaxValue = 1f)
    {
        float value = _paintedPercentage;

        _active = false;
        _uvArea = 0f;

        DOTween.To(() => value, x => value = x, overrideMaxValue, time)
            .OnUpdate(() =>
            {
                _paintedPercentage = value;
            })
            .OnComplete(() =>
            {
                _initialized = false;
                _renderer = null;
            });
    }
    public void EditorInit(TextureResolution textureResolution, float extendsIslandOffset)
    {
        this.extendsIslandOffset = extendsIslandOffset;
        this.textureResolution = textureResolution;

        this.customRenderer = GetComponent<Renderer>();
    }
}