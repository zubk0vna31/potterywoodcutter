using UnityEngine;

public class TriggerPainter : MonoBehaviour
{
    [SerializeField]
    private string interactionTag;

    [SerializeField]
    private Color paintColor;

    [SerializeField, Range(0f, 1f)]
    private float radius = 1;
    [SerializeField, Range(0f, 1f)]
    private float strength = 1;
    [SerializeField, Range(0f, 1f)]
    private float hardness = 1;

    private bool active;

    public void Toggle(bool value)
    {
        active = value;
    }

    public void SetRadius(float radius)
    {
        this.radius = radius;
    }

    private void OnTriggerStay(Collider other)
    {
        if (!active || !other.CompareTag(interactionTag)) return;

        Paintable paintable = other.GetComponentInParent<Paintable>();

        if (paintable is null) return;

        //Vector3 pos = other.contacts[0].point;
        PaintManager.instance.paint(paintable, transform.position /*pos*/,
            radius, hardness, strength, paintColor);
    }
}
