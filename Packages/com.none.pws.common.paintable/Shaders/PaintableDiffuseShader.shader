Shader "Unlit/PaintableDiffuseShader"
{
    Properties
    {
        //[HideInInspector]
        _MaskTexture("Mask",2D) = "black"{}
        _Texture1("Texture 1", 2D) = "white" {}
        _Texture2 ("Texture 2", 2D) = "white" {}

        _TintColor("Tint Color", Color) = (1,1,1,1) 
        _ShadowColor("Shadow Color", Color) = (1,1,1,1) 
        [HDR]
        _AmbientColor("Ambient Color", Color) = (0.33,0.33,0.33,1) 

    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"
            #include "Lighting.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                float3 normal : NORMAL;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
                float3 worldNormal : NORMAL;
                //SHADOW_COORDS(2);
            };

            sampler2D _Texture1,_Texture2,_MaskTexture;

            float4 _Texture1_ST;
            float4 _Texture2_ST;

            half _PolishMaskAll=0;
            int _PolishAll=0;

            half4 _AmbientColor,_TintColor;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.worldNormal = UnityObjectToWorldNormal(v.normal);
                o.uv = v.uv;
                //TRANSFER_SHADOW(o);
                return o;
            }

            fixed2 GetUV(fixed2 uv,float4 st)
            {
                return fixed2(uv.xy * st.xy + st.zw);
            }

            fixed4 frag (v2f i) : SV_Target
            {
                float shadow = 1;

                float3 normal = normalize(i.worldNormal);
                float NdotL = (dot(_WorldSpaceLightPos0, normal)+1)*0.5;

                float4 light = NdotL*shadow * _LightColor0;

                float mask = tex2D(_MaskTexture,i.uv).r;

                float lerpValue = 0;

                if(_PolishAll>0)
                {
                    lerpValue = lerp(mask,1,_PolishMaskAll);
                }
                else 
                {
                    lerpValue = mask;
                }

                fixed4 tex12 = lerp(tex2D(_Texture1,GetUV(i.uv,_Texture1_ST)),
                tex2D(_Texture2,GetUV(i.uv,_Texture2_ST)),lerpValue);

                return tex12*_TintColor*(_AmbientColor+light);
            }
            ENDCG
        }
    }
}
