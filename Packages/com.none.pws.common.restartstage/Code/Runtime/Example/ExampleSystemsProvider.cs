using Leopotam.Ecs;
using Modules.Root.ContainerComponentModel;
using Modules.Root.ECS;
using UnityEngine;
using PWS.Common.Messages;
using PWS.Common.ItemDataHolder;

namespace PWS.Common.RestartStage
{
    public class ExampleSystemsProvider : MonoBehaviour, ISystemsProvider
    {
        internal class SystemsDependencies
        {
            [Inject] public IExampleDataHolder ExampleData;
        }

        public EcsSystems GetSystems(EcsWorld world, EcsSystems endFrame, EcsSystems mainSystems)
        {
            SystemsDependencies dependencies = new SystemsDependencies();
            SceneContainer.Instance.Inject(dependencies);

            EcsSystems systems = new EcsSystems(world);
            /*systems

                //restart
                .Add(new CurrentStateRestoreDataProcessing<SomeState, ExampleDataHolder>(dependencies.ExampleDataHolder))

                //other systems
                ;*/

            endFrame
                .OneFrame<RestartCurrentStepMessage>()
                ;

            return systems;
        }
    }
}
