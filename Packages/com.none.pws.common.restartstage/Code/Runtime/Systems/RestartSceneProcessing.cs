using Leopotam.Ecs;
using PWS.Common.Messages;
using PWS.Common.SceneSwitchService;
using UnityEngine.SceneManagement;

namespace PWS.Common.RestartStage
{
    public class RestartSceneProcessing : IEcsRunSystem
    {
        // auto injected fields
        private EcsFilter<RestartFirstStepMessage> _restartFirstSignal;

        private ISceneSwitchService _sceneSwitchService;

        public RestartSceneProcessing(ISceneSwitchService sceneSwitchService)
        {
            _sceneSwitchService = sceneSwitchService;
        }

        public void Run()
        {
            if (!_restartFirstSignal.IsEmpty())
            {
                _sceneSwitchService.LoadScene(SceneManager.GetActiveScene().buildIndex);
            }
        }
    }
}