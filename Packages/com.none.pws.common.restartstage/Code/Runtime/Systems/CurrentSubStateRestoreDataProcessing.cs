using Leopotam.Ecs;
using Modules.StateGroup.Components;
using Modules.StateGroup.Core;
using PWS.Common.ItemDataHolder;
using PWS.Common.Messages;
using UnityEngine;

namespace PWS.Common.RestartStage
{
    public class CurrentSubStateRestoreDataProcessing<StateT, DataT, SubStateT> : IEcsRunSystem where StateT : struct
        where DataT : IItemDataHolder, new() where SubStateT : struct
    {
        // auto injected fields
        private EcsFilter<StateEnter, SubStateT> _entering;
        private EcsFilter<SubStateT> _inState;
        private EcsFilter<RestartCurrentStepMessage> _restartCurrentSignal;

        private StateFactory _stateFactory;

        //runtime data
        private IItemDataHolder _savedData;
        private IItemDataHolder _data;

        public CurrentSubStateRestoreDataProcessing(IItemDataHolder data)
        {
            _data = data;
            _savedData = new DataT();
        }

        public void Run()
        {
            if (!_inState.IsEmpty())
            {
                if (!_restartCurrentSignal.IsEmpty())
                {
                    _data.CopyFrom(_savedData);
                    _data.SetDirty();

                    _stateFactory.SetState<StateT>();
                }
            }

            if (!_entering.IsEmpty())
            {
                _savedData.CopyFrom(_data);
            }
        }
    }
    public class CurrentSubStateRestoreDataProcessing<StateT, SubStateT> : IEcsRunSystem where StateT : struct where SubStateT : struct
    {
        // auto injected fields
        private EcsFilter<SubStateT> _inState;
        private EcsFilter<RestartCurrentStepMessage> _restartCurrentSignal;
        private StateFactory _stateFactory;

        public void Run()
        {
            if (!_inState.IsEmpty())
            {
                if (!_restartCurrentSignal.IsEmpty())
                {
                    _stateFactory.SetState<StateT>();
                }
            }
        }
    }
}