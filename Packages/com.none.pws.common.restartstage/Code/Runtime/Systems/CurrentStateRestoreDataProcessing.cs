using Leopotam.Ecs;
using Modules.StateGroup.Components;
using PWS.Common.ItemDataHolder;
using PWS.Common.Messages;

namespace PWS.Common.RestartStage
{
    public class CurrentStateRestoreDataProcessing<StateT, DataT> : IEcsRunSystem where StateT : struct where DataT : IItemDataHolder, new()
    {
        // auto injected fields
        private EcsFilter<StateEnter, StateT> _entering;
        private EcsFilter<StateT> _inState;
        private EcsFilter<RestartCurrentStepMessage> _restartCurrentSignal;

        //runtime data
        private IItemDataHolder _savedData;
        private IItemDataHolder _data;

        public CurrentStateRestoreDataProcessing(IItemDataHolder data)
        {
            _data = data;
            _savedData = new DataT();
        }

        public void Run()
        {
            if (!_inState.IsEmpty())
            {
                if (!_restartCurrentSignal.IsEmpty())
                {
                    _data.CopyFrom(_savedData);
                    _data.SetDirty();

                    foreach (var idx in _inState)
                    {
                        _inState.GetEntity(idx).Get<StateEnter>();
                        return;
                    }
                }
            }

            if (!_entering.IsEmpty())
            {
                _savedData.CopyFrom(_data);
            }
        }
    }

    public class CurrentStateRestoreDataProcessing<StateT> : IEcsRunSystem where StateT : struct
    {
        // auto injected fields
        private EcsFilter<StateT> _inState;
        private EcsFilter<RestartCurrentStepMessage> _restartCurrentSignal;

        public void Run()
        {
            if (!_inState.IsEmpty())
            {
                if (!_restartCurrentSignal.IsEmpty())
                {
                    foreach (var idx in _inState)
                    {
                        _inState.GetEntity(idx).Get<StateEnter>();
                        return;
                    }
                }
            }
        }
    }
}