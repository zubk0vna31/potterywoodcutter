﻿using Leopotam.Ecs;
using Modules.ViewHub;
using UnityEngine;

namespace PWS.Common.GrandDad.ViewComponents
{
    public class GrandDadAnimatorRefViewComponent : ViewComponent
    {
        [SerializeField] private Animator _animator;
        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            ecsEntity.Get<GrandDadData>().Animator = _animator;
        }
    }
}