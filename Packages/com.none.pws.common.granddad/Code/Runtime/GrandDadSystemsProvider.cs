﻿using Leopotam.Ecs;
using UnityEngine;
using Modules.Root.ECS;

namespace PWS.Common.GrandDad
{
    public class GrandDadSystemsProvider : MonoBehaviour, ISystemsProvider
    {
        public EcsSystems GetSystems(EcsWorld world, EcsSystems endFrame, EcsSystems mainSystems)
        {
            EcsSystems systems = new EcsSystems(world);
            
            systems
                .Add(new GrandDadRotationSystem())
                .Add(new GrandDadAnimationSystem())
               
                ;

            return systems;
        }
    }
}