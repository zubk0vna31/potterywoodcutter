﻿using Leopotam.Ecs;
using Modules.Audio;

namespace PWS.Common.GrandDad
{
    public class GrandDadAnimationSystem : IEcsRunSystem
    {
        private readonly EcsFilter<GrandDadData, AudioSourceRef> _dad;
        public void Run()
        {
            foreach (var dad in _dad)
            {
                if (_dad.Get2(dad).AudioSource.isPlaying)
                {
                    _dad.Get1(dad).Animator.SetBool("Talk", true);
                }
                else if (!_dad.Get2(dad).AudioSource.isPlaying)
                {
                    _dad.Get1(dad).Animator.SetBool("Talk", false);
                }
            }
        }
    }
}