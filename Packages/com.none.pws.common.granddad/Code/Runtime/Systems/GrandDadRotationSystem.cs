﻿using Leopotam.Ecs;
using Modules.ViewHub;
using PWS.Common.Audio;
using PWS.Tutorial;
using UnityEngine;

namespace PWS.Common.GrandDad
{
    public class GrandDadRotationSystem : IEcsRunSystem
    {
        private readonly EcsFilter<UnityView, GrandDadTag> _dad;
        private readonly EcsFilter<PlayerTag, UnityView> _player;

        private Vector3 _targetPosition;
        public void Run()
        {
            foreach (var dad in _dad)
            {
                foreach (var player in _player)
                {
                    _targetPosition = _player.Get2(player).Transform.position;
                    _targetPosition.y = 0;
                    _dad.Get1(dad).Transform.LookAt(_targetPosition);
                }
            }
        }
    }
}