using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PWS.Common.ItemDataHolder
{
    public class ExampleDataHolder : IExampleDataHolder
    {
        private float _size;
        public float Size => _size;

        public void CopyFrom(IItemDataHolder itemData)
        {
            ExampleDataHolder data = (ExampleDataHolder)itemData;
            _size = data._size;
        }

        public void SetDirty()
        {
            //update or set bool isUpdated = true
        }
    }
}
