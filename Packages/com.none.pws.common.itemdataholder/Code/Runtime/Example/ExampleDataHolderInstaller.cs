using Modules.Root.ContainerComponentModel;

namespace PWS.Common.ItemDataHolder
{
    // mono installer to allow runtime modification and observation of serialized data
    public class ExampleDataHolderInstaller : AMonoInstaller
    {
        public override void Install(IContainer container)
        {
            IExampleDataHolder itemDataHolder = new ExampleDataHolder();
            container.Bind(itemDataHolder);

            //��������� ��� ������ ������ ������
            //�������� ��� �� ������ Functional/Sevices
            //��������� ���������� ���������� � SceneContainer/MonoInstallers
        }
    }
}