namespace PWS.Common.ItemDataHolder
{
    public interface IExampleDataHolder : IItemDataHolder
    {
        float Size { get; }
    }
}
