﻿namespace PWS.Common.ItemDataHolder
{
    public interface IItemDataHolder
    {
        void CopyFrom(IItemDataHolder itemData);
        void SetDirty();
    }
}