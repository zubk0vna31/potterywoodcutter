using UnityEngine;

namespace PWS.Pottery.Common
{
    // mono installer to allow runtime modification and observation of serialized data
    public class MaterialsReseter : MonoBehaviour
    {
        [SerializeField]
        private Material[] _materials;
        [SerializeField]
        private bool _resetOnStart = true;

        private void Start()
        {
            if (!_resetOnStart) return;

            Debug.Log("Reset Mats");
            foreach(Material mat in _materials)
            {
                mat.SetFloat("_Wetness", 0);
                mat.SetFloat("_Dryness", 0);
                mat.SetFloat("_Firing", 0);
                mat.SetFloat("_FiringSpecular", 0);

                mat.SetTexture("_DecSculptTex", null);
                mat.SetTexture("_DecSculptBumpMap", null);
                mat.SetTexture("_DecSculptMask", null);

                mat.SetTexture("_DecPaintTex", null);
                mat.SetTexture("_DecPaintMask", null);

                mat.SetTexture("_GlazingMask", null);
            }
        }
    }
}