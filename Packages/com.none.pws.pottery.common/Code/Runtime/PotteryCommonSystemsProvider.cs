using Leopotam.Ecs;
using Modules.Root.ContainerComponentModel;
using Modules.Root.ECS;
using Modules.SGMBLinker;
using Modules.StateGroup.Core;
using Modules.StateGroup.Systems;
using PWS.Common.Messages;
using PWS.Common.RestartStage;
using PWS.Common.ResultsSubmission;
using PWS.Common.ResultsSubmission.ResultCapture;
using PWS.Common.SceneSwitchService;
using UnityEngine;

namespace PWS.Pottery.Common
{
    public class PotteryCommonSystemsProvider : MonoBehaviour, ISystemsProvider
    {
        internal class SystemsDependencies
        {
            [Inject] public ISceneSwitchService SceneSwitchService;
            [Inject] public IResultsSubmissionService ResultsSubmissionService;
        }

        // auto injected fields
        [SerializeField] private SetStateSOCommand _initState;

        public EcsSystems GetSystems(EcsWorld world, EcsSystems endFrame, EcsSystems mainSystems)
        {
            SystemsDependencies dependencies = new SystemsDependencies();
            SceneContainer.Instance.Inject(dependencies);

            EcsSystems systems = new EcsSystems(world);
            systems
                .Add(new RXMessagePropagator<RestartCurrentStepMessage>())
                .Add(new RXMessagePropagator<RestartFirstStepMessage>())

                .Add(new RestartSceneProcessing(dependencies.SceneSwitchService))
                .Add(new InitStateSetupSystem(_initState))
                
                .Add(new ResultsSubmissionSystem(dependencies.ResultsSubmissionService, "Pottery"))
                .OneFrame<SubmitResultsSignal>()
                ;
            
            return systems;
        }
    }
}