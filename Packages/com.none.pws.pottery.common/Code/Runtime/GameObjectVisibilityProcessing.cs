using Leopotam.Ecs;
using Modules.StateGroup.Components;
using Modules.ViewHub;
using UnityEngine;

namespace PWS.Pottery.Common
{
    public class GameObjectVisibilityProcessing<StateT, ViewT> : IEcsRunSystem, IEcsInitSystem where StateT : struct where ViewT : struct
    {
        // auto injected fields
        protected readonly EcsFilter<StateEnter, StateT> _entering;
        protected readonly EcsFilter<StateExit, StateT> _exiting;
        protected readonly EcsFilter<StateT> _inState;

        protected readonly EcsFilter<UnityView, ViewT> _view;
        protected readonly EcsFilter<VisibilitySignal, ViewT> _visibilitySignal;

        private bool _listenSignal;

        public GameObjectVisibilityProcessing(bool listenSignal = false)
        {
            _listenSignal = listenSignal;
        }

        public void Init()
        {
            foreach (var i in _view)
            {
                _view.Get1(i).GameObject.SetActive(false);
            }
        }

        public virtual void Run()
        {
            if (!_entering.IsEmpty())
            {
                foreach (var i in _view)
                {

                    Debug.Log(typeof(ViewT) + " : " + typeof(StateT) + "_entering");
                    _view.Get1(i).GameObject.SetActive(true);
                }
            }
            if (!_exiting.IsEmpty())
            {
                foreach (var i in _view)
                {
                    Debug.Log(typeof(ViewT) + " : " + typeof(StateT) + "_exiting");
                    _view.Get1(i).GameObject.SetActive(false);
                }
            }

            if (!_listenSignal)
                return;
            if (_inState.IsEmpty())
                return;

            foreach (var idx in _visibilitySignal)
            {
                foreach (var i in _view)
                {
                    _view.Get1(i).GameObject.SetActive(_visibilitySignal.Get1(idx).Show);
                }
            }
        }
    }
}
