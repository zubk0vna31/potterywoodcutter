using Leopotam.Ecs;
using Modules.Audio;
using PWS.Common.Audio;
using PWS.Common.GameModeInfoService;

namespace PWS.Pottery.Common
{
    public class GameModeBasedVoiceVolumeSetup : IEcsInitSystem
    {
        private readonly EcsFilter<AudioSourceRef, GrandDadTag> _grandDad;

        private GameMode _gameMode;

        public GameModeBasedVoiceVolumeSetup(GameMode gameMode)
        {
            _gameMode = gameMode;
        }

        public void Init()
        {
            if (_gameMode == GameMode.Tutorial)
                return;

            foreach (var grandDad in _grandDad)
            {
                _grandDad.Get1(grandDad).AudioSource.enabled = false;
            }
        }
    }
}
