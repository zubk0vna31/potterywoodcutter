﻿using Modules.Root.ContainerComponentModel;
using Modules.VRFeatures;
using PWS.Common.GameModeInfoService;
using UnityEditor;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace PWS.Pottery.Sculpting.Interactor
{
    [RequireComponent(typeof(XRBaseInteractable))]
    public class GameModeBasedHandInteractableMaterialChanger : InteractableMaterialChanger
    {
        [Inject] private IGameModeInfoService _gameModeData;

        private GameMode _gameMode;

        public void Start()
        {
            SceneContainer.Instance.Inject(this);
            _gameMode = _gameModeData.CurrentMode;

            SetMaterial(DefaultMaterial);
        }

        public override void SetMaterial(Material mat)
        {
            //Debug.Log(_gameMode);
            if (_gameMode != GameMode.Tutorial)
            {
                foreach (Renderer rend in Renderers)
                {
                    rend.enabled = mat != DefaultMaterial;

                    if (rend.material != SelectedMaterial)
                        rend.material = SelectedMaterial;
                }

                return;
            }

            base.SetMaterial(mat);
        }
    }
}