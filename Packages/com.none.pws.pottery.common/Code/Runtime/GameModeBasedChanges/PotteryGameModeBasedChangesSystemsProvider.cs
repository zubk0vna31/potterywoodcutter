using Leopotam.Ecs;
using Modules.Root.ContainerComponentModel;
using Modules.Root.ECS;
using PWS.Common.GameModeInfoService;
using UnityEngine;

namespace PWS.Pottery.Common
{
    public class PotteryGameModeBasedChangesSystemsProvider : MonoBehaviour, ISystemsProvider
    {
        internal class SystemsDependencies
        {
            [Inject] public IGameModeInfoService GameModeData;
        }

        public EcsSystems GetSystems(EcsWorld world, EcsSystems endFrame, EcsSystems mainSystems)
        {
            SystemsDependencies dependencies = new SystemsDependencies();
            SceneContainer.Instance.Inject(dependencies);

            EcsSystems systems = new EcsSystems(world);
            systems
                .Add(new GameModeBasedForceSkipSetup(dependencies.GameModeData.CurrentMode))
                .Add(new GameModeBasedVoiceVolumeSetup(dependencies.GameModeData.CurrentMode))
            ;
            return systems;
        }
    }
}