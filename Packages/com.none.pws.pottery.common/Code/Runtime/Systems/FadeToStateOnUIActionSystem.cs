﻿using Leopotam.Ecs;
using Modules.StateGroup.Core;
using PWS.Common.SceneSwitchService;
using PWS.Common.UI;

namespace PWS.Pottery.Common
{
    public class FadeToStateOnUIActionSystem<StateT> : IEcsRunSystem where StateT : struct
    {
        // auto injected fields
        protected readonly EcsFilter<StateT> _inState;
        protected readonly EcsFilter<NextStateSignal> _actionPerformed;
        protected readonly StateFactory _stateFactory;

        protected readonly SetStateSOCommand _nextState;
        private readonly ISceneSwitchService _sceneSwitchService;

        public FadeToStateOnUIActionSystem(SetStateSOCommand nextState, ISceneSwitchService sceneSwitchService)
        {
            _nextState = nextState;
            _sceneSwitchService = sceneSwitchService;
        }

        public virtual void Run()
        {
            if (_inState.IsEmpty()) return;
            if (_actionPerformed.IsEmpty()) return;

            foreach (var i in _actionPerformed)
            {
                _actionPerformed.GetEntity(i).Del<NextStateSignal>();
            }

            _sceneSwitchService.NextState(() => _nextState.Execute(_stateFactory));

        }
    }
}