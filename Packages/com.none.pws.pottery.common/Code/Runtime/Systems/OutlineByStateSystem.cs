﻿using Leopotam.Ecs;
using Modules.StateGroup.Components;
using PWS.WoodCutting.Common;

namespace PWS.Pottery.Common
{
    public class OutlineByStateSystem<T> : IEcsRunSystem where T : struct
    {
        private readonly EcsFilter<StateEnter, T> _stateEnter;
        private readonly EcsFilter<ToolOutline> _tools;
        private readonly int _stateID;

        public OutlineByStateSystem(int stateID)
        {
            _stateID = stateID;
        }

        public void Run()
        {
            if (!_stateEnter.IsEmpty())
            {
                foreach (var tool in _tools)
                {
                    _tools.Get1(tool).view.UpdateStateDependency(_stateID);
                }
            }
        }
    }
}