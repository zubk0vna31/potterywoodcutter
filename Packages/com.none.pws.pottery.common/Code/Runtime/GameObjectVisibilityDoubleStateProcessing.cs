using Leopotam.Ecs;
using Modules.StateGroup.Components;

namespace PWS.Pottery.Common
{
    public class GameObjectVisibilityDoubleStateProcessing<StateT, ViewT, SecondStateT> : GameObjectVisibilityProcessing<StateT, ViewT>
        where StateT : struct where ViewT : struct where SecondStateT : struct
    {
        readonly EcsFilter<StateEnter, SecondStateT> _enteringSecond;

        public override void Run()
        {
            if (!_exiting.IsEmpty() && !_enteringSecond.IsEmpty())
                return;

            base.Run();
        }
    }
}
