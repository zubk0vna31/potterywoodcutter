using UnityEngine;

namespace PWS.Pottery.Common
{
    public struct ClayMeshRenderer
    {
        public MeshRenderer MeshRenderer;
    }
}
