using UnityEngine;

namespace PWS.Pottery.Common
{
    public struct LineRendererComponent
    {
        public LineRenderer LineRenderer;
    }
}
