using UnityEngine;

namespace PWS.Pottery.Common
{
    public struct MeshFilterComponent
    {
        public MeshFilter MeshFilter;
    }
}
