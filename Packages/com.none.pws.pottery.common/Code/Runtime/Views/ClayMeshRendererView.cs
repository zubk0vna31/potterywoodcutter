using Leopotam.Ecs;
using Modules.ViewHub;
using UnityEngine;

namespace PWS.Pottery.Common
{
    public class ClayMeshRendererView : ViewComponent
    {
        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            ecsEntity.Get<ClayMeshRenderer>().MeshRenderer = GetComponent<MeshRenderer>();
        }
    }
}
