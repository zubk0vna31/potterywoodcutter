using Leopotam.Ecs;
using Modules.ViewHub;
using UnityEngine;

namespace PWS.Pottery.Common
{
    public class LineRendererRef : ViewComponent
    {
        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            ecsEntity.Get<LineRendererComponent>().LineRenderer = GetComponent<LineRenderer>();
        }
    }
}
