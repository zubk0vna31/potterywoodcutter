using Leopotam.Ecs;
using Modules.ViewHub;
using UnityEngine;

namespace PWS.Pottery.Common
{
    public class MeshFilterRef : ViewComponent
    {
        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            ecsEntity.Get<MeshFilterComponent>().MeshFilter = GetComponent<MeshFilter>();
        }
    }
}
