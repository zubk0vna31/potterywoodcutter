using Modules.StateGroup.Core;
using UnityEngine;

namespace PWS.Pottery.Common
{
    public struct DefaultState
    {

    }

    [CreateAssetMenu(menuName = "PWS/WoodCutting/Stages/Default/SetStateCommand")]
    public class DefaultStateSetStateSOCommand : SetStateSOCommand<DefaultState>
    {

    }
}
