﻿using UnityEngine;
using Leopotam.Ecs;

namespace Modules.Utils
{
    /// <summary>
    /// Utils.WaitTimer
    /// Attaches to entity and present for specified remaining time
    /// when the time passes, component removes from the entity
    /// </summary>

    public struct WaitTimer
    {
        public float RemainingTime;
    }

    public class WaitTimerSystem : IEcsRunSystem
    {
        readonly EcsFilter<WaitTimer> _filter;
        readonly Utils.TimeService _time;

        public void Run()
        {
            if (_filter.IsEmpty())
                return;

            foreach (var i in _filter)
            {
                _filter.Get1(i).RemainingTime -= _time.DeltaTime;
                if(_filter.Get1(i).RemainingTime <= 0.0f) 
                {
                    _filter.GetEntity(i).Del<WaitTimer>();
                }
            }
        }
    }
}