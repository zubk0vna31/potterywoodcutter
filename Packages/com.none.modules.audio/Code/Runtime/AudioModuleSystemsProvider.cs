﻿using Leopotam.Ecs;
using Modules.Root.ECS;
using UnityEngine;

namespace Modules.Audio
{
    public class AudioModuleSystemsProvider : MonoBehaviour, ISystemsProvider
    {
        public EcsSystems GetSystems(EcsWorld world, EcsSystems endFrame, EcsSystems mainSystems)
        {
           EcsSystems systems = new EcsSystems(world);
            systems
                .Add(new PlaySoundAtPointSystem())
                .Add(new PlayLocalizedVoiceAtPointSystem())
                .Add(new PlayGlobalSoundSystem())
                .Add(new PlayGlobalLocalizedVoiceSystem())
                ;
            endFrame
                .OneFrame<PlaySoundAtPointSignal>()
                .OneFrame<PlayGlobalAuidioSignal>()
                .OneFrame<PlayLocalizedVoiceAtPointSignal>()
                .OneFrame<PlayGlobalLocalizedVoiceSignal>()
                ;

            return systems;
        }
    }
}