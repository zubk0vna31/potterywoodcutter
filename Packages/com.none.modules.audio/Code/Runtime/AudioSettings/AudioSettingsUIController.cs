﻿using Modules.Root.ContainerComponentModel;
using UnityEngine;
using UnityEngine.UI;

namespace Modules.Audio
{
    public class AudioSettingsUIController : MonoBehaviour
    {
        [SerializeField] private Slider _voiceSlider;
        [SerializeField] private Slider _fxSlider;

        [Inject] private AudioSettingsService _service;

        private void Awake()
        {
            AppContainer.Inject(this);

            _voiceSlider.value = _service.CurrentVoiceVolume;
            _fxSlider.value = _service.CurrentFXVolume;
            
            _voiceSlider.onValueChanged.AddListener((value) => _service.SetVoiceVolume(value));
            _fxSlider.onValueChanged.AddListener((value) => _service.SetFXVolume(value));
        }

        private void OnEnable()
        {
            _voiceSlider.value = _service.CurrentVoiceVolume;
            _fxSlider.value = _service.CurrentFXVolume;
        }
    }
}