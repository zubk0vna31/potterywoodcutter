﻿using System.Collections;
using UnityEngine;
using UnityEngine.Audio;

namespace Modules.Audio
{
    public class AudioSettingsService : MonoBehaviour
    {
        public const string VOICE_VOLUME_SAVE_KEY = "voice_volume_save";
        public const string FX_VOLUME_SAVE_KEY = "fx_volume_save";
        
        private const float MIN_DEFAULT_RANGE = 0f;
        private const float MAX_DEFAULT_RANGE = 1f;
        
        [Range(MIN_DEFAULT_RANGE, MAX_DEFAULT_RANGE)] 
        [SerializeField] private float _currentVoiceVolume = 0.5f;
        [Range(MIN_DEFAULT_RANGE, MAX_DEFAULT_RANGE)]
        [SerializeField] private float _currentFXVolume = 0.5f;
        
        [Header("Mixer config")]
        [SerializeField] private AudioMixer _mixer;
        [SerializeField] private float _minMixerVolume = -80f;
        [SerializeField] private float _maxMixerVolume = 20f;

        public float CurrentVoiceVolume => _currentVoiceVolume;
        public float CurrentFXVolume => _currentFXVolume;
        
        private void Awake()
        {
            if (PlayerPrefs.HasKey(VOICE_VOLUME_SAVE_KEY))
            {
                _currentVoiceVolume = PlayerPrefs.GetFloat(VOICE_VOLUME_SAVE_KEY);
            }

            if (PlayerPrefs.HasKey(FX_VOLUME_SAVE_KEY))
            {
                _currentFXVolume = PlayerPrefs.GetFloat(FX_VOLUME_SAVE_KEY);
            }
        }

        private IEnumerator Start()
        {
            yield return null;
            SetVolume(VOICE_VOLUME_SAVE_KEY, _currentVoiceVolume);
            SetVolume(FX_VOLUME_SAVE_KEY, _currentFXVolume);
        }

        public void SetVoiceVolume(float voiceVolumeValue)
        {
            _currentVoiceVolume = voiceVolumeValue;
            SetVolume(VOICE_VOLUME_SAVE_KEY, voiceVolumeValue);
        }
        
        public void SetFXVolume(float fxVolumeValue)
        {
            _currentFXVolume = fxVolumeValue;
            SetVolume(FX_VOLUME_SAVE_KEY, fxVolumeValue);
        }

        private void SetVolume(string key, float value)
        {
            var mixerValue = Mathf.Lerp(_minMixerVolume, _maxMixerVolume, value);
            _mixer.SetFloat(key, mixerValue);
            PlayerPrefs.SetFloat(key, value);
            PlayerPrefs.Save();
        }
    }
}