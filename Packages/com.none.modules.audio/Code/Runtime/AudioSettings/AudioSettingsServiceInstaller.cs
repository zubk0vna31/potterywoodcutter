﻿using Modules.Root.ContainerComponentModel;
using UnityEngine;

namespace Modules.Audio
{
    public class AudioSettingsServiceInstaller : AMonoInstaller
    {
        [SerializeField] private AudioSettingsService _audioSettingsService;
        
        public override void Install(IContainer container)
        {
            container.Bind<AudioSettingsService>(_audioSettingsService);
        }
    }
}