﻿using I2.Loc;
using UnityEngine;

namespace Modules.Audio
{
    public struct AudioSourceRef
    {
        public AudioSource AudioSource;
        public Localize VoiceLocalize;
        public Transform Transform;
    }
}