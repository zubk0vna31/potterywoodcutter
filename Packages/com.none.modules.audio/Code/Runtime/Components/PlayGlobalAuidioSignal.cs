﻿using I2.Loc;
using UnityEngine;

namespace Modules.Audio
{
    public struct PlayGlobalAuidioSignal
    {
        public AudioClip AudioClip;
        public AudioSource AudioSource;
    }
    public struct PlayGlobalLocalizedVoiceSignal
    {
        public string Term;
        public Localize VoiceLocalize;
        public AudioSource AudioSource;
    }
}