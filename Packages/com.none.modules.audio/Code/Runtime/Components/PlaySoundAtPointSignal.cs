﻿using UnityEngine;

namespace Modules.Audio
{
    public struct PlaySoundAtPointSignal
    {
        public OneShotSoundTemplate SoundPrefab;
        public Vector3 PlayPosition;
        public AudioClip AudioClip;
    }
    public struct PlayLocalizedVoiceAtPointSignal
    {
        public OneShotSoundTemplate SoundPrefab;
        public Vector3 PlayPosition;
        public string Term;
    }
}