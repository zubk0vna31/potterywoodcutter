﻿using Leopotam.Ecs;
using Modules.ViewHub;
using UnityEngine;

namespace Modules.Audio
{
    public class AmbienceSoundTemplate : EntityTemplate
    {
        [SerializeField] private AudioSource _audioSource;
        [SerializeField] private Transform _transform;
        
        protected override void SetupEntity(EcsEntity entity, EcsWorld world)
        {
            base.SetupEntity(entity, world);
            entity.Get<AmbienceSoundTag>();
            ref var entityRef = ref entity.Get<AudioSourceRef>();
            entityRef.AudioSource = _audioSource;
            entityRef.Transform = _transform;
        }
    }
}