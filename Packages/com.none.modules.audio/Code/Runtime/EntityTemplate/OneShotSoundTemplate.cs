﻿using I2.Loc;
using Leopotam.Ecs;
using Modules.ViewHub;
using UnityEngine;

namespace Modules.Audio
{
    public class OneShotSoundTemplate : EntityTemplate
    {
        [SerializeField] private Localize _localize;
        [SerializeField] private AudioSource _audioSource;
        [SerializeField] private Transform _transform;

        protected override void SetupEntity(EcsEntity entity, EcsWorld world)
        {
            base.SetupEntity(entity, world);
            entity.Get<OneShotSource>();
            ref var entityRef = ref entity.Get<AudioSourceRef>();
            entityRef.VoiceLocalize = _localize;
            entityRef.AudioSource = _audioSource;
            entityRef.Transform = _transform;
        }
    }
}