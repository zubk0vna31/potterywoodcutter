﻿using Leopotam.Ecs;
using Modules.UIMessaging;
using UnityEngine;

namespace Modules.Audio
{
    public class PlayLocalizedVoiceAtPointSystem : IEcsRunSystem, IEcsInitSystem
    {
        private readonly EcsFilter<PlayLocalizedVoiceAtPointSignal> _signal;
        private readonly EcsFilter<OneShotSource, AudioSourceRef> _oneShotSource;
        private readonly EcsWorld _ecsWorld;
        
        private bool _isIDFinded;
        private IStringToIDMappingService _mapping;
        
        public void Init()
        {
            _mapping = new StringToIDMappingService();
        }
        
        public void Run()
        {
            if (_signal.IsEmpty())
                return;
            
            foreach (var signal in _signal)
            {
                _isIDFinded = false;
                ref var soundAtPointSignal = ref _signal.Get1(signal);
                var id = _mapping.GetID(soundAtPointSignal.SoundPrefab.name);
                foreach (var source in _oneShotSource)
                {
                    ref var oneShotSource = ref _oneShotSource.Get1(source);
                    ref var audioSourceRef = ref _oneShotSource.Get2(source);
                    if (id == oneShotSource.SourceID)
                    {
                        if (audioSourceRef.AudioSource.isPlaying == false)
                        {
                            _isIDFinded = true;
                            audioSourceRef.Transform.position = soundAtPointSignal.PlayPosition;
                            
                            if (soundAtPointSignal.Term != null && audioSourceRef.VoiceLocalize)
                            {
                                audioSourceRef.VoiceLocalize.SetTerm(soundAtPointSignal.Term);
                            }
                            
                            audioSourceRef.AudioSource.Play();
                            break;
                        }
                    }
                }
                if (_isIDFinded == false)
                {
                    var newEntity = _ecsWorld.NewEntity();
                    var newObject = Object.Instantiate
                        (
                        soundAtPointSignal.SoundPrefab, 
                        soundAtPointSignal.PlayPosition,
                        Quaternion.identity
                        );
                    newObject.Init(newEntity, _ecsWorld);
                    newEntity.Get<OneShotSource>().SourceID = id;
                    ref var newEntityPlay = ref newEntity.Get<AudioSourceRef>();
                    
                    if (soundAtPointSignal.Term != null && newEntityPlay.VoiceLocalize)
                    {
                        newEntityPlay.VoiceLocalize.SetTerm(soundAtPointSignal.Term);
                    }
                    newEntityPlay.AudioSource.Play();
                }
            }
        }
    }
}