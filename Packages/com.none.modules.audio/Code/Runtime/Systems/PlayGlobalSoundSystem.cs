﻿using Leopotam.Ecs;

namespace Modules.Audio
{
    public class PlayGlobalSoundSystem : IEcsRunSystem
    {
        private readonly EcsFilter<PlayGlobalAuidioSignal> _signal;
        private readonly EcsFilter<AudioSourceRef, GlobalAudioSourceTag> _globalAudioSource;
        
        public void Run()
        {
            if (_signal.IsEmpty())
                return;

            foreach (var signal in _signal)
            {
                ref var audioSignal = ref _signal.Get1(signal);
                
                foreach (var source in _globalAudioSource)
                {
                    if (_signal.Get1(signal).AudioSource != null)
                    {
                        audioSignal.AudioSource.clip = audioSignal.AudioClip;
                        audioSignal.AudioSource.Play();
                    }
                    else
                    {
                        ref var globalAudioSource = ref _globalAudioSource.Get1(source).AudioSource;
                        globalAudioSource.clip = audioSignal.AudioClip;
                        globalAudioSource.Play();
                    }
                }
            }
        }
    }
}