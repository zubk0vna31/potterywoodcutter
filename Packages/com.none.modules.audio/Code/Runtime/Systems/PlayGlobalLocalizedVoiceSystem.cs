﻿using Leopotam.Ecs;

namespace Modules.Audio
{
    public class PlayGlobalLocalizedVoiceSystem : IEcsRunSystem
    {
        private readonly EcsFilter<PlayGlobalLocalizedVoiceSignal> _signal;
        private readonly EcsFilter<AudioSourceRef, GlobalAudioSourceTag> _globalAudioSource;
        
        public void Run()
        {
            if (_signal.IsEmpty())
                return;

            foreach (var signal in _signal)
            {
                ref var audioSignal = ref _signal.Get1(signal);
                
                foreach (var source in _globalAudioSource)
                {
                    if (_signal.Get1(signal).VoiceLocalize != null)
                    {
                        audioSignal.VoiceLocalize.SetTerm(audioSignal.Term);
                        audioSignal.AudioSource.Play();
                    }
                    else
                    {
                        ref var globalAudioSource = ref _globalAudioSource.Get1(source);
                        if (globalAudioSource.VoiceLocalize)
                        {
                            globalAudioSource.VoiceLocalize.SetTerm(audioSignal.Term);
                            globalAudioSource.AudioSource.Play();
                        }
                    }
                }
            }
        }
    }
}