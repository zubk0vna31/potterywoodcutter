﻿using Leopotam.Ecs;
using Modules.ViewHub;
using UnityEngine;

namespace Modules.Audio
{
    public class AudioSourceViewComponent : ViewComponent
    {
        [SerializeField] private AudioSource _audioSource;
        [SerializeField] private Transform _transform;
        
        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            ref var entity = ref ecsEntity.Get<AudioSourceRef>();
            entity.AudioSource = _audioSource;
            entity.Transform = _transform;
        }
    }
}