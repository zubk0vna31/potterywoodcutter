﻿using I2.Loc;
using Leopotam.Ecs;
using Modules.ViewHub;
using UnityEngine;

namespace Modules.Audio
{
    public class GlobalAudioSourceViewComponent : ViewComponent
    {
        [SerializeField] private Transform _transform;
        [SerializeField] private AudioSource _audioSource;
        [SerializeField] private Localize _localize;

        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            ref var sourceRef = ref ecsEntity.Get<AudioSourceRef>();
            sourceRef.AudioSource = _audioSource;
            sourceRef.Transform = _transform;
            sourceRef.VoiceLocalize = _localize;
            ecsEntity.Get<GlobalAudioSourceTag>();
        }
    }
}