﻿using Leopotam.Ecs;
using Modules.ViewHub;
using UnityEngine;

namespace Modules.Audio
{
    public class ForTestGlobalAudioTemplate : EntityTemplate
    {
        private EcsEntity _entity;

        [SerializeField] private AudioClip _buttonsClip;
        [SerializeField] private AudioClip _menuClip;
        
        protected override void SetupEntity(EcsEntity entity, EcsWorld world)
        {
            base.SetupEntity(entity, world);
            _entity = entity;
        }

        public void TestButton()
        {
            _entity.Get<PlayGlobalAuidioSignal>().AudioClip = _buttonsClip;
        }

        public void TestMenu()
        {
            _entity.Get<PlayGlobalAuidioSignal>().AudioClip = _menuClip;
        }
    }
}