﻿using Leopotam.Ecs;
using Modules.UPhysics;

namespace Modules.Audio
{
    public class TestItemSoundSystem : IEcsRunSystem
    {
        private readonly EcsFilter<Triggered, AudioSourceRef> _triggered;
        
        public void Run()
        {
            if (_triggered.IsEmpty())
                return;
            
            foreach (var i in _triggered)
            {
                ref var source = ref _triggered.Get2(i).AudioSource;
                source.PlayOneShot(source.clip);
                _triggered.GetEntity(i).Del<Triggered>();
            }
        }
    }
}