﻿using Leopotam.Ecs;
using Modules.ViewHub;
using UnityEngine;

namespace Modules.Audio
{
    public class ForStepTestTemplate : EntityTemplate
    {
        private EcsEntity _entity;
        
        [SerializeField] private OneShotSoundTemplate _prefab;
        [SerializeField] private AudioClip _clip;
        
        protected override void SetupEntity(EcsEntity entity, EcsWorld world)
        {
            base.SetupEntity(entity, world);
            _entity = entity;
        }

        public void TestPointSignal(Transform position)
        {
            ref var entity = ref _entity.Get<PlaySoundAtPointSignal>();
            entity.AudioClip = _clip;
            entity.PlayPosition = position.position;
            entity.SoundPrefab = _prefab;
        }
    }
}