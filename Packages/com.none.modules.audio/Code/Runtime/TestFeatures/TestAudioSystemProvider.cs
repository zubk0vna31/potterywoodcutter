﻿using Leopotam.Ecs;
using Modules.Root.ECS;
using UnityEngine;

namespace Modules.Audio
{
    public class TestAudioSystemProvider : MonoBehaviour, ISystemsProvider
    {
        public EcsSystems GetSystems(EcsWorld world, EcsSystems endFrame, EcsSystems mainSystems)
        {
            EcsSystems systems = new EcsSystems(world);
            
            systems
                .Add(new TestItemSoundSystem())
                ;
            
            return systems;
        }
    }
}