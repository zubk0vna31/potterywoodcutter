
namespace PWS.WoodCutting.Stages.ItemPolishing
{
    public interface IItemPolishingDataHolder 
    {
        public StickyObject GetStickyObject(int key);
        public void AddStickyObject(int key, StickyObject stickyObject);
    }
}
