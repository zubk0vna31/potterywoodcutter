using Modules.StateGroup.Core;
using UnityEngine;

namespace PWS.WoodCutting
{
    [CreateAssetMenu(menuName = "PWS/WoodCutting/Stages/Item Polishing/SetStateSOCommand")]
    public class SetItemPolishingStateSOCommand : SetStateSOCommand<ItemPolishingState>
    {

    }
}
