using Modules.Root.ContainerComponentModel;
using UnityEngine;

namespace PWS.WoodCutting.Stages.ItemPolishing
{
    public class ItemPolishingDataHolderInstaller : AMonoInstaller
    {
        public override void Install(IContainer container)
        {
            IItemPolishingDataHolder dataHolder = new ItemPolishingDataHolder();

            container.Bind(dataHolder);
        }

        
    }
}
