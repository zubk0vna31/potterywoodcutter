using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PWS.WoodCutting.Stages.ItemPolishing
{
    public class ItemPolishingDataHolder : IItemPolishingDataHolder
    {
        private Dictionary<int, StickyObject> stickyObjects =
             new Dictionary<int, StickyObject>(5);

        public void AddStickyObject(int key, StickyObject stickyObject)
        {
            stickyObjects[key] = stickyObject;
        }

        public StickyObject GetStickyObject(int key)
        {
            return stickyObjects[key];
        }
    }
}
