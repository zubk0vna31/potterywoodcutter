using PWS.WoodCutting.Common;
using UnityEngine;

namespace PWS.WoodCutting
{
    [CreateAssetMenu(fileName = "Glue Removal Stage Config",
        menuName = "PWS/WoodCutting/Stages/Item Polishing/Config")]
    public class ItemPolisingStateConfig : StateConfig
    {
        [Header("String")]
        public string buttonSubstageTitleTerm;
        public string buttonSubstageTitle;

        [Range(0f,1f)]
        public float skipThreshold = 0.7f;
        [Range(0f, 1f)]
        public float skipDuration = 1f;

        [Range(0f, 5f)]
        public float rotationDuration = 0.7f;

        [Range(0f, 5f)]
        public float checkInterval = 0.7f; 
    }
}
