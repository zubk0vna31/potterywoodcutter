using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace PWS.WoodCutting
{
    public struct PlankPaintable
    {
        public Dictionary<Renderer, float> uvAreas;
        public Paintable paintable;
        public Dictionary<Renderer, Collider> renderersColliders;
        public Renderer mid;
        public int completedAmount;

        public Renderer Last => renderersColliders.ElementAt(completedAmount).Key;

        public void ResetUnityComponents()
        {
            completedAmount = 0;

            var arr = uvAreas.Keys.ToList();
            arr.Add(mid);

            paintable.ResetComponent(arr);
        }
    }
}
