using Leopotam.Ecs;
using Modules.ViewHub;
using PWS.WoodCutting.Common;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace PWS.WoodCutting
{
    public class PlankPaintableViewComponent : ViewComponent
    {

        [SerializeField]
        private Paintable paintable;
        [SerializeField]
        private Renderer mid;

        [SerializeField]
        private List<Renderer> renderers;

        [SerializeField]
        private List<float> uvAreas;

        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            ref var plank = ref ecsEntity.Get<PlankPaintable>();

            plank.paintable = paintable;
            plank.renderersColliders = renderers.ToDictionary(x=>x,y=>y.GetComponent<Collider>());

            foreach (var i in plank.renderersColliders.Values)
            {
                i.enabled = false;
            }

            plank.uvAreas = new Dictionary<Renderer, float>();

            for (int i = 0; i < renderers.Count; i++)
            {
                plank.uvAreas.Add(renderers[i], uvAreas[i]);
            }

            plank.mid = mid;

            plank.paintable.enabled = false;
            gameObject.SetActive(false);
        }

#if UNITY_EDITOR
        [ContextMenu("Calculate UV Areas")]
        public void CalculateUVAreas()
        {
            uvAreas = new List<float>(renderers.Count);

            foreach (var rend in renderers)
            {
                uvAreas.Add(rend.gameObject.GetComponent<MeshFilter>().sharedMesh.GetAreaOnUV());
            }

            EditorUtility.SetDirty(gameObject);
        }
#endif
    }
}
