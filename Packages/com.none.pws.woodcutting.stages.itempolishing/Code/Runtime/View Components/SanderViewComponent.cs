﻿using System;
using Leopotam.Ecs;
using Modules.ViewHub;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace PWS.WoodCutting
{
    public class SanderViewComponent : ViewComponent
    {
        private EcsEntity _entity;

        [SerializeField] private XRGrabInteractable _interactable;
        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            ecsEntity.Get<SanderTag>();
            _entity = ecsEntity;
            
            _interactable.activated.AddListener(SetStartSignal);
            _interactable.deactivated.AddListener(SetStopSignal);
        }

        private void SetStartSignal(ActivateEventArgs arg)
        {
                _entity.Get<SanderCleaningStartSignal>();
        }

        private void SetStopSignal(DeactivateEventArgs arg)
        {
                _entity.Get<SanderCleaningStopSignal>();
        }
    }
}