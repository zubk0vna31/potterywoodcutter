using Leopotam.Ecs;
using Modules.Root.ContainerComponentModel;
using Modules.Root.ECS;
using PWS.Common.Audio;
using PWS.Common.GameModeInfoService;
using PWS.Common.ResultsEvaluation;
using PWS.Common.UI;
using PWS.WoodCutting.Common;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;
using PWS.Common.TravelPoints;


namespace PWS.WoodCutting.Stages.ItemPolishing
{
    public enum ItemPolishingSubstates
    {
        None = 0,
        Transition,
        Top,
        Bot
    }

    internal class StateDependencies
    {
        [Inject] public IGameModeInfoService gameModeInfoService;
        [Inject] public IResultsEvaluationDataHolder resultsEvaluation;
        [Inject] public IItemPolishingDataHolder itemPolishingDataHolder;
    }

    public class WoodCuttingItemPolishingSystemsProvider : MonoBehaviour, ISystemsProvider
    {
        [Header("Core")]
        [SerializeField]
        private StateConfig _stateConfig;
        [SerializeField]
        private StateInfoData _stateInfo;

        [Header("Scene Related")]
        [SerializeField]
        private Transform _orientation;
        [SerializeField]
        private Transform _stage;
        [SerializeField]
        private Renderer _midRenderer;

        [Header("Voice config")]
        [SerializeField] private VoiceConfig _voiceConfig;
        [SerializeField] private XRBaseInteractable _sander;

        public EcsSystems GetSystems(EcsWorld world, EcsSystems endFrame, EcsSystems mainSystems)
        {
            StateDependencies dependecies = new StateDependencies();
            SceneContainer.Instance.Inject(dependecies);


            EcsSystems systems = new EcsSystems(world, "WoodCutting Item Polishing");

            systems

                .Add(new SetStateDelayedSystem<ItemPolishingState>())
                .Add(new SubstateSystem<ItemPolishingState, ItemPolishingSubstates, ItemPolisingStateConfig>
                (_stateConfig, ItemPolishingSubstates.Top, ItemPolishingSubstates.Transition, ItemPolishingSubstates.None))

                .Add(new OutlineByStateSystem<ItemPolishingState>(_stateConfig))


                .Add(new ItemPolishingStateEnterSystem<ItemPolishingState>())

                .Add(new ItemPolisingTopSubstateSystem(_stateConfig, ItemPolishingSubstates.Top,
                dependecies.itemPolishingDataHolder,dependecies.gameModeInfoService, _stage, _orientation))
                .Add(new ItemPolisingBotSubstateSystem(_stateConfig, ItemPolishingSubstates.Bot,
                dependecies.itemPolishingDataHolder,dependecies.gameModeInfoService, _stage, _midRenderer))

                .Add(new ItemPolishingPaintableUpdateSystem(_stateConfig))

                // Try replace this (below) with this (above)
                //.Add(new ItemPolishingHandleSystem<ItemPolishingState>(_stateConfig, _orientation, _stage))

                //Tracker
                .Add(new ItemPolishingProcessTracker<ItemPolishingState>())
                .Add(new StateInfoWindowUIProcessing<ItemPolishingState>(_stateInfo))

                 .Add(new WoodCuttingSetGradeByProgress<ItemPolishingState>(dependecies.resultsEvaluation,
                _stateInfo))

                .Add(new StateWindowPlacementProcessing<ItemPolishingState>("Main"))

                .Add(new TravelPointPositioningSystem<ItemPolishingState>("Default"))


                .Add(new SubstateButtonUIProcessing<ItemPolishingState,ItemPolishingSubstates>(ItemPolishingSubstates.Top))
                .Add(new SubstateButtonUIProcessing<ItemPolishingState,ItemPolishingSubstates>(ItemPolishingSubstates.Bot))

                .Add(new StateWindowProgressUIProcessing<ItemPolishingState>())

                //Voice
                .Add(new VoiceInteractableAudioSystem<ItemPolishingState>(_voiceConfig, _sander))
                
                //Sounds
                .Add(new StickableBasedSoundSystem<ItemPolishingState>())

                ;

            return systems;
        }
    }
}
