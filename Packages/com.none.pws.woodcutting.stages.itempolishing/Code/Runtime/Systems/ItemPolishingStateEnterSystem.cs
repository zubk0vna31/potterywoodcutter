using Leopotam.Ecs;
using Modules.StateGroup.Components;
using Modules.ViewHub;
using UnityEngine;

namespace PWS.WoodCutting
{
    public class ItemPolishingStateEnterSystem<T> : IEcsRunSystem where T : struct
    {
        private readonly EcsFilter<T> _state;
        private readonly EcsFilter<StateEnter,T> _stateEnter;

        private readonly EcsFilter<PlankPaintable> _planks;

        public void Run()
        {
            if (_state.IsEmpty()) return;

            if (!_stateEnter.IsEmpty())
            {
                StateEnter();
            }
        }

        private void StateEnter()
        {
            foreach (var i in _planks)
            {
                _planks.Get1(i).paintable.SetNewMaxPercentage(2f);
            }
        }
    }
}
