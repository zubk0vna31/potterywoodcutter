using DG.Tweening;
using Leopotam.Ecs;
using Modules.ViewHub;
using PWS.Common;
using PWS.Common.GameModeInfoService;
using PWS.Common.UI;
using PWS.WoodCutting.Common;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace PWS.WoodCutting.Stages.ItemPolishing
{
    public class ItemPolisingBotSubstateSystem : RunSubstateSystem<ItemPolishingState,
        ItemPolishingSubstates, ItemPolisingStateConfig>
    {
        private readonly IItemPolishingDataHolder _dataHolder;
        private readonly Transform _stateTransform;

        private readonly EcsFilter<StateWindow, StateProgress> _stateWindowWithProgress;

        private readonly EcsFilter<PlankPaintable, UnityView> _planks;
        private readonly EcsFilter<Table, UnityView> _table;

        private readonly EcsFilter<NextStateSignal> _nextStateSignal;

        private readonly IGameModeInfoService _gameModeInfoService;
        private readonly Renderer _midRenderer;



        private int _order;

        public ItemPolisingBotSubstateSystem(StateConfig stateConfig, ItemPolishingSubstates currentSubstate,
            IItemPolishingDataHolder dataHolder, IGameModeInfoService gameModeInfoService,
            Transform stateTransform,Renderer midRenderer) : base(stateConfig, currentSubstate)
        {
            _dataHolder = dataHolder;
            _gameModeInfoService = gameModeInfoService;
            _stateTransform = stateTransform;
            _midRenderer = midRenderer;

            _order = 1;
        }

        protected override void OnSubstateEnter()
        {
            base.OnSubstateEnter();
            ChangeTarget();
        }

        protected override void OnSubstateUpdate()
        {
            base.OnSubstateUpdate();

            if (!_nextStateSignal.IsEmpty())
            {
                _dataHolder.GetStickyObject(_order).Toggle(false);
                _stateTransform.gameObject.SetActive(false);

                // Grade 
                float grade = 0f;
                foreach (var i in _stateWindowWithProgress)
                {
                    grade = _stateWindowWithProgress.Get2(i).ProgressValue;
                }
                _ecsWorld.NewEntity().Get<SetGradeSignal>().grade = grade;

                FullPaint();
            }
        }

        private void ChangeTarget()
        {
            foreach (var i in _planks)
            {
                ref var paint = ref _planks.Get1(i);
                var last = paint.Last;

                paint.paintable.BackupCurrentPercentage();
                paint.renderersColliders[last].enabled = true;
                paint.paintable.SetRenderTarget(last, paint.uvAreas[last]);
            }

            foreach (var i in _stateWindowWithProgress)
            {
                _stateWindowWithProgress.Get1(i).Template.ResetButtonTitle();
                _stateWindowWithProgress.Get2(i).SkipThreshold = 
                    _gameModeInfoService.CurrentMode.Equals(GameMode.Free) ? 0.0f :
                    _stateWindowWithProgress.Get2(i).Value+_config.skipThreshold * 0.5f;
            }

            _dataHolder.GetStickyObject(_order).Toggle(true);
        }

        private void FullPaint()
        {
            List<PlankPaintable> planks = new List<PlankPaintable>();

            foreach (var i in _planks)
            {
                planks.Add(_planks.Get1(i));

                var renderers =
                    _planks.Get1(i).renderersColliders.Keys.ToList();
                renderers.Add(_midRenderer);

                _planks.Get1(i).paintable.ColorAllWithDuration(renderers.ToArray(), _config.skipDuration,null,false,false);
                _planks.Get1(i).renderersColliders[_planks.Get1(i).Last].enabled = false;
                
            }

            foreach (var i in _stateWindowWithProgress)
            {
                _stateWindowWithProgress.Get1(i).Template.HideButtons();
                _stateWindowWithProgress.GetEntity(i).Get<Ignore>();
            }

            ChangeState(_config.skipDuration);
        }

        protected override void OnStateExit()
        {
            base.OnStateExit();

            foreach (var i in _planks)
            {

                _planks.Get1(i).paintable.Dissable();
            }
        }
    }
}
