using DG.Tweening;
using Leopotam.Ecs;
using Modules.ViewHub;
using PWS.Common.GameModeInfoService;
using PWS.Common.UI;
using PWS.WoodCutting.Common;
using System.Linq;
using UnityEngine;

namespace PWS.WoodCutting.Stages.ItemPolishing
{
    public class ItemPolisingTopSubstateSystem : RunSubstateSystem<ItemPolishingState, ItemPolishingSubstates, ItemPolisingStateConfig>
    {
        private readonly IItemPolishingDataHolder _dataHolder;
        private readonly Transform _stateTransform, _orientation;

        private readonly EcsFilter<StateWindow, StateProgress> _stateWindowWithProgress;

        private readonly EcsFilter<PlankPaintable, UnityView> _planks;
        private readonly EcsFilter<Table, UnityView> _table;

        private readonly EcsFilter<NextStateSignal> _nextStateSignal;

        private readonly IGameModeInfoService _gameModeInfoService;


        private int _order;

        public ItemPolisingTopSubstateSystem(StateConfig stateConfig, ItemPolishingSubstates currentSubstate,
            IItemPolishingDataHolder dataHolder, IGameModeInfoService gameModeInfoService, Transform stateTransform,
            Transform orientation) : base(stateConfig, currentSubstate)
        {
            _dataHolder = dataHolder;

            _gameModeInfoService = gameModeInfoService;

            _stateTransform = stateTransform;
            _orientation = orientation;

            _order = 0;
        }

        protected override void OnSubstateEnter()
        {
            base.OnSubstateEnter();

            // Setup data
            var stickyObjects = _stateTransform.GetComponentsInChildren<StickyObject>().OrderBy(x =>
            Vector3.Dot(_stateTransform.up, x.transform.up)).ToArray();

            for (int i = 0; i < stickyObjects.Length; i++)
            {
                _dataHolder.AddStickyObject(i, stickyObjects[i]);
            }

            // Change state window and progress values
            foreach (var i in _stateWindowWithProgress)
            {
                _stateWindowWithProgress.Get1(i).Template.ChangeButtonTitle(_config.buttonSubstageTitleTerm);
                _stateWindowWithProgress.Get2(i).SkipThreshold =
                    _gameModeInfoService.CurrentMode.Equals(GameMode.Free) ? 0.0f :
                    _config.skipThreshold * 0.5f;
            }


            // Don't remember that all this do...
            Vector3 tableUp = Vector3.zero;

            foreach (var i in _table)
            {
                tableUp = _table.Get2(i).Transform.up;
                break;
            }

            // Sort planks. And this also...
            foreach (var i in _planks)
            {
                ref var paint = ref _planks.Get1(i);

                paint.paintable.enabled = true;
                paint.renderersColliders = paint.renderersColliders.OrderByDescending(
                    x => Vector3.Dot(tableUp, x.Key.transform.parent.up)).ToDictionary(x => x.Key, y => y.Value);
            }

            _stateTransform.gameObject.SetActive(true);

            // Activate painting
            foreach (var i in _planks)
            {
                ref var paint = ref _planks.Get1(i);
                var last = paint.Last;

                paint.renderersColliders[last].enabled = true;
                paint.paintable.InitializeCustomRenderer(last, paint.uvAreas[last], _config.skipThreshold);
            }

            _dataHolder.GetStickyObject(_order).Toggle(true);
        }

        protected override void OnSubstateUpdate()
        {
            base.OnSubstateUpdate();

            if (!_nextStateSignal.IsEmpty())
            {
                _dataHolder.GetStickyObject(_order).Toggle(false);

                // Complete side
                foreach (var i in _planks)
                {
                    _planks.Get1(i).completedAmount++;
                }

                // Rotate shield
                Transform plankParent = null;
                foreach (var i in _table)
                {
                    plankParent = _table.Get1(i).planksParent;
                }

                plankParent.DORotateQuaternion(Quaternion.AngleAxis(
                    180f, _orientation.right) * plankParent.rotation, _config.rotationDuration);

                // Change substate
                ChangeSubstate(ItemPolishingSubstates.Bot, _config.rotationDuration);
            }
        }
    }
}
