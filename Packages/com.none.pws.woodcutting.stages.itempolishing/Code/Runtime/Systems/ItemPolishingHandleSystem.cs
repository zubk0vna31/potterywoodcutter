using DG.Tweening;
using Leopotam.Ecs;
using Modules.StateGroup.Components;
using Modules.StateGroup.Core;
using Modules.ViewHub;
using PWS.Common;
using PWS.Common.UI;
using PWS.WoodCutting.Common;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace PWS.WoodCutting
{
    public class ItemPolishingHandleSystem<T> : IEcsRunSystem where T : struct
    {
        private readonly EcsFilter<T> _state;
        private readonly EcsFilter<StateEnter, T> _stateEnter;

        private readonly EcsFilter<PlankPaintable, UnityView> _planks;
        private readonly EcsFilter<Table, UnityView> _table;

        private readonly EcsFilter<StateWindow, StateProgress> _stateWindowWithProgress;
        private readonly EcsFilter<NextStateSignal> _nextStateSignal;

        private readonly Modules.Utils.TimeService _timeService;
        private readonly StateFactory _stateFactory;
        private readonly EcsWorld _ecsWorld;

        private readonly ItemPolisingStateConfig _stateConfig;
        private readonly Transform _orientation, _stageTransform;

        private StickyObject [] _stickyObjects;

        private int _maxIterations;
        private int _currentItteration;

        private bool _ignore;
        private bool _initialized;

        private float _timer;

        private bool Active => _currentItteration < _maxIterations;

        public ItemPolishingHandleSystem(StateConfig stateConfig, Transform orientation, Transform stage)
        {
            _stateConfig = stateConfig as ItemPolisingStateConfig;
            _orientation = orientation;
            _stageTransform = stage;
            _stickyObjects = _stageTransform.GetComponentsInChildren<StickyObject>().OrderBy(x=>
            Vector3.Dot(_stageTransform.up,x.transform.up)).ToArray();
        }

        public void Run()
        {
            if (_state.IsEmpty()) return;

            if (!_stateEnter.IsEmpty())
            {
                StateEnter();
            }

            HandleTimer();

            if (!_nextStateSignal.IsEmpty())
            {
                Next();
            }
        }

        private void HandleTimer()
        {
            if (!Active || _ignore) return;

            if (_timer > 0)
            {
                _timer -= _timeService.DeltaTime;

                if (_timer <= 0f)
                {
                    _timer = _stateConfig.checkInterval;

                    if (!IsPainted())
                    {
                        return;
                    }
                    else
                    {
                        ToggleButtonVI(true, true);
                    }
                }
            }
        }

        private bool IsPainted(bool updateItteration = false)
        {
            float p = 0;
            foreach (var i in _planks)
            {
                ref var plank = ref _planks.Get1(i);
                plank.paintable.CalculatePercentage();
                //plank.percentages[plank.completedAmount] = plank.paintable.PaintedPercentage;

                //p += plank.percentages.Aggregate((x, y) => x + y);
            }

            p /= (_planks.GetEntitiesCount() * 2);


            float refPercentage = _stateConfig.skipThreshold /
                (_maxIterations - _currentItteration);

            if (p >= refPercentage)
            {
                if (updateItteration) _currentItteration++;
                return true;
            }
            else return false;
        }

        private void StateEnter()
        {
            _initialized = false;
            _ignore = false;
            _currentItteration = 0;
            _timer = _stateConfig.checkInterval;

            foreach (var i in _stateWindowWithProgress)
            {
                _stateWindowWithProgress.Get1(i).Template.ChangeButtonTitle(_stateConfig.buttonSubstageTitleTerm);
                _stateWindowWithProgress.Get1(i).Template.ShowButton(false);
                break;
            }

            Vector3 tableUp = Vector3.zero;

            foreach (var i in _table)
            {
                tableUp = _table.Get2(i).Transform.up;
                break;
            }

            //Sort planks
            foreach (var i in _planks)
            {
                ref var paint = ref _planks.Get1(i);

                paint.paintable.enabled = true;

                paint.renderersColliders = paint.renderersColliders.OrderByDescending(
                    x => Vector3.Dot(tableUp, x.Key.transform.parent.up)).ToDictionary(x => x.Key, y => y.Value);

                if (_maxIterations <= 0)
                {
                    _maxIterations = paint.renderersColliders.Count;
                }
            }

            _stageTransform.gameObject.SetActive(true);

            for (int i = 0; i < _stickyObjects.Length; i++)
            {
                _stickyObjects[i].Toggle(false);
            }

            Next();
        }

        private void Next()
        {
            ToggleButtonVI(false, true);

            if (!_initialized)
            {
                _initialized = true;
                foreach (var i in _planks)
                {
                    _planks.Get1(i).renderersColliders[_planks.Get1(i).Last].enabled = true;
                    _planks.Get1(i).paintable.InitializeCustomRenderer(_planks.Get1(i).Last,
                        _planks.Get1(i).uvAreas[_planks.Get1(i).Last],
                        _stateConfig.skipThreshold);
                }

                _currentItteration = 0;

                _stickyObjects[_currentItteration].Toggle(true);
            }

            if (_currentItteration >= _maxIterations)
            {
                for (int i = 0; i < _stickyObjects.Length; i++)
                {
                    _stickyObjects[i].Deactivate();
                }

                _stageTransform.gameObject.SetActive(false);

                _stateConfig.nextState.Execute(_stateFactory);
                return;
            }

            if (IsPainted(true))
            {
                if (!Active)
                {
                    // Grade 
                    float grade = 0f;
                    foreach (var i in _stateWindowWithProgress)
                    {
                        grade = _stateWindowWithProgress.Get2(i).ProgressValue;
                    }
                    _ecsWorld.NewEntity().Get<SetGradeSignal>().grade = grade;

                    FullPaint();
                    return;
                }

                foreach (var i in _planks)
                {
                    _planks.Get1(i).completedAmount++;
                }

                Transform plankParent = null;

                foreach (var i in _table)
                {
                    plankParent = _table.Get1(i).planksParent;
                    break;
                }

                _ignore = true;

                plankParent.DORotateQuaternion(Quaternion.AngleAxis(
                    180f, _orientation.right) * plankParent.rotation, _stateConfig.rotationDuration)
                    .OnComplete(() =>
                    {
                        _ignore = false;
                        ChangeTarget();
                    });
            }
        }

        private void ToggleButtonVI(bool visibility, bool interaction)
        {
            foreach (var j in _stateWindowWithProgress)
            {
                _stateWindowWithProgress.Get1(j).Template.ShowButton(visibility);
                _stateWindowWithProgress.Get1(j).Template.ToggleButtonInteraction(interaction);
            }
        }

        private void FullPaint()
        {
            bool callbackAdded = false;

            foreach (var i in _planks)
            {
                //_planks.Get1(i).PaintAll();

                if (!callbackAdded)
                {
                    callbackAdded = true;
                    _planks.Get1(i).paintable.ColorAllWithDuration(
                        _planks.Get1(i).renderersColliders.Keys.ToArray(),1f, () =>
                   {
                       Next();
                   });
                }
                else
                {
                    _planks.Get1(i).paintable.ColorAllWithDuration(
                        _planks.Get1(i).renderersColliders.Keys.ToArray());
                }

                _planks.Get1(i).renderersColliders[_planks.Get1(i).Last].enabled = false;
            }
        }

        private void ChangeTarget()
        {
            foreach (var i in _planks)
            {
                _planks.Get1(i).renderersColliders[_planks.Get1(i).Last].enabled = true;
                _planks.Get1(i).paintable.SetRenderTarget(_planks.Get1(i).Last,_planks.Get1(i).uvAreas[_planks.Get1(i).Last]);
            }



            foreach (var i in _stateWindowWithProgress)
            {
                _stateWindowWithProgress.Get1(i).Template.ResetButtonTitle();
            }

            if (_timer <= 0)
            {
                _timer = _stateConfig.checkInterval;
            }

            _stickyObjects[_currentItteration-1].Toggle(false);
            _stickyObjects[_currentItteration].Toggle(true);
        }
    }
}
