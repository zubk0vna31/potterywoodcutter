using Leopotam.Ecs;
using Modules.StateGroup.Components;
using PWS.Common.UI;
using System.Linq;

namespace PWS.WoodCutting
{ 
    public class ItemPolishingProcessTracker<T> : IEcsRunSystem where T : struct
    {
        private readonly EcsFilter<T> _state;
        private readonly EcsFilter<StateEnter, T> _stateEnter;

        private readonly EcsFilter<StateWindow, StateProgress> _stateWindowWithProgress;
        private readonly EcsFilter<PlankPaintable> _planks;


        public void Run()
        {
            if (_state.IsEmpty()) return;

            StateEnter();
            State();
        }

        private void StateEnter()
        {
            if (_stateEnter.IsEmpty()) return;

            foreach (var i in _stateWindowWithProgress)
            {
                ref var progress = ref _stateWindowWithProgress.Get2(i);

                progress.Value = 0;
                progress.CompletionThreshold = 2f;
            }
        }

        private void State()
        {
            foreach (var i in _stateWindowWithProgress)
            {
                float value = 0;
                foreach (var j in _planks)
                {
                    value += _planks.Get1(j).paintable.PaintedPercentage;
                }

                value /= (2*_planks.GetEntitiesCount());

                _stateWindowWithProgress.Get2(i).Value = value;
            }
        }

    }
}
