using Leopotam.Ecs;
using Modules.Utils;
using PWS.WoodCutting.Common;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PWS.WoodCutting.Stages.ItemPolishing
{
    public class ItemPolishingPaintableUpdateSystem : RunSystem<ItemPolishingState,ItemPolisingStateConfig>
    {
        private readonly EcsFilter<PlankPaintable> _planks;

        private readonly TimeService _timeService;

        private float _timer;

        public ItemPolishingPaintableUpdateSystem(StateConfig config) : base(config)
        {
        }

        protected override void OnStateEnter()
        {
            base.OnStateEnter();
            _timer = _config.checkInterval;
        }

        protected override void OnStateUpdate()
        {
            base.OnStateUpdate();

            if (_timer > 0)
            {
                _timer -= _timeService.DeltaTime;

                if (_timer <= 0)
                {
                    UpdatePaintable();
                }
            }
            else
            {
                UpdatePaintable();
            }
        }

        private void UpdatePaintable()
        {
            foreach (var i in _planks)
            {
                _planks.Get1(i).paintable.CalculatePercentage();
            }

            _timer = _config.checkInterval;
        }
    }
}
