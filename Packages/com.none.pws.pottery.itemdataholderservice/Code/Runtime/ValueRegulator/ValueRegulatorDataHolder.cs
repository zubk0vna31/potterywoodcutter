﻿using PWS.Common.ItemDataHolder;
using UnityEngine;

namespace PWS.Pottery.ItemDataHolderService
{
    public class ValueRegulatorDataHolder<MachineT> : IValueRegulatorDataHolder<MachineT>
    {
        private float _value;
        public float Value {
            get => _value;
            set => _value = value;
        }
        private bool _isUpdated;
        public bool IsUpdated
        {
            get => _isUpdated;
            set => _isUpdated = value;
        }
        private float[] _keyCustomValues;
        public float[] KeyCustomValues {
            set => _keyCustomValues = value;
        }

        public float ConvertToCustom()
        {
            float result = 0;

            float offset = 0;
            for (int i = 0; i < _keyCustomValues.Length - 1; i++)
            {
                if(_value > offset && _value <= offset + 0.25f)
                {
                    float localValue = (_value - offset) / 0.25f;

                    result = Mathf.Lerp(_keyCustomValues[i], _keyCustomValues[i + 1], localValue);

                    break;
                }

                offset += 0.25f;
            }

            return result;
        }
        public float ConvertFromCustom(float customValue)
        {
            float result = 0;

            float offset = 0;
            for (int i = 0; i < _keyCustomValues.Length - 1; i++)
            {
                float sectorRange = _keyCustomValues[i + 1] - _keyCustomValues[i];
                if (customValue > offset && customValue <= offset + sectorRange)
                {
                    float localValue = (customValue - offset) / sectorRange;

                    result = Mathf.Lerp(i * 0.25f, (i + 1) * 0.25f, localValue);

                    break;
                }

                offset += sectorRange;
            }

            return result;
        }

        public void CopyFrom(IItemDataHolder itemData)
        {
            ValueRegulatorDataHolder<MachineT> data = (ValueRegulatorDataHolder<MachineT>)itemData;
            _value = data.Value;
        }

        public void SetDirty()
        {
            IsUpdated = true;
        }

    }
}