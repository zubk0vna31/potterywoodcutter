﻿using PWS.Common.ItemDataHolder;

namespace PWS.Pottery.ItemDataHolderService
{
    public interface IValueRegulatorDataHolder<MachineT> : IItemDataHolder
    {
        float Value { get; set; }
        float[] KeyCustomValues { set; }
        bool IsUpdated { get; set; }
        float ConvertToCustom();
        float ConvertFromCustom(float customValue);
    }
}