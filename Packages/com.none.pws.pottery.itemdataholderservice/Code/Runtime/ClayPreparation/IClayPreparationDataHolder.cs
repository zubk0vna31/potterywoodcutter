
using PWS.Common.ItemDataHolder;

namespace PWS.Pottery.ItemDataHolderService
{
    public interface IClayPreparationDataHolder : IItemDataHolder
    {
        float Crumple { get; set; }
        float Saturation { get; set; }

        bool IsUpdated { get; set; }
    }
}