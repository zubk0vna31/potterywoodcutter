using System.Collections;
using System.Collections.Generic;
using PWS.Common.ItemDataHolder;
using UnityEngine;

namespace PWS.Pottery.ItemDataHolderService
{
    public class ClayPreparationDataHolder : IClayPreparationDataHolder
    {
        private float _crumple;
        public float Crumple {
            get => _crumple;
            set => _crumple = value;
        }

        private float _saturation;
        public float Saturation {
            get => _saturation;
            set => _saturation = value;
        }

        private bool _isUpdated;
        public bool IsUpdated
        {
            get => _isUpdated;
            set => _isUpdated = value;
        }

        public void CopyFrom(IItemDataHolder itemData)
        {
            ClayPreparationDataHolder data = (ClayPreparationDataHolder)itemData;
            _crumple = data.Crumple;
            _saturation = data.Saturation;

        }

        public void SetDirty()
        {
            _isUpdated = true;
        }
    }
}