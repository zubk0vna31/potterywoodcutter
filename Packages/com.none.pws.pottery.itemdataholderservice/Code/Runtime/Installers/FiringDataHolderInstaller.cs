using Modules.Root.ContainerComponentModel;

namespace PWS.Pottery.ItemDataHolderService
{
    // mono installer to allow runtime modification and observation of serialized data
    public class FiringDataHolderInstaller : AMonoInstaller
    {
        public override void Install(IContainer container)
        {
            IValueRegulatorDataHolder<HoursRegulator> hoursDataHolder = new ValueRegulatorDataHolder<HoursRegulator>();
            IValueRegulatorDataHolder<TemperatureRegulator> temperatureDataHolder = new ValueRegulatorDataHolder<TemperatureRegulator>();

            container.Bind(hoursDataHolder);
            container.Bind(temperatureDataHolder);

        }
    }
}