using Modules.Root.ContainerComponentModel;

namespace PWS.Pottery.ItemDataHolderService
{
    // mono installer to allow runtime modification and observation of serialized data
    public class WheelSpeedDataHolderInstaller : AMonoInstaller
    {
        public override void Install(IContainer container)
        {
            IValueRegulatorDataHolder<WheelSpeedRegulator> itemDataHolder = new ValueRegulatorDataHolder<WheelSpeedRegulator>();

            container.Bind(itemDataHolder);

        }
    }
}