using Modules.Root.ContainerComponentModel;

namespace PWS.Pottery.ItemDataHolderService
{
    // mono installer to allow runtime modification and observation of serialized data
    public class DryingDataHolderInstaller : AMonoInstaller
    {
        public override void Install(IContainer container)
        {
            IValueRegulatorDataHolder<DryingRegulator> itemDataHolder = new ValueRegulatorDataHolder<DryingRegulator>();

            container.Bind(itemDataHolder);

        }
    }
}