using Modules.Root.ContainerComponentModel;

namespace PWS.Pottery.ItemDataHolderService
{
    // mono installer to allow runtime modification and observation of serialized data
    public class ClayPreparationDataHolderInstaller : AMonoInstaller
    {
        public override void Install(IContainer container)
        {
            IClayPreparationDataHolder itemDataHolder = new ClayPreparationDataHolder();

            container.Bind(itemDataHolder);

        }
    }
}