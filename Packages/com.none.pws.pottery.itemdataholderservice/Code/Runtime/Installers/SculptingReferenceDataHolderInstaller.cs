using UnityEngine;
using Modules.Root.ContainerComponentModel;

namespace PWS.Pottery.ItemDataHolderService
{
    // mono installer to allow runtime modification and observation of serialized data
    public class SculptingReferenceDataHolderInstaller : AMonoInstaller
    {
        [Tooltip("Copies data from template if exists")]
        [SerializeField] private SculptingTemplateData _itemDataTemplate;

        [SerializeField]
        private SmoothParamaters _smoothParamaters;

        public override void Install(IContainer container)
        {
            ISculptingDataHolder itemDataHolder = new SculptingDataHolder(_smoothParamaters);
            if (_itemDataTemplate)
                itemDataHolder.CopyFrom(_itemDataTemplate.Data);

            container.Bind(new SculptingReferenceDataHolder(itemDataHolder));

        }
    }
}