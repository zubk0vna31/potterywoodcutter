using UnityEngine;
using Modules.Root.ContainerComponentModel;

namespace PWS.Pottery.ItemDataHolderService
{
    // mono installer to allow runtime modification and observation of serialized data
    public class SculptingDataHolderInstaller : AMonoInstaller
    {
        [Tooltip("Copies data from template if exists")]
        [SerializeField] private SculptingTemplateData _initDataTemplate;

        [SerializeField]
        private DefaultParameters _defaultParameters;
        [SerializeField]
        private SmoothParamaters _smoothParamaters;

        public override void Install(IContainer container)
        {
            ISculptingDataHolder itemDataHolder = new SculptingDataHolder(_defaultParameters, _smoothParamaters);
            if (_initDataTemplate)
                itemDataHolder.CopyFrom(_initDataTemplate.Data);
            itemDataHolder.SetDirty();

            container.Bind(itemDataHolder);

        }
    }
}