using UnityEngine;

namespace PWS.Pottery.ItemDataHolderService
{
    public enum RingType
    {
        Default,
        OpeningPoint,
        ClosingPoint
    }

    public class MeshRing
    {
        public Vector2 Position;

        public RingType Type;

        public float UvClosureOffset;
        public Color Color;

        public bool TopWallRing;

        public void CreateRing(Vector2 position, RingType type, Color color)
        {
            Position = position;
            Type = type;
            Color = color;
            if (type != RingType.Default)
            {
                UvClosureOffset = position.x;
                Position.x = 0;
            }
        }
    }
}