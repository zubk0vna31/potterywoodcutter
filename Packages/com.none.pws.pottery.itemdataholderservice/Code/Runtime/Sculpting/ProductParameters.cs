using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PWS.Pottery.ItemDataHolderService
{
    [System.Serializable]
    public class DefaultParameters
    {
        public float Height = 0.2f;
        public float Radius = 0.1f;
        public float WallWidthMax = 0.03f;
        public float WallWidthMin = 0.01f;
        public float RingsOffset = 0.01f;

        public DefaultParameters(DefaultParameters defaultParameters)
        {
            Height = defaultParameters.Height;
            Radius = defaultParameters.Radius;
            WallWidthMin = defaultParameters.WallWidthMin;
            WallWidthMax = defaultParameters.WallWidthMax;
            RingsOffset = defaultParameters.RingsOffset;
        }
    }

    [System.Serializable]
    public class GeneralParameters
    {
        public float FullHeight;
        public float BottomClosureHeight;
        public float TopClosureHeight;

        public GeneralParameters(GeneralParameters generalParameters)
        {
            FullHeight = generalParameters.FullHeight;
            BottomClosureHeight = generalParameters.BottomClosureHeight;
            TopClosureHeight = generalParameters.TopClosureHeight;
        }

        public GeneralParameters(float productHeight)
        {
            FullHeight = productHeight;
            BottomClosureHeight = 0;
            TopClosureHeight = productHeight;
        }
    }

    [System.Serializable]
    public class SmoothParamaters
    {
        [Range(3, 64)]
        public int Vertices = 32;
        [Range(0, 180)]
        public float AngleThreshold = 140;
        [Range(0, 10)]
        public int Segments = 3;
        [Range(0, 1f)]
        public float LerpValue = 0.5f;
        public float MaxOffset = 0.1f;

        public SmoothParamaters(SmoothParamaters smoothParamaters)
        {
            Vertices = smoothParamaters.Vertices;
            AngleThreshold = smoothParamaters.AngleThreshold;
            Segments = smoothParamaters.Segments;
            LerpValue = smoothParamaters.LerpValue;
            MaxOffset = smoothParamaters.MaxOffset;
        }
    }
}