using PWS.Common.ItemDataHolder;
using System.Collections.Generic;

namespace PWS.Pottery.ItemDataHolderService
{
    public interface ISculptingDataHolder : IItemDataHolder
    {
        DefaultParameters DefaultParameters { get; }
        GeneralParameters GeneralParameters { get; }
        SmoothParamaters SmoothParamaters { get; }

        List<MeshRing> MeshRings { get; }
        List<Ring> Rings { get; }
        Ring GetRing(int i);
        int GetRingsCount();

        Ring TopRing { get; }
        Ring BottomRing { get; }

        bool IsDataUpdated { get; set; }
        bool IsRingsUpdated { get; set; }
    }
}