using UnityEngine;

namespace PWS.Pottery.ItemDataHolderService
{
    [CreateAssetMenu(fileName = "SculptingTemplateData", menuName = "PWS/Pottery/SculptingTemplateData", order = 1)]
    public class SculptingTemplateData : ScriptableObject
    {
        public SculptingDataHolder Data;
    }
}