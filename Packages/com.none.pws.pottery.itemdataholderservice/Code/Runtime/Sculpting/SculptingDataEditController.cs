using UnityEngine;

namespace PWS.Pottery.ItemDataHolderService
{
    public class SculptingDataEditController : MonoBehaviour
    {
        /*[Tooltip("Copies data from template if exists")]
        public SculptingTemplateData InitialTemplate;
        
        public readonly float DefaulRingsOffset = 0.02f;

        [HideInInspector]
        public IItemDataHolder Data;

        [HideInInspector]
        public bool IsUpdated;

        public void Start()
        {
            CreateRings();
        }

        public void SetTopClosureHeight(float height)
        {
            Data.TopClosureHeight = Mathf.Max(Data.BottomClosureHeight + Data.BottomRing.Width, height);
            CreateRings();
        }

        public void SetProductHeight(float height)
        {
            Data.ProductHeight = Mathf.Max(DefaulRingsOffset, height);
            CreateRings();
        }

        public void SetRingOffset(Ring ring, float offset)
        {
            ring.SetOffset(Mathf.Max(ring.Width, offset));
            CreateRings();
        }

        void CreateRings()
        {
            float nextRingHeight = 0;
            int i = 0;

            Vector2 removedTopRingPosition = Data.TopRing.Position;

            while (nextRingHeight < Data.ProductHeight || i < Data.Rings.Count)
            {
                if (i >= Data.Rings.Count)
                {
                    int prevId = Data.Rings.Count - 1;
                    Data.Rings[prevId].SetHeight(nextRingHeight - DefaulRingsOffset);

                    Data.Rings.Add(new Ring(new Vector2(Data.Rings[prevId].Position.x, nextRingHeight), Data.Rings[prevId].Width));
                }
                else
                {
                    if (Data.Rings[i].Position.y > Data.ProductHeight && Data.Rings.Count > 2)
                    {
                        if (i == Data.Rings.Count - 1)
                            removedTopRingPosition = Data.TopRing.Position;

                        Data.Rings.RemoveAt(i);
                        nextRingHeight -= DefaulRingsOffset;
                        i--;
                    }
                }
                nextRingHeight += DefaulRingsOffset;
                i++;
            }

            if (removedTopRingPosition.y <= Data.TopRing.Position.y)
            {
                Data.TopRing.SetHeight(Data.ProductHeight);
                Data.TopClosureHeight = Mathf.Min(Data.TopClosureHeight, Data.ProductHeight);
            }
            else
            {
                Vector2 vectorToRemovedTopRing = removedTopRingPosition - Data.TopRing.Position;
                float lerp = (Data.ProductHeight - Data.TopRing.Position.y) / vectorToRemovedTopRing.y;
                Vector2 lerpPosition = Vector2.Lerp(Data.TopRing.Position, removedTopRingPosition, lerp);
                Data.TopRing.Position = lerpPosition;
                Data.TopClosureHeight = Mathf.Min(Data.TopClosureHeight, lerpPosition.y);
            }


            IsUpdated = true;
        }*/
    }
}
