using UnityEngine;

namespace PWS.Pottery.ItemDataHolderService
{
    [System.Serializable]
    public class Ring
    {
        public Vector2 Position;
        public float Width;
        public Color Color;
        public bool TopWallRing;

        public Ring(Vector2 position, float width)
        {
            Position = position;
            Width = width;
            Color = Color.white;
        }

        public Ring(Vector2 position, float width, Color color)
        {
            Position = position;
            Width = width;
            Color = color;
        }

        public void SetData(Vector2 offset, float width, Color color)
        {
            Position = offset;
            Width = width;
            Color = color;
        }

        public void SetHeight(float height)
        {
            Position.y = height;
        }
        public void SetOffset(float offset)
        {
            Position.x = offset;
        }
    }
}