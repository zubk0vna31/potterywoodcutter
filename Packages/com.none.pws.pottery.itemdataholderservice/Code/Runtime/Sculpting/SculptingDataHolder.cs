using PWS.Common.ItemDataHolder;
using System.Collections.Generic;
using UnityEngine;

namespace PWS.Pottery.ItemDataHolderService
{
    [System.Serializable]
    public class SculptingDataHolder : ISculptingDataHolder
    {
        [SerializeField]
        private GeneralParameters _generalParameters;
        public GeneralParameters GeneralParameters
        {
            get => _generalParameters;
        }

        [SerializeField]
        private DefaultParameters _defaultParameters;
        public DefaultParameters DefaultParameters
        {
            get => _defaultParameters;
        }

        [SerializeField]
        private SmoothParamaters _smoothParamaters;
        public SmoothParamaters SmoothParamaters
        {
            get => _smoothParamaters;
        }

        public List<MeshRing> _meshRings;
        public List<MeshRing> MeshRings
        {
            get => _meshRings;
        }

        [SerializeField]
        private List<Ring> _rings;
        public List<Ring> Rings
        {
            get => _rings;
        }

        public Ring TopRing
        {
            get => _rings[_rings.Count - 1];
        }

        public Ring BottomRing
        {
            get => _rings[0];
        }

        private bool _isDataUpdated;
        public bool IsDataUpdated
        {
            get => _isDataUpdated;
            set => _isDataUpdated = value;
        }
        private bool _isRingsUpdated;
        public bool IsRingsUpdated
        {
            get => _isRingsUpdated;
            set => _isRingsUpdated = value;
        }
        public void SetDirty()
        {
            _isDataUpdated = true;
            _isRingsUpdated = true;
        }

        public SculptingDataHolder()
        {

        }
        public SculptingDataHolder(SmoothParamaters smoothParamaters)
        {
            _smoothParamaters = new SmoothParamaters(smoothParamaters);
            _meshRings = new List<MeshRing>();
        }

        public SculptingDataHolder(DefaultParameters parameters, SmoothParamaters smoothParamaters)
        {
            _generalParameters = new GeneralParameters(parameters.Height);
            _defaultParameters = new DefaultParameters(parameters);
            _smoothParamaters = new SmoothParamaters(smoothParamaters);

            _rings = new List<Ring>();
            _rings.Add(new Ring(new Vector2(parameters.Radius, 0), parameters.WallWidthMax));

            _meshRings = new List<MeshRing>();
        }

        public Ring GetRing(int i)
        {
            return _rings[i];
        }

        public int GetRingsCount()
        {
            return _rings.Count;
        }

        public void CopyFrom(IItemDataHolder itemData)
        {
            SculptingDataHolder reference = (SculptingDataHolder)itemData;

            _generalParameters = new GeneralParameters(reference.GeneralParameters);
            _defaultParameters = new DefaultParameters(reference.DefaultParameters);

            _rings = new List<Ring>();
            foreach (Ring ring in reference.Rings)
            {
                _rings.Add(new Ring(ring.Position, ring.Width, ring.Color));
            }
        }
    }
}