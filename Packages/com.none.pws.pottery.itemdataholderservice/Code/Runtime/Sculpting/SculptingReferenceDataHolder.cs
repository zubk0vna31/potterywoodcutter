namespace PWS.Pottery.ItemDataHolderService
{
    public class SculptingReferenceDataHolder
    {
        public ISculptingDataHolder Data;

        public SculptingReferenceDataHolder(ISculptingDataHolder data)
        {
            Data = data;
        }
    }
}