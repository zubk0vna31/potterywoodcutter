using Leopotam.Ecs;

namespace Code.Runtime.Systems
{
    // stage specific system that runs only at specific stage :/
    public abstract class StageRunSystem <T> : IEcsRunSystem where T:struct
    {
        private readonly EcsFilter<T> _stageFilter;
        public void Run()
        {
            if(_stageFilter.IsEmpty())
                return;
            
            StageRun();
        }

        public abstract void StageRun();
    }
}