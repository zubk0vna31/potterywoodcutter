using Modules.Root.ContainerComponentModel;
using PWS.Common.Messages;
using PWS.Common.SceneSwitchService;
using PWS.Common.UserInfoService;
using PWS.Features.Achievements;
using PWS.Tutorial;
using System;
using UniRx;

namespace PWS.Common
{

    public class MessagesLinker : IInitializable, IDisposable
    {
        // dependencies
        [Inject] private IUserInfoService _userInfoService;
        [Inject] private ITutorialService _tutorialService;
        [Inject] private ISceneSwitchService _sceneSwitchService;

        private string _hubScene;

        private CompositeDisposable _disposables;

        public MessagesLinker(string hubScene)
        {
            _hubScene = hubScene;
        }

        public void Init()
        {
            _disposables = new CompositeDisposable();

            MessageBroker.Default.Receive<RestartCurrentStepMessage>().Subscribe(msg => RestartCurrentStep(msg)).AddTo(_disposables);
            MessageBroker.Default.Receive<RestartFirstStepMessage>().Subscribe(msg => RestartFirstStep(msg)).AddTo(_disposables);
            MessageBroker.Default.Receive<ReturnToHubMessage>().Subscribe(msg => ReturnToHub(msg)).AddTo(_disposables);
            MessageBroker.Default.Receive<ResetUserMessage>().Subscribe(msg => ResetUser(msg)).AddTo(_disposables);
        }

        void RestartCurrentStep(RestartCurrentStepMessage msg)
        {

        }
        void RestartFirstStep(RestartFirstStepMessage msg)
        {

        }
        void ReturnToHub(ReturnToHubMessage msg)
        {

            _sceneSwitchService.LoadScene(_hubScene);
        }
        void ResetUser(ResetUserMessage msg)
        {
            _userInfoService.ResetRegistration();
            _tutorialService.TutorialCompleted = false;
            _sceneSwitchService.LoadScene(_hubScene);
        }

        public void Dispose()
        {
            if (_disposables != null)
            {
                _disposables.Dispose();
            }
        }

    }
}