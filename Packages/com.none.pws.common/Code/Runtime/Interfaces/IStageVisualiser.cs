namespace PWS.Pottery.Common
{
    public interface IStageVisualiser
    {
        void Visualise();
    }
}