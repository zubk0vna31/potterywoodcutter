namespace PWS.Pottery.Common
{
    public interface IStageDataEvaluator
    {
        public float CompletionPercentage { get; }
    }
}