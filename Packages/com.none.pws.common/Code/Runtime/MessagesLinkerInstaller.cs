using Modules.Root.ContainerComponentModel;
using UnityEngine;

namespace PWS.Common
{
    [CreateAssetMenu(fileName = "MessagesLinkerInstaller", menuName = "PWS/Common/MessageLinker/Installer", order = 0)]
    public class MessagesLinkerInstaller : ASOInstaller
    {
        [SerializeField]
        private string _hubScene;

        public override void Install(IContainer container)
        {
            IInitializable messagesLinker = new MessagesLinker(_hubScene);
            container.Bind(messagesLinker);
            container.RegisterInitializable(messagesLinker);
        }
    }
}