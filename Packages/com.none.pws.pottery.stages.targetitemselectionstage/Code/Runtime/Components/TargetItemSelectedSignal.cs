﻿using PWS.Pottery.ItemDataHolderService;

namespace PWS.Pottery.Stages.TargetItemSelectionStage
{
    public struct TargetItemSelectedSignal
    {
        public ISculptingDataHolder Reference;
    }
}