﻿using Leopotam.Ecs;
using PWS.Pottery.ItemDataHolderService;

namespace PWS.Pottery.Stages.TargetItemSelectionStage
{
    public struct TargetItemSelection
    {
        public TargetItemSelectionView View;
    }
}