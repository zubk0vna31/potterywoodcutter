﻿using UnityEngine;
using System.Collections;

namespace PWS.Pottery.Stages.TargetItemSelectionStage
{
    [CreateAssetMenu(fileName = "TargetItemSelectionStateConfig", menuName = "PWS/Pottery/Stages/TargetItemSelectionStage/Config", order = 0)]
    public class TargetItemSelectionStateConfig : ScriptableObject
    {
    }
}