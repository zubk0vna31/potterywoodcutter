using Leopotam.Ecs;
using Modules.StateGroup.Components;
using PWS.Pottery.ItemDataHolderService;

namespace PWS.Pottery.Stages.TargetItemSelectionStage
{
    public class TargetItemSelectionProcessing : IEcsRunSystem
    {
        // auto injected fields
        readonly EcsFilter<StateEnter, TargetItemSelectionState> _entering;
        readonly EcsFilter<StateExit, TargetItemSelectionState> _exiting;
        readonly EcsFilter<TargetItemSelectionState> _inState;

        readonly EcsFilter<TargetItemSelection> _view;
        readonly EcsFilter<TargetItemSelectedSignal> _signal;

        //runtime data
        private ISculptingDataHolder _referenceData;

        public TargetItemSelectionProcessing(ISculptingDataHolder referenceData)
        {
            _referenceData = referenceData;
        }

        public void Run()
        {
            if (!_entering.IsEmpty())
            {
                foreach (var window in _view)
                {
                    _view.Get1(window).View.Show();
                }
            }
            if (!_exiting.IsEmpty())
            {
                foreach (var window in _view)
                {
                    _view.Get1(window).View.Hide();
                }
            }

            if (_inState.IsEmpty()) return;

            foreach (var signal in _signal)
            {
                _referenceData.CopyFrom(_signal.Get1(signal).Reference);
            }
        }
    }
}