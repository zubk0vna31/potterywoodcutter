﻿using UnityEngine;
using Modules.ViewHub;
using Leopotam.Ecs;
using DG.Tweening;
using PWS.Pottery.ItemDataHolderService;
using PWS.Common.UI;
using UnityEngine.XR.Interaction.Toolkit;

namespace PWS.Pottery.Stages.TargetItemSelectionStage
{
    public class TargetItemSelectionView : ViewComponent
    {
        [SerializeField] private Transform _itemContainer;
        [SerializeField] private SculptingItem[] _references;

        [SerializeField] private float _jumpHeight = 0.5f;
        [SerializeField] private float _jumpPower = 0.1f;
        [SerializeField] private float _jumpTime = 0.5f;

        private SculptingItem _selectedReference;
        private EcsEntity _ecsEntity;

        private Vector3 _initPosition;

        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            _initPosition = _itemContainer.position;

            _ecsEntity = ecsEntity;
            ecsEntity.Get<TargetItemSelection>().View = this;
            foreach (var r in _references)
            {
                SculptingItem reference = r;
                r.Item.selectEntered.AddListener((SelectEnterEventArgs args) =>
                {
                    if (_selectedReference == null)
                    {
                        _selectedReference = reference;
                        _itemContainer.DOScaleY(0, _jumpTime);
                        _itemContainer.DOLocalJump(Vector3.up * _jumpHeight, _jumpPower, 0, _jumpTime).OnComplete(SelectionCompleted);
                        
                    }
                });
            }


            Hide();
        }

        void SelectionCompleted()
        {
            if (_selectedReference == null) return;

            _ecsEntity.Get<TargetItemSelectedSignal>().Reference = _selectedReference.SculptingTemplate.Data;
            _ecsEntity.Get<NextStateSignal>();

            _selectedReference = null;
        }

        public void Show()
        {
            if (!_itemContainer.gameObject.activeSelf)
                _itemContainer.gameObject.SetActive(true);
        }
        public void Hide()
        {
            if (_itemContainer.gameObject.activeSelf)
                _itemContainer.gameObject.SetActive(false);

            _itemContainer.position = _initPosition;
            _itemContainer.localScale = Vector3.one;
        }

        [System.Serializable]
        public class SculptingItem
        {
            public SculptingTemplateData SculptingTemplate;
            public XRBaseInteractable Item;
        }
    }
}