using Modules.StateGroup.Core;
using UnityEngine;

namespace PWS.Pottery.Stages.TargetItemSelectionStage
{
    [CreateAssetMenu(menuName = "PWS/Pottery/Stages/TargetItemSelectionStage/SetStateSOCommand")]
    public class SetTargetItemStateSOCommand : SetStateSOCommand<TargetItemSelectionState>
    {
        
    }
}