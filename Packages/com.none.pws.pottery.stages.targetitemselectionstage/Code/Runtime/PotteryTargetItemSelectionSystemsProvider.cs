﻿using UnityEngine;
using Modules.StateGroup.Core;
using PWS.Common.UI;
using Modules.Root.ECS;
using Leopotam.Ecs;
using Modules.Root.ContainerComponentModel;
using PWS.Pottery.ItemDataHolderService;
using PWS.Common.Audio;
using PWS.Common.TravelPoints;

namespace PWS.Pottery.Stages.TargetItemSelectionStage
{
    public class PotteryTargetItemSelectionSystemsProvider : MonoBehaviour, ISystemsProvider
    {
        internal class SystemsDependencies
        {
            [Inject] public SculptingReferenceDataHolder SculptingReferenceData;
        }

        [SerializeField] private SetStateSOCommand _nextState;
        [SerializeField] private TargetItemSelectionStateConfig _config;
        [SerializeField] private StateInfoData _stateInfo;

        [Header("Voice config")] 
        [SerializeField] private VoiceConfig _voiceIntroConfig;
        [SerializeField] private VoiceConfig _voiceBaseConfig;

        public EcsSystems GetSystems(EcsWorld world, EcsSystems endFrame, EcsSystems mainSystems)
        {
            SystemsDependencies dependencies = new SystemsDependencies();
            SceneContainer.Instance.Inject(dependencies);

            EcsSystems systems = new EcsSystems(world);
            systems
                .Add(new TransitToStateOnUIActionSystem<TargetItemSelectionState>(_nextState))
                //.Add(new StateCurrentRestartProcessing<TargetItemSelectionState, SculptingDataHolder>(dependencies.SculptingReferenceData.Data))

                .Add(new TargetItemSelectionProcessing(dependencies.SculptingReferenceData.Data))

                //ui
                .Add(new StateInfoWindowUIProcessing<TargetItemSelectionState>(_stateInfo))
                
                //voice
                .Add(new VoiceIntroAudioSystem<TargetItemSelectionState>(_voiceIntroConfig))
                .Add(new VoiceBaseAudioSystem<TargetItemSelectionState>(_voiceBaseConfig))

                //travel point
                .Add(new TravelPointPositioningSystem<TargetItemSelectionState>("Start"))

                ;

            endFrame
                .OneFrame<NextStateSignal>()
                .OneFrame<TargetItemSelectedSignal>()
                ;

            return systems;
        }
    }
}