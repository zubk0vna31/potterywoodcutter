﻿using UnityEngine;
using Modules.ViewHub;
using Leopotam.Ecs;


namespace PWS.Pottery.Stages.ItemSlicingStage
{
    public class CuttingToolTriggerView : ViewComponent
    {
        private EcsEntity _ecsEntity;

        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            _ecsEntity = ecsEntity;
            ecsEntity.Get<CuttingToolTrigger>();
        }
    }
}