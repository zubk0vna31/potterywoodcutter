﻿using UnityEngine;
using Modules.ViewHub;
using Leopotam.Ecs;


namespace PWS.Pottery.Stages.ItemSlicingStage
{
    public class ItemSlicingContainerView : ViewComponent
    {
        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            ecsEntity.Get<ItemSlicingContainer>();
        }
    }
}