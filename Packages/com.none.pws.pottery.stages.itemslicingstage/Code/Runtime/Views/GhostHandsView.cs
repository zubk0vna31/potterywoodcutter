﻿using Leopotam.Ecs;
using Modules.ViewHub;
using UnityEngine;

namespace PWS.Pottery.Stages.ItemSlicingStage
{
    public class GhostHandsView : ViewComponent
    {
        [SerializeField] private GameObject _ghostHands;
        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            ecsEntity.Get<GhostHandsComponent>().GhostHands = _ghostHands;
        }
    }
}