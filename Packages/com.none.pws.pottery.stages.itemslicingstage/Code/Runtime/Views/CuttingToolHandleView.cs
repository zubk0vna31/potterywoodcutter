﻿using UnityEngine;
using Modules.ViewHub;
using Leopotam.Ecs;


namespace PWS.Pottery.Stages.ItemSlicingStage
{
    public class CuttingToolHandleView : ViewComponent
    {
        private EcsEntity _ecsEntity;

        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            _ecsEntity = ecsEntity;
            ecsEntity.Get<CuttingToolHandle>();
        }

        private void OnTriggerEnter(Collider other)
        {
            if (!other.tag.Equals("CuttingToolTrigger")) return;

            _ecsEntity.Get<CuttingToolTriggerSignal>().Triggered = true;
        }
        private void OnTriggerExit(Collider other)
        {
            if (!other.tag.Equals("CuttingToolTrigger")) return;

            _ecsEntity.Get<CuttingToolTriggerSignal>().Triggered = false;
        }
    }
}