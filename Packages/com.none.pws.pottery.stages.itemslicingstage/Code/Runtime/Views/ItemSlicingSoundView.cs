﻿using Leopotam.Ecs;
using Modules.ViewHub;
using PWS.Common.Audio;
using UnityEngine;

namespace PWS.Pottery.Stages.ItemSlicingStage
{
    public class ItemSlicingSoundView : ViewComponent
    {
        [SerializeField] private RandomSoundClipsData _slicingClips;
        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            ecsEntity.Get<ItemSlicingSoundData>().SlicingClips = _slicingClips;
        }
    }
}