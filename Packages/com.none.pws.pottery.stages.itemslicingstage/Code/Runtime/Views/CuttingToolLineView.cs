﻿using Modules.ViewHub;
using Leopotam.Ecs;

namespace PWS.Pottery.Stages.ItemSlicingStage
{
    public class CuttingToolLineView : ViewComponent
    {
        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            ecsEntity.Get<CuttingToolLine>().View = GetComponent<CuttingTool>();
        }
    }
}