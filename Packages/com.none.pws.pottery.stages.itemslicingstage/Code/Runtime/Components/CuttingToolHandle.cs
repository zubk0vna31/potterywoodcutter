using UnityEngine;

namespace PWS.Pottery.Stages.ItemSlicingStage
{
    public struct CuttingToolHandle
    {
        public bool Triggering;
        public Vector3 InitPos;
        public float Side;
    }
}
