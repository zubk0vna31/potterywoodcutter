﻿using UnityEngine;

namespace PWS.Pottery.Stages.ItemSlicingStage
{
    public struct GhostHandsComponent
    {
        public GameObject GhostHands;
    }
}