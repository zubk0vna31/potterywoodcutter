using Leopotam.Ecs;

namespace PWS.Pottery.Stages.ItemSlicingStage
{
    public struct CuttingToolTrigger
    {
        public bool CuttingEnded;
    }
}
