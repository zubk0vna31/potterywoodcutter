﻿using PWS.Common.Audio;

namespace PWS.Pottery.Stages.ItemSlicingStage
{
    public struct ItemSlicingSoundData
    {
        public RandomSoundClipsData SlicingClips;
    }
}