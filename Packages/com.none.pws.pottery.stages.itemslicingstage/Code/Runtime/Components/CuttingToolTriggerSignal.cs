namespace PWS.Pottery.Stages.ItemSlicingStage
{
    public struct CuttingToolTriggerSignal
    {
        public bool Triggered;
    }
}
