using Leopotam.Ecs;
using Modules.Root.ContainerComponentModel;
using Modules.Root.ECS;
using Modules.StateGroup.Core;
using PWS.Pottery.Common;
using UnityEngine;
using PWS.Common.Messages;
using PWS.Common.RestartStage;
using PWS.Common.UI;
using PWS.Pottery.MeshGeneration;
using PWS.Pottery.ItemDataHolderService;
using Modules.VRFeatures;
using PWS.Common.Audio;
using PWS.Common.ResultsEvaluation;
using PWS.Common.ValueRegulator;
using PWS.Common.GameModeInfoService;
using PWS.Common.TravelPoints;

namespace PWS.Pottery.Stages.ItemSlicingStage
{
    public class PotteryItemSlicingSystemsProvider : MonoBehaviour, ISystemsProvider
    {
        internal class SystemsDependencies
        {
            [Inject] public ISculptingDataHolder SculptingData;
            [Inject] public IValueRegulatorDataHolder<WheelSpeedRegulator> WheelData;
            [Inject] public IResultsEvaluationDataHolder ResultsEvaluationData;
            [Inject] public IGameModeInfoService GameModeData;
        }

        [SerializeField] private SetStateSOCommand _nextState;
        [SerializeField] private SetStateSOCommand _afterNextState;
        [SerializeField] private ItemSlicingStateConfig _config;
        [SerializeField] private ValueRegulatorConfig _wheelSpeedRegulatorConfig;
        [SerializeField] private StateInfoData _stateInfo;
        
        [Header("Voice config")] 
        [SerializeField] private VoiceConfig _voiceConfig;

        public EcsSystems GetSystems(EcsWorld world, EcsSystems endFrame, EcsSystems mainSystems)
        {
            SystemsDependencies dependencies = new SystemsDependencies();
            SceneContainer.Instance.Inject(dependencies);

            EcsSystems systems = new EcsSystems(world);
            systems

                //show sculpting inside
                .Add(new ProductMeshAngleRangeProcessing<ItemSlicingWheelStopState, SculptingProductMesherTag>(360, 1, dependencies.SculptingData))

                //stop wheel state systems
                .Add(new GameObjectVisibilityProcessing<ItemSlicingWheelStopState, WheelSpeedRegulator>())
                .Add(new ValueRegulatorSetupProcessing<ItemSlicingWheelStopState, WheelSpeedRegulator>(_wheelSpeedRegulatorConfig, dependencies.WheelData))
                .Add(new ItemSlicingStopWheelProcessing(dependencies.WheelData))

                .Add(new MeshVisibilityProcessing<ItemSlicingWheelStopState, SculptingProductTag>())
                //ui
                .Add(new StateInfoWindowUIProcessing<ItemSlicingWheelStopState>(_stateInfo))

                //state systems
                .Add(new SkipNextStateOnUIActionSystem<ItemSlicingState>(_nextState, _afterNextState, dependencies.SculptingData, _config))

                //restart
                .Add(new CurrentStateRestoreDataProcessing<ItemSlicingState>())

                .Add(new GameObjectVisibilityProcessing<ItemSlicingState, ItemSlicingContainer>())
                .Add(new ItemSlicingHandlesPositioning(dependencies.SculptingData, _config))
                .Add(new ItemSlicingLineRendererProcessing(dependencies.SculptingData, _config))
                .Add(new ItemSlicingCompletionTracker(dependencies.SculptingData, _config))

                //Outline
                .Add(new OutlineByStateSystem<ItemSlicingWheelStopState>(0))
                .Add(new OutlineByStateSystem<ItemSlicingState>(3))

                .Add(new SetCustomGrade<ItemSlicingState>(1, dependencies.ResultsEvaluationData, _stateInfo))

                //ui
                .Add(new StateInfoWindowUIProcessing<ItemSlicingState>(_stateInfo))

                .Add(new StateWindowPlacementProcessing<ItemSlicingState>("Wheel"))
                .Add(new StateWindowProgressUIProcessing<ItemSlicingState>())
                .Add(new StateWindowButtonUIProcessing<ItemSlicingState>())

                .Add(new MeshVisibilityProcessing<ItemSlicingState, SculptingProductTag>())
                
                //voice
                .Add(new VoiceAudioSystem<ItemSlicingState>(_voiceConfig))

                //sound
                .Add(new ItemSlicingSoundSystem(dependencies.SculptingData, _config))

                //travel point
                .Add(new TravelPointPositioningSystem<ItemSlicingWheelStopState>("Wheel"))
                .Add(new TravelPointPositioningSystem<ItemSlicingState>("Wheel"))

                ;

            endFrame
                .Add(new ValueRegulatorUpdateProcessing<ItemSlicingWheelStopState, WheelSpeedRegulator>(dependencies.WheelData))

                //sculpting product
                .Add(new SculptingDataProcessingSystem<ItemSlicingWheelStopState>(dependencies.SculptingData))
                .Add(new MeshRingsDataProcessingSystem<ItemSlicingWheelStopState>(dependencies.SculptingData))
                .Add(new MeshUpdateSystem<ItemSlicingWheelStopState, SculptingProductMesherTag>(dependencies.SculptingData))
                .Add(new ItemSlicingGhostHandsVisibilitySystem(dependencies.GameModeData))

                .OneFrame<RestartCurrentStepMessage>()
                .OneFrame<NextStateSignal>()
                .OneFrame<CuttingToolTriggerSignal>()
                .OneFrame<SelectEntered>()
                .OneFrame<SelectExited>()
                .OneFrame<ValueRegulatorChangedSignal>()
                ;

            return systems;
        }
    }
}
