using Modules.StateGroup.Core;
using UnityEngine;

namespace PWS.Pottery.Stages.ItemSlicingStage
{
    [CreateAssetMenu(fileName = "SetItemSlicingStateSOCommand", menuName = "PWS/Pottery/Stages/ItemSlicingStage/SetStateSOCommand")]
    public class SetItemSlicingStateSOCommand : SetStateSOCommand<ItemSlicingWheelStopState>
    {

    }
}