using UnityEngine;

namespace PWS.Pottery.Stages.ItemSlicingStage
{
    [CreateAssetMenu(fileName = "ItemSlicingStageConfig", menuName = "PWS/Pottery/Stages/ItemSlicingStage/Config", order = 0)]
    public class ItemSlicingStateConfig : ScriptableObject
    {
        public float CompletionThreshold = 0.9f;

        public float Offset = 0.1f;

        public float LineOffset = 0.05f;

        public float SculptingProductHeightToSkipNextState = 0.1f;
    }
}
