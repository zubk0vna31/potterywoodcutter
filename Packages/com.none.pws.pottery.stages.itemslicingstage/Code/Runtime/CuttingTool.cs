using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PWS.Pottery.Stages.ItemSlicingStage
{
    public class CuttingTool : MonoBehaviour
    {

        [SerializeField]
        private Vector3 _offset = Vector3.zero;
        [SerializeField]
        private int _segments = 10;
        [SerializeField]

        private Transform _point0;
        [SerializeField]
        private Transform _point1;

        private LineRenderer _lineRenderer;

        // Start is called before the first frame update
        void Start()
        {
            _lineRenderer = GetComponent<LineRenderer>();
        }

        public void SetOffset(Vector3 offset)
        {
            _offset = offset;
        }

        // Update is called once per frame
        void Update()
        {
            if (_offset.sqrMagnitude > 0)
            {
                _lineRenderer.positionCount = _segments;
                for (int i = 0; i < _segments; i++)
                {
                    float lerp = (float)i / (_segments - 1);
                    float value = lerp * 2 - 1;
                    Vector3 pos = Vector3.Lerp(_point0.position, _point1.position, lerp);
                    pos += _offset * (Mathf.Pow(value, 2) - 1);

                    _lineRenderer.SetPosition(i, pos);
                }
            }
            else
            {
                _lineRenderer.positionCount = 2;
                _lineRenderer.SetPosition(0, _point0.position);
                _lineRenderer.SetPosition(1, _point1.position);
            }
        }
    }
}
