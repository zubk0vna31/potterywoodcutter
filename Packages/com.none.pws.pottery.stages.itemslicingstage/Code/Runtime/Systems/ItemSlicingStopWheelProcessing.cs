﻿using Leopotam.Ecs;
using Modules.StateGroup.Components;
using Modules.StateGroup.Core;
using PWS.Pottery.ItemDataHolderService;

namespace PWS.Pottery.Stages.ItemSlicingStage
{
    public class ItemSlicingStopWheelProcessing : IEcsRunSystem
    {
        // auto injected fields
        readonly EcsFilter<ItemSlicingWheelStopState>.Exclude<StateExit> _inState;

        private IValueRegulatorDataHolder<WheelSpeedRegulator> _wheelData;

        private StateFactory _stateFactory;

        public ItemSlicingStopWheelProcessing(IValueRegulatorDataHolder<WheelSpeedRegulator> wheelData)
        {
            _wheelData = wheelData;
        }

        public void Run()
        {
            if (!_inState.IsEmpty())
            {
                if (_wheelData.Value == 0)
                {
                    _stateFactory.SetState<ItemSlicingState>();
                }
            }
        }
    }
}