﻿using Leopotam.Ecs;
using Modules.StateGroup.Components;
using PWS.Common.GameModeInfoService;

namespace PWS.Pottery.Stages.ItemSlicingStage
{
    public class ItemSlicingGhostHandsVisibilitySystem : IEcsRunSystem
    {
        private readonly EcsFilter<StateEnter, ItemSlicingState> _enterState;
        private readonly EcsFilter<CuttingToolHandle> _tool;
        private readonly EcsFilter<GhostHandsComponent> _ghostHands;
        private readonly EcsFilter<CuttingToolTriggerSignal> _signal;

        private IGameModeInfoService _gameModeData;

        public ItemSlicingGhostHandsVisibilitySystem(IGameModeInfoService gameModeData)
        {
            _gameModeData = gameModeData;
        }

        public void Run()
        {
            if (_gameModeData.CurrentMode != GameMode.Tutorial)
                return;

            if (!_enterState.IsEmpty())
            {
                foreach (var i in _ghostHands)
                {
                    _ghostHands.Get1(i).GhostHands.SetActive(true);
                }
            }
            
            if (!_signal.IsEmpty())
            {
                var bothHands = true;
                foreach (var i in _tool)
                {
                    bothHands &= _tool.Get1(i).Triggering;
                }
                
                if (bothHands)
                {
                    foreach (var i in _ghostHands)
                    {
                        _ghostHands.Get1(i).GhostHands.SetActive(false);
                    }
                }
            }
        }
    }
}