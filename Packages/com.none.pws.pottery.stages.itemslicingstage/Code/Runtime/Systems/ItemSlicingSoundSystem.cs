﻿using Leopotam.Ecs;
using Modules.Audio;
using Modules.StateGroup.Components;
using Modules.UPhysics;
using Modules.ViewHub;
using PWS.Pottery.ItemDataHolderService;
using PWS.Pottery.MeshGeneration;
using UnityEngine;

namespace PWS.Pottery.Stages.ItemSlicingStage
{
    public class ItemSlicingSoundSystem : IEcsRunSystem
    {
        private readonly EcsFilter<StateEnter, ItemSlicingState> _enterState;
        private readonly EcsFilter<ItemSlicingState> _inState;
        private readonly EcsFilter<CuttingToolTrigger> _trigger;
        private readonly EcsFilter<RigidbodyComponent, CuttingToolHandle> _handle;
        private readonly EcsFilter<AudioSourceRef, ItemSlicingSoundData> _slicingClips;
        private readonly EcsFilter<UnityView, SculptingProductTag> _product;

        private bool _cuttingEnded;
        
        private ISculptingDataHolder _data;
        private ItemSlicingStateConfig _config;
        
        public ItemSlicingSoundSystem(ISculptingDataHolder data, ItemSlicingStateConfig config)
        {
            _data = data;
            _config = config;
        }
        public void Run()
        {
            if (!_enterState.IsEmpty())
                _cuttingEnded = false;
            
            if (_inState.IsEmpty())
                return;
            
            if (!_cuttingEnded)
            {
                foreach (var trigger in _trigger)
                {
                    if (_trigger.Get1(trigger).CuttingEnded)
                    {
                        foreach (var i in _slicingClips)
                        {
                            _slicingClips.Get1(i).AudioSource.Stop();
                            _cuttingEnded = true;
                            return;
                        }
                    }
                }
            }
            else
            {
                return;
            }

            foreach (var slicingClips in _slicingClips)
            {
                bool canCut = true;

                foreach (var handle in _handle)
                {
                    canCut &= _handle.Get2(handle).Triggering;
                }
                
                if (canCut)
                {
                    foreach (var product in _product)
                    {
                        float endPos = _data.BottomRing.Position.x + _data.BottomRing.Width / 2;
                        Vector3 avgPos = Vector3.zero;

                        foreach (var handle in _handle)
                        {
                            Vector3 localPos = _product.Get1(product).Transform
                                .InverseTransformPoint(_handle.Get1(handle).Rigidbody.position);
                            avgPos += localPos;
                        }

                        avgPos /= _handle.GetEntitiesCount();

                        if ((avgPos.z - _config.LineOffset) >= -endPos)
                        {
                            ref var sourceRef = ref _slicingClips.Get1(slicingClips);
                            if (sourceRef.AudioSource.isPlaying)
                                return;
                            
                            ref var clips = ref _slicingClips.Get2(slicingClips);
                            ref var play = ref _slicingClips.GetEntity(slicingClips).Get<PlayGlobalAuidioSignal>();
                            play.AudioSource = sourceRef.AudioSource;
                            play.AudioSource.loop = true;
                            var id = Random.Range(0, clips.SlicingClips.Clips.Count);
                            play.AudioClip = clips.SlicingClips.Clips[id];
                        }
                        else
                        {
                            _slicingClips.Get1(slicingClips).AudioSource.Stop();
                        }
                    }
                }
            }
        }
    }
}