﻿using Leopotam.Ecs;
using Modules.StateGroup.Core;
using PWS.Common.UI;
using PWS.Pottery.ItemDataHolderService;
using UnityEngine;

namespace PWS.Pottery.Stages.ItemSlicingStage
{
    public class SkipNextStateOnUIActionSystem<StateT> : TransitToStateOnUIActionSystem<StateT> where StateT : struct
    {
        protected readonly SetStateSOCommand _afterNextState;

        private ISculptingDataHolder _sculptingData;
        private ItemSlicingStateConfig _config;

        public SkipNextStateOnUIActionSystem(SetStateSOCommand nextState, SetStateSOCommand afterNextState, ISculptingDataHolder sculptingData, ItemSlicingStateConfig config) : base(nextState)
        {
            _afterNextState = afterNextState;
            _sculptingData = sculptingData;
            _config = config;
        }

        public override void Run() {
            if(_sculptingData.GeneralParameters.FullHeight < _config.SculptingProductHeightToSkipNextState)
            {
                if (_inState.IsEmpty()) return;
                if (_actionPerformed.IsEmpty()) return;

                foreach (var i in _actionPerformed)
                {
                    _actionPerformed.GetEntity(i).Del<NextStateSignal>();
                }

                _afterNextState.Execute(_stateFactory);

                return;
            }

            base.Run();
        }
    }
}
