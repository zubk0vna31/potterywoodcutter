using DG.Tweening;
using Leopotam.Ecs;
using Modules.StateGroup.Components;
using Modules.ViewHub;
using PWS.Pottery.ItemDataHolderService;
using Modules.VRFeatures;
using UnityEngine;
using Modules.UPhysics;
using PWS.Pottery.MeshGeneration;

namespace PWS.Pottery.Stages.ItemSlicingStage
{
    public class ItemSlicingHandlesPositioning : IEcsInitSystem, IEcsRunSystem
    {
        // auto injected fields
        readonly EcsFilter<StateEnter, ItemSlicingState> _entering;
        readonly EcsFilter<ItemSlicingState> _inState;
        readonly EcsFilter<StateExit, ItemSlicingState> _exiting;

        readonly EcsFilter<UnityView, CuttingToolTrigger> _trigger;
        readonly EcsFilter<RigidbodyComponent, CuttingToolHandle, XRKinematicGrabInteractableComponent> _handle;
        readonly EcsFilter<RigidbodyComponent, CuttingToolHandle, SelectEntered> _selectEntered;
        readonly EcsFilter<RigidbodyComponent, CuttingToolHandle, SelectExited> _selectExited;
        readonly EcsFilter<CuttingToolTriggerSignal, CuttingToolHandle> _signal;

        private ISculptingDataHolder _data;
        private ItemSlicingStateConfig _config;

        //runtimeData

        public ItemSlicingHandlesPositioning(ISculptingDataHolder data, ItemSlicingStateConfig config)
        {
            _data = data;
            _config = config;
        }

        public void Init()
        {
            foreach (var handle in _handle)
            {
                _handle.Get2(handle).InitPos = _handle.Get1(handle).Rigidbody.position;
            }
        }

        public void Run()
        {
            if (!_entering.IsEmpty())
            {
                foreach (var handle in _handle)
                {
                    _handle.Get1(handle).Rigidbody.isKinematic = false;
                    _handle.Get2(handle).Triggering = false;
                    _handle.Get1(handle).Rigidbody.position = _handle.Get2(handle).InitPos;
                    _handle.Get1(handle).Rigidbody.velocity = Vector3.zero;
                    _handle.Get3(handle).Interactable.UseSystem = true;
                    _handle.Get2(handle).Side = 0;
                }
            }
            if (!_exiting.IsEmpty())
            {
                foreach (var handle in _handle)
                {
                    _handle.Get1(handle).Rigidbody.isKinematic = false;
                    _handle.Get3(handle).Interactable.UseSystem = false;
                }
            }

            if (_inState.IsEmpty())
                return;

            foreach (var selectEntered in _selectEntered)
            {
                _selectEntered.Get1(selectEntered).Rigidbody.isKinematic = true;
            }
            foreach (var selectExited in _selectExited)
            {
                foreach (var trigger in _trigger)
                {
                    if (!_selectExited.Get2(selectExited).Triggering || _trigger.Get2(trigger).CuttingEnded)
                        _selectExited.Get1(selectExited).Rigidbody.isKinematic = false;
                }
            }

            foreach (var signal in _signal)
            {
                if(_signal.Get1(signal).Triggered)
                    _signal.Get2(signal).Triggering = true;
            }

            foreach (var handle in _handle)
            {
                if (_handle.Get3(handle).Interactable.isSelected)
                {
                    Vector3 pos = _handle.Get3(handle).Interactable.selectingInteractor.transform.position;

                    foreach (var trigger in _trigger)
                    {
                        if (!_trigger.Get2(trigger).CuttingEnded && _handle.Get2(handle).Triggering)
                        {
                            float offset = _data.BottomRing.Position.x + _config.Offset;
                            Vector3 localPos = _trigger.Get1(trigger).Transform.InverseTransformPoint(pos);
                            float side = _handle.Get2(handle).Side;

                            foreach (var handle2 in _handle)
                            {
                                if (_handle.Get2(handle2).Equals(_handle.Get2(handle)))
                                    continue;

                                if (_handle.Get2(handle).Side == 0 && _handle.Get2(handle2).Side == 0)
                                {
                                    side = Mathf.Sign(localPos.x);
                                    _handle.Get2(handle).Side = side;
                                } else if (_handle.Get2(handle).Side == 0 && _handle.Get2(handle2).Side != 0) {
                                    side = _handle.Get2(handle2).Side * -1;
                                    _handle.Get2(handle).Side = side;
                                }
                            }
                            localPos.x = offset;
                            localPos.x *= side;

                            localPos.z = Mathf.Clamp(localPos.z, -_trigger.Get1(trigger).Transform.localPosition.z * 2, 0);
                            pos = _trigger.Get1(trigger).Transform.TransformPoint(localPos);

                            pos.y = _trigger.Get1(trigger).Transform.position.y;
                        }
                    }

                    _handle.Get1(handle).Rigidbody.position = pos;
                    _handle.Get1(handle).Rigidbody.rotation = _handle.Get3(handle).Interactable.selectingInteractor.transform.rotation;
                }
            }

        }
    }
}