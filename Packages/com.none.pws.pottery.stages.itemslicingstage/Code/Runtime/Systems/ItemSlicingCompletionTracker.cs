using DG.Tweening;
using Leopotam.Ecs;
using Modules.StateGroup.Components;
using Modules.ViewHub;
using PWS.Pottery.ItemDataHolderService;
using Modules.VRFeatures;
using UnityEngine;
using Modules.UPhysics;
using PWS.Pottery.MeshGeneration;
using PWS.Common.UI;

namespace PWS.Pottery.Stages.ItemSlicingStage
{

    public class ItemSlicingCompletionTracker : IEcsRunSystem
    {
        // auto injected fields
        readonly EcsFilter<StateEnter, ItemSlicingState> _entering;
        readonly EcsFilter<ItemSlicingState> _inState;

        readonly EcsFilter<StateWindow, StateProgress> _stateWindowWithProgress;

        readonly EcsFilter<UnityView, CuttingToolTrigger> _trigger;
        readonly EcsFilter<RigidbodyComponent, CuttingToolHandle, XRKinematicGrabInteractableComponent> _handle;

        readonly EcsFilter<UnityView, SculptingProductTag> _product;

        private ISculptingDataHolder _data;
        private ItemSlicingStateConfig _config;

        //runtimeData

        public ItemSlicingCompletionTracker(ISculptingDataHolder data, ItemSlicingStateConfig config)
        {
            _data = data;
            _config = config;
        }

        public void Run()
        {
            if (!_entering.IsEmpty())
            {
                foreach (var i in _stateWindowWithProgress)
                {
                    _stateWindowWithProgress.Get2(i).Value = 0;
                    _stateWindowWithProgress.Get2(i).CompletionThreshold = _config.CompletionThreshold;
                }

                foreach (var trigger in _trigger)
                {
                    _trigger.Get2(trigger).CuttingEnded = false;
                }
            }

            if (_inState.IsEmpty())
                return;

            bool canCut = true;
            foreach (var handle in _handle)
            {
                canCut &= _handle.Get2(handle).Triggering;
            }

            foreach (var trigger in _trigger)
            {
                canCut &= !_trigger.Get2(trigger).CuttingEnded;
            }

            if (canCut) {
                foreach (var product in _product)
                {
                    float diameter = _data.BottomRing.Position.x * 2 + _data.BottomRing.Width;
                    float endPos = _data.BottomRing.Position.x + _data.BottomRing.Width / 2;
                    Vector3 avgPos = Vector3.zero;
                    foreach (var handle in _handle)
                    {
                        Vector3 localPos = _product.Get1(product).Transform.InverseTransformPoint(_handle.Get1(handle).Rigidbody.position);
                        avgPos += localPos;
                    }
                    avgPos /= _handle.GetEntitiesCount();

                    if (Mathf.Abs(avgPos.x) < 0.01f)
                    {
                        float completion = 1 - Mathf.Clamp01(Mathf.Abs((avgPos.z - _config.LineOffset) - endPos) / diameter);

                        foreach (var i in _stateWindowWithProgress)
                        {
                            if (completion > _stateWindowWithProgress.Get2(i).Value)
                                _stateWindowWithProgress.Get2(i).Value = completion;
                        }

                        if ((avgPos.z - _config.LineOffset) >= endPos)
                        {
                            foreach (var trigger in _trigger)
                            {
                                _trigger.Get2(trigger).CuttingEnded = true;
                            }
                        }
                    }
                }
            }
        }
    }
}