using DG.Tweening;
using Leopotam.Ecs;
using Modules.StateGroup.Components;
using Modules.ViewHub;
using PWS.Pottery.ItemDataHolderService;
using Modules.VRFeatures;
using UnityEngine;
using Modules.UPhysics;
using PWS.Pottery.MeshGeneration;
using PWS.Common.UI;
using PWS.Pottery.Common;

namespace PWS.Pottery.Stages.ItemSlicingStage
{

    public class ItemSlicingLineRendererProcessing : IEcsRunSystem
    {
        // auto injected fields
        readonly EcsFilter<StateEnter, ItemSlicingState> _entering;
        readonly EcsFilter<ItemSlicingState> _inState;
        readonly EcsFilter<StateExit, ItemSlicingState> _exiting;

        readonly EcsFilter<CuttingToolLine> _line;

        readonly EcsFilter<UnityView, CuttingToolTrigger> _trigger;
        readonly EcsFilter<RigidbodyComponent, CuttingToolHandle, XRKinematicGrabInteractableComponent> _handle;

        readonly EcsFilter<UnityView, SculptingProductTag> _product;

        private ISculptingDataHolder _data;
        private ItemSlicingStateConfig _config;

        //runtimeData

        public ItemSlicingLineRendererProcessing(ISculptingDataHolder data, ItemSlicingStateConfig config)
        {
            _data = data;
            _config = config;
        }

        public void Run()
        {
            if (!_entering.IsEmpty() || !_exiting.IsEmpty())
            {
                foreach (var line in _line)
                {
                    _line.Get1(line).View.SetOffset(Vector3.zero);
                }
            }

            if (_inState.IsEmpty())
                return;

            bool canCut = true;
            foreach (var handle in _handle)
            {
                canCut &= _handle.Get2(handle).Triggering;
            }
            foreach (var trigger in _trigger)
            {
                canCut &= !_trigger.Get2(trigger).CuttingEnded;
            }

            if (canCut) {
                foreach (var product in _product)
                {
                    float diameter = _data.BottomRing.Position.x * 2 + _data.BottomRing.Width;
                    float endPos = _data.BottomRing.Position.x + _data.BottomRing.Width / 2;
                    float beginPos = -endPos;
                    Vector3 avgPos = Vector3.zero;
                    foreach (var handle in _handle)
                    {
                        Vector3 localPos = _product.Get1(product).Transform.InverseTransformPoint(_handle.Get1(handle).Rigidbody.position);
                        avgPos += localPos;
                    }
                    avgPos /= _handle.GetEntitiesCount();

                    foreach (var line in _line)
                    {
                        if (avgPos.z - _config.LineOffset >= endPos)
                        {
                            _line.Get1(line).View.SetOffset(Vector3.zero);
                        }
                        else
                        {
                            float offset = Mathf.Clamp(avgPos.z - beginPos, 0, _config.LineOffset);
                            _line.Get1(line).View.SetOffset(_product.Get1(product).Transform.forward * offset);
                        }
                    }
                    
                }
            }
        }
    }
}