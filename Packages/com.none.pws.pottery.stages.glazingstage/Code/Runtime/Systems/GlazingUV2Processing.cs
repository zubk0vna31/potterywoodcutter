using Leopotam.Ecs;
using Modules.StateGroup.Components;
using Modules.ViewHub;
using PWS.Pottery.Common;
using PWS.Pottery.ItemDataHolderService;
using PWS.Pottery.MeshGeneration;
using UnityEngine;

namespace PWS.Pottery.Stages.GlazingStage
{
    public class GlazingUV2Processing<StateT> : IEcsRunSystem where StateT : struct
    {
        // auto injected fields
        readonly EcsFilter<StateEnter, StateT> _entering;
        readonly EcsFilter<MeshFilterComponent, SculptingProductTag> _meshFilter;

        private ISculptingDataHolder _data;

        //runtime data
        private Vector2[] _uvs2;

        public GlazingUV2Processing(ISculptingDataHolder data)
        {
            _data = data;
        }

        public void Run()
        {
            if (!_entering.IsEmpty())
            {
                foreach (var meshFilter in _meshFilter)
                {
                    UpdateUV2(_meshFilter.Get1(meshFilter).MeshFilter);
                }
            }
        }

        void UpdateUV2(MeshFilter meshFilter)
        {
            Mesh mesh = meshFilter.sharedMesh;

            int realVertices = _data.SmoothParamaters.Vertices + 1;

            _uvs2 = new Vector2[_data.MeshRings.Count * realVertices];

            for (int i = 0; i < _data.MeshRings.Count; i++)
            {
                for (int j = 0; j < realVertices; j++)
                {
                    float uvX = (float)j / _data.SmoothParamaters.Vertices;
                    float uvY = (float)i / (_data.MeshRings.Count - 1);

                    _uvs2[i * realVertices + j] = new Vector2(uvX, uvY);
                }
            }

            mesh.uv2 = _uvs2;
            meshFilter.sharedMesh = mesh;
        }
    }
}
