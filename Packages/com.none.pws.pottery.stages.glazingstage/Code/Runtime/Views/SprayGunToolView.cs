﻿using Modules.ViewHub;
using Leopotam.Ecs;
using UnityEngine;
using PWS.Pottery.Stages.DecorativeSculptingStage;
using UnityEngine.XR.Interaction.Toolkit;

namespace PWS.Pottery.Stages.GlazingStage
{
    public class SprayGunToolView : ViewComponent
    {
        [SerializeField]
        private float _hitDistance = 0.3f;
        [SerializeField]
        private Transform _painter;

        private EcsEntity _ecsEntity;
        private XRGrabInteractable _interactable;
        private bool _triggered;

        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            ecsEntity.Get<DrawInteractor>();
            ecsEntity.Get<SprayGunTool>();
            _ecsEntity = ecsEntity;

            _interactable = GetComponent<XRGrabInteractable>();
            _interactable.selectExited.AddListener(DroppedGun);
            _interactable.activated.AddListener(TriggerPulled);
            _interactable.deactivated.AddListener(TriggerReleased);
        }

        private void Update()
        {
            if (!_triggered) return;

            RaycastHit[] hits = Physics.RaycastAll(_painter.position, _painter.forward, _hitDistance);
            foreach (RaycastHit hit in hits)
            {
                if (!hit.collider)
                    continue;

                if (!hit.collider.tag.Equals("SculptingProduct"))
                    continue;

                _ecsEntity.Get<StackCutterHitSignal>().Position = hit.textureCoord2;
            }
        }


        void TriggerReleased(DeactivateEventArgs args)
        {
            _triggered = false;
        }

        void TriggerPulled(ActivateEventArgs args)
        {
            _triggered = true;
        }

        void DroppedGun(SelectExitEventArgs args)
        {
            _triggered = false;
        }
    }
}