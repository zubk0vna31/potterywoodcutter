﻿using Modules.ViewHub;
using Leopotam.Ecs;
using UnityEngine;
using PWS.Pottery.Stages.DecorativeSculptingStage;

namespace PWS.Pottery.Stages.GlazingStage
{
    public class GlazingDrawableRef : ViewComponent
    {
        [SerializeField]
        private Drawable _drawable;

        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            ecsEntity.Get<DrawableComponent<GlazingDrawableComponent>>().View = _drawable;
        }
    }
}