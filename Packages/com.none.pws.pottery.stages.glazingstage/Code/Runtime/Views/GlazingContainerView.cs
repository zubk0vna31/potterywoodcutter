﻿using Modules.ViewHub;
using Leopotam.Ecs;
using UnityEngine;

namespace PWS.Pottery.Stages.GlazingStage
{
    public class GlazingContainerView : ViewComponent
    {
        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            ecsEntity.Get<GlazingContainer>();
        }
    }
}