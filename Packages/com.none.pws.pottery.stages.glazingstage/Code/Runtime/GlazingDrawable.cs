﻿using PWS.Pottery.Stages.DecorativeSculptingStage;
using System.Linq;
using UnityEngine;

namespace PWS.Pottery.Stages.GlazingStage
{
    public class GlazingDrawable : Drawable
    {
        [SerializeField]
        private int _resolution = 512;
        private int m_ReferenceAmount;
        private int m_PaintedAmount;
        
        public override float Percentage => Mathf.Clamp01((m_PaintedAmount * 1.0f) / m_ReferenceAmount);

        public override void SetReference(Texture2D reference)
        {
        }

        public override void Init(string texId)
        {
            m_PaintedAmount = 0;
            m_ReferenceAmount = _resolution * _resolution;

            //Material setup
            m_MaskID = Shader.PropertyToID(texId);
            m_Material = m_MeshRenderer.sharedMaterial;

            //Create mask texture

            if (!m_Mask) {
                m_Mask = new Texture2D(_resolution, _resolution, TextureFormat.R8, false);
                m_Mask.filterMode = FilterMode.Bilinear;
            }
            
            m_Mask.SetPixels(Enumerable.Repeat(
                Color.black, _resolution * _resolution).ToArray());
            m_Mask.Apply();
            
            //Set mask texture to material
            m_Material.SetTexture(m_MaskID, m_Mask);
        }
        
        public override void Paint(Vector2 pos, int radius = 0, bool useCircle = false)
        {
            Vector2Int pixel = new Vector2Int((int)(pos.x * _resolution), (int)(pos.y * _resolution));

            pixel.x = Mathf.Clamp(pixel.x, 0, _resolution);
            pixel.y = Mathf.Clamp(pixel.y, 0, _resolution);
            
            if (radius < 1)
            {
                useCircle = false;
            }
            
            bool needApplyTexture = false;
            
            for (int i = pixel.x - radius; i <= pixel.x + radius; i++)
            {
                for (int j = pixel.y - radius; j <= pixel.y + radius; j++)
                {
                    int iRepeat = Mathf.RoundToInt(Mathf.Repeat(i, _resolution));
                    int jRepeat = Mathf.RoundToInt(Mathf.Repeat(j, _resolution));
                    var current = new Vector2Int(iRepeat, jRepeat);

                    var hasPixel = m_Mask.GetPixel(current.x, current.y).r > 0;
                    if (hasPixel) continue;

                    if (useCircle)
                    {
                        if (InCircle(new Vector2Int(i, j), pixel, radius))
                        {
                            m_Mask.SetPixel(iRepeat, jRepeat, Color.red);
                            needApplyTexture = true;

                            m_PaintedAmount++;
                        }
                    }
                    else
                    {
                        m_Mask.SetPixel(iRepeat, jRepeat, Color.red);
                        needApplyTexture = true;
                        
                        m_PaintedAmount++;
                    }
                }
            }

            if (needApplyTexture)
            {
                m_Mask.Apply();
                m_Material.SetTexture(m_MaskID, m_Mask);
            }
        }

        public override void PaintAllPixels()
        {
            m_Mask.SetPixels(Enumerable.Repeat(
                   Color.red, _resolution * _resolution).ToArray());
            m_Mask.Apply();

            //Set mask texture to material
            m_Material.SetTexture(m_MaskID, m_Mask);
        }

        public override void ClearAll()
        {
            if (!m_Mask)
                return;
            if (!m_Material)
                return;

            m_Mask.SetPixels(Enumerable.Repeat(
                Color.black, _resolution * _resolution).ToArray());
            m_Mask.Apply();

            m_Material.SetTexture(m_MaskID, m_Mask);
        }

        bool InCircleRepeat(Vector2Int p, Vector2Int o, int radius)
        {
            int x = Mathf.Abs(p.x - o.x);
            int y = Mathf.Abs(p.y - o.y);

            int halfRes = Mathf.RoundToInt((float)_resolution / 2);
            if (x >= halfRes)
            {
                x = _resolution - x;
                Debug.Log(x);
            }
            if (y >= halfRes)
                y = _resolution - y;

            return (Mathf.Pow(x, 2) + Mathf.Pow(y, 2) <= radius * radius);
        }
    }
}
