using Leopotam.Ecs;

namespace PWS.Pottery.Stages.GlazingStage
{
    public struct GlazingItemPlacingState : IEcsIgnoreInFilter
    {
    }
    public struct GlazingColorSelectionState : IEcsIgnoreInFilter
    {
    }
    public struct GlazingPaintingState : IEcsIgnoreInFilter
    {
    }
}
