using Leopotam.Ecs;
using Modules.Root.ContainerComponentModel;
using Modules.Root.ECS;
using Modules.StateGroup.Core;
using Modules.VRFeatures;
using PWS.Common.Audio;
using PWS.Common.Messages;
using PWS.Common.RestartStage;
using PWS.Common.ResultsEvaluation;
using PWS.Common.TravelPoints;
using PWS.Common.UI;
using PWS.Pottery.Common;
using PWS.Pottery.ItemDataHolderService;
using PWS.Pottery.MeshGeneration;
using PWS.Pottery.Stages.DecorativePainting;
using PWS.Pottery.Stages.DecorativeSculptingStage;
using UnityEngine;

namespace PWS.Pottery.Stages.GlazingStage
{
    public class PotteryGlazingStageSystemsProvider : MonoBehaviour, ISystemsProvider
    {
        internal class SystemsDependencies
        {
            [Inject] public ISculptingDataHolder SculptingData;
            [Inject] public IResultsEvaluationDataHolder ResultsEvaluationData;
        }

        [SerializeField] private SetStateSOCommand _nextState;
        [SerializeField] private DecoratingStateConfig _config;
        [SerializeField] private StateInfoData _stateInfo;

        [Header("Voice config")]
        [SerializeField] private VoiceConfig _itemPlacingVoiceConfig;
        [SerializeField] private VoiceConfig _colorSelectionVoiceConfig;
        [SerializeField] private VoiceConfig _paintingStateVoiceConfig;

        public EcsSystems GetSystems(EcsWorld world, EcsSystems endFrame, EcsSystems mainSystems)
        {
            SystemsDependencies dependencies = new SystemsDependencies();
            SceneContainer.Instance.Inject(dependencies);
            EcsSystems systems = new EcsSystems(world);
            systems
                .Add(new TransitToStateOnUIActionSystem<GlazingPaintingState>(_nextState))

                .Add(new CurrentSubStateRestoreDataProcessing<GlazingColorSelectionState, GlazingPaintingState>())
                .Add(new DecorativeSculptingRestartProcessing<GlazingColorSelectionState,
                GlazingDrawableComponent, SculptingProductTag>(_config.TextureID))

                //item placing state systems
                .Add(new SnapSocketActivityProcessing<GlazingItemPlacingState, DecorativeSculptingSnapSocket>())
                .Add(new GameObjectVisibilityProcessing<GlazingItemPlacingState, GlazingContainer>())

                .Add(new DecoratingItemPlacingProcessing<GlazingItemPlacingState, GlazingColorSelectionState>())

                .Add(new MeshVisibilityProcessing<GlazingItemPlacingState, SculptingProductMesherTag>())
                .Add(new XRInteractableProcessing<GlazingItemPlacingState, SculptingProductTag>())

                .Add(new StateInfoWindowUIProcessing<GlazingItemPlacingState>(_stateInfo))

                //color selection state systems
                .Add(new GameObjectVisibilityProcessing<GlazingColorSelectionState, DecorativePaintingColorMenu>())

                .Add(new DecorativePaintingColorMenuItemProcessing<GlazingColorSelectionState, GlazingDrawableComponent, GlazingPaintingState>("_GlazingColor"))

                .Add(new MeshVisibilityProcessing<GlazingColorSelectionState, SculptingProductMesherTag>())

                .Add(new StateInfoWindowUIProcessing<GlazingColorSelectionState>(_stateInfo))

                //paint state systems

                .Add(new DecoratingSkipProcessing<GlazingPaintingState, GlazingDrawableComponent, SculptingProductTag>(_nextState))

                .Add(new GameObjectVisibilityDoubleStateProcessing<GlazingPaintingState, GlazingContainer, GlazingColorSelectionState>())
                .Add(new GameObjectVisibilityProcessing<GlazingPaintingState, SprayGunTool>())

                .Add(new DecoratingPainterProcessing<GlazingPaintingState, GlazingDrawableComponent, SculptingProductTag>(_config))
                .Add(new DecoratingCompletionTracker<GlazingPaintingState, GlazingDrawableComponent, SculptingProductTag>(_config))

                //Outline
                .Add(new OutlineByStateSystem<GlazingPaintingState>(6))

                .Add(new SetGradeByProgress<GlazingPaintingState>(dependencies.ResultsEvaluationData, _stateInfo))

                .Add(new MeshVisibilityDoubleStateProcessing<GlazingPaintingState, SculptingProductMesherTag, GlazingColorSelectionState>())

                .Add(new StateInfoWindowUIProcessing<GlazingPaintingState>(_stateInfo))
                .Add(new StateWindowPlacementProcessing<GlazingPaintingState>("Table"))
                .Add(new StateWindowProgressUIProcessing<GlazingPaintingState>())
                .Add(new StateWindowButtonUIProcessing<GlazingPaintingState>())
                .Add(new SkipStageButtonUIProcessing<GlazingPaintingState>())
                
                //voice systems
                .Add(new VoiceAudioSystem<GlazingItemPlacingState>(_itemPlacingVoiceConfig))
                .Add(new VoiceAudioSystem<GlazingColorSelectionState>(_colorSelectionVoiceConfig))
                .Add(new VoiceAudioSystem<GlazingPaintingState>(_paintingStateVoiceConfig))

                //travel point
                .Add(new TravelPointPositioningSystem<GlazingItemPlacingState>("Start"))
                .Add(new TravelPointPositioningSystem<GlazingColorSelectionState>("Start"))
                .Add(new TravelPointPositioningSystem<GlazingPaintingState>("Start"))

                ;

            endFrame
                //sculpting product
                .Add(new SculptingDataProcessingSystem<GlazingItemPlacingState>(dependencies.SculptingData))
                .Add(new MeshRingsDataProcessingSystem<GlazingItemPlacingState>(dependencies.SculptingData))
                .Add(new MeshUpdateSystem<GlazingItemPlacingState, SculptingProductMesherTag>(dependencies.SculptingData))

                .Add(new GlazingUV2Processing<GlazingPaintingState>(dependencies.SculptingData))

                .OneFrame<RestartCurrentStepMessage>()
                .OneFrame<NextStateSignal>()
                .OneFrame<SkipStageMessage>()
                .OneFrame<NextSubStateSignal>()

                .OneFrame<DecorativePaintingUpdateSignal>()
                .OneFrame<StackCutterHitSignal>()
                .OneFrame<DecorMenuItemSelected>()
                ;
            return systems;
        }
    }
}
