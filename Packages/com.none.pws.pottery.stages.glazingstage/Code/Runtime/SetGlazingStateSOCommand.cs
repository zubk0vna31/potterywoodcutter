using Modules.StateGroup.Core;
using UnityEngine;

namespace PWS.Pottery.Stages.GlazingStage
{
    [CreateAssetMenu(fileName = "SetGlazingStateSOCommand", menuName = "PWS/Pottery/Stages/GlazingStage/SetStateSOCommand")]
    public class SetGlazingStateSOCommand : SetStateSOCommand<GlazingItemPlacingState>
    {

    }
}