using UnityEditor;
using UnityEngine;

namespace PWS.WoodCutting.Stages.FinalAssembling.Editor
{
    public class CustomShortcuts : ScriptableObject
    {
        [MenuItem("Inspector Tools/Collapse All %#q")]
        private static void CollapseAll()
        {
            SetAllInspectorsExpanded(false);
        }

        [MenuItem("Inspector Tools/Expand All %&q")]
        private static void ExpandAll(MenuCommand command)
        {
            SetAllInspectorsExpanded(true);
        }

        private static void SetAllInspectorsExpanded(bool expanded)
        {
            var activeEditorTracker = ActiveEditorTracker.sharedTracker;

            for (var i = 0; i < activeEditorTracker.activeEditors.Length; i++)
            {
                activeEditorTracker.SetVisible(i, expanded ? 1 : 0);
            }
        }
    }
}
