using Leopotam.Ecs;
using Modules.Root.ContainerComponentModel;
using Modules.Root.ECS;
using PWS.Common.SceneSwitchService;
using PWS.Common.UI;
using PWS.WoodCutting.Common;
using UnityEngine;

namespace PWS.WoodCutting.Stages.FinalAssembling
{
    public class WoodCuttingFinalAssemlingSystemsProvider : MonoBehaviour, ISystemsProvider
    {
        internal class SceneSwitcherServiceContainer
        {
            [Inject] public ISceneSwitchService sceneSwitchService;
        }

        [Header("Core")]
        [SerializeField]
        private StateConfig _stateConfig;
        [SerializeField]
        private StateInfoData _stateInfo;

        [Header("Scene-Related")]
        [SerializeField]
        private Transform playerTeleportTransform;


        public EcsSystems GetSystems(EcsWorld world, EcsSystems endFrame, EcsSystems mainSystems)
        {
            EcsSystems systems = new EcsSystems(world, "WoodCutting Final Assembling");

            SceneSwitcherServiceContainer sceneSwitcher = new SceneSwitcherServiceContainer();

            SceneContainer.Instance.Inject(sceneSwitcher);

            systems

                .Add(new FinalAssemblingStateInitializeSystem(_stateConfig, sceneSwitcher.sceneSwitchService))

                .Add(new OutlineByStateSystem<FinalAssemblingState>(_stateConfig))


                ;


            return systems;
        }
    }
}
