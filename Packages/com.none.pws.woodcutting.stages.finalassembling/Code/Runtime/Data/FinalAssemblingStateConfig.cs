using PWS.WoodCutting.Common;
using UnityEngine;

namespace PWS.WoodCutting.Stages.FinalAssembling
{
    [CreateAssetMenu(fileName = "W_FinalAssemblingStateConfig",menuName = 
        "PWS/WoodCutting/Stages/Final Assembling/State Config")]
    public class FinalAssemblingStateConfig : StateConfig
    {
        [Range(0f,2.5f)]
        public float fadeInDuration = 0.5f;
    }
}
