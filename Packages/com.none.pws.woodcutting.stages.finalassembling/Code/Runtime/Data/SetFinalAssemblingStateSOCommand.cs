using Modules.StateGroup.Core;
using UnityEngine;

namespace PWS.WoodCutting.Stages.FinalAssembling
{
    [CreateAssetMenu(fileName = "W_SetFinalAssemblingStateSOCommand", menuName =
        "PWS/WoodCutting/Stages/Final Assembling/SOCommand")]
    public class SetFinalAssemblingStateSOCommand : SetStateSOCommand<FinalAssemblingState>
    {
        
    }
}
