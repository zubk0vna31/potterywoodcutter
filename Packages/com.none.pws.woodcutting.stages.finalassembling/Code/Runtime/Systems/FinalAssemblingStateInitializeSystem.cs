using Modules.StateGroup.Core;
using Modules.Utils;
using PWS.Common.SceneSwitchService;
using PWS.WoodCutting.Common;
using UnityEngine;

namespace PWS.WoodCutting.Stages.FinalAssembling
{
    public class FinalAssemblingStateInitializeSystem : RunSystem<FinalAssemblingState, FinalAssemblingStateConfig>
    {
        private readonly ISceneSwitchService _sceneSwitchService;
        private readonly TimeService _timeService;
        private readonly StateFactory _stateFactory;

        private float _timer;

        public FinalAssemblingStateInitializeSystem(StateConfig config,
            ISceneSwitchService sceneSwitchService) : base(config)
        {
            _sceneSwitchService = sceneSwitchService;
        }

        protected override void OnStateEnter()
        {
            _sceneSwitchService.FadeIn(_config.fadeInDuration);
            _timer = _config.fadeInDuration;
        }

        protected override void OnStateUpdate()
        {
            base.OnStateUpdate();

            if (_timer > 0)
            {
                _timer -= _timeService.DeltaTime;

                if (_timer <= 0f)
                {
                    _config.nextState.Execute(_stateFactory);
                }
            }
        }
    }
}
