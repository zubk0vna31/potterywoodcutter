using Modules.Root.ContainerComponentModel;
using UniRx;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;
using PWS.Common.Messages;
using UnityEngine.Events;

namespace PWS.Common.QuickMenu
{
    public class QuickMenu : MonoBehaviour
    {
        [SerializeField] private InputActionReference _openAction;

        [Header("View")]
        [SerializeField] private GameObject _canvas;
        [SerializeField] private Button _restartCurrentButton;
        [SerializeField] private Button _restartFirstButton;
        [SerializeField] private Button _returnToHubButton;
        [SerializeField] private Button _resetUserButton;

        [SerializeField] private Slider _musicVolumeSlider;
        [SerializeField] private Slider _voiceVolumeSlider;

        [SerializeField] public UnityEvent OnMenuOpenedEvent;
        [SerializeField] public UnityEvent OnMenuClosedEvent;

        private bool _active;

        private void Awake()
        {
            AppContainer.Inject(this);
        }

        // Start is called before the first frame update
        void Start()
        {
            Init();
            Hide();
            _openAction.action.performed += OpenAction_performed;
        }

        void Init()
        {
            _restartCurrentButton.onClick.AddListener(OnClickRestartCurrent);
            _restartFirstButton.onClick.AddListener(OnClickRestartFirst);
            _returnToHubButton.onClick.AddListener(OnClickToHub);
            _resetUserButton.onClick.AddListener(OnClickResetUser);

            _musicVolumeSlider.onValueChanged.AddListener(OnValueChangedMusicVolume);
            _voiceVolumeSlider.onValueChanged.AddListener(OnValueChangedVoiceVolume);
        }

        private void OpenAction_performed(InputAction.CallbackContext obj)
        {
            if (_active)
            {
                OnMenuClosedEvent.Invoke();
                Hide();
            }
            else
            {
                OnMenuOpenedEvent.Invoke();
                Show();
            }
        }

        void OnClickRestartCurrent()
        {
            MessageBroker.Default.Publish(new RestartCurrentStepMessage());
        }
        void OnClickRestartFirst()
        {
            MessageBroker.Default.Publish(new RestartFirstStepMessage());
        }
        void OnClickToHub()
        {
            MessageBroker.Default.Publish(new ReturnToHubMessage());
        }
        void OnClickResetUser()
        {
            MessageBroker.Default.Publish(new ResetUserMessage());
        }

        void OnValueChangedMusicVolume(float value)
        {

        }

        void OnValueChangedVoiceVolume(float value)
        {

        }

        public void Show()
        {
            _active = true;
            _canvas.SetActive(true);
        }

        public void Hide()
        {
            _active = false;
            _canvas.SetActive(false);
        }

        private void OnDestroy()
        {
            _openAction.action.performed -= OpenAction_performed;
            _restartCurrentButton.onClick.RemoveListener(OnClickRestartCurrent);
            _restartFirstButton.onClick.RemoveListener(OnClickRestartFirst);
            _returnToHubButton.onClick.RemoveListener(OnClickToHub);
            _resetUserButton.onClick.RemoveListener(OnClickResetUser);

            _musicVolumeSlider.onValueChanged.RemoveListener(OnValueChangedMusicVolume);
            _voiceVolumeSlider.onValueChanged.RemoveListener(OnValueChangedVoiceVolume);
        }
    }
}
