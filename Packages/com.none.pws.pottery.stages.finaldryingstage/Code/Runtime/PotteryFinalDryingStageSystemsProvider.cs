using Leopotam.Ecs;
using Modules.Root.ContainerComponentModel;
using Modules.Root.ECS;
using Modules.StateGroup.Core;
using Modules.VRFeatures;
using PWS.Common.Audio;
using PWS.Common.Messages;
using PWS.Common.RestartStage;
using PWS.Common.ResultsEvaluation;
using PWS.Common.SceneSwitchService;
using PWS.Common.TravelPoints;
using PWS.Common.UI;
using PWS.Common.ValueRegulator;
using PWS.Pottery.Common;
using PWS.Pottery.ItemDataHolderService;
using PWS.Pottery.MeshGeneration;
using PWS.Pottery.Stages.InitialDryingStage;
using UnityEngine;

namespace PWS.Pottery.Stages.FinalDryingStage
{
    public class PotteryFinalDryingStageSystemsProvider : MonoBehaviour, ISystemsProvider
    {
        internal class SystemsDependencies
        {
            [Inject] public IValueRegulatorDataHolder<DryingRegulator> DryingData;
            [Inject] public IResultsEvaluationDataHolder ResultsEvaluationData;
            [Inject] public ISceneSwitchService SceneSwitchService;
        }

        [SerializeField] private SetStateSOCommand _nextState;
        [SerializeField] private DryingStateConfig _config;
        [SerializeField] private ValueRegulatorConfig _regulatorConfig;
        [SerializeField] private StateInfoData _stateInfo;
        
        [Header("Voice config")] 
        [SerializeField] private VoiceConfig _voiceConfig;

        public EcsSystems GetSystems(EcsWorld world, EcsSystems endFrame, EcsSystems mainSystems)
        {
            SystemsDependencies dependencies = new SystemsDependencies();
            SceneContainer.Instance.Inject(dependencies);

            EcsSystems systems = new EcsSystems(world);
            systems
                .Add(new FadeToStateOnUIActionSystem<FinalDryingState>(_nextState, dependencies.SceneSwitchService))
                .Add(new CurrentStateRestoreDataProcessing<FinalDryingState, ValueRegulatorDataHolder<DryingRegulator>>(dependencies.DryingData))

                //state systems
                .Add(new SnapSocketActivityProcessing<FinalDryingState, InitialDryingSnapSocket>())
                .Add(new GameObjectVisibilityProcessing<FinalDryingState, DryingContainer>())

                .Add(new GameObjectVisibilityProcessing<FinalDryingState, DryingRegulator>(true))
                .Add(new ValueRegulatorSetupProcessing<FinalDryingState, DryingRegulator>(_regulatorConfig, dependencies.DryingData, true))

                .Add(new DryingCompletionTracker<FinalDryingState>(_regulatorConfig, dependencies.DryingData))
                .Add(new DryingDrynessProcessing<FinalDryingState>(_config))

                //Outline
                .Add(new OutlineByStateSystem<FinalDryingState>(0))

                .Add(new SetGradeByValueRegulator<FinalDryingState, DryingRegulator>(dependencies.ResultsEvaluationData,
                dependencies.DryingData, _stateInfo, _config.TargetDays))

                //ui
                .Add(new StateInfoWindowUIProcessing<FinalDryingState>(_stateInfo))
                .Add(new StateWindowPlacementProcessing<FinalDryingState>("Drying"))
                .Add(new StateWindowButtonUIProcessing<FinalDryingState>())

                .Add(new XRInteractableProcessing<FinalDryingState, SculptingProductTag>())

                .Add(new MeshVisibilityProcessing<FinalDryingState, SculptingProductTag>())
                
                //voice
                .Add(new VoiceAudioSystem<FinalDryingState>(_voiceConfig))

                //travel point
                .Add(new TravelPointPositioningSystem<FinalDryingState>("Shelf"))

                ;

            endFrame
                //drying
                .Add(new ValueRegulatorUpdateProcessing<FinalDryingState, DryingRegulator>(dependencies.DryingData))

                .OneFrame<RestartCurrentStepMessage>()
                .OneFrame<NextStateSignal>()
                .OneFrame<ValueRegulatorChangedSignal>()
                ;

            return systems;
        }
    }
}
