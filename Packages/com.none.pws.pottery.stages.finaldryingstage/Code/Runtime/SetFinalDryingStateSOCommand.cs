using Modules.StateGroup.Core;
using UnityEngine;

namespace PWS.Pottery.Stages.FinalDryingStage
{
    [CreateAssetMenu(fileName = "SetFinalDryingStateSOCommand", menuName = "PWS/Pottery/Stages/FinalDryingStage/SetStateSOCommand")]
    public class SetFinalDryingStateSOCommand : SetStateSOCommand<FinalDryingState>
    {

    }
}