using Leopotam.Ecs;
using Modules.ViewHub;
using PWS.Pottery.ItemDataHolderService;

namespace PWS.Pottery.WheelSpeedSetupStage
{
    public class WheelSpeedSetupView : ViewComponent
    {
        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            ecsEntity.Get<WheelSpeedRegulator>();
        }
    }
}
