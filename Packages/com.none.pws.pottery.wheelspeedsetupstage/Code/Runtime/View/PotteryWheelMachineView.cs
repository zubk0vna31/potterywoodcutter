using Leopotam.Ecs;
using Modules.ViewHub;
using PWS.Common.ValueRegulator;
using PWS.Pottery.ItemDataHolderService;
using UnityEngine;

namespace PWS.Pottery.WheelSpeedSetupStage
{
    public class PotteryWheelMachineView : ViewComponent, IValueRegulatorVizualizableView
    {
        [SerializeField] private Transform _wheelTr;
        private float _rpm = 0;

        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            ecsEntity.Get<ValueRegulatorVisualizable<WheelSpeedRegulator>>().View = this;
        }

        public void UpdateValue(float value)
        {
            _rpm = value;
        }

        void Update()
        {
            if (_rpm == 0) return;

            _wheelTr.Rotate(_wheelTr.up, _rpm * 360 * Time.deltaTime / 60);
        }
    }
}
