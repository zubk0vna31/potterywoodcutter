using Leopotam.Ecs;
using Modules.ViewHub;
using PWS.Common.ValueRegulator;
using PWS.Pottery.ItemDataHolderService;
using UnityEngine;

namespace PWS.Pottery.WheelSpeedSetupStage
{
    public class PotteryWheelMachineUvView : ViewComponent, IValueRegulatorVizualizableView
    {
        [SerializeField]
        private MeshRenderer _meshRenderer;

        private float _rpm = 0;
        private float _textureOffset;

        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            ecsEntity.Get<ValueRegulatorVisualizable<WheelSpeedRegulator>>().View = this;
        }

        public void UpdateValue(float value)
        {
            _rpm = value;
        }

        void Update()
        {
            if (_rpm == 0) return;

            _textureOffset -= _rpm * Time.deltaTime / 60;
            _textureOffset = Mathf.Repeat(_textureOffset, 1);
            _meshRenderer.sharedMaterial.SetTextureOffset("_MainTex", Vector2.right * _textureOffset);
        }
    }
}
