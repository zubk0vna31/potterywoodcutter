using Modules.StateGroup.Core;
using UnityEngine;

namespace PWS.Pottery.WheelSpeedSetupStage
{
    [CreateAssetMenu(fileName = "SetWheelSpeedSetupStateSOCommand", menuName = "PWS/Pottery/Stages/WheelSpeedSetupStage/SetStateSOCommand")]
    public class SetWheelSpeedSetupStateSOCommand : SetStateSOCommand<WheelSpeedSetupClayFallState>
    {
        
    }
}