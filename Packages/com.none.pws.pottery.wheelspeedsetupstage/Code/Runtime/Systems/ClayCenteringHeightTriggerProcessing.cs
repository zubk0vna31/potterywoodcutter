using Leopotam.Ecs;
using Modules.StateGroup.Components;
using Modules.StateGroup.Core;
using Modules.ViewHub;
using Modules.VRFeatures;
using PWS.Pottery.MeshGeneration;
using PWS.Pottery.Stages.ClayCenteringStage;
using UnityEngine;

namespace PWS.Pottery.WheelSpeedSetupStage
{
    public class ClayCenteringHeightTriggerProcessing : IEcsRunSystem
    {
        // auto injected fields
        //readonly EcsFilter<StateEnter, WheelSpeedSetupClayFallState> _entering;
        readonly EcsFilter<WheelSpeedSetupClayFallState>.Exclude<StateExit> _inState;

        readonly EcsFilter<UnityView, XRInteractableComponents, ClayCenteringSphere> _sphere;
        readonly EcsFilter<UnityView, SculptingProductTag> _product;
        //readonly EcsFilter<UnityView, ClayCenteringContainer> _container;

        private StateFactory _stateFactory;

        public void Run()
        {
            if (!_inState.IsEmpty())
            {
                foreach (var sphere in _sphere)
                {
                    if (!_sphere.Get2(sphere).View.IsSelected && !_sphere.Get2(sphere).View.IsKinematic)
                    {
                        if(_sphere.Get1(sphere).Transform.localPosition.y <= 0.05f)
                        {
                            _stateFactory.SetState<WheelSpeedSetupState>();
                            Vector3 localPos = _sphere.Get1(sphere).Transform.localPosition;
                            localPos.y = 0;
                            _sphere.Get1(sphere).Transform.localPosition = localPos;
                            foreach (var product in _product)
                            {
                                _product.Get1(product).Transform.position = _sphere.Get1(sphere).Transform.position;
                            }
                        }
                    }
                }
            }
        }
    }
}
