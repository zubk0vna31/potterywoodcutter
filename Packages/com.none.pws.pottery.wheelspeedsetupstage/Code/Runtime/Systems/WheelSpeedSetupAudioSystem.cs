﻿using Leopotam.Ecs;
using Modules.Utils;
using PWS.Common.ValueRegulator;
using PWS.Pottery.ItemDataHolderService;
using Modules.Audio;
using UnityEngine;

namespace PWS.Pottery.WheelSpeedSetupStage
{
    public class WheelSpeedSetupAudioSystem : IEcsRunSystem
    {
        private readonly EcsFilter<ValueRegulatorChangedSignal, WheelSpeedRegulator> _changedSignal;
        private readonly EcsFilter<WheelSpeedRegulator> _regulator;
        private readonly EcsFilter<WheelAudioPlaingTag> _playingTag;
        private readonly EcsFilter<AudioSourceRef> _audioSourceRef;
        private readonly TimeService _timeService;
        
        private const float WHEEL_SPEED_ACCELERATION = 0.1f;
        private const float WHEEL_SPEED_SLOWDOWN = 0.2f;
        private const float MIN_PITCH = 1f;
        private const float MAX_PITCH = 1.5f;
        private const float MIN_VOLUME = 0.1f;
        private const float MAX_VOLUME = 0.2f;
        
        private OneShotSoundTemplate _soundPrefab;
        private AudioSourceRef _wheelLoopAudioSourceRef;
        private float _signalPitchValue;
        private float _signalVolumeValue;
        
        private OneShotSoundTemplate _wheelOn;
        private OneShotSoundTemplate _wheelLoop;
        private OneShotSoundTemplate _wheelOff;
        private Transform _wheelPosition;

        private IValueRegulatorDataHolder<WheelSpeedRegulator> _wheelSpeedData;

        public WheelSpeedSetupAudioSystem(
            OneShotSoundTemplate wheelOn,
            OneShotSoundTemplate wheelLoop,
            OneShotSoundTemplate wheelOff,
            Transform wheelPosition,
            IValueRegulatorDataHolder<WheelSpeedRegulator> wheelSpeedData
            )
        {
            _wheelOn = wheelOn;
            _wheelLoop = wheelLoop;
            _wheelOff = wheelOff;
            _wheelPosition = wheelPosition;
            _wheelSpeedData = wheelSpeedData;
        }
        
        public void Run()
        {
            
            if (_wheelLoopAudioSourceRef.AudioSource != null)
            {
                if (_wheelLoopAudioSourceRef.AudioSource.volume < _signalVolumeValue)
                {
                    _wheelLoopAudioSourceRef.AudioSource.volume += _timeService.DeltaTime * WHEEL_SPEED_ACCELERATION;
                }

                if (_wheelLoopAudioSourceRef.AudioSource.volume > _signalVolumeValue)
                {
                    _wheelLoopAudioSourceRef.AudioSource.volume -= _timeService.DeltaTime * WHEEL_SPEED_SLOWDOWN;
                }
                
                if (_wheelLoopAudioSourceRef.AudioSource.pitch < _signalPitchValue)
                {
                    _wheelLoopAudioSourceRef.AudioSource.pitch += _timeService.DeltaTime * WHEEL_SPEED_ACCELERATION;
                }

                if (_wheelLoopAudioSourceRef.AudioSource.pitch > _signalPitchValue)
                {
                    _wheelLoopAudioSourceRef.AudioSource.pitch -= _timeService.DeltaTime * WHEEL_SPEED_SLOWDOWN;
                }
            }
            
            if (_changedSignal.IsEmpty() && !_wheelSpeedData.IsUpdated)
                return;
            
            foreach (var regulator in _regulator)
            {
                var value =_wheelSpeedData.Value;
                _signalPitchValue = Mathf.Lerp(MIN_PITCH, MAX_PITCH, value);
                _signalVolumeValue = Mathf.Lerp(MIN_VOLUME, MAX_VOLUME, value);
                if (_playingTag.IsEmpty() && value == 0f)
                {
                    return;
                }
                
                if (_playingTag.IsEmpty() && value > 0f)
                {
                    _soundPrefab = _wheelOn;
                    _regulator.GetEntity(regulator).Get<WheelAudioPlaingTag>();
                }
                else if (!_playingTag.IsEmpty() && value <= 0f)
                {
                    foreach (var playingTag in _playingTag)
                    {
                        _playingTag.GetEntity(playingTag).Del<WheelAudioPlaingTag>();
                    }
                    if (_wheelLoopAudioSourceRef.AudioSource != null)
                        _wheelLoopAudioSourceRef.AudioSource.Stop();
                    
                    _soundPrefab = _wheelOff;
                }
                else if (!_playingTag.IsEmpty() && value > 0f)
                {
                    if (!_audioSourceRef.IsEmpty())
                    {
                        foreach (var sourceRef in _audioSourceRef)
                        {
                            ref var entity = ref _audioSourceRef.Get1(sourceRef);
                            if (entity.Transform.name.Contains(_wheelOn.gameObject.name)) //TODO replace string comparison with component
                            {
                                if (entity.AudioSource.isPlaying)
                                    return;
                            }
                            
                            if (entity.Transform.name.Contains(_wheelLoop.gameObject.name)) //TODO replace string comparison with component
                            {
                                _wheelLoopAudioSourceRef = entity;
                                if (entity.AudioSource.isPlaying)
                                {
                                    if (entity.AudioSource.loop == false)
                                        entity.AudioSource.loop = true;

                                    return;
                                }

                                entity.AudioSource.Play();
                                entity.AudioSource.loop = true;
                                return;
                            }
                        }

                        _soundPrefab = _wheelLoop;
                    }
                }

                ref var playSound = ref _regulator.GetEntity(regulator).Get<PlaySoundAtPointSignal>();
                playSound.PlayPosition = _wheelPosition.position;
                playSound.SoundPrefab = _soundPrefab;
            }
        }
    }
}