﻿
using Leopotam.Ecs;
using PWS.Common.UI;
using Modules.StateGroup.Components;
using PWS.Common.ValueRegulator;
using PWS.Pottery.ItemDataHolderService;

namespace PWS.Pottery.WheelSpeedSetupStage
{
    public class WheelSpeedSetupCompletionTracker : IEcsRunSystem
    {

        // auto injected fields
        readonly EcsFilter<StateEnter, WheelSpeedSetupState> _entering;
        readonly EcsFilter<WheelSpeedSetupState> _inState;
        readonly EcsFilter<StateWindow, StateProgress> _stateWindowWithProgress;

        private ValueRegulatorConfig _config;
        private IValueRegulatorDataHolder<WheelSpeedRegulator> _data;

        public WheelSpeedSetupCompletionTracker(ValueRegulatorConfig config, IValueRegulatorDataHolder<WheelSpeedRegulator> data)
        {
            _config = config;
            _data = data;

        }

        public void Run()
        {
            if (!_entering.IsEmpty())
            {

                foreach (var i in _stateWindowWithProgress)
                {
                    _stateWindowWithProgress.Get2(i).Value = 0;
                    _stateWindowWithProgress.Get2(i).CompletionThreshold = _data.ConvertFromCustom(_config.CompletionCustomValueMin);
                }
            }

            if (!_inState.IsEmpty())
            {
                foreach (var i in _stateWindowWithProgress)
                {
                    _stateWindowWithProgress.Get2(i).Value = _data.Value;
                }
            }
        }
    }
}