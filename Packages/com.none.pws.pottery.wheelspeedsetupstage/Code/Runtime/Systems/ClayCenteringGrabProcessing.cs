using Leopotam.Ecs;
using Modules.StateGroup.Components;
using Modules.ViewHub;
using Modules.VRFeatures;
using PWS.Pottery.Stages.ClayCenteringStage;
using UnityEngine;

namespace PWS.Pottery.WheelSpeedSetupStage
{
    public class ClayCenteringGrabProcessing : IEcsRunSystem
    {
        // auto injected fields
        readonly EcsFilter<StateEnter, WheelSpeedSetupClayFallState> _entering;
        readonly EcsFilter<WheelSpeedSetupClayFallState> _inState;

        readonly EcsFilter<UnityView, XRInteractableComponents, ClayCenteringSphere> _sphere;
        readonly EcsFilter<UnityView, ClayCenteringContainer> _container;

        private ClayCenteringStateConfig _config;

        //runtime data
        private float _height;

        public ClayCenteringGrabProcessing(ClayCenteringStateConfig config)
        {
            _config = config;
        }

        public void Run()
        {
            if (!_entering.IsEmpty())
            {
                foreach (var sphere in _sphere)
                {
                    _height = _sphere.Get1(sphere).Transform.localPosition.y;
                }
            }
            if (!_inState.IsEmpty())
            {
                foreach (var sphere in _sphere)
                {
                    foreach (var container in _container)
                    {
                        Vector3 localPos = _container.Get1(container).Transform.InverseTransformPoint(_sphere.Get1(sphere).Transform.position);
                        float height = localPos.y;
                        localPos.y = 0;
                        localPos = Vector3.ClampMagnitude(localPos, _config.WheelRadius);
                        localPos.y = height;

                        if (_sphere.Get2(sphere).View.IsKinematic && _sphere.Get2(sphere).View.IsSelected)
                        {
                            localPos.y = _height;
                        }
                        _sphere.Get1(sphere).Transform.position = _container.Get1(container).Transform.TransformPoint(localPos);
                    }
                }
            }
        }
    }
}
