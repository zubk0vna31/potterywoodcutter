using Leopotam.Ecs;
using Modules.StateGroup.Components;
using Modules.ViewHub;
using PWS.Pottery.Stages.ClayCenteringStage;
using UnityEngine;

namespace PWS.Pottery.WheelSpeedSetupStage
{
    public class ClayCenteringInitProcessing : IEcsInitSystem, IEcsRunSystem
    {
        // auto injected fields
        readonly EcsFilter<StateEnter, WheelSpeedSetupClayFallState> _entering;
        readonly EcsFilter<UnityView, ClayCenteringSphere> _sphere;

        //runtime data
        private Vector3 _initLocalPos;
        
        public void Init()
        {
            foreach (var idx in _sphere)
            {
                _initLocalPos = _sphere.Get1(idx).Transform.localPosition;
            }
        }

        public void Run()
        {
            if (!_entering.IsEmpty())
            {
                foreach (var idx in _sphere)
                {
                    _sphere.Get1(idx).Transform.localPosition = _initLocalPos;
                    _sphere.Get1(idx).Transform.localRotation = Quaternion.identity;
                }
            }
        }
    }
}