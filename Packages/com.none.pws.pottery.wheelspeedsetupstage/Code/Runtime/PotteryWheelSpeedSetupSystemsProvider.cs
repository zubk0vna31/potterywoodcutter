using UnityEngine;
using Modules.Root.ECS;
using Leopotam.Ecs;
using Modules.StateGroup.Core;
using PWS.Common.UI;
using PWS.Common.ValueRegulator;
using Modules.Root.ContainerComponentModel;
using Modules.Audio;
using PWS.Common.Audio;
using PWS.Common.RestartStage;
using PWS.Pottery.ItemDataHolderService;
using PWS.Pottery.Common;
using PWS.Common.Messages;
using PWS.Common.ResultsEvaluation;
using Modules.VRFeatures;
using PWS.Pottery.Stages.ClayCenteringStage;
using PWS.Pottery.MeshGeneration;
using PWS.Common.TravelPoints;

namespace PWS.Pottery.WheelSpeedSetupStage
{
    public class PotteryWheelSpeedSetupSystemsProvider : MonoBehaviour, ISystemsProvider
    {
        internal class SystemsDependencies
        {
            [Inject] public ISculptingDataHolder SculptingData;
            [Inject] public IValueRegulatorDataHolder<WheelSpeedRegulator> WheelSpeedData;
            [Inject] public IResultsEvaluationDataHolder ResultsEvaluationData;
        }
        
        [SerializeField] private SetStateSOCommand _nextState;
        [SerializeField] private ValueRegulatorConfig _wheelSpeedConfig;
        [SerializeField] private ClayCenteringStateConfig _config;
        [SerializeField] private StateInfoData _centeringStateInfo;
        [SerializeField] private StateInfoData _stateInfo;

        [Header("Wheel Audio Sources")] 
        [SerializeField] private OneShotSoundTemplate _wheelOn;
        [SerializeField] private OneShotSoundTemplate _wheelLoop;
        [SerializeField] private OneShotSoundTemplate _wheelOff;
        [Space] 
        [SerializeField] private Transform _wheelPosition;

        [Header("Voice config")]
        [SerializeField] private VoiceConfig _voiceFallConfig;
        [SerializeField] private VoiceConfig _voiceConfig;

        public EcsSystems GetSystems(EcsWorld world, EcsSystems endFrame, EcsSystems mainSystems)
        {
            SystemsDependencies dependencies = new SystemsDependencies();
            SceneContainer.Instance.Inject(dependencies);

            EcsSystems systems = new EcsSystems(world);
            systems
                .Add(new TransitToStateOnUIActionSystem<WheelSpeedSetupState>(_nextState))
                .Add(new CurrentSubStateRestoreDataProcessing<WheelSpeedSetupClayFallState,
                ValueRegulatorDataHolder<WheelSpeedRegulator>, WheelSpeedSetupState>(dependencies.WheelSpeedData))

                //fall state systems
                .Add(new GameObjectVisibilityProcessing<WheelSpeedSetupClayFallState, ClayCenteringContainer>())

                .Add(new ClayCenteringInitProcessing())
                .Add(new ClayCenteringGrabProcessing(_config))
                .Add(new ClayCenteringHeightTriggerProcessing())

                .Add(new GameObjectVisibilityProcessing<WheelSpeedSetupClayFallState, ClayCenteringSphere>())

                //ui
                .Add(new StateInfoWindowUIProcessing<WheelSpeedSetupClayFallState>(_centeringStateInfo))

                .Add(new XRInteractableProcessing<WheelSpeedSetupClayFallState, ClayCenteringSphere>())

                //wheel speed setup state systems
                .Add(new GameObjectVisibilityProcessing<WheelSpeedSetupState, WheelSpeedRegulator>())
                .Add(new ValueRegulatorSetupProcessing<WheelSpeedSetupState, WheelSpeedRegulator>(_wheelSpeedConfig, dependencies.WheelSpeedData))
                .Add(new WheelSpeedSetupCompletionTracker(_wheelSpeedConfig, dependencies.WheelSpeedData))

                .Add(new ClayCenteringRotationProcessing<WheelSpeedSetupState>(dependencies.WheelSpeedData))

                .Add(new SetGradeByValueRegulator<WheelSpeedSetupState, WheelSpeedRegulator>(dependencies.ResultsEvaluationData,
                dependencies.WheelSpeedData, _stateInfo, _wheelSpeedConfig.CompletionCustomValueMin, _wheelSpeedConfig.CompletionCustomValueMax))

                .Add(new StateWindowPlacementProcessing<WheelSpeedSetupState>("Wheel"))
                .Add(new StateInfoWindowUIProcessing<WheelSpeedSetupState>(_stateInfo))
                .Add(new StateWindowProgressUIProcessing<WheelSpeedSetupState>())
                .Add(new StateWindowButtonUIProcessing<WheelSpeedSetupState>())

                .Add(new MeshVisibilityProcessing<WheelSpeedSetupState, SculptingProductMesherTag>())

                //audio systems
                .Add(new WheelSpeedSetupAudioSystem(_wheelOn, _wheelLoop, _wheelOff, _wheelPosition,
                dependencies.WheelSpeedData))

                //voice systems
                .Add(new VoiceAudioSystem<WheelSpeedSetupClayFallState>(_voiceFallConfig))
                .Add(new VoiceAudioSystem<WheelSpeedSetupState>(_voiceConfig))

                //travel point
                .Add(new TravelPointPositioningSystem<WheelSpeedSetupClayFallState>("Wheel"))
                .Add(new TravelPointPositioningSystem<WheelSpeedSetupState>("Wheel"))

                ;

            endFrame
                .Add(new SculptingDataProcessingSystem<WheelSpeedSetupState>(dependencies.SculptingData))
                .Add(new MeshRingsDataProcessingSystem<WheelSpeedSetupState>(dependencies.SculptingData))
                .Add(new MeshUpdateSystem<WheelSpeedSetupState, SculptingProductMesherTag>(dependencies.SculptingData))

                //wheel Speed
                .Add(new ValueRegulatorUpdateProcessing<WheelSpeedSetupState, WheelSpeedRegulator>(dependencies.WheelSpeedData))

                .OneFrame<RestartCurrentStepMessage>()
                .OneFrame<NextStateSignal>()
                .OneFrame<ValueRegulatorChangedSignal>()
                ;

            return systems;
        }
    }
}
