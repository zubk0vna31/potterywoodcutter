﻿using Leopotam.Ecs;
using Modules.Root.ECS;
using PWS.Tutorial;
using UnityEngine;

namespace PWS.Common.CustomHands
{
    public class CustomHandsSystemsProvider : MonoBehaviour, ISystemsProvider
    {
        public EcsSystems GetSystems(EcsWorld world, EcsSystems endFrame, EcsSystems mainSystems)
        {
            EcsSystems systems = new EcsSystems(world);
            
            systems
                .Add(new CustomHandsSystem())
                .Add(new ActivateInstrumentHandler())
                ;

            endFrame
                .OneFrame<InteractableSelectEnteredSignal>()
                .OneFrame<InteractableSelectExitedSignal>()
                .OneFrame<InstrumentRunSignal>()
                .OneFrame<InstrumentStopSignal>()
                ;
            return systems;
        }
    }
}