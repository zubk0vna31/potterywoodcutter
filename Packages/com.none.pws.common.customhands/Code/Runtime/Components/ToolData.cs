﻿using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace PWS.Common.CustomHands
{
    public struct ToolData
    {
        public bool BothHands;
        public bool DoNotDisableInteractorsSelecting;
        public XRBaseInteractable Interactable;
        public GameObject AttachLeftHand;
        public GameObject AttachRightHand;
        public int ID;
    }
}