﻿using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.XR.Interaction.Toolkit;

namespace PWS.Common.CustomHands
{
    public struct HandData
    {
        public Hand Hand;
        public GameObject SelfModelParent;
        public GameObject OtherModelParent;
        public XRBaseInteractor Interactor;
        public Animator Animator;
        public InputActionReference PrimeryButton;
        public InputActionReference Grid;
        public InputActionReference Teleportation;
        public int InteractbleID;
        public bool InteractableSelected;
    }
}