﻿using Leopotam.Ecs;
using Modules.ViewHub;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.XR.Interaction.Toolkit;

namespace PWS.Common.CustomHands
{
    public enum Hand
    {
        Left,
        Right
    }
    public class CustomHandView : ViewComponent
    {
        private EcsEntity _entity;
        
        [SerializeField] private Hand _hand;
        [SerializeField] private GameObject _selfModelParent;
        [SerializeField] private GameObject _otherModelParent;
        [SerializeField] private XRBaseInteractor _interactor;
        [SerializeField] private InputActionReference _primeryButton;
        [SerializeField] private InputActionReference _grid;
        [SerializeField] private InputActionReference _teleportation;
        private Animator _animator;
        
        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            _entity = ecsEntity;
            ref var entity = ref ecsEntity.Get<HandData>();
            entity.Hand = _hand;
            entity.SelfModelParent = _selfModelParent;
            entity.OtherModelParent = _otherModelParent;
            entity.Interactor = _interactor;
            entity.Animator = _animator;
            entity.Grid = _grid;
            entity.PrimeryButton = _primeryButton;
            entity.Teleportation = _teleportation;

            _interactor.selectEntered.AddListener(arg =>
            {
                _entity.Get<InteractableSelectEnteredSignal>().InteractableID = _interactor.selectTarget.transform.GetInstanceID();
                _entity.Get<HandData>().InteractableSelected = true;
            });
            _interactor.selectExited.AddListener(arg =>
            {
                _entity.Get<InteractableSelectExitedSignal>();
                _entity.Get<HandData>().InteractableSelected = false;
            });

            ecsEntity.Get<PWS.Common.ResultsSubmission.ResultCapture.ExcludedFromScreenshot>().RenderersParent =
                _selfModelParent;
        }

        private void Start()
        {
            _primeryButton.action.started += ActionForFinger_started;
            _teleportation.action.started += ActionForFinger_started;
            _grid.action.started += ActionGrid_started;
            _primeryButton.action.canceled += ActionForFinger_canceled;
            _teleportation.action.canceled += ActionForFinger_canceled;
            _grid.action.canceled += ActionGrid_canceled;
        }
        
        private void ActionForFinger_started(InputAction.CallbackContext obj)
        {
            ref var entity = ref _entity.Get<HandData>();
            if (entity.Animator == null)
            {
                entity.Animator = _selfModelParent.GetComponentInChildren<Animator>();
            }
            
            entity.Animator.SetBool("ForFinger", true);
        }
        
        private void ActionForFinger_canceled(InputAction.CallbackContext obj)
        {
            if (_entity.IsWorldAlive())
                _entity.Get<HandData>().Animator.SetBool("ForFinger", false);
        }
        
        private void ActionGrid_started(InputAction.CallbackContext obj)
        {
            ref var entity = ref _entity.Get<HandData>();
            if (entity.Animator == null)
            {
                entity.Animator = _selfModelParent.GetComponentInChildren<Animator>();
            }
            
            entity.Animator.SetBool("Line", true);
        }

        private void ActionGrid_canceled(InputAction.CallbackContext obj)
        {
            if (_entity.IsWorldAlive())
                _entity.Get<HandData>().Animator.SetBool("Line", false);
        }

        private void OnDestroy()
        {
            _primeryButton.action.started -= ActionForFinger_started;
            _teleportation.action.started -= ActionForFinger_started;
            _grid.action.started -= ActionGrid_started;
            _primeryButton.action.canceled -= ActionForFinger_canceled;
            _teleportation.action.canceled -= ActionForFinger_canceled;
            _grid.action.canceled -= ActionGrid_canceled;
        }
    }
}