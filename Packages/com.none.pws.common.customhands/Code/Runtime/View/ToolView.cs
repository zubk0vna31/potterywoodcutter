﻿using Leopotam.Ecs;
using Modules.ViewHub;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace PWS.Common.CustomHands
{
    public class ToolView : ViewComponent
    {
        [SerializeField] private bool _bothHands;
        [SerializeField] private bool _doNotDisableInteractorsSelecting;
        [SerializeField] private XRBaseInteractable _interactable;
        [SerializeField] private GameObject _attachLeftHand;
        [SerializeField] private GameObject _attachRightHand;
        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            ref var entity = ref ecsEntity.Get<ToolData>();
            entity.BothHands = _bothHands;
            entity.DoNotDisableInteractorsSelecting = _doNotDisableInteractorsSelecting;
            entity.Interactable = _interactable;
            entity.AttachLeftHand = _attachLeftHand;
            entity.AttachRightHand = _attachRightHand;
            entity.ID = transform.GetInstanceID();
        }
    }
}