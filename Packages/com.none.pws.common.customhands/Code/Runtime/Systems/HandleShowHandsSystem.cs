﻿using Leopotam.Ecs;
using Modules.ViewHub;
using PWS.Pottery.MeshGeneration;
using PWS.Pottery.Stages.HandlePlacingStage;

namespace PWS.Common.CustomHands
{
    public class HandleShowHandsSystem : IEcsRunSystem
    {
        private readonly EcsFilter<HandlePlacingInteractable> _handle;
        private readonly EcsFilter<UnityView, SculptingProductTag> _product;
        private readonly EcsFilter<HandData, InteractableSelectEnteredSignal> _enteredSignal;
        private readonly EcsFilter<HandData, InteractableSelectExitedSignal> _exitedSignal;
        public void Run()
        {
            if (!_enteredSignal.IsEmpty())
            {
                foreach (var entered in _enteredSignal)
                {
                    foreach (var handle in _handle)
                    {
                        foreach (var product in _product)
                        {
                            ref var interactor = ref _enteredSignal.Get1(entered);
                            var productID = _product.Get1(product).Transform.GetInstanceID();
                            if (interactor.Interactor.selectTarget.transform.GetInstanceID() == productID)
                            {
                                interactor.InteractbleID = productID;
                                
                                if (interactor.Hand == Hand.Left)
                                {
                                    _handle.Get1(handle).View.LeftHandModel.gameObject.SetActive(true);
                                    interactor.SelfModelParent.gameObject.SetActive(false);
                                }
                                else
                                {
                                    _handle.Get1(handle).View.RightHandModel.gameObject.SetActive(true);
                                    interactor.SelfModelParent.gameObject.SetActive(false);
                                }
                            }
                        }
                    }
                }
            }

            if (!_exitedSignal.IsEmpty())
            {
                foreach (var exited in _exitedSignal)
                {
                    foreach (var handle in _handle)
                    {
                        foreach (var product in _product)
                        {
                            ref var interactor = ref _exitedSignal.Get1(exited);
                            
                            if (interactor.InteractbleID == _product.Get1(product).Transform.GetInstanceID())
                            {
                                ref var activeHandle = ref _handle.Get1(handle);
                                activeHandle.View.LeftHandModel.gameObject.SetActive(false);
                                activeHandle.View.RightHandModel.gameObject.SetActive(false);
                                interactor.SelfModelParent.gameObject.SetActive(true);
                            }
                        }
                    }
                }
            }
        }
    }
}