﻿using Leopotam.Ecs;

namespace PWS.Common.CustomHands
{
    public class CustomHandsSystem : IEcsRunSystem
    {
        private readonly EcsFilter<ToolData> _tools;
        private readonly EcsFilter<HandData, InteractableSelectEnteredSignal> _enteredSignal;
        private readonly EcsFilter<HandData, InteractableSelectExitedSignal> _exitedSignal;
        private readonly EcsFilter<HandData> _interactors;


        public void Run()
        {
            foreach (var entered in _enteredSignal)
            {
                ref var hand = ref _enteredSignal.Get1(entered);
                foreach (var tool in _tools)
                {
                    ref var toolData = ref _tools.Get1(tool);
                    if (_enteredSignal.Get2(entered).InteractableID == toolData.ID)
                    {
                        hand.InteractbleID = toolData.ID;
                        if (!toolData.DoNotDisableInteractorsSelecting)
                        {
                            if (_enteredSignal.GetEntitiesCount() > 1)
                            {
                                foreach (var idx in _interactors)
                                {
                                    if (_interactors.Get1(idx).Hand == Hand.Left)
                                    {
                                        _interactors.Get1(idx).Interactor.allowSelect = false;
                                    }
                                }
                            }
                            else
                            {
                                foreach (var idx in _interactors)
                                {
                                    if (_interactors.Get1(idx).Interactor.selectTarget == null)
                                    {
                                        _interactors.Get1(idx).Interactor.allowSelect = false;
                                    }
                                }
                            }
                        }

                        if (hand.Hand == Hand.Left)
                        {
                            toolData.AttachLeftHand.SetActive(true);
                        }
                        else
                        {
                            toolData.AttachRightHand.SetActive(true);
                        }

                        SetInteractorInvisible(toolData, hand);
                    }
                }
            }

            foreach (var exited in _exitedSignal)
            {
                ref var hand = ref _exitedSignal.Get1(exited);
                foreach (var tool in _tools)
                {
                    ref var toolData = ref _tools.Get1(tool);
                    if (hand.InteractbleID == toolData.ID)
                    {
                        if (!toolData.DoNotDisableInteractorsSelecting)
                        {
                            var selected = false;
                            foreach (var idx in _interactors)
                            {
                                selected |= _interactors.Get1(idx).InteractableSelected;
                            }

                            if (!selected)
                            {
                                foreach (var idx in _interactors)
                                {
                                    _interactors.Get1(idx).Interactor.allowSelect = true;
                                }

                                toolData.AttachLeftHand.SetActive(false);
                                toolData.AttachRightHand.SetActive(false);
                                hand.SelfModelParent.SetActive(true);
                                hand.OtherModelParent.SetActive(true);
                            }
                        }
                        else
                        {
                            toolData.AttachLeftHand.SetActive(false);
                            toolData.AttachRightHand.SetActive(false);
                            hand.SelfModelParent.SetActive(true);
                            hand.OtherModelParent.SetActive(true);
                        }
                    }
                }
            }
        }

        private static void SetInteractorInvisible(ToolData toolData, HandData hand)
        {
            if (toolData.BothHands)
            {
                hand.SelfModelParent.SetActive(false);
                hand.OtherModelParent.SetActive(false);
            }
            else
            {
                hand.SelfModelParent.SetActive(false);
            }
        }
    }
}