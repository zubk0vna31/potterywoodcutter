using Leopotam.Ecs;
using Modules.Root.ContainerComponentModel;
using Modules.Root.ECS;
using PWS.Common.Audio;
using PWS.Common.GameModeInfoService;
using PWS.Common.ResultsEvaluation;
using PWS.Common.TravelPoints;
using PWS.Common.UI;
using PWS.WoodCutting.Common;
using UnityEngine;

namespace PWS.WoodCutting.Stages.Chamfering
{
    internal class StateDependecies
    {
        [Inject] public IGameModeInfoService gameModeInfoService;
        [Inject] public IResultsEvaluationDataHolder resultsEvaluation;
    }

    public class WoodCuttingChamferingSystemsProvider : MonoBehaviour, ISystemsProvider
    {
        [Header("Core")]
        [SerializeField]
        private StateConfig _stateConfig;
        [SerializeField]
        private StateInfoData _stateInfo;

        [SerializeField, Range(0.001f, 2f)]
        private float uvMargin = 0.15f;

        //[Header("Scene Related")]

        [Header("Voice config")]
        [SerializeField] private VoiceConfig _voiceConfig;

        public EcsSystems GetSystems(EcsWorld world, EcsSystems endFrame, EcsSystems mainSystems)
        {
            StateDependecies dependecies = new StateDependecies();
            SceneContainer.Instance.Inject(dependecies);

            EcsSystems systems = new EcsSystems(world, "WoodCutting Chamfering");

            systems

                .Add(new SetStateDelayedSystem<ChamferingState>())

                .Add(new OutlineByStateSystem<ChamferingState>(_stateConfig))


                .Add(new ChamferingInitializeSystem(_stateConfig, uvMargin))
                .Add(new ChamferingSkipStateSystem(_stateConfig))

                .Add(new ChamferingStatePaintableUpdateSystem(_stateConfig))

                //Tracker
                .Add(new ChamferingStateTrackerSystem(_stateConfig,dependecies.gameModeInfoService))

                .Add(new WoodCuttingSetGradeByProgress<ChamferingState>(dependecies.resultsEvaluation,
                _stateInfo))

                .Add(new StateInfoWindowUIProcessing<ChamferingState>(_stateInfo))
                .Add(new StateWindowPlacementProcessing<ChamferingState>("Main"))

                .Add(new TravelPointPositioningSystem<ChamferingState>("Default"))


                .Add(new StateWindowProgressUIProcessing<ChamferingState>())
                .Add(new StateWindowButtonUIProcessing<ChamferingState>(false))
                
                //Voice
                .Add(new VoiceAudioSystem<ChamferingState>(_voiceConfig))
                
                //Sound
                .Add(new StickableBasedSoundSystem<ChamferingState>())

                ;


            return systems;
        }
    }
}
