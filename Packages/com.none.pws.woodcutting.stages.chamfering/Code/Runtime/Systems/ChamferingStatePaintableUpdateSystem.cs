using Leopotam.Ecs;
using PWS.WoodCutting.Common;

namespace PWS.WoodCutting.Stages.Chamfering
{
    public class ChamferingStatePaintableUpdateSystem : RunSystem<ChamferingState, ChamferingStateConfig>
    {
        private readonly EcsFilter<PlankFinal> _planks;
        public ChamferingStatePaintableUpdateSystem(StateConfig config) : base(config)
        {
        }

        protected override void OnStateUpdate()
        {
            base.OnStateUpdate();

            foreach (var i in _planks)
            {
                var percentage = _planks.Get1(i).chamferingPattern.CurrentTime();

                if (percentage >= 1f && !_planks.Get1(i).chamferingPattern.IsLoopTraveled(1f))
                {
                    _planks.Get1(i).chamferingPattern.SetLoopTraveled(percentage);
                    _planks.Get1(i).chamferPaintable.BackupPercentage();
                    _planks.Get1(i).chamferPaintable.SetMaterialColors(_config.chamferedColor,
                        _config.chamferedColor2, _config.chamferedColor2);
                }
                else if (percentage >= 2f)
                {
                    _planks.Get1(i).chamferingPattern.DeactivateTriggerZone(0);
                }
            }


        }
    }
}
