﻿using Leopotam.Ecs;
using PWS.Common;
using PWS.Common.UI;
using PWS.WoodCutting.Common;

namespace PWS.WoodCutting.Stages.Chamfering
{
    public class ChamferingSkipStateSystem : RunSystem<ChamferingState, ChamferingStateConfig>
    {
        private readonly EcsFilter<NextStateSignal> _nextStateSignal;

        private readonly EcsFilter<PlankFinal> _plankFinal;

        private readonly EcsFilter<StateProgress> _stateProgress;
        private readonly EcsWorld _ecsWorld;

        public ChamferingSkipStateSystem(StateConfig config) : base(config)
        {
        }

        protected override void OnStateUpdate()
        {
            base.OnStateUpdate();

            if (_nextStateSignal.IsEmpty()) return;

            float grade = 0f;
            foreach (var i in _stateProgress)
            {
                grade = _stateProgress.Get1(i).ProgressValue;
            }
            _ecsWorld.NewEntity().Get<SetGradeSignal>().grade = grade;

            foreach (var i in _nextStateSignal)
            {
                _nextStateSignal.Get1(i).button.gameObject.SetActive(false);
                _nextStateSignal.GetEntity(i).Get<Ignore>();
            }

            foreach (var i in _plankFinal)
            {
                ref var plank = ref _plankFinal.Get1(i);
                plank.chamferPaintable.PaintAllWithDuration(_config.paintAllDuration);
                plank.chamferingPattern.SetAllLoopsTraveled();
                plank.chamferingPattern.Dissable();
                plank.view.ShowChamferedObject(_config.paintAllDuration);
            }

            var entity = _ecsWorld.NewEntity();
            entity.Get<SetStateDelayed>().state = _config.nextState;
            entity.Get<SetStateDelayed>().delay = _config.paintAllDuration;
        }
    }
}