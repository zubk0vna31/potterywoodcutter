﻿using Leopotam.Ecs;
using PWS.Common.GameModeInfoService;
using PWS.Common.UI;
using PWS.WoodCutting.Common;
using UnityEngine;

namespace PWS.WoodCutting.Stages.Chamfering
{
    public class ChamferingStateTrackerSystem : RunSystem<ChamferingState, ChamferingStateConfig>
    {
        private readonly EcsFilter<StateProgress> _stateProgress;
        private readonly EcsFilter<PlankFinal> _planks;
        private IGameModeInfoService _gameModeInfoService;

        public ChamferingStateTrackerSystem(StateConfig config,IGameModeInfoService gameModeInfoService) : base(config)
        {
            _gameModeInfoService = gameModeInfoService;
        }

        protected override void OnStateEnter()
        {
            base.OnStateEnter();

            foreach (var i in _stateProgress)
            {
                ref var progress = ref _stateProgress.Get1(i);

                progress.Value = 0f;
                progress.CompletionThreshold = 2f;
                progress.SkipThreshold = _gameModeInfoService.CurrentMode.Equals(GameMode.Free)?
                    0.0f :_config.skipThreshold;
            }
        }

        protected override void OnStateUpdate()
        {
            base.OnStateUpdate();

            foreach (var i in _stateProgress)
            {
                float value = 0f;

                foreach (var plank in _planks)
                {
                    value += _planks.Get1(plank).chamferingPattern.CurrentTime();
                }

                value = Mathf.Clamp01(value / (_planks.GetEntitiesCount()*2f));

                _stateProgress.Get1(i).Value = value;
            }
        }
    }
}