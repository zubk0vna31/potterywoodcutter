﻿using Leopotam.Ecs;
using PWS.WoodCutting.Common;

namespace PWS.WoodCutting.Stages.Chamfering
{
    public class ChamferingInitializeSystem : RunSystem<ChamferingState, ChamferingStateConfig>
    {
        private readonly EcsFilter<PlankFinal> _planks;
        private readonly float _uvMargin;

        public ChamferingInitializeSystem(StateConfig config,float uvMargin) : base(config)
        {
            _uvMargin = uvMargin;
        }

        protected override void OnStateEnter()
        {
            base.OnStateEnter();

            foreach (var i in _planks)
            {
                ref var plankFinal = ref _planks.Get1(i);

                plankFinal.view.State8.gameObject.SetActive(false);
                plankFinal.view.State9.gameObject.SetActive(true);

                plankFinal.chamferingPattern.Initialize(plankFinal.pattern.vertexPathFollowConfig,
                                    plankFinal.pattern.chamferToolConfig);
                plankFinal.chamferingPattern.SetMaxTime(2f);

                plankFinal.chamferPaintable.Initialize(plankFinal.chamferingPattern.GetUVArea(),
                    _config.skipThreshold,false,_uvMargin);
                plankFinal.chamferPaintable.SetNewMaxPercentage(2f);
                plankFinal.chamferPaintable.SetMaterialColors(_config.normalColor,
                    _config.chamferedColor,_config.chamferedColor2);
            }
        }
    }
}