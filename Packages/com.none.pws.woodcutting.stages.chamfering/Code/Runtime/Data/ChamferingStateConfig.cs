using PWS.WoodCutting.Common;
using UnityEngine;

namespace PWS.WoodCutting.Stages.Chamfering
{
    [CreateAssetMenu(fileName = "ChamferingStateConfig",
        menuName = "PWS/WoodCutting/Stages/Chamfering/Config")]
    public class ChamferingStateConfig : StateConfig
    {
        public Material meshMaterial;
        [Range(0f, 1f)]
        public float skipThreshold = 0.5f;
        [Range(0.1f, 1f)]
        public float paintableUpdateInterval = 0.5f;
        [Range(0.1f, 1f)]
        public float paintAllDuration = 0.5f;
        

        public Color normalColor;
        public Color chamferedColor;
        public Color chamferedColor2;
    }
}
