using Modules.StateGroup.Core;
using UnityEngine;

namespace PWS.WoodCutting.Stages.Chamfering
{
    [CreateAssetMenu(menuName = "PWS/WoodCutting/Stages/Chamfering/SetStateCommand")]
    public class SetChamferingStateSOCommand : SetStateSOCommand<ChamferingState>
    {        
    }
}
