using Leopotam.Ecs;
using Modules.StateGroup.Components;
using PWS.Common.ResultsSubmission.ResultCapture;
using UnityEngine;

namespace PWS.WoodCutting.Stages.ResultsEvaluation
{
    public class WoodCuttingScreenshotSubmissionTracker : IEcsRunSystem
    {
        private const float ADDITIONAL_WAIT_TIME = 0.5f;
        
        private readonly EcsFilter<StateEnter, ResultsEvaluationState> _signal;
        private readonly EcsWorld _world;
        readonly float _resultSubmissionWaitTime;

        private float _waitFromTime;
        private bool _waitingForSubmission;

        public WoodCuttingScreenshotSubmissionTracker(float resultSubmissionWaitTime)
        {
            _resultSubmissionWaitTime = resultSubmissionWaitTime;
        }

        public void Run()
        {
            if (!_signal.IsEmpty())
            {
                _waitFromTime = Time.time;
                _waitingForSubmission = true;
            }

            if (_waitingForSubmission && (Time.time - _waitFromTime) > _resultSubmissionWaitTime+ADDITIONAL_WAIT_TIME)
            {
                _waitingForSubmission = false;
                _world.NewEntity().Get<SubmitResultsSignal>();
            }
        }
    }
}