using Leopotam.Ecs;
using Modules.StateGroup.Core;
using Modules.Utils;
using Modules.ViewHub;
using PWS.Common.GameModeInfoService;
using PWS.Common.ResultsEvaluation;
using PWS.Common.SceneSwitchService;
using PWS.Features.Achievements;
using PWS.Tutorial;
using PWS.WoodCutting.Common;
using PWS.WoodCutting.MarkupPainting;
using UnityEngine;

namespace PWS.WoodCutting.Stages.ResultsEvaluation
{
    public class ResultsEvaluationStateInitializeSystem : RunSystem<ResultsEvaluationState, ResultsEvaluationStateConfig>
    {
        private readonly EcsFilter<PatternSelectMenu> _menu;
        private readonly EcsFilter<PlayerTag, UnityView> _player;
        private readonly EcsFilter<PlankFinal> _planks;
        private readonly EcsFilter<ResultsEvaluationWindow> _resultWindows;
        private readonly EcsFilter<PWS.Pottery.Stages.DecorativePainting.ColorMenuItemSelected> _colorSelected;

        private readonly ISceneSwitchService _sceneSwitchService;
        private readonly IResultsEvaluationDataHolder _resultsEvaluationDataHolder;
        private readonly IAchievementsService _achievementsService;
        private readonly IGameModeInfoService _gameModeInfoService;
        private readonly TimeService _timeService;
        private readonly Transform _playerTeleport, _platbandParent, _playerCamera;

        private float _timer;
        private float _finalGrade;
        private float _finalPercentage;

        public ResultsEvaluationStateInitializeSystem(StateConfig config,
            ISceneSwitchService sceneSwitchService, IResultsEvaluationDataHolder resultsEvaluationDataHolder,
            IAchievementsService achievementsService, IGameModeInfoService gameModeInfoService,
            Transform playerTeleport, Transform platbandParent, Transform playerCamera) : base(config)
        {
            _sceneSwitchService = sceneSwitchService;
            _resultsEvaluationDataHolder = resultsEvaluationDataHolder;
            _achievementsService = achievementsService;
            _gameModeInfoService = gameModeInfoService;
            _playerTeleport = playerTeleport;
            _platbandParent = platbandParent;
            _playerCamera = playerCamera;
        }

        protected override void OnStateEnter()
        {
            Color color = Color.white;

            foreach (var i in _colorSelected)
            {
                color = _colorSelected.Get1(i).Color;
            }

            foreach (var i in _menu)
            {
                var platbandFinal = _menu.Get1(i).View.LoadPlatband(_platbandParent);
                platbandFinal.GetComponent<PlatbandFinal>().UpdatePaintedColor(color,1f);
            }

            foreach (var i in _player)
            {
                var player = _player.Get2(i).Transform;

                Vector3 playerPos = player.position;
                playerPos.y = 0f;

                Vector3 cameraPos = _playerCamera.position;
                cameraPos.y = 0f;

                Vector3 cameraDir = _playerCamera.forward;
                cameraDir.y = 0;

                player.position = _playerTeleport.position + (playerPos - cameraPos);
                player.RotateAround(_playerTeleport.position, Vector3.up,
                    -Vector3.SignedAngle(_playerTeleport.forward, cameraDir, Vector3.up));

#if UNITY_EDITOR
                _playerCamera.parent.localPosition = -Vector3.up * (_playerCamera.localPosition.y
                    - _playerCamera.parent.localPosition.y);
#endif
            }

            foreach (var i in _planks)
            {
                _planks.GetEntity(i).Destroy();
            }

            // Result UI
            foreach (var i in _resultWindows)
            {
                _resultWindows.Get1(i).View.SetData(_resultsEvaluationDataHolder.Grades,
                    _resultsEvaluationDataHolder.ResultGradeMultiplier);
                _resultWindows.Get1(i).View.Show();

                _finalGrade = _resultWindows.Get1(i).View.FinalGrade;
                _finalPercentage = _resultWindows.Get1(i).View.Percentage;
            }

            _sceneSwitchService.FadeOut(_config.fadeOutDuration);
            _timer = _config.fadeOutDuration;
        }

        protected override void OnStateUpdate()
        {
            base.OnStateUpdate();

            if (_timer > 0)
            {
                _timer -= _timeService.DeltaTime;

                if (_timer <= 0f)
                {
                    // Achievement
                    _achievementsService.AppendProgressFloat(AchievementsNames.tyap_lyap, _finalPercentage);

                    // Achievement
                    _achievementsService.AppendProgressFloat(AchievementsNames.experience_gained_is_never_lost,_finalGrade);

                    // Achievement
                    _achievementsService.Unlock(AchievementsNames.clamping_device_i_say);

                    // Achievement
                    if(_gameModeInfoService.CurrentMode.Equals(GameMode.Free))
                        _achievementsService.Unlock(AchievementsNames.specialist);
                }
            }
        }
    }
}
