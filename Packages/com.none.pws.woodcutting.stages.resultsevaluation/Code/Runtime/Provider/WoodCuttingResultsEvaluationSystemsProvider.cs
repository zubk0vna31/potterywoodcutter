using Leopotam.Ecs;
using Modules.Root.ContainerComponentModel;
using Modules.Root.ECS;
using PWS.Common.Audio;
using PWS.Common.Confetti;
using PWS.Common.GameModeInfoService;
using PWS.Common.ResultsEvaluation;
using PWS.Common.SceneSwitchService;
using PWS.Common.UI;
using PWS.Features.Achievements;
using PWS.FeatureToggles;
using PWS.WoodCutting.Common;
using UnityEngine;

namespace PWS.WoodCutting.Stages.ResultsEvaluation
{
    public class WoodCuttingResultsEvaluationSystemsProvider : MonoBehaviour, ISystemsProvider
    {
        internal class SceneSwitcherServiceContainer
        {
            [Inject] public ISceneSwitchService sceneSwitchService;
        }

        internal class ResultsEvaluationDependecies
        {
            [Inject] public IResultsEvaluationDataHolder resultsEvaluation;
            [Inject] public IAchievementsService achievementsService;
            [Inject] public IGameModeInfoService gameModeInfoService;
        }

        [Header("Core")]
        [SerializeField]
        private StateConfig _stateConfig;
        [SerializeField]
        private StateInfoData _stateInfo;

        [Header("Scene-Related")]
        [SerializeField]
        private Transform playerTeleport;
        [SerializeField]
        private Transform platbandParent;
        [SerializeField]
        private Transform playerCamera;
        
        [Header("Voice config")]
        [SerializeField] private VoiceConfig _voiceConfig;



        public EcsSystems GetSystems(EcsWorld world, EcsSystems endFrame, EcsSystems mainSystems)
        {
            ResultsEvaluationDependecies dependecies = new ResultsEvaluationDependecies();
            SceneContainer.Instance.Inject(dependecies);

            SceneSwitcherServiceContainer sceneSwitcher = new SceneSwitcherServiceContainer();
            SceneContainer.Instance.Inject(sceneSwitcher);

            EcsSystems systems = new EcsSystems(world, "WoodCutting Results Evaluation");


            systems

                .Add(new ResultsEvaluationStateInitializeSystem(_stateConfig,
                sceneSwitcher.sceneSwitchService, dependecies.resultsEvaluation,dependecies.achievementsService,
                dependecies.gameModeInfoService,
                playerTeleport,platbandParent, playerCamera))

                .Add(new StateInfoWindowUIProcessing<ResultsEvaluationState>(_stateInfo))
                .Add(new StateWindowPlacementProcessing<ResultsEvaluationState>("Main"))

                .Add(new WoodCuttingScreenshotSubmissionTracker(GetFadeoutTime()))
                
                .Add(new VoiceAudioSystem<ResultsEvaluationState>(_voiceConfig))
                ;
            
            if (FeatureManagerFacade.FeatureEnabled(ConfettiFeatures.PWS_Common_Confetti))
            {
                systems.Add(new ConfettiSystem<ResultsEvaluationState>());
            }

            return systems;
        }

        private float GetFadeoutTime()
        {
            ResultsEvaluationStateConfig resultsEvaluationStateConfig = _stateConfig as ResultsEvaluationStateConfig;
            if (resultsEvaluationStateConfig != null)
            {
                return resultsEvaluationStateConfig.fadeOutDuration;
            }

            return 1.0f;
        }
    }
}
