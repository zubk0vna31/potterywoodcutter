using Modules.StateGroup.Core;
using UnityEngine;

namespace PWS.WoodCutting.Stages.ResultsEvaluation
{
    [CreateAssetMenu(fileName = "W_SetResultsEvaluationStateSOCommand", menuName =
        "PWS/WoodCutting/Stages/Results Evaluation/SOCommand")]
    public class SetResultsEvaluationStateSOCommand : SetStateSOCommand<ResultsEvaluationState>
    {
        
    }
}
