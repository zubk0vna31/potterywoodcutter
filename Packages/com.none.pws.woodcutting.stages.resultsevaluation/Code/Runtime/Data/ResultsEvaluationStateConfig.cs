using PWS.WoodCutting.Common;
using UnityEngine;

namespace PWS.WoodCutting.Stages.ResultsEvaluation
{
    [CreateAssetMenu(fileName = "W_ResultsEvaluationStateConfig", menuName =
        "PWS/WoodCutting/Stages/Results Evaluation/State Config")]
    public class ResultsEvaluationStateConfig : StateConfig
    {
        [Range(0f,2.5f)]
        public float fadeOutDuration = 0.5f;
    }
}
