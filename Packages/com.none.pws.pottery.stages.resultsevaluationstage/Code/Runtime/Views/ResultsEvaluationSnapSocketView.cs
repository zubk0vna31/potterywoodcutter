using Modules.ViewHub;
using Leopotam.Ecs;

namespace PWS.Pottery.Stages.ResultsEvaluationStage
{
    public class ResultsEvaluationSnapSocketView : ViewComponent
    {
        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            ecsEntity.Get<ResultsEvaluationSnapSocket>();
        }
    }
}