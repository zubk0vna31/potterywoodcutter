using Modules.ViewHub;
using Leopotam.Ecs;

namespace PWS.Pottery.Stages.ResultsEvaluationStage
{
    public class ResultsEvaluationContainerView : ViewComponent
    {
        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            ecsEntity.Get<ResultsEvaluationContainer>();
        }
    }
}