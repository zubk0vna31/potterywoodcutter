using Leopotam.Ecs;
using Modules.Root.ContainerComponentModel;
using Modules.Root.ECS;
using Modules.VRFeatures;
using PWS.Common.Audio;
using PWS.Common.Confetti;
using PWS.Common.GameModeInfoService;
using PWS.Common.ResultsEvaluation;
using PWS.Common.UI;
using PWS.Features.Achievements;
using PWS.FeatureToggles;
using PWS.Pottery.Common;
using PWS.Pottery.MeshGeneration;
using UnityEngine;

namespace PWS.Pottery.Stages.ResultsEvaluationStage
{
    public class PotteryResultsEvaluationStageSystemsProvider : MonoBehaviour, ISystemsProvider
    {
        internal class SystemsDependencies
        {
            [Inject] public IResultsEvaluationDataHolder ResultsEvaluationData;
            [Inject] public IAchievementsService AchievementsService;
            [Inject] public IGameModeInfoService GameModeInfoService;
        }
        [Header("Voice config")]
        [SerializeField] private VoiceConfig _voiceConfig;
        [SerializeField] private StateInfoData _stateInfo;

        public EcsSystems GetSystems(EcsWorld world, EcsSystems endFrame, EcsSystems mainSystems)
        {
            SystemsDependencies dependencies = new SystemsDependencies();
            SceneContainer.Instance.Inject(dependencies);
            EcsSystems systems = new EcsSystems(world);
            systems
                //item placing states
                .Add(new SnapSocketActivityProcessing<ResultsEvaluationItemPlacingState, ResultsEvaluationSnapSocket>())

                .Add(new ResultsEvaluationItemPlacingProcessing())

                .Add(new MeshVisibilityProcessing<ResultsEvaluationItemPlacingState, SculptingProductMesherTag>())
                .Add(new XRInteractableProcessing<ResultsEvaluationItemPlacingState, SculptingProductTag>())

                .Add(new StateInfoWindowUIProcessing<ResultsEvaluationItemPlacingState>(_stateInfo))

                //system states

                // Achievements
                .Add(new FinalResultsAchievements<ResultsEvaluationState>(dependencies.AchievementsService,dependencies.GameModeInfoService))

                .Add(new GameObjectVisibilityProcessing<ResultsEvaluationState, ResultsEvaluationContainer>())
                .Add(new MeshVisibilityProcessing<ResultsEvaluationState, SculptingProductMesherTag>())

                .Add(new ResultsEvaluationWindowUIProcessing<ResultsEvaluationState>(dependencies.ResultsEvaluationData))

                .Add(new StateInfoWindowUIProcessing<ResultsEvaluationState>(_stateInfo))

                //voice systems
                .Add(new VoiceAudioSystem<ResultsEvaluationItemPlacingState>(_voiceConfig))
                
                // screenshots submission
                .Add(new ScreenshotSubmissionTracker())
                ;
            
            //confetti system
            if (FeatureManagerFacade.FeatureEnabled(ConfettiFeatures.PWS_Common_Confetti))
            {
                systems.Add(new ConfettiSystem<ResultsEvaluationState>());
            }

            return systems;
        }
    }
}
