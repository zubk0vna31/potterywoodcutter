using Modules.StateGroup.Core;
using UnityEngine;

namespace PWS.Pottery.Stages.ResultsEvaluationStage
{
    [CreateAssetMenu(fileName = "SetResultsEvaluationStateSOCommand", menuName = "PWS/Pottery/Stages/ResultsEvaluationStage/SetStateSOCommand")]
    public class SetResultsEvaluationStateSOCommand : SetStateSOCommand<ResultsEvaluationItemPlacingState>
    {

    }
}