using Leopotam.Ecs;
using Modules.StateGroup.Components;

namespace PWS.Pottery.Stages.ResultsEvaluationStage
{
    public class ScreenshotSubmissionTracker : IEcsRunSystem
    {
        // auto injected fields
        private readonly EcsFilter<StateEnter, ResultsEvaluationState> _signal;
        private readonly EcsWorld _world;
        
        public void Run()
        {
            if(_signal.IsEmpty())
                return;

            _world.NewEntity().Get<PWS.Common.ResultsSubmission.ResultCapture.SubmitResultsSignal>();
        }
    }
}