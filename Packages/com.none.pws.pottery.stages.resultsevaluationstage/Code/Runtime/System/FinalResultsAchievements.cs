using Leopotam.Ecs;
using Modules.StateGroup.Components;
using Modules.Utils;
using PWS.Common.GameModeInfoService;
using PWS.Common.ResultsEvaluation;
using PWS.Features.Achievements;

namespace PWS.Pottery.Stages.ResultsEvaluationStage
{
    public class FinalResultsAchievements<T> : IEcsRunSystem where T:struct
    {
        private readonly EcsFilter<T> _state;

        private readonly IAchievementsService _achievementsService;
        private readonly IGameModeInfoService _gameModeInfoService;
        private readonly TimeService _timeService;

        private readonly EcsFilter<ResultsEvaluationWindow> _resultWindows;

        private float delay;

        public FinalResultsAchievements(IAchievementsService achievementsService,IGameModeInfoService gameModeInfoService ,float delay = 1.0f)
        {
            _achievementsService = achievementsService;
            _gameModeInfoService = gameModeInfoService;
            this.delay = delay;
        }

        public void Run()
        {
            if (_state.IsEmpty()) return;


            if (delay > 0)
            {
                delay -= _timeService.DeltaTime;

                if (delay <= 0)
                {
                    OnTimerEnded();
                }
            }
            else
            {
                OnTimerEnded();
            }
        }

        private void OnTimerEnded()
        {
            float percentage=0;

            foreach (var i in _resultWindows)
            {
                percentage = _resultWindows.Get1(i).View.Percentage;
            }

            // first
            _achievementsService.Unlock(AchievementsNames.potter);

            // fast
            _achievementsService.AppendProgressFloat(AchievementsNames.fast_walkthrough, percentage);

            // ceramist
            if (_gameModeInfoService.CurrentMode.Equals(GameMode.Free))
            {
                _achievementsService.Unlock(AchievementsNames.ceramist);
            }
        }
    }
}
