using Leopotam.Ecs;
using Modules.StateGroup.Components;
using Modules.StateGroup.Core;
using Modules.VRFeatures;
using PWS.Pottery.MeshGeneration;
using UnityEngine;

namespace PWS.Pottery.Stages.ResultsEvaluationStage
{
    public class ResultsEvaluationItemPlacingProcessing : IEcsRunSystem
    {
        // auto injected fields
        readonly EcsFilter<StateEnter, ResultsEvaluationItemPlacingState> _entering;
        readonly EcsFilter<ResultsEvaluationItemPlacingState>.Exclude<StateExit> _inState;

        readonly EcsFilter<ResultsEvaluationSnapSocket, SelectEntered> _placingSignal;
        readonly EcsFilter<XRInteractableComponents, SculptingProductTag> _product;

        private StateFactory _stateFactory;

        //runtime data
        private const float _waitTime = 0.3f;
        private float _timer;
        private bool _isSelected;

        public void Run()
        {
            if (!_entering.IsEmpty())
            {
                _timer = 0;
                _isSelected = false;
            }

            if (_inState.IsEmpty())
                return;

            foreach (var signal in _placingSignal)
            {
                /*foreach (var product in _product)
                {
                    _product.Get1(product).View.EnableCollider(false);
                }*/
                _isSelected = true;
            }

            if (_isSelected)
            {
                if(_timer < _waitTime)
                {
                    _timer += Time.deltaTime;
                }
                else
                {
                    _stateFactory.SetState<ResultsEvaluationState>();
                }
            }
        }
    }
}
