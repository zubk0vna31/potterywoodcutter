﻿#if UNITY_EDITOR
using UnityEditor;
using UnityEditorInternal;
using Modules.Root.ECS;
using UnityEngine;

namespace Modules.Root.Editor
{
    [CustomEditor(typeof(EcsStartup), true)]
    public class EcsStartupEditor : UnityEditor.Editor
    {
        private ReorderableList _providersList;
        private ReorderableList _fixedUpdateProvidersList;


        private void OnEnable()
        {
            _providersList = EditorHelper.DrawReorderableList(serializedObject, "_systemProviders", "Update Systems Providers");
            _fixedUpdateProvidersList = EditorHelper.DrawReorderableList(serializedObject, "_fixedUpdateSystemProviders", "FixedUpdate Update Systems Providers");
        }

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
            EditorGUILayout.Space();
            
            _fixedUpdateProvidersList.DoLayoutList();

            EditorGUILayout.Space();

            _providersList.DoLayoutList();
            
            serializedObject.ApplyModifiedProperties();
        }
    }
}
#endif
