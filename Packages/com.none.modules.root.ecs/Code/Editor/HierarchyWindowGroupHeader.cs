#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;

namespace GameCore.Editor
{
    [InitializeOnLoad]
    public static class HierarchyWindowHeader
    {
        static HierarchyWindowHeader()
        {
            EditorApplication.hierarchyWindowItemOnGUI += HierarchyWindowItemOnGUI;
        }

        static void HierarchyWindowItemOnGUI(int instanceID, Rect selectionRect)
        {
            var gameObject = EditorUtility.InstanceIDToObject(instanceID) as GameObject;

            if (gameObject != null)
            {
                if (gameObject.name.StartsWith("---", System.StringComparison.Ordinal))
                {
                    EditorGUI.DrawRect(new Rect(selectionRect.x , selectionRect.y, selectionRect.width, selectionRect.height), Color.gray);
                    EditorGUI.LabelField(selectionRect, gameObject.name.Replace("-", ""));
                }else if (gameObject.name.StartsWith("--+--", System.StringComparison.Ordinal))
                {
                    EditorGUI.DrawRect(new Rect(selectionRect.x , selectionRect.y, selectionRect.width, selectionRect.height), Color.gray);
                    EditorGUI.LabelField(selectionRect, gameObject.name.Substring(5, gameObject.name.Length-5));
                }

            }
        }
    }
}
#endif