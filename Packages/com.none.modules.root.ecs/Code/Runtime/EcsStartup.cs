﻿using UnityEngine;
using Leopotam.Ecs;
using System.Collections.Generic;
using System.Linq;

namespace Modules.Root.ECS
{
    public class EcsStartup : MonoBehaviour
    {
        [SerializeField, HideInInspector] private List<UnityEngine.Object> _systemProviders;
        [SerializeField, HideInInspector] private List<UnityEngine.Object> _fixedUpdateSystemProviders;

        private EcsWorld _world;
        private EcsSystems _systems;
        private EcsSystems _fixedUpdateSystems;

        void OnEnable()
        {
            _world = new EcsWorld();
            _systems = new EcsSystems(_world);
            _fixedUpdateSystems = new EcsSystems(_world, "FixedUpdateSystems");
            EcsSystems fixedUpdateEndFrame = new EcsSystems(_world, "FixedUpdateEndFrame");
            EcsSystems endFrame = new EcsSystems(_world, "EndFrame");

#if UNITY_EDITOR
            Leopotam.Ecs.UnityIntegration.EcsWorldObserver.Create(_world);
            Leopotam.Ecs.UnityIntegration.EcsSystemsObserver.Create(_systems);
            Leopotam.Ecs.UnityIntegration.EcsSystemsObserver.Create(_fixedUpdateSystems);
#endif

            _systems.Add(new Utils.UnityTimeSystem());
            _fixedUpdateSystems.Add(new Utils.FixedTimeSystem());

            for (int i = 0; i < _systemProviders.Count; i++)
            {
                if(_systemProviders[i] is ISystemsProvider) 
                {
                    _systems.Add((_systemProviders[i] as ISystemsProvider).GetSystems(_world, endFrame, _systems));
                }
            }

            if(_fixedUpdateSystemProviders != null)
            {
                for (int i = 0; i < _fixedUpdateSystemProviders.Count; i++)
                {
                    if(_fixedUpdateSystemProviders[i] is ISystemsProvider)
                    {
                        _fixedUpdateSystems.Add((_fixedUpdateSystemProviders[i] as ISystemsProvider).GetSystems(_world, fixedUpdateEndFrame, _fixedUpdateSystems));
                    }
                }
            }

            Utils.TimeService timeService = new Utils.TimeService();

            _systems.Add(endFrame);
            _systems.Inject(timeService, typeof(Utils.TimeService));

            _fixedUpdateSystems.Add(fixedUpdateEndFrame);
            _fixedUpdateSystems.Inject(timeService, typeof(Utils.TimeService));

            _systems
                .Init();

            _fixedUpdateSystems
                .Init();
        }

#if UNITY_EDITOR

        [ContextMenu("Add")]
        public void Add() 
        {
            List<Object> temp = _systemProviders;
            _systemProviders = new List<Object>();
            _systemProviders.Add(null);
            _systemProviders.AddRange(temp);
        }

#endif
        void Update()
        {
            _systems.Run();
        }

        void FixedUpdate()
        {
            _fixedUpdateSystems.Run();
        }

        void OnDisable()
        {
            Cleanup();
        }

        private void OnDestroy()
        {
            Cleanup();
        }

        private void Cleanup()
        {
            if(_fixedUpdateSystems != null)
            {
                _fixedUpdateSystems.Destroy();
                _fixedUpdateSystems = null;
            }

            if (_systems != null)
            {
                _systems.Destroy();
                _systems = null;
                _world.Destroy();
                _world = null;
            }
        }
    }
}
