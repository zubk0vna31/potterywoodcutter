using UnityEngine;
using Modules.Root.ContainerComponentModel;

namespace PWS.Tutorial
{
    [CreateAssetMenu(fileName = "TutorialServiceInstaller", menuName = "PWS/TutorialService/Installer", order = 0)]
    public class TutorialServiceInstaller : ASOInstaller
    {
        [SerializeField]
        private string _sceneName;

        public override void Install(IContainer container)
        {
            container.Bind<ITutorialService>(new TutorialService(_sceneName));
        }
    }
}
