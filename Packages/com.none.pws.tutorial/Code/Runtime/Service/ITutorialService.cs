using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PWS.Tutorial
{
    public interface ITutorialService
    {
        bool TutorialCompleted { get; set; }
        void LoadTutorial();
    }
}
