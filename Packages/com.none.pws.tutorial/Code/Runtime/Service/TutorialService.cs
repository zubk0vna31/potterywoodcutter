using Modules.Root.ContainerComponentModel;
using PWS.Common.SceneSwitchService;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PWS.Tutorial
{
    public class TutorialService : ITutorialService
    {
        // dependencies
        [Inject] private ISceneSwitchService _sceneSwitchService;

        private string _sceneName;
        private bool _tutorialCompleted;

        public bool TutorialCompleted {
            get => _tutorialCompleted;
            set => _tutorialCompleted = value;
        }

        public void LoadTutorial()
        {
            _sceneSwitchService.LoadScene(_sceneName);
        }

        public TutorialService(string sceneName)
        {
            _sceneName = sceneName;
        }
    }
}
