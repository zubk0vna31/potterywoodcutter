using UnityEngine;

namespace PWS.Tutorial
{
    [CreateAssetMenu(fileName = "TutorialData", menuName = "PWS/Tutorial/TutorialData", order = 0)]
    public class TutorialConfig : ScriptableObject
    {
        public int MaxTeleportationsCount = 2;
        public int MaxLeapMoveCount = 2;
        public int MaxSnapTurnsCount = 2;
        public int MaxGrabCount = 2;
        public int MaxInstrumentsRunCount = 1;
        public int MaxHandPositioningGrabCount = 1;
    }
}
