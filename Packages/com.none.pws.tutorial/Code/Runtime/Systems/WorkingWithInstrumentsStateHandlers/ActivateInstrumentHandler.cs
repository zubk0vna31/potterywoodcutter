﻿using Leopotam.Ecs;

namespace PWS.Tutorial
{
    public class ActivateInstrumentHandler : IEcsRunSystem
    {
        private readonly EcsFilter<InstrumentRunSignal> _runSignal;
        private readonly EcsFilter<InstrumentStopSignal> _stopSignal;
        private readonly EcsFilter<ToolComponent> _tool;
        public void Run()
        {
            if (!_runSignal.IsEmpty())
            {
                foreach (var i in _tool)
                {
                    ref var tool = ref _tool.Get1(i);

                    if (tool.Interactable.isSelected)
                    {
                        tool.Animator.SetBool("Activate", true);
                    }
                }
            }

            if (!_stopSignal.IsEmpty())
            {
                foreach (var i in _tool)
                {
                    ref var tool = ref _tool.Get1(i);

                    if (tool.Interactable.isSelected)
                    {
                        tool.Animator.SetBool("Activate", false);
                    }
                }
            }
        }
    }
}