﻿using Leopotam.Ecs;
using Modules.StateGroup.Components;
using Modules.UIMessaging.Data;
using Modules.UIMessaging.Messages;

namespace PWS.Tutorial
{
    public class WorkingWithInstrumentsStateUIHandler : IEcsRunSystem
    {
        private readonly EcsFilter<WorkingWithInstrumentsState, StateEnter> _enterState;
        private readonly EcsFilter<WorkingWithInstrumentsState, StateExit> _exitState;
        private readonly EcsFilter<WorkingWithInstrumentsState> _inState;
        private readonly EcsFilter<InstrumentRunSignal> _signal;
        private readonly EcsFilter<InstrumentRunCounter> _counter;

        private readonly TargetActionPair _showTooltipUI;
        private readonly TargetActionPair _hideTooltipUI;
        private readonly TargetActionPair _onTargetActionsCountAchieved;
        
        public WorkingWithInstrumentsStateUIHandler(TargetActionPair showTooltipUI, TargetActionPair hideTooltipUI, TargetActionPair onTargetActionsCountAchieved)
        {
            _showTooltipUI = showTooltipUI;
            _hideTooltipUI = hideTooltipUI;
            _onTargetActionsCountAchieved = onTargetActionsCountAchieved;
        }

        public void Run()
        {
            if (!_enterState.IsEmpty())
            {
                UniRx.MessageBroker.Default.Publish(new PerformUIActionMessage(_showTooltipUI));
            }

            if (!_inState.IsEmpty())
            {
                if (!_signal.IsEmpty())
                {
                    foreach (var i in _counter)
                    {
                        if (_counter.Get1(i).MaxCount == _counter.Get1(i).Count)
                        {
                            UniRx.MessageBroker.Default.Publish(new PerformUIActionMessage(_onTargetActionsCountAchieved));
                            return;
                        }
                    }
                }
            }

            if (!_exitState.IsEmpty())
            {
                UniRx.MessageBroker.Default.Publish(new PerformUIActionMessage(_hideTooltipUI));
            }
        }
    }
}