﻿using Leopotam.Ecs;
using Modules.StateGroup.Components;

namespace PWS.Tutorial
{
    public class WorkingWithInstrumentsStateInstrumentRunHandler : IEcsRunSystem
    {
        private readonly EcsFilter<WorkingWithInstrumentsState, StateEnter> _enter;
        private readonly EcsFilter<WorkingWithInstrumentsState, StateExit> _exit;
        private readonly EcsFilter<PlayerTag> _player;
        private readonly EcsFilter<PlayerTag, InstrumentRunCounter>_playerWithCounter;

        private readonly TutorialConfig _tutorialConfig;

        public WorkingWithInstrumentsStateInstrumentRunHandler(TutorialConfig tutorialConfig)
        {
            _tutorialConfig = tutorialConfig;
        }

        public void Run()
        {
            if (!_enter.IsEmpty())
            {
                // add grab counter to player
                foreach (var i in _player)
                {
                    ref var counter = ref _player.GetEntity(i).Get<InstrumentRunCounter>();
                    counter.Count = 0;
                    counter.MaxCount = _tutorialConfig.MaxInstrumentsRunCount;
                }
            }

            if (!_exit.IsEmpty())
            {
                // remove grab counter from player
                foreach (var i in _playerWithCounter)
                {
                    _playerWithCounter.GetEntity(i).Del<InstrumentRunCounter>();
                }
            }
        }
    }
}