using Leopotam.Ecs;

namespace PWS.Tutorial
{
    public class TeleportationCountSystem: IEcsRunSystem
    {
        // auto injected fields
        readonly EcsFilter<TeleportationSignal, TeleportationCounter> _signal;

        public void Run()
        {
            if (_signal.IsEmpty())
                return;

            foreach (var i in _signal)
            {
                _signal.Get2(i).Count += 1;
                UnityEngine.Debug.Log("Teleported++");
            }
        }
    }
}
