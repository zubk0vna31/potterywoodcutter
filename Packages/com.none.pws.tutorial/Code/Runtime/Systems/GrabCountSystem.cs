using Leopotam.Ecs;

namespace PWS.Tutorial
{
    public class GrabCountSystem : IEcsRunSystem
    {
        // auto injected fields
        readonly EcsFilter<GrabSignal, GrabCounter> _signal;

        public void Run()
        {
            if (_signal.IsEmpty())
                return;

            foreach (var i in _signal)
            {
                _signal.GetEntity(i).Get<GrabCounter>().Count++;
            }
        }
    }
}
