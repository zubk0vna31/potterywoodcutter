using Leopotam.Ecs;
using Modules.StateGroup.Core;
using Modules.UIMessaging.Data;
using Modules.UIMessaging.Messages;

namespace PWS.Tutorial
{
    public class TransitToStateOnUIActionSystem<FromStateT, ToStateT> : IEcsRunSystem where FromStateT : struct where ToStateT : struct
    {
        // auto injected fields
        private readonly EcsFilter<FromStateT> _fromState;
        private readonly EcsFilter<UIActionPerformedMessage> _actionPerformed;
        private readonly StateFactory _stateFactory;
        
        private TargetActionPair _action;
        
        public TransitToStateOnUIActionSystem(TargetActionPair action)
        {
            _action = action;
        }

        public void Run()
        {
            if(_fromState.IsEmpty())
                return;

            foreach (var i in _actionPerformed)
            {
                if (_action.IsIn(ref _actionPerformed.Get1(i)))
                {
                    _stateFactory.SetState<ToStateT>();
                    break;
                }
            }
        }
    }
}