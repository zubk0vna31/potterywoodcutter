using Leopotam.Ecs;
using Modules.StateGroup.Components;
using PWS.Tutorial.States;

namespace PWS.Tutorial
{
    // handles SnapTurn tracking during move state of tutorial
    // transits to NextStateT once required amount of SnapTurn achieved 
    public class SnapTurnStateTurnHandler : IEcsRunSystem
    {
        // auto injected fields
        private readonly EcsFilter<SnapTurnState, StateEnter> _enter;
        private readonly EcsFilter<SnapTurnState, StateExit> _exit;
        private readonly EcsFilter<PlayerTag> _player;
        private readonly EcsFilter<PlayerTag, SnapTurnCounter> _playerWithCounter;

        private readonly TutorialConfig _tutorialConfig;

        public SnapTurnStateTurnHandler(TutorialConfig tutorialConfig)
        {
            _tutorialConfig = tutorialConfig;
        }

        public void Run()
        {
            if (!_enter.IsEmpty())
            {
                // add SnapTurn counter to player
                foreach (var i in _player)
                {
                    ref var counter = ref _player.GetEntity(i).Get<SnapTurnCounter>();
                    counter.Count = 0;
                    counter.MaxCount = _tutorialConfig.MaxSnapTurnsCount;
                }
            }

            if (!_exit.IsEmpty())
            {
                // remove SnapTurn counter from player
                foreach (var i in _playerWithCounter)
                {
                    _playerWithCounter.GetEntity(i).Del<SnapTurnCounter>();
                }
            }
        }
    }
}