using Leopotam.Ecs;
using Modules.StateGroup.Core;
using PWS.Tutorial.States;

namespace PWS.Tutorial.Systems
{
    // issues initial simulation state on initialization of ecs systems
    public class InitStateSpawner : IEcsInitSystem
    {
        // auto injected fields
        private StateFactory _stateFactory;

        private ITutorialService _tutorialService;

        public InitStateSpawner(ITutorialService tutorialService)
        {
            _tutorialService = tutorialService;
        }

        public void Init()
        {
            _stateFactory.SetState<TeleportState>();
        }
    }
}