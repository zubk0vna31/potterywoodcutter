using Leopotam.Ecs;
using Modules.StateGroup.Components;
using Modules.StateGroup.Core;
using Modules.UIMessaging.Data;
using Modules.UIMessaging.Messages;
using PWS.Tutorial.States;

namespace PWS.Tutorial
{
    public class FinalStateUIProcessing : IEcsRunSystem
    {
        // auto injected fields
        readonly EcsFilter<StateEnter, FinalState> _entering;
        readonly EcsFilter<StateExit, FinalState> _exiting;
        
        private TargetActionPair _show;
        private TargetActionPair _hide;

        public FinalStateUIProcessing(TargetActionPair show, TargetActionPair hide)
        {
            _show = show;
            _hide = hide;
        }

        public void Run()
        {
            if (!_entering.IsEmpty())
            {
                UniRx.MessageBroker.Default.Publish(new PerformUIActionMessage(_show));
            }
            
            if (!_exiting.IsEmpty())
            {
                UniRx.MessageBroker.Default.Publish(new PerformUIActionMessage(_hide));
            }
        }
    }
}
