using Leopotam.Ecs;
using Modules.UIMessaging.Data;
using Modules.UIMessaging.Messages;
using PWS.Tutorial.States;

namespace PWS.Tutorial
{
    public class PlayerOpenMenuDoneProcessing : IEcsRunSystem
    {
        // auto injected fields
        readonly EcsFilter<OpenMenuSignal> _signal;
        readonly EcsFilter<FinalState> _inState;

        private ITutorialService _tutorialService;
        private TargetActionPair _hide;

        public PlayerOpenMenuDoneProcessing(TargetActionPair hide, ITutorialService tutorialService)
        {
            _hide = hide;
            _tutorialService = tutorialService;
        }

        public void Run()
        {
            if (_inState.IsEmpty() || _signal.IsEmpty())
                return;

            foreach (var i in _signal)
            {
                UniRx.MessageBroker.Default.Publish(new PerformUIActionMessage(_hide));
                _tutorialService.TutorialCompleted = true;
            }
        }
    }
}
