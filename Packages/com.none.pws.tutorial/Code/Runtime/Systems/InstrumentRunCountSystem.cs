﻿using Leopotam.Ecs;
using UnityEngine;

namespace PWS.Tutorial
{
    public class InstrumentRunCountSystem : IEcsRunSystem
    {
        private readonly EcsFilter<WorkingWithInstrumentsState> _state;
        private readonly EcsFilter<InstrumentRunSignal> _signal;
        private readonly EcsFilter<InstrumentRunCounter> _counter;
        public void Run()
        {
            if (_state.IsEmpty())
                return;
            
            if (_signal.IsEmpty())
                return;

            foreach (var i in _counter)
            {
                _counter.GetEntity(i).Get<InstrumentRunCounter>().Count++;
                Debug.Log("InstrumentRunCounter++");
            }
        }
    }
}