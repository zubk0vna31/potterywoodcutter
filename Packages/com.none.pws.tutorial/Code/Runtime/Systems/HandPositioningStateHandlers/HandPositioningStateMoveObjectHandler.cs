﻿using Leopotam.Ecs;
using Modules.StateGroup.Components;
using Modules.ViewHub;

namespace PWS.Tutorial
{
    public class HandPositioningStateMoveObjectHandler : IEcsRunSystem
    {
        private readonly EcsFilter<HandPositioningState>.Exclude<StateEnter> _inState;
        private readonly EcsFilter<UnityView, HandPositioningObjectData> _object;
        private readonly EcsFilter<UnityView, HandPositioningHandData> _hand;

        public void Run()
        {
            if (!_inState.IsEmpty())
            {
                foreach (var objectIdx in _object)
                {
                    if (_hand.IsEmpty())
                        return;
                    
                    var move = true;
                    ref var objectData = ref _object.Get2(objectIdx);
                    ref var objectView = ref _object.Get1(objectIdx);

                    foreach (var hand in _hand)
                    {
                        move &= _hand.Get2(hand).Activated;

                        if (_hand.Get2(hand).Activated)
                        {
                            _hand.Get1(hand).Transform.parent = objectData.HandsModelAttach;
                        }
                    }

                    if (move)
                    {
                        objectView.Transform.parent = objectData.RightControllerModelParent.parent;
                        objectData.ObjectRigidbody.isKinematic = true;
                        objectData.ObjectWasMoved = true;
                        _object.GetEntity(objectIdx).Get<HandPositioningSignal>();
                    }
                    else if (objectData.ObjectWasMoved)
                    {
                        objectView.Transform.parent = null;
                        objectData.ObjectRigidbody.isKinematic = false;
                        objectData.ObjectWasMoved = false;
                    }
                }
            }
        }
    }
}