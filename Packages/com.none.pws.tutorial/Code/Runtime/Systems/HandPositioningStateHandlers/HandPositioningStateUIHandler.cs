﻿using Leopotam.Ecs;
using Modules.StateGroup.Components;
using Modules.UIMessaging.Data;
using Modules.UIMessaging.Messages;

namespace PWS.Tutorial
{
    public class HandPositioningStateUIHandler : IEcsRunSystem
    {
        private readonly EcsFilter<HandPositioningState, StateEnter> _enterMoveState;
        private readonly EcsFilter<HandPositioningState, StateExit> _exitMoveState;
        private readonly EcsFilter<HandPositioningState> _inState;
        private readonly EcsFilter<HandPositioningSignal> _signal;
        private readonly EcsFilter<HandPositioningGrabCounter> _performed;

        private readonly TargetActionPair _showTooltipUI;
        private readonly TargetActionPair _hideTooltipUI;
        private readonly TargetActionPair _onTargetActionsCountAchieved;
        
        public HandPositioningStateUIHandler(TargetActionPair showTooltipUI, TargetActionPair hideTooltipUI, TargetActionPair onTargetActionsCountAchieved)
        {
            _showTooltipUI = showTooltipUI;
            _hideTooltipUI = hideTooltipUI;
            _onTargetActionsCountAchieved = onTargetActionsCountAchieved;
        }

        public void Run()
        {
            if (!_enterMoveState.IsEmpty())
            {
                UniRx.MessageBroker.Default.Publish(new PerformUIActionMessage(_showTooltipUI));
            }

            if (!_inState.IsEmpty() && !_signal.IsEmpty())
            {
                foreach (var i in _performed)
                {
                    if (_performed.Get1(i).MaxCount == _performed.Get1(i).Count)
                    {
                        UniRx.MessageBroker.Default.Publish(new PerformUIActionMessage(_onTargetActionsCountAchieved));
                        return;
                    }
                }
            }

            if (!_exitMoveState.IsEmpty())
            {
                UniRx.MessageBroker.Default.Publish(new PerformUIActionMessage(_hideTooltipUI));
            }
        }
    }
}