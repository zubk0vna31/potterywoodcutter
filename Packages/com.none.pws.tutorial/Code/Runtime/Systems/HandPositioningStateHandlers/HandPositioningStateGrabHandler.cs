﻿using Leopotam.Ecs;
using Modules.StateGroup.Components;

namespace PWS.Tutorial
{
    public class HandPositioningStateGrabHandler : IEcsRunSystem
    {
        private readonly EcsFilter<HandPositioningState, StateEnter> _enter;
        private readonly EcsFilter<HandPositioningState, StateExit> _exit;
        private readonly EcsFilter<PlayerTag> _player;
        private readonly EcsFilter<PlayerTag, HandPositioningGrabCounter> _playerWithCounter;
        private readonly EcsFilter<HandPositioningObjectData> _objectData;

        private readonly TutorialConfig _tutorialConfig;

        public HandPositioningStateGrabHandler(TutorialConfig tutorialConfig)
        {
            _tutorialConfig = tutorialConfig;
        }

        public void Run()
        {
            if (!_enter.IsEmpty())
            {
                foreach (var i in _player)
                {
                    ref var counter = ref _player.GetEntity(i).Get<HandPositioningGrabCounter>();
                    counter.Count = 0;
                    counter.MaxCount = _tutorialConfig.MaxHandPositioningGrabCount;
                }

                foreach (var i in _objectData)
                {
                    _objectData.Get1(i).HandsModelAttach.gameObject.SetActive(true);
                }
            }

            foreach (var i in _objectData)
            {
                if (!_objectData.Get1(i).LeftInteractable.isSelected)
                    _objectData.Get1(i).LeftHandModel.SetActive(true);
                
                if (!_objectData.Get1(i).RightInteractable.isSelected)
                    _objectData.Get1(i).RightHandModel.SetActive(true);
            }
            
            if (!_exit.IsEmpty())
            {
                foreach (var i in _playerWithCounter)
                {
                    _playerWithCounter.GetEntity(i).Del<HandPositioningGrabCounter>();
                }
                
                foreach (var i in _objectData)
                {
                    _objectData.Get1(i).HandsModelAttach.gameObject.SetActive(false);
                }
            }
        }
    }
}