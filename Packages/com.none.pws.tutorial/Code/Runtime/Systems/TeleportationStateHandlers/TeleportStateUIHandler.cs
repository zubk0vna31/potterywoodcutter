using Leopotam.Ecs;
using Modules.StateGroup.Components;
using Modules.UIMessaging.Data;
using Modules.UIMessaging.Messages;
using PWS.Tutorial.States;

namespace PWS.Tutorial
{
    public class TeleportStateUIHandler : IEcsRunSystem
    {
        // auto injected fields
        private readonly EcsFilter<TeleportState, StateEnter> _enterMoveState;
        private readonly EcsFilter<TeleportState, StateExit> _exitMoveState;
        private readonly EcsFilter<TeleportState> _inState;
        private readonly EcsFilter<TeleportationSignal, TeleportationCounter> _performed;

        private readonly TargetActionPair _showTooltipUI;
        private readonly TargetActionPair _hideTooltipUI;
        private readonly TargetActionPair _onTargetActionsCountAchieved;
        
        public TeleportStateUIHandler(TargetActionPair showTooltipUI, TargetActionPair hideTooltipUI, TargetActionPair onTargetActionsCountAchieved)
        {
            _showTooltipUI = showTooltipUI;
            _hideTooltipUI = hideTooltipUI;
            _onTargetActionsCountAchieved = onTargetActionsCountAchieved;
        }

        public void Run()
        {
            if (!_enterMoveState.IsEmpty())
            {
                UniRx.MessageBroker.Default.Publish(new PerformUIActionMessage(_showTooltipUI));
            }

            if (!_inState.IsEmpty())
            {
                foreach (var i in _performed)
                {
                    if (_performed.Get2(i).MaxCount == _performed.Get2(i).Count)
                    {
                        UniRx.MessageBroker.Default.Publish(new PerformUIActionMessage(_onTargetActionsCountAchieved));
                        return;
                    }
                }
            }

            if (!_exitMoveState.IsEmpty())
            {
                UniRx.MessageBroker.Default.Publish(new PerformUIActionMessage(_hideTooltipUI));
            }
        }
    }
}