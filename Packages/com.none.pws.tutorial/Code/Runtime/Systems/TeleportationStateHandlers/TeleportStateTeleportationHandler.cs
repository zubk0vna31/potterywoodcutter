using Leopotam.Ecs;
using Modules.StateGroup.Components;
using PWS.Tutorial.States;

namespace PWS.Tutorial
{
    // handles teleportation tracking during move state of tutorial
    // transits to NextStateT once required amount of teleportations achieved 
    public class TeleportStateTeleportationHandler : IEcsRunSystem
    {
        // auto injected fields
        private readonly EcsFilter<TeleportState, StateEnter> _enter;
        private readonly EcsFilter<TeleportState, StateExit> _exit;
        private readonly EcsFilter<PlayerTag> _player;
        private readonly EcsFilter<PlayerTag, TeleportationCounter> _playerWithCounter;

        private readonly TutorialConfig _tutorialConfig;

        public TeleportStateTeleportationHandler(TutorialConfig tutorialConfig)
        {
            _tutorialConfig = tutorialConfig;
        }

        public void Run()
        {
            if (!_enter.IsEmpty())
            {
                // add teleportation counter to player
                foreach (var i in _player)
                {
                    ref var counter = ref _player.GetEntity(i).Get<TeleportationCounter>();
                    counter.Count = 0;
                    counter.MaxCount = _tutorialConfig.MaxTeleportationsCount;
                }
            }

            if (!_exit.IsEmpty())
            {
                // remove teleportation counter from player
                foreach (var i in _playerWithCounter)
                {
                    _playerWithCounter.GetEntity(i).Del<TeleportationCounter>();
                }
            }
        }
    }
}