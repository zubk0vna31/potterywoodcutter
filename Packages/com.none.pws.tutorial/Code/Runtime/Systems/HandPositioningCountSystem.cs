﻿using Leopotam.Ecs;
using UnityEngine;

namespace PWS.Tutorial
{
    public class HandPositioningCountSystem : IEcsRunSystem
    {
        readonly EcsFilter<HandPositioningSignal> _signal;
        readonly EcsFilter<HandPositioningGrabCounter> _counter;

        public void Run()
        {
            if (_signal.IsEmpty())
                return;

            foreach (var i in _counter)
            {
                _counter.Get1(i).Count++;
                Debug.Log("HandPositioningGrabCounter++");
            }
        }
    }
}