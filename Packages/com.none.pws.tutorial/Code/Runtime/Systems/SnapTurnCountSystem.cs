using Leopotam.Ecs;

namespace PWS.Tutorial
{
    public class SnapTurnCountSystem : IEcsRunSystem
    {
        // auto injected fields
        readonly EcsFilter<SnapTurnSignal, SnapTurnCounter> _signal;

        public void Run()
        {
            if (_signal.IsEmpty())
                return;

            foreach (var i in _signal)
            {
                _signal.Get2(i).Count += 1;
                UnityEngine.Debug.Log("Turned++");
            }
        }
    }
}
