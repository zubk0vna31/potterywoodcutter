using Leopotam.Ecs;
using Modules.StateGroup.Components;
using PWS.Tutorial.States;

namespace PWS.Tutorial
{
    // handles grabs amount tracking during move state of tutorial
    // transits to NextStateT once required amount of teleportations achieved 
    public class InteractionStateGrabsHandler : IEcsRunSystem
    {
        // auto injected fields
        private readonly EcsFilter<InteractionState, StateEnter> _enter;
        private readonly EcsFilter<InteractionState, StateExit> _exit;
        private readonly EcsFilter<PlayerTag> _player;
        private readonly EcsFilter<PlayerTag, GrabCounter> _playerWithCounter;

        private readonly TutorialConfig _tutorialConfig;

        public InteractionStateGrabsHandler(TutorialConfig tutorialConfig)
        {
            _tutorialConfig = tutorialConfig;
        }

        public void Run()
        {
            if (!_enter.IsEmpty())
            {
                // add grab counter to player
                foreach (var i in _player)
                {
                    ref var counter = ref _player.GetEntity(i).Get<GrabCounter>();
                    counter.Count = 0;
                    counter.MaxCount = _tutorialConfig.MaxGrabCount;
                }
            }

            if (!_exit.IsEmpty())
            {
                // remove grab counter from player
                foreach (var i in _playerWithCounter)
                {
                    _playerWithCounter.GetEntity(i).Del<GrabCounter>();
                }
            }
        }
    }
}