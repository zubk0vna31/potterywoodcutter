using Leopotam.Ecs;
using Modules.StateGroup.Components;
using PWS.Tutorial.States;

namespace PWS.Tutorial
{
    // handles LeapMove tracking during move state of tutorial
    // transits to NextStateT once required amount of LeapMove achieved 
    public class LeapMoveStateMoveHandler : IEcsRunSystem
    {
        // auto injected fields
        private readonly EcsFilter<LeapMoveState, StateEnter> _enter;
        private readonly EcsFilter<LeapMoveState, StateExit> _exit;
        private readonly EcsFilter<PlayerTag> _player;
        private readonly EcsFilter<PlayerTag, LeapMoveCounter> _playerWithCounter;

        private readonly TutorialConfig _tutorialConfig;

        public LeapMoveStateMoveHandler(TutorialConfig tutorialConfig)
        {
            _tutorialConfig = tutorialConfig;
        }

        public void Run()
        {
            if (!_enter.IsEmpty())
            {
                // add LeapMove counter to player
                foreach (var i in _player)
                {
                    ref var counter = ref _player.GetEntity(i).Get<LeapMoveCounter>();
                    counter.Count = 0;
                    counter.MaxCount = _tutorialConfig.MaxLeapMoveCount;
                }
            }

            if (!_exit.IsEmpty())
            {
                // remove LeapMove counter from player
                foreach (var i in _playerWithCounter)
                {
                    _playerWithCounter.GetEntity(i).Del<LeapMoveCounter>();
                }
            }
        }
    }
}