using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace PWS.Tutorial
{
    public class TipWindow : MonoBehaviour
    {
        [Header("View")]
        [SerializeField] private GameObject _canvas;

        [Header("If Next button needed")]
        [SerializeField] private Button _nextButton;

        [SerializeField] private UnityEvent _nextEvent;

        private void Start()
        {
            Hide();
        }

        public void Show()
        {
            _canvas.SetActive(true);

            //ShowButton();
        }

        public void Hide()
        {
            _canvas.SetActive(false);

            HideButton();
        }

        void OnClickNext()
        {
            _nextEvent.Invoke();
        }

        public void ShowButton()
        {
            if (!_nextButton) return;

            _nextButton.onClick.AddListener(OnClickNext);
            _nextButton.gameObject.SetActive(true);
        }
        public void HideButton()
        {
            if (!_nextButton) return;

            _nextButton.onClick.RemoveListener(OnClickNext);
            _nextButton.gameObject.SetActive(false);
        }

    }
}
