﻿using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace PWS.Tutorial
{
    public struct HandPositioningObjectData
    {
        public Transform LeftControllerModelParent;
        public Transform RightControllerModelParent;
        public Transform HandsModelAttach;
        public Rigidbody ObjectRigidbody;
        public XRGrabInteractable LeftInteractable;
        public XRGrabInteractable RightInteractable;
        public GameObject LeftHandModel;
        public GameObject RightHandModel;
        public bool ObjectWasMoved;
    }
}