using Leopotam.Ecs;

namespace PWS.Tutorial
{
    public struct LeapMoveCounter
    {
        public int Count;
        public int MaxCount;
    }
}
