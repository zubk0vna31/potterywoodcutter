﻿using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace PWS.Tutorial
{
    public struct ToolComponent
    {
        public XRBaseInteractable Interactable;
        public Animator Animator;
    }
}