using Leopotam.Ecs;

namespace PWS.Tutorial
{
    public struct GrabCounter
    {
        public int Count;
        public int MaxCount;
    }
}
