﻿using UnityEngine.XR.Interaction.Toolkit;

namespace PWS.Tutorial
{
    public struct HandPositioningHandData
    {
        public XRGrabInteractable Interactable;
        public bool Activated;
    }
}