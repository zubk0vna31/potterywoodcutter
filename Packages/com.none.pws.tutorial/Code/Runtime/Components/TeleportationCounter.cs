using Leopotam.Ecs;

namespace PWS.Tutorial
{
    public struct TeleportationCounter
    {
        public int Count;
        public int MaxCount;
    }
}
