﻿namespace PWS.Tutorial
{
    public struct InstrumentRunCounter
    {
        public int Count;
        public int MaxCount;
    }
}