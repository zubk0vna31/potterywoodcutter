using Leopotam.Ecs;

namespace PWS.Tutorial
{
    public struct SnapTurnCounter
    {
        public int Count;
        public int MaxCount;
    }
}
