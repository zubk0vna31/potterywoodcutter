using Leopotam.Ecs;
using Modules.Root.ContainerComponentModel;
using Modules.Root.ECS;
using Modules.UIMessaging;
using Modules.UIMessaging.Data;
using PWS.Tutorial.Systems;
using UnityEngine;
using PWS.Tutorial.States;

namespace PWS.Tutorial
{
    internal class TutorialSystemsDependencies
    {
        [Inject] public ITutorialService TutorialService;

        [Inject] public IStringToIDMappingService stringToIdMapping;
    }

    public class TutorialSystemsProvider : MonoBehaviour, ISystemsProvider
    {
        [Header("Configs")] 
        [SerializeField] private TutorialConfig _tutorialConfig;
        
        [Header("Messages")]
        [SerializeField] private StringReference _showId;
        [SerializeField] private StringReference _hideId;
        [SerializeField] private StringReference _showButtonId;
        [SerializeField] private StringReference _nextId;

        [Header("Teleport Canvas")]
        [SerializeField] private StringReference _teleportCanvasId;

        [Header("Leap Move Canvas")]
        [SerializeField] private StringReference _leapMoveCanvasId;

        [Header("Snap Turn Canvas")]
        [SerializeField] private StringReference _snapTurnCanvasId;

        [Header("Interaction Canvas")]
        [SerializeField] private StringReference _interactionCanvasId;
        
        [Header("Working With Instruments Canvas")]
        [SerializeField] private StringReference _workingWithInstrumentsCanvasId;
        
        [Header("Hand Positioning Canvas")]
        [SerializeField] private StringReference _handPositioningCanvasId;

        [Header("Final Canvas")]
        [SerializeField] private StringReference _finalCanvasId;

        public EcsSystems GetSystems(EcsWorld world, EcsSystems endFrame, EcsSystems mainSystems)
        {
            TutorialSystemsDependencies dependencies = new TutorialSystemsDependencies();
            SceneContainer.Instance.Inject(dependencies);

            EcsSystems systems = new EcsSystems(world);
            
            //teleport
            var teleportShow = dependencies.stringToIdMapping.GetTargetActionPair(_teleportCanvasId, _showId);
            var teleportHide = dependencies.stringToIdMapping.GetTargetActionPair(_teleportCanvasId, _hideId);
            var teleportShowButton = dependencies.stringToIdMapping.GetTargetActionPair(_teleportCanvasId, _showButtonId);
            var teleportNext = dependencies.stringToIdMapping.GetTargetActionPair(_teleportCanvasId, _nextId);

            //Leap Move
            var leapMoveShow = dependencies.stringToIdMapping.GetTargetActionPair(_leapMoveCanvasId, _showId);
            var leapMoveHide = dependencies.stringToIdMapping.GetTargetActionPair(_leapMoveCanvasId, _hideId);
            var leapMoveShowButton = dependencies.stringToIdMapping.GetTargetActionPair(_leapMoveCanvasId, _showButtonId);
            var leapMoveNext = dependencies.stringToIdMapping.GetTargetActionPair(_leapMoveCanvasId, _nextId);

            //Snap Turn
            var snapTurnShow = dependencies.stringToIdMapping.GetTargetActionPair(_snapTurnCanvasId, _showId);
            var snapTurnHide = dependencies.stringToIdMapping.GetTargetActionPair(_snapTurnCanvasId, _hideId);
            var snapTurnShowButton = dependencies.stringToIdMapping.GetTargetActionPair(_snapTurnCanvasId, _showButtonId);
            var snapTurnNext = dependencies.stringToIdMapping.GetTargetActionPair(_snapTurnCanvasId, _nextId);

            //interaction
            var interactionShow = dependencies.stringToIdMapping.GetTargetActionPair(_interactionCanvasId, _showId);
            var interactionHide = dependencies.stringToIdMapping.GetTargetActionPair(_interactionCanvasId, _hideId);
            var interactionShowButton = dependencies.stringToIdMapping.GetTargetActionPair(_interactionCanvasId, _showButtonId);
            var interactionNext = dependencies.stringToIdMapping.GetTargetActionPair(_interactionCanvasId, _nextId);
            
            //working with instruments
            var workingWithInstrumentsShow = dependencies.stringToIdMapping.GetTargetActionPair(_workingWithInstrumentsCanvasId, _showId);
            var workingWithInstrumentsHide = dependencies.stringToIdMapping.GetTargetActionPair(_workingWithInstrumentsCanvasId, _hideId);
            var workingWithInstrumentsShowButton = dependencies.stringToIdMapping.GetTargetActionPair(_workingWithInstrumentsCanvasId, _showButtonId);
            var workingWithInstrumentsNext = dependencies.stringToIdMapping.GetTargetActionPair(_workingWithInstrumentsCanvasId, _nextId);
            
            //hand positioning
            var handPositioningShow = dependencies.stringToIdMapping.GetTargetActionPair(_handPositioningCanvasId, _showId);
            var handPositioningHide = dependencies.stringToIdMapping.GetTargetActionPair(_handPositioningCanvasId, _hideId);
            var handPositioningShowButton = dependencies.stringToIdMapping.GetTargetActionPair(_handPositioningCanvasId, _showButtonId);
            var handPositioningNext = dependencies.stringToIdMapping.GetTargetActionPair(_handPositioningCanvasId, _nextId);

            //final
            var finalShow = dependencies.stringToIdMapping.GetTargetActionPair(_finalCanvasId, _showId);
            var finalHide = dependencies.stringToIdMapping.GetTargetActionPair(_finalCanvasId, _hideId);

            systems
                .Add(new InitStateSpawner(dependencies.TutorialService))

                // interactors
                .Add(new TeleportationCountSystem())
                .Add(new LeapMoveCountSystem())
                .Add(new SnapTurnCountSystem())
                .Add(new GrabCountSystem())
                .Add(new InstrumentRunCountSystem())
                
                // teleport tutorial state handlers
                .Add(new TransitToStateOnUIActionSystem<TeleportState, LeapMoveState>(teleportNext))
                .Add(new TeleportStateTeleportationHandler(_tutorialConfig))
                .Add(new TeleportStateUIHandler(teleportShow, teleportHide, teleportShowButton))

                // leap move tutorial state handlers
                .Add(new TransitToStateOnUIActionSystem<LeapMoveState, SnapTurnState>(leapMoveNext))
                .Add(new LeapMoveStateMoveHandler(_tutorialConfig))
                .Add(new LeapMoveStateUIHandler(leapMoveShow, leapMoveHide, leapMoveShowButton))

                // snap turn tutorial state handlers
                .Add(new TransitToStateOnUIActionSystem<SnapTurnState, InteractionState>(snapTurnNext))
                .Add(new SnapTurnStateTurnHandler(_tutorialConfig))
                .Add(new SnapTurnStateUIHandler(snapTurnShow, snapTurnHide, snapTurnShowButton))

                // grab tutorial state handlers
                .Add(new TransitToStateOnUIActionSystem<InteractionState, WorkingWithInstrumentsState>(interactionNext))
                .Add(new InteractionStateGrabsHandler(_tutorialConfig))
                .Add(new InteractionStateUIHandler(interactionShow, interactionHide, interactionShowButton))
                
                // working with instruments tutorial state handlers
                .Add(new TransitToStateOnUIActionSystem<WorkingWithInstrumentsState, HandPositioningState>(workingWithInstrumentsNext))
                .Add(new WorkingWithInstrumentsStateInstrumentRunHandler(_tutorialConfig))
                .Add(new WorkingWithInstrumentsStateUIHandler(workingWithInstrumentsShow, workingWithInstrumentsHide, workingWithInstrumentsShowButton))
                .Add(new ActivateInstrumentHandler())
                
                // hand positioning tutorial handlers
                .Add(new HandPositioningStateMoveObjectHandler())
                .Add(new TransitToStateOnUIActionSystem<HandPositioningState, FinalState>(handPositioningNext))
                .Add(new HandPositioningStateGrabHandler(_tutorialConfig))
                .Add(new HandPositioningStateUIHandler(handPositioningShow, handPositioningHide, handPositioningShowButton))

                
                // final state handlers
                .Add(new FinalStateUIProcessing(finalShow, finalHide))
                .Add(new PlayerOpenMenuDoneProcessing(finalHide, dependencies.TutorialService))
                ;
                

            endFrame
                .Add(new HandPositioningCountSystem())
                .OneFrame<TeleportationSignal>()
                .OneFrame<LeapMoveSignal>()
                .OneFrame<SnapTurnSignal>()
                .OneFrame<GrabSignal>()
                .OneFrame<InstrumentRunSignal>()
                .OneFrame<InstrumentStopSignal>()
                .OneFrame<HandPositioningSignal>()
                .OneFrame<OpenMenuSignal>();

            return systems;
        }

    }
}
