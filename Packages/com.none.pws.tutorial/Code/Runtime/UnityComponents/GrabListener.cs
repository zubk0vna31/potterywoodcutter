using Leopotam.Ecs;
using Modules.ViewHub;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.XR.Interaction.Toolkit;

namespace PWS.Tutorial
{
    public class GrabListener : ViewComponent
    {
        EcsEntity _entity;

        [Header("Hands")]
        [SerializeField] private XRRayInteractor leftHandGrab;
        [SerializeField] private XRRayInteractor rightHandGrab;

        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            XRRig xrRig = GetComponent<XRRig>();

            _entity = ecsEntity;

            if(leftHandGrab)
                leftHandGrab.selectExited.AddListener(OnGrab);
            if (rightHandGrab)
                rightHandGrab.selectExited.AddListener(OnGrab);
        }

        [ContextMenu("OnGrab")]
        public void OnGrab(SelectExitEventArgs args)
        {
            _entity.Get<GrabSignal>();
        }
    }
}
