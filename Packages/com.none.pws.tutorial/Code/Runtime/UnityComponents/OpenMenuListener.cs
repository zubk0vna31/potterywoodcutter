using Leopotam.Ecs;
using Modules.ViewHub;
using PWS.Common.QuickMenu;
using UnityEngine;

namespace PWS.Tutorial
{
    public class OpenMenuListener : ViewComponent
    {
        EcsEntity _entity;

        [SerializeField] private QuickMenu quickMenu;

        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            _entity = ecsEntity;
            quickMenu.OnMenuOpenedEvent.AddListener(OnMenuOpened);
        }

        [ContextMenu("OnMenuOpened")]
        public void OnMenuOpened()
        {
            _entity.Get<OpenMenuSignal>();
        }
    }
}
