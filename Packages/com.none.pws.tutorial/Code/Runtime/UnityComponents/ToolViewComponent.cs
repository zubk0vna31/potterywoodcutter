﻿using Leopotam.Ecs;
using Modules.ViewHub;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace PWS.Tutorial
{
    public class ToolViewComponent : ViewComponent
    {
        [SerializeField] private XRGrabInteractable _interactable;
        [SerializeField] private Animator _animator;
        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            ref var entity = ref ecsEntity.Get<ToolComponent>();

            entity.Interactable = _interactable;
            entity.Animator = _animator;
        }
    }
}