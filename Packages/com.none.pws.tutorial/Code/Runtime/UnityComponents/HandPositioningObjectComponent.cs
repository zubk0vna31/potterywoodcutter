﻿using Leopotam.Ecs;
using Modules.ViewHub;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace PWS.Tutorial
{
    public class HandPositioningObjectComponent : ViewComponent
    {
        [SerializeField] private Transform _leftControllerModelParent; 
        [SerializeField] private Transform _rightControllerModelParent;
        [SerializeField] private Transform _handsModelAttach;
        [SerializeField] private Rigidbody _objectRigidbody;
        [SerializeField] private XRGrabInteractable _leftInteractable;
        [SerializeField] private XRGrabInteractable _rightInteractable;
        [SerializeField] private GameObject _leftHandModel;
        [SerializeField] private GameObject _rightHandModel;
        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            ref var entity = ref ecsEntity.Get<HandPositioningObjectData>();
            entity.LeftControllerModelParent = _leftControllerModelParent;
            entity.RightControllerModelParent = _rightControllerModelParent;
            entity.HandsModelAttach = _handsModelAttach;
            entity.ObjectRigidbody = _objectRigidbody;
            entity.ObjectWasMoved = false;
            entity.LeftInteractable = _leftInteractable;
            entity.RightInteractable = _rightInteractable;
            entity.LeftHandModel = _leftHandModel;
            entity.RightHandModel = _rightHandModel;

        }
    }
}