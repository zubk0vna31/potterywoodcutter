using Leopotam.Ecs;
using Modules.ViewHub;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace PWS.Tutorial
{
    public class SnapTurnListener : ViewComponent
    {
        EcsEntity _entity;

        [SerializeField] private SnapTurnProviderBase _provider;

        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            _entity = ecsEntity;

            _provider.endLocomotion += OnSnapTurned;
        }

        [ContextMenu("OnSnapTurned")]
        private void OnSnapTurned(LocomotionSystem obj)
        {
            _entity.Get<SnapTurnSignal>();
        }

        public void OnDestroy()
        {
            _provider.endLocomotion -= OnSnapTurned;
        }
    }
}
