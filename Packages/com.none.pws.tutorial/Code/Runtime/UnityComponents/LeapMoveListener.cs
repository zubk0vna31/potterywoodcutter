using Leopotam.Ecs;
using Modules.ViewHub;
using Modules.VRFeatures;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace PWS.Tutorial
{
    public class LeapMoveListener : ViewComponent
    {
        EcsEntity _entity;

        [SerializeField] private LeapMoveProvider _provider;

        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            _entity = ecsEntity;

            _provider.endLocomotion += OnLeapMoved;
        }

        [ContextMenu("OnLeapMoved")]
        private void OnLeapMoved(LocomotionSystem obj)
        {
            _entity.Get<LeapMoveSignal>();
        }

        public void OnDestroy()
        {
            _provider.endLocomotion -= OnLeapMoved;
        }
    }
}
