﻿using Leopotam.Ecs;
using Modules.ViewHub;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace PWS.Tutorial
{
    public class InstrumentRunListener : ViewComponent
    {
        private EcsEntity _entity;
        
        [SerializeField] private XRBaseInteractor _rightHandInteractor;
        [SerializeField] private XRBaseInteractor _leftHandInteractor;
        
        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            _entity = ecsEntity;
            
            _rightHandInteractor.selectEntered.AddListener(arg =>
            {
                _rightHandInteractor.selectTarget.activated.AddListener(SetInstrumentRunSignal);
                _rightHandInteractor.selectTarget.deactivated.AddListener(SetInstrumentStopSignal);
            });
            _leftHandInteractor.selectEntered.AddListener(arg =>
            {
                _leftHandInteractor.selectTarget.activated.AddListener(SetInstrumentRunSignal);
                _leftHandInteractor.selectTarget.deactivated.AddListener(SetInstrumentStopSignal);
            });
        }
        private void SetInstrumentRunSignal(ActivateEventArgs args)
        {
            _entity.Get<InstrumentRunSignal>();
        }

        private void SetInstrumentStopSignal(DeactivateEventArgs args)
        {
            _entity.Get<InstrumentStopSignal>();
        }
    }
}