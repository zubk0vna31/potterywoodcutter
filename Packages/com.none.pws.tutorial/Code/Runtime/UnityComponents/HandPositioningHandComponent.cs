﻿using Leopotam.Ecs;
using Modules.ViewHub;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace PWS.Tutorial
{
    public class HandPositioningHandComponent : ViewComponent
    {
        private EcsEntity _entity;
        
        [SerializeField] private XRGrabInteractable _thisInteractable;
        [SerializeField] private Transform _interactorModelParent;
        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            _entity = ecsEntity;
            ref var entity = ref _entity.Get<HandPositioningHandData>();
            entity.Interactable = _thisInteractable;
            entity.Activated = false;
           ;
            
            _thisInteractable.selectEntered.AddListener(arg =>
            {
                _interactorModelParent.gameObject.SetActive(false);
               _entity.Get<HandPositioningHandData>().Activated = true;

            });
            
            _thisInteractable.selectExited.AddListener(arg =>
            {
                _interactorModelParent.gameObject.SetActive(true);
                _entity.Get<HandPositioningHandData>().Activated = false;
            });
        }
    }
}