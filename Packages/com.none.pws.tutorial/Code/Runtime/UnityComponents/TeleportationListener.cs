using Leopotam.Ecs;
using Modules.ViewHub;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace PWS.Tutorial
{
    public class TeleportationListener : ViewComponent
    {
        EcsEntity _entity;

        [SerializeField] private TeleportationProvider _teleportationProvider;

        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            _entity = ecsEntity;

            _teleportationProvider.endLocomotion += OnTeleported;
        }

        [ContextMenu("OnTeleported")]
        private void OnTeleported(LocomotionSystem obj)
        {
            _entity.Get<TeleportationSignal>();
        }

        public void OnDestroy()
        {
            _teleportationProvider.endLocomotion -= OnTeleported;
        }
    }
}
