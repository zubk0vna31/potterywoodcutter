using Leopotam.Ecs;
using Modules.ViewHub;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PWS.Tutorial
{
    public class PlayerTemplate : EntityTemplate
    {
        protected override void SetupEntity(EcsEntity entity, EcsWorld world)
        {
            base.SetupEntity(entity, world);
            entity.Get<PlayerTag>();
        }
    }
}
