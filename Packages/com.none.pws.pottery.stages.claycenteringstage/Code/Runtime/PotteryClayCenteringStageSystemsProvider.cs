using Leopotam.Ecs;
using Modules.Root.ContainerComponentModel;
using Modules.Root.ECS;
using Modules.StateGroup.Core;
using Modules.VRFeatures;
using PWS.Common.Audio;
using PWS.Common.Messages;
using PWS.Common.RestartStage;
using PWS.Common.ResultsEvaluation;
using PWS.Common.TravelPoints;
using PWS.Common.UI;
using PWS.Pottery.Common;
using PWS.Pottery.ItemDataHolderService;
using PWS.Pottery.MeshGeneration;
using PWS.Pottery.Sculpting.Interactor;
using UnityEngine;

namespace PWS.Pottery.Stages.ClayCenteringStage
{
    public class PotteryClayCenteringStageSystemsProvider : MonoBehaviour, ISystemsProvider
    {
        internal class SystemsDependencies
        {
            [Inject] public ISculptingDataHolder SculptingData;
            [Inject] public IValueRegulatorDataHolder<WheelSpeedRegulator> WheelSpeedData;
            [Inject] public IResultsEvaluationDataHolder ResultsEvaluationData;
        }

        [SerializeField] private SetStateSOCommand _nextState;
        [SerializeField] private ClayCenteringStateConfig _config;
        [SerializeField] private StateInfoData _stateInfo;
        
        [Header("Voice config")] 
        //[SerializeField] private VoiceConfig _voiceFallConfig;
        [SerializeField] private VoiceConfig _voicePushConfig;

        public EcsSystems GetSystems(EcsWorld world, EcsSystems endFrame, EcsSystems mainSystems)
        {
            SystemsDependencies dependencies = new SystemsDependencies();
            SceneContainer.Instance.Inject(dependencies);

            EcsSystems systems = new EcsSystems(world);
            systems
                .Add(new TransitToStateOnUIActionSystem<ClayCenteringStatePush>(_nextState))
                .Add(new CurrentStateRestoreDataProcessing<ClayCenteringStatePush>())
                /*
                //fall state systems
                .Add(new GameObjectVisibilityProcessing<ClayCenteringStateFall, ClayCenteringContainer>())

                .Add(new ClayCenteringInitProcessing())
                .Add(new ClayCenteringGrabProcessing(_config))
                .Add(new ClayCenteringHeightTriggerProcessing(_config))

                .Add(new GameObjectVisibilityProcessing<ClayCenteringStateFall, ClayCenteringSphere>())

                //ui
                .Add(new StateInfoWindowUIProcessing<ClayCenteringStateFall>(_stateInfo))

                .Add(new XRInteractableProcessing<ClayCenteringStateFall, ClayCenteringSphere>())
                */

                //push state systems
                //.Add(new GameObjectVisibilityDoubleStateProcessing<ClayCenteringStatePush, ClayCenteringContainer, ClayCenteringStateFall>())
                .Add(new GameObjectVisibilityProcessing<ClayCenteringStatePush, ClayCenteringContainer>())
                .Add(new HandInteractorVisibiltySystem<ClayCenteringStatePush>(SculptingHand.ClayCentering))

                .Add(new ClayCenteringRotationProcessing<ClayCenteringStatePush>(dependencies.WheelSpeedData))
                .Add(new ClayCenteringHandsInitPosition(dependencies.SculptingData, _config))
                .Add(new HandInteractorPositionSystem<ClayCenteringStatePush>(_config.HandMoveAxes, _config.HandSmoothSpeed, false))
                .Add(new ClayCenteringInteractableProcessing(_config))

                .Add(new ClayCenteringCompletionTracker(_config))

                .Add(new SetGradeByProgress<ClayCenteringStatePush>(dependencies.ResultsEvaluationData, _stateInfo))

                //ui
                .Add(new StateInfoWindowUIProcessing<ClayCenteringStatePush>(_stateInfo))
                .Add(new StateWindowPlacementProcessing<ClayCenteringStatePush>("Wheel"))
                .Add(new StateWindowProgressUIProcessing<ClayCenteringStatePush>())
                .Add(new StateWindowButtonUIProcessing<ClayCenteringStatePush>())

                .Add(new MeshVisibilityProcessing<ClayCenteringStatePush, SculptingProductMesherTag>())
                
                //voice
                //.Add(new VoiceAudioSystem<ClayCenteringStateFall>(_voiceFallConfig))
                .Add(new VoiceAudioSystem<ClayCenteringStatePush>(_voicePushConfig))
                
                .Add(new ClayCenteringUnbalancedSoundSystem())

                //travel point
                .Add(new TravelPointPositioningSystem<ClayCenteringStatePush>("Wheel"))

                ;

            endFrame

                //sculpting product
                .Add(new SculptingDataProcessingSystem<ClayCenteringStatePush>(dependencies.SculptingData))
                .Add(new MeshRingsDataProcessingSystem<ClayCenteringStatePush>(dependencies.SculptingData))
                .Add(new MeshUpdateSystem<ClayCenteringStatePush, SculptingProductMesherTag>(dependencies.SculptingData))

                .OneFrame<RestartCurrentStepMessage>()
                .OneFrame<NextStateSignal>()
                ;

            return systems;
        }
    }
}
