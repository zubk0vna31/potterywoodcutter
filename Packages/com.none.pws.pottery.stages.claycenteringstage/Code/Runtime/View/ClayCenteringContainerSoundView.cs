﻿using Leopotam.Ecs;
using Modules.ViewHub;
using UnityEngine;

namespace PWS.Pottery.Stages.ClayCenteringStage
{
    public class ClayCenteringContainerSoundView : ViewComponent
    {
        private const float MAX_PITCH = 2.5f;
        private const float MIN_PITCH = 0.2f;
        private const float MAX_VOLUME = 1.8f;
        private const float MIN_VOLUME = -0.30f;
        
        [SerializeField] private AudioClip _unbalancedClip;
        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            ref var entity = ref ecsEntity.Get<ClayCenteringSoundData>();
            entity.UnbalancedClip = _unbalancedClip;
            entity.MaxPitch = MAX_PITCH;
            entity.MinPitch = MIN_PITCH;
            entity.MaxVolume = MAX_VOLUME;
            entity.MinVolume = MIN_VOLUME;
        }
    }
}