using Leopotam.Ecs;
using Modules.ViewHub;

namespace PWS.Pottery.Stages.ClayCenteringStage
{
    public class ClayCenteringSphereView : ViewComponent
    {
        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            ecsEntity.Get<ClayCenteringSphere>();
        }
    }
}
