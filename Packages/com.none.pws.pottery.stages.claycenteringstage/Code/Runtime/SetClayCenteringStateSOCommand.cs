using Modules.StateGroup.Core;
using UnityEngine;

namespace PWS.Pottery.Stages.ClayCenteringStage
{
    [CreateAssetMenu(fileName = "SetClayCenteringStateSOCommand", menuName = "PWS/Pottery/Stages/ClayCenteringStage/SetStateSOCommand")]
    public class SetClayCenteringStateSOCommand : SetStateSOCommand<ClayCenteringStatePush>
    {

    }
}