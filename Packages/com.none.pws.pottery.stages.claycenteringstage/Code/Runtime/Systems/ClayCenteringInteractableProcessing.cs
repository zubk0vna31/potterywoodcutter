using Leopotam.Ecs;
using Modules.StateGroup.Components;
using Modules.ViewHub;
using PWS.Pottery.ItemDataHolderService;
using PWS.Pottery.MeshGeneration;
using PWS.Pottery.Sculpting.Interactor;
using UnityEngine;
using DG.Tweening;

namespace PWS.Pottery.Stages.ClayCenteringStage
{
    public class ClayCenteringInteractableProcessing : IEcsRunSystem
    {
        // auto injected fields
        readonly EcsFilter<ClayCenteringStatePush> _inState;
        readonly EcsFilter<StateExit, ClayCenteringStatePush> _exiting;

        readonly EcsFilter<UnityView, SculptingInteractorTag> _generalInteractor;
        readonly EcsFilter<UnityView, SculptingProductTag> _product;
        readonly EcsFilter<UnityView, HandTag> _hands;

        private ClayCenteringStateConfig _config;


        public ClayCenteringInteractableProcessing(ClayCenteringStateConfig config)
        {
            _config = config;
        }
        public void Run()
        {
            if (_inState.IsEmpty())
                return;

            bool move = true;
            foreach (var i in _hands)
            {
                move &= _hands.Get2(i).View.Interactable.isSelected;
            }

            foreach (var productIdx in _product)
            {
                if (move)
                {
                    foreach (var interactorIdx in _generalInteractor)
                    {
                        Vector3 pos = _generalInteractor.Get2(interactorIdx).Position;
                        pos.y = _product.Get1(productIdx).Transform.position.y;
                        _product.Get1(productIdx).Transform.position = pos;

                        Vector3 localPos = _product.Get1(productIdx).Transform.localPosition;
                        localPos = Vector3.ClampMagnitude(localPos, _config.WheelRadius);
                        _product.Get1(productIdx).Transform.localPosition = localPos;
                    }
                }

                if (!_exiting.IsEmpty())
                {
                    _product.Get1(productIdx).Transform.DOLocalMove(Vector3.zero, 0.1f);
                }
            }
        }
    }
}
