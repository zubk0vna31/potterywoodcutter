using Leopotam.Ecs;
using Modules.StateGroup.Components;
using Modules.ViewHub;
using PWS.Pottery.ItemDataHolderService;
using PWS.Pottery.MeshGeneration;
using PWS.Pottery.Sculpting.Interactor;
using UnityEngine;

namespace PWS.Pottery.Stages.ClayCenteringStage
{
    public class ClayCenteringHandsInitPosition : IEcsRunSystem
    {
        // auto injected fields
        readonly EcsFilter<StateEnter, ClayCenteringStatePush> _entering;
        readonly EcsFilter<ClayCenteringStatePush> _inState;

        readonly EcsFilter<UnityView, HandTag> _hands;
        readonly EcsFilter<UnityView, SculptingProductTag> _product;

        private ISculptingDataHolder _data;
        private ClayCenteringStateConfig _config;

        public ClayCenteringHandsInitPosition(ISculptingDataHolder data, ClayCenteringStateConfig config)
        {
            _data = data;
            _config = config;
        }

        public void Run()
        {
            if (!_inState.IsEmpty())
            {
                bool move = true;

                foreach (var i in _hands)
                {
                    move &= _hands.Get2(i).View.Interactable.isSelected;
                }

                if (!move)
                {
                    foreach (var productIdx in _product)
                    {
                        foreach (var idx in _hands)
                        {
                            ref var handsView = ref _hands.Get1(idx);
                            ref var handsTag = ref _hands.Get2(idx);
                            ref var prodact = ref _product.Get1(productIdx);
                            if (!_entering.IsEmpty())
                            {
                                handsView.Transform.position = handsTag.View.ClayCenteringHandVisual.transform.position;
                                handsTag.View.ClayCenteringHandVisual.transform.position = handsView.Transform.position;
                                handsTag.OriginLocalPosition = handsView.Transform.localPosition;
                            }
                            
                            Vector3 handPos = handsTag.OriginLocalPosition;
                            handPos = prodact.Transform.localRotation * handPos;

                            handsView.Transform.position = prodact.Transform.TransformPoint(handPos);
                        }
                    }
                }
            }
        }
    }
}
