﻿using Leopotam.Ecs;
using Modules.Audio;
using PWS.Common.UI;
using UnityEngine;

namespace PWS.Pottery.Stages.ClayCenteringStage
{
    public class ClayCenteringUnbalancedSoundSystem : IEcsRunSystem
    {
        private readonly EcsFilter<ClayCenteringStatePush> _inState;
        private readonly EcsFilter<StateProgress> _progress;
        private readonly EcsFilter<AudioSourceRef, ClayCenteringSoundData> _soundData;
        
        public void Run()
        {
           if (_inState.IsEmpty())
               return;

           foreach (var soundData in _soundData)
           {
               foreach (var progress in _progress)
               {
                   var progressValue = _progress.Get1(progress).Value;
                   ref var sourceRef = ref _soundData.Get1(soundData);
                   ref var soundConfig = ref _soundData.Get2(soundData);
               
                   if (!sourceRef.AudioSource.isPlaying)
                   {
                       sourceRef.AudioSource.clip = soundConfig.UnbalancedClip;
                       sourceRef.AudioSource.loop = true;
                       sourceRef.AudioSource.Play();
                   }

                   sourceRef.AudioSource.pitch = Mathf.Lerp(soundConfig.MinPitch, soundConfig.MaxPitch, progressValue);
                   sourceRef.AudioSource.volume = Mathf.Lerp(soundConfig.MaxVolume, soundConfig.MinVolume, progressValue);
               }
           }
        }
    }
}