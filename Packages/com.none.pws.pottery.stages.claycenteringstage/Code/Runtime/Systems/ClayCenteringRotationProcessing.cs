using Leopotam.Ecs;
using Modules.StateGroup.Components;
using Modules.ViewHub;
using PWS.Pottery.ItemDataHolderService;
using PWS.Pottery.MeshGeneration;
using PWS.Pottery.Sculpting.Interactor;
using UnityEngine;

namespace PWS.Pottery.Stages.ClayCenteringStage
{
    public class ClayCenteringRotationProcessing<StateT> : IEcsRunSystem where StateT : struct
    {
        // auto injected fields
        readonly EcsFilter<StateEnter, StateT> _entering;
        readonly EcsFilter<StateT> _inState;

        readonly EcsFilter<UnityView, HandTag> _hands;
        readonly EcsFilter<UnityView, SculptingProductTag> _product;

        private IValueRegulatorDataHolder<WheelSpeedRegulator> _data;

        //runtime data
        private Vector3 _initOffset;
        private float _rotation;
        private float _rpm;

        public ClayCenteringRotationProcessing(IValueRegulatorDataHolder<WheelSpeedRegulator> data)
        {
            _data = data;
        }

        public void Run()
        {
            if (!_entering.IsEmpty())
            {

                foreach (var product in _product)
                {
                    _initOffset = _product.Get1(product).Transform.localPosition;
                    _rotation = 0;
                }
            }

            if (!_inState.IsEmpty())
            {
                bool move = true;
                foreach (var i in _hands)
                {
                    move &= _hands.Get2(i).View.Interactable.isSelected;
                }

                if (move)
                {
                    foreach (var product in _product)
                    {
                        _initOffset = _product.Get1(product).Transform.localPosition;
                        _rotation = 0;
                    }
                }
                else
                {
                    _rpm = _data.ConvertToCustom();
                    float anglePerSec = _rpm * 360 * Time.deltaTime / 60;
                    _rotation = Mathf.Repeat(_rotation + anglePerSec, 360);
                    Vector3 newOffset = Quaternion.Euler(0, _rotation, 0) * _initOffset;

                    foreach (var product in _product)
                    {
                        _product.Get1(product).Transform.localPosition = newOffset;
                    }
                }
            }
        }
    }
}
