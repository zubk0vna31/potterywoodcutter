using Leopotam.Ecs;
using Modules.StateGroup.Components;
using Modules.ViewHub;
using Modules.VRFeatures;
using PWS.Common.UI;
using PWS.Pottery.MeshGeneration;
using PWS.Pottery.Sculpting.Interactor;
using UnityEngine;

namespace PWS.Pottery.Stages.ClayCenteringStage
{
    public class ClayCenteringCompletionTracker : IEcsRunSystem
    {
        // auto injected fields
        readonly EcsFilter<StateEnter, ClayCenteringStatePush> _entering;
        readonly EcsFilter<ClayCenteringStatePush> _inState;

        readonly EcsFilter<StateWindow, StateProgress> _stateWindowWithProgress;

        readonly EcsFilter<UnityView, XRInteractableComponents, SculptingProductTag> _product;
        readonly EcsFilter<UnityView, ClayCenteringContainer> _container;

        readonly EcsFilter<HandTag> _hands;

        private ClayCenteringStateConfig _config;

        //runtime data
        private float _distanceBetweenMaxPoints;

        public ClayCenteringCompletionTracker(ClayCenteringStateConfig config)
        {
            _config = config;
        }

        public void Run()
        {
            if (!_entering.IsEmpty())
            {
                _distanceBetweenMaxPoints = 0.3f;

                foreach (var i in _stateWindowWithProgress)
                {
                    _stateWindowWithProgress.Get2(i).Value = 0;
                    _stateWindowWithProgress.Get2(i).CompletionThreshold = _config.CompletionThreshold;
                }
            }

            if (!_inState.IsEmpty())
            {
                foreach (var productIdx in _product)
                {
                    foreach (var container in _container)
                    {
                        Vector3 localPos = _container.Get1(container).Transform.InverseTransformPoint(_product.Get1(productIdx).Transform.position);

                        float completion = 1 - Mathf.Clamp01(localPos.magnitude / _distanceBetweenMaxPoints);

                        foreach (var i in _stateWindowWithProgress)
                        {
                            _stateWindowWithProgress.Get2(i).Value = completion;
                        }
                    }
                }
            }
        }
    }
}
