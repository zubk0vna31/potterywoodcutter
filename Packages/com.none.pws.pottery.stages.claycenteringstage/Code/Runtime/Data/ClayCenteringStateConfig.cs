using PWS.Pottery.Sculpting.Interactor;
using UnityEngine;

namespace PWS.Pottery.Stages.ClayCenteringStage
{
    [CreateAssetMenu(fileName = "ClayCenteringStateConfig", menuName = "PWS/Pottery/Stages/ClayCenteringStage/Config", order = 0)]
    public class ClayCenteringStateConfig : ScriptableObject
    {
        public float CompletionThreshold = 0.9f;

        public float WheelRadius = 0.2f;

        [Header("Hands")]
        public InteractableParameters HandMoveAxes;
        public float HandSmoothSpeed = 5;
        public float ProductRadius = 0.05f;

    }
}
