﻿using UnityEngine;

namespace PWS.Pottery.Stages.ClayCenteringStage
{
    public struct ClayCenteringSoundData
    {
        public float MaxPitch;
        public float MinPitch;
        public float MaxVolume;
        public float MinVolume;
        
        public AudioClip UnbalancedClip;
    }
}