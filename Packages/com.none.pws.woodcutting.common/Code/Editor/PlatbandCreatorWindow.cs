using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace PWS.WoodCutting.Common.Editor
{
    public class SaveAssetCommand
    {
        public UnityEngine.Object @object;
        public Type @type;
        public bool overwrite;
        public string savePath;

        public SaveAssetCommand(UnityEngine.Object @object, Type @type, bool overwrite, string savePath)
        {
            this.@object = @object;
            this.@type = @type;
            this.overwrite = overwrite;
            this.savePath = savePath;
        }

        public void Execute()
        {
            if (!overwrite)
            {
                AssetDatabase.CreateAsset(@object, savePath);
            }
            else
            {
                EditorUtility.SetDirty(@object);
            }
        }
    }

    public class PlatbandCreatorWindow : EditorWindow
    {
        // Editor Only
        private const string fileStructurePath =
            "Assets/Prefabs/WoodCutting/FileStructure/";

        private const string prefabName =
            "Prefab.prefab";
        private const string finalPrefabName =
            "Final.prefab";
        private const string drillZoneName =
           "DrillZone.prefab";
        private const string triggerZoneName =
          "TriggerZone.prefab";
        private const string defaultPatternConfigPath =
           "Configs/DefaultPatternConfig.asset";
        private const string woodMaterialPath =
           "Materials/Wood.mat";
        private const string woodFinalMaterialPath =
           "Materials/WoodFinal.mat";
        private const string triggerZoneMaterialPath =
           "Materials/Chamfer.mat";

        private const string drilledPartsSearchName =
            "Drill";
        private const string sawedPartsSearcName =
            "Saw";
        private const string spritePivotSearchName =
            "Pivot";
        private const string vertexPathSearchName =
           "Path";
        private const string vertexPathStartPointSearchName =
          "Start";
        private const string vertexSubPathSearchName =
           "Subpath";
        private const string vertexSubPathStartPointSearchName =
          "Substart";
        private const string sawedFinalObjectSearchName =
           "State8";
        private const string chamferedFinalObjectSearchName =
           "State9";
        private const string chamferedMeshPartsSearchName =
           "Substate9";

        // Editor Data
        private string outputPath = "";
        private bool mainVertical, subVertical;
        private bool mainClockwise, subClockwise;

        // Add menu named "My Window" to the Window menu
        [MenuItem("Tools/Platband Creator")]
        public static void OpenWindow()
        {
            // Get existing open window or if none, make a new one:
            PlatbandCreatorWindow window = GetWindow<PlatbandCreatorWindow>();

            window.titleContent = new GUIContent("Platband Creator");
            window.minSize = new Vector2(500, 300);
            window.maxSize = new Vector2(500, 300);
            var pos = EditorGUIUtility.GetMainWindowPosition();
            window.position = new Rect(pos.width * 0.5f - 150, pos.height * 0.5f - 150, 300, 300);
            window.Show();
        }

        public void OnGUI()
        {
            if (GUILayout.Button("Select Platband Folder", GUILayout.Height(50)))
            {
                outputPath = EditorUtility.OpenFolderPanel("Selecting Folder", "", "");
            }

            EditorGUILayout.BeginHorizontal();

            EditorGUILayout.BeginHorizontal(GUILayout.Width(position.width * 0.5f));

            EditorGUILayout.LabelField("Main is verticaly");
            mainVertical = EditorGUILayout.Toggle(mainVertical);

            EditorGUILayout.EndHorizontal();


            if (mainVertical)
            {
                mainClockwise = EditorGUILayout.Toggle("Main Clockwise", mainClockwise);
            }

            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();

            EditorGUILayout.BeginHorizontal(GUILayout.Width(position.width * 0.5f));
            EditorGUILayout.LabelField("Sub is verticaly");
            subVertical = EditorGUILayout.Toggle(subVertical);
            EditorGUILayout.EndHorizontal();


            if (subVertical)
            {
                subClockwise = EditorGUILayout.Toggle("Sub Clockwise", subClockwise);
            }

            EditorGUILayout.EndHorizontal();



            bool pathEmpty = outputPath.Length == 0;

            var style = new GUIStyle();
            style.fontSize = pathEmpty ? 22 : 18;
            style.normal.textColor = pathEmpty ? Color.red : Color.green;

            style.wordWrap = true;
            style.alignment = TextAnchor.MiddleCenter;

            EditorGUILayout.LabelField(pathEmpty ? "Path is empty"
                : "Selected path: " + outputPath, style, GUILayout.ExpandHeight(true));

            if (GUILayout.Button("Generate", GUILayout.Height(50)))
            {
                if (outputPath.Equals(string.Empty))
                {
                    return;
                }

                // Clear path until meet <Assets> word. Need to use with asset data base class
                outputPath = outputPath.Substring(outputPath.IndexOf("Assets"));

                GeneratePlatband($"{outputPath}/{WoodCuttingPaths.mainPatternFolderName}");
                GeneratePlatband($"{outputPath}/{WoodCuttingPaths.subPatternFolderName}");
                SetupFinalPrefab(outputPath);

                AssetDatabase.SaveAssets();
                AssetDatabase.Refresh();

                EditorUtility.ClearProgressBar();
            }


        }

        private void GeneratePlatband(string outputPath)
        {
            bool main = outputPath.Contains("Main");

            // Load sprites
            Sprite solid = (Sprite)AssetDatabase.LoadAssetAtPath(outputPath +
                WoodCuttingPaths.inputSolidSpritePath, typeof(Sprite));
            Sprite dashed = (Sprite)AssetDatabase.LoadAssetAtPath(outputPath +
                WoodCuttingPaths.inputDashedSpritePath, typeof(Sprite));

            // Load additional pattern config if it exists
            PatternConfig patternConfig = (PatternConfig)AssetDatabase.LoadAssetAtPath(
                outputPath + WoodCuttingPaths.inputPatternConfigPath, typeof(PatternConfig));

            // If doesn't, then load default one
            if (patternConfig == null)
            {
                patternConfig = (PatternConfig)AssetDatabase.LoadAssetAtPath(
                fileStructurePath + defaultPatternConfigPath, typeof(PatternConfig));
            }

            // Initialize save assist data list
            Queue<SaveAssetCommand> saveAssetCommands = new Queue<SaveAssetCommand>();

            // Load sprite config
            saveAssetCommands.Enqueue(LoadScriptableObject(outputPath +
                WoodCuttingPaths.outputSpriteConfigName, typeof(SpriteConfig),
                out SpriteConfig spriteConfig));

            // Load mesh config
            saveAssetCommands.Enqueue(LoadScriptableObject(outputPath +
                WoodCuttingPaths.outputMeshConfigName, typeof(MeshConfig),
                out MeshConfig meshConfig));

            // Load vertex path configs
            saveAssetCommands.Enqueue(LoadScriptableObject(outputPath +
                WoodCuttingPaths.outputVertexPathConfigName, typeof(VertexPathConfig),
                out VertexPathConfig vertexPathConfig));

            // // Load path config for subpaths
            saveAssetCommands.Enqueue(LoadScriptableObject(outputPath +
                WoodCuttingPaths.subPathsConfigPath, typeof(PathsConfig),
                out PathsConfig subPathsConfig));

            // // // Delete every asset from sub path config
            if (!(subPathsConfig.Paths is null))
            {
                foreach (var path in subPathsConfig.Paths)
                {
                    AssetDatabase.DeleteAsset(path);
                }
            }

            // Fill sprite and mesh config with data
            spriteConfig.main = main;
            spriteConfig.solidPattern = solid;
            spriteConfig.dashedPattern = dashed;
            spriteConfig.solidRefPixelAmount = SpriteConfig.GetAlphaPixelAmountByThreshold(
                solid.texture, patternConfig.alphaThreshold);

            MeshConfig.Copy(meshConfig, Pattern.GenerateMeshConfig(
                spriteConfig, patternConfig.alphaThreshold));

            // Instantiate objects
            var loadedModel = AssetDatabase.LoadAssetAtPath(outputPath + WoodCuttingPaths.inputViewPath, typeof(GameObject));
            var model = (GameObject)PrefabUtility.InstantiatePrefab(loadedModel);

            var loadedPrefab = AssetDatabase.LoadAssetAtPath(fileStructurePath + prefabName, typeof(GameObject));
            var outputPrefab = (GameObject)PrefabUtility.InstantiatePrefab(loadedPrefab);
            outputPrefab.transform.position = outputPrefab.transform.eulerAngles = Vector3.zero;

            var view = outputPrefab.GetComponent<PlankFinalViewComponent>();

            model.transform.SetParent(outputPrefab.transform);
            model.transform.localPosition = model.transform.localEulerAngles = Vector3.zero;
            model.transform.SetAsLastSibling();

            Transform[] childs = model.GetComponentsInChildren<Transform>()
                .Where(x => x != outputPrefab).ToArray();

            // Fill configs with data
            // // Vertex path
            Transform mainPath = null;

            try
            {
                mainPath = childs.First(x => x.name.StartsWith(sawedPartsSearcName) && x.childCount < 2);
            }
            catch
            {
                Debug.Log("There are no \"Path\" object...");
            }

            vertexPathConfig.Copy(GenerateVertexPathConfigWithMesh(outputPath, mainPath, mainPath,
               patternConfig.meshThickness, out var mainMesh, out var pathTranform));


            if (pathTranform)
                pathTranform.gameObject.SetActive(false);

            // Delete existing mesh
            if ((Mesh)AssetDatabase.LoadAssetAtPath(outputPath+WoodCuttingPaths.outputMeshName, typeof(Mesh)))
            {
                AssetDatabase.DeleteAsset(WoodCuttingPaths.outputSubPathMeshName);
            }

            saveAssetCommands.Enqueue(new SaveAssetCommand(mainMesh, typeof(Mesh),
              false, outputPath + WoodCuttingPaths.outputMeshName));

            // // Vertex subpaths
            Transform[] subPaths = null;

            try
            {
                subPaths = childs.Where(x => x.name.StartsWith(sawedPartsSearcName) && x.childCount > 1 && x!=mainPath).ToArray();
            }
            catch
            {
                Debug.Log("There are no \"SubPath\" objects...");
            }

            int pathInsertIndex = WoodCuttingPaths.outputVertexSubPathConfigName.IndexOf(".asset");

            Dictionary<Transform, VertexPathConfig> sawedPartWithConfig = new Dictionary<Transform, VertexPathConfig>();
            CombineInstance[] combine = new CombineInstance[subPaths.Length + 1];

            int i = 1;
            float offset = 1f / (subPaths.Length + 1);
            float padding = 0.05f;
            string[] vertexSubPaths = new string[subPaths.Length];
            foreach (var subPath in subPaths)
            {
                string vertexPath = outputPath + WoodCuttingPaths.outputVertexSubPathConfigName.Insert(
                    pathInsertIndex, $"{i}");

                vertexSubPaths[i - 1] = vertexPath;

                var saveCommand = LoadScriptableObject(vertexPath, typeof(VertexPathConfig),
                    out VertexPathConfig vertexSubPathConfig);

                vertexSubPathConfig.Copy(GenerateVertexPathConfigWithMesh(outputPath, mainPath, subPath,
                   patternConfig.meshThickness, out var subMesh, out pathTranform,
                   true, new Vector2(i * offset + padding * 0.25f, (i + 1) * offset - padding * ((i + 1) == subPaths.Length + 1 ? 0.5f : 0.25f))));

                saveAssetCommands.Enqueue(saveCommand);

                sawedPartWithConfig.Add(subPath, vertexSubPathConfig);

                combine[i].mesh = subMesh;
                combine[i].transform = Matrix4x4.TRS(Vector3.zero, subPath.rotation, Vector3.one);

                if (pathTranform) pathTranform.gameObject.SetActive(false);

                i++;
            }

            combine[0].mesh = VertexPathMeshCreator.GenerateMesh(new VertexPath(vertexPathConfig.VertexPathData,
                mainPath.transform), patternConfig.meshThickness, new Vector2(padding * 0.5f, offset - padding * 0.25f));
            combine[0].transform = Matrix4x4.TRS(Vector3.zero, mainPath.rotation, Vector3.one);

            float overrideMainPathUVArea = combine[0].mesh.GetAreaOnUV();

            // Add vertex path to subpaths config
            subPathsConfig.Paths = vertexSubPaths;

            // Delete existing mesh
            if ((Mesh)AssetDatabase.LoadAssetAtPath(outputPath + WoodCuttingPaths.outputSubPathMeshName, typeof(Mesh)))
            {
                AssetDatabase.DeleteAsset(outputPath + WoodCuttingPaths.outputSubPathMeshName);
            }

            Mesh combinedMesh = new Mesh();
            combinedMesh.CombineMeshes(combine, true);

            saveAssetCommands.Enqueue(new SaveAssetCommand(combinedMesh, typeof(Mesh),
                false, outputPath + WoodCuttingPaths.outputSubPathMeshName));

            // Setup sawed and chamfered final objects
            var sawedFinalObject = childs.First(x => x.name.StartsWith(sawedFinalObjectSearchName));
            if (sawedFinalObject)
            {
                view.SawedFinalObject = sawedFinalObject.transform;
            }
            var chamferedFinalObject = childs.First(x => x.name.StartsWith(chamferedFinalObjectSearchName));
            if (chamferedFinalObject)
            {
                view.ChamferedFinalObject = chamferedFinalObject.transform;
            }

            var chamferMeshChilds = chamferedFinalObject.gameObject.
                GetComponentsInChildren<Transform>().Where(x => x != chamferedFinalObject).ToArray();

            // Setuping state 6
            SetupState6(childs, view.State6, spriteConfig);

            // Setuping state 7
            SetupState7(childs, view.State7, out var drillParts);

            // Setuping state 8
            SetupState8(childs, drillParts, view.State8, vertexPathConfig, overrideMainPathUVArea, new Vector2(padding * 0.5f, offset - padding * 0.25f),
                sawedPartWithConfig, out var mainDrilPart);

            // Setuping state 9
            SetupState9(mainDrilPart, view.State9, vertexPathConfig);

            // Setuping state 10
            SetupState10(chamferedFinalObject, chamferMeshChilds, mainMesh.bounds, view);

            // Setup materials on view model
            SetupWoodMaterials(model.transform);

            // Update main paths config
            PathsConfig pathsConfig = (PathsConfig)AssetDatabase.LoadAssetAtPath(
                WoodCuttingPaths.pathsConfigPath, typeof(PathsConfig));

            bool overwrite = pathsConfig != null;

            if (!overwrite)
            {
                pathsConfig = CreateInstance<PathsConfig>();
            }

            saveAssetCommands.Enqueue(new SaveAssetCommand(pathsConfig, typeof(PathsConfig),
                overwrite, WoodCuttingPaths.pathsConfigPath));

            var categories = Directory.GetDirectories($"{Application.dataPath}/Resources/WoodCutting/Platbands");
            var patternPaths = new List<string>(100);

            foreach (var c in categories)
            {
                var platbands = Directory.GetDirectories(c);

                foreach (var j in platbands)
                {
                    patternPaths.AddRange(Directory.GetDirectories(j));
                }
            }

            for (int j = 0; j < patternPaths.Count; j++)
            {
                patternPaths[j] = patternPaths[j].Substring(patternPaths[j].IndexOf("Assets"));
                patternPaths[j] = patternPaths[j].Replace('\\', '/');
            }

            pathsConfig.Paths = patternPaths.Where(x=>!x.Contains("Input") && !x.Contains("[")).ToArray();

            // Save all data
            while (saveAssetCommands.Count > 0)
            {
                saveAssetCommands.Dequeue().Execute();
            }

            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();

            // Assign meshes and materials
            view.State8.GetChild(1).GetComponent<MeshFilter>().sharedMesh = combinedMesh;
            view.State8.GetChild(1).GetComponent<MeshCollider>().sharedMesh = combinedMesh;
            view.State8.GetChild(1).GetComponent<MeshRenderer>().sharedMaterial =
                (Material)AssetDatabase.LoadAssetAtPath(fileStructurePath + triggerZoneMaterialPath,
                typeof(Material));

            view.State9.GetChild(1).GetComponent<MeshFilter>().sharedMesh = mainMesh;
            view.State9.GetChild(1).GetComponent<MeshCollider>().sharedMesh = mainMesh;
            view.State9.GetChild(1).GetComponent<MeshRenderer>().sharedMaterial =
                (Material)AssetDatabase.LoadAssetAtPath(fileStructurePath + triggerZoneMaterialPath,
                typeof(Material));

            // Save prefab variant
            if (AssetDatabase.LoadAssetAtPath(outputPath + WoodCuttingPaths.outputPrefabName, typeof(GameObject)))
            {
                Debug.Log("Prefab Pattern is already exist. Deleting existing file...");
                AssetDatabase.DeleteAsset(outputPath + WoodCuttingPaths.outputPrefabName);
            }

            PrefabUtility.SaveAsPrefabAsset(outputPrefab, outputPath + WoodCuttingPaths.outputPrefabName);
            DestroyImmediate(outputPrefab);

           
        }

        private SaveAssetCommand LoadScriptableObject<T>(string path, Type type,
            out T output, bool deleteIfExist = false) where T : ScriptableObject
        {
            var scriptableObject = AssetDatabase.LoadAssetAtPath(path, type);

            bool ovewrite = scriptableObject != null;

            if (!deleteIfExist)
            {
                if (!ovewrite)
                {
                    scriptableObject = CreateInstance(type);
                }
            }
            else
            {
                if (ovewrite)
                {
                    AssetDatabase.DeleteAsset(path);
                }
                scriptableObject = CreateInstance(type);
            }

            output = scriptableObject as T;

            return new SaveAssetCommand(scriptableObject, type, ovewrite, path);
        }

        private VertexPathConfig GenerateVertexPathConfigWithMesh(string outputPath, Transform origin,
            Transform sawedPart, float meshThickness, out Mesh mesh, out Transform pathTransform,
            bool offsetUV = false, Vector2 uvBeginEnd = default(Vector2))
        {
            pathTransform = null;
            Mesh pathMesh = null;

            bool subPath = sawedPart.childCount > 1;
            string outputName = subPath ? WoodCuttingPaths.outputSubPathMeshName :
                WoodCuttingPaths.outputMeshName;

            try
            {
                if (subPath)
                {
                    pathTransform = sawedPart.GetComponentsInChildren<Transform>(true).
                   First(x => x.name.StartsWith(vertexSubPathSearchName));
                }
                else
                {
                    pathTransform = sawedPart.parent.GetComponentsInChildren<Transform>(true).
                   First(x => x.name.StartsWith(vertexPathSearchName));
                }

                pathMesh = pathTransform.GetComponent<MeshFilter>().sharedMesh;
            }
            catch
            {
                Debug.LogWarning("No \"Path/SubPath...\" object inside View.fbx");
            }

            Vector3 startPosition = sawedPart.transform.position;

            try
            {
                if (subPath)
                {
                    startPosition = sawedPart.GetComponentsInChildren<Transform>(true).
                   First(x => x.name.StartsWith(vertexSubPathStartPointSearchName)).position;
                }
                else
                {
                    startPosition = sawedPart.parent.GetComponentsInChildren<Transform>(true).
                   First(x => x.name.StartsWith(vertexPathStartPointSearchName)).position;
                }
            }
            catch
            {
                Debug.LogWarning("No \"Start/SubStart...\" object inside View.fbx");
            }

            if ((Mesh)AssetDatabase.LoadAssetAtPath(
                outputPath + outputName,
               typeof(Mesh)) != null)
            {
                AssetDatabase.DeleteAsset(outputPath + outputName);
            }

            if (!offsetUV)
            {
                uvBeginEnd = new Vector2(0, 1);
            }

            return VertexPathCreator.CreateVertexPathConfig(origin, pathTransform.position - origin.position,
                pathMesh, meshThickness, startPosition, out mesh, uvBeginEnd);
        }

        private void SetupFinalPrefab(string path)
        {
            var loadedPrefab = AssetDatabase.LoadAssetAtPath(fileStructurePath + finalPrefabName, typeof(GameObject));
            var loadedModel = AssetDatabase.LoadAssetAtPath(path + WoodCuttingPaths.inputFinalViewPath, typeof(GameObject));
            var loadedMat = (Material)AssetDatabase.LoadAssetAtPath(fileStructurePath + woodFinalMaterialPath, typeof(Material));

            var outputPrefab = (GameObject)PrefabUtility.InstantiatePrefab(loadedPrefab);
            var model = (GameObject)PrefabUtility.InstantiatePrefab(loadedModel);

            outputPrefab.transform.position = outputPrefab.transform.eulerAngles = Vector3.zero;
            model.transform.SetParent(outputPrefab.transform);
            model.transform.localPosition = model.transform.localEulerAngles = Vector3.zero;
            model.transform.SetAsLastSibling();

            var platbandFinal = outputPrefab.GetComponent<PlatbandFinal>();
            platbandFinal.gameObject.layer = 29;
            platbandFinal.Collect(loadedMat);

            // Save prefab variant
            if (AssetDatabase.LoadAssetAtPath(path + $"/Final.prefab", typeof(GameObject)))
            {
                Debug.Log("Final Platband Prefab is already exist. Deleting existing file...");
                AssetDatabase.DeleteAsset(path + $"/Final.prefab");
            }

            PrefabUtility.SaveAsPrefabAsset(outputPrefab, path + $"/Final.prefab");
            DestroyImmediate(outputPrefab);
        }

        private void SetupWoodMaterials(Transform view)
        {
            var mats = view.GetComponentsInChildren<Renderer>();

            var loadedMat = (Material)AssetDatabase.LoadAssetAtPath(fileStructurePath + woodMaterialPath, typeof(Material));

            foreach (var mat in mats)
            {
                mat.sharedMaterial = loadedMat;
            }
        }

        private void SetupState6(Transform[] childs, Transform state, SpriteConfig spriteConfig)
        {
            var spritePivot = childs.First(x => x.name.StartsWith(spritePivotSearchName));

            if (!spritePivot) return;
            Vector3 pivot = spritePivot.transform.position;
            pivot -= state.GetChild(0).GetChild(0).transform.forward * 0.0001f;
            state.GetChild(0).transform.position = pivot;

            bool needRotationOverride = spriteConfig.main ? (mainVertical) : (subVertical);

            if (needRotationOverride || spriteConfig.solidPattern.texture.width < spriteConfig.solidPattern.texture.height)
            {
                bool clockWise = spriteConfig.main ? (mainClockwise) : (subClockwise);

                if (!needRotationOverride)
                {
                    clockWise = false;
                }

                state.GetChild(0).GetChild(0).transform.eulerAngles += Vector3.forward * (90 * (clockWise ? -1 : 1)); ;
            }

        }

        private void SetupState7(Transform[] childs, Transform state, out Transform[] drillParts)
        {
            drillParts = childs.Where(x => x.name.StartsWith(drilledPartsSearchName)).ToArray();

            if (drillParts == null || drillParts.Length == 0)
            {
                return;
            }

            var prefab = AssetDatabase.LoadAssetAtPath(fileStructurePath + drillZoneName, typeof(GameObject));
            var drillZoneParent = state.GetChild(0);

            int i = 0;
            foreach (var part in drillParts)
            {
                // Spawn drill zone
                var drillZone = (GameObject)PrefabUtility.InstantiatePrefab(prefab, drillZoneParent);

                drillZone.name += $"{i}";
                drillZone.transform.position = part.position;
                drillZone.transform.rotation = part.rotation;

                drillZone.GetComponent<DrillObject>()._target = part.gameObject;

                // Setup drilled part
                var rb = part.gameObject.AddComponent<Rigidbody>();
                var col = part.gameObject.AddComponent<BoxCollider>();

                rb.isKinematic = true;
                col.enabled = false;

                i++;
            }
        }

        private void SetupState8(Transform[] childs, Transform[] drillParts, Transform state,
            VertexPathConfig vertexPathConfig, float overrideMainPathUVArea, Vector2 mainUVBeginEnd,
            Dictionary<Transform, VertexPathConfig> sawPartWithConfig, out Transform mainDrillPart)
        {
            mainDrillPart = null;

            if (drillParts == null || drillParts.Length == 0)
            {
                return;
            }

            var prefab = (GameObject)AssetDatabase.LoadAssetAtPath(fileStructurePath + triggerZoneName, typeof(GameObject));
            var sawZoneParent = state.GetChild(0).GetChild(0);

            var vertexPathEditorData = new List<VertexPathStickyPattern.VertexPathEditorData>();

            int index = 0;
            foreach (var drillPart in drillParts)
            {
                bool isMain = false;

                if (!sawPartWithConfig.ContainsKey(drillPart.parent))
                {
                    mainDrillPart = drillPart;
                    isMain = true;
                }


                // Spawn trigger zone
                var triggerZone = (GameObject)PrefabUtility.InstantiatePrefab(prefab, sawZoneParent);
                triggerZone.name = $"{index}";
                triggerZone.transform.position = drillPart.position;
                triggerZone.transform.rotation = drillPart.rotation;

                // Setup saw part 
                var sawPart = drillPart.parent.gameObject.AddComponent<SawingPart>().EditorInit(); ;

                // Add new editor data to list
                vertexPathEditorData.Add(new VertexPathStickyPattern.VertexPathEditorData()
                {
                    regionID = index++,
                    triggerZone = triggerZone.transform,
                    sawingPart = sawPart,
                    vertexPathConfig = isMain ? vertexPathConfig : sawPartWithConfig[sawPart.transform],
                    overrideUVArea = isMain ? overrideMainPathUVArea : 0f,
                    overrideUVSize = isMain ?
                    new Vector4(mainUVBeginEnd.x, 0, mainUVBeginEnd.y - mainUVBeginEnd.x, 1) : new Vector4(-1, -1, -1, -1)
                });
            }

            state.GetComponentInChildren<VertexPathStickyPattern>()
                .PassVertexPathEditorData(vertexPathEditorData.ToArray());

            Transform main = mainDrillPart;

            var otherSawPart = childs.Where(x => x.name.StartsWith(sawedPartsSearcName) && x.childCount == 0 && x!= main.parent).ToArray();

            SawingPart[] sawingParts = new SawingPart[otherSawPart.Length];
            index = 0;
            foreach (var part in otherSawPart)
            {
                sawingParts[index] = part.gameObject.AddComponent<SawingPart>().EditorInit();
                index++;
            }

            state.GetComponentInParent<PlankFinalViewComponent>().PassOtherSawingParts(sawingParts);
            state.GetComponentInChildren<VertexPathStickyPattern>()
              .PassOtherSawingParts(sawingParts);
        }

        private void SetupState9(Transform drillPart, Transform state, VertexPathConfig config)
        {
            var prefab = (GameObject)AssetDatabase.LoadAssetAtPath(fileStructurePath + triggerZoneName,
                typeof(GameObject));

            // Setup saw part 
            var sawPart = drillPart.parent.gameObject.GetComponent<SawingPart>();
            var triggerZoneParent = state.GetChild(0);

            // Spawn trigger zone
            var triggerZone = (GameObject)PrefabUtility.InstantiatePrefab(prefab, triggerZoneParent);
            triggerZone.transform.position = drillPart.position;
            triggerZone.transform.rotation = drillPart.rotation;
            triggerZone.transform.name = (0).ToString();

            state.GetComponentInChildren<VertexPathStickyPattern>()
               .PassVertexPathEditorData(new VertexPathStickyPattern.VertexPathEditorData[]
               {
                    new VertexPathStickyPattern.VertexPathEditorData()
                        {
                        regionID = 0,
                        triggerZone = triggerZone.transform,
                        sawingPart = sawPart,
                        vertexPathConfig = config
                        }
               });

        }

        private void SetupState10(Transform finalObject, Transform[] childMeshes, Bounds bounds,
            PlankFinalViewComponent view)
        {
            ToningEditorData[] data = new ToningEditorData[childMeshes.Length];

            childMeshes = childMeshes.OrderByDescending(x => Vector3.Dot(x.up, finalObject.up)).ToArray();

            int i = 0;
            foreach (var child in childMeshes)
            {
                child.gameObject.AddComponent<MeshCollider>().sharedMesh =
                    child.gameObject.GetComponent<MeshFilter>().sharedMesh;

                child.tag = "Paintable";

                data[i] = new ToningEditorData()
                {
                    renderer = child.GetComponent<Renderer>(),
                    uvArea = child.gameObject.GetComponent<MeshFilter>().sharedMesh.GetAreaOnUV()
                };

                i++;
            }

            var toningPaintable = finalObject.gameObject.AddComponent<Paintable>();
            toningPaintable.EditorInit(Paintable.TextureResolution._512, 0.15f);

            view.PassToningEditorData(data);
            view.ToningPaintable = toningPaintable;

            // Update collider dimensions (XZ only)
            var boxCollider = view.State10.GetChild(0).GetComponent<BoxCollider>();
            boxCollider.center = new Vector3(bounds.center.x, boxCollider.center.y, bounds.center.z);
            boxCollider.size = new Vector3(bounds.size.x, boxCollider.size.y, bounds.size.z);
        }
    }

}


