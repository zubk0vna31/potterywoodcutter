using Leopotam.Ecs;
using Modules.ViewHub;
using UnityEngine;

namespace PWS.WoodCutting
{
    public class TableTemplate : EntityTemplate
    {
        [SerializeField]
        private Transform planksParent;

        protected override void SetupEntity(EcsEntity entity, EcsWorld world)
        {
            base.SetupEntity(entity, world);

            ref var table = ref entity.Get<Table>();

            table.planksParent = planksParent;
        }
    }
}
