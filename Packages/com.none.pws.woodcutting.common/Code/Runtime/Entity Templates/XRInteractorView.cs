﻿using Leopotam.Ecs;
using Modules.ViewHub;
using UnityEngine.XR.Interaction.Toolkit;

namespace PWS.WoodCutting.Common
{
    public class XRInteractorView : ViewComponent
    {
        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            ecsEntity.Get<XRInteractor>().interactor = GetComponent<XRBaseInteractor>();
        }
    }
}
