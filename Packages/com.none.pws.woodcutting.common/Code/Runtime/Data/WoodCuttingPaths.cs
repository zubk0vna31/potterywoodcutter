namespace PWS.WoodCutting.Common
{
    public static class WoodCuttingPaths
    {
        // Input (Editor only)
        public const string inputSolidSpritePath =
           "/Sprites/Solid.png";
        public const string inputDashedSpritePath =
           "/Sprites/Dashed.png";
        public const string inputViewPath =
            "/Prefabs/View.fbx";
        public const string inputFinalViewPath =
            "/Input/FinalView.fbx";

        // Generated Data (Output)
        public const string outputPrefabName =
            "/GeneratedData/PatternPrefab.prefab";
        public const string outputMeshConfigName =
            "/GeneratedData/MeshConfig.asset";
        public const string outputSpriteConfigName =
           "/GeneratedData/SpriteConfig.asset";
        public const string outputRegionConfigName =
            "/GeneratedData/RegionConfig.asset";
        public const string outputVertexPathConfigName =
            "/GeneratedData/VertexPathConfig.asset";
        public const string outputMeshName =
          "/GeneratedData/Mesh.asset";
        public const string outputSubPathMeshName =
          "/GeneratedData/SubMesh.asset";

        public const string outputVertexSubPathConfigName =
            "/GeneratedData/SubPaths/VertexSubPathConfig.asset";

        public const string subPathsConfigPath =
           "/GeneratedData/SubPathConfig.asset";

        // Custom Configs (Readable)
        public const string inputPatternConfigPath =
          "/Configs/PatternConfig.asset";
        public const string inputMarkupToolConfig =
           "/Configs/MarkupToolConfig.asset";
        public const string inputDrillToolConfig =
           "/Configs/DrillToolConfig.asset";
        public const string inputJigsawToolConfig =
           "/Configs/JigsawToolConfig.asset";
        public const string inputChamferToolConfig =
          "/Configs/ChamferToolConfig.asset";
        public const string inputVertexPathFollowConfig =
        "/Configs/VertexPathFollowConfig.asset";

        // Runtime (Resources)
        public const string pathsConfigPath =
            "Assets/Resources/WoodCutting/GeneratedData/PathsConfig.asset";

        public const string pathsConfigPathFromResources =
            "WoodCutting/GeneratedData/PathsConfig";

        public const string groupConfigName =
            "GroupConfig";
        public const string previewName =
            "Preview";
        public const string finalName =
           "Final";
        public const string mainPatternFolderName =
            "MainPattern";
        public const string subPatternFolderName =
            "SubPattern";
    }
}
