using UnityEngine;

namespace PWS.WoodCutting.Common
{
    [System.Serializable]
    public struct MarkupPaintingSerializedData
    {
        public Vector3[] vertices;
        public Vector3[] normals;
        public int[] triangles;
        public Vector2[] uv;
        public int referencePixelAmount;
        public Vector2 size;
    }
}
