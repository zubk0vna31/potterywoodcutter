using Leopotam.Ecs;
using Modules.StateGroup.Components;
using Modules.StateGroup.Core;
using PWS.WoodCutting.Common;

namespace PWS.WoodCutting
{
    public abstract class RunSystem<StateT,ConfigT>: IEcsRunSystem where StateT: struct where ConfigT:class
    {
        public readonly EcsFilter<StateT> _onState;
        public readonly EcsFilter<StateEnter,StateT> _onStateEnter;
        public readonly EcsFilter<StateExit,StateT> _onStateExit;

        public readonly EcsWorld _ecsWorld;
        public readonly StateFactory _stateFactory;

        protected readonly ConfigT _config;
        protected readonly SetStateSOCommand _nextState;

        public RunSystem(StateConfig config)
        {
            _config = config as ConfigT;
            _nextState = config.nextState;
        }

        public void Run()
        {
            if (_onState.IsEmpty()) return;


            if (!_onStateEnter.IsEmpty())
                OnStateEnter();

            OnStateUpdate();

            if(!_onStateExit.IsEmpty())
                OnStateExit();
        }

        protected virtual void OnStateEnter()
        {
        }

        protected virtual void OnStateUpdate()
        {

        }

        protected virtual void OnStateExit()
        {
        }

        protected void ChangeState(float delay = 0f)
        {
            if (delay <= 0)
            {
                _nextState.Execute(_stateFactory);
            }
            else
            {
                ref var setStateDelayed = ref _ecsWorld.NewEntity().Get<SetStateDelayed>();
                setStateDelayed.state = _nextState;
                setStateDelayed.delay = delay;
            }
        }

        protected bool IsExiting()
        {
            return !_onStateExit.IsEmpty();
        }

    }
}
