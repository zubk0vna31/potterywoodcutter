using UnityEngine;

namespace PWS.WoodCutting.Common
{
    [System.Serializable]
    public class PlankPatternCreateDataHelper
    {
        public bool main;
        public GameObject prefab;
        public Sprite solidPattern;
        public Sprite dashedPattern;
        public Sprite walkablePattern;
        public Texture2D regionMap;


        public bool IsValid()
        {
            return prefab && regionMap && dashedPattern;
        }
    }
}
