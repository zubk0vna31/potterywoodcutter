using UnityEngine;

namespace PWS.WoodCutting.Common
{
    [CreateAssetMenu(fileName = "Jigsaw Tool Config",
        menuName = "PWS/WoodCutting/Stages/Sawing/Jigsaw Tool Config")]
    public class JigsawToolConfig : ToolConfig
    {
       
    }
}
