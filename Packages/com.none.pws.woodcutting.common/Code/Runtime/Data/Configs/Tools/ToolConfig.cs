using UnityEngine;

namespace PWS.WoodCutting.Common
{
    public class ToolConfig : ScriptableObject
    {
        [Range(0f, 0.25f)]
        public float SquaredDistance = 0.1f;
        [Range(1, 64)]
        public int PixelSnapRadius = 5;
        [Range(1, 64)]
        public int PixelBrushSize = 3;
        [Range(1e-5f, 0.1f)]
        public float triggerPainterRadius = 0.007f;
    }
}
