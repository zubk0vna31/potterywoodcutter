using UnityEngine;

namespace PWS.WoodCutting.Common
{
    [CreateAssetMenu(fileName = "Markup Tool Config",
        menuName = "PWS/WoodCutting/Stages/Markup Painting/Markup Tool Config")]
    public class MarkupToolConfig : ToolConfig
    {
    }
}
