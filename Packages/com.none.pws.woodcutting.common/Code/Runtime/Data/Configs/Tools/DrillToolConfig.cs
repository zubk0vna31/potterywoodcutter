using UnityEngine;

namespace PWS.WoodCutting.Common
{
    [CreateAssetMenu(fileName = "Drill Tool Config",
        menuName = "PWS/WoodCutting/Stages/Holes Drilling/Drill Tool Config")]
    public class DrillToolConfig : ToolConfig
    {
    }
}
