using UnityEngine;

namespace PWS.WoodCutting.Common
{
    [CreateAssetMenu(fileName = "Chamfer Tool Config",
       menuName = "PWS/WoodCutting/Stages/Chamfering/Chamfer Tool Config")]
    public class ChamferToolConfig : ToolConfig
    {
    }
}
