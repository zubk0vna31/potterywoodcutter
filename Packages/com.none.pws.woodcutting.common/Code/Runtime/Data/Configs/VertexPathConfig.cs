using UnityEngine;


[System.Serializable]
public struct VertexPathTableData
{
    public int Index;
    public float Value;
}

[System.Serializable]
public struct VertexPathSerializedData
{
    public Vector3[] vertex;
    public Vector3[] tangents;
    public Vector3[] bitangents;
    public Vector3[] normals;
    public VertexPathTableData[] timeTable;
    public float vertexPathDistance;

}

public class VertexPathConfig : ScriptableObject
{
    [SerializeField]
    private VertexPathSerializedData vertexPathData;

    [SerializeField]
    private float meshThickness;

    [SerializeField]
    private float uvArea;

    [SerializeField]
    private Vector4 uvSize;

    public VertexPathSerializedData VertexPathData => vertexPathData;
    public float MeshThickness => meshThickness;
    public float UVArea => uvArea;
    public Vector4 UVSize => uvSize;

    public void Create(VertexPathSerializedData vertexPathData, float meshThickness, float uvArea,Vector4 uvSize)
    {
        this.vertexPathData = vertexPathData;
        this.meshThickness = meshThickness;
        this.uvArea = uvArea;
        this.uvSize = uvSize;
    }

    public void Copy(VertexPathConfig other)
    {
        this.vertexPathData = other.vertexPathData;
        this.meshThickness = other.meshThickness;
        this.uvArea = other.uvArea;
        this.uvSize = other.uvSize;
    }
}
