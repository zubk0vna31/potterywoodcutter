using UnityEngine;

namespace PWS.WoodCutting.Common
{
    [CreateAssetMenu(fileName = "Vertex Path Follow Config",
      menuName = "PWS/WoodCutting/Stages/Chamfering/Vertex Path Follow Config")]
    public class VertexPathFollowConfig : ScriptableObject
    {
        [Range(1e-3f, 0.1f)]
        public float minTimeToMove = 1e-3f;
        public Vector2 followOffset = Vector2.zero;
        [Range(0f,10f)]
        public float followSpeed;
        [Range(0f, 10f)]
        public float rotationSpeed;
    }
}
