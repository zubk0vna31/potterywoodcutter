using UnityEngine;

namespace PWS.WoodCutting.Common
{
    [CreateAssetMenu(fileName = "A* Follow Config",
       menuName = "PWS/WoodCutting/Stages/Sawing/A* Follow Config")]
    public class AStarFollowConfig : ScriptableObject
    {
        [Range(0.05f, 1f)]
        public float updateInterval = 0.35f;

        [Range(5e-7f, 0.1f)]
        public float minDstSqrToMove = 5e-2f;
        [Range(5e-5f, 0.25f)]
        public float turnDst = 5e-3f;
        [Range(5e-4f, 0.1f)]
        public float stopingDst = 15e-2f;


        [Range(5e-3f, 0.25f)]
        public float speed = 15e-2f;
        [Range(0f, 25f)]
        public float rotSpeed = 3.5f;
    }
}
