using UnityEngine;

namespace PWS.WoodCutting.Common
{
    public class Pattern
    {
        // Base
        public bool main;
        public GameObject prefab;
        public string resourcePath;

        // Generated Data (Configs)
        public PatternConfig patternConfig;
        public SpriteConfig spriteConfig;
        public MeshConfig meshConfig;
        public RegionConfig regionConfig;
        public VertexPathConfig vertexPathConfig;
        public VertexPathFollowConfig vertexPathFollowConfig;

        // Tool Configs
        public MarkupToolConfig markupToolConfig;
        public DrillToolConfig drillToolConfig;
        public JigsawToolConfig jigsawToolConfig;
        public ChamferToolConfig chamferToolConfig;

        // Other Configs
        public AStarFollowConfig aStarFollowConfig;

        public Pattern(bool main,GameObject prefab,string resourcePath,PatternConfig patternConfig,
            SpriteConfig spriteConfig,MeshConfig meshConfig,RegionConfig regionConfig,VertexPathConfig vertexPathConfig)
        {
            this.main = main;
            this.prefab = prefab;
            this.resourcePath = resourcePath;
            this.patternConfig = patternConfig;
            this.spriteConfig = spriteConfig;
            this.meshConfig = meshConfig;
            this.regionConfig = regionConfig;
            this.vertexPathConfig = vertexPathConfig;
        }

        public void SetMarkupTool(MarkupToolConfig toolConfig)
        {
            markupToolConfig = toolConfig;
        }

        public void SetDrillTool(DrillToolConfig toolConfig)
        {
            drillToolConfig = toolConfig;
        }

        public void SetJigsawTool(JigsawToolConfig toolConfig)
        {
            jigsawToolConfig = toolConfig;
        }

        public void SetAStarPathFollowConfig(AStarFollowConfig aStarFollowConfig)
        {
            this.aStarFollowConfig = aStarFollowConfig;
        }

        public void SetVertexPathFollowConfig(VertexPathFollowConfig vertexPathFollowConfig)
        {
            this.vertexPathFollowConfig = vertexPathFollowConfig;
        }

        public void SetChamferTool(ChamferToolConfig toolConfig)
        {
            chamferToolConfig= toolConfig;
        }

        public static MeshConfig GenerateMeshConfig(
        SpriteConfig spriteConfig, float alphaThreshold)
        {
            var vertices = new Vector3[4];
            var triangles = new int[6];
            var normals = new Vector3[4];
            var uv = new Vector2[4];

            Vector2Int textureSize = new Vector2Int(
                spriteConfig.dashedPattern.texture.width, spriteConfig.dashedPattern.texture.height);
            float pixelPerUnit = spriteConfig.dashedPattern.pixelsPerUnit;

            vertices[0] = new Vector3(-textureSize.x * 0.5f, -textureSize.y * 0.5f) / pixelPerUnit;
            vertices[1] = new Vector3(-textureSize.x * 0.5f, textureSize.y * 0.5f) / pixelPerUnit;
            vertices[2] = new Vector3(textureSize.x * 0.5f, -textureSize.y * 0.5f) / pixelPerUnit;
            vertices[3] = new Vector3(textureSize.x * 0.5f, textureSize.y * 0.5f) / pixelPerUnit;


            triangles[0] = 0;
            triangles[1] = 1;
            triangles[2] = 3;

            triangles[3] = 3;
            triangles[4] = 2;
            triangles[5] = 0;

            normals[0] = normals[1] = normals[2] = normals[3] = new Vector3(0, 0, 1);

            uv[0] = new Vector2(0, 0);
            uv[1] = new Vector2(0, 1);
            uv[2] = new Vector2(1, 0);
            uv[3] = new Vector2(1, 1);

            MeshConfig output = ScriptableObject.CreateInstance<MeshConfig>();

            output.vertices = vertices;
            output.normals = normals;
            output.triangles = triangles;
            output.uv = uv;

            return output;
        }

    }
}