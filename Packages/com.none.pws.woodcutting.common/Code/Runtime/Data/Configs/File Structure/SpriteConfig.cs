using UnityEngine;

namespace PWS.WoodCutting.Common
{
    public class SpriteConfig : ScriptableObject
    {
        public bool main;

        public Sprite solidPattern;
        public Sprite dashedPattern;

        public int solidRefPixelAmount;

        public static int GetAlphaPixelAmountByThreshold(Texture2D tex,float threshold)
        {
            var refColors = tex.GetPixels();
            var refPixelAmount = 0;
            for (int i = 0; i < refColors.Length; i++)
            {
                if (refColors[i].a >= threshold)
                {
                    refPixelAmount++;
                }
            }

            return refPixelAmount;
        }

        public static void Copy(SpriteConfig target,SpriteConfig source)
        {
            target.main = source.main;
            target.solidPattern = source.solidPattern;
            target.dashedPattern = source.dashedPattern;
            target.solidRefPixelAmount = source.solidRefPixelAmount;
        }
    }
}
