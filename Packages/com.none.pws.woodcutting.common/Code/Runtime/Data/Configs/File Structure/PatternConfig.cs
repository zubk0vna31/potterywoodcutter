using UnityEngine;

namespace PWS.WoodCutting.Common
{
    [CreateAssetMenu(fileName ="Default Pattern Config",menuName = "PWS/WoodCutting/Configs/Pattern Config")]
    public class PatternConfig : ScriptableObject
    {
        [Range(0f,1f)]
        public float alphaThreshold;
        [Range(0f, 1f)]
        public float colliderThickness;
        [Range(0.0001f, 0.5f)]
        public float meshThickness;
    }
}
