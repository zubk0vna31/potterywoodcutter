using UnityEngine;

namespace PWS.WoodCutting.Common
{
    [CreateAssetMenu(menuName = "PWS/WoodCutting/File Structure/Group Config")]
    public class GroupConfig : ScriptableObject
    {
        public string HeaderTerm;

        [Header("Deprecated Parameters")]
        public string Header;
    }
}
