using UnityEngine;

namespace PWS.WoodCutting.Common
{
    public class RegionConfig : ScriptableObject
    {
        public int width;
        public int height;
        public int regionAmount;
        public int[] regionsIdx;
        public int[] perRegionAmount;
        public Vector3Int[] regionMap;
    }
}
