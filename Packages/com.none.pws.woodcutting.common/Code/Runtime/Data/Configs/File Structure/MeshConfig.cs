using UnityEngine;

namespace PWS.WoodCutting.Common
{
    public class MeshConfig : ScriptableObject
    {
        public Vector3[] vertices;
        public Vector3[] normals;
        public int[] triangles;
        public Vector2[] uv;
     
        public static Vector3 GetSize(MeshConfig meshConfig,float thickness=1f)
        {
            return new Vector3(
             meshConfig.vertices[3].x - meshConfig.vertices[0].x,
             meshConfig.vertices[3].y - meshConfig.vertices[0].y,
             thickness);                        
        }

        public static Vector3 GetColliderCenter(float thickness,bool invert=false)
        {
            return Vector3.forward * (0.5f * thickness * (invert ? -1:1));
        }

        public static void Copy(MeshConfig target,MeshConfig source)
        {
            target.vertices = source.vertices;
            target.normals = source.normals;
            target.triangles = source.triangles;
            target.uv = source.uv;
        }
    }
}
