using UnityEngine;

namespace PWS.WoodCutting.Common
{
    [CreateAssetMenu(menuName = "PWS/WoodCutting/Test/Test")]
    public class PathsConfig : ScriptableObject
    {
        public string[] Paths;
    }
}
