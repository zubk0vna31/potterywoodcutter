using Modules.StateGroup.Core;
using UnityEngine;

namespace PWS.WoodCutting.Common
{
    public abstract class StateConfig : ScriptableObject
    {
        public SetStateSOCommand nextState;
        [Range(1,32)]
        public int stateID = 1;

        public string stateName;
    }
}
