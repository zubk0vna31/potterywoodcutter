
namespace PWS.WoodCutting.Common
{
    public interface ITask
    {
        public bool Completed();
    }
}
