using System;
using UnityEngine;

namespace alicewithalex
{
    public class WoodCuttingTriggerListener : MonoBehaviour
    {
        public Action<Transform, Collider> onTriggerEnter;
        public Action<Transform, Collider> onTriggerStay;
        public Action<Transform, Collider> onTriggerExit;

        public void OnTriggerEnter(Collider other)
        {
            onTriggerEnter?.Invoke(transform, other);
        }

        public void OnTriggerStay(Collider other)
        {
            onTriggerStay?.Invoke(transform, other);
        }

        public void OnTriggerExit(Collider other)
        {
            onTriggerExit?.Invoke(transform, other);
        }
    }
}