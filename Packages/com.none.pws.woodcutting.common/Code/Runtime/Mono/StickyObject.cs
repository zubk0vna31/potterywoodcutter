using PWS.WoodCutting.Common;
using UnityEngine;

public class StickyObject : MonoBehaviour
{
    [Header("Interaction")]
    public string _interactionTag;

    [SerializeField, Range(0f, 5f)]
    private float _stickDistanceSquared = 2.5f;

    [SerializeField,Tooltip("Support <BoxCollider> only for now!")]
    private BoxCollider _boxCollider;

    private Stickable _stickable;
    private Tool _tool;
    [SerializeField]
    private bool _active;

    private void Update()
    {
        if (!_active || !_stickable || !_stickable.IsStickable)
        {
            Detach();
            return;
        }

        Reposition(_stickable);
    }

    

    private void Reposition(Stickable _stickable,bool instant=false)
    {
        Vector3 normal =transform.up;
        Vector3 closestPoint = transform.InverseTransformPoint(_stickable.Interactor.position);
        closestPoint.y = 0f;
        closestPoint = ClampPositionInsideCollider(closestPoint);
        closestPoint = transform.TransformPoint(closestPoint);

        Debug.DrawLine(closestPoint, _stickable.Interactor.position);

        Vector3 interactorForward = _stickable.Interactor.forward;
        Vector3 projectedOnNormalInteractorDirection = Vector3.ProjectOnPlane(interactorForward, normal).normalized;

        Vector3 stickPointNewPos = Quaternion.LookRotation(projectedOnNormalInteractorDirection, normal)
            * Quaternion.Inverse(_stickable.StickPoint.localRotation) * _stickable.StickPoint.position;

        Vector3 stickLocalToParent = _stickable.transform.InverseTransformPoint(_stickable.StickPoint.position);
        Vector3 finalPos = closestPoint - _stickable.transform.rotation * stickLocalToParent;

        if ((closestPoint - _stickable.Interactor.position).sqrMagnitude >= _stickDistanceSquared)
        {
            Detach();
            return;
        }

        if (instant)
        {
            _stickable.transform.rotation = Quaternion.LookRotation(projectedOnNormalInteractorDirection, normal)
            * Quaternion.Inverse(_stickable.StickPoint.localRotation);
        }


        _stickable.transform.position = Vector3.Lerp(_stickable.transform.position, finalPos,
           Time.deltaTime * _stickable.MovementSpeed);

        var newRotation = Quaternion.LookRotation(projectedOnNormalInteractorDirection,normal)
            * Quaternion.Inverse(_stickable.StickPoint.localRotation);

        (newRotation * Quaternion.Inverse(_stickable.transform.rotation)).ToAngleAxis(out var angle, out var axis);

        if (Vector3.Angle(normal, axis) > 90f)
        {
            angle = -angle;
        }

        _stickable.transform.RotateAround(closestPoint, normal, angle);

    }

    private Vector3 ClampPositionInsideCollider(Vector3 pos)
    {
        Vector3 size = _boxCollider.size;

        if (Mathf.Abs(pos.x) > size.x * 0.5f)
        {
            pos.x = Mathf.Sign(pos.x) * size.x * 0.5f;
        }

        if (Mathf.Abs(pos.z) > size.z * 0.5f)
        {
            pos.z = Mathf.Sign(pos.z) * size.z * 0.5f;
        }

        return pos;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!_active || !other.CompareTag(_interactionTag) || _stickable) return;

        var stickable = other.GetComponentInParent<Stickable>();

        if (!stickable || !stickable.IsStickable || stickable.IsSticked) return;

        _stickable = stickable;
        _stickable.SetStick(true);

        _tool = _stickable.GetComponent<Tool>();

        if(_tool)
            _tool.ToggleStickableComponents(true);

        Reposition(_stickable, true);
    }

    private void OnTriggerStay(Collider other)
    {
        if (!_active || !other.CompareTag(_interactionTag) || _stickable) return;

        var stickable = other.GetComponentInParent<Stickable>();

        if (!stickable || !stickable.IsStickable || stickable.IsSticked) return;

        _stickable = stickable;
        _stickable.SetStick(true);
        _tool = _stickable.GetComponent<Tool>();

        if(_tool)
            _tool.ToggleStickableComponents(true);

        Reposition(_stickable, true);
    }


    public void Deactivate()
    {
        _active = false;
        Detach();
    }

    public void Detach()
    {
        if (_stickable)
        {
            _stickable.SetStick(false);

            if (_tool)
                _tool.ToggleStickableComponents(false);

            _stickable = null;
            _tool = null;
        }
    }

    public void Toggle(bool value)
    {
        _active = value;

        if (!value)
        {
            Detach();
        }
    }

    public void ChangeSqrDst(float sqrDst)
    {
        _stickDistanceSquared = sqrDst;
    }
}
