using alicewithalex;
using PWS.WoodCutting.Common;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[RequireComponent(typeof(MeshFilter), typeof(MeshRenderer), typeof(BoxCollider))]
public class SawingPattern : MonoBehaviour
{
    public struct PixelQueueData
    {
        public Vector2Int origin;
        public HashSet<Vector2Int> neighbours;
    }

    [SerializeField]
    private string interactionTag;

    [SerializeField]
    private Shader mainShader;

    [SerializeField]
    private Transform sawMarker;

    [SerializeField]
    [Range(0f, 1f)]
    private float expandDistance = 0.15f;

    [Header("Debug")]
    [SerializeField]
    private Pattern debugPatternConfig;

    private SawingPathFinder pathFinder;
    private Stickable stickable;
    private JigsawTool jigsawTool;
    private BoxCollider boxCollider;

    private JigsawToolConfig jigsawToolConfig;
    private float alphaThreshold;

    private Material material;
    private Texture2D walkableTexture;
    private Texture2D solidTexture;
    private Texture2D maskTexture;

    private float pixelSizeWalkable;
    private float pixelSizeSolid;

    private int paintedPixelAmount;
    private int referencePixelAmount;
    private Vector2 textureDifference;

    private int maskID;
    private int currentRegion;

    private Vector3 offset;
    private Transform sawZone;
    private float sawZoneOffset;

    private Queue<PixelQueueData> aStarUpdateQueue;
    private Dictionary<int, int> perRegionProccessed;

    public float Percentage => (pathFinder.ProccessedNodesAmount * 1.0f / referencePixelAmount);

    public float WalkablePixelSize => pixelSizeWalkable;

    public bool IsToolActivated => jigsawTool == null ? false : jigsawTool.Activated;

    public void Initialize(Pattern patternConfig, SawingPathFinder sawingPathFinder,
        Color normalColor, Color markupColor, float clipping, int referencePixelAmount)
    {
        aStarUpdateQueue = new Queue<PixelQueueData>();
        perRegionProccessed = new Dictionary<int, int>(32);

        this.referencePixelAmount = referencePixelAmount;
        this.jigsawToolConfig = patternConfig.jigsawToolConfig;
        this.alphaThreshold = patternConfig.patternConfig.alphaThreshold;
        //this.referencePixelAmount = patternConfig.spriteConfig.walkableRefPixelAmount;

        Rebuild(patternConfig, normalColor, markupColor, clipping);
        boxCollider = RecalculateCollider(
            MeshConfig.GetColliderCenter(patternConfig.patternConfig.colliderThickness, true),
            MeshConfig.GetSize(patternConfig.meshConfig, patternConfig.patternConfig.colliderThickness));


        //walkableTexture = patternConfig.spriteConfig.walkablePattern.texture;
        solidTexture = patternConfig.spriteConfig.solidPattern.texture;

        textureDifference = new Vector2((solidTexture.width * 1.0f / walkableTexture.width),
            (solidTexture.height * 1.0f / walkableTexture.height));

        var listeners = GetComponentsInChildren<WoodCuttingTriggerListener>();

        foreach (var listener in listeners)
        {
            listener.onTriggerEnter += EnterZone;
        }

        float worldSizeX = boxCollider.size.x * transform.lossyScale.x;

        pixelSizeWalkable = worldSizeX / walkableTexture.width;
        pixelSizeSolid = worldSizeX / solidTexture.width;

        var nodes = new Node[patternConfig.regionConfig.regionMap.Length];

        for (int i = 0; i < nodes.Length; i++)
        {
            nodes[i] = new Node(patternConfig.regionConfig.regionMap[i].x,
                patternConfig.regionConfig.regionMap[i].y, 
                true, patternConfig.regionConfig.regionMap[i].z);
        }

        pathFinder = sawingPathFinder;
        //pathFinder.Initialize(nodes, patternConfig.spriteConfig.walkablePattern.texture,
        //    patternConfig.aStarFollowConfig,
        //    patternConfig.patternConfig.alphaThreshold, boxCollider, this);

        sawMarker.gameObject.SetActive(false);
    }

    private void Rebuild(Pattern patternConfig,
    Color normalColor, Color markupColor, float clipping)
    {
        Mesh mesh = new Mesh();

        mesh.vertices = patternConfig.meshConfig.vertices;
        mesh.normals = patternConfig.meshConfig.normals;
        mesh.triangles = patternConfig.meshConfig.triangles;
        mesh.uv = patternConfig.meshConfig.uv;

        mesh.RecalculateNormals();
        mesh.RecalculateBounds();

        GetComponent<MeshFilter>().mesh = mesh;

        material = new Material(mainShader);

        maskID = Shader.PropertyToID("_Mask");

        maskTexture = new Texture2D(patternConfig.spriteConfig.dashedPattern.texture.width,
             patternConfig.spriteConfig.dashedPattern.texture.height, TextureFormat.R8, false);
        maskTexture.filterMode = FilterMode.Bilinear;

        maskTexture.SetPixels(Enumerable.Repeat(Color.black,
            maskTexture.width * maskTexture.height).ToArray());
        maskTexture.Apply();


        material.SetTexture("_MainTex", patternConfig.spriteConfig.solidPattern.texture);
        material.SetTexture("_MarkupTex", patternConfig.spriteConfig.solidPattern.texture);
        material.SetTexture(maskID, maskTexture);

        material.SetColor("_Color1", normalColor);
        material.SetColor("_Color2", markupColor);
        material.SetFloat("_Cliping", clipping);

        GetComponent<MeshRenderer>().material = material;
    }

    private BoxCollider RecalculateCollider(Vector3 colliderCenter, Vector3 colliderSize)
    {
        var c1 = transform.GetComponent<BoxCollider>();

        if (!c1.isTrigger)
            c1.isTrigger = true;

        c1.center = colliderCenter;
        c1.size = colliderSize;

        return c1;
    }

    private void Update()
    {
        if (!stickable || !stickable.IsStickable)
        {
            TryClear();
            return;
        }

        UpdateTransform();

        if (IsToolActivated)
        {
            TryUpdateAStarMap();

            Paint(GetPixelPos(stickable.StickPoint.position, solidTexture),
                maskTexture, jigsawToolConfig.PixelBrushSize, alphaThreshold);
        }
    }

    private void TryUpdateAStarMap()
    {
        if (aStarUpdateQueue.Count <= 0) return;

        var pixels = aStarUpdateQueue.Peek();

        var currentPixelPos = GetPixelPos(stickable.StickPoint.position, solidTexture);

        //Debug.DrawLine(GetWorldPos(currentPixelPos, solidTexture, pixelSizeSolid),
        //    sawMarker.position, Color.green);

        //Debug.DrawLine(GetWorldPos(pixels.origin, solidTexture, pixelSizeSolid),
        //    sawMarker.position, Color.yellow);

        float distance = (pixels.origin - currentPixelPos).magnitude;

        if (distance > jigsawToolConfig.PixelBrushSize * 3f)
        {
            aStarUpdateQueue.Dequeue();
            int counter = 0;

            foreach (var p in pixels.neighbours)
            {
                var remapedPixel = GetRemapedPixel(p, textureDifference);

                if (pathFinder.UpdateAStarMap(false, remapedPixel,currentRegion)) counter++;
            }

            if (pathFinder.UpdateAStarMap(false,
                GetRemapedPixel(pixels.origin, textureDifference),currentRegion)) counter++;

            if (!perRegionProccessed.ContainsKey(currentRegion))
            {
                perRegionProccessed.Add(currentRegion, 0);
            }

            perRegionProccessed[currentRegion] += counter;
        }
    }

    public void UpdateSawZone(Vector3 worldProjPos, Quaternion lookRotation)
    {
        sawZone.transform.position = worldProjPos;
        sawZone.transform.localPosition = new Vector3(sawZone.transform.localPosition.x, sawZoneOffset,
            sawZone.transform.localPosition.z);

        sawZone.transform.rotation = lookRotation;
    }

    private Vector2Int GetRemapedPixel(Vector2Int p, Vector2 difference)
    {
        var result = new Vector2Int(
            Mathf.RoundToInt(p.x * 1.0f / difference.x),
            Mathf.RoundToInt(p.y * 1.0f / difference.y));

        return result;
    }

    public int GetProccessedRegionAmount(int region)
    {
        if (!perRegionProccessed.ContainsKey(region)) return -1;

        return perRegionProccessed[region];
    }

    private void TryClear()
    {
        if (stickable)
        {
            stickable.SetStick(false);
            stickable = null;
            jigsawTool = null;
            sawMarker.gameObject.SetActive(false);
            pathFinder.RemoveFollowerAndTarget();
        }
    }

    private void UpdateTransform()
    {
        Vector3 handProjPos = GetWorldPosProjected(stickable.Interactor.position) + offset;

        handProjPos = ClampPositionInsideCollider(handProjPos, boxCollider, expandDistance);

        sawMarker.position = handProjPos;

        if (!stickable.IsStickable || !InBounds(handProjPos, boxCollider, expandDistance) ||
            ((handProjPos - stickable.Interactor.position).sqrMagnitude >= jigsawToolConfig.SquaredDistance))
        {
            TryClear();
            return;
        }
    }

    private void Paint(Vector2Int o, Texture2D tex, int radius, float alphaThreshold)
    {
        bool change = false;

        var pixelQueueData = new PixelQueueData();
        pixelQueueData.neighbours = new HashSet<Vector2Int>();
        pixelQueueData.origin = o;

        for (int i = o.x - radius; i <= o.x + radius; i++)
        {
            for (int j = o.y - radius; j <= o.y + radius; j++)
            {
                if (InBounds(i, j, tex.width, tex.height) && InCircle(i, j, o.x, o.y, radius)
                    && tex.GetPixel(i, j).a >= alphaThreshold)
                {
                    if (tex.GetPixel(i, j).r <= 0.0f)
                    {
                        change = true;
                        paintedPixelAmount++;
                        tex.SetPixel(i, j, Color.red);

                        if (i != o.x && j != o.y)
                        {
                            pixelQueueData.neighbours.Add(new Vector2Int(i, j));
                        }
                    }
                }
            }
        }

        if (change)
        {
            tex.Apply();
            material.SetTexture(maskID, tex);

            aStarUpdateQueue.Enqueue(pixelQueueData);
        }
    }

    private void EnterZone(Transform self, Collider other)
    {
        if (stickable || !other.CompareTag(interactionTag)) return;

        var target = other.GetComponentInParent<Stickable>();

        if (target)
        {
            if (target.IsStickable)
            {
                jigsawTool = target.GetComponent<JigsawTool>();

                stickable = target;
                stickable.SetStick(true);


                RepositionStickable(self);
                pathFinder.SetTarget(stickable.Interactor, offset);
                pathFinder.SetFollower(stickable.transform, stickable.StickPoint);

                sawZone = self;
                sawZoneOffset = sawZone.localPosition.y;

                try
                {
                    currentRegion = Int32.Parse(self.name);
                }
                catch
                {
                    currentRegion = -1;
                }

            }
        }
    }

    private void RepositionStickable(Transform zone)
    {
        var worldPos = pathFinder.GetClosetPathPoint(zone.position);
        var handProjPos = GetWorldPosProjected(stickable.Interactor.position);

        this.offset = worldPos - handProjPos;

        stickable.transform.forward = zone.forward;

        Vector3 offset = stickable.StickPoint.rotation * stickable.transform.InverseTransformPoint(
               stickable.StickPoint.position);

        stickable.transform.position = worldPos - offset;

        sawMarker.gameObject.SetActive(true);
    }

    private Vector3 GetWorldPosProjected(Vector3 worldPos)
    {
        Vector3 localPos = transform.InverseTransformPoint(worldPos);
        localPos.z = 0f;

        return transform.TransformPoint(localPos);
    }

    private Vector2Int GetPixelPos(Vector3 worldPos, Texture2D tex)
    {
        Vector3 localPos = transform.InverseTransformPoint(worldPos);
        localPos.z = 0f;

        Vector3 size = boxCollider.size;

        localPos.x = Mathf.Clamp01((localPos.x + size.x * 0.5f) / (size.x));
        localPos.y = Mathf.Clamp01((localPos.y + size.y * 0.5f) / (size.y));

        int x = Mathf.FloorToInt(localPos.x * tex.width);
        int y = Mathf.FloorToInt(localPos.y * tex.height);

        return new Vector2Int(x, y);
    }

    private Vector3 GetWorldPos(Vector2Int pixelPos, Texture2D tex, float pixelSize)
    {
        Vector3 localPos = Vector3.zero;
        Vector3 size = boxCollider.size;

        localPos.x = (pixelPos.x * 1.0f) / tex.width;
        localPos.y = (pixelPos.y * 1.0f) / tex.height;

        localPos.x = (localPos.x * size.x) - size.x * 0.5f + pixelSize * 0.5f;
        localPos.y = (localPos.y * size.y) - size.y * 0.5f + pixelSize * 0.5f;

        return transform.TransformPoint(localPos);
    }

    private Vector3 ClampPositionInsideCollider(Vector3 pos, BoxCollider collider, float expand = 0.0f)
    {
        pos = transform.InverseTransformPoint(pos);
        pos.z = 0f;

        Vector3 size = collider.size;

        expand = Mathf.Clamp01(expand);
        if (expand > 0.0f)
            size += new Vector3(size.x * 0.15f, size.y * 0.15f);

        if (Mathf.Abs(pos.x) > size.x * 0.5f)
        {
            pos.x = Mathf.Sign(pos.x) * size.x * 0.5f;
        }

        if (Mathf.Abs(pos.y) > size.y * 0.5f)
        {
            pos.y = Mathf.Sign(pos.y) * size.y * 0.5f;
        }

        return transform.TransformPoint(pos);
    }

    private bool InCircle(int x, int y, int originX, int originY, int radius)
    {
        return (originX - x) * (originX - x) + (originY - y) * (originY - y) <= radius * radius;
    }

    private bool InBounds(int x, int y, int w, int h)
    {
        return x >= 0 && x < w && y >= 0 && y < h;
    }

    private bool InBounds(Vector3 pos, BoxCollider collider, float expand = -1f)
    {
        pos = transform.InverseTransformPoint(pos);
        pos.z = 0f;

        Vector3 size = collider.size;

        if (expand > 0)
        {
            size += Vector3.one * expand;
        }

        if (Mathf.Abs(pos.x) > size.x * 0.5f)
        {
            return false;
        }

        if (Mathf.Abs(pos.y) > size.y * 0.5f)
        {
            return false;
        }

        if (Mathf.Abs(pos.z) > size.z * 0.5f)
        {
            return false;
        }

        return true;
    }
}
