using UnityEngine;

namespace PWS.WoodCutting.Common
{
    public class RollerTool : Tool
    {
        [SerializeField]
        private TriggerPainter [] triggerPainters;
        [SerializeField]
        private Stickable stickable;

        public override void ToggleStickableComponents(bool value)
        {
            base.ToggleStickableComponents(value);

            foreach (var painter in triggerPainters)
            {
                painter.Toggle(value);
            }
        }

    }
}
