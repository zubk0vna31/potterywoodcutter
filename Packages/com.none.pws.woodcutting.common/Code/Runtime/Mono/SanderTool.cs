using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace PWS.WoodCutting.Common
{
    public class SanderTool : Tool
    {
        [SerializeField]
        private TriggerPainter triggerPainter;
        [SerializeField]
        private Stickable stickable;

        public override void ToggleStickableComponents(bool value)
        {
            base.ToggleStickableComponents(value);

            if (!value)
            {
                triggerPainter.Toggle(false);
            }
            else
            {
                if (stickable.IsSticked && Activated)
                    triggerPainter.Toggle(true);
            }
        }

        protected override void OnActivated(ActivateEventArgs arg)
        {
            base.OnActivated(arg);

            if (stickable.IsSticked)
                triggerPainter.Toggle(true);
        }

        protected override void OnDeactivated(DeactivateEventArgs arg)
        {
            base.OnDeactivated(arg);
            triggerPainter.Toggle(false);
        }

        protected override void OnSelectExited(SelectExitEventArgs arg)
        {
            base.OnSelectExited(arg);
            triggerPainter.Toggle(false);
        }

#if UNITY_EDITOR

        [ContextMenu("Collect References")]
        protected override void CollectReferences()
        {
            base.CollectReferences();
            CollectReference(out stickable);
            CollectReferenceInChildren(out triggerPainter);
        }

#endif
    }
}
