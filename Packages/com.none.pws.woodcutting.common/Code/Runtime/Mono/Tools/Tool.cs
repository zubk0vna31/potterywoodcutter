using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace PWS.WoodCutting.Common
{
    [RequireComponent(typeof(XRGrabInteractable), typeof(AudioSource))]
    public class Tool : MonoBehaviour
    {
        [SerializeField]
        private XRGrabInteractable grabInteractable;
        [SerializeField]
        private AudioSource audioSource;

        private bool activated;
        private bool soundOn;

        public bool Activated => activated;

        protected bool SoundOn
        {
            get => soundOn;
            set
            {
                soundOn = value;
                if (audioSource && audioSource.isPlaying && !soundOn) audioSource.Stop();
            }
        }

        protected virtual void Start()
        {
            soundOn = true;

            grabInteractable.activated.AddListener(OnActivated);
            grabInteractable.deactivated.AddListener(OnDeactivated);
            grabInteractable.selectExited.AddListener(OnSelectExited);
        }

        protected virtual void Update()
        {
            if (!activated) return;
        }

        public virtual void SetConfigData(ToolConfig config)
        {

        }

        public virtual void ToggleStickableComponents(bool value)
        {

        }

#if UNITY_EDITOR

        [ContextMenu("Collect References")]
        protected virtual void CollectReferences()
        {
            CollectReference(out grabInteractable);
            //CollectReference(out audioSource);
        }

        protected void CollectReference<T>(out T reference) where T : Behaviour
        {
            if (!TryGetComponent<T>(out reference))
            {
                reference = gameObject.AddComponent<T>();
                Debug.LogWarning($"\"{typeof(T)}\" was added to gameObject." +
                    $"Please, assign all nessesary variables and tweak all properties before continue." +
                    $"Be sure to do that to avoid any erros run-time ");
            }
        }

        protected void CollectReferenceInChildren<T>(out T reference) where T : Behaviour
        {
            if (!TryGetComponent<T>(out reference))
            {
                reference = gameObject.GetComponentInChildren<T>();

                if (reference) return;

                reference = gameObject.AddComponent<T>();
                Debug.LogWarning($"\"{typeof(T)}\" was added to gameObject." +
                    $"Please, assign all nessesary variables and tweak all properties before continue." +
                    $"Be sure to do that to avoid any erros run-time ");
            }
        }

#endif 
        protected void ToggleSound(bool value)
        {
            SoundOn = value;
        }

        protected virtual void OnSelectExited(SelectExitEventArgs arg)
        {
            activated = false;
            if (audioSource)
                audioSource.Stop();
        }

        protected virtual void OnActivated(ActivateEventArgs arg)
        {
            activated = true;

            if (audioSource && soundOn)
                audioSource.Play();
        }

        protected virtual void OnDeactivated(DeactivateEventArgs arg)
        {
            activated = false;
            if (audioSource)
                audioSource.Stop();
        }
    }
}
