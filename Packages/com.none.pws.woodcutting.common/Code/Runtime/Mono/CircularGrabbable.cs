using DG.Tweening;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.XR.Interaction.Toolkit;

namespace PWS.WoodCutting
{

    public class CircularEvent<T>
    {
        private T value;
        private Action action;

        private bool performed;

        public CircularEvent(T value, Action action)
        {
            this.value = value;
            this.action = action;
        }

        public bool IsCompleted(T value, Func<T, T, bool> compareFunction)
        {
            if (performed) return true;

            if (compareFunction.Invoke(value, this.value))
            {
                action?.Invoke();
                performed = true;
                return true;
            }
            else return false;
        }
    }

    public enum Axis
    {
        Right,
        Up,
        Forward
    }

    [RequireComponent(typeof(Rigidbody))]
    public class CircularGrabbable : XRBaseInteractable
    {
        #region Inspector Variables
        [Header("Core")]
        [SerializeField]
        private Axis axis;

        [SerializeField]
        private float minAngle = 0, maxAngle = 90;

        [SerializeField]
        private bool forceStart;

        [SerializeField]
        private float startAngle = 45;

        [Header("Rotation Events")]
        [SerializeField]
        private UnityEvent onMinAngleReach;
        [SerializeField]
        private UnityEvent onMaxAngleReach;
        [SerializeField]
        private UnityEvent<float> onAngleChanged;

        #endregion

        #region Private Variables

        private Quaternion startRotation;
        private float currentAngle;
        [SerializeField,Range(0f,30f)]
        private float minMaxAngleThreshold = 1.0f;

        private Vector3 worldPlaneNormal, localPlaneNormal;
        private Vector3 previousProjectPosition;
        private Vector3 previousWorldPos;

        private bool affecting, initialized;

        private List<CircularEvent<float>> circularEvent = new List<CircularEvent<float>>();
        private List<IGrabbableDependable> grabbableDependables = new List<IGrabbableDependable>();

        #endregion

        #region Public Variables and Properties


        public float TwistAmount => Mathf.InverseLerp(minAngle, maxAngle, currentAngle);

        #endregion

        #region Unity Methods

        protected override void OnEnable()
        {
            base.OnEnable();

            // Object must be kinematic
            GetComponent<Rigidbody>().isKinematic = true;
            Initialize();

            grabbableDependables = GetComponentsInParent<IGrabbableDependable>().ToList();
        }

        protected override void OnDisable()
        {
            if (affecting) affecting = false;

            base.OnDisable();

            //Need testing this
            hoveringInteractors.Clear();
        }

        #endregion

        #region Circular Motion

        private void Initialize()
        {
            worldPlaneNormal = Vector3.zero;
            worldPlaneNormal[(int)axis] = 1.0f;
            localPlaneNormal = worldPlaneNormal;



            if (transform.parent)
            {
                worldPlaneNormal = transform.parent.localToWorldMatrix.MultiplyVector(worldPlaneNormal).normalized;
            }
            else
            {
                if (transform.rotation != Quaternion.identity)
                {
                    worldPlaneNormal = transform.rotation * worldPlaneNormal;
                }
            }

            if (!initialized)
            {
                initialized = true;
                startRotation = transform.localRotation;
            }
            currentAngle = 0f;

            if (forceStart)
            {
                currentAngle = Mathf.Clamp(startAngle, minAngle, maxAngle);
            }

            UpdateTransform();
        }

        private Vector3 ComputeProjectPosition(Vector3 interactorPos)
        {
            Vector3 relative = (interactorPos - transform.position).normalized;

            return Vector3.ProjectOnPlane(relative, worldPlaneNormal).normalized;
        }

        private void ComputeAngle(Transform interactor)
        {
            //Vector3 worldPos = transform.InverseTransformPoint(previousWorldPos);
            //Vector3 currentWorldPos = transform.InverseTransformPoint(interactor.position);
            //float angle = Vector3.SignedAngle(worldPos, currentWorldPos, localPlaneNormal);

            //if (currentAngle <= minAngle)
            //{
            //    if (angle >= 0)
            //    {
            //        angle = Vector3.SignedAngle(worldPos, currentWorldPos, localPlaneNormal);
            //        currentWorldPos = Quaternion.AngleAxis(-angle - minMaxAngleThreshold * 0.99f, localPlaneNormal) * currentWorldPos;
            //    }
            //}
            //else if (currentAngle >= maxAngle)
            //{
            //    if (angle >= 0)
            //    {
            //        angle = Vector3.SignedAngle(worldPos, currentWorldPos, localPlaneNormal);
            //        currentWorldPos = Quaternion.AngleAxis(-angle - minMaxAngleThreshold * 0.99f, localPlaneNormal) * currentWorldPos;
            //    }
            //}

            Vector3 currentProjectPosition = ComputeProjectPosition(interactor.position);

            //Debug.DrawLine(transform.position, transform.position + worldPlaneNormal, Color.magenta);
            //Debug.DrawLine(transform.position, transform.position + previousProjectPosition, Color.red);
            //Debug.DrawLine(transform.position, transform.position + ComputeProjectPosition(interactor.position), Color.blue);
            //Debug.DrawLine(transform.position, transform.position + currentProjectPosition, Color.green);

            if (currentProjectPosition.Equals(previousProjectPosition)) return;

            float absAngle = Vector3.Angle(previousProjectPosition, currentProjectPosition);

            if (absAngle < Mathf.Epsilon) return;

            Vector3 cross = Vector3.Cross(previousProjectPosition, currentProjectPosition);

            float dot = Vector3.Dot(worldPlaneNormal, cross);

            float signedAngle = absAngle;

            if (dot < 0.0f)
            {
                signedAngle = -signedAngle;
            }

            float angleTmp = Mathf.Clamp(currentAngle + signedAngle, minAngle, maxAngle);

            bool success = true;

            if (currentAngle == minAngle)
            {
                if (!(angleTmp > minAngle && absAngle < minMaxAngleThreshold))
                {
                    success = false;
                }
            }
            else if (currentAngle == maxAngle)
            {
                if (!(angleTmp < maxAngle && absAngle < minMaxAngleThreshold))
                {
                    success = false;
                }
            }
            else if (angleTmp == minAngle)
            {
                onMinAngleReach?.Invoke();
            }
            else if (angleTmp == maxAngle)
            {
                onMaxAngleReach?.Invoke();
            }

            if (success)
            {
                currentAngle = angleTmp;
                previousProjectPosition = currentProjectPosition;

                onAngleChanged?.Invoke(TwistAmount);

                foreach (var circularEvent in circularEvent)
                {
                    circularEvent.IsCompleted(TwistAmount, CompareFunction);
                }
            }
        }

        public void ForceUpdateAngle(float normalizedValue)
        {
            currentAngle = Mathf.InverseLerp(minAngle, maxAngle, normalizedValue);
            onAngleChanged?.Invoke(TwistAmount);
            UpdateTransform();
        }

        public void ContiniousUpdateAngle(float time)
        {
            DOTween.To(() => currentAngle, x => currentAngle = x, maxAngle, time).OnUpdate(() =>
            {
                onAngleChanged?.Invoke(TwistAmount);
                UpdateTransform();
            });
        }

        private bool CompareFunction(float a, float b)
        {
            if (a >= b)
            {
                return true;
            }
            else return false;
        }

        private void UpdateTransform()
        {
            transform.localRotation = startRotation * Quaternion.AngleAxis(currentAngle, localPlaneNormal);

            foreach (var i in grabbableDependables)
            {
                i.UpdateDependency(TwistAmount);
            }
        }

        public Vector3 ClampMagnitude(Vector3 v, float max, float min)
        {
            double sm = v.sqrMagnitude;
            if (sm > (double)max * (double)max) return v.normalized * max;
            else if (sm < (double)min * (double)min) return v.normalized * min;
            return v;
        }

        #endregion

        #region Interactable

        public override void ProcessInteractable(XRInteractionUpdateOrder.UpdatePhase updatePhase)
        {
            base.ProcessInteractable(updatePhase);

            if (!updatePhase.Equals(XRInteractionUpdateOrder.UpdatePhase.Dynamic) || !isSelected) return;

            ComputeAngle(selectingInteractor.transform);
            UpdateTransform();
        }

        protected override void OnSelectEntering(SelectEnterEventArgs args)
        {
            base.OnSelectEntering(args);
            BeginInteraction(args.interactor.transform);
        }

        protected override void OnSelectExiting(SelectExitEventArgs args)
        {
            base.OnSelectExiting(args);
            EndInteraction(args.interactor.transform);
        }

        private void BeginInteraction(Transform interactor)
        {
            previousWorldPos = interactor.position;
            previousProjectPosition = ComputeProjectPosition(previousWorldPos);

            affecting = true;

            ComputeAngle(interactor);
            UpdateTransform();
        }

        private void EndInteraction(Transform interactor)
        {
            affecting = false;
        }

        #endregion

        #region Public Methods

        public void AddMaxAngleReachedEvent(UnityAction maxAngleReachEvent)
        {
            onMaxAngleReach.AddListener(maxAngleReachEvent);
        }

        public void AddMinAngleReachedEvent(UnityAction minAngleReachEvent)
        {
            onMinAngleReach.AddListener(minAngleReachEvent);
        }

        public void AddSpecificValueReachedEvent(float value, Action action)
        {
            if (action == null) return;

            circularEvent.Add(new CircularEvent<float>(value, action));
        }

        public void ResetComponent()
        {
            transform.localRotation = startRotation;
            currentAngle = 0;
            UpdateTransform();
        }

        #endregion
    }
}
