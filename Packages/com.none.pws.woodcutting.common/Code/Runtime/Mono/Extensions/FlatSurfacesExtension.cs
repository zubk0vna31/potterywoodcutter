using UnityEngine;

namespace PWS.WoodCutting.Common
{
    public static class FlatSurfacesExtension
    {
        public static float PixelSize(this Texture2D tex, BoxCollider col,Transform tr)
        {
            return col.size.x*tr.lossyScale.x/ tex.width;
        }

        public static float PixelSize(this Texture2D tex, Vector2 size, Transform tr)
        {
            return size.x * tr.lossyScale.x / tex.width;
        }

        public static Vector2Int GetPixelPos(this Transform tr,Vector3 pos,Vector2 size,
            Texture2D tex,Vector3 axis=default(Vector3),float angle=0.0f)
        {
            pos = tr.InverseTransformPoint(pos);
            pos.z = 0f;

            if(angle!=0.0f)
                pos = Quaternion.AngleAxis(angle, axis) * pos;

            pos.x = Mathf.Clamp01((pos.x + size.x * 0.5f) / (size.x));
            pos.y = Mathf.Clamp01((pos.y + size.y * 0.5f) / (size.y));
           
            return new Vector2Int(Mathf.FloorToInt(pos.x * tex.width),
                Mathf.FloorToInt(pos.y * tex.height));
        }

        public static Vector3 GetWorldPos(this Transform tr,Vector2Int pos, Vector2 size,
            Texture2D tex,Vector3 axis = default(Vector3),float angle =0.0f)
        {
            float pixelSize = PixelSize(tex, size, tr);

            var worldPos = new Vector3((pos.x * 1.0f) / tex.width, (pos.y * 1.0f) / tex.height, 0f);

            worldPos.x = (worldPos.x * size.x) - size.x * 0.5f + pixelSize * 0.5f;
            worldPos.y = (worldPos.y * size.y) - size.y * 0.5f + pixelSize * 0.5f;


            if (angle != 0.0f)
                worldPos = Quaternion.AngleAxis(angle, axis) * worldPos;

            return tr.TransformPoint(worldPos);
        }
    }
}
