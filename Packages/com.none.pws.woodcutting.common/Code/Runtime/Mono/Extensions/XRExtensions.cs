﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.XR.Interaction.Toolkit;

namespace PWS.WoodCutting.Common
{
    public static class XRExtensions 
    {
        public static void ForceDeselect(this XRBaseInteractable interactable)
        {
            interactable.interactionManager.CancelInteractableSelection(interactable);
            //Assert.IsFalse(interactable.isSelected);
        }

        public static void ForceDeselect(this XRBaseInteractor interactor)
        {
            interactor.interactionManager.CancelInteractorSelection(interactor);
            //Assert.IsFalse(interactor.isSelectActive);
        }
    }
}
