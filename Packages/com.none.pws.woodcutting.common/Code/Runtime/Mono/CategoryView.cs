using I2.Loc;
using TMPro;
using UnityEngine;

namespace PWS.WoodCutting.Common
{

    public class CategoryView : MonoBehaviour
    {
        [Header("Base")]
        [SerializeField]
        private Localize _headerLocalize;
        [SerializeField]
        private TextMeshProUGUI header;
        [SerializeField]
        private Transform itemContainer;

        public void Initialize(string header)
        {
            //this.header.text = header;
            _headerLocalize.SetTerm(header);
        }

        public Transform ItemContainter => itemContainer;
    }
}
