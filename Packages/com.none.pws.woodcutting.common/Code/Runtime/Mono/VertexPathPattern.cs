using alicewithalex;
using PWS.WoodCutting.Common;
using System;
using UnityEngine;

[RequireComponent(typeof(Paintable))]
public class VertexPathPattern : MonoBehaviour
{
    [SerializeField]
    private string interactionTag;
    [SerializeField]
    private Transform chamferZone;

    // Configs
    private VertexPathConfig vertexPathConfig;
    private VertexPathFollowConfig vertexPathFollowConfig;
    private ToolConfig toolConfig;

    // Run-time data
    private VertexPath vertexPath;
    private Paintable paintable;

    private Transform follower, offseter;
    private Transform attractionTarget;

    private Stickable stickable;
    private ChamferTool chamferTool;

    private bool active;
    private float followTime,previousTime;
    private Vector3 previousValidPosition;
    private Vector3 previousValidForward;
    private float chamferZoneOffset;

    public float Percentage => paintable.PaintedPercentage;

    public void Initialize(VertexPathConfig vertexPathConfig,VertexPathFollowConfig vertexPathFollowConfig, ToolConfig toolConfig,
        Paintable paintable, float skipThreshold)
    {
        this.vertexPathConfig = vertexPathConfig;
        this.vertexPathFollowConfig = vertexPathFollowConfig;
        this.toolConfig = toolConfig;

        vertexPath = new VertexPath(this.vertexPathConfig.VertexPathData, transform);
        this.paintable = paintable;

        this.paintable.Initialize(vertexPathConfig.UVArea, skipThreshold);

        chamferZone.GetComponent<WoodCuttingTriggerListener>().onTriggerEnter += EnterZone;
        CalculateTriggerZoneOffset();

        active = true;

        previousTime = followTime = 0.0f;
    }

#if UNITY_EDITOR
    public void SetTriggerZone(Transform triggerZone)
    {
        chamferZone = triggerZone;
    }
#endif

    private void CalculateTriggerZoneOffset()
    {
        Vector3 pointOnPath = transform.TransformPoint(vertexPath.GetPointAtTime(
            0f, vertexPathFollowConfig.followOffset));

        chamferZoneOffset = (pointOnPath - chamferZone.position).magnitude;
    }

    private void EnterZone(Transform self, Collider other)
    {

        if (!active || !other.CompareTag(interactionTag) || this.stickable) return;

        var stickable = other.GetComponentInParent<Stickable>();

        if (!stickable.IsStickable) return;

        this.stickable = stickable;
        this.stickable.SetStick(true);
        this.chamferTool = stickable.GetComponent<ChamferTool>();

        AddAttractionTarget(stickable.Interactor);
        AddFollower(stickable.transform,stickable.StickPoint);

        UpdateTargetTransform(followTime);
    }

    public void AddAttractionTarget(Transform attractionTarget)
    {
        this.attractionTarget = attractionTarget;
    }

    public void Detach()
    {
        RemoveAllFollowers();
        RemoveAttractionTarget();

        if (stickable)
        {
            stickable.SetStick(false);
            stickable = null;
            chamferTool = null;

            if (chamferZone)
            {
                chamferZone.position = previousValidPosition + Vector3.Cross(transform.up, previousValidForward) * chamferZoneOffset;
                chamferZone.rotation = Quaternion.LookRotation(previousValidForward, transform.up);
            }
        }

        
    }

    public void RemoveAttractionTarget()
    {
        attractionTarget = null;
    }

    public void AddFollower(Transform follower, Transform offseter)
    {
        this.follower = follower;
        this.offseter = offseter;
    }

    public void RemoveAllFollowers()
    {
        follower = null;
    }

    public void Dissable()
    {
        Detach();
        RemoveAttractionTarget();
        RemoveAllFollowers();
        active = false;

    }

    private void Update()
    {
        if (!active || !stickable)
        {
            Detach();
            return;
        }

        if (chamferTool.Activated)
        {
            float normalizedPathTime = vertexPath.GetNormalizedTime(
                vertexPath.GetClosestPointTimeData(attractionTarget.position));

            if (Mathf.Abs(vertexPath.GetTimeClamped(followTime) - normalizedPathTime)
                < vertexPathFollowConfig.minTimeToMove) return;

            previousTime = followTime;

            followTime += GetClosestDirection(followTime, normalizedPathTime)
                * Time.deltaTime * vertexPathFollowConfig.followSpeed;
        }

        UpdateTargetTransform(followTime);
    }

    private int GetClosestDirection(float current, float target)
    {
        current = vertexPath.GetTimeClamped(current);

        float diffNormal = Mathf.Abs(target - current);
        float diffOver1 = (target + 1) - current;
        float diffOver2 = (1 - target) + current;

        if (diffOver1 < diffOver2)
        {
            if (diffOver1 < diffNormal)
            {
                return 1;
            }
        }
        else
        {
            if (diffOver2 < diffNormal)
            {
                return -1;
            }
        }

        int result = target - current > 0 ? 1 : target - current < 0 ? -1 : 0;

        return result;
    }

    private void UpdateTargetTransform(float time)
    {
        var pointOnPath  = transform.TransformPoint(vertexPath.GetPointAtTime(
            time, vertexPathFollowConfig.followOffset));

        if((pointOnPath - attractionTarget.position).sqrMagnitude >= toolConfig.SquaredDistance)
        {
            Detach();
            return;
        }

        follower.transform.position = previousValidPosition
            - follower.rotation*follower.InverseTransformPoint(offseter.position);

        follower.transform.rotation = Quaternion.Slerp(follower.rotation, Quaternion.LookRotation(
            transform.TransformDirection(vertexPath.GetForwardDirAtTime(time))*(previousTime>followTime?-1:1), Vector3.up) * Quaternion.Inverse(offseter.localRotation),
            Time.deltaTime * (vertexPathFollowConfig.rotationSpeed * 10f));

        previousValidPosition = pointOnPath;
        previousValidForward = follower.transform.forward;
    }
}
