using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

[RequireComponent(typeof(XRBaseInteractable))]
public class InteractionVisualPresentation : MonoBehaviour
{
    internal struct OutlineData
    {
        public Dictionary<int,Material> original;
        public Material outline;
    }

    [SerializeField]
    private Material _interactionMaterial;

    [SerializeField]
    private List<Renderer> _targets;

    private XRBaseInteractable _baseInteractable;

    private Dictionary<Renderer,OutlineData> _outlineStorage;

    void Start()
    {
        _baseInteractable = GetComponent<XRBaseInteractable>();

        _baseInteractable.hoverEntered.AddListener(Hover);
        _baseInteractable.hoverExited.AddListener(Unhover);

        if(_targets==null || _targets.Count == 0)
        {
            _targets = GetComponentsInChildren<Renderer>().ToList();
        }

        _outlineStorage = new Dictionary<Renderer, OutlineData>();

        InitializeTargetsWithInteractionMaterial();
    }

    private void InitializeTargetsWithInteractionMaterial()
    {
        foreach (var t in _targets)
        {
            OutlineData outlineData = new OutlineData();
            outlineData.original = new Dictionary<int, Material>();

            for (int i = 0; i < t.materials.Length; i++)
            {
                outlineData.original.Add(i, t.materials[i]);
            }

            outlineData.outline = new Material(_interactionMaterial);

            _outlineStorage.Add(t, outlineData);
        }
    }

    private void Hover(HoverEnterEventArgs args)
    {
        foreach (var t in _targets)
        {
            Material[] mats = new Material[t.materials.Length];

            for (int i = 0; i < t.materials.Length; i++)
            {
                mats[i] = _outlineStorage[t].outline;
            }

            t.materials = mats;
        }
    }

    private void Unhover(HoverExitEventArgs args)
    {
        foreach (var t in _targets)
        {
            Material[] mats = new Material[t.materials.Length];

            foreach (var i in _outlineStorage[t].original.Keys)
            {
                mats[i] = _outlineStorage[t].original[i];
            }

            t.materials = mats;
        }
    }
}
