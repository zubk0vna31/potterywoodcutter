using alicewithalex;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace PWS.WoodCutting.Common
{
    public struct VertexPathData
    {
        public SawingPart sawingPart;
        public RigidbodyData rigidbodyData;
    }

    public class VertexPathStickyPattern : MonoBehaviour
    {
        [SerializeField]
        private string interactionTag;

        [SerializeField]
        private Transform marker;

        [SerializeField]
        private VertexPathEditorData[] vertexPathEditorData;

        [SerializeField]
        private SawingPart[] otherSawingParts = new SawingPart[0];

        // Run-time data
        private VertexPathFollowConfig followConfig;
        private ToolConfig toolConfig;

        private Dictionary<int, VertexPath> vertexPaths;
        private Dictionary<int, VertexPathRegion> vertexPathRegions;
        private int[] regions;

        private Transform follower, offsetter;
        private Transform attractionTarget;

        private VertexPathRegion currentRegion;
        private Stickable stickable;
        private Tool tool;

        private bool initialized;

        private bool active;
        private bool isStickablePassFirstUpdate;
        private float maxTime;

        private bool[] loopsTraveled;

        private Vector3 previousValidPosition;
        private Vector3 previousValidForward;

        public int[] Regions => regions;

        public float FullTime => vertexPathRegions.Select(x => x.Value.CurrentTime).Aggregate((x, y) => x + y) / regions.Length;

        public virtual void Initialize(VertexPathFollowConfig followConfig, ToolConfig toolConfig)
        {
            initialized = true;

            this.followConfig = followConfig;
            this.toolConfig = toolConfig;

            previousValidForward = previousValidPosition = Vector3.zero;
            maxTime = 1.0f;
            active = true;


            vertexPaths = vertexPathEditorData.ToDictionary(v => v.regionID, v =>
              new VertexPath(v.vertexPathConfig.VertexPathData, transform));

            regions = vertexPaths.Keys.ToArray();

            SetupVertexPathRegions();
            SetupEvents();

            ToggleMarker(false);
        }


        private void ToggleMarker(bool value)
        {
            marker.gameObject.SetActive(value);
        }

        public float CurrentTime(int region = 0)
        {
            return vertexPathRegions[region].CurrentTime;
        }

        public void SetMaxTime(float maxTime)
        {
            this.maxTime = maxTime;

            loopsTraveled = new bool[Mathf.FloorToInt(this.maxTime)];
        }

        public void SetLoopTraveled(float percentage)
        {
            int loopIndex = Mathf.FloorToInt(percentage);

            if (loopIndex < 0 || loopIndex > loopsTraveled.Length - 1) return;

            loopsTraveled[loopIndex] = true;
        }

        public bool IsLoopTraveled(float percentage)
        {
            int loopIndex = Mathf.FloorToInt(percentage);

            if (loopIndex < 0 || loopIndex > loopsTraveled.Length - 1) return false;

            return loopsTraveled[loopIndex];
        }

        public void SetAllLoopsTraveled()
        {
            for (int i = 0; i < loopsTraveled.Length; i++)
            {
                loopsTraveled[i] = true;
            }
        }

        private void SetupVertexPathRegions()
        {
            vertexPathRegions = new Dictionary<int, VertexPathRegion>();

            int longestID = vertexPaths.OrderByDescending(x => x.Value.Distance).First().Key;

            Vector3 startPos = Vector3.zero;

            foreach (var data in vertexPathEditorData)
            {
                startPos = (vertexPaths[data.regionID].GetPointAtTime(
                0f, followConfig.followOffset));
                startPos.y = 0f;
                startPos = transform.TransformPoint(startPos);

                float localModifier = 0.5f;

                float speedModifier = data.regionID == longestID ? 1f :
                    (Mathf.Clamp(vertexPaths[longestID].Distance * localModifier / vertexPaths[data.regionID].Distance, 1f, 100f));

                vertexPathRegions.Add(
                    data.regionID,
                    new VertexPathRegion(data.regionID, data.sawingPart, data.triggerZone,
                    (startPos - data.triggerZone.position).magnitude, speedModifier));
            }
        }

        private void SetupEvents()
        {
            foreach (var data in vertexPathEditorData)
            {
                data.triggerZone.GetComponent<WoodCuttingTriggerListener>().onTriggerEnter += EnterZone;
            }
        }

        private void EnterZone(Transform self, Collider other)
        {
            if (!active || !other.CompareTag(interactionTag) || this.stickable) return;

            int regionID = -1;

            try
            {
                regionID = int.Parse(self.name);
            }
            catch
            {

            }

            if (!vertexPathRegions.ContainsKey(regionID)) return;

            var stickable = other.GetComponentInParent<Stickable>();

            if (!stickable.IsStickable) return;

            this.stickable = stickable;
            this.stickable.SetStick(true);
            this.tool = stickable.GetComponent<Tool>();

            this.tool.SetConfigData(toolConfig);

            if (this.tool.Activated)
            {
                this.tool.ToggleStickableComponents(true);
            }

            isStickablePassFirstUpdate = false;

            AddAttractionTarget(stickable.Interactor);
            AddFollower(stickable.transform, stickable.StickPoint);

            currentRegion = vertexPathRegions[regionID];

            ToggleMarker(true);
            UpdateMarker(currentRegion.CurrentTime);
            UpdateCurrentTarget(currentRegion.CurrentTime, true);
        }

        private void UpdateCurrentTarget(float time, bool rotateInstant = false)
        {
            var pointOnPath = transform.TransformPoint(vertexPaths[currentRegion.ID].GetPointAtTime(
                time, followConfig.followOffset));

            if ((pointOnPath - attractionTarget.position).sqrMagnitude >= toolConfig.SquaredDistance)
            {
                Detach();
                return;
            }

            if (rotateInstant)
            {
                follower.transform.rotation = Quaternion.LookRotation(transform.TransformDirection(vertexPaths[currentRegion.ID]
                  .GetForwardDirAtTime(time)) * (currentRegion.MovePositive ? 1 : -1),
                  Vector3.up);
            }

            follower.transform.position = previousValidPosition
                - follower.rotation * follower.InverseTransformPoint(offsetter.position);

            var newRotation = follower.transform.rotation;

            if (!rotateInstant)
            {
                newRotation = Quaternion.Slerp(follower.rotation, Quaternion.LookRotation(
                  transform.TransformDirection(vertexPaths[currentRegion.ID]
                  .GetForwardDirAtTime(time)) * (currentRegion.MovePositive ? 1 : -1),
                  Vector3.up) * Quaternion.Inverse(offsetter.localRotation),
                  Time.deltaTime * (followConfig.rotationSpeed * 10f));
            }
            else
            {
                newRotation = Quaternion.LookRotation(
                 transform.TransformDirection(vertexPaths[currentRegion.ID]
                 .GetForwardDirAtTime(time)) * (currentRegion.MovePositive ? 1 : -1),
                 Vector3.up) * Quaternion.Inverse(offsetter.localRotation);
            }

            (newRotation * Quaternion.Inverse(follower.rotation)).ToAngleAxis(out var angle, out var axis);

            if (Vector3.Angle(Vector3.up, axis) > 90f)
            {
                angle = -angle;
            }

            follower.transform.RotateAround(offsetter.position, Vector3.up, angle);

            previousValidPosition = pointOnPath;
            previousValidForward = follower.transform.forward;

            isStickablePassFirstUpdate = true;

            if (currentRegion.CurrentTime >= maxTime)
            {
                Detach();
            }
        }

        private void UpdateMarker(float time)
        {
            marker.position = transform.TransformPoint(vertexPaths[currentRegion.ID].GetPointAtTime(
                time));
        }

        private void Update()
        {
            if (!active || !stickable || !stickable.IsStickable || currentRegion is null)
            {
                Detach();
                return;
            }

            float normalizedPathTime = vertexPaths[currentRegion.ID].GetNormalizedTime(
                   vertexPaths[currentRegion.ID].GetClosestPointTimeData(attractionTarget.position));

            if (tool.Activated)
            {
                if (Mathf.Abs(vertexPaths[currentRegion.ID].GetTimeClamped(currentRegion.CurrentTime) - normalizedPathTime)
                    < followConfig.minTimeToMove) return;

                currentRegion.Update(GetClosestDirectionPositive(currentRegion.CurrentTime, normalizedPathTime)
                    * (Time.deltaTime * followConfig.followSpeed * currentRegion.SpeedModifer));
            }

            UpdateMarker(normalizedPathTime);
            UpdateCurrentTarget(currentRegion.CurrentTime);
        }

        private int GetClosestDirection(float current, float target)
        {
            current = vertexPaths[currentRegion.ID].GetTimeClamped(current);

            float diffNormal = Mathf.Abs(target - current);
            float diffOver1 = (target + 1) - current;
            float diffOver2 = (1 - target) + current;

            if (diffOver1 < diffOver2)
            {
                if (diffOver1 < diffNormal)
                {
                    return 1;
                }
            }
            else
            {
                if (diffOver2 < diffNormal)
                {
                    return -1;
                }
            }

            int result = target - current > 0 ? 1 : target - current < 0 ? -1 : 0;

            return result;
        }

        private int GetClosestDirectionPositive(float current, float target)
        {
            current = vertexPaths[currentRegion.ID].GetTimeClamped(current);

            if (Mathf.Abs(current - target) < Mathf.Epsilon ||
                (target < current && current - target < 0.5f))
            {
                return 0;
            }
            else return 1;
        }

        public void AddFollower(Transform follower, Transform offseter)
        {
            this.follower = follower;
            this.offsetter = offseter;
        }

        public void AddAttractionTarget(Transform attractionTarget)
        {
            this.attractionTarget = attractionTarget;
        }

        public void Detach()
        {
            RemoveFollower();
            RemoveAttractionTarget();
            ToggleMarker(false);

            if (stickable)
            {
                tool.ToggleStickableComponents(false);

                Vector3 stickableForward = stickable.StickPoint.forward;

                stickable.SetStick(false);
                stickable = null;
                tool = null;

                if (isStickablePassFirstUpdate && !(currentRegion is null))
                {
                    Vector3 forward = (currentRegion.MovePositive ? previousValidForward : -previousValidForward);
                    float dotProduct = Vector3.Dot(forward, stickableForward);


                    currentRegion.TriggerZone.position =
                        previousValidPosition - Vector3.Cross(transform.up, forward
                        * (currentRegion.TriggerZoneOffset * Mathf.Sign(dotProduct)));
                    currentRegion.TriggerZone.localPosition = new Vector3(
                        currentRegion.TriggerZone.localPosition.x, 0f,
                        currentRegion.TriggerZone.localPosition.z);
                    currentRegion.TriggerZone.rotation =
                        Quaternion.LookRotation(previousValidForward, transform.up);

                    if (currentRegion.CurrentTime >= maxTime)
                        currentRegion.Deactivate();

                    currentRegion = null;
                }
            }


        }

        public void RemoveAttractionTarget()
        {
            attractionTarget = null;
        }

        public void RemoveFollower()
        {
            follower = null;
        }

        public void Dissable()
        {
            Detach();
            RemoveAttractionTarget();
            RemoveFollower();
            active = false;
        }

        public void DropAll(float duration, float delay)
        {
            foreach (var data in vertexPathRegions.Keys)
            {
                vertexPathRegions[data].SawingPart.Toggle(true);
                vertexPathRegions[data].SawingPart.Shrink(duration, delay);
                vertexPathRegions[data].TriggerZone.gameObject.SetActive(false);
            }
        }

        public void Drop(int region)
        {
            if (!vertexPathRegions.ContainsKey(region)) return;

            vertexPathRegions[region].SawingPart.Toggle(true);
            DeactivateTriggerZone(region);
        }

        public void DeactivateTriggerZone(int region)
        {
            if (!vertexPathRegions.ContainsKey(region)) return;

            vertexPathRegions[region].TriggerZone.gameObject.SetActive(false);
        }

        public float GetUVArea()
        {
            if (vertexPathEditorData == null) return 1f;

            float area = 0f;

            foreach (var item in vertexPathEditorData)
            {
                if (item.overrideUVArea == 0)
                    area += item.vertexPathConfig.UVArea;
                else area += item.overrideUVArea;
            }

            return area;
        }

        public float GetUVArea(int region)
        {
            for (int i = 0; i < vertexPathEditorData.Length; i++)
            {
                if (vertexPathEditorData[i].regionID == region)
                {
                    if (vertexPathEditorData[i].overrideUVArea > 0) return vertexPathEditorData[i].overrideUVArea;
                    return vertexPathEditorData[i].vertexPathConfig.UVArea;
                }
            }

            return 0;
        }

        public Vector4 GetUVSize(int region)
        {
            for (int i = 0; i < vertexPathEditorData.Length; i++)
            {
                if (vertexPathEditorData[i].regionID == region)
                {
                    if (vertexPathEditorData[i].overrideUVSize.x >= 0) return vertexPathEditorData[i].overrideUVSize;
                    return vertexPathEditorData[i].vertexPathConfig.UVSize;
                }
            }

            return new Vector4(0, 0, 1, 1);
        }

        public VertexPathData[] GetSawData()
        {
            var data = new VertexPathData[vertexPathEditorData.Length+otherSawingParts.Length];

            int i = 0;
            for (i = 0; i < vertexPathEditorData.Length; i++)
            {
                data[i] = new VertexPathData()
                {
                    sawingPart = vertexPathEditorData[i].sawingPart,
                    rigidbodyData = new RigidbodyData()
                    {
                        pos = vertexPathEditorData[i].sawingPart.transform.position,
                        rot = vertexPathEditorData[i].sawingPart.transform.rotation,
                        scale = vertexPathEditorData[i].sawingPart.transform.localScale,
                        colliderEnabled = vertexPathEditorData[i].sawingPart.Collider.enabled,
                        isKinematic = vertexPathEditorData[i].sawingPart.Rigidbody.isKinematic
                    }
                };
            }

            if (otherSawingParts.Length > 0)
            {
                for (int j = i, w = 0; j < data.Length; j++, w++)
                {
                    data[j] = new VertexPathData()
                    {
                        sawingPart = otherSawingParts[w],
                        rigidbodyData = new RigidbodyData()
                        {
                            pos = otherSawingParts[w].transform.position,
                            rot = otherSawingParts[w].transform.rotation,
                            scale = otherSawingParts[w].transform.localScale,
                            colliderEnabled = otherSawingParts[w].Collider.enabled,
                            isKinematic = otherSawingParts[w].Rigidbody.isKinematic
                        }
                    };
                }
            }

            return data;
        }

        public Transform [] GetTriggerZones()
        {
            return vertexPathEditorData.Select(x => x.triggerZone).ToArray();
        }

        public void ResetComponent(VertexPathData [] vertexPathData)
        {
            if (!initialized) return;

            // Dissable component
            Dissable();

            // Unsubscribe from events
            foreach (var data in vertexPathEditorData)
            {
                data.triggerZone.GetComponent<WoodCuttingTriggerListener>().onTriggerEnter -= EnterZone;

            }

            foreach (var data in vertexPathData)
            {
                data.sawingPart.transform.position = data.rigidbodyData.pos;
                data.sawingPart.transform.rotation = data.rigidbodyData.rot;
                data.sawingPart.transform.localScale = data.rigidbodyData.scale;
                data.sawingPart.Toggle(data.rigidbodyData.colliderEnabled,data.rigidbodyData.isKinematic);
            }

            // Clear vertex paths
            var vertexPaths = this.vertexPaths.Values.ToArray();

            for (int i = 0; i < vertexPaths.Length; i++)
            {
                vertexPaths[i] = null;
            }

            this.vertexPaths.Clear();
        }


#if UNITY_EDITOR
        public void PassVertexPathEditorData(params VertexPathEditorData[] vertexPathEditorData)
        {
            this.vertexPathEditorData = vertexPathEditorData;
        }

        public void PassOtherSawingParts(SawingPart [] other)
        {
            otherSawingParts = other;
        }
#endif

        internal class VertexPathRegion
        {
            public readonly int ID;
            public readonly float TriggerZoneOffset;
            public readonly Transform TriggerZone;
            public readonly float SpeedModifer;
            public readonly SawingPart SawingPart;

            private bool active;
            private float currentTime, previousTime;

            public float CurrentTime => currentTime;
            public bool MovePositive => currentTime >= previousTime;

            public VertexPathRegion()
            {
            }

            public VertexPathRegion(int id, SawingPart sawingPart, Transform triggerZone, float triggerZoneOffset, float speedModifier = 1f)
            {
                this.ID = id;
                this.SawingPart = sawingPart;
                this.TriggerZone = triggerZone;
                this.TriggerZoneOffset = triggerZoneOffset;
                this.SpeedModifer = speedModifier;

                currentTime = previousTime = 0.0f;
                active = true;
            }

            public void Update(float appendAmount)
            {
                if (!active || appendAmount <= 0f) return;
                previousTime = currentTime;
                currentTime += appendAmount;
            }

            public void Deactivate()
            {
                active = false;
            }
        }

        [System.Serializable]
        public struct VertexPathEditorData
        {
            public VertexPathConfig vertexPathConfig;
            public float overrideUVArea;
            public Vector4 overrideUVSize;
            public Transform triggerZone;
            public SawingPart sawingPart;
            public int regionID;

        }
    }
}
