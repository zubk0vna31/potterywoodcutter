#if UNITY_EDITOR
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace PWS.WoodCutting.Common
{
    public class VertexPathCreator : MonoBehaviour
    {
        private const string SAVE_PATH = "Configs/VertexPaths";

        [Header("Input Data")]
        public Mesh inputMesh;
        public bool looped;
        [Range(0f, 0.05f)]
        public float meshThickness = 0.05f;

        [Header("Output Properties")]
        public string fileName = "File";
        public bool generateMesh = false;

        [Header("Debug Core")]
        public VertexPathConfig debugConfig;

        [Header("Debug Visual")]
        public bool isDebugOn = false;
        [Range(0, 1f)]
        public float pointSize = 0.1f;
        public Color pointColor, vertexPathColor;

        [Header("Debug Properties")]
        public bool isFollow = false;
        public Transform follower;
        public Transform attractionTarget;
        public Vector2 followOffset;
        public float debugTime = 0.0f;
        [Range(0f, 10f)]
        public float debugSpeed = 0.05f;
        [Range(0f, 50f)]
        public float debugRotSpeed = 0.05f;

        // Scene-only data
        private VertexPath vertexPath;
        private float averageSize;
        private float currentTime;

        private void Start()
        {
            if (!debugConfig) return;

            vertexPath = new VertexPath(debugConfig.VertexPathData, transform);
            debugTime = currentTime = 0.0f;
        }

        private void Update()
        {
            if (vertexPath == null) return;

            if (isFollow)
            {
                float t = vertexPath.GetNormalizedTime(vertexPath.GetClosestPointTimeData(attractionTarget.position));
                if (Mathf.Abs(vertexPath.GetTimeClamped(currentTime) - t) < 0.001f) return;

                currentTime = debugTime;
                currentTime += GetClosestDirection(currentTime, t) * Time.deltaTime * debugSpeed;
                debugTime = currentTime;
            }
            else
            {
                currentTime = debugTime;
            }

            UpdateTargetTransform(currentTime);
        }

        private int GetClosestDirection(float current, float target)
        {
            current = vertexPath.GetTimeClamped(current);

            float diffNormal = Mathf.Abs(target - current);
            float diffOver1 = (target + 1) - current;
            float diffOver2 = (1 - target) + current;

            if (diffOver1 < diffOver2)
            {
                if (diffOver1 < diffNormal)
                {
                    return 1;
                }
            }
            else
            {
                if (diffOver2 < diffNormal)
                {
                    return -1;
                }
            }

            int result = target - current > 0 ? 1 : target - current < 0 ? -1 : 0;

            return result;
        }

        private void UpdateTargetTransform(float t)
        {
            if (!follower) return;

            follower.transform.position = transform.TransformPoint(vertexPath.GetPointAtTime(t, followOffset));
            follower.transform.rotation = Quaternion.Slerp(follower.rotation, Quaternion.LookRotation(
                vertexPath.GetForwardDirAtTime(t), Vector3.up), Time.deltaTime * debugRotSpeed);
        }

        public void OnDrawGizmosSelected()
        {
            if (!isDebugOn || debugConfig == null || vertexPath == null) return;

            Gizmos.matrix = transform.localToWorldMatrix;

            for (int i = 0; i < vertexPath.PointAmount; i++)
            {
                Gizmos.color = pointColor;

                Gizmos.DrawWireCube(vertexPath.GetVertex(i), Vector3.one * (averageSize * pointSize));

                Gizmos.color = vertexPathColor;


                if (looped)
                {
                    Gizmos.DrawLine(vertexPath.GetVertex(i), vertexPath.GetVertex((i + 1) % vertexPath.PointAmount));
                }
                else
                {
                    if (i < vertexPath.PointAmount - 1)
                        Gizmos.DrawLine(vertexPath.GetVertex(i), vertexPath.GetVertex(i + 1));
                }
            }
        }

        public static Vector3[] ReorderVertices(Vector3[] originalVertices, Vector3 localStartPos)
        {
            var closest = originalVertices.OrderBy(x =>
            (x - localStartPos).sqrMagnitude).First();

            Debug.Log($"{localStartPos.x}|{localStartPos.y}|{localStartPos.z}| BU");

            int newIndex = -1;
            for (int i = 0; i < originalVertices.Length; i++)
            {
                Debug.Log($"{originalVertices[i].x}|{originalVertices[i].y}|{originalVertices[i].z}|");


                if (closest == originalVertices[i])
                {
                    newIndex = i;
                    Debug.Log(i);
                    break;
                }
            }


            var output = new Vector3[originalVertices.Length];

            for (int i = 0; i < output.Length; i++)
            {
                output[i] = originalVertices[(i + newIndex+1) % originalVertices.Length];
            }

            output = output.Reverse().ToArray();

            return output;
        }

        public static VertexPathConfig CreateVertexPathConfig(Transform transform,Vector3 offset,
            Mesh mesh, float thickness, Vector3 startPos, out Mesh saveMesh,Vector2 uvBeginEnd,bool looped = true)
        {
            if (mesh.vertices.Length < 1)
            {
                saveMesh = null;
                return default(VertexPathConfig);
            }

            var vertices = mesh.vertices;

            for (int i = 0; i < vertices.Length; i++)
            {
                vertices[i] += offset;
            }

            Debug.Log(transform.name);

            var points = new List<Vector3>(ReorderVertices(vertices,
                transform.InverseTransformPoint(startPos)));

            if (looped)
            {
                points.Add(points[0]);
            }

            var vertexPath = new VertexPath(points.ToArray(),transform);

            saveMesh = VertexPathMeshCreator.GenerateMesh(vertexPath, thickness, uvBeginEnd);
            VertexPathConfig vertexPathConfig = ScriptableObject.CreateInstance<VertexPathConfig>();

            vertexPathConfig.Create(vertexPath.Serialize(), thickness, saveMesh.GetAreaOnUV(),
                new Vector4(uvBeginEnd.x,0,uvBeginEnd.y-uvBeginEnd.x,1));

            return vertexPathConfig;
        }
    }
}
#endif