using UnityEngine;

namespace PWS.WoodCutting.Common
{
    public static class VertexPathMeshCreator
    {
        public static Mesh GenerateMesh(VertexPath vertexPath, float thickness,Vector2 uvBeginEnd)
        {
            Vector3[] vertices = new Vector3[vertexPath.PointAmount * 3];
            Vector2[] uv = new Vector2[vertexPath.PointAmount * 3];
            Vector3[] normals = new Vector3[vertices.Length];
            int[] triangles = new int[6 * 2 * (vertices.Length - 3)];

            float[] uvOffset = new float[]
            {
                uvBeginEnd.x,
                (uvBeginEnd.x+uvBeginEnd.y)*0.5f,
                uvBeginEnd.y
            };

            Vector3 previous = vertexPath.GetVertex(vertexPath.PointAmount - 2);

            for (int i = 0, j = 0, t = 0; i < vertexPath.PointAmount; i++, j += 3, t += 12)
            {
                Vector3 current = vertexPath.GetVertex(i);

                var prevToCurrentDir = (current - previous).normalized;

                float angle = Vector3.Angle(prevToCurrentDir, vertexPath.GetTangent(i));
                float offset = 1f;

                if (angle >= 1 && angle <= 89)
                {
                    offset = Mathf.Cos(angle * Mathf.Deg2Rad);
                }

                vertices[j] = current - vertexPath.GetBitangent(i) * (thickness / offset) * 0.5f;
                vertices[j + 1] = current + vertexPath.GetBitangent(i) * (thickness / offset) * 0.5f;
                vertices[j + 2] = current - vertexPath.GetNormal(i) * thickness + vertexPath.GetBitangent(i) * (thickness / offset) * 0.5f;

                float time = vertexPath.GetVertexTime(i);

                uv[j] = new Vector2(uvOffset[0], time);
                uv[j + 1] = new Vector2(uvOffset[1], time);
                uv[j + 2] = new Vector2(uvOffset[2], time);

                normals[j] = vertexPath.GetNormal(i);
                normals[j + 2] = vertexPath.GetBitangent(i);
                normals[j + 1] = (normals[j] + normals[j + 2]) * 0.5f;

                if (i < vertexPath.PointAmount - 1)
                {
                    // Triangles
                    triangles[t] = j;
                    triangles[t + 1] = j + 1;
                    triangles[t + 2] = j + 3;

                    triangles[t + 3] = j + 1;
                    triangles[t + 4] = j + 4;
                    triangles[t + 5] = j + 3;

                    triangles[t + 6] = j + 1;
                    triangles[t + 7] = j + 2;
                    triangles[t + 8] = j + 4;

                    triangles[t + 9] = j + 2;
                    triangles[t + 10] = j + 5;
                    triangles[t + 11] = j + 4;
                }
                previous = current;
            }

            var mesh = new Mesh();

            mesh.vertices = vertices;
            mesh.normals = normals;
            mesh.triangles = triangles;
            mesh.uv = uv;

            mesh.RecalculateNormals();
            mesh.RecalculateBounds();

            return mesh;
        }

        public static float GetAreaOnUV(this Mesh mesh)
        {
            float area = 0f;

            for (int i = 0; i < mesh.triangles.Length; i += 3)
            {
                Vector2 p0 = mesh.uv[mesh.triangles[i]];
                Vector2 p1 = mesh.uv[mesh.triangles[i + 1]];
                Vector2 p2 = mesh.uv[mesh.triangles[i + 2]];


                float p01 = Mathf.Abs((p1 - p0).magnitude);
                float p12 = Mathf.Abs((p2 - p1).magnitude);
                float p20 = Mathf.Abs((p2 - p0).magnitude);

                float p = (p01 + p12 + p20) * 0.5f;

                area += Mathf.Sqrt(p * (p - p01) * (p - p12) * (p - p20));
            }

            return area;
        }
    }
}