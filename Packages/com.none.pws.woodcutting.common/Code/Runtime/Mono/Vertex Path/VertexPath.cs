using System.Collections.Generic;
using UnityEngine;

namespace PWS.WoodCutting.Common
{
    [System.Serializable]
    public class VertexPath
    {
        public struct PathTimeData
        {
            public int min;
            public int max;
            public float t;
        }


        private Transform transform;
        private Vector3[] vertex;
        private Vector3[] tangents;
        private Vector3[] bitangents;
        private Vector3[] normals;
        public readonly bool looped;

        private VertexPathTableData[] distanceTable;
        private VertexPathTableData[] timeTable;
        private float vertexPathDistance;

        public int PointAmount => vertex.Length;
        public float Distance => vertexPathDistance;

        public VertexPath(Vector3[] vertex,Transform transform=null)
        {
            this.vertex = vertex;

            if (transform)
            {
                SetTransform(transform);
            }

            looped = vertex[0] == vertex[vertex.Length - 1];
            GenerateVertexData();
            GenerateDistanceTable();
            GenerateTimeTable();
        }

        public VertexPath(VertexPathSerializedData data, Transform transform)
        {
            vertex = data.vertex;
            looped = vertex[0] == vertex[vertex.Length - 1];

            timeTable = data.timeTable;
            tangents = data.tangents;
            bitangents = data.bitangents;
            normals = data.normals;
            vertexPathDistance = data.vertexPathDistance;

            this.transform = transform;
        }

        public void SetTransform(Transform transform)
        {
            this.transform = transform;

            for (int i = 0; i < vertex.Length; i++)
            {
                vertex[i] = transform.rotation * vertex[i];
            }
        }

        private void GenerateVertexData()
        {
            normals = new Vector3[PointAmount];
            tangents = new Vector3[PointAmount];
            bitangents = new Vector3[PointAmount];

            Vector3 previous = Vector3.zero;
            Vector3 next = Vector3.zero;
            Vector3 current;

            for (int i = 0; i < PointAmount; i++)
            {
                if (i == 0)
                {
                    previous = vertex[PointAmount - 2];
                    next = vertex[i + 1];
                }
                else if (i == PointAmount - 1)
                {
                    next = vertex[1];
                }
                else
                {
                    next = vertex[i + 1];
                    previous = vertex[i - 1];
                }

                current = vertex[i];

                normals[i] = Vector3.up;

                tangents[i] = (((current - previous).normalized
                    + (next - current).normalized) * 0.5f).normalized;

                bitangents[i] = Vector3.Cross(tangents[i], normals[i]);

                previous = current;
            }
        }

        public VertexPathSerializedData Serialize()
        {
            VertexPathSerializedData data = new VertexPathSerializedData();

            data.vertex = vertex;
            data.timeTable = timeTable;
            data.vertexPathDistance = vertexPathDistance;
            data.tangents = tangents;
            data.bitangents = bitangents;
            data.normals = normals;

            return data;
        }

        private void GenerateDistanceTable()
        {
            SortedDictionary<float, int> distanceTable = new SortedDictionary<float, int>();

            vertexPathDistance = 0f;
            float current = vertexPathDistance;

            distanceTable.Add(current, 0);
            for (int i = 1; i < vertex.Length; i++)
            {
                current = (vertex[i - 1] - vertex[i]).magnitude;
                vertexPathDistance += current;

                distanceTable.Add(vertexPathDistance, i);
            }

            this.distanceTable = new VertexPathTableData[vertex.Length];

            int index = 0;
            foreach (var t in distanceTable)
            {
                this.distanceTable[index] = new VertexPathTableData
                {
                    Value = t.Key,
                    Index = t.Value
                };
                index++;
            }
        }

        private void GenerateTimeTable()
        {
            SortedDictionary<float, int> timeTable = new SortedDictionary<float, int>();

            foreach (var table in distanceTable)
            {
                timeTable.Add(table.Value / vertexPathDistance, table.Index);
            }

            this.timeTable = new VertexPathTableData[vertex.Length];

            int i = 0;
            foreach (var t in timeTable)
            {
                this.timeTable[i] = new VertexPathTableData
                {
                    Value = t.Key,
                    Index = t.Value
                };
                i++;
            }
        }

        public Vector3 GetVertex(int index)
        {
            return vertex[index];
        }

        public Vector3 GetTangent(int index)
        {
            return tangents[index];
        }

        public Vector3 GetNormal(int index)
        {
            return normals[index];
        }

        public Vector3 GetBitangent(int index)
        {
            return bitangents[index];
        }

        public float GetVertexTime(int index)
        {
            for (int i = 0; i < timeTable.Length; i++)
            {
                if (timeTable[i].Index == index) return timeTable[i].Value;
            }

            return 0.5f;
        }

        private Vector2Int GetClosestIndecies(float t)
        {
            VertexPathTableData minKeyValue;
            VertexPathTableData maxKeyValue;

            bool minFounded = false;
            bool maxFounded = false;

            int minIndex = -1, maxIndex = -1;

            bool reverse = timeTable[timeTable.Length / 2].Value < t;

            if (!reverse)
            {
                for (int i = 0; i < timeTable.Length; i++)
                {
                    if (minFounded && maxFounded) break;

                    if (!minFounded)
                    {
                        minKeyValue = timeTable[i];

                        if (minKeyValue.Value == t)
                        {
                            minIndex = Mathf.Clamp(minKeyValue.Index, 0, timeTable.Length - 2);
                            minFounded = true;
                        }
                        else if (minKeyValue.Value < t)
                        {
                            //nothing
                        }
                        else
                        {
                            minIndex = minKeyValue.Index - 1;
                            minFounded = true;
                        }
                    }

                    if (minFounded && !maxFounded)
                    {
                        maxIndex = minIndex + 1;
                        maxFounded = true;
                    }
                }
            }
            else
            {
                for (int i = timeTable.Length - 1; i >= 0; i--)
                {
                    if (minFounded && maxFounded) break;

                    if (!maxFounded)
                    {
                        maxKeyValue = timeTable[i];

                        if (maxKeyValue.Value == t)
                        {
                            maxIndex = Mathf.Clamp(maxKeyValue.Index, 1, timeTable.Length - 1);
                            maxFounded = true;
                        }
                        else if (maxKeyValue.Value > t)
                        {
                            //nothing
                        }
                        else
                        {
                            maxIndex = maxKeyValue.Index + 1;
                            maxFounded = true;
                        }
                    }

                    if (maxFounded && !minFounded)
                    {
                        minIndex = maxIndex - 1;
                        minFounded = true;
                    }
                }
            }

            return new Vector2Int(minIndex, maxIndex);
        }

        public float GetTimeClamped(float t)
        {
            if (!looped)
            {
                return Mathf.Clamp01(t);
            }
            else
            {
                if (t >= 0 && t <= 1f)
                {
                    return t;
                }


                float mod = t - Mathf.FloorToInt(t);
                if (t < 0)
                {
                    mod = Mathf.Abs(mod);

                }

                return mod;
            }
        }

        public float GetNormalizedTime(PathTimeData data)
        {
            return GetTimeClamped(Mathf.Lerp(
                timeTable[data.min].Value, timeTable[data.max].Value, data.t));
        }

        private float GetTimeReranged(float t, Vector2Int idx)
        {
            return Mathf.InverseLerp(timeTable[idx.x].Value, timeTable[idx.y].Value, t);
        }

        private PathTimeData CalculateClosestPointOnPathData(Vector3 localPoint)
        {
            float minSqrDst = float.MaxValue;
            Vector3 closestPoint = Vector3.zero;
            int minSegmentIndex = 0;
            int maxSegmentIndex = 0;

            for (int i = 0; i < vertex.Length; i++)
            {
                int nextIdx = i + 1;
                if (nextIdx >= vertex.Length)
                {
                    if (looped)
                    {
                        nextIdx %= vertex.Length;
                    }
                    else
                    {
                        break;
                    }
                }

                Vector3 closestPointOnSegment = ClosestPointOnLineSegment(
                    localPoint, vertex[i], vertex[nextIdx]);

                float sqrDst = (localPoint - closestPointOnSegment).sqrMagnitude;
                if (sqrDst < minSqrDst)
                {
                    minSqrDst = sqrDst;
                    closestPoint = closestPointOnSegment;
                    minSegmentIndex = i;
                    maxSegmentIndex = nextIdx;

                }

            }
            float closestSegmentLength = (vertex[minSegmentIndex] -
                vertex[maxSegmentIndex]).magnitude;
            float t = (closestPoint - vertex[minSegmentIndex]).magnitude / closestSegmentLength;

            return new PathTimeData { min = minSegmentIndex, max = maxSegmentIndex, t = t };
        }

        private Vector3 ClosestPointOnLineSegment(Vector3 p, Vector3 a, Vector3 b)
        {
            Vector3 aB = b - a;
            Vector3 aP = p - a;
            float sqrLenAB = aB.sqrMagnitude;

            if (sqrLenAB == 0)
                return a;

            float t = Mathf.Clamp01(Vector3.Dot(aP, aB) / sqrLenAB);
            return a + aB * t;
        }

        public Vector3 GetClosestPointOnPath(Vector3 worldPos)
        {
            var data = CalculateClosestPointOnPathData(transform.InverseTransformPoint(worldPos));
            return transform.TransformPoint(Vector3.Lerp(vertex[data.min], vertex[data.max], data.t));
        }

        public PathTimeData GetClosestPointTimeData(Vector3 worldPos)
        {
            return CalculateClosestPointOnPathData(transform.InverseTransformPoint(worldPos));
        }

        public Vector3 GetPointAtTime(float t)
        {
            t = GetTimeClamped(t);
            var idx = GetClosestIndecies(t);
            return Vector3.Lerp(vertex[timeTable[idx.x].Index],
                vertex[timeTable[idx.y].Index], GetTimeReranged(t, idx));
        }

        public Vector3 GetPointAtTime(float t, Vector2 offset)
        {
            t = GetTimeClamped(t);
            var idx = GetClosestIndecies(t);

            var vertexA = timeTable[idx.x].Index;
            var vertexB = timeTable[idx.y].Index;

            return Vector3.Lerp(
                vertex[vertexA]
                + bitangents[vertexA] * offset.x
                + normals[vertexA] * offset.y,

                vertex[vertexB]
                + bitangents[vertexB] * offset.x
                + normals[vertexB] * offset.y,

                GetTimeReranged(t, idx)); ;
        }

        public Vector3 GetForwardDirAtTime(float t)
        {
            t = GetTimeClamped(t);
            var idx = GetClosestIndecies(t);

            float step = 0.025f;

            Vector3 next = Vector3.Lerp(vertex[timeTable[idx.x].Index],
                vertex[timeTable[idx.y].Index], Mathf.Clamp01(GetTimeReranged(t, idx) + step));
            Vector3 prev = Vector3.Lerp(vertex[timeTable[idx.x].Index],
                vertex[timeTable[idx.y].Index], Mathf.Clamp01(GetTimeReranged(t, idx) - step));

            return (next - prev).normalized;
        }
    }
}
