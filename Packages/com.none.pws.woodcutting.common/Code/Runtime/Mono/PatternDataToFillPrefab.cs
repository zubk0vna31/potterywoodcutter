using System.Collections.Generic;
using UnityEngine;

public class PatternDataToFillPrefab : MonoBehaviour
{
    [Header("Prefabs")]
    public GameObject zonePrefab;
    public GameObject sawZonePrefab;
    public GameObject drillZonePrefab;

    [Header("Core")]
    public Transform spritePivot;
    [Range(1e-3f, 0.25f)]
    public float toolZoneSize = 0.1f;
    [Header("Drilling Holes")]
    public List<GameObject> drillHoles;
    [Header("Sawing")]
    public GameObject sawedPattern;
    public List<GameObject> sawingParts;
    [Header("Chamfering")]
    public GameObject chamferedPattern;
    public GameObject chamferEnterZone;
}
