using System.Collections;
using System.Collections.Generic;
using System.Linq;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

namespace PWS.WoodCutting.Common
{
    public class AStarPath
    {
        public readonly Vector3[] lookPoints;
        public readonly Line[] turnBoundaries;
        public readonly int finishLineIndex;
        public readonly int slowDownIndex;

        public AStarPath(Vector3[] waypoints, float turnDist, float stopingDist)
        {
            lookPoints = waypoints;
            turnBoundaries = new Line[lookPoints.Length];
            finishLineIndex = turnBoundaries.Length - 1;

            Vector2 previousPoint = V3ToV2(waypoints[0]);
            for (int i = 0; i < lookPoints.Length; i++)
            {
                Vector2 currentPoint = V3ToV2(lookPoints[i]);
                Vector2 dirToCurrentPoint = (currentPoint - previousPoint).normalized;
                Vector2 turnBoundaryPoint = (i == finishLineIndex) ? currentPoint : currentPoint - dirToCurrentPoint * turnDist;
                turnBoundaries[i] = new Line(turnBoundaryPoint, previousPoint - dirToCurrentPoint * turnDist);
                previousPoint = turnBoundaryPoint;
            }

            float dstFromEndPoint = 0;
            for (int i = lookPoints.Length - 1; i > 0; i--)
            {
                dstFromEndPoint += Vector3.Distance(lookPoints[i], lookPoints[i - 1]);
                if (dstFromEndPoint > stopingDist)
                {
                    slowDownIndex = i;
                    break;
                }
            }
        }

        Vector2 V3ToV2(Vector3 v3)
        {
            return new Vector2(v3.x, v3.y);
        }
    }

    public class FloatComparer : IComparer<float>
    {
        public int Compare(float x, float y)
        {
            if (x >= y)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }
    }

    [RequireComponent(typeof(BoxCollider))]
    public class SawingPathFinder : MonoBehaviour
    {
        public bool isDebug;

        [Range(0f, 1f)]
        public float gizmoRadius = 0.1f;
        public Color pointColor, lineColor;

        private AStarFollowConfig aStarPathFollowConfig;
        private Texture2D texture;

        private BoxCollider boxCollider;
        private AStar aStar;
        private SawingPattern sawingPattern;

        private float pixelSize;
        private float alphaThreshold;
        private int searchSize;

        private Vector3 currentPos;
        private Vector3 previousPos;
        private Vector3 lastValidPos;

        private Transform follower;
        private Transform followerOrigin;
        private Transform target;
        private Vector3 targetOffset;
        private Quaternion currentLookRotation;

        private AStarPath path;
        private float timer;
        private bool initialized;
        private int proccessedNodesAmount;

        private IEnumerator pathProcessing;

        public int ProccessedNodesAmount => (proccessedNodesAmount);

        private void Update()
        {
            if (!initialized) return;

            if (!sawingPattern.IsToolActivated)
            {
                if (pathProcessing != null)
                {
                    StopCoroutine(pathProcessing);
                    currentPos = previousPos = GetWorldProjectedPos(followerOrigin.position);
                }
                return;
            }

            timer += Time.deltaTime;

            if (timer >= aStarPathFollowConfig.updateInterval)
            {
                ProcessPath();
                timer = 0f;
            }
        }

        public void Initialize(Node[] nodes, Texture2D texture, AStarFollowConfig aStarPathFollowConfig,
            float alphaThreshold, BoxCollider boxCollider, SawingPattern sawingPattern)
        {
            this.boxCollider = boxCollider;
            this.texture = texture;
            this.aStarPathFollowConfig = aStarPathFollowConfig;
            this.alphaThreshold = alphaThreshold;
            this.sawingPattern = sawingPattern;

            aStar = new AStar(nodes, this.texture.width, this.texture.height);

            pixelSize = sawingPattern.WalkablePixelSize;
            searchSize = Mathf.RoundToInt(Mathf.Max(texture.width, texture.height) * 0.125f);

            initialized = true;
        }


        public bool UpdateAStarMap(bool walkable, Vector2Int node,int currentRegion)
        {
            if (aStar.UpdateNodeByCoords(node.x, node.y, walkable, currentRegion))
            {
                proccessedNodesAmount += (!walkable ? 1 : -1);
                return true;
            }
            else return false;
        }

        private void ProcessPath()
        {
            if (!follower || !target) return;

            currentPos = GetWorldProjectedPos(target.position) + targetOffset;

            bool foundNewPath = false;
            if ((currentPos - previousPos).sqrMagnitude > aStarPathFollowConfig.minDstSqrToMove)
            {

                var nodes = aStar.CalculatePath(
                    GetClosestPointWithAlphaThreshold(GetPixelPos(followerOrigin.position), alphaThreshold,
                    searchSize),

                     GetClosestPointWithAlphaThreshold(GetPixelPos(currentPos), alphaThreshold,
                    searchSize), true);

                if (nodes != null && nodes.Count > 0)
                {
                    Vector3[] localPoints = new Vector3[nodes.Count];

                    for (int i = 0; i < localPoints.Length; i++)
                    {
                        localPoints[i] = transform.InverseTransformPoint(GetWorldPos(
                            new Vector2Int(nodes[i].x, nodes[i].y)));
                    }

                    path = new AStarPath(localPoints,
                        aStarPathFollowConfig.turnDst, aStarPathFollowConfig.stopingDst);

                    foundNewPath = true;
                }

                if (pathProcessing != null)
                {
                    StopCoroutine(pathProcessing);
                }

                if (foundNewPath)
                {
                    pathProcessing = PathProcessing();
                    StartCoroutine(pathProcessing);
                }

            }
            previousPos = currentPos;
        }

        private IEnumerator PathProcessing()
        {
            bool followingPath = true;
            int pathIndex = 0;
            //transform.LookAt(path.lookPoints[0]);

            float speedPercent = 1;

            while (followingPath)
            {
                Vector2 pos2D = GetLocalProjectedPos(followerOrigin.position);
                while (path.turnBoundaries[pathIndex].HasCrossedLine(pos2D))
                {
                    if (pathIndex == path.finishLineIndex)
                    {
                        followingPath = false;
                        break;
                    }
                    else
                    {
                        pathIndex++;
                    }
                }

                if (followingPath)
                {

                    if (pathIndex >= path.slowDownIndex && aStarPathFollowConfig.stopingDst > 0)
                    {
                        speedPercent = Mathf.Clamp01(path.turnBoundaries[path.finishLineIndex]
                            .DistanceFromPoint(pos2D) / aStarPathFollowConfig.stopingDst);
                        if (speedPercent < 0.01f)
                        {
                            followingPath = false;
                        }
                    }

                    var dir = (GetWorldPosFromLocal(path.lookPoints[pathIndex])
                            - followerOrigin.position).normalized;

                    float angle = Vector3.SignedAngle(followerOrigin.forward, dir, transform.forward);
                    float sign = Mathf.Sign(angle);
                    angle = Mathf.Abs(angle);

                    follower.RotateAround(followerOrigin.position, transform.forward,
                        angle * sign * Time.deltaTime * aStarPathFollowConfig.rotSpeed);
                    follower.Translate(Vector3.forward * (
                        Time.deltaTime * aStarPathFollowConfig.speed * speedPercent), Space.Self);

                    currentLookRotation = Quaternion.LookRotation(follower.forward, Vector3.up);
                    lastValidPos = GetWorldProjectedPos(followerOrigin.position);
                }

                yield return null;

            }

            pathProcessing = null;
            path = null;
        }

        public void SetTarget(Transform target, Vector3 targetOffset)
        {
            this.target = target;
            this.targetOffset = targetOffset;

            currentPos = GetWorldProjectedPos(this.target.position) + this.targetOffset;
            previousPos = currentPos;
        }

        public void SetFollower(Transform follower, Transform followerOrigin)
        {
            this.follower = follower;
            this.followerOrigin = followerOrigin;

            lastValidPos = followerOrigin.position;
        }

        public void RemoveFollowerAndTarget()
        {
            sawingPattern.UpdateSawZone(lastValidPos, currentLookRotation);

            target = null;
            follower = null;
            path = null;
            if (pathProcessing != null)
                StopCoroutine(pathProcessing);
        }

        public Vector3 GetClosetPathPoint(Vector3 pos, int searchRadius = 64)
        {
            return GetWorldPos(GetClosestPointWithAlphaThreshold(
                GetPixelPos(pos), alphaThreshold, searchRadius));
        }

        private Vector2Int GetClosestPointWithAlphaThreshold(Vector2Int origin,
            float alphaThreshold, int radius)
        {
            List<Vector2Int> pixels = new List<Vector2Int>();

            for (int i = origin.x - radius; i <= origin.x + radius; i++)
            {
                for (int j = origin.y - radius; j <= origin.y + radius; j++)
                {
                    if (InBounds(origin.x, origin.y, texture.width, texture.height) &&
                        InCircle(i, j, origin.x, origin.y, radius))
                    {
                        if (texture.GetPixel(i, j).a >= alphaThreshold)
                        {
                            pixels.Add(new Vector2Int(i, j));
                        }
                    }
                }
            }

            if (pixels.Count <= 0)
            {
                return origin;
            }

            return pixels.OrderBy(x => (origin - x).sqrMagnitude).First();
        }

        private Vector2 GetLocalProjectedPos(Vector3 worldPos)
        {
            var result = transform.InverseTransformPoint(worldPos);
            return new Vector2(result.x, result.y);
        }

        private Vector3 GetWorldProjectedPos(Vector3 worldPos)
        {
            var result = transform.InverseTransformPoint(worldPos);
            result.z = 0f;
            return transform.TransformPoint(result);
        }

        private Vector3 GetWorldPosFromLocal(Vector2 localPos)
        {
            return transform.TransformPoint(localPos);
        }

        private bool InBounds(int x, int y, int w, int h)
        {
            return x >= 0 && x < w && y >= 0 && y < h;
        }

        private bool InCircle(int x, int y, int originX, int originY, int radius)
        {
            return (originX - x) * (originX - x) + (originY - y) * (originY - y) <= radius * radius;
        }

        private Vector2Int GetPixelPos(Vector3 worldPos)
        {
            Vector3 localPos = transform.InverseTransformPoint(worldPos);
            localPos.z = 0f;

            Vector3 size = boxCollider.size;

            localPos.x = Mathf.Clamp01((localPos.x + size.x * 0.5f) / (size.x));
            localPos.y = Mathf.Clamp01((localPos.y + size.y * 0.5f) / (size.y));

            int x = Mathf.FloorToInt(localPos.x * texture.width);
            int y = Mathf.FloorToInt(localPos.y * texture.height);

            x = Mathf.Clamp(x, 0, texture.width - 1);
            y = Mathf.Clamp(y, 0, texture.height - 1);


            return new Vector2Int(x, y);
        }

        private Vector3 GetWorldPos(Vector2Int pixelPos)
        {
            Vector3 localPos = Vector3.zero;
            Vector3 size = boxCollider.size;


            localPos.x = (pixelPos.x * 1.0f) / texture.width;
            localPos.y = (pixelPos.y * 1.0f) / texture.height;

            localPos.x = (localPos.x * size.x) - size.x * 0.5f + pixelSize * 0.5f;
            localPos.y = (localPos.y * size.y) - size.y * 0.5f + pixelSize * 0.5f;

            return transform.TransformPoint(localPos);
        }

        private void OnDrawGizmosSelected()
        {
            if (!isDebug) return;

            if (path != null)
            {
                for (int i = 0; i < path.lookPoints.Length; i++)
                {
                    //Gizmos.color = Color.white;
                    //path.turnBoundaries[i].DrawWithGizmos(transform, gizmoRadius);

                    if (i == 0 || i == path.lookPoints.Length)
                    {
                        Gizmos.color = pointColor;
                        Gizmos.DrawWireSphere(GetWorldPosFromLocal(path.lookPoints[i]), gizmoRadius);
                    }

                    if (i < path.lookPoints.Length - 1)
                    {
                        Gizmos.color = lineColor;
                        Gizmos.DrawLine(GetWorldPosFromLocal(path.lookPoints[i]),
                            GetWorldPosFromLocal(path.lookPoints[i + 1]));
                    }
                }
            }

            Gizmos.color = Color.green;
            for (int i = 0; i < aStar.Nodes.GetLength(0); i += 1)
            {
                for (int j = 0; j < aStar.Nodes.GetLength(1); j += 1)
                {
                    if (!aStar.Nodes[i, j].walkable) continue;

                    Gizmos.DrawCube(GetWorldPos(new Vector2Int(aStar.Nodes[i, j].x, aStar.Nodes[i, j].y)), Vector3.one * pixelSize);
                }
            }
        }

    }

    public static class ConvertExtension
    {
        public static T[,] ConvertTo2D<T>(this IEnumerable<T> array, int width, int height)
        {
            T[,] output = new T[width, height];

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    output[i, j] = array.ElementAt(i * height + j);
                }
            }

            return output;
        }

        public static T[,] ConvertTo2D<T>(this T[] arr, int w, int h)
        {
            T[,] result = new T[w, h];

            for (int i = 0; i < w; i++)
            {
                for (int j = 0; j < h; j++)
                {
                    result[i, j] = arr[i * h + j];
                }
            }

            return result;
        }

        public static T[] ConvertTo1D<T>(this T[,] arr, int w, int h)
        {
            T[] result = new T[w * h];

            for (int i = 0; i < w; i++)
            {
                for (int j = 0; j < h; j++)
                {
                    result[i * h + j] = arr[i, j];
                }
            }

            return result;
        }

        public static T[,] Transpose<T>(this T[,] arr)
        {
            int w = arr.GetLength(0);
            int h = arr.GetLength(1);

            T[,] result = new T[h, w];

            for (int i = 0; i < w; i++)
            {
                for (int j = 0; j < h; j++)
                {
                    result[j, i] = arr[i, j];
                }
            }

            return result;
        }

        public static T[,] Rotate90<T>(this T[,] arr)
        {
            var tmp = arr;
            int w = arr.GetLength(0);
            int h = arr.GetLength(1);
            arr = new T[h, w];

            for (int i = 0; i < h; i++)
            {
                for (int j = 0; j < w; j++)
                {
                    arr[i, j] = tmp[w - 1 - j, i];
                }
            }

            return arr;
        }

        public static T[,] Rotate270<T>(this T[,] arr)
        {
            var tmp = arr;
            int w = arr.GetLength(0);
            int h = arr.GetLength(1);
            arr = new T[h, w];

            for (int i = 0; i < h; i++)
            {
                for (int j = 0; j < w; j++)
                {
                    arr[i, j] = tmp[j, h - 1 - i];
                }
            }

            return arr;
        }

        public static T[,] VerticalFlip<T>(this T[,] arr)
        {
            var tmp = arr;
            int h = arr.GetLength(0);
            int w = arr.GetLength(1);

            arr = new T[h, w];

            for (int i = 0; i < h; i++)
            {
                int n = h - 1 - i;

                for (int j = 0; j < w; j++)
                {
                    arr[n, j] = tmp[i, j];
                }
            }

            return arr;
        }

        public static T[,] HorizontalFlip<T>(this T[,] arr)
        {
            var tmp = arr;
            int h = arr.GetLength(0);
            int w = arr.GetLength(1);

            arr = new T[h, w];

            for (int i = 0; i < h; i++)
            {
                int n = h - 1 - i;

                for (int j = 0; j < w; j++)
                {
                    arr[n, w - 1 - j] = tmp[i, j];
                }
            }

            return arr;
        }
    }
}
