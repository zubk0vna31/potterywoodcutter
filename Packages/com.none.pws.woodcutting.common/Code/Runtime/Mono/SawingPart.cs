using DG.Tweening;
using UnityEngine;

namespace PWS.WoodCutting.Common
{
    public class SawingPart : MonoBehaviour
    {
        [SerializeField]
        private Rigidbody _rigidbody;
        [SerializeField]
        private MeshCollider _collider;

        public Collider Collider => _collider;
        public Rigidbody Rigidbody => _rigidbody;

        public void Toggle(bool value)
        {
            if (_rigidbody.isKinematic == !value) return;

            _rigidbody.isKinematic = !value;
            _collider.enabled = value;
        }

        public void Toggle(bool col,bool kinematic)
        {
            _rigidbody.isKinematic = kinematic;
            _collider.enabled = col;
        }

        public void Shrink(float shrink,float delay)
        {
            transform.DOScale(Vector3.zero, shrink)
                       .SetEase(Ease.InSine)
                       .SetDelay(delay)
                       .OnComplete(() => gameObject.SetActive(false));
        }

#if UNITY_EDITOR
        public SawingPart EditorInit()
        {
            _rigidbody = gameObject.AddComponent<Rigidbody>();
            _collider = gameObject.AddComponent<MeshCollider>();
            _collider.convex = true;

            Toggle(false);

            return this;
        }
#endif

    }
}
