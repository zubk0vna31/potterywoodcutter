using PWS.WoodCutting.Common;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

[RequireComponent(typeof(XRGrabInteractable))]
public class DrillTool : Tool
{
    public Transform drillStick;
    [Range(-10,10)]
    public float speed;

    protected override void Update()
    {
        if (!Activated) return;

        drillStick.transform.rotation = Quaternion.AngleAxis(Time.deltaTime*speed*360f,drillStick.forward) * drillStick.transform.rotation;
    }
}
