
namespace PWS.WoodCutting.Common
{
    [System.Serializable]
    public class Node
    {
        public int x, y;
        public bool walkable;
        public int region;

        [System.NonSerialized]
        public int gCost;
        [System.NonSerialized]
        public int hCost;
        [System.NonSerialized]
        public Node parent;

        public Node(int x, int y, bool walkable=false,int region =-1)
        {
            this.x = x;
            this.y = y;
            this.walkable = walkable;
            this.region = region;
        }


        public int fCost => gCost + hCost;
    }
}
