using System;
using System.Collections.Generic;
using UnityEngine;

namespace PWS.WoodCutting.Common
{

    public class AStar
    {
        private Node[,] grid;

        public Node[,] Nodes => grid;

        public AStar(float[,] grid, params float[] notWalkable)
        {
            this.grid = CreateGrid(grid, notWalkable);
        }

        public AStar(float[,] grid, float thresholdValue, IComparer<float> comparer)
        {
            this.grid = CreateGrid(grid, thresholdValue, comparer);
        }

        public AStar(Node[,] nodes)
        {
            grid = nodes;
        }

        public AStar(Node[] nodes, int w, int h)
        {
            grid = new Node[w, h];

            for (int i = 0; i < w; i++)
            {
                for (int j = 0; j < h; j++)
                {
                    grid[i, j] = new Node(i, j);
                }
            }

            foreach (var node in nodes)
            {
                grid[node.x, node.y].walkable = true;
            }
        }

        public bool UpdateNodeByCoords(int x, int y, bool walkable, int region)
        {
            if (!InBounds(x, y, grid.GetLength(0), grid.GetLength(1))
                || grid[x, y].walkable == walkable || grid[x, y].region != region) return false;
            grid[x, y].walkable = walkable;
            return true;
        }

        public void ShowGridInfo()
        {
            var width = grid.GetLength(0);
            var length = width * 2 - 1 + 2;

            var str = new string('-', length);

            str += Environment.NewLine;
            for (int i = 0; i < grid.GetLength(1); i++)
            {
                str += "[";
                for (int j = 0; j < grid.GetLength(0); j++)
                {
                    str += (grid[j, grid.GetLength(1) - 1 - i].walkable ? 1 : 0).ToString();

                    if (j < grid.GetLength(0) - 1)
                    {
                        str += ",";
                    }
                    else
                    {
                        str += "]";
                        str += Environment.NewLine;
                    }
                }
            }
            str += new string('-', length);
            Debug.Log(str);
        }

        public List<Node> CalculatePath(Vector2Int start, Vector2Int end, bool returnClosestFoundPath = false)
        {
            //Debug.Log($"A*: begin working");
            //Debug.Log($"A*: start:{start}");
            //Debug.Log($"A*: end:{end}");

            List<Node> openSet = new List<Node>();
            HashSet<Node> closedSet = new HashSet<Node>();

            Node startNode = grid[start.x, start.y];
            Node endNode = grid[end.x, end.y];

            if (!endNode.walkable)
            {
                var closestNode = FindClosestWalkable(endNode);

                if (closestNode != null)
                {
                    endNode = closestNode;
                }
            }

            openSet.Add(startNode);

            bool pathFound = false;

            while (openSet.Count > 0)
            {
                Node current = openSet[0];

                for (int i = 1; i < openSet.Count; i++)
                {
                    if (openSet[i].fCost < current.fCost || (openSet[i].fCost == current.fCost && openSet[i].hCost < current.hCost))
                    {
                        current = openSet[i];
                    }
                }

                openSet.Remove(current);
                closedSet.Add(current);

                if (current == endNode)
                {
                    pathFound = true;
                    break;
                }

                foreach (var neighbour in GetNeighbours(current.x, current.y))
                {
                    if (!neighbour.walkable || closedSet.Contains(neighbour))
                    {
                        continue;
                    }

                    int newDistanceToNeighbour = current.gCost + GetDistance(current, neighbour);

                    if (newDistanceToNeighbour < neighbour.gCost || !openSet.Contains(neighbour))
                    {
                        neighbour.gCost = newDistanceToNeighbour;
                        neighbour.hCost = GetDistance(neighbour, endNode);
                        neighbour.parent = current;


                        if (!openSet.Contains(neighbour))
                        {
                            openSet.Add(neighbour);
                        }
                    }
                }
            }

            List<Node> path = null;

            if (pathFound)
            {
                path = new List<Node>();
                Node currentPathNode = endNode;

                while (currentPathNode != startNode)
                {
                    path.Add(currentPathNode);

                    currentPathNode = currentPathNode.parent;
                }

                path.Reverse();

                //Debug.Log($"A*: path founded with length of {path.Count}");
            }
            else
            {
                //Debug.Log($"A*: path not founded");
            }

            return path;
        }

        private Node FindClosestWalkable(Node node)
        {
            int maxSearchSize = 10;
            int currentSearchSize = 1;

            Node closestWalkable = null;

            int w = grid.GetLength(0);
            int h = grid.GetLength(1);

            do
            {
                for (int i = node.x - currentSearchSize; i <= node.x + currentSearchSize; i++)
                {
                    if (InBounds(i, node.y + currentSearchSize, w, h))
                    {
                        if (grid[i, node.y + currentSearchSize].walkable)
                        {
                            closestWalkable = grid[i, node.y + currentSearchSize];
                        }
                    }

                    if (closestWalkable != null) break;


                    if (InBounds(i, node.y - currentSearchSize, w, h))
                    {
                        if (grid[i, node.y - currentSearchSize].walkable)
                        {
                            closestWalkable = grid[i, node.y - currentSearchSize];
                        }
                    }

                    if (closestWalkable != null) break;
                }

                if (closestWalkable != null) break;

                for (int i = node.y - currentSearchSize; i <= node.y + currentSearchSize; i++)
                {
                    if (InBounds(node.x + currentSearchSize, i, w, h))
                    {
                        if (grid[node.x + currentSearchSize, i].walkable)
                        {
                            closestWalkable = grid[node.x + currentSearchSize, i];
                        }
                    }
                    if (closestWalkable != null) break;

                    if (InBounds(node.x - currentSearchSize, i, w, h))
                    {
                        if (grid[node.x - currentSearchSize, i].walkable)
                        {
                            closestWalkable = grid[node.x - currentSearchSize, i];
                        }
                    }

                    if (closestWalkable != null) break;
                }

                currentSearchSize++;
            }
            while (currentSearchSize < maxSearchSize && closestWalkable == null);

            return closestWalkable;
        }

        private Node[,] CreateGrid(float[,] grid, float[] notWalkable)
        {
            Node[,] nodes = new Node[grid.GetLength(0), grid.GetLength(1)];

            for (int i = 0; i < nodes.GetLength(0); i++)
            {
                for (int j = 0; j < nodes.GetLength(1); j++)
                {
                    bool walkable = true;

                    for (int q = 0; q < notWalkable.Length; q++)
                    {
                        if (grid[i, j].Equals(notWalkable[q]))
                        {
                            walkable = false;
                            break;
                        }
                    }

                    nodes[i, j] = new Node(i, j, walkable);
                }
            }

            return nodes;
        }

        private Node[,] CreateGrid(float[,] grid, float thresholdValue, IComparer<float> comparer)
        {
            Node[,] nodes = new Node[grid.GetLength(0), grid.GetLength(1)];

            for (int i = 0; i < nodes.GetLength(0); i++)
            {
                for (int j = 0; j < nodes.GetLength(1); j++)
                {
                    nodes[i, j] = new Node(i, j, (comparer.Compare(grid[i, j], thresholdValue) == 0 ? false : true));
                }
            }

            return nodes;
        }

        private bool InBounds(int x, int y, int w, int h)
        {
            return x >= 0 && y >= 0 && x < w && y < h;
        }

        private List<Node> GetNeighbours(int x, int y)
        {
            List<Node> nodes = new List<Node>();

            for (int i = -1; i <= 1; i++)
            {
                for (int j = -1; j <= 1; j++)
                {
                    if ((i == 0 && j == 0)) continue;

                    if (true  /*(i == 0 && j != 0) || (j == 0 && i != 0)*/)
                    {

                        if (InBounds(i + x, j + y, grid.GetLength(0), grid.GetLength(1)))
                        {
                            nodes.Add(grid[i + x, j + y]);
                        }
                    }
                }
            }

            return nodes;
        }

        private int GetDistance(Node a, Node b)
        {
            int dstX = Mathf.Abs(a.x - b.x);
            int dstY = Mathf.Abs(a.y - b.y);

            if (dstX > dstY)
                return 14 * dstY + 10 * (dstX - dstY);
            else return 14 * dstX + 10 * (dstY - dstX);

            //if (dstX > dstY)
            //    return 10 * dstY + 10 * (dstX - dstY);
            //else return 10 * dstX + 10 * (dstY - dstX);
        }
    }
}
