using UnityEngine;

namespace PWS.WoodCutting.Common
{
    [CreateAssetMenu(fileName = "AStarData",menuName ="PWS/WoodCutting/Common/AStar/Data")]
    public class AstarSerializedData : ScriptableObject
    {
        public Node[] Nodes;
        public int width, height;
    }
}
