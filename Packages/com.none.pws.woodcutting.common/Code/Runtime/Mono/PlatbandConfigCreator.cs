//using System.Collections;
//using System.Collections.Generic;
//using System.IO;
//using System.Linq;
//using UnityEditor;
//using UnityEngine;

//namespace PWS.WoodCutting.Common
//{

//    public class PlatbandConfigCreator : MonoBehaviour
//    {
//        public const string SAVE_PATH = "Configs/Platbands";

//        public string fileName = "Platband";
//        public SpriteConfig spriteConfig;
//        public MarkupToolConfig markupPaintingPatternConfig;
//        public List<PlankPatternCreateDataHelper> dataToCreate = new List<PlankPatternCreateDataHelper>();

//#if UNITY_EDITOR
//        public void Create()
//        {
//            if (dataToCreate.Count <= 0 || !spriteConfig) return;

//            string directoryPath = Application.dataPath + "/" + SAVE_PATH;

//            if (!Directory.Exists(directoryPath))
//                Directory.CreateDirectory(directoryPath);

//            string folderPath = directoryPath + "/" + fileName;

//            if (!Directory.Exists(folderPath))
//                Directory.CreateDirectory(folderPath);

//            string plankPatternsPath = folderPath + "/Plank Patterns";

//            if (!Directory.Exists(plankPatternsPath))
//                Directory.CreateDirectory(plankPatternsPath);


//            Pattern[] plankPatterns = new Pattern[dataToCreate.Count];

//            bool succsess = true;

//            int i = 0;
//            foreach (var item in dataToCreate)
//            {
//                if (!item.IsValid())
//                {
//                    succsess = false;
//                    Debug.LogError($"{item} is not valid!");
//                    break;
//                }

//                //PlankPatternConfig plankPatternConfig = ScriptableObject.CreateInstance<PlankPatternConfig>();

//                Pattern plankPatternConfig = null;

//                plankPatternConfig.main = item.main;
//                plankPatternConfig.prefab = item.prefab;

//                plankPatternConfig.width = item.regionMap.width;
//                plankPatternConfig.height = item.regionMap.height;

//                plankPatternConfig.regionMap = GetRegionMap(item.regionMap, item.regionMap,
//                    out var regionAmount,out var regionInfo);
//                plankPatternConfig.solidPattern = item.solidPattern;
//                plankPatternConfig.dashedPattern = item.dashedPattern;
//                plankPatternConfig.walkablePattern = item.walkablePattern;

//                plankPatternConfig.regionAmount = regionAmount;

//                plankPatternConfig.regionsIdx = regionInfo.Keys.ToArray();
//                plankPatternConfig.perRegionAmount = regionInfo.Values.ToArray();

//                plankPatternConfig.spriteConfig = spriteConfig;
//                plankPatternConfig.markupPaintingPatternConfig = markupPaintingPatternConfig;

//                plankPatterns[i] = plankPatternConfig;
//                i++;
//            }

//            if (succsess)
//            {
//                PlatbandConfig platbandConfig = ScriptableObject.CreateInstance<PlatbandConfig>();

//                platbandConfig.plankPatterns = new Pattern[plankPatterns.Length];

//                i = 0;
//                int additionalCounter = 0;

//                string assetPath = $"Assets/{SAVE_PATH}/{fileName}";

//                foreach (var pattern in plankPatterns)
//                {
//                    platbandConfig.plankPatterns[i] = pattern;

//                    string filePath = assetPath + "/Plank Patterns/" + $"{fileName} " +
//                        $"{(pattern.main ? "Main" : $"Additional")} Plank Pattern Config" +
//                        $"{(pattern.main ? "" : $" {(additionalCounter + 1)}")}.asset";


//                    //var asset = AssetDatabase.LoadAssetAtPath<PlankPatternConfig>(filePath);
//                    Pattern asset = null;

//                    if (asset==null)
//                    {
//                        asset.main = pattern.main;
//                        asset.width = pattern.width;
//                        asset.height = pattern.height;
//                        asset.prefab = pattern.prefab;
//                        asset.regionAmount = pattern.regionAmount;
//                        asset.regionMap = pattern.regionMap;
//                        asset.regionsIdx = pattern.regionsIdx;
//                        asset.perRegionAmount = pattern.perRegionAmount;
//                        asset.spriteConfig = pattern.spriteConfig;

//                        asset.solidPattern =    pattern.solidPattern;
//                        asset.dashedPattern =   pattern.dashedPattern;
//                        asset.walkablePattern = pattern.walkablePattern;

//                        asset.markupPaintingPatternConfig = pattern.markupPaintingPatternConfig;
//                    }
//                    //else AssetDatabase.CreateAsset(pattern, filePath);

//                    if (!pattern.main)
//                        additionalCounter++;

//                    i++;
//                }

//                assetPath = $"Assets/{SAVE_PATH}/{fileName}/" + $"{fileName} Config.asset";

//                var configAsset = AssetDatabase.LoadAssetAtPath<PlatbandConfig>(assetPath);

//                if (configAsset)
//                {
//                    Debug.Log("Overwriting...");
//                    configAsset.plankPatterns = platbandConfig.plankPatterns;
//                }
//                else
//                {
//                    Debug.Log("Creating...");
//                    AssetDatabase.CreateAsset(platbandConfig, assetPath);
//                }

//                AssetDatabase.SaveAssets();
//                AssetDatabase.Refresh();
//            }
//        }

//        public void Clear()
//        {
//            fileName = "Platband";
//            dataToCreate.Clear();
//        }

//        private Vector3Int[] GetRegionMap(Texture2D regionMap, Texture2D reference,
//            out int regionAmount,out Dictionary<int,int> regionInfo)
//        {
//            var output = new List<Vector3Int>();

//            var colors = regionMap.GetPixels();
//            var refColor = reference.GetPixels();

//            Dictionary<int, Vector2Int> regionIdx = new Dictionary<int, Vector2Int>();
//            regionInfo = new Dictionary<int, int>();

//            int currentRegion = 0;

//            for (int i = 0; i < colors.Length; i++)
//            {
//                var color = colors[i];

//                if (color.r > 0.0f && refColor[i].a >= spriteConfig.alphaThreshold)
//                {
//                    int key = (int)(color.r * 100f);

//                    if (!regionIdx.ContainsKey(key))
//                    {
//                        regionIdx.Add(key, new Vector2Int(currentRegion,0));
//                        currentRegion++;
//                    }

//                    int x = i % reference.width;
//                    int y = i / reference.width;

//                    Vector2Int newData = new Vector2Int(regionIdx[key].x, regionIdx[key].y+1);

//                    output.Add(new Vector3Int(x,y, newData.x));
//                    regionIdx[key] = newData;
//                }
//                else continue;
//            }

//            regionAmount = regionIdx.Count;

//            foreach (var key in regionIdx.Keys)
//            {
//                regionInfo.Add(regionIdx[key].x, regionIdx[key].y);
//            }

//            return output.ToArray();
//        }

//#endif

//    }
//}
