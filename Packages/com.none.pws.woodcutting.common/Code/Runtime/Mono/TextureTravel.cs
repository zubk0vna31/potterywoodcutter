using alicewithalex;
using JetBrains.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer), typeof(BoxCollider))]
public class TextureTravel : MonoBehaviour
{
    struct SearchData
    {
        public Vector3 position;
        public int sign;
        public int amount;
    }

    [Header("Tags")]
    public string interactionTag;

    [Header("Debug")]
    public Transform handProjectedMarker;
    public Transform sawProgress;

    [Range(0f, 0.25f)]
    public float expandDistance = 0.01f;

    [Header("Circle Tracing")]
    [Range(0f, 1f)]
    public float alphaThreshold = 0.5f;
    [Range(0f, 1f)]
    public float circleSearchAverageAlphaThreshold = 0.5f;
    [Range(1, 32)]
    public int circleTracingForwardDistance = 5;
    [Range(1, 32)]
    public int circleTracingSideDistance = 5;
    [Range(1, 32)]
    public int circleTracingRadius = 5;
    [Range(1, 32)]
    public int originSearchClosestNeightRadius = 16;
    [Range(5, 105)]
    public int circleTracingAngle = 60;
    [Range(1, 8)]
    public int tracingResolution = 2;

    [Header("Movement")]
    [Range(1, 8)]
    public int moveAmount = 1;
    [Range(0.01f, 5f)]
    public float rotationAmount = 1.25f;
    [Range(0.01f, 100f)]
    public float moveSmoothness = 25f;
    [Range(0.0001f, 0.1f)]
    public float rotationDistance = 0.1f;
    [Range(0.01f, 100f)]
    public float rotationSmoothness = 15f;
    [Range(0f, 1f)]
    public float dotProductThresholdToMove = 0.15f;
    public AnimationCurve rotationCurve;

    private Stickable stickable;
    private BoxCollider boxCollider;
    private Vector2 pixelSize;
    private Texture2D texture;
    private SpriteRenderer spriteRenderer;
    private int textureHash;
    private bool firstContact = true;
    private Vector3 offset;

    public float Percentage => 0f;

    void Start()
    {
        boxCollider = GetComponent<BoxCollider>();
        spriteRenderer = GetComponent<SpriteRenderer>();

        textureHash = Shader.PropertyToID("_Mask");
        texture = spriteRenderer.sprite.texture;

        spriteRenderer.material.SetTexture(textureHash, texture);

        Vector3 worldSize = boxCollider.size;

        worldSize.x *= transform.lossyScale.x;
        worldSize.y *= transform.lossyScale.y;
        worldSize.z *= transform.lossyScale.z;

        pixelSize = new Vector2(worldSize.x / texture.width, worldSize.y / texture.height);


        var listeners = GetComponentsInChildren<WoodCuttingTriggerListener>();

        foreach (var listener in listeners)
        {
            listener.onTriggerEnter += EnterZone;
        }

        handProjectedMarker.gameObject.SetActive(false);
    }

    private void EnterZone(Transform self, Collider other)
    {
        if (stickable || !other.CompareTag(interactionTag)) return;

        var target = other.GetComponentInParent<Stickable>();

        if (target)
        {
            if (target.IsStickable)
            {
                stickable = target;
                stickable.SetStick(true);


                RepositionStickable(self);

            }
        }
    }

    private void RepositionStickable(Transform zone)
    {
        //if (firstContact)
        //{
        //    Vector2Int origin = GetPixelPos(zone.position);
        //    Vector3 worldPos = zone.position;
        //    if (texture.GetPixel(origin.x, origin.y).a < alphaThreshold)
        //    {
        //        worldPos = GetWorldPos(GetClosestPointWithAlphaThreshold(origin,
        //            alphaThreshold, originSearchClosestNeightRadius));
        //    }
        //    else
        //    {
        //        worldPos = GetWorldPos(origin);
        //    }

        //    stickable.transform.forward = zone.forward;

        //    Vector3 offset = stickable.StickPoint.rotation * stickable.transform.InverseTransformPoint(
        //           stickable.StickPoint.position);

        //    stickable.transform.position = worldPos - offset;

        //    sawProgress.transform.position = worldPos;
        //    sawProgress.gameObject.SetActive(true);

        //    firstContact = false;
        //}
        //else
        //{
        //    Vector3 worldPos = sawProgress.transform.position;

        //    stickable.transform.forward = sawProgress.forward;

        //    Vector3 offset = stickable.StickPoint.rotation * stickable.transform.InverseTransformPoint(
        //           stickable.StickPoint.position);

        //    stickable.transform.position = worldPos - offset;
        //}

        Vector2Int origin = GetPixelPos(zone.position);
        Vector3 worldPos = zone.position;
        if (texture.GetPixel(origin.x, origin.y).a < alphaThreshold)
        {
            worldPos = GetWorldPos(GetClosestPointWithAlphaThreshold(origin,
                alphaThreshold, originSearchClosestNeightRadius));
        }
        else
        {
            worldPos = GetWorldPos(origin);
        }

        Vector3 handProjPos = GetWorldPosProjected(stickable.Interactor.position);

        this.offset = worldPos - handProjPos;

        stickable.transform.forward = zone.forward;

        Vector3 offset = stickable.StickPoint.rotation * stickable.transform.InverseTransformPoint(
               stickable.StickPoint.position);

        stickable.transform.position = worldPos - offset;



        handProjectedMarker.gameObject.SetActive(true);
    }

    void Update()
    {
        if (!stickable)
        {
            return;
        }

        if (!stickable.IsStickable)
        {
            stickable.SetStick(false);
            stickable = null;
            handProjectedMarker.gameObject.SetActive(false);
            return;
        }

        SphereTracing();
    }

    private void SphereTracing()
    {
        Vector3 handProjPos = GetWorldPosProjected(stickable.Interactor.position)+offset;

        handProjPos = ClampPositionInsideCollider(handProjPos, boxCollider, 0.15f);

        //Visual Debug
        handProjectedMarker.position = handProjPos;

        Vector3 targetProjPos = ClampPositionInsideCollider(
            GetWorldPosProjected(stickable.StickPoint.position), boxCollider);
        Vector3 dirToHand = (handProjPos - targetProjPos).normalized;


        Vector2Int origin = GetPixelPos(targetProjPos);
        if (texture.GetPixel(origin.x, origin.y).a < alphaThreshold)
        {
            Vector3 worldPos = GetWorldPos(GetClosestPointWithAlphaThreshold(origin,
                alphaThreshold, originSearchClosestNeightRadius));

            worldPos = worldPos -
                stickable.StickPoint.rotation * stickable.transform.InverseTransformPoint(
                    stickable.StickPoint.position);

            stickable.transform.position = Vector3.MoveTowards(
                stickable.transform.position, worldPos, Time.deltaTime * moveSmoothness);
            return;
        }

        //Debug.DrawLine(targetProjPos, handProjPos, Color.red);

        float distanceInWorldSpace = circleTracingForwardDistance * pixelSize.x;
        float sideDistanceInWorldSpace = circleTracingSideDistance * pixelSize.x;
        float radiusInWorldSpace = circleTracingRadius * pixelSize.x;

        Vector3 nextPos = targetProjPos + stickable.transform.forward * distanceInWorldSpace;

        Vector3 clampedNextPos = GetWorldPos(GetPixelPos(nextPos));

        //Debug.DrawLine(targetProjPos, clampedNextPos, Color.blue);


        //Search area setup

        Color sideLineColor = new Color(0.75f, 0.21f, 0.1f, 1f);

        //Colect data
        List<SearchData> searchData = new List<SearchData>();

        float deltaAngle = (circleTracingAngle) / tracingResolution;

        for (int i = -tracingResolution; i <= tracingResolution; i++)
        {
            SearchData data = new SearchData();

            Vector3 dir = (Quaternion.AngleAxis(
            deltaAngle * i, -transform.forward) * stickable.transform.forward).normalized;

            Vector3 circleOrigin = GetWorldPos(GetPixelPos(targetProjPos + dir * sideDistanceInWorldSpace));

            int amount = GetPixelAmountInRadiusWithAlphaThreshold(circleOrigin, circleTracingRadius, alphaThreshold);

            //if (i < 0)
            //    Debug.DrawLine(targetProjPos, circleOrigin, Color.yellow);
            //else if (i > 0) 
            //    Debug.DrawLine(targetProjPos, circleOrigin, Color.magenta);
            //else
            //    Debug.DrawLine(targetProjPos, circleOrigin, sideLineColor);


            data.position = circleOrigin;
            data.amount = amount;
            data.sign = Mathf.Clamp(i, -1, 1);

            searchData.Add(data);
        }

        Vector3 bestPos = searchData.Aggregate((x, y) => x.amount > y.amount ? x : y).position;
        Vector3 bestDir = (bestPos - targetProjPos).normalized;

        float dotProduct = Vector3.Dot(dirToHand, stickable.transform.forward);
        float rotMultiplier = rotationCurve.Evaluate(Mathf.InverseLerp(dotProductThresholdToMove, 1f, dotProduct));
        float distanceToHand = Mathf.Abs((handProjPos - targetProjPos).magnitude) / (rotationDistance);

        float angle = Vector3.SignedAngle(stickable.transform.forward, dirToHand, -transform.forward);
        float sign = Mathf.Sign(angle);
        //float amountMultiplier = 0;

        //if (dotProduct < dotProductThresholdToMove && sign != 0)
        //{
        //    if (sign < 0)
        //    {
        //        Vector3 bestPosOnSide = searchData.Where(x => x.sign < 0).Aggregate((x, y) =>
        //            x.amount > y.amount ? x : y).position;
        //        amountMultiplier = GetAverageAlphaInRegion(GetPixelPos(bestPosOnSide), circleTracingRadius);

        //        Debug.DrawLine(targetProjPos, bestPosOnSide, Color.green);
        //    }
        //    else if (sign > 0)
        //    {
        //        Vector3 bestPosOnSide = searchData.Where(x => x.sign > 0).Aggregate((x, y) =>
        //          x.amount > y.amount ? x : y).position;
        //        amountMultiplier = GetAverageAlphaInRegion(GetPixelPos(bestPosOnSide), circleTracingRadius);
        //        Debug.DrawLine(targetProjPos, bestPosOnSide, Color.green);
        //    }
        //}
        //else
        //{
        //    amountMultiplier = GetAverageAlphaInRegion(GetPixelPos(clampedNextPos), circleTracingRadius);
        //}

        //amountMultiplier = Mathf.InverseLerp(circleSearchAverageAlphaThreshold, 1f, amountMultiplier);


        //var rot = Quaternion.Slerp(stickable.transform.rotation,
        //    Quaternion.AngleAxis(rotationAmount * sign * rotMultiplier * distanceToHand * amountMultiplier,
        //    -transform.forward) * stickable.transform.rotation,
        //    Time.deltaTime * rotationSmoothness);

        var rot = Quaternion.Slerp(stickable.transform.rotation,
          Quaternion.AngleAxis(rotationAmount * sign * rotMultiplier * distanceToHand ,
          -transform.forward) * stickable.transform.rotation,
          Time.deltaTime * rotationSmoothness);

        angle = Quaternion.Angle(stickable.transform.rotation, rot);

        stickable.transform.RotateAround(stickable.StickPoint.position, -transform.forward, angle * sign);

        if (dotProduct < dotProductThresholdToMove)
        {
            return;
        }

        float moveMultiplier = Mathf.InverseLerp(dotProductThresholdToMove, 1f, dotProduct)
            *(Mathf.Lerp(0.25f,1f,Mathf.InverseLerp(0,1f,texture.GetPixel(origin.x, origin.y).a)));
        float worldSpaceMoveAmount = moveAmount * pixelSize.x;

        Vector3 nextWorldPos = targetProjPos + stickable.transform.forward * (worldSpaceMoveAmount * moveMultiplier);

        var nextPixelPos = GetPixelPos(nextWorldPos);

        if (!InBounds(handProjPos, boxCollider, expandDistance))
        {
            stickable.SetStick(false);
            stickable = null;
            handProjectedMarker.gameObject.SetActive(false);
            return;
        }

        if (!InBounds(nextWorldPos, boxCollider) || texture.GetPixel(nextPixelPos.x, nextPixelPos.y).a < alphaThreshold)
        {
            return;
        }

        Vector3 finalPos = nextWorldPos -
                stickable.StickPoint.rotation * stickable.transform.InverseTransformPoint(
                    stickable.StickPoint.position);

        stickable.transform.position = Vector3.MoveTowards(stickable.transform.position, finalPos,
            Time.deltaTime * moveSmoothness);
    }

    private void SphereTracing2()
    {
        Vector3 handProjPos = GetWorldPosProjected(stickable.Interactor.position);

        handProjPos = ClampPositionInsideCollider(handProjPos, boxCollider, 0.15f);

        //Visual Debug
        handProjectedMarker.position = handProjPos;

        Vector3 targetProjPos = ClampPositionInsideCollider(
            GetWorldPosProjected(stickable.StickPoint.position), boxCollider);
        Vector3 dirToHand = (handProjPos - targetProjPos).normalized;


        Vector2Int origin = GetPixelPos(targetProjPos);
        if (texture.GetPixel(origin.x, origin.y).a < alphaThreshold)
        {
            Vector3 worldPos = GetWorldPos(GetClosestPointWithAlphaThreshold(origin,
                alphaThreshold, originSearchClosestNeightRadius));

            worldPos = worldPos -
                stickable.StickPoint.rotation * stickable.transform.InverseTransformPoint(
                    stickable.StickPoint.position);

            stickable.transform.position = Vector3.MoveTowards(
                stickable.transform.position, worldPos, Time.deltaTime * moveSmoothness);
            return;
        }

        Debug.DrawLine(targetProjPos, handProjPos, Color.red);

        float distanceInWorldSpace = circleTracingForwardDistance * pixelSize.x;
        float sideDistanceInWorldSpace = circleTracingSideDistance * pixelSize.x;
        float radiusInWorldSpace = circleTracingRadius * pixelSize.x;

        origin = GetPixelPos(handProjPos);


        if (texture.GetPixel(origin.x, origin.y).a < alphaThreshold)
        {
            List<Vector2Int> circleTraceResult = new List<Vector2Int>(circleTracingRadius * circleTracingRadius);

            for (int i = origin.x - circleTracingRadius; i <= origin.x + circleTracingRadius; i++)
            {
                for (int j = origin.y - circleTracingRadius; j <= origin.y + circleTracingRadius; j++)
                {
                    if (InBounds(i, j, texture.width, texture.height) &&
                        InCircle(i, j, origin.x, origin.y, circleTracingRadius) && texture.GetPixel(i, j).a >= alphaThreshold)
                    {
                        circleTraceResult.Add(new Vector2Int(i, j));
                    }
                }
            }

            if (circleTraceResult.Count == 0) return;

            origin = circleTraceResult.Aggregate((x, y) => x + y) / circleTraceResult.Count;
        }

        Vector3 bestPos = GetWorldPos(origin);
        Vector3 bestDir = (bestPos - targetProjPos).normalized;
        Debug.DrawLine(targetProjPos, bestPos, Color.blue);

        float dotProduct = Vector3.Dot(bestDir, stickable.transform.forward);
        float rotMultiplier = rotationCurve.Evaluate(Mathf.InverseLerp(dotProductThresholdToMove, 1f, dotProduct));
        float distanceToHand = Mathf.Abs((bestPos - targetProjPos).magnitude) / (rotationDistance);

        float angle = Vector3.SignedAngle(stickable.transform.forward, bestDir, -transform.forward);
        float sign = Mathf.Sign(angle);

        //var rot = Quaternion.Slerp(stickable.transform.rotation,
        //    Quaternion.AngleAxis(rotationAmount * sign * rotMultiplier * distanceToHand,
        //    -transform.forward) * stickable.transform.rotation,
        //    Time.deltaTime * rotationSmoothness);

        var rot = Quaternion.Slerp(stickable.transform.rotation,
            Quaternion.AngleAxis(rotationAmount * sign * rotMultiplier * distanceToHand,
            -transform.forward) * stickable.transform.rotation,
            Time.deltaTime * rotationSmoothness);

        angle = Quaternion.Angle(stickable.transform.rotation, rot);

        stickable.transform.RotateAround(stickable.StickPoint.position, -transform.forward, angle * sign);

        if (dotProduct < dotProductThresholdToMove)
        {
            return;
        }

        float moveMultiplier = Mathf.InverseLerp(dotProductThresholdToMove, 1f, dotProduct);
        float worldSpaceMoveAmount = moveAmount * pixelSize.x;

        Vector3 nextWorldPos = targetProjPos + stickable.transform.forward * (worldSpaceMoveAmount * moveMultiplier);

        var nextPixelPos = GetPixelPos(nextWorldPos);

        if (!InBounds(handProjPos, boxCollider, expandDistance))
        {
            stickable.SetStick(false);
            stickable = null;
            handProjectedMarker.gameObject.SetActive(false);
            return;
        }

        if (!InBounds(nextWorldPos, boxCollider) || texture.GetPixel(nextPixelPos.x, nextPixelPos.y).a < alphaThreshold)
        {
            return;
        }

        Vector3 finalPos = nextWorldPos -
                stickable.StickPoint.rotation * stickable.transform.InverseTransformPoint(
                    stickable.StickPoint.position);

        stickable.transform.position = Vector3.MoveTowards(stickable.transform.position, finalPos,
            Time.deltaTime * moveSmoothness);

        sawProgress.transform.rotation = Quaternion.LookRotation(bestDir, -transform.forward);
        sawProgress.transform.position = nextWorldPos;
    }

    private bool InBounds(Vector3 pos, BoxCollider collider, float expand = -1f)
    {
        pos = transform.InverseTransformPoint(pos);
        pos.z = 0f;

        Vector3 size = collider.size;

        if (expand > 0)
        {
            size += Vector3.one * expand;
        }

        if (Mathf.Abs(pos.x) > size.x * 0.5f)
        {
            return false;
        }

        if (Mathf.Abs(pos.y) > size.y * 0.5f)
        {
            return false;
        }

        if (Mathf.Abs(pos.z) > size.z * 0.5f)
        {
            return false;
        }

        return true;
    }

    private Vector3 ClampPositionInsideCollider(Vector3 pos, BoxCollider collider, float expand = 0.0f)
    {
        pos = transform.InverseTransformPoint(pos);
        pos.z = 0f;

        Vector3 size = collider.size;

        expand = Mathf.Clamp01(expand);
        if (expand > 0.0f)
            size += new Vector3(size.x * 0.15f, size.y * 0.15f);

        if (Mathf.Abs(pos.x) > size.x * 0.5f)
        {
            pos.x = Mathf.Sign(pos.x) * size.x * 0.5f;
        }

        if (Mathf.Abs(pos.y) > size.y * 0.5f)
        {
            pos.y = Mathf.Sign(pos.y) * size.y * 0.5f;
        }

        return transform.TransformPoint(pos);
    }

    private Vector2Int GetClosestPointWithAlphaThreshold(Vector2Int origin, float alphaThreshold, int radius)
    {
        List<Vector2Int> pixels = new List<Vector2Int>();

        for (int i = origin.x - radius; i <= origin.x + radius; i++)
        {
            for (int j = origin.y - radius; j <= origin.y + radius; j++)
            {
                if (InBounds(origin.x, origin.y, texture.width, texture.height) &&
                    InCircle(i, j, origin.x, origin.y, radius))
                {
                    if (texture.GetPixel(i, j).a >= alphaThreshold)
                    {
                        pixels.Add(new Vector2Int(i, j));
                    }
                }
            }
        }

        if (pixels.Count <= 0)
        {
            return origin;
        }

        return pixels.OrderBy(x => (origin - x).sqrMagnitude).First();
    }

    private Vector2Int GetLastColoredPixelInLine(Vector2Int p1, Vector2Int p2)
    {
        var lineSamples = SampleLine(p1, p2);

        Vector2Int firstLastAlpha = p1;

        foreach (var p in lineSamples)
        {
            if (texture.GetPixel(p.x, p.y).a < alphaThreshold)
            {
                break;
            }
            else
            {
                firstLastAlpha = p;
            }
        }

        return firstLastAlpha;
    }

    private List<Vector2Int> GetPixelInRadius(Vector3 worldPos, int radius)
    {
        List<Vector2Int> pixels = new List<Vector2Int>();

        Vector2Int origin = GetPixelPos(worldPos);

        for (int i = origin.x - radius; i <= origin.x + radius; i++)
        {
            for (int j = origin.y - radius; j <= origin.y + radius; j++)
            {
                if (InBounds(origin.x, origin.y, texture.width, texture.height) &&
                    InCircle(i, j, origin.x, origin.y, radius))
                {
                    pixels.Add(new Vector2Int(i, j));
                }
            }
        }

        return pixels;
    }

    private float GetAverageAlphaInRegion(Vector2Int p, int radius)
    {
        float alpha = 0;
        int amount = 0;
        Vector2Int origin = p;

        for (int i = origin.x - radius; i <= origin.x + radius; i++)
        {
            for (int j = origin.y - radius; j <= origin.y + radius; j++)
            {
                if (InBounds(origin.x, origin.y, texture.width, texture.height) &&
                    InCircle(i, j, origin.x, origin.y, radius))
                {
                    amount++;
                    alpha += texture.GetPixel(i, j).a;
                }
            }
        }

        return alpha / amount;
    }

    private int GetPixelAmountInRadiusWithAlphaThreshold(Vector3 worldPos, int radius, float alphaThreshold = 0.5f)
    {
        int amount = 0;

        Vector2Int origin = GetPixelPos(worldPos);

        for (int i = origin.x - radius; i <= origin.x + radius; i++)
        {
            for (int j = origin.y - radius; j <= origin.y + radius; j++)
            {
                if (InBounds(origin.x, origin.y, texture.width, texture.height) &&
                    InCircle(i, j, origin.x, origin.y, radius))
                {
                    if (texture.GetPixel(i, j).a >= alphaThreshold)
                    {
                        amount++;
                    }
                }
            }
        }

        return amount;
    }

    private bool InCircle(int x, int y, int originX, int originY, int radius)
    {
        return (originX - x) * (originX - x) + (originY - y) * (originY - y) <= radius * radius;
    }

    private Vector2Int GetPixelPos(Vector3 worldPos)
    {
        Vector3 localPos = transform.InverseTransformPoint(worldPos);
        localPos.z = 0f;

        Vector3 size = boxCollider.size;

        localPos.x = Mathf.Clamp01((localPos.x + size.x * 0.5f) / (size.x));
        localPos.y = Mathf.Clamp01((localPos.y + size.y * 0.5f) / (size.y));

        int x = Mathf.FloorToInt(localPos.x * texture.width);
        int y = Mathf.FloorToInt(localPos.y * texture.height);

        return new Vector2Int(x, y);
    }

    private Vector3 GetWorldPos(Vector2Int pixelPos)
    {
        Vector3 localPos = Vector3.zero;
        Vector3 size = boxCollider.size;

        localPos.x = (pixelPos.x * 1.0f) / texture.width;
        localPos.y = (pixelPos.y * 1.0f) / texture.height;

        localPos.x = (localPos.x * size.x) - size.x * 0.5f + pixelSize.x * 0.5f;
        localPos.y = (localPos.y * size.y) - size.y * 0.5f + pixelSize.x * 0.5f;

        return transform.TransformPoint(localPos);
    }

    private Vector3 GetWorldPosProjected(Vector3 worldPos)
    {
        Vector3 localPos = transform.InverseTransformPoint(worldPos);
        localPos.z = 0f;

        return transform.TransformPoint(localPos);
    }

    private Vector2Int GetPixelByAngleAndDistanceFromOrigin(Vector2Int origin, Vector2 forwardDir,
        float angle, int distance)
    {
        forwardDir = Rotate(forwardDir, angle);

        Vector2 move = (forwardDir * distance);

        return origin + new Vector2Int((int)move.x, (int)move.y);
    }

    private Vector2Int GetPixelDirectionAndDistance(Vector2Int origin, Vector2 forwardDir, int distance)
    {
        Vector2 move = (forwardDir * distance);

        return origin + new Vector2Int((int)move.x, (int)move.y);
    }

    private Vector2 Rotate(Vector2 v, float degrees)
    {
        float sin = Mathf.Sin(degrees * Mathf.Deg2Rad);
        float cos = Mathf.Cos(degrees * Mathf.Deg2Rad);

        float tx = v.x;
        float ty = v.y;
        v.x = (cos * tx) - (sin * ty);
        v.y = (sin * tx) + (cos * ty);
        return v;
    }

    private bool InBounds(int x, int y, int w, int h)
    {
        return x >= 0 && x < w && y >= 0 && y < h;
    }

    public List<Vector2Int> SampleLine(Vector2Int p1, Vector2Int p2)
    {
        List<Vector2Int> points = new List<Vector2Int>();

        int dx = p2.x - p1.x;
        int dy = p2.y - p1.y;

        if (dx != 0 || dy != 0)
        {
            if (Math.Abs(dx) >= Math.Abs(dy))
            {
                float y = p1.y + 0.5f;
                float dly = ((float)dy) / dx;
                if (dx > 0)
                    for (int x = p1.x; x <= p2.x; x++)
                    {
                        points.Add(new Vector2Int(x, (int)(y)));
                        y += dly;
                    }
                else
                    for (int x = p1.x; x >= p2.x; x--)
                    {
                        points.Add(new Vector2Int(x, (int)(y)));
                        y -= dly;
                    }
            }
            else
            {
                float x = p1.x + 0.5f;
                float dlx = ((float)dx) / dy;
                if (dy > 0)
                    for (int y = p1.y; y <= p2.y; y++)
                    {
                        points.Add(new Vector2Int((int)(x), y));
                        x += dlx;
                    }
                else
                    for (int y = p1.y; y >= p2.y; y--)
                    {
                        points.Add(new Vector2Int((int)(x), y));
                        x -= dlx;
                    }
            }
        }



        return points;
    }

}
