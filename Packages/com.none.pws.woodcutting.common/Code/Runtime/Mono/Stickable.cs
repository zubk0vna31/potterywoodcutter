using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class Stickable : MonoBehaviour
{
    [SerializeField]
    private bool _isChangeHandMaterial;

    [SerializeField]
    private bool _isChangeStickedObjectMaterial;

    [SerializeField]
    private MeshRenderer _meshRenderer;

    [SerializeField]
    private Material _stickableGhostMaterial, _handGhostMaterial;

    [SerializeField, Range(0f, 25f)]
    private float _rotationSpeed, _movementSpeed;

    [SerializeField]
    private XRGrabInteractable _grabInteractable;

    [SerializeField]
    private Transform _stickPoint;

    public Transform StickPoint => _stickPoint;

    public Transform Interactor => _grabInteractable.selectingInteractor.transform;

    public bool IsStickable => _grabInteractable.isSelected;

    public bool IsSticked => _sticked;
    public float RotationSpeed => _rotationSpeed;
    public float MovementSpeed => _movementSpeed;

    public float StickSqrtDistance => _stickSqrtDistance;

    private float _stickSqrtDistance;
    private Material _initialMaterial;
    private bool _sticked;

    private MeshRenderer _leftHandRenderer, _rightHandRenderer;
    private Material _leftHandMaterial, _rightHandMaterial;

    public void Start()
    {
        if (!_meshRenderer)
        {
            _meshRenderer = GetComponentInChildren<MeshRenderer>();
        }

        _stickSqrtDistance = transform.InverseTransformPoint(_stickPoint.position).sqrMagnitude;

        _initialMaterial = _meshRenderer.material;

        _grabInteractable.selectEntered.AddListener(SelectEnter);
        _grabInteractable.selectExited.AddListener(SelectExit);
    }

    private void SelectEnter(SelectEnterEventArgs arg0)
    {
        if (_isChangeHandMaterial)
        {
            XRDirectInteractor directInteractor = arg0.interactor.GetComponent<XRDirectInteractor>();

            if (!directInteractor) return;

            if (IsLayerInsideLayerMask(LayerMask.NameToLayer("LeftHandGrab"), directInteractor.interactionLayerMask))
            {
                if (!_leftHandRenderer)
                {
                    _leftHandRenderer = arg0.interactor.GetComponentInChildren<MeshRenderer>();
                    _leftHandMaterial = _leftHandRenderer.material;
                }

                _leftHandRenderer.material = _handGhostMaterial;

            }
            else if (IsLayerInsideLayerMask(LayerMask.NameToLayer("RightHandGrab"), directInteractor.interactionLayerMask))
            {
                if (!_rightHandRenderer)
                {
                    _rightHandRenderer = arg0.interactor.GetComponentInChildren<MeshRenderer>();
                    _rightHandMaterial = _rightHandRenderer.material;
                }

                _rightHandRenderer.material = _handGhostMaterial;
            }
        }
    }

    private void SelectExit(SelectExitEventArgs arg0)
    {
        if (_isChangeHandMaterial)
        {
            XRDirectInteractor directInteractor = arg0.interactor.GetComponent<XRDirectInteractor>();

            if (!directInteractor) return;

            if (IsLayerInsideLayerMask(LayerMask.NameToLayer("LeftHandGrab"), directInteractor.interactionLayerMask))
            {
                if (!_leftHandRenderer)
                {
                    _leftHandRenderer = arg0.interactor.GetComponentInChildren<MeshRenderer>();
                    _leftHandMaterial = _leftHandRenderer.material;
                }

                _leftHandRenderer.material = _leftHandMaterial;

            }
            else if (IsLayerInsideLayerMask(LayerMask.NameToLayer("RightHandGrab"), directInteractor.interactionLayerMask))
            {
                if (!_rightHandRenderer)
                {
                    _rightHandRenderer = arg0.interactor.GetComponentInChildren<MeshRenderer>();
                    _rightHandMaterial = _rightHandRenderer.material;
                }

                _rightHandRenderer.material = _rightHandMaterial;
            }
        }
    }

    private bool IsLayerInsideLayerMask(int layer, LayerMask layerMask)
    {
        return layerMask == (layerMask | (1 << layer));
    }

    public void SetStick(bool value)
    {
        _grabInteractable.trackPosition = _grabInteractable.trackRotation = !value;

        _sticked = value;

        if (_isChangeStickedObjectMaterial)
            _meshRenderer.material = value ? _stickableGhostMaterial : _initialMaterial;
    }
}
