using PWS.WoodCutting.Common;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PlankPatternVisualizer : MonoBehaviour
{
    public static Color[] colors = new Color[]
    {
        new Color(1,1,1,1), // white
        new Color(1,0,0,1), // red
        new Color(0,1,0,1), // green
        new Color(0,0,1,1), // blue
        new Color(1,1,0,1), // yellow
        new Color(0,1,1,1), // cyan
        new Color(1,0,1,1), // magenta
        new Color(1,128f/255f,0f,1), // oragne
        new Color(119f/255f,0f,1f,1), // purple
        new Color(171f/255f,1f,0f,1), // lime
    };

    public Pattern plankPatternConfig;
    public MeshRenderer meshRenderer;
    [Range(1e-2f, 1f)]
    public float size = 0.1f;

    [ContextMenu("Visualize")]
    public void Visualize()
    {
        if (plankPatternConfig == null || meshRenderer == null) return;

        Texture2D texture = new Texture2D(
            plankPatternConfig.regionConfig.width, plankPatternConfig.regionConfig.height,
            TextureFormat.RGB24, false);

        texture.SetPixels(Enumerable.Repeat(Color.black, texture.width * texture.height).ToArray());
        texture.filterMode = FilterMode.Point;

        int randomOffset = Random.Range(0, colors.Length);

        foreach (var pixel in plankPatternConfig.regionConfig.regionMap)
        {
            texture.SetPixel(pixel.x, pixel.y, colors[(pixel.z + randomOffset )% colors.Length]);
        }

        texture.Apply();

        float aspectRatio = texture.width * 1.0f / texture.height;

        meshRenderer.sharedMaterial.SetTexture("_MainTex", texture);
        meshRenderer.transform.localScale = new Vector3(size, size / aspectRatio, 1f);
    }
}
