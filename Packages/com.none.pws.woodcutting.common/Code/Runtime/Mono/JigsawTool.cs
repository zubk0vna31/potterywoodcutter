using PWS.WoodCutting.Common;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class JigsawTool : Tool
{
    [SerializeField]
    private TriggerPainter triggerPainter;
    [SerializeField]
    private Stickable stickable;

    public override void ToggleStickableComponents(bool value)
    {
        base.ToggleStickableComponents(value);

        if (!value)
        {
            triggerPainter.Toggle(false);
        }
        else
        {
            if (stickable.IsSticked && Activated)
                triggerPainter.Toggle(true);
        }
    }

    public override void SetConfigData(ToolConfig config)
    {
        base.SetConfigData(config);
        triggerPainter.SetRadius(config.triggerPainterRadius);
    }

    protected override void OnActivated(ActivateEventArgs arg)
    {
        base.OnActivated(arg);
        if (stickable.IsSticked)
            triggerPainter.Toggle(true);
    }

    protected override void OnDeactivated(DeactivateEventArgs arg)
    {
        base.OnDeactivated(arg);
        triggerPainter.Toggle(false);
    }

    protected override void OnSelectExited(SelectExitEventArgs arg)
    {
        base.OnSelectExited(arg);
        triggerPainter.Toggle(false);
    }

#if UNITY_EDITOR

    [ContextMenu("Collect References")]
    protected override void CollectReferences()
    {
        base.CollectReferences();
        CollectReferenceInChildren(out triggerPainter);
    }

#endif
}
