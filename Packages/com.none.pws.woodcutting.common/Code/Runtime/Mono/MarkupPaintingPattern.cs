using DG.Tweening;
using PWS.WoodCutting.Common;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;



[RequireComponent(typeof(MeshFilter), typeof(MeshRenderer), typeof(BoxCollider))]
public class MarkupPaintingPattern : MonoBehaviour
{
    [Header("Interaction")]
    public string _interactionTag;

    [Header("Shader")]
    public Shader _mainShader;

    [Header("Debug")]
    public Pattern plankPatternConfig;
    public Color normalColor, markupColor;
    [Range(0f, 1f)]
    public float clipping = 0.5f;
    [Range(0f, 1f)]
    public float maskValue = 0.0f;

    private MeshRenderer _meshRenderer;
    private BoxCollider _boxCollider;
    private Material _material;
    private Texture2D _solidTexture;
    private Texture2D _maskTexture;
    private Stickable _stickable;

    private MarkupToolConfig _markupToolConfig;
    private float _alphaThreshold;

    private int _referencePixelAmount;
    private int _coloredPixelAmount;

    private bool _snapped;
    private bool _active;

    public float Progress
    {
        get
        {
            return Mathf.Clamp01((_coloredPixelAmount * 1.0f) / _referencePixelAmount);
        }
    }

    public void Initialize(Pattern pattern,
         Color normalColor, Color markupColor, float clipping)
    {
        _markupToolConfig = pattern.markupToolConfig;

        _solidTexture = pattern.spriteConfig.solidPattern.texture;
        _referencePixelAmount = pattern.spriteConfig.solidRefPixelAmount;
        _alphaThreshold = pattern.patternConfig.alphaThreshold;

        Rebuild(pattern, normalColor, markupColor, clipping);
        _boxCollider = RecalculateCollider(MeshConfig.GetColliderCenter(
            pattern.patternConfig.colliderThickness, true),
            MeshConfig.GetSize(pattern.meshConfig, pattern.patternConfig.colliderThickness));

        _active = true;

        _meshRenderer = GetComponent<MeshRenderer>();
        _material = _meshRenderer.material;

        _maskTexture = new Texture2D(pattern.spriteConfig.dashedPattern.texture.width,
            pattern.spriteConfig.dashedPattern.texture.height, TextureFormat.R8, false);
        _maskTexture.filterMode = FilterMode.Bilinear;

        _maskTexture.SetPixels(Enumerable.Repeat(Color.black, _maskTexture.width * _maskTexture.height).ToArray());
        _maskTexture.Apply();

        _material.SetTexture("_Mask", _maskTexture);
        _material.SetInt("_PaintAll", 0);
        _material.SetFloat("_PaintAllValue", 0f);
    }

    private BoxCollider RecalculateCollider(Vector3 colliderCenter, Vector3 colliderSize)
    {
        var c1 = transform.GetComponent<BoxCollider>();

        if (!c1.isTrigger)
            c1.isTrigger = true;

        c1.center = colliderCenter;
        c1.size = colliderSize;

        return c1;
    }

    private void Rebuild(Pattern patternConfig,
        Color normalColor, Color markupColor, float clipping)
    {
        Mesh mesh = new Mesh();

        mesh.vertices = patternConfig.meshConfig.vertices;
        mesh.normals = patternConfig.meshConfig.normals;
        mesh.triangles = patternConfig.meshConfig.triangles;
        mesh.uv = patternConfig.meshConfig.uv;

        mesh.RecalculateNormals();
        mesh.RecalculateBounds();

        GetComponent<MeshFilter>().mesh = mesh;

        var mat = new Material(_mainShader);

        mat.SetTexture("_MainTex", patternConfig.spriteConfig.dashedPattern.texture);
        mat.SetTexture("_MarkupTex", patternConfig.spriteConfig.solidPattern.texture);

        mat.SetColor("_Color1", normalColor);
        mat.SetColor("_Color2", markupColor);
        mat.SetFloat("_Clipping", clipping);

        GetComponent<MeshRenderer>().material = mat;
    }

    private void Update()
    {
        if (!_active || !_stickable || !_stickable.IsStickable)
        {
            if (_stickable)
            {
                _snapped = false;
                _stickable.SetStick(false);
                _stickable = null;
            }
            return;
        }


        Reposition(_stickable);

        if (!_snapped)
        {
            if (_stickable)
            {
                _snapped = false;
                _stickable.SetStick(false);
                _stickable = null;
            }
        }

        if (_stickable)
        {
            Paint(_stickable.StickPoint.position);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!_active || !other.CompareTag(_interactionTag) || _stickable) return;

        var stickable = other.GetComponentInParent<Stickable>();

        if (!stickable.IsStickable) return;

        _stickable = stickable;
        _stickable.SetStick(true);
    }

    private void OnTriggerStay(Collider other)
    {
        if (!_active || !other.CompareTag(_interactionTag) || _stickable) return;

        var stickable = other.GetComponentInParent<Stickable>();

        if (!stickable.IsStickable) return;

        _stickable = stickable;
        _stickable.SetStick(true);
    }

    public void Deactivate()
    {
        _active = false;
        if (_stickable)
        {
            _stickable.SetStick(false);
            _stickable = null;
            _snapped = false;
        }
    }

    private void Reposition(Stickable _target)
    {

        Vector3 closestPoint = _boxCollider.ClosestPoint(_target.Interactor.position);
        Vector3 normal = -transform.forward;

        //Debug.DrawLine(transform.position, transform.position + normal, Color.green, 1f);


        Vector3 interactorForward = _target.Interactor.forward;
        Vector3 projectedOnNormalInteractorDirection = Vector3.ProjectOnPlane(interactorForward, normal).normalized;

        //Debug.DrawLine(transform.position, transform.position + projectedOnNormalInteractorDirection, Color.blue, 1f);


        Vector3 stickLocalToParent = _target.transform.InverseTransformPoint(_target.StickPoint.position);

        Vector3 pointOnTexture = transform.TransformPoint(GetClosestPointOnTexture(closestPoint, out var isFound))
            - _target.transform.rotation * stickLocalToParent;

        //Debug.DrawLine(transform.position, closestPoint, Color.green, 1f);

        //if(isFound)
        //    Debug.DrawLine(transform.position, pointOnTexture, Color.yellow, 1f);


        if (!isFound ||
            ((closestPoint - _target.Interactor.position).sqrMagnitude >= _markupToolConfig.SquaredDistance))
        {
            _snapped = false;
            return;
        }

        if (!_snapped && isFound)
        {
            _snapped = true;
        }


        _target.transform.rotation = Quaternion.Slerp(_target.transform.rotation, Quaternion.LookRotation(
            projectedOnNormalInteractorDirection, normal) * Quaternion.Inverse(_target.StickPoint.localRotation),
            Time.deltaTime * _target.RotationSpeed);

        _target.transform.position = Vector3.Lerp(_target.transform.position, pointOnTexture,
           Time.deltaTime * _target.MovementSpeed);
    }

    private Vector3 GetClosestPointOnTexture(Vector3 position, out bool isFound)
    {
        Vector2 bounds = new Vector2(_boxCollider.size.x, _boxCollider.size.y) * 0.5f;

        position = transform.InverseTransformPoint(position);
        position.z = 0f;

        isFound = false;

        var initialPos = position;

        position.x /= bounds.x;
        position.y /= bounds.y;

        position.x = (position.x + 1) * 0.5f;
        position.y = (position.y + 1) * 0.5f;

        int x = (int)Mathf.Lerp(0, _maskTexture.width, position.x);
        int y = (int)Mathf.Lerp(0, _maskTexture.height, position.y);

        HashSet<Vector2Int> searchResult = new HashSet<Vector2Int>();

        var searchRadius = _markupToolConfig.PixelSnapRadius;

        for (int i = x - searchRadius; i <= x + searchRadius; i++)
        {
            for (int j = y - searchRadius; j <= y + searchRadius; j++)
            {
                if (InBounds(i, j, _solidTexture.width, _solidTexture.height))
                {
                    if (_solidTexture.GetPixel(i, j).a >
                        _alphaThreshold /*0.95f*/)
                    {
                        searchResult.Add(new Vector2Int(i, j));
                    }
                }
            }
        }



        if (searchResult.Count == 0) return initialPos;

        isFound = true;

        Vector2Int closestPixel = searchResult.OrderBy(value => (new Vector2Int(x, y) - value).sqrMagnitude).First();

        Vector3 resultPosition = new Vector3(closestPixel.x, closestPixel.y, 0f);

        resultPosition.x = Mathf.InverseLerp(0, _solidTexture.width, resultPosition.x);
        resultPosition.y = Mathf.InverseLerp(0, _solidTexture.height, resultPosition.y);

        resultPosition.x = (resultPosition.x * 2) - 1;
        resultPosition.y = (resultPosition.y * 2) - 1;

        resultPosition.x *= bounds.x;
        resultPosition.y *= bounds.y;

        return resultPosition;
    }

    private void Paint(Vector3 position)
    {
        Vector3 localPos = transform.InverseTransformPoint(_boxCollider.ClosestPoint(position));
        localPos.z = 0;

        Vector2 bounds = new Vector2(_boxCollider.size.x, _boxCollider.size.y) * 0.5f;

        localPos.x /= bounds.x;
        localPos.y /= bounds.y;

        localPos.x = (localPos.x + 1) * 0.5f;
        localPos.y = (localPos.y + 1) * 0.5f;

        int x = (int)Mathf.Lerp(0, _maskTexture.width, localPos.x);
        int y = (int)Mathf.Lerp(0, _maskTexture.height, localPos.y);

        int brushSize = _markupToolConfig.PixelBrushSize;

        for (int i = x - brushSize; i <= x + brushSize; i++)
        {
            for (int j = y - brushSize; j <= y + brushSize; j++)
            {
                if (InBounds(i, j, _maskTexture.width, _maskTexture.height))
                {
                    if (_maskTexture.GetPixel(i, j).r <= 0)
                    {
                        _maskTexture.SetPixel(i, j, Color.red);
                        if (_solidTexture.GetPixel(i, j).a >=
                            _alphaThreshold /*0.5f*/)
                            _coloredPixelAmount++;
                    }
                }
            }
        }

        _maskTexture.Apply();

        _material.SetTexture("_Mask", _maskTexture);
    }

    private bool InBounds(int x, int y, int w, int h)
    {
        return x >= 0 && x < w && y >= 0 && y < h;
    }

    public void PaintAllWithDuration(float duration)
    {
        if (!_active) return;



        int pixelAmount = _coloredPixelAmount;

        duration = duration * (1f - Progress);

        DOTween.To(() => pixelAmount, x => pixelAmount = x, _referencePixelAmount, duration).OnUpdate
            (() => _coloredPixelAmount = pixelAmount);

        _material.SetInt("_PaintAll", 1);

        Deactivate();

        float value = 0f;
        DOTween.To(() => value, x => value = x, 1f, duration)
            .OnUpdate(() =>
            {
                _material.SetFloat("_PaintAllValue", value);
            });
    }

    public void ResetComponent()
    {
        _coloredPixelAmount = 0;

        _active = false;
        if (_stickable)
        {
            _stickable.SetStick(false);
            _stickable = null;
            _snapped = false;
        }
    }
}
