using DG.Tweening;
using PWS.WoodCutting;
using UnityEngine;

public struct RigidbodyData
{
    public Vector3 pos;
    public Quaternion rot;
    public Vector3 scale;
    public bool colliderEnabled;
    public bool isKinematic;
}

public class DrillObject : MonoBehaviour
{

    [Header("Target")]
    public GameObject _target;

    [Space(10)]
    public string _interactionTag;

    [Space(10)]
    public GameObject _zoneVisual;

    [Space(10)]
    [Range(0f, 50f)]
    public float _speed;
    [Range(0.00001f, 0.1f)]
    public float _moveAmount;

    [Space(10)]
    public Axis _moveAxis;
    public bool _negate;

    [Space(10)]
    [Range(0f, 0.25f)]
    public float _maxDst;
    [Range(0f, 0.25f)]
    public float _sqrDst;

    private Rigidbody _rigidbody;
    private Collider _collider;

    private DrillTool _drillTool;
    private Stickable _stickable;

    private Vector3 _direction;
    private Vector3 _initialPosition;

    private float _startLocalPos, _endLocalPos, _current, _drillAmount;
    private float _height;
    private int _moveSign;

    private bool _active, _skipping, _initialized;

    public float Percentage => Mathf.InverseLerp(_startLocalPos, _endLocalPos, _drillAmount);

    public void Initialize()
    {
        _initialized = true;

        _rigidbody = _target.GetComponentInChildren<Rigidbody>();
        _collider = _target.GetComponentInChildren<Collider>();

        _direction = Vector3.zero;
        _direction[(int)_moveAxis] = _negate ? -1 : 1;

        if (transform.parent)
        {
            _direction = _rigidbody.rotation * _direction;
        }

        _initialPosition = _rigidbody.position;

        _moveSign = _negate ? -1 : 1;

        _height = _maxDst * 0.5f;

        _startLocalPos = _height * (-_moveSign);
        _endLocalPos = _height * _moveSign;

        _endLocalPos *= 1.25f;

        _current = _startLocalPos;
        _drillAmount = _current;

        _zoneVisual.gameObject.SetActive(false);

        _active = true;
    }

    private void Update()
    {
        if (!_initialized || !_active || _skipping) return;

        Move();
        Drill();
    }

    public RigidbodyData GetDrillData()
    {
        return new RigidbodyData()
        {
            pos = _target.transform.position,
            rot = _target.transform.rotation,
            colliderEnabled = _target.GetComponentInChildren<Collider>().enabled,
            isKinematic = _target.GetComponentInChildren<Rigidbody>().isKinematic
        };
    }

    public void ResetComponent(RigidbodyData data)
    {
        _skipping = false;
        _active = true;

        _target.transform.position = data.pos;
        _target.transform.rotation= data.rot;
        _target.GetComponentInChildren<Collider>(true).enabled = data.colliderEnabled;
        _target.GetComponentInChildren<Rigidbody>(true).isKinematic = data.isKinematic;
        _zoneVisual.SetActive(true);

        _current = _startLocalPos;
        _drillAmount = _current;

        if (_stickable)
        {
            _stickable.SetStick(false);
            _stickable = null;
            _drillTool = null;
        }
    }

    private void Move()
    {
        if (_stickable == null || !_stickable.IsStickable)
        {
            if (_stickable)
            {
                _stickable.SetStick(false);
                _stickable = null;
                _drillTool = null;
            }

            return;
        }

        float currentHeight = _current;

        if ((_drillTool && _drillTool.Activated))
        {
            var targetHeight = _current + _moveSign * _moveAmount;

            //Debug.Log($"Current:{_current}");
            //Debug.Log($"Target:{targetHeight}");
            //Debug.Log($"Move Sign:{_moveSign}");
            //Debug.Log($"Start:{_startLocalPos * -_moveSign}");
            //Debug.Log($"End:{_endLocalPos * -_moveSign}");

            currentHeight = Mathf.Clamp(Mathf.Lerp(_current, targetHeight,
                Time.deltaTime * _speed), _endLocalPos * -_moveSign, _startLocalPos * -_moveSign);
        }

        Vector3 currentPos = transform.TransformPoint(Vector3.up * currentHeight)
           - _stickable.transform.rotation * _stickable.StickPoint.localPosition;

        if ((currentPos - _stickable.Interactor.position).sqrMagnitude >= _sqrDst)
        {
            if (_stickable)
            {
                _stickable.SetStick(false);
                _stickable = null;
                _drillTool = null;
            }
            return;
        }

        if (_moveSign <= 0)
        {
            if (currentHeight < _current && currentHeight < _drillAmount)
            {
                _drillAmount = currentHeight;
            }
        }
        else
        {
            if (currentHeight > _current && currentHeight > _drillAmount)
            {
                _drillAmount = currentHeight;
            }
        }

        _current = currentHeight;
        _stickable.transform.position = currentPos;

        //Rotate
        Vector3 interactorForward = _stickable.Interactor.forward;
        Vector3 projectedOnNormalInteractorDirection = Vector3.ProjectOnPlane(interactorForward, transform.up).normalized; ;

        var angleAmount = Quaternion.Angle(_stickable.transform.rotation, Quaternion.LookRotation(
            projectedOnNormalInteractorDirection, transform.up) * Quaternion.Inverse(_stickable.StickPoint.localRotation));

        float angle = Vector3.SignedAngle(_stickable.StickPoint.forward,
            projectedOnNormalInteractorDirection, transform.up);
        float sign2 = Mathf.Sign(angle);


        _stickable.transform.RotateAround(_stickable.StickPoint.position,
            transform.up, sign2 * angleAmount);
    }

    private void Drill()
    {
        if (!_skipping)
        {
            if (!_drillTool || !_drillTool.Activated) return;
        }

        _rigidbody.position = Vector3.Lerp(_rigidbody.position,
            transform.TransformPoint(Vector3.up * (_drillAmount - _height)), Time.deltaTime * _speed);

        if ((_initialPosition - _rigidbody.position).sqrMagnitude >= _maxDst * _maxDst)
        {
            _collider.enabled = true;
            _rigidbody.isKinematic = false;
            _drillTool = null;
            _drillAmount = _endLocalPos;

            if (_stickable)
            {
                _stickable.SetStick(false);
                _stickable = null;
            }

            Deactivate();
        }
    }

    public void Activate()
    {
        _active = true;
        _zoneVisual.gameObject.SetActive(true);
    }

    public void Deactivate()
    {
        _active = false;
        _zoneVisual.gameObject.SetActive(false);
    }

    public void DrillWithDuration(float targetTime)
    {
        _skipping = true;

        if (_stickable)
        {
            _stickable.SetStick(false);
            _stickable = null;
            _drillTool = null;
        }

        float delta = 1f - Percentage;
        targetTime = targetTime * delta;

        float value = Percentage;

        DOTween.To(() => value, x => value = x, 1f, targetTime).
            OnUpdate(() =>
            {
                _drillAmount = _current = Mathf.Lerp(_startLocalPos, _endLocalPos, value);
                Drill();
            })
            .OnComplete(() => Deactivate());
    }

    private void OnTriggerStay(Collider other)
    {
        if (!_initialized || !_active || _collider.enabled || !other.CompareTag(_interactionTag) || _stickable) return;

        var stickable = other.GetComponentInParent<Stickable>();

        if (!stickable || !stickable.IsStickable || stickable.IsSticked) return;

        _stickable = stickable;
        _stickable.SetStick(true);

        _drillTool = _stickable.GetComponent<DrillTool>();

        //Rotate
        Vector3 interactorForward = _stickable.Interactor.forward;
        Vector3 projectedOnNormalInteractorDirection = Vector3.ProjectOnPlane(interactorForward, transform.up).normalized; ;


        _stickable.transform.rotation = Quaternion.LookRotation(
            projectedOnNormalInteractorDirection, transform.up) * Quaternion.Inverse(_stickable.StickPoint.localRotation);


        _stickable.transform.position = transform.TransformPoint(Vector3.up * (_current)) - _stickable.transform.rotation *
             _stickable.StickPoint.localPosition;
    }
}


