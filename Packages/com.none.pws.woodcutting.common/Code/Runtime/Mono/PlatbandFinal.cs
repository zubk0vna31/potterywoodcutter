using UnityEngine;

namespace PWS.WoodCutting.Common
{
    public class PlatbandFinal : MonoBehaviour
    {
        [SerializeField]
        private Renderer[] renderers;

        void Start()
        {
            if(renderers==null || renderers.Length == 0)
            {
                Collect();
            }
        }

        public void UpdatePaintedColor(Color color,float paintedAmount=0)
        {
            foreach (var r in renderers)
            {
                r.material.SetColor("_PaintedColor", color);
                r.material.SetFloat("_CustomAmount", paintedAmount);
            }
        }


        [ContextMenu("Collect")]
        public void Collect(Material material = default(Material))
        {
            renderers = GetComponentsInChildren<Renderer>();

            if (material && renderers != null)
            {
                foreach (var r in renderers)
                {
                    r.gameObject.layer = 29;
                    r.sharedMaterial = material;
                }
            }
        }
    }
}
