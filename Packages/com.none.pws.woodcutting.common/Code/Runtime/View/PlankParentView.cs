using Leopotam.Ecs;
using Modules.ViewHub;
using UnityEngine;

namespace PWS.WoodCutting.Common
{
    public class PlankParentView : ViewComponent
    {
        public Transform [] planks;
        public Transform shiled;

        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            ecsEntity.Get<PlankParent>().transform = transform;
            ecsEntity.Get<PlankParent>().view = this;
        }

        public void Switch()
        {
            foreach (var p in planks)
            {
                p.gameObject.SetActive(false);
            }

            shiled.gameObject.SetActive(true);
        }
    }
}
