using DG.Tweening;
using Leopotam.Ecs;
using Modules.ViewHub;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace PWS.WoodCutting.Common
{
    [System.Serializable]
    public struct ToningEditorData
    {
        public Renderer renderer;
        public float uvArea;
    }
    
    public class PlankFinalViewComponent : ViewComponent
    {
        [Header("State Transforms")]
        [SerializeField]
        private Transform state6;
        [SerializeField]
        private Transform state7;
        [SerializeField]
        private Transform state8;
        [SerializeField]
        private Transform state9;
        [SerializeField]
        private Transform state10;

        [Header("Components")]
        [SerializeField]
        private MarkupPaintingPattern markupPaintingPattern;
        [SerializeField]
        private VertexPathStickyPattern sawingPattern;
        [SerializeField]
        private Paintable sawingPaintable;
        [SerializeField]
        private VertexPathStickyPattern chamferingPattern;
        [SerializeField]
        private Paintable chamferPaintable;


        [Header("Editor Collected Data")]
        [SerializeField]
        private Transform sawedFinalObject;
        [SerializeField]
        private Transform chamferedFinalObject;
        [SerializeField]
        private Paintable toningPaintable;
        [SerializeField]
        private SawingPart[] otherSawingParts;
        [SerializeField]
        private ToningEditorData [] toningEditorData;


        public Transform State6 => state6;
        public Transform State7 => state7;
        public Transform State8 => state8;
        public Transform State9 => state9;
        public Transform State10 => state10;
        public StickyObject StickyObject => State10.GetChild(0).GetComponent<StickyObject>();
        public Transform SawedFinalObject
        {
            set => sawedFinalObject = value;
        }
        public Transform ChamferedFinalObject
        {
            get => chamferedFinalObject;
            set => chamferedFinalObject = value;
        }
        public Paintable ToningPaintable
        {
            set => toningPaintable = value;
        }

        public void ShowChamferedObject(float delay)
        {
            float value = 0f;
            DOTween.To(() => value, x => value = x, 1f, delay)
                .OnComplete(() =>
                {
                    sawedFinalObject.gameObject.SetActive(false);
                    chamferedFinalObject.gameObject.SetActive(true);
                    State9.gameObject.SetActive(false);
                });
        }


        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            ref var plank = ref ecsEntity.Get<PlankFinal>();

            // Pass components
            plank.markupPaintingPattern = markupPaintingPattern;
            plank.sawingPattern = sawingPattern;
            plank.sawingPaintable = sawingPaintable;
            plank.chamferingPattern = chamferingPattern;
            plank.chamferPaintable = chamferPaintable;
            plank.toningPaintable = toningPaintable;

            plank.view = this;

            // Activate only markup painting state
            state6.gameObject.SetActive(true);
            state7.gameObject.SetActive(false);
            state8.gameObject.SetActive(false);
            state9.gameObject.SetActive(false);
            state10.gameObject.SetActive(false);
            chamferedFinalObject.gameObject.SetActive(false);

            // Data
            plank.queue = new Queue<ToningSubstateData>();
            foreach (var data in toningEditorData)
            {
                plank.queue.Enqueue(new ToningSubstateData()
                {
                    renderer = data.renderer,
                    uvArea = data.uvArea
                });
            }
        }

        public void PassOtherSawingParts(SawingPart[] other)
        {
            otherSawingParts = other;
        }

        public void PassToningEditorData(ToningEditorData[] data)
        {
            toningEditorData = data;
        }

        public void EnableOtherSawingParts(float duration, float delay)
        {
            foreach (var part in otherSawingParts)
            {
                part.Toggle(true);
                part.Shrink(duration, delay);
            }
        }
    }
}
