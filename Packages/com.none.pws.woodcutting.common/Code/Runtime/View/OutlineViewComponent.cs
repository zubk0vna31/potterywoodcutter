using Leopotam.Ecs;
using Modules.ViewHub;
using System;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace PWS.WoodCutting.Common
{

    public class OutlineViewComponent : ViewComponent
    {
        public Outline outline;
        [Range(1, 32)]
        public int stateID = 0;

        private bool inCurrentState;
        private bool selected;

        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            ecsEntity.Get<ToolOutline>().view = this; ;
            outline.enabled = false;

            XRBaseInteractable grabInteractable = GetComponentInChildren<XRBaseInteractable>();

            if (grabInteractable)
            {
                grabInteractable.selectEntered.AddListener(Enter);
                grabInteractable.selectExited.AddListener(Exit);
            }
        }

        private void Exit(SelectExitEventArgs arg0)
        {
            selected = false;

            if (!inCurrentState)
            {
                if (outline.enabled) outline.enabled = false;
                return;
            }

            outline.enabled = true;
        }

        private void Enter(SelectEnterEventArgs arg0)
        {
            selected = true;

            if (!inCurrentState)
            {
                if (outline.enabled) outline.enabled = false;
                return;
            }

            outline.enabled = false;
        }

        public void UpdateStateDependency(int state)
        {
            inCurrentState = state==stateID;

            if (!inCurrentState)
            {
                outline.enabled = false;
            }
            else
            {
                if(!selected)
                    outline.enabled = true;
            }
        }
    }

}