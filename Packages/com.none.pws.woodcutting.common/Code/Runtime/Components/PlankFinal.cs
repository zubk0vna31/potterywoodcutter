using DG.Tweening;
using PWS.WoodCutting.Common;
using System.Collections.Generic;
using UnityEngine;

namespace PWS.WoodCutting
{
    public struct ToningSubstateData
    {
        public Renderer renderer;
        public float uvArea;
    }

    public struct PlankFinal
    {
        // Base
        public Pattern pattern;
        public PlankFinalViewComponent view;
        public float currentPercentage;

        // Components
        public MarkupPaintingPattern markupPaintingPattern;
        public VertexPathStickyPattern sawingPattern;
        public Paintable sawingPaintable;
        public VertexPathStickyPattern chamferingPattern;
        public Paintable chamferPaintable;
        public Paintable toningPaintable;

        // Data
        public Queue<ToningSubstateData> queue;
        public Queue<Renderer> renderers;

        // Properties
        public float Percentage
        {
            get
            {
                return toningPaintable.PaintedPercentage + currentPercentage;
            }
        }
        public ToningSubstateData Last => queue.Peek();

        public bool IsEmpty => queue.Count == 0;

        // Functions
        public void Complete(float time=0f)
        {
            if (!toningPaintable.Active || IsEmpty) return;

            if (renderers == null)
            {
                renderers = new Queue<Renderer>();
            }

            renderers.Enqueue(queue.Dequeue().renderer);
            if (IsEmpty)
            {
                toningPaintable.Dissable(time,2f); // 2f cause we have two side for each pattern-object

                while (renderers.Count > 0)
                {
                    PaintWithDuration(renderers.Dequeue(), time);
                }

                return;
            }

            currentPercentage += toningPaintable.PaintedPercentage;

            toningPaintable.SetRenderTarget(Last.renderer, Last.uvArea);
        }

        private void PaintWithDuration(Renderer renderer,float duration)
        {
            float value = 0f;
            DOTween.To(() => value, x => value = x, 1f, duration)
                .OnUpdate(() =>
                {
                    renderer.material.SetFloat("_CustomAmount", value);
                });
        }
    }
}
