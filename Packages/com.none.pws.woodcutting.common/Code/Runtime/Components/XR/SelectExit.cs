using UnityEngine.XR.Interaction.Toolkit;

namespace PWS.WoodCutting
{
    public struct SelectExit
    {
        public XRBaseInteractor interactor;
        public XRBaseInteractable interactable;
    }
}