using UnityEngine.XR.Interaction.Toolkit;

namespace PWS.WoodCutting
{
    public struct SelectEnter 
    {
        public XRBaseInteractor interactor;
        public XRBaseInteractable interactable;
    }
}
