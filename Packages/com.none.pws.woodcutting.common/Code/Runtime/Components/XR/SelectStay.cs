using UnityEngine.XR.Interaction.Toolkit;

namespace PWS.WoodCutting
{
    public struct SelectStay 
    {
        public XRBaseInteractor interactor;
        public XRBaseInteractable interactable;
        public XRInteractorLineVisual interactorLineVisual;
    }
}
