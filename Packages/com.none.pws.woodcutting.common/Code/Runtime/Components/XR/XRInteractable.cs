﻿using UnityEngine.XR.Interaction.Toolkit;

namespace PWS.WoodCutting.Common
{
    public struct XRInteractable 
    {
        public XRBaseInteractable interactable;
    }
}
