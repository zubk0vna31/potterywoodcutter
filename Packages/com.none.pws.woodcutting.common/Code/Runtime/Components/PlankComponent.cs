using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace PWS.WoodCutting
{ 
    public struct PlankComponent
    {
        public int plankID;
        public int slotID;

        public Collider collider, triggerCollider;
        public Rigidbody rigidbody;
        public XRGrabInteractable grabbable;
    }
}
