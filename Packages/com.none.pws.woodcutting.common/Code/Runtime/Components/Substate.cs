using System;

namespace PWS.WoodCutting
{
    public struct Substate<T> where T : Enum
    {
        public T substate;
        public T nextSubstate;
    }

    public struct SubstateEnter
    {

    }

    public struct SubstateExit
    {

    }
}
