
using Leopotam.Ecs;
using Modules.ViewHub;
using PWS.WoodCutting.Common;
using UnityEngine;

namespace PWS.WoodCutting
{
    public struct PlankParent 
    {
        public Transform transform;
        public PlankParentView view;

        public void Deactivate()
        {
            var templates = transform.GetComponentsInChildren<EntityTemplate>();

            for (int i = 0; i < templates.Length; i++)
            {
                if (templates[i].transform == transform) continue;

                templates[i].gameObject.SetActive(false);
                //templates[i].GetComponent<EntityRef>().Entity.Destroy();
            }
        }

        
    }
}
