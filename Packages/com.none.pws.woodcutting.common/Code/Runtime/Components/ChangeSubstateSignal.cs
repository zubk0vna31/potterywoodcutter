using System;

namespace PWS.WoodCutting.Common
{
    public enum MarkupPaintingSubstates
    {
        None=0,
        Transition,
        SelectPattern,
        DrawingPattern
    }

    public struct ChangeSubstateSignal<T> where T: Enum
    {
        public T nextSubstate;
        public float delay;
    }
}
