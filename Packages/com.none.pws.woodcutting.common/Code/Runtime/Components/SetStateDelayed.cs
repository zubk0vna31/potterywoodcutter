using Modules.StateGroup.Core;

namespace PWS.WoodCutting
{
    public struct SetStateDelayed 
    {
        public SetStateSOCommand state;
        public float delay;
    }
}
