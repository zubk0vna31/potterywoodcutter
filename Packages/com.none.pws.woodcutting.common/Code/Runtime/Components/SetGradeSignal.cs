using Leopotam.Ecs;

namespace PWS.Common
{
    public struct SetGradeSignal 
    {
        public float grade;
    }
}
