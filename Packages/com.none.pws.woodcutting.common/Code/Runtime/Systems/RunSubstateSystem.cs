using Leopotam.Ecs;
using Modules.StateGroup.Core;
using System;
using System.Diagnostics;

namespace PWS.WoodCutting.Common
{
    public class RunSubstateSystem<StateT, SubstateT, ConfigT> : RunSystem<StateT, ConfigT>
        where StateT : struct
        where SubstateT : Enum
        where ConfigT : class
    {
        // auto injected fields
        public readonly EcsFilter<Substate<SubstateT>> _substate;
        public readonly EcsFilter<Substate<SubstateT>, SubstateEnter> _substateEnter;
        public readonly EcsFilter<Substate<SubstateT>, SubstateExit> _substateExit;

        public readonly SubstateT _currentSubstate;

        public RunSubstateSystem(StateConfig stateConfig,SubstateT currentSubstate):base(stateConfig)
        {
            _currentSubstate = currentSubstate;
        }

        protected override void OnStateUpdate()
        {
            base.OnStateUpdate();

            if(InSubstate())
                Substate();
        }

        protected virtual bool InSubstate()
        {
            if (_substate.IsEmpty())
            {
                return false;
            }

            foreach (var i in _substate)
            {
                if (!_substate.Get1(i).substate.Equals(_currentSubstate))
                {
                    return false;
                }
            }

            return true;
        }
        protected virtual void Substate()
        {
            if (!_substateEnter.IsEmpty())
                OnSubstateEnter();

            OnSubstateUpdate();

            if (!_substateExit.IsEmpty())
                OnSubstateExit();
        }
        protected virtual void OnSubstateEnter()
        {
        }
        protected virtual void OnSubstateExit()
        {
        }
        protected virtual void OnSubstateUpdate()
        {
            
        }

        protected void ChangeSubstate(SubstateT nextSubstate,float delay=0f)
        {
            ref var changeSubstateSignal = ref _ecsWorld.NewEntity().Get<ChangeSubstateSignal<SubstateT>>();
            changeSubstateSignal.nextSubstate = nextSubstate;
            changeSubstateSignal.delay = delay;
        }

    }
}
