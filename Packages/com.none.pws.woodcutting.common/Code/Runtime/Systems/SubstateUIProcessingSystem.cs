using Leopotam.Ecs;
using Modules.StateGroup.Components;
using PWS.Common.UI;
using System;
using UnityEngine;

namespace PWS.WoodCutting.Stages.Toning
{
    public class SubstateUIProcessingSystem<StateT, SubstateT> : IEcsRunSystem where StateT : struct where SubstateT : Enum
    {
        // auto injected fields
        private readonly EcsFilter<StateEnter, StateT> _stateEnter;
        private readonly EcsFilter<StateExit, StateT> _stateExit;
        private readonly EcsFilter<StateT> _state;

        private readonly EcsFilter<Substate<SubstateT>> _substate;
        private readonly EcsFilter<Substate<SubstateT>, SubstateEnter> _substateEnter;
        private readonly EcsFilter<Substate<SubstateT>, SubstateExit> _substateExit;

        private readonly EcsFilter<StateWindow, StateProgress> _stateWindowWithProgress;
        private readonly SubstateT _currentSubstate;

        public SubstateUIProcessingSystem(SubstateT currentSubstate)
        {
            _currentSubstate = currentSubstate;
        }

        public void Run()
        {
            if (_state.IsEmpty()) return;

            StateEnter();
            State();
            StateExit();
        }

        private void StateEnter()
        {
            if (_stateEnter.IsEmpty()) return;


        }

        private void StateExit()
        {
            if (_stateExit.IsEmpty()) return;

            foreach (var i in _stateWindowWithProgress)
            {
                _stateWindowWithProgress.Get1(i).Template.SetSliderValue(0);
                _stateWindowWithProgress.Get1(i).Template.SetSliderLabel(0);
                _stateWindowWithProgress.Get1(i).Template.ShowSlider(false);
            }
        }

        private void OnStateUpdate()
        {

        }

        private void State()
        {
            OnStateUpdate();
            if (InSubstate()) Substate();
        }

        private void Substate()
        {
            OnSubstateEnter();
            OnSubstateUpdate();
            OnSubstateExit();
        }

        private void OnSubstateEnter()
        {
            if (_substateEnter.IsEmpty()) return;

            foreach (var i in _stateWindowWithProgress)
            {
                _stateWindowWithProgress.Get1(i).Template.ShowSlider(true);
            }
        }

        private void OnSubstateExit()
        {
            if (_substateExit.IsEmpty()) return;
        }

        private void OnSubstateUpdate()
        {
            foreach (var i in _stateWindowWithProgress)
            {
                float value = _stateWindowWithProgress.Get2(i).Value;

                _stateWindowWithProgress.Get1(i).Template.SetSliderValue(value);
                _stateWindowWithProgress.Get1(i).Template.SetSliderLabel(Mathf.Floor(value * 1000) / 10);
            }
        }

        private bool InSubstate()
        {
            if (_substate.IsEmpty())
            {
                return false;
            }

            foreach (var i in _substate)
            {
                if (!_substate.Get1(i).substate.Equals(_currentSubstate))
                {
                    return false;
                }
            }

            return true;
        }
    }
}
