using Leopotam.Ecs;
using Modules.StateGroup.Core;
using UnityEngine;

namespace PWS.WoodCutting
{
    public class SetStateDelayedSystem<T> : IEcsRunSystem where T : struct
    {
        private readonly EcsFilter<T> _state;
        private readonly EcsFilter<SetStateDelayed> _setState;

        private readonly Modules.Utils.TimeService _timeService;
        private readonly StateFactory _stateFactory;

        public void Run()
        {
            if (_state.IsEmpty()) return;

            foreach (var i in _setState)
            {
                ref var setState = ref _setState.Get1(i);

                if (setState.delay >= 0)
                {
                    setState.delay -= _timeService.DeltaTime;

                    if (setState.delay <= 0)
                    {
                        setState.state.Execute(_stateFactory);
                        _setState.GetEntity(i).Del<SetStateDelayed>();
                    }
                }
            }
        }

    }
}
