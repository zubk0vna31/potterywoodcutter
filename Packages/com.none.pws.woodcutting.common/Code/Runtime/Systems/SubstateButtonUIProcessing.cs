using Leopotam.Ecs;
using Modules.StateGroup.Components;
using PWS.Common.UI;
using System;
using System.Diagnostics;

namespace PWS.WoodCutting.Common
{
    public class SubstateButtonUIProcessing<StateT, SubstateT> : IEcsRunSystem where StateT : struct where SubstateT : Enum
    {
        // auto injected fields
        private readonly EcsFilter<StateEnter, StateT> _stateEnter;
        private readonly EcsFilter<StateExit, StateT> _stateExit;
        private readonly EcsFilter<StateT> _state;

        private readonly EcsFilter<Substate<SubstateT>> _substate;
        private readonly EcsFilter<Substate<SubstateT>, SubstateEnter> _substateEnter;
        private readonly EcsFilter<Substate<SubstateT>, SubstateExit> _substateExit;

        private readonly EcsFilter<StateWindow, StateProgress>.Exclude<Ignore> _stateWindowWithProgress;
        private readonly EcsFilter<StateWindow, Ignore> _stateWindowIgnored;

        private readonly SubstateT _currentSubstate;

        public SubstateButtonUIProcessing(SubstateT currentSubstate)
        {
            _currentSubstate = currentSubstate;
        }

        public void Run()
        {
            if (_state.IsEmpty()) return;

            StateEnter();
            State();
            StateExit();
        }

        private void StateEnter()
        {
            if (_stateEnter.IsEmpty()) return;

            ShowStateWindow();
        }

        private void StateExit()
        {
            if (_stateExit.IsEmpty()) return;

            HideStateWindow();
        }

        private void OnStateUpdate()
        {

        }

        private void State()
        {
            OnStateUpdate();
            if (InSubstate()) Substate();
        }

        private void Substate()
        {
            OnSubstateEnter();
            OnSubstateUpdate();
            OnSubstateExit();
        }

        private void OnSubstateEnter()
        {
            if (_substateEnter.IsEmpty()) return;

            ShowStateWindow();
        }

        private void OnSubstateExit()
        {
            if (_substateExit.IsEmpty()) return;

            HideStateWindow();
        }

        private void OnSubstateUpdate()
        {
            foreach (var i in _stateWindowWithProgress)
            {
                ref var stateWindow = ref _stateWindowWithProgress.Get1(i);
                ref var stateProgress = ref _stateWindowWithProgress.Get2(i);

                if (stateProgress.Value >= stateProgress.CompletionThreshold)
                {
                    stateWindow.Template.ShowCompleteButton();
                }
                else if (stateProgress.Value >= stateProgress.SkipThreshold)
                {
                    stateWindow.Template.ShowSkipButton();
                }
                else
                {
                    stateWindow.Template.HideButtons();
                }
            }
        }

        private bool InSubstate()
        {
            if (_substate.IsEmpty())
            {
                return false;
            }

            foreach (var i in _substate)
            {
                if (!_substate.Get1(i).substate.Equals(_currentSubstate))
                {
                    return false;
                }
            }

            return true;
        }

        private void ShowStateWindow()
        {
            foreach (var i in _stateWindowIgnored)
            {
                _stateWindowIgnored.GetEntity(i).Del<Ignore>();
            }

            foreach (var i in _stateWindowWithProgress)
            {
                _stateWindowWithProgress.Get1(i).Template.ShowSlider(true);
            }
        }

        private void HideStateWindow()
        {
            foreach (var i in _stateWindowWithProgress)
            {
                _stateWindowWithProgress.Get1(i).Template.HideButtons();
                _stateWindowWithProgress.GetEntity(i).Get<Ignore>();
            }
        }
    }
}
