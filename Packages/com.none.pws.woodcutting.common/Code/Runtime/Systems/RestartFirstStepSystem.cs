using Leopotam.Ecs;
using PWS.Common.Messages;
using PWS.Common.SceneSwitchService;
using PWS.HUB.Simulation;
using UnityEngine.SceneManagement;

namespace PWS.WoodCutting.Common
{
    public class RestartFirstStepSystem : IEcsRunSystem
    {
        private readonly EcsFilter<RestartFirstStepMessage> _message;

        private readonly ISceneSwitchService _sceneSwitchService;

        public RestartFirstStepSystem(ISceneSwitchService sceneSwitchService)
        {
            _sceneSwitchService = sceneSwitchService;
        }

        public void Run()
        {
            if (_message.IsEmpty()) return;

            _sceneSwitchService.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }
}
