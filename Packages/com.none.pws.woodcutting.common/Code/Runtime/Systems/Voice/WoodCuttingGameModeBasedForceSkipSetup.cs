using Leopotam.Ecs;
using Modules.StateGroup.Components;
using PWS.Common.GameModeInfoService;
using PWS.Common.UI;
using UnityEngine;

namespace PWS.Pottery.Common
{
    public class WoodCuttingGameModeBasedForceSkipSetup : IEcsRunSystem
    {
        readonly EcsFilter<StateEnter> _entering;
        readonly EcsFilter<StateWindow, StateProgress> _progress;

        private GameMode _gameMode;

        public WoodCuttingGameModeBasedForceSkipSetup(GameMode gameMode)
        {
            _gameMode = gameMode;
        }

        public void Run()
        {
            if (_entering.IsEmpty())
                return;

            if (_gameMode != GameMode.Free)
                return;

            foreach (var progress in _progress)
            {
                _progress.Get2(progress).SkipThreshold = -1;
            }
        }
    }
}
