using Leopotam.Ecs;
using Modules.Root.ContainerComponentModel;
using Modules.Root.ECS;
using PWS.Common.GameModeInfoService;
using PWS.Pottery.Common;
using UnityEngine;

namespace PWS.WoodCutting.Common
{
    public class WoodCuttingVoiceChangeSystemsProvider : MonoBehaviour,ISystemsProvider
    {
        internal class SystemsDependencies
        {
            [Inject] public IGameModeInfoService GameModeData;
        }
        public EcsSystems GetSystems(EcsWorld world, EcsSystems endFrame, EcsSystems mainSystems)
        {
            SystemsDependencies dependencies = new SystemsDependencies();
            SceneContainer.Instance.Inject(dependencies);
            EcsSystems systems = new EcsSystems(world);
            systems
                .Add(new WoodCuttingGameModeBasedForceSkipSetup(dependencies.GameModeData.CurrentMode))
                .Add(new WoodCuttingGameModeBasedVoiceVolumeSetup(dependencies.GameModeData.CurrentMode))
            ;
            return systems;
        }
    }
}
