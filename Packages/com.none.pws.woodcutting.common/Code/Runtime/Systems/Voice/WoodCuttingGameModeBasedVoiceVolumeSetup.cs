using Leopotam.Ecs;
using Modules.Audio;
using PWS.Common.GameModeInfoService;
using UnityEngine;

namespace PWS.WoodCutting.Common
{
    public class WoodCuttingGameModeBasedVoiceVolumeSetup : IEcsInitSystem
    {
        private readonly EcsFilter<AudioSourceRef,WoodCuttingGrandDadTag> _grandDad;

        private GameMode _gameMode;

        public WoodCuttingGameModeBasedVoiceVolumeSetup(GameMode gameMode)
        {
            _gameMode = gameMode;
        }

        public void Init()
        {
            if (_gameMode == GameMode.Tutorial)
                return;

            foreach (var grandDad in _grandDad)
            {
                _grandDad.Get1(grandDad).AudioSource.enabled = false;
            }
        }
    }
}
