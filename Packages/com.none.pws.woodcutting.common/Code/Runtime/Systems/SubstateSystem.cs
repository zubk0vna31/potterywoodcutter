using Leopotam.Ecs;
using Modules.Utils;
using System;
using UnityEngine;

namespace PWS.WoodCutting.Common
{
    public class SubstateSystem<StateT, SubstateT, ConfigT> : RunSystem<StateT, ConfigT>
        where StateT : struct where SubstateT : Enum where ConfigT : class
    {
        // auto-injected fields
        public readonly EcsFilter<ChangeSubstateSignal<SubstateT>> _changeSubstateSignal;

        public readonly SubstateT _initialSubstate;
        public readonly SubstateT _transitionSubstate;
        public readonly SubstateT _noneSubstate;

        public readonly TimeService _timeService;

        // runtime data
        private EcsEntity _substateEntity;
        private float _timer;
        private bool _transitionSubstateEntered;
        private bool _transitionSubstateExited;

        public SubstateSystem(StateConfig stateConfig,
            SubstateT initialSubstate,
            SubstateT transitionSubstate,
            SubstateT noneSubstate) : base(stateConfig)
        {
            _initialSubstate = initialSubstate;
            _transitionSubstate = transitionSubstate;
            _noneSubstate = noneSubstate;
        }

        protected override void OnStateEnter()
        {
            base.OnStateEnter();

            _timer = 0.0f;


            _substateEntity = _ecsWorld.NewEntity();
            _substateEntity.Get<Substate<SubstateT>>().substate = _initialSubstate;
            _substateEntity.Get<Substate<SubstateT>>().nextSubstate = _noneSubstate;
            _substateEntity.Get<SubstateEnter>();
        }

        protected override void OnStateUpdate()
        {
            base.OnStateUpdate();

            if (_timer <= 0.0f)
            {
                ChangeSubstate();
            }
            else
            {
                if (!_transitionSubstateEntered)
                {
                    _transitionSubstateEntered = true;
                    _substateEntity.Get<Substate<SubstateT>>().substate = _transitionSubstate;
                    _substateEntity.Get<SubstateEnter>();
                }

                _timer -= _timeService.DeltaTime;

                if (_timer <= 0.0f && _transitionSubstateExited)
                {
                    ChangeSubstate();
                }
                else
                {
                    _transitionSubstateExited = true;
                    _substateEntity.Get<SubstateExit>();
                }
            }

            if (_changeSubstateSignal.IsEmpty()) return;


            foreach (var i in _changeSubstateSignal)
            {
                _timer = _changeSubstateSignal.Get1(i).delay;

                _substateEntity.Get<SubstateExit>();
                _substateEntity.Get<Substate<SubstateT>>().nextSubstate =
                    _changeSubstateSignal.Get1(i).nextSubstate;
                _changeSubstateSignal.GetEntity(i).Del<ChangeSubstateSignal<SubstateT>>();
            }
        }

        protected override void OnStateExit()
        {
            base.OnStateExit();

            if (!_substateEntity.IsAlive()) return;
            _substateEntity.Destroy();
        }

        private void ChangeSubstate()
        {
            if (IsExiting() && !_substateEntity.IsAlive()) return;

            ref var substate = ref _substateEntity.Get<Substate<SubstateT>>();

            if (!substate.nextSubstate.Equals(_noneSubstate))
            {
                substate.substate = substate.nextSubstate;
                _substateEntity.Get<SubstateEnter>();
                substate.nextSubstate = _noneSubstate;
                _transitionSubstateEntered = false;
                _transitionSubstateExited = false;
            }
        }

    }
}
