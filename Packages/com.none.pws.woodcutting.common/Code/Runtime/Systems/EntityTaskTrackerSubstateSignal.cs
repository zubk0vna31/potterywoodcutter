using Leopotam.Ecs;
using Modules.StateGroup.Components;
using System;
using UnityEngine;

namespace PWS.WoodCutting.Common
{
    public class EntityTaskTrackerSubstateSignal<StateT,ComponentT,SubstateT> : IEcsRunSystem 
        where StateT: struct 
        where ComponentT: struct
        where SubstateT:Enum
    {
        // auto injected fields
        private readonly EcsFilter<StateT> _state;
        private readonly EcsFilter<StateEnter,StateT> _stateEnter;
        private readonly EcsFilter<ComponentT> _filter;

        private readonly EcsWorld _ecsWorld;

        private readonly SubstateT _nextSubstate;
        private readonly int _constEntityAmount;

        // run-time data
        private bool _enabled;

        public EntityTaskTrackerSubstateSignal(SubstateT nextSubstate,int constEntityAmount=-1)
        {
            _enabled = true;
            _nextSubstate = nextSubstate;
            _constEntityAmount = constEntityAmount;
        }

        public void Run()
        {
            if (_state.IsEmpty()) return;

            if (!_stateEnter.IsEmpty())
            {
                _enabled = true;
            }

            if (!_enabled || _filter.IsEmpty()) return;

            int completedAmount = 0;
            int correctAmount = 0;

            foreach (var i in _filter)
            {
                if(_filter.Get1(i) is ITask)
                {
                    correctAmount++;
                    if ((_filter.Get1(i) as ITask).Completed()) completedAmount++;
                }
            }

            if (_constEntityAmount < 0)
            {
                if (completedAmount == correctAmount)
                {
                    _enabled = false;
                    _ecsWorld.NewEntity().Get<ChangeSubstateSignal<SubstateT>>().nextSubstate = _nextSubstate;
                }
            }
            else
            {
                if (completedAmount == _constEntityAmount)
                {
                    _enabled = false;
                    _ecsWorld.NewEntity().Get<ChangeSubstateSignal<SubstateT>>().nextSubstate = _nextSubstate;
                }
            }
        }
    }
}
