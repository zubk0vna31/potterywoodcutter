using Leopotam.Ecs;
using Modules.Root.ContainerComponentModel;
using Modules.Root.ECS;
using Modules.SGMBLinker;
using Modules.StateGroup.Core;
using Modules.StateGroup.Systems;
using PWS.Common;
using PWS.Common.Messages;
using PWS.Common.ResultsSubmission;
using PWS.Common.ResultsSubmission.ResultCapture;
using PWS.Common.SceneSwitchService;
using PWS.Common.UI;
using PWS.HUB.Simulation;
using UnityEngine;

namespace PWS.WoodCutting.Common
{
    public class WoodCuttingCommonSystemsProvider : MonoBehaviour, ISystemsProvider
    {
        internal class SystemsDependencies
        {
            [Inject] public IResultsSubmissionService ResultsSubmissionService;
            [Inject] public ISceneSwitchService sceneSwitchService;
        }
        
        [SerializeField] private SetStateSOCommand _initState;

        public EcsSystems GetSystems(EcsWorld world, EcsSystems endFrame, EcsSystems mainSystems)
        {
            SystemsDependencies dependencies = new SystemsDependencies();
            SceneContainer.Instance.Inject(dependencies);
            
            EcsSystems systems = new EcsSystems(world, "WoodCutting Common");
            systems

                // Systems
                .Add(new RXMessagePropagator<RestartCurrentStepMessage>())
                .Add(new RXMessagePropagator<RestartFirstStepMessage>())
                .Add(new InitStateSetupSystem(_initState))
                
                .Add(new ResultsSubmissionSystem(dependencies.ResultsSubmissionService, "WoodCutting"))
                .Add(new RestartFirstStepSystem(dependencies.sceneSwitchService))

                .OneFrame<SubmitResultsSignal>()
                ;

            endFrame

                .OneFrame<SetGradeSignal>()
                .OneFrame<NextStateSignal>()
                .OneFrame<CompleteStateSignal>()
                .OneFrame<SelectEnter>()
                .OneFrame<SelectExit>()
                .OneFrame<SkipStageMessage>()

            ;


            return systems;
        }
    }
}