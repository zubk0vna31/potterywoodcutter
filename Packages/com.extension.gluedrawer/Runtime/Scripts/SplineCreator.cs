﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class SplineCreator : MonoBehaviour
{
    [SerializeField, Range(0.001f, 10f)]
    private float distanceThreshold = 1f;

    [SerializeField, Range(0.001f, 0.75f)]
    private float autoControlLength = 0.5f;

    [SerializeField, Range(1, 8)]
    private int lineResolution = 2;

    [SerializeField]
    private LayerMask drawLayer;

    [Header("Gizmo")]
    [SerializeField]
    private bool showGizmo;
    [SerializeField]
    private Color pointColor;
    [SerializeField]
    private Color controlPointColor;
    [SerializeField]
    private Color lineColor;
    [SerializeField, Range(0.001f, 10f)]
    private float gizmoSize;

    private Camera mainCamera;

    private List<Vector3> points;
    private Vector3 previousPoint;

    private bool pressed;

    public Action<Vector3[], float> OnPathModified;

    //void Start()
    //{
    //    points = new List<Vector3>();

    //    mainCamera = Camera.main;

    //    previousPoint = Vector3.positiveInfinity;
    //}

    //void Update()
    //{
    //    if (Input.GetKey(KeyCode.Mouse0))
    //    {
    //        if (Physics.Raycast(mainCamera.ScreenPointToRay(Input.mousePosition), out var hit, 1000f, paintLayer, QueryTriggerInteraction.Ignore))
    //        {
    //            if (!pressed)
    //            {
    //                pressed = true;
    //            }

    //            if ((hit.point - previousPoint).magnitude < distanceThreshold) return;

    //            AddPoint(hit.point);
    //            previousPoint = hit.point;
    //        }
    //    }
    //    else
    //    {
    //        if (pressed) pressed = false;
    //    }
    //}

    public void Initialize()
    {
        if (points != null) return;

        points = new List<Vector3>();

        mainCamera = Camera.main;

        previousPoint = Vector3.positiveInfinity;
    }

    public void Initialize(List<Vector3> points)
    {
        this.points = new List<Vector3>();

        mainCamera = Camera.main;

        previousPoint = Vector3.positiveInfinity;

        int i = 0;
        foreach (var p in points)
        {
            AddPoint(p,i==points.Count-1);
            previousPoint = p;
            i++;
        }
    }

    public void Draw(Vector3 point)
    {
        if ((point - previousPoint).magnitude < distanceThreshold) return;

        AddPoint(point);
        previousPoint = point;
    }

    private void AddPoint(Vector3 point)
    {
        if (points.Count == 0)
        {
            //Add anchor point
            points.Add(point);
        }
        else
        {
            //Add controll points
            Vector3 dir = (points[points.Count - 1] - point).normalized;
            float distance = (points[points.Count - 1] - point).magnitude;

            points.Add(points[points.Count - 1] - dir * (distance * 0.5f * autoControlLength));
            points.Add(point + dir * (distance * 0.5f * autoControlLength));

            //Add anchor point
            points.Add(point);
        }

        AutoSetControllPoints();

        if (NumOfAnchorPoints() >= 1)
        {
            OnPathModified?.Invoke(GetSampledPoints(out var pathDistance), pathDistance);
        }
    }

    private void AddPoint(Vector3 point, bool notifyListener = false)
    {
        if (points.Count == 0)
        {
            //Add anchor point
            points.Add(point);
        }
        else
        {
            //Add controll points
            Vector3 dir = (points[points.Count - 1] - point).normalized;
            float distance = (points[points.Count - 1] - point).magnitude;

            points.Add(points[points.Count - 1] - dir * (distance * 0.5f * autoControlLength));
            points.Add(point + dir * (distance * 0.5f * autoControlLength));

            //Add anchor point
            points.Add(point);
        }

        AutoSetControllPoints();

        if (NumOfAnchorPoints() >= 1 && notifyListener)
        {
            OnPathModified?.Invoke(GetSampledPoints(out var pathDistance), pathDistance);
        }
    }




    private void AutoSetControllPoints()
    {
        if (NumOfAnchorPoints() > 1)
        {
            for (int i = 0; i < points.Count; i += 3)
            {
                if (i == 0 || i == points.Count - 1) continue;


                Vector3 anchorPos = points[i];
                Vector3 dir = Vector3.zero;

                float[] neighbourDistances = new float[2];

                if (i - 3 >= 0)
                {
                    Vector3 offset = points[i - 3] - anchorPos;
                    dir += offset.normalized;
                    neighbourDistances[0] = offset.magnitude;
                }
                if (i + 3 < points.Count)
                {
                    Vector3 offset = points[i + 3] - anchorPos;
                    dir -= offset.normalized;
                    neighbourDistances[1] = -offset.magnitude;
                }

                dir.Normalize();

                // Set the control points along the calculated direction, with a distance proportional to the distance to the neighbouring control point
                for (int j = 0; j < 2; j++)
                {
                    int controlIndex = i + j * 2 - 1;
                    if (controlIndex >= 0 && controlIndex < points.Count)
                    {
                        points[controlIndex] = anchorPos + dir * neighbourDistances[j] * autoControlLength;
                    }
                }
            }
        }

        if (NumOfAnchorPoints() == 1)
        {
            points[1] = points[0] + (points[3] - points[0]) * .25f;
            points[2] = points[3] + (points[0] - points[3]) * .25f;
        }
        else if (NumOfAnchorPoints() > 1)
        {
            points[1] = (points[0] + points[2]) * .5f;
            points[points.Count - 2] = (points[points.Count - 1] + points[points.Count - 3]) * .5f;
        }
    }

    private int NumOfAnchorPoints()
    {
        return points.Count / 3;
    }

    private Vector3[] GetSampledPoints(out float pathDistance)
    {
        List<Vector3> sampledPoints = new List<Vector3>();

        Vector3 previousPoint = Vector3.zero;
        Vector3 previousSample = Vector3.zero;
        pathDistance = 0f;

        for (int i = 0; i < points.Count; i += 3)
        {
            Vector3 current = points[i];

            if (i == 0)
            {
                sampledPoints.Add(current);
            }
            else if (i > 0)
            {
                if (lineResolution > 1)
                {
                    for (int j = 0; j < lineResolution; j++)
                    {
                        float t = (j * 1.0f + 1.0f) / (lineResolution + 1);

                        Vector3 newSample = Beizer(previousPoint, points[i - 2], points[i - 1], current, t);
                        sampledPoints.Add(newSample);

                        pathDistance += Vector3.Distance(newSample, previousSample);

                        previousSample = newSample;
                    }
                }

                sampledPoints.Add(current);

                pathDistance += Vector3.Distance(current, previousSample);
            }

            previousPoint = previousSample = current;
        }

        return sampledPoints.ToArray();
    }

    private Vector3 Beizer(Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3, float t)
    {
        t = Mathf.Clamp01(t);
        return (1 - t) * (1 - t) * (1 - t) * p0 + 3 * (1 - t) * (1 - t) * t * p1 + 3 * (1 - t) * t * t * p2 + t * t * t * p3;
    }
    private void OnDrawGizmosSelected()
    {
        if (!showGizmo || points == null || points.Count == 0) return;

        Vector3 previousPoint = Vector3.zero;
        int i = 0;
        foreach (var p in points)
        {
            bool anchor = i % 3 == 0;

            Gizmos.color = anchor ? pointColor : controlPointColor;

            Gizmos.DrawWireSphere(p, anchor ? gizmoSize : gizmoSize * 0.5f);

            if (i > 0 && anchor)
            {
                Gizmos.color = lineColor;

                if (lineResolution == 1)
                {
                    Gizmos.DrawLine(previousPoint, p);
                }
                else
                {
                    Vector3 previousBeizerSample = previousPoint;

                    for (int j = 0; j <= lineResolution; j++)
                    {
                        float t = (j * 1.0f + 1.0f) / (lineResolution + 1);

                        Vector3 newBeizerSample = Beizer(previousPoint, points[i - 2], points[i - 1], p, t);

                        Gizmos.DrawLine(previousBeizerSample, newBeizerSample);

                        previousBeizerSample = newBeizerSample;
                    }
                }
            }

            if (anchor)
                previousPoint = p;
            i++;
        }
    }

}
