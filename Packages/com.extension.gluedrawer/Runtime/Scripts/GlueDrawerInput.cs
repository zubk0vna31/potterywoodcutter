using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlueDrawerInput : MonoBehaviour
{
    public GameObject gluePrefab;
    public LayerMask mask;
    public GlueDrawerSystem drawSystem;

    void Start()
    {
        drawSystem.Initialize(gluePrefab, mask);
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.Mouse0))
        {
            drawSystem.Draw(Camera.main.ScreenPointToRay(Input.mousePosition), 1000f, out var hit,transform);
        }
        else
        {
            drawSystem.InputLost();
        }
    }
}
