﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class GlueRatingSystem : MonoBehaviour
{
    [SerializeField]
    private Transform start, end;

    [SerializeField]
    private GameObject rateBlockCorrect,rateBlockWrong;

    [SerializeField,Range(0f,0.25f)]
    private float rateBlockPadding = 0.1f;

    [SerializeField, Range(1, 100)]
    private int resolution = 32;

    [SerializeField,Range(0.001f,10000f)]
    private float rayLength = 1000f;

    [SerializeField, Range(-5f, 5f)]
    private float rayHeightOffset = 0f;

    [SerializeField, Range(0.001f, 5f)]
    private float boxCastSize = 0.01f;

    [SerializeField]
    private LayerMask glueMask;

    private float length;
    private IEnumerator waiter;

    private List<GameObject> rateBlocks;
    private GameObject rateBlockParent;

    private bool completed;
    private float percentage;

    public bool Completed => completed;
    public float Percentage => percentage;

    private float successPercentage, rateDuration;

    public void SetConfigData(float successPercentage, float rateDuration)
    {
        this.successPercentage = successPercentage;
        this.rateDuration = rateDuration;
    }

    private void Start()
    {
        length = (start.position - end.position).magnitude;

        rateBlocks = new List<GameObject>();

        rateBlockParent = new GameObject("Rate Blocks");
        rateBlockParent.transform.SetParent(transform);
        rateBlockParent.transform.localPosition = Vector3.zero;
    }

    public bool Rate(float duration=-1f)
    {
        if (duration > 0)
        {
            if (rateBlocks == null) return false;

            if (rateBlocks.Count > 0)
            {
                if (waiter != null)
                {
                    StopCoroutine(waiter);
                    Clear();
                }
            }
        }

        float step = length / resolution;

        int successCount = 0;

        Vector3 rayOrigin = Vector3.zero;
        Vector3 rayDirection = start.forward;
        Vector3 upVector = start.up;
        Vector3 stepDirection = (end.position-start.position).normalized;
        float extraRayLength = rayLength;

        for (int i = 0; i < resolution; i++)
        {
            rayOrigin = start.position + stepDirection * (step*(i+0.5f)) + upVector* rayHeightOffset;

            Vector3 rateBlockOrigin = rayOrigin;

            rayOrigin -= rayDirection * extraRayLength;

            bool correct = false;

            if(Physics.BoxCast(rayOrigin,new Vector3(step, rayLength*0.5f, step)*0.5f, rayDirection,
                Quaternion.LookRotation(rayDirection, upVector),rayLength+ extraRayLength*2f,glueMask,QueryTriggerInteraction.Ignore))
            {
                correct = true;
                successCount++;
            }

            if (duration <= 0) continue;

            GameObject rateBlock = Instantiate(correct ? rateBlockCorrect : rateBlockWrong, rateBlockParent.transform);
            rateBlock.name = $"Rate Block {i} - {correct}";

            rateBlock.transform.rotation = Quaternion.LookRotation(rayDirection, upVector);

            rateBlock.transform.localScale = new Vector3(step, boxCastSize * 2, rayLength) - new Vector3(rateBlockPadding, 0f, rateBlockPadding);

            rateBlock.transform.position = rateBlockOrigin + rateBlock.transform.forward * rateBlockPadding * 0.5f;

            rateBlocks.Add(rateBlock);
        }

        if (duration > 0)
        {
            waiter = WaitThenPerform(rateDuration, () =>
             {
                 Clear();
                 waiter = null;
             });
            StartCoroutine(waiter);
        }

        percentage = (successCount * 1.0f / resolution);
        completed = percentage >= successPercentage;

        return completed;
    }

    private void Clear()
    {
        for (int i = 0; i < rateBlocks.Count; i++)
        {
            Destroy(rateBlocks[i]);
        }

        rateBlocks.Clear();
    }

    public void ResetComponent()
    {
        StopAllCoroutines();
        Clear();
        percentage = 0f;
    }

    private IEnumerator WaitThenPerform(float duration,Action callback)
    {
        yield return new WaitForSeconds(duration);

        callback?.Invoke();
    }
}
