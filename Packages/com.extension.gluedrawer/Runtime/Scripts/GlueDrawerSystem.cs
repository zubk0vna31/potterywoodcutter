﻿using System.Collections.Generic;
using UnityEngine;

public class GlueDrawerSystem : MonoBehaviour
{
    private GameObject gluePrefab;

    private LayerMask drawLayer;

    private Dictionary<SplineCreator, GlueMeshRenderer> splineRenderer;

    private SplineCreator current;

    private bool pressed;

    public void Initialize(GameObject gluePrefab,LayerMask drawLayer)
    {
        splineRenderer = new Dictionary<SplineCreator, GlueMeshRenderer>();

        this.gluePrefab = gluePrefab;
        this.drawLayer = drawLayer;
    }

    public void ToggleCollider(bool value)
    {
        if (splineRenderer != null)
        {
            foreach (var key in splineRenderer.Keys)
            {
                splineRenderer[key].ToggleCollider(value);
            }
        }
    }
    public void Clear()
    {
        splineRenderer = new Dictionary<SplineCreator, GlueMeshRenderer>();
        current = null;
        pressed = false;
    }

    public bool Draw(Ray ray, float distance,out RaycastHit hit,Transform parent)
    {
        if (Physics.Raycast(ray, out hit, distance,
            drawLayer, QueryTriggerInteraction.Ignore))
        {
            if (!pressed)
            {
                pressed = true;

                var spline = Instantiate(gluePrefab, parent).GetComponent<SplineCreator>();
                var renderer = spline.gameObject.GetComponent<GlueMeshRenderer>();

                spline.name = $"Glue {splineRenderer.Count}";
                spline.Initialize();

                splineRenderer.Add(spline, renderer);
                current = spline;
            }

            current.Draw(hit.point);

            return true;
        }
        else
        {
            InputLost();

            return false;
        }
    }

    public void InputLost()
    {
        if (pressed)
        {
            splineRenderer[current].ShouldUpdateCollider = false;

            pressed = false;
            current = null;
        }
    }

}
