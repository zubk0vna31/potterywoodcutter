﻿using UnityEngine;
using System;
using DG.Tweening;
using System.Collections;

[RequireComponent(typeof(SplineCreator))]
public class GlueMeshRenderer : MonoBehaviour
{
    [SerializeField, Range(0.001f, 16f)]
    private float baseRadius = 12;

    [SerializeField, Range(3, 32)]
    private int baseResolution = 12;

    [SerializeField]
    private AnimationCurve radiusCurve = AnimationCurve.Constant(0, 1f, 1f);

    [SerializeField, Range(0f, 10f)]
    private float curveAffectDistance = 1f;

    [SerializeField, Range(0.1f, 5f)]
    private float colliderUpdateInterval = 0.5f;

    [SerializeField]
    private Material glueMaterial;

    [SerializeField]
    private bool offsetByDeltaAngle;
    [HideInInspector]
    public bool useLocalSpace = false;

    private MeshFilter meshFilter;
    private MeshRenderer meshRenderer;
    private MeshCollider meshCollider;

    private Vector3[] vertices;
    private int[] triangles;

    private Vector3[] worldPoints;
    private bool[] worldPointsPushed;
    private float pathDistance;

    private bool fullyPushed;
    private float pushAmount;
    private Vector3 pushDirection;

    private float timer;

    private bool shouldUpdateCollider = true;
    private float deltaAngle;

    public bool ShouldUpdateCollider
    {
        get => shouldUpdateCollider;
        set
        {
            //Last update before stop do it
            UpdateCollider();

            shouldUpdateCollider = value;
        }
    }

    public int PointsAmount => worldPoints.Length;
    public bool FullyPushed => fullyPushed;
    public float PushAmount => pushAmount;

    public float OffsetByDeltaAngle => deltaAngle;

    public float BaseRadius => baseRadius;
    public Vector3 PushDirection
    {
        set
        {
            pushDirection = transform.worldToLocalMatrix * value.normalized;
        }
    }

    public Action<int, Vector3, float> OnGlueMeshModified;

    private void Update()
    {
        if (!ShouldUpdateCollider) return;

        if (timer > 0)
        {
            timer -= Time.deltaTime;

            if (timer <= 0)
            {
                timer = colliderUpdateInterval;

                UpdateCollider();
            }
        }
    }

    private void GenerateMesh(Vector3[] points, float pathDistance)
    {
        vertices = new Vector3[points.Length * baseResolution];
        triangles = new int[(points.Length - 1) * baseResolution * 6];

        worldPoints = points;
        worldPointsPushed = new bool[points.Length];
        this.pathDistance = pathDistance;

        float deltaAngle = (360f / baseResolution) * Mathf.Deg2Rad;
        if (offsetByDeltaAngle)
            this.deltaAngle = deltaAngle * 0.5f;
        float currentDistance = 0f;

        for (int i = 0, t = 0; i < points.Length; i++)
        {
            Vector3 forward = i == points.Length - 1 ? (points[i] - points[i - 1]).normalized
                : (points[i + 1] - points[i]).normalized;
            Vector3 right = Vector3.Cross(Vector3.up, forward);
            Vector3 normal = Vector3.Cross(forward, right);

            float time = 1f;

            if (i > 0)
                currentDistance += Vector3.Distance(points[i], points[i - 1]);

            float affectDistance = Mathf.Lerp(0f, curveAffectDistance, Mathf.InverseLerp(2, 10, points.Length));

            if (pathDistance < affectDistance * 2f)
            {
                affectDistance = pathDistance * 0.25f;
            }

            if (currentDistance < affectDistance || (currentDistance > (pathDistance - affectDistance)))
            {
                if (currentDistance < affectDistance)
                {
                    time = currentDistance / affectDistance;
                }
                else if (currentDistance > (pathDistance - affectDistance))
                {
                    time = (pathDistance - currentDistance) / affectDistance;
                }

                time = Mathf.Clamp01(time);
            }

            for (int j = 0; j < baseResolution; j++, t += 6)
            {
                float radius = baseRadius * radiusCurve.Evaluate(time);
                float angle = deltaAngle * (j);

                if (offsetByDeltaAngle)
                {
                    angle += deltaAngle * 0.5f;
                }

                float x = Mathf.Cos(angle) * radius;
                float y = Mathf.Sin(angle) * radius;

                //Vector3 position = points[i] + normal * y + right * x;

                //vertices[i * baseResolution + j] = position;
                if (!useLocalSpace)
                {
                    Vector3 position = new Vector3(points[i].x, points[i].y, points[i].z) + normal * y + right * x;
                    vertices[i * baseResolution + j] = transform.worldToLocalMatrix * new Vector4(position.x, position.y, position.z, 1f);
                }
                else
                {
                    Vector3 position = new Vector3(points[i].x, points[i].y, points[i].z) + normal * y + right * x;
                    vertices[i * baseResolution + j] = position;
                }

                if (i < points.Length - 1)
                {
                    int index = baseResolution * i;

                    triangles[t] = index + j;
                    triangles[t + 1] = index + (j + 1) % baseResolution + baseResolution;
                    triangles[t + 2] = index + j + baseResolution;

                    triangles[t + 3] = index + (j + 1) % baseResolution;
                    triangles[t + 4] = index + (j + 1) % baseResolution + baseResolution;
                    triangles[t + 5] = index + j;
                }
            }
        }

        bool initialize = false;

        if (meshFilter == null)
        {
            meshFilter = gameObject.AddComponent<MeshFilter>();

            meshFilter.mesh = new Mesh();

            timer = colliderUpdateInterval;

            initialize = true;
        }

        if (meshRenderer == null)
        {
            meshRenderer = gameObject.AddComponent<MeshRenderer>();
            meshRenderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;

            meshRenderer.material = glueMaterial;
        }

        if (meshCollider == null)
        {
            meshCollider = gameObject.AddComponent<MeshCollider>();
        }

        meshFilter.mesh.SetVertices(vertices);
        meshFilter.mesh.SetTriangles(triangles, 0);
        meshFilter.mesh.RecalculateNormals();

        if (initialize)
        {
            UpdateCollider();
        }
    }

    private void ModifyMesh(float pathDistance,int from=-1,int to=-1,bool updatePushAmount=true)
    {
        this.pathDistance = pathDistance;

        float deltaAngle = (360f / baseResolution) * Mathf.Deg2Rad;
        if (offsetByDeltaAngle)
            this.deltaAngle = deltaAngle * 0.5f;

        float currentDistance = 0f;

        int pushedAmount = 0;

        from = from < 0 ? 0 : from;
        to = to < 0 ? worldPoints.Length : to;

        for (int i = from, t = 0; i < to; i++)
        {
            if (worldPointsPushed[i])
            {
                pushedAmount++;
            }

            Vector3 forward = i == worldPoints.Length - 1 ? (worldPoints[i] - worldPoints[i - 1]).normalized
                : (worldPoints[i + 1] - worldPoints[i]).normalized;
            Vector3 right = Vector3.Cross(Vector3.up, forward);
            Vector3 normal = Vector3.Cross(forward, right);

            float time = 1f;

            if (i > 0)
                currentDistance += Vector3.Distance(worldPoints[i], worldPoints[i - 1]);

            float affectDistance = Mathf.Lerp(0f, curveAffectDistance, Mathf.InverseLerp(2, 10, worldPoints.Length));

            if (pathDistance < affectDistance * 2f)
            {
                affectDistance = pathDistance * 0.25f;
            }

            if (currentDistance < affectDistance || (currentDistance > (pathDistance - affectDistance)))
            {
                if (currentDistance < affectDistance)
                {
                    time = currentDistance / affectDistance;
                }
                else if (currentDistance > (pathDistance - affectDistance))
                {
                    time = (pathDistance - currentDistance) / affectDistance;
                }

                time = Mathf.Clamp01(time);
            }

            for (int j = 0; j < baseResolution; j++, t += 6)
            {
                float radius = baseRadius * radiusCurve.Evaluate(time);
                float angle = deltaAngle * (j);

                if (offsetByDeltaAngle)
                {
                    angle += deltaAngle * 0.5f;
                }

                float x = Mathf.Cos(angle) * radius;
                float y = Mathf.Sin(angle) * radius;



                //Vector3 position = worldPoints[i] + normal * y + right * x;
                //Vector3 position = new Vector3(worldPoints[i].x, worldPoints[i].y, worldPoints[i].z) + normal * y + right * x;

                //vertices[i * baseResolution + j] = transform.worldToLocalMatrix * new Vector4(position.x, position.y, position.z, 1f);
                //vertices[i * baseResolution + j] = position;

                //vertices[i * baseResolution + j] = position;
                if (!useLocalSpace)
                {
                    Vector3 position = new Vector3(worldPoints[i].x, worldPoints[i].y, worldPoints[i].z) + normal * y + right * x;
                    vertices[i * baseResolution + j] = transform.worldToLocalMatrix * new Vector4(position.x, position.y, position.z, 1f);
                }
                else
                {
                    Vector3 position = new Vector3(worldPoints[i].x, worldPoints[i].y, worldPoints[i].z) + normal * y + right * x;
                    vertices[i * baseResolution + j] = position;
                }


                if (i < worldPoints.Length - 1)
                {
                    int index = baseResolution * i;

                    triangles[t] = index + j;
                    triangles[t + 1] = index + (j + 1) % baseResolution + baseResolution;
                    triangles[t + 2] = index + j + baseResolution;

                    triangles[t + 3] = index + (j + 1) % baseResolution;
                    triangles[t + 4] = index + (j + 1) % baseResolution + baseResolution;
                    triangles[t + 5] = index + j;
                }
            }
        }

        fullyPushed = worldPointsPushed.Length == pushedAmount;

        if(updatePushAmount)
            pushAmount = pushedAmount * 1.0f / worldPointsPushed.Length;

        meshFilter.mesh.SetVertices(vertices);
        meshFilter.mesh.SetTriangles(triangles, 0);
        meshFilter.mesh.RecalculateNormals();

        UpdateCollider();
    }

    private void UpdateCollider()
    {
        if (meshCollider == null) return;

        meshCollider.sharedMesh = null;
        meshCollider.sharedMesh = meshFilter.mesh;
    }

    public void ToggleCollider(bool value)
    {
        if (meshCollider == null) return;

        meshCollider.enabled = value;

        if (value)
        {
            UpdateCollider();
        }
    }

    public void SetUpVector(Vector3 up)
    {
        pushDirection = up;
    }

    public void Push(Transform pusher, float strength = 1f)
    {
        BoxCollider collider = pusher.GetComponent<BoxCollider>();

        bool shouldUpdateMesh = false;

        float currentDistance = 0f;


        for (int i = 0; i < worldPoints.Length; i++)
        {
            Vector3 worldPoint = transform.TransformPoint(worldPoints[i]);

            Vector3 closestPoint = collider.ClosestPoint(worldPoint);


            if (!worldPointsPushed[i] && (closestPoint - worldPoint).magnitude <= baseRadius)
            {
                worldPoints[i] -= pushDirection * strength;
                worldPointsPushed[i] = true;

                OnGlueMeshModified?.Invoke(i, pushDirection, strength);

                shouldUpdateMesh = true;
            }

            if (i > 0)
                currentDistance += Vector3.Distance(worldPoints[i], worldPoints[i - 1]);

        }

        pathDistance = currentDistance;

        if (shouldUpdateMesh)
        {
            ModifyMesh(pathDistance);
        }
    }

    public void Push(int index, Vector3 direction, float strength)
    {
        if (worldPointsPushed[index]) return;

        bool shouldUpdateMesh = false;

        float currentDistance = 0f;

        for (int i = 0; i < worldPoints.Length; i++)
        {
            if (i == index)
            {
                worldPoints[i] += direction * (strength * Mathf.Sin(45 * Mathf.Deg2Rad) + 0.001f);
                worldPointsPushed[i] = true;
                shouldUpdateMesh = true;
            }

            if (i > 0)
                currentDistance += Vector3.Distance(worldPoints[i], worldPoints[i - 1]);

        }

        pathDistance = currentDistance;

        if (shouldUpdateMesh)
        {
            ModifyMesh(pathDistance);
        }
    }

    public void PushAll(float strength = 1f, float duration = 0f)
    {
        if (gameObject.activeSelf)
        {
            StartCoroutine(Push(strength, duration));
            StartCoroutine(FakePercentage(duration));
        }
    }

    private IEnumerator Push(float strength,float duration)
    {
        int division = 30;
        float time = 0f;
        float deltaTime = duration / division;

        float currentDistance = 0f;
        int value = 0;
        int maxValue = worldPoints.Length;
        int deltaValue = maxValue / division;
        int iteration = 1;

        while (time < duration)
        {
            int currentMax = Mathf.Clamp(iteration * deltaValue, 0, maxValue);
            for (int i = value; i < currentMax; i++)
            {
                Vector3 worldPoint = transform.TransformPoint(worldPoints[i]);

                if (!worldPointsPushed[i])
                {
                    worldPoints[i] -= pushDirection * strength;
                    worldPointsPushed[i] = true;

                    OnGlueMeshModified?.Invoke(i, pushDirection, strength);
                }

                if (i > 0)
                    currentDistance += Vector3.Distance(worldPoints[i], worldPoints[i - 1]);
            }

            ModifyMesh(pathDistance,value, currentMax,false);

            yield return new WaitForSeconds(deltaTime);
            time += deltaTime;
            value = currentMax;
            iteration++;
        }
        
    }

    private IEnumerator FakePercentage(float duration)
    {
        float initial = pushAmount;
        float value = 0f;
        while (value < 1f)
        {
            value = Mathf.Clamp01(value+Time.deltaTime / duration);
            pushAmount = Mathf.Lerp(initial, 1f, value);
            yield return null;
        }
    }

    public void ResetComponent()
    {
        pushAmount = 0f;
    }

    private void OnEnable()
    {
        GetComponent<SplineCreator>().OnPathModified += GenerateMesh;
    }

    private void OnDisable()
    {
        GetComponent<SplineCreator>().OnPathModified -= GenerateMesh;
    }

}
