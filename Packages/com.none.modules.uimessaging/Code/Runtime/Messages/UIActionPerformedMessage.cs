using Modules.UIMessaging.Data;
using UnityEngine;

namespace Modules.UIMessaging.Messages
{
    public struct UIActionPerformedMessage
    {
        public int TargetID;
        public int ActionID;

        public UIActionPerformedMessage(int targetID, int actionID)
        {
            TargetID = targetID;
            ActionID = actionID;
        }

        public UIActionPerformedMessage(TargetActionPair pair)
        {
            TargetID = pair.TargetID;
            ActionID = pair.ActionID;
        }
    }
}