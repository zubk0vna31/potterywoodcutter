using Modules.UIMessaging.Data;

namespace Modules.UIMessaging.Messages
{
    public struct PerformUIActionMessage
    {
        public int TargetID;
        public int ActionID;

        public PerformUIActionMessage(int targetID, int actionID)
        {
            TargetID = targetID;
            ActionID = actionID;
        }

        public PerformUIActionMessage(TargetActionPair pair)
        {
            TargetID = pair.TargetID;
            ActionID = pair.ActionID;
        }
    }
}