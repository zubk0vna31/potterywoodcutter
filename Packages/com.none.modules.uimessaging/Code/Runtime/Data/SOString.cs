using UnityEngine;

namespace Modules.UIMessaging.Data
{
    [CreateAssetMenu(menuName = "Modules/UIMessaging/SOString")]
    public class SOString : ScriptableObject
    {
        [SerializeField] private string _value;

        public string Value => _value;
    }
}