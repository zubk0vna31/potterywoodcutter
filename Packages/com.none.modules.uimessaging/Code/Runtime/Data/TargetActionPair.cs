using Modules.UIMessaging.Messages;
using UnityEngine;

namespace Modules.UIMessaging.Data
{
    [System.Serializable]
    public struct TargetActionPair
    {
        [SerializeField] public int TargetID;
        [SerializeField] public int ActionID;

        public TargetActionPair(int targetID, int actionID)
        {
            TargetID = targetID;
            ActionID = actionID;
        }

        public bool IsIn(ref PerformUIActionMessage message)
        {
            return message.ActionID == ActionID && message.TargetID == TargetID;
        }
        
        public bool IsIn(ref UIActionPerformedMessage message)
        {
            return message.ActionID == ActionID && message.TargetID == TargetID;
        }
        
        public bool IsIn(PerformUIActionMessage message)
        {
            return IsIn(ref message);
        }
        
        public bool IsIn(UIActionPerformedMessage message)
        {
            return IsIn(ref message);
        }
    }
}