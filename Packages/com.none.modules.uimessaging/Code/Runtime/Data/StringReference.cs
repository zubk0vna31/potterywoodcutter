using UnityEngine;

namespace Modules.UIMessaging.Data
{
    [System.Serializable]
    public class StringReference
    {
        [SerializeField] public bool UseReference;
        [SerializeField] public string StringValue;
        [SerializeField] public SOString SOValue;

        public string Value => UseReference ? SOValue.Value : StringValue;
    }
}