using System;
using System.Collections.Generic;
using Modules.Root.ContainerComponentModel;
using Modules.UIMessaging.Data;
using Modules.UIMessaging.Messages;
using UniRx;
using UnityEngine;
using UnityEngine.Events;

namespace Modules.UIMessaging.Code.Runtime.Binders
{
    [System.Serializable]
    internal class MessageActionPair
    {
        [SerializeField] public StringReference Action;
        [SerializeField] public UnityEvent OnAction;
    }
    
    public class UIMessagingBinder : MonoBehaviour
    {
        // dependencies
        [Inject] private IStringToIDMappingService _stringToIDMappingService;
        
        [SerializeField] private StringReference _thisTarget;
        [SerializeField] private List<MessageActionPair> _receivers;

        public int ID { get; private set; }

        private CompositeDisposable _disposables;
        private bool _initialized = false;

        public void Awake()
        {
            TryInit();
        }

        public void SendActionPerformedMessage(string action)
        {
            SendActionPerformedMessage(_stringToIDMappingService.GetID(action));
        }

        public void SendActionPerformedMessage(StringReference action)
        {
            SendActionPerformedMessage(_stringToIDMappingService.GetID(action));
        }

        public void SendActionPerformedMessage(SOString action)
        {
            SendActionPerformedMessage(_stringToIDMappingService.GetID(action));
        }

        public void SendActionPerformedMessage(int actionID)
        {
            UniRx.MessageBroker.Default.Publish
                (new UIActionPerformedMessage(ID, actionID));
        }

        private void OnDestroy()
        {
            _disposables.Dispose();
        }

        private void TryInit()
        {
            if(_initialized)
                return;

            AppContainer.Inject(this);
            _disposables = new CompositeDisposable();

            ID = _stringToIDMappingService.GetID(_thisTarget);

            foreach (var receiver in _receivers)
            {
                TargetActionPair pair =
                    _stringToIDMappingService.GetTargetActionPair(_thisTarget, receiver.Action);
                
                UniRx.MessageBroker.Default
                    .Receive<PerformUIActionMessage>()
                    .Where(m => pair.IsIn(m))
                    .Subscribe(m => receiver.OnAction?.Invoke())
                    .AddTo(_disposables);
            }

            _initialized = true;
        }
        
        #if UNITY_EDITOR
        private void Reset()
        {
            _thisTarget = new StringReference(); 
            _thisTarget.StringValue = this.gameObject.name;
        }
#endif
    }
}