using System;
using Modules.Root.ContainerComponentModel;
using Modules.UIMessaging.Code.Runtime.Binders;
using Modules.UIMessaging.Data;
using UnityEngine;

namespace Modules.UIMessaging.Binders
{
    public class ActionPerformedSender : MonoBehaviour
    {
        // dependencies
        [Inject] private IStringToIDMappingService _stringToIDMappingService;
        
        public UIMessagingBinder Binder;
        public StringReference Action;
        
        [System.NonSerialized] private int _actionID;
        private bool _initialized = false;

        public void Awake()
        {
            TryInit();
        }

        public void Send()
        {
            TryInit();
            Binder.SendActionPerformedMessage(_actionID);
        }

        private void TryInit()
        {
            if(_initialized)
                return;
            
            AppContainer.Inject(this);
            _actionID = _stringToIDMappingService.GetID(Action);
            
            TryFindBinder();
            
            _initialized = true;
        }

        private void TryFindBinder()
        {
            if (Binder == null)
            {
                Binder = GetComponentInParent<UIMessagingBinder>();
                if (Binder == null)
                {
                    UnityEngine.Debug.LogError("UIMessaging Binder not found", this);
                }
            }
        }
        
#if UNITY_EDITOR
        void Reset()
        {
            TryFindBinder();
        }
#endif
    }
}