using Modules.UIMessaging.Data;

namespace Modules.UIMessaging
{
    public interface IStringToIDMappingService
    {
        int GetID(string fromString);
        int GetID(SOString fromString);
        int GetID(StringReference fromString);
        TargetActionPair GetTargetActionPair(string target, string action);
        TargetActionPair GetTargetActionPair(SOString target, SOString action);
        TargetActionPair GetTargetActionPair(StringReference target, StringReference action);
    }
}