using System.Collections.Generic;
using Modules.UIMessaging.Data;
using UnityEngine;

namespace Modules.UIMessaging
{   
    public class StringToIDMappingService : IStringToIDMappingService
    {
        private Dictionary<string, int> _stringToID;

        public StringToIDMappingService()
        {
            _stringToID = new Dictionary<string, int>();
        }

        public int GetID(string fromString)
        {
            if (_stringToID.ContainsKey(fromString))
            {
                return _stringToID[fromString];
            }

            _stringToID.Add(fromString, _stringToID.Count);
            return _stringToID.Count - 1;
        }

        public int GetID(SOString fromString)
        {
            return GetID(fromString.Value);
        }
        
        public int GetID(StringReference fromString)
        {
            return GetID(fromString.Value);
        }

        public TargetActionPair GetTargetActionPair(string target, string action)
        {
            return new TargetActionPair(GetID(target), GetID(action));
        }

        public TargetActionPair GetTargetActionPair(SOString target, SOString action)
        {
            return GetTargetActionPair(target.Value, action.Value);
        }
        
        public TargetActionPair GetTargetActionPair(StringReference target, StringReference action)
        {
            return GetTargetActionPair(target.Value, action.Value);
        }
    }
}