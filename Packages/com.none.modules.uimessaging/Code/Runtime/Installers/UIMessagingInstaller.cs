using Modules.Root.ContainerComponentModel;
using UnityEngine;

namespace Modules.UIMessaging.Installers
{
    [CreateAssetMenu(menuName = "Modules/UIMessaging/Installer")]
    public class UIMessagingInstaller : ASOInstaller
    {
        public override void Install(IContainer container)
        {
            container.Bind<IStringToIDMappingService>(new StringToIDMappingService());
        }
    }
}