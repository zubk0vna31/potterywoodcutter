using Leopotam.Ecs;
using Modules.Root.ECS;
using Modules.SGMBLinker;
using Modules.UIMessaging.Data;
using Modules.UIMessaging.Messages;
using UnityEngine;

namespace Modules.UIMessaging.SystemProviders
{
    [CreateAssetMenu(menuName = "Modules/UIMessaging/SystemsProvider")]
    public class UIMessagingSystemsProvider : ScriptableObject, ISystemsProvider
    {
        public EcsSystems GetSystems(EcsWorld world, EcsSystems endFrame, EcsSystems mainSystems)
        {
            EcsSystems systems = new EcsSystems(world);
            systems.Add(new RXMessagePropagator<UIActionPerformedMessage>());
            return systems;
        }
    }
}