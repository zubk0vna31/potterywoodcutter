using System;
using System.Collections.Generic;
using Modules.Root.ContainerComponentModel;
using Modules.UIMessaging.Messages;
using UniRx;
using UnityEditor;
using UnityEngine;

namespace Modules.UIMessaging.Code.Editor
{
    public class MessageSender : EditorWindow
    {
        private int buildingID = 0;
        private bool _touchControlState;
        private int _communicationID = 0;
        private bool _communicationState;

        private string Target;
        private string Action;

        private string Target2; // todo correct naming or another approach for now it just fields for different messages
        private string Action2; // todo correct naming or another approach for now it just fields for different messages

        public static void HorizontalLine(Color color, int thickness = 1, int padding = 10)
        {
            Rect r = EditorGUILayout.GetControlRect(GUILayout.Height(padding + thickness));
            r.height = thickness;
            r.y += padding / 2;
            r.x -= 2;
            r.width += 6;
            EditorGUI.DrawRect(r, color);
        }

        [MenuItem("Modules/UIMessaging/Message Sender")]
        static void Init()
        {
            // Get existing open window or if none, make a new one:
            var window = (MessageSender) GetWindow(typeof(MessageSender));
            window.titleContent = new GUIContent("UIMessages Sender");
            window.Show();
        }

        private void Header()
        {
            GUILayout.Label("UI Message Sender", new GUIStyle(GUI.skin.label)
            {
                alignment = TextAnchor.MiddleCenter,
                fontSize = 16
            });
        }

        private void Subheader(string message)
        {
            GUILayout.Label(message, new GUIStyle(GUI.skin.label)
            {
                alignment = TextAnchor.MiddleCenter,
                fontSize = 14
            });
        }

        private void UIPerformedActionMessage()
        {
            GUILayout.BeginVertical();

            GUILayout.Label("UI Action Performed message", new GUIStyle(GUI.skin.label)
            {
                fontSize = 14
            });

            GUILayout.Label("Target");
            Target = GUILayout.TextField(Target);
            GUILayout.Label("Action");
            Action = GUILayout.TextField(Action);

            if (GUILayout.Button("Send"))
            {
                IStringToIDMappingService stringToIDMappingService =
                    AppContainer.Component<IStringToIDMappingService>();
                MessageBroker.Default.Publish(new  UIActionPerformedMessage(stringToIDMappingService.GetID(Target), stringToIDMappingService.GetID(Action)));
            }

            GUILayout.EndVertical();
        }
        
        private void PerformUIActionMessage()
        {
            GUILayout.BeginVertical();

            GUILayout.Label("Perform UI Action Message", new GUIStyle(GUI.skin.label)
            {
                fontSize = 14
            });


            GUILayout.Label("Target");
            Target2 = GUILayout.TextField(Target2);
            GUILayout.Label("Action");
            Action2 = GUILayout.TextField(Action2);

            if (GUILayout.Button("Send"))
            {
                IStringToIDMappingService stringToIDMappingService =
                    AppContainer.Component<IStringToIDMappingService>();
                MessageBroker.Default.Publish(new  PerformUIActionMessage(stringToIDMappingService.GetID(Target2), stringToIDMappingService.GetID(Action2)));
            }

            GUILayout.EndVertical();
        }

        private void OnGUI()
        {
            Header();

            if (!EditorApplication.isPlaying)
            {
                EditorGUILayout.HelpBox("Application is not running", MessageType.Error);
                return;
            }

            HorizontalLine(new Color(0, 0, 0, 0.4f));
            PerformUIActionMessage();
            HorizontalLine(new Color(0, 0, 0, 0.4f));
            UIPerformedActionMessage();
            HorizontalLine(new Color(0, 0, 0, 0.4f));
            
        }
    }
}