using System;
using Leopotam.Ecs;
using Modules.ViewHub;
using Modules.VRFeatures;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace PWS.Pottery.Sculpting.Interactor
{
    public enum Hand
    {
        Left,
        Right
    }

    public enum SculptingHand
    {
        InternalsForming,
        WallsLifting,
        ItemForming,
        ClayCentering
    }

    public class SculptingHandInteractorTemplate : EntityTemplate
    {
        [SerializeField] private Hand _hand;

        [SerializeField] private GameObject _internalsFormingHandVisual;
        [SerializeField] private GameObject _wallsLiftingHandVisual;
        [SerializeField] private GameObject _itemFormingHandVisual;
        [SerializeField] private GameObject _clayCenteringHandVisual;
        [SerializeField] private InteractableColorChanger _colorChanger;

        [HideInInspector] public XRSculptingInteractable Interactable;
        private Collider _collider;

        public GameObject ClayCenteringHandVisual => _clayCenteringHandVisual;
        public GameObject InternalsFormingHandVisual => _internalsFormingHandVisual;
        public GameObject WallsLiftingHandVisual => _wallsLiftingHandVisual;
        public GameObject ItemFormingHandVisual => _itemFormingHandVisual;

        protected override void SetupEntity(EcsEntity entity, EcsWorld world)
        {
            base.SetupEntity(entity, world);

            _collider = GetComponent<Collider>();

            Interactable = GetComponent<XRSculptingInteractable>();
            Interactable.selectEntered.AddListener((SelectEnterEventArgs arg0) => {
                entity.Get<SelectEnteredSignal>();
            });
            Interactable.selectExited.AddListener((SelectExitEventArgs arg0) => {
                entity.Get<SelectExitedSignal>();
            });

            ref var hand = ref entity.Get<HandTag>();
            hand.View = this;
            hand.OriginLocalPosition = transform.localPosition;

            if (_hand == Hand.Left)
                entity.Get<LeftHandTag>();
            else
                entity.Get<RightHandTag>();

            HideHand();
        }

        public void ShowHand(SculptingHand sculptingHand)
        {
            switch (sculptingHand)
            {
                case SculptingHand.InternalsForming:
                    SetGOActive(_internalsFormingHandVisual, true);
                    _colorChanger.SkinnedMeshRenderer =
                        _internalsFormingHandVisual.GetComponentInChildren<SkinnedMeshRenderer>();
                    break;
                case SculptingHand.WallsLifting:
                    SetGOActive(_wallsLiftingHandVisual, true);
                    _colorChanger.SkinnedMeshRenderer =
                        _wallsLiftingHandVisual.GetComponentInChildren<SkinnedMeshRenderer>();
                    break;
                case SculptingHand.ItemForming:
                    SetGOActive(_itemFormingHandVisual, true);
                    _colorChanger.SkinnedMeshRenderer =
                        _itemFormingHandVisual.GetComponentInChildren<SkinnedMeshRenderer>();
                    break;
                case SculptingHand.ClayCentering:
                    SetGOActive(_clayCenteringHandVisual, true);
                    _colorChanger.SkinnedMeshRenderer =
                        _clayCenteringHandVisual.GetComponentInChildren<SkinnedMeshRenderer>();
                    break;
            }

            if (!_collider.enabled)
                _collider.enabled = true;
        }

        public void HideHand()
        {
            SetGOActive(_internalsFormingHandVisual, false);
            SetGOActive(_wallsLiftingHandVisual, false);
            SetGOActive(_itemFormingHandVisual, false);
            SetGOActive(_clayCenteringHandVisual, false);

            if (_collider.enabled)
                _collider.enabled = false;
        }

        public void SetGOActive(GameObject go, bool active)
        {
            if (go.activeSelf != active)
                go.SetActive(active);
        }
    }
}
