using Leopotam.Ecs;
using Modules.ViewHub;
using UnityEngine;

namespace PWS.Pottery.Sculpting.Interactor
{
    public class SculptingGeneralInteractorTemplate : EntityTemplate
    {
        protected override void SetupEntity(EcsEntity entity, EcsWorld world)
        {
            base.SetupEntity(entity, world);

            entity.Get<SculptingInteractorTag>().View = this;
        }
    }
}
