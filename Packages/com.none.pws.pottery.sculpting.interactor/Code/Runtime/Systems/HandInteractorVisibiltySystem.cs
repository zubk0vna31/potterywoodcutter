﻿using Leopotam.Ecs;
using Modules.StateGroup.Components;
using PWS.Pottery.Common;
using UnityEngine;

namespace PWS.Pottery.Sculpting.Interactor
{
    public class HandInteractorVisibiltySystem<StateT> : IEcsRunSystem where StateT : struct
    {
        // auto injected fields
        readonly EcsFilter<StateEnter, StateT> _entering;
        readonly EcsFilter<StateExit, StateT> _exiting;
        readonly EcsFilter<StateT> _inState;

        readonly EcsFilter<VisibilitySignal, HandTag> _visibilitySignal;

        readonly EcsFilter<HandTag> _hands;

        private SculptingHand _sculptingHand;

        public HandInteractorVisibiltySystem(SculptingHand sculptingHand)
        {
            _sculptingHand = sculptingHand;
        }

        public void Run()
        {
            if (!_entering.IsEmpty())
            {
                foreach (var i in _hands)
                {
                    _hands.Get1(i).View.ShowHand(_sculptingHand);
                }
            }
            if (!_exiting.IsEmpty())
            {
                foreach (var i in _hands)
                {
                    _hands.Get1(i).View.HideHand();
                }
            }

            if (_inState.IsEmpty())
                return;

            foreach (var idx in _visibilitySignal)
            {
                if (_visibilitySignal.Get1(idx).Show)
                    _visibilitySignal.Get2(idx).View.ShowHand(_sculptingHand);
                else
                    _visibilitySignal.Get2(idx).View.HideHand();
            }
        }
    }
}