﻿using Leopotam.Ecs;
using Modules.StateGroup.Components;
using Modules.ViewHub;
using UnityEngine;

namespace PWS.Pottery.Sculpting.Interactor
{
    public class HandInteractorPositionSystem<StateT> : IEcsRunSystem where StateT : struct
    {
        // auto injected fields
        readonly EcsFilter<StateEnter, StateT> _entering;
        readonly EcsFilter<StateT> _inState;

        readonly EcsFilter<UnityView, SculptingInteractorTag> _generalInteractor;
        readonly EcsFilter<UnityView, HandTag> _bothHands;

        readonly EcsFilter<SelectEnteredSignal, HandTag> _handSelectEntered;
        readonly EcsFilter<SelectExitedSignal, HandTag> _handSelectExited;

        private InteractableParameters _interactableMoveAxes;
        private float _moveSmoothSpeed;
        private bool _useOriginPos;

        public HandInteractorPositionSystem(InteractableParameters interactableMoveAxes, float moveSmoothSpeed, bool useOriginPos = true)
        {
            _interactableMoveAxes = interactableMoveAxes;
            _moveSmoothSpeed = moveSmoothSpeed;
            _useOriginPos = useOriginPos;
        }

        public void Run()
        {
            if (!_entering.IsEmpty())
            {
                Vector3 initAvgPosition = AvgPosition();

                foreach (var i in _bothHands)
                {
                    _bothHands.Get2(i).VectorFromCenter = _bothHands.Get1(i).Transform.position - initAvgPosition;
                    _bothHands.Get2(i).InitLocalPosition = _bothHands.Get1(i).Transform.localPosition;
                }
                foreach (var i in _generalInteractor)
                {
                    _generalInteractor.Get2(i).Position = initAvgPosition;

                }
            }

            if (_inState.IsEmpty())
                return;

            foreach (var i in _handSelectEntered)
            {
                _handSelectEntered.Get2(i).HandModelParent = _handSelectEntered.Get2(i).View.Interactable.selectingInteractor.transform.Find("ModelParent");
                _handSelectEntered.Get2(i).HandModelParent.gameObject.SetActive(false);
            }

            MoveHands();

            foreach (var i in _handSelectExited)
            {
                if (_handSelectExited.Get2(i).HandModelParent)
                {
                    /*_handSelectExited.Get2(i).HandModelParent.localPosition = Vector3.zero;
                    _handSelectExited.Get2(i).HandModelParent.localRotation = Quaternion.identity;*/
                    _handSelectExited.Get2(i).HandModelParent.gameObject.SetActive(true);
                    _handSelectExited.Get2(i).HandModelParent = null;
                }
            }

            /*foreach (var i in _bothHands)
            {
                if (_bothHands.Get2(i).HandModelParent)
                {
                    _bothHands.Get2(i).HandModelParent.position = _bothHands.Get1(i).Transform.position;
                    _bothHands.Get2(i).HandModelParent.rotation = _bothHands.Get1(i).Transform.rotation;
                }
            }*/

        }

        void MoveHands()
        {
            bool move = true;

            foreach (var i in _bothHands)
            {
                move &= _bothHands.Get2(i).View.Interactable.isSelected;
            }

            if (move)
            {
                Vector3 avgPosition = Vector3.zero;
                foreach (var i in _bothHands)
                {
                    Vector3 oldLocalPos = _useOriginPos ? _bothHands.Get2(i).OriginLocalPosition : _bothHands.Get2(i).InitLocalPosition;

                    Vector3 interactorPosition = _bothHands.Get2(i).View.Interactable.selectingInteractor.transform.position;
                    interactorPosition = ClampHandHeight(interactorPosition);

                    Vector3 newLocalPos = _bothHands.Get1(i).Transform.parent.InverseTransformPoint(interactorPosition);

                    if (!_interactableMoveAxes.XMove) newLocalPos.x = oldLocalPos.x;
                    if (!_interactableMoveAxes.YMove) newLocalPos.y = oldLocalPos.y;
                    if (!_interactableMoveAxes.ZMove) newLocalPos.z = oldLocalPos.z;

                    avgPosition += _bothHands.Get1(i).Transform.parent.TransformPoint(newLocalPos);
                }
                avgPosition /= _bothHands.GetEntitiesCount();

                foreach (var i in _generalInteractor)
                {
                    _generalInteractor.Get2(i).Position = Vector3.Lerp(_generalInteractor.Get2(i).Position, avgPosition, _moveSmoothSpeed * Time.deltaTime);

                    foreach (var j in _bothHands)
                    {
                        _bothHands.Get1(j).Transform.position = _generalInteractor.Get2(j).Position + _bothHands.Get2(j).VectorFromCenter;
                    }
                }

            }
        }

        Vector3 AvgPosition()
        {
            Vector3 avgPosition = Vector3.zero;
            foreach (var handIdx in _bothHands)
            {
                avgPosition += _bothHands.Get1(handIdx).Transform.position;
            }

            return avgPosition / _bothHands.GetEntitiesCount();
        }

        Vector3 ClampHandHeight(Vector3 interactorPosition)
        {
            Vector3 handPosition = interactorPosition;
            float bottomHeight = interactorPosition.y;
            handPosition.y = Mathf.Max(handPosition.y, bottomHeight);
            return handPosition;
        }
    }
}