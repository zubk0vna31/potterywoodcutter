using UnityEngine.XR.Interaction.Toolkit;

namespace PWS.Pottery.Sculpting.Interactor
{
    [System.Serializable]
    public struct InteractableParameters
    {
        public bool XMove;
        public bool YMove;
        public bool ZMove;
    }

    public class XRSculptingInteractable : XRBaseInteractable
    {
        /*
        public override void ProcessInteractable(XRInteractionUpdateOrder.UpdatePhase updatePhase)
        {
            if (selectingInteractor && !selectingInteractor.gameObject.name.Equals(_interactorHandName))
                return;

            base.ProcessInteractable(updatePhase);

            if(updatePhase == XRInteractionUpdateOrder.UpdatePhase.Dynamic)
            {
                if (isSelected)
                {
                    Vector3 oldLocalPos = _tr.localPosition;

                    _tr.position = selectingInteractor.transform.position;

                    Vector3 newLocalPos = _tr.localPosition;

                    if (!MoveAxes.XMove) newLocalPos.x = oldLocalPos.x;
                    if (!MoveAxes.YMove) newLocalPos.y = oldLocalPos.y;
                    if (!MoveAxes.ZMove) newLocalPos.z = oldLocalPos.z;

                    _tr.localPosition = newLocalPos;

                    if (handModelParent)
                    {
                        handModelParent.position = _tr.position;
                        handModelParent.rotation = _tr.rotation;
                    }
                }
            }
        }*/
    }
}
