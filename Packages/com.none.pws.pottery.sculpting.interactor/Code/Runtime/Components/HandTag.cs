﻿
using UnityEngine;

namespace PWS.Pottery.Sculpting.Interactor
{
    public struct HandTag
    {
        public SculptingHandInteractorTemplate View;
        public Transform HandModelParent;
        public Vector3 InitLocalPosition;
        public Vector3 OriginLocalPosition;
        public Vector3 VectorFromCenter;
    }
}