﻿
using UnityEngine;

namespace PWS.Pottery.Sculpting.Interactor
{
    public struct SculptingInteractorTag
    {
        public SculptingGeneralInteractorTemplate View;
        public Vector3 Position;
    }
}