using System.Collections.Generic;
using UnityEngine;

namespace PWS.Common.TravelPoints
{
    public struct TravelPointTag 
    {
        public Dictionary<string, Vector4> dataSet;
        public float height;
    }
}
