using Leopotam.Ecs;
using Modules.StateGroup.Components;
using Modules.ViewHub;
using UnityEngine;

namespace PWS.Common.TravelPoints
{
    public class TravelPointPositioningSystem<T> : IEcsRunSystem where T : struct
    {
        private readonly EcsFilter<StateEnter, T> _stateEnter;
        private readonly EcsFilter<StateExit, T> _stateExit;
        private readonly EcsFilter<TravelPointTag, UnityView> _travelPoints;

        private readonly string _placementKey;

        public TravelPointPositioningSystem(string placementKey)
        {
            _placementKey = placementKey;
        }

        public void Run()
        {
            if (!_stateEnter.IsEmpty())
            {
                foreach (var i in _travelPoints)
                {
                    _travelPoints.Get2(i).GameObject.SetActive(true);

                    var data = _travelPoints.Get1(i).dataSet[_placementKey];
                    var height = _travelPoints.Get1(i).height;

                    _travelPoints.Get2(i).Transform.position = data;
                    _travelPoints.Get2(i).Transform.localScale = new Vector3(data.w, height, data.w);
                }
            }

            if (!_stateExit.IsEmpty())
            {
                foreach (var i in _travelPoints)
                {
                    _travelPoints.Get2(i).GameObject.SetActive(false);
                }
            }
        }
    }
}
