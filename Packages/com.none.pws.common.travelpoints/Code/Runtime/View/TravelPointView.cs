using Leopotam.Ecs;
using Modules.ViewHub;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace PWS.Common.TravelPoints
{
    [System.Serializable]
    public struct TravelPointEditorData
    {
        public string key;
        [Tooltip("XYZ for position, W for XZ scale")]
        public Vector4 transformInfo;
    }

    public class TravelPointView : ViewComponent
    {
        private const string defaultKey = "Default";

        [SerializeField,Range(0.1f,1.5f)]
        private float height = 0.75f;

        [SerializeField]
        private List<TravelPointEditorData> data = new List<TravelPointEditorData>();

        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            ref var travelPoint = ref ecsEntity.Get<TravelPointTag>();

            Dictionary<string, Vector4> dataSet = new Dictionary<string, Vector4>(data.Count);

            foreach (var d in data)
            {
                if (dataSet.ContainsKey(d.key))
                {
                    continue;
                }

                dataSet.Add(d.key, d.transformInfo);
            }

            travelPoint.dataSet = dataSet;
            travelPoint.height = height;

            gameObject.SetActive(false);
        }
#if UNITY_EDITOR
        public void AddCurrentSettings()
        {
            Vector4 transformInfo = new Vector4(transform.position.x, transform.position.y, transform.position.z,
                Mathf.Min(transform.localScale.x, transform.localScale.z));

            data.Add(new TravelPointEditorData()
            {
                key = defaultKey,
                transformInfo = transformInfo
            });

            Debug.Log("Current settings was added. Don't forget to change key. If you don't, then it will be removed " +
                "when entity will be inited!");

            EditorUtility.SetDirty(this);
        }
#endif
    }
}
