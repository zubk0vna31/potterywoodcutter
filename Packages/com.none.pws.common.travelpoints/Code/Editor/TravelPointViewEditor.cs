using UnityEngine;
using UnityEditor;

namespace PWS.Common.TravelPoints
{
    [CustomEditor(typeof(TravelPointView))]
    public class TravelPointViewEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            if(GUILayout.Button("Add Current Settings"))
            {
                (target as TravelPointView).AddCurrentSettings();
            }
        }
    }
}
