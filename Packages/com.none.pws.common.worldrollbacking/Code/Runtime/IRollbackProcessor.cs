using Leopotam.Ecs;

namespace PWS.Common.WorldRollbacking
{
    public interface IProcessor
    {
        
    }
    
    public interface IOnCreateNewDumpProcessor : IProcessor
    {
        void OnCreateNewDump();
    }

    public interface IDumpRollbackProcessor : IProcessor
    {
        void OnSaveDump(EcsEntity currentEntity, EcsEntity dumpEntity);

        void OnRestoreFromDump(EcsEntity currentEntity, EcsEntity dumpEntity);
    }

    // called during rollback when Entity present in current world, but not present in dump world
    public interface IOnDeleteEntityProcessor : IProcessor
    {
        void OnDeleteEntity(EcsEntity entityToDelete);
    }
}