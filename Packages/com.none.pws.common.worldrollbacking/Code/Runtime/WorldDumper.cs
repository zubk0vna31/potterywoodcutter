using System;
using System.Collections.Generic;
using Leopotam.Ecs;
using UnityEngine;

namespace PWS.Common.WorldRollbacking
{
    public class WorldDumper
    {
        public EcsWorld Dump;
        public EcsWorld TargetWorld;
        public Dictionary<EcsEntity, EcsEntity> _targetEntityToDumpEntityMap;
        private HashSet<Type> _ignoreDefaultCopyComponents;
        private List<IOnDeleteEntityProcessor> _onDeleteEntityProcessors;
        private List<IDumpRollbackProcessor> _dumpRollbackProcessor;
        private List<IOnCreateNewDumpProcessor> _onCreateNewDumpProcessors;
        
        #if UNITY_EDITOR && DEBUG
        private Leopotam.Ecs.UnityIntegration.EcsWorldObserver _worldDebugListener;
        #endif

        public WorldDumper(EcsWorld targetWorld)
        {
            TargetWorld = targetWorld;
            _targetEntityToDumpEntityMap = new Dictionary<EcsEntity, EcsEntity>();
            
            _onDeleteEntityProcessors = new List<IOnDeleteEntityProcessor>();
            _dumpRollbackProcessor = new List<IDumpRollbackProcessor>();
            _onCreateNewDumpProcessors = new List<IOnCreateNewDumpProcessor>();
            _ignoreDefaultCopyComponents = new HashSet<Type>();
        }

        public WorldDumper AddProcessor(IProcessor processor)
        {
            if (processor is IOnCreateNewDumpProcessor dumpProcessor)
            {
                _onCreateNewDumpProcessors.Add(dumpProcessor);
            }

            if (processor is IOnDeleteEntityProcessor deleteEntityProcessor)
            {
                _onDeleteEntityProcessors.Add(deleteEntityProcessor);
            }

            if (processor is IDumpRollbackProcessor dumpRollbackProcessor)
            {
                _dumpRollbackProcessor.Add(dumpRollbackProcessor);
            }

            return this;
        }

        // component will not be copied by default from/to dump, requires custom processor
        public WorldDumper ExcludeFromDefaultProcessing<T>()
        {
            if (!_ignoreDefaultCopyComponents.Contains(typeof(T)))
            {
                _ignoreDefaultCopyComponents.Add(typeof(T));
            }

            return this;
        }

        public void SaveDump()
        {
            UnityEngine.Debug.Log("Saving dump");
#if UNITY_EDITOR && DEBUG
            if (Dump != null && _worldDebugListener != null)
            {
                Dump.RemoveDebugListener(_worldDebugListener);
                GameObject.Destroy(_worldDebugListener.gameObject);
            }
#endif

            foreach (var processor in _onCreateNewDumpProcessors)
            {
                processor.OnCreateNewDump();
            }
            Dump = new EcsWorld();

#if UNITY_EDITOR && DEBUG
            _worldDebugListener = Leopotam.Ecs.UnityIntegration.EcsWorldObserver.Create(Dump, "DUMP WORLD").GetComponent<Leopotam.Ecs.UnityIntegration.EcsWorldObserver>();
#endif
            
            EcsEntity[] entities = new EcsEntity[0];
            TargetWorld.GetAllEntities(ref entities);
            foreach (var entity in entities)
            {
                EcsEntity dumpEntity = Dump.NewEntity();
                foreach (var processor in _dumpRollbackProcessor)
                {
                    processor.OnSaveDump(entity, dumpEntity);
                }

                //
                if (dumpEntity.GetComponentsCount() == 0)
                {
                    dumpEntity.Destroy();
                    continue;
                }
                //

                _targetEntityToDumpEntityMap[entity] = dumpEntity;
            }

        }

        public void RestoreFromDump()
        {
            UnityEngine.Debug.Log("Restoring from dump");
            EcsEntity[] entities = new EcsEntity[0];
            TargetWorld.GetAllEntities(ref entities);
            
            HashSet<EcsEntity> unprocessedDumpEntities = new HashSet<EcsEntity>();
            foreach (var pair in _targetEntityToDumpEntityMap)
            {
                unprocessedDumpEntities.Add(pair.Value);
            }

            // update current entities in worlds
            // and remove entities not present in dump
            foreach (var entity in entities)
            {
                if (_targetEntityToDumpEntityMap.ContainsKey(entity))
                {
                    EcsEntity dumpEntity = _targetEntityToDumpEntityMap[entity];
                    foreach (var processor in _dumpRollbackProcessor)
                    {
                        processor.OnRestoreFromDump(entity, dumpEntity);
                    }

                    unprocessedDumpEntities.Remove(dumpEntity);
 ;               }
                else
                {
                    // entity not present in dump world, destroy it
                    foreach (var processor in _onDeleteEntityProcessors)
                    {
                        processor.OnDeleteEntity(entity);
                    }
                    entity.Destroy();
                }
            }

            foreach (var entity in unprocessedDumpEntities)
            {
                EcsEntity newEntity = TargetWorld.NewEntity();
                foreach (var processor in _dumpRollbackProcessor)
                {
                    processor.OnRestoreFromDump(newEntity, entity);
                }
            }
        }
    }
}