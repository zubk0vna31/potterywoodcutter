using Leopotam.Ecs;

namespace PWS.Common.WorldRollbacking
{
    public class DefaultRollbackProcessor<T> : IDumpRollbackProcessor where T : struct
    {
        public virtual void OnSaveDump(EcsEntity currentEntity, EcsEntity dumpEntity)
        {
            if(!currentEntity.Has<T>())
                return;
            
            dumpEntity.Get<T>() = currentEntity.Get<T>();
        }

        public virtual void OnRestoreFromDump(EcsEntity currentEntity, EcsEntity dumpEntity)
        {
            if (dumpEntity.Has<T>())
            {
                currentEntity.Get<T>() = dumpEntity.Get<T>();
            }
            else if(currentEntity.Has<T>())
            {
                currentEntity.Del<T>();
            }
        }
    }
}