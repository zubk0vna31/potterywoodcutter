using Leopotam.Ecs;
using Modules.StateGroup.Components;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PWS.Common.ResultsEvaluation
{
    public class ResultsEvaluationWindowUIProcessing<StateT> : IEcsRunSystem where StateT : struct
    {
        // auto injected fields
        readonly EcsFilter<StateEnter, StateT> _entering;
        readonly EcsFilter<StateExit, StateT> _exiting;

        readonly EcsFilter<ResultsEvaluationWindow> _view;

        private IResultsEvaluationDataHolder _data;

        public ResultsEvaluationWindowUIProcessing(IResultsEvaluationDataHolder data)
        {
            _data = data;
        }

        public void Run()
        {
            foreach (var i in _view)
            {
                if (!_entering.IsEmpty())
                {
                    _view.Get1(i).View.SetData(_data.Grades);
                    _view.Get1(i).View.Show();
                }

                if (!_exiting.IsEmpty())
                {
                    _view.Get1(i).View.Hide();
                }
            }
        }
    }
}
