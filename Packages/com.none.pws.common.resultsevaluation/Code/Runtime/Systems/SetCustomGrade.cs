﻿using Leopotam.Ecs;
using Modules.StateGroup.Components;
using PWS.Common.UI;
using System.Collections.Generic;
using UnityEngine;

namespace PWS.Common.ResultsEvaluation
{
    public class SetCustomGrade<StateT> : IEcsRunSystem where StateT : struct
    {
        // auto injected fields
        readonly EcsFilter<StateExit, StateT> _exiting;

        private readonly EcsWorld _ecsWorld;

        private IResultsEvaluationDataHolder _resultsEvaluationData;
        private StateInfoData _stateInfo;
        private float _grade;

        public SetCustomGrade(float grade, IResultsEvaluationDataHolder resultsEvaluationData, StateInfoData stateInfo)
        {
            _grade = grade;
            _resultsEvaluationData = resultsEvaluationData;
            _stateInfo = stateInfo;
        }

        public void Run()
        {
            if (!_exiting.IsEmpty())
            {
                float grade = _grade * _resultsEvaluationData.ResultGradeMultiplier;

                _resultsEvaluationData.AddGrade(_stateInfo.HeaderTerm, grade);

                _ecsWorld.NewEntity().Get<GradeWasGivenSignal>().Grade = _grade;

                /*foreach (KeyValuePair<string, float> grade in _resultsEvaluationData.Grades)
                {
                    Debug.Log(grade.Key + ": " + grade.Value);
                }*/
            }
        }
    }
}