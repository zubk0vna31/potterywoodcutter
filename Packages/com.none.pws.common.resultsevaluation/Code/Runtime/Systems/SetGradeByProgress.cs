﻿using Leopotam.Ecs;
using Modules.StateGroup.Components;
using PWS.Common.UI;
using System.Collections.Generic;
using UnityEngine;

namespace PWS.Common.ResultsEvaluation
{
    public class SetGradeByProgress<StateT> : IEcsRunSystem where StateT : struct
    {
        // auto injected fields
        readonly EcsFilter<StateExit, StateT> _exiting;
        readonly EcsFilter<StateWindow, StateProgress> _progress;

        private readonly EcsWorld _ecsWorld;

        private IResultsEvaluationDataHolder _resultsEvaluationData;
        private StateInfoData _stateInfo;

        public SetGradeByProgress(IResultsEvaluationDataHolder resultsEvaluationData, StateInfoData stateInfo)
        {
            _resultsEvaluationData = resultsEvaluationData;
            _stateInfo = stateInfo;
        }

        public void Run()
        {
            if (!_exiting.IsEmpty())
            {
                foreach (var i in _progress)
                {
                    float grade = 0;

                    if (_progress.Get2(i).Value > 0.75f && _progress.Get2(i).Value <= 0.9f)
                        grade = 0.5f;
                    else if (_progress.Get2(i).Value > 0.9f)
                        grade = 1;

                    grade *= _resultsEvaluationData.ResultGradeMultiplier;

                    //float grade = (_progress.Get2(i).Value - _progress.Get2(i).SkipThreshold) / (1 - _progress.Get2(i).SkipThreshold);
                    _resultsEvaluationData.AddGrade(_stateInfo.HeaderTerm, grade);

                    _ecsWorld.NewEntity().Get<GradeWasGivenSignal>().Grade = _progress.Get2(i).Value;
                }

                /*foreach (KeyValuePair<string, float> grade in _resultsEvaluationData.Grades)
                {
                    Debug.Log(grade.Key + ": " + grade.Value.ToString("0.00"));
                }*/
            }
        }
    }
}