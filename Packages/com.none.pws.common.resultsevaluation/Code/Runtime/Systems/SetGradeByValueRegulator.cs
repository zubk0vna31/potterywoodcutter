﻿using Leopotam.Ecs;
using Modules.StateGroup.Components;
using PWS.Common.UI;
using PWS.Pottery.ItemDataHolderService;
using System.Collections.Generic;
using UnityEngine;

namespace PWS.Common.ResultsEvaluation
{
    public class SetGradeByValueRegulator<StateT, MachineT> : IEcsRunSystem where StateT : struct where MachineT : struct
    {
        // auto injected fields
        readonly EcsFilter<StateExit, StateT> _exiting;

        private readonly EcsWorld _ecsWorld;

        private IResultsEvaluationDataHolder _resultsEvaluationData;
        private IValueRegulatorDataHolder<MachineT> _valueRegulatorData;
        private StateInfoData _stateInfo;
        private float _minRangeValue;
        private float _maxRangeValue;
        private bool _avgWithPrev;

        public SetGradeByValueRegulator(IResultsEvaluationDataHolder resultsEvaluationData, 
            IValueRegulatorDataHolder<MachineT> valueRegulatorData, StateInfoData stateInfo,
            float minRangeValue, float maxRangeValue, bool avgWithPrev = false)
        {
            _resultsEvaluationData = resultsEvaluationData;
            _valueRegulatorData = valueRegulatorData;
            _stateInfo = stateInfo;
            _minRangeValue = valueRegulatorData.ConvertFromCustom(minRangeValue);
            _maxRangeValue = valueRegulatorData.ConvertFromCustom(maxRangeValue);
            _avgWithPrev = avgWithPrev;
        }
        public SetGradeByValueRegulator(IResultsEvaluationDataHolder resultsEvaluationData,
           IValueRegulatorDataHolder<MachineT> valueRegulatorData, StateInfoData stateInfo,
           float targetValue, bool avgWithPrev = false)
        {
            _resultsEvaluationData = resultsEvaluationData;
            _valueRegulatorData = valueRegulatorData;
            _stateInfo = stateInfo;
            _minRangeValue = valueRegulatorData.ConvertFromCustom(targetValue);
            _maxRangeValue = _minRangeValue;
            _avgWithPrev = avgWithPrev;
        }

        public void Run()
        {
            if (!_exiting.IsEmpty())
            {
                float delta = 0;
                if(_valueRegulatorData.Value > _maxRangeValue)
                    delta = _valueRegulatorData.Value - _maxRangeValue;
                else if (_valueRegulatorData.Value < _minRangeValue)
                    delta = _minRangeValue - _valueRegulatorData.Value;

                //fixed grades
                /*
                float grade = 1;
                if (delta > 0 && delta <= 0.25f)
                    grade = 0.5f;
                else if (delta > 0.25f && delta <= 0.5f)
                    grade = 0.2f;
                else if (delta > 0.5f)
                    grade = 0;*/

                //float grades
                float grade = Mathf.Clamp01(1 - delta);
                grade *= _resultsEvaluationData.ResultGradeMultiplier;

                if (_avgWithPrev)
                    grade = (grade + _resultsEvaluationData.GetGrade(_stateInfo.HeaderTerm)) / 2;

                _resultsEvaluationData.AddGrade(_stateInfo.HeaderTerm, grade);

                _ecsWorld.NewEntity().Get<GradeWasGivenSignal>().Grade = grade / _resultsEvaluationData.ResultGradeMultiplier;

                /*foreach (KeyValuePair<string, float> grade in _resultsEvaluationData.Grades)
                {
                    Debug.Log(grade.Key + ": " + grade.Value.ToString("0.00"));
                }*/
            }
        }
    }
}