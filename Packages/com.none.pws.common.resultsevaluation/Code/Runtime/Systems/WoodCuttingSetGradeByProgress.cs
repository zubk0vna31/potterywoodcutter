using Leopotam.Ecs;
using PWS.Common.UI;
using UnityEngine;

namespace PWS.Common.ResultsEvaluation
{
    public class WoodCuttingSetGradeByProgress<T> : IEcsRunSystem where T : struct
    {
        private readonly IResultsEvaluationDataHolder _resultsEvaluationData;
        private readonly StateInfoData _stateInfo;

        private readonly EcsFilter<T> _state;
        private readonly EcsFilter<SetGradeSignal> _setGradeSignal;

        private readonly EcsWorld _ecsWorld;

        public WoodCuttingSetGradeByProgress(IResultsEvaluationDataHolder resultsEvaluationData,
            StateInfoData stateInfo)
        {
            _resultsEvaluationData = resultsEvaluationData;
            _stateInfo = stateInfo;
        }

        public void Run()
        {
            if (_state.IsEmpty()) return;

            if (!_setGradeSignal.IsEmpty())
            {
                foreach (var i in _setGradeSignal)
                {
                    int pointsAmount = 5;
                    float gradeEdge = 1f / pointsAmount;

                    float grade = _setGradeSignal.Get1(i).grade/100f;

                    grade = Mathf.Floor(grade / gradeEdge) * gradeEdge;

                    Debug.Log($"<color=orange>{_stateInfo.Header}</color> - <color=cyan>{grade}</color>");

                    _resultsEvaluationData.AddGrade(_stateInfo.HeaderTerm, grade);

                    _ecsWorld.NewEntity().Get<GradeWasGivenSignal>().Grade = _setGradeSignal.Get1(i).grade / 100f;
                }
            }
        }
    }
}
