using Modules.Root.ContainerComponentModel;
using UnityEngine;

namespace PWS.Common.ResultsEvaluation
{
    // mono installer to allow runtime modification and observation of serialized data
    public class ResultsEvaluationDataHolderInstaller : AMonoInstaller
    {
        [SerializeField]
        private float _resultGradeMultiplier = 5;

        public override void Install(IContainer container)
        {
            IResultsEvaluationDataHolder itemDataHolder = new ResultsEvaluationDataHolder(_resultGradeMultiplier);

            container.Bind(itemDataHolder);

        }
    }
}