using System.Collections.Generic;

namespace PWS.Common.ResultsEvaluation
{
    public interface IResultsEvaluationDataHolder
    {
        Dictionary<string, float> Grades { get; }

        float ResultGradeMultiplier { get; }
        void AddGrade(string name, float grade);
        float GetGrade(string name);
        float ResultGrade();

    }
}