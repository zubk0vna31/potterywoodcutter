using System.Collections.Generic;

namespace PWS.Common.ResultsEvaluation
{
    public class ResultsEvaluationDataHolder : IResultsEvaluationDataHolder
    {
        private float _resultGradeMultiplier;

        private Dictionary<string, float> _grades = new Dictionary<string, float>();
        public Dictionary<string, float> Grades => _grades;

        public float ResultGradeMultiplier => _resultGradeMultiplier;

        public ResultsEvaluationDataHolder(float resultGradeMultiplier)
        {
            _resultGradeMultiplier = resultGradeMultiplier;
        }


        public void AddGrade(string name, float grade)
        {
            _grades[name] = grade;
        }

        public float GetGrade(string name)
        {
            if (!_grades.ContainsKey(name))
                return 0;

            return _grades[name];
        }

        public float ResultGrade()
        {
            float resultGrade = 0;

            foreach (float grade in _grades.Values)
            {
                resultGrade += grade;
            }
            resultGrade /= _grades.Values.Count;

            return resultGrade * _resultGradeMultiplier;
        }
    }
}