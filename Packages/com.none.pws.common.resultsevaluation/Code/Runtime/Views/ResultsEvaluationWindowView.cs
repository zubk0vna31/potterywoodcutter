using Leopotam.Ecs;
using Modules.ViewHub;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace PWS.Common.ResultsEvaluation
{
    public class ResultsEvaluationWindowView : ViewComponent
    {
        [SerializeField] private GameObject _showHideTarget;

        [SerializeField]
        private TMP_Text _avgGradeLabel;
        [SerializeField] private StateGradeItem[] _items;

        private EcsEntity _entity;
        private float _finalGrade;
        private float _percentage;

        public float FinalGrade => _finalGrade;
        public float Percentage => _percentage;

        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            _entity = ecsEntity;
            ecsEntity.Get<ResultsEvaluationWindow>().View = this;
            Hide();
        }

        public virtual void Show()
        {
            if (_showHideTarget.activeSelf)
                return;

            StartCoroutine(UpdateCanvas());
        }

        IEnumerator UpdateCanvas()
        {
            _showHideTarget.gameObject.SetActive(true);
            yield return null;
            _showHideTarget.gameObject.SetActive(false);
            _showHideTarget.gameObject.SetActive(true);
        }

        public virtual void Hide()
        {
            if (!_showHideTarget.activeSelf)
                return;

            _showHideTarget.gameObject.SetActive(false);
        }

        public void SetData(Dictionary<string, float> grades)
        {
            float avgGrade = 0;
            int i = 0;
            foreach (KeyValuePair<string, float> grade in grades)
            {
                avgGrade += grade.Value;

                if (i >= _items.Length)
                    break;

                _items[i].SetData(grade.Key, grade.Value.ToString("0.0"));
                _items[i].gameObject.SetActive(true);
                i++;
            }

            for (int j = i; j < _items.Length; j++)
            {
                _items[i].gameObject.SetActive(false);
            }

            avgGrade /= grades.Values.Count;
            _avgGradeLabel.text = avgGrade.ToString("0.0");
        }

        public void SetData(Dictionary<string, float> grades,float gradeMultiplier=1f)
        {
            float avgGrade = 0;
            int i = 0;
            foreach (KeyValuePair<string, float> grade in grades)
            {
                float value = grade.Value;
                _percentage += value;
                value *= gradeMultiplier;

                avgGrade += value;

                if (i >= _items.Length)
                    break;

                _items[i].SetData(grade.Key, value.ToString("0.0"));
                _items[i].gameObject.SetActive(true);
                i++;
            }

            for (int j = i; j < _items.Length; j++)
            {
                _items[i].gameObject.SetActive(false);
            }

            avgGrade /= grades.Values.Count;
            _percentage /= grades.Values.Count;
            _finalGrade = avgGrade;
            _avgGradeLabel.text = avgGrade.ToString("0.0");
        }
    }
}
