﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
using I2.Loc;

namespace PWS.Common.ResultsEvaluation
{
    public class StateGradeItem : MonoBehaviour
    {
        [SerializeField]
        private TMP_Text _stateLabel;
        [SerializeField]
        private Localize _stateLocalize;
        [SerializeField]
        private TMP_Text _gradeLabel;
        [SerializeField]
        private Toggle _toggle;

        public void SetData(string stateTerm, string grade, bool isOn = true)
        {
            //_stateLabel.text = state;
            _stateLocalize.SetTerm(stateTerm);
            _gradeLabel.text = grade;
            _toggle.isOn = isOn;
        }
    }
}