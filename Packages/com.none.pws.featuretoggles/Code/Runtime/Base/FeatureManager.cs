using System.Collections.Generic;

namespace PWS.FeatureToggles
{
    public class FeatureManager
    {
        private HashSet<string> _features;
        private Dictionary<string,string> _args;

        private bool Initialized => _features != null;

        public FeatureManager()
        {
            _features = new HashSet<string>();
        }

        public FeatureManager(IEnumerable<string> features,Dictionary<string,string> args) : this()
        {
            foreach (var feature in features)
            {
                EnableFeature(feature);
            }

            _args = args;
        }

        public void EnableFeature(string feature)
        {
            SetFeature(feature, true);
        }

        public void DissableFeature(string feature)
        {
            SetFeature(feature, false);
        }

        public void SetFeature(string feature,bool enabled)
        {
            if (!Initialized) return;

            if (enabled) 
            {
                if (!FeatureEnabled(feature))
                {
                    _features.Add(feature);
                }
            }
            else
            {
                if (FeatureEnabled(feature))
                {
                    _features.Remove(feature);
                }
            }
        }

        public bool FeatureEnabled(string feature)
        {
            if (!Initialized) return false;

            return _features.Contains(feature);
        }

        public string FindArg(string arg)
        {
            if (_args.ContainsKey(arg))
            {
                return _args[arg];
            }
            else return string.Empty;
        }
    }
}
