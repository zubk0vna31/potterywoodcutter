using System.Linq;
using UnityEngine;

namespace PWS.FeatureToggles
{
    public static class FeatureManagerFacade
    {
        //Relative to Resource folder
        //It must exist to succesfully initialize feature manager
        //Even if there are no configuration inside
        private const string _configurationsFolderPath = "Features/Configurations";

        private static FeatureManager _featureManager;
        private static bool _initialized;

        public static FeatureManager FeatureManager
        {
            get
            {
                if (!_initialized)
                {
                    Initialize();
                }

                return _featureManager;
            }
        }

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
        public static void Initialize()
        {
            if (_initialized) return;

            //Load all configuration configs
            ConfigurationConfig [] loadedFiles = 
                Resources.LoadAll<ConfigurationConfig>(_configurationsFolderPath);

            ConfigurationConfig configurationConfig = null;
            int configurationsLoadedAmount = 0;
            int firstConfigurationIndex = -1;

            int index = 0;
            foreach (var file in loadedFiles)
            {
                if (file is ConfigurationConfig)
                {
                    if (firstConfigurationIndex < 0)
                    {
                        firstConfigurationIndex = index;
                    }

                    configurationsLoadedAmount++;

                    if ((file as ConfigurationConfig).ConfigurationEnabled)
                    {
                        configurationConfig = file as ConfigurationConfig;
                        break;
                    }
                }
                index++;
            }

            if(configurationConfig is null)
            {
                if (configurationsLoadedAmount != 0)
                {
                    configurationConfig = loadedFiles[firstConfigurationIndex] as ConfigurationConfig;
                }
            }

            //Init feature manager
            if (configurationConfig is null)
            {
                _featureManager = new FeatureManager();
            }
            else
            {
                _featureManager = new FeatureManager(
                    configurationConfig.FeaturesToggleData.Where(x =>
                    x.Enabled).Select(x => x.FeatureKey),
                    configurationConfig.GetDictionary());
            }

            _initialized = true;
        }

        public static void EnableFeature(string feature)
        {
            FeatureManager.EnableFeature(feature);
        }

        public static void DissableFeature(string feature)
        {
            FeatureManager.DissableFeature(feature);
        }

        public static void SetFeature(string feature, bool enabled)
        {
            FeatureManager.SetFeature(feature, enabled);
        }

        public static bool FeatureEnabled(string feature)
        {
            return FeatureManager.FeatureEnabled(feature);
        }

        public static string FindArg(string arg)
        {
            return FeatureManager.FindArg(arg);
        }
    }
}
