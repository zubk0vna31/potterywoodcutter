using UnityEngine;

namespace PWS.FeatureToggles
{
    [System.Serializable]
    public class FeatureToggleData 
    {
        public string name => FeatureKey;

        public bool Enabled;
        public string FeatureKey;
        public string Description;
    }
}
