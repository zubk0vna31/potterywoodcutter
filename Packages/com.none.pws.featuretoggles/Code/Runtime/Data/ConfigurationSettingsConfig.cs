using UnityEngine;

namespace PWS.FeatureToggles
{
    public class ConfigurationSettingsConfig : ScriptableObject
    {
        public ConfigurationConfig[] ConfigurationConfigs;
    }
}
