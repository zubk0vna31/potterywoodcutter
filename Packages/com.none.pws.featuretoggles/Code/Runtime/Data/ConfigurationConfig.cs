using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace PWS.FeatureToggles
{
    [System.Serializable]
    public class StringKeyValuePair
    {
        public string Key;
        public string Value;
    }

    [CreateAssetMenu(fileName = "Configuration Config",menuName = "PWS/Feature Toggles/Configuration Config")]
    public class ConfigurationConfig : ScriptableObject
    {
        public bool ConfigurationEnabled;
        public List<FeatureToggleData> FeaturesToggleData = new List<FeatureToggleData>();

        public List<StringKeyValuePair> Args = new List<StringKeyValuePair>();

        public Dictionary<string,string> GetDictionary()
        {
            if (Args.Count == 0) return new Dictionary<string, string>();

            return Args.ToDictionary(x => x.Key, y => y.Value);
        }
    }
}
