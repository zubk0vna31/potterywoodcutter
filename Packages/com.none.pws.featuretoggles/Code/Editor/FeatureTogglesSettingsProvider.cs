using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace PWS.FeatureToggles.Editor
{
    public class FeatureTogglesSettingsProvider : SettingsProvider
    {
        //Relative to Resource folder
        private string configurationsFolderPath = "Features/Configurations";
        private string editorConfigurationsFolderPath = "Assets/Resources/Features/Configurations";
        private string skinPath = "Packages/com.none.pws.featuretoggles/Editor Files/Skin.guiskin";

        private List<SerializedObject> serializedObjects;
        private SerializedObject selectedSerializedObject;

        private static bool initialized;

        private string fileName;
        private string featureName;

        private bool featuresSelected=true;

        private int selectedIndex;

        private Vector2 configurationScrollPosition;
        private Vector2 featureScrollPosition;

        private GUISkin skin;

        public FeatureTogglesSettingsProvider(string path, SettingsScope scopes,
            IEnumerable<string> keywords = null) : base(path, scopes, keywords)
        {
        }

        [SettingsProvider]
        public static SettingsProvider CreateMyCustomSettingsProvider()
        {
            var provider = new FeatureTogglesSettingsProvider("Project/Feature Toggles", SettingsScope.Project);

            initialized = false;

            return provider;
        }

        public void Initialize()
        {
            skin = AssetDatabase.LoadAssetAtPath(skinPath, typeof(GUISkin)) as GUISkin;
            initialized = true;
        }


        public override void OnGUI(string searchContext)
        {
            base.OnGUI(searchContext);

            if (!initialized)
            {
                Initialize();
            }

            if (serializedObjects is null)
            {
                LoadSerializedObject();
            }

            DrawSideBar();
        }

        private void LoadSerializedObject()
        {
            Object[] loadedFiles = Resources.LoadAll(configurationsFolderPath);

            serializedObjects = new List<SerializedObject>();

            if (loadedFiles.Length == 0)
            {
                return;
            }

            foreach (var file in loadedFiles)
            {
                if (!(file is ConfigurationConfig))
                {
                    continue;
                }

                serializedObjects.Add(new SerializedObject(file as ConfigurationConfig));
            }

            selectedSerializedObject = null;
        }

        private void DrawSideBar()
        {
            EditorGUILayout.BeginHorizontal();

            EditorGUILayout.BeginVertical(skin.GetStyle("box"), GUILayout.ExpandWidth(false),
                GUILayout.MinWidth(100));

            configurationScrollPosition = EditorGUILayout.BeginScrollView(configurationScrollPosition,
                GUIStyle.none, GUI.skin.verticalScrollbar,
                GUILayout.ExpandHeight(true));

            var rects = new List<Rect>(serializedObjects.Count);

            int i = 0;
            foreach (var s in serializedObjects)
            {
                GUIStyle style = selectedSerializedObject != null && selectedSerializedObject == s ?
                    skin.GetStyle("feature_selected") : skin.GetStyle("feature_normal");

                if (s.FindProperty("ConfigurationEnabled").boolValue)
                {
                    style = skin.GetStyle("feature_active");
                }

                if (GUILayout.Button(s.targetObject.name, style))
                {
                    if (selectedSerializedObject != s)
                    {
                        selectedSerializedObject = s;
                        selectedIndex = i;
                        GUI.FocusControl(null);
                    }
                }

                rects.Add(GUILayoutUtility.GetLastRect());
                i++;
            }


            EditorGUILayout.EndScrollView();

            var rect = GUILayoutUtility.GetLastRect();

            MouseInput(rect, rects);

            fileName = EditorGUILayout.TextField(fileName, skin.GetStyle("textfield"));

            EditorGUILayout.BeginHorizontal();

            if (GUILayout.Button("Add (+)", skin.GetStyle("button"), GUILayout.Height(32)))
            {
                Add();
            }

            if (GUILayout.Button("Refresh (~)", skin.GetStyle("button"), GUILayout.Height(32)))
            {
                Refresh();
            }

            EditorGUILayout.EndHorizontal();

            EditorGUILayout.EndVertical();

            if (!(selectedSerializedObject is null))
            {
                DrawSelected();
            }




            EditorGUILayout.EndHorizontal();
        }

        private void MouseInput(Rect rect, List<Rect> rects)
        {
            if (Event.current.type != EventType.MouseDown || !rect.Contains(Event.current.mousePosition)) return;

            for (int i = 0; i < rects.Count; i++)
            {
                if (rects[i].Contains(Event.current.mousePosition))
                {
                    break;
                }
            }

            selectedSerializedObject = null;

            Repaint();
        }

        private void Add()
        {
            if (string.IsNullOrEmpty(fileName))
            {
                Debug.LogWarning("File name is empty or null");
                return;
            }

            string finalPath = editorConfigurationsFolderPath + "/" + fileName + ".asset";

            if (AssetDatabase.LoadAssetAtPath(
                finalPath, typeof(ConfigurationConfig)) != null)
            {
                bool dialogResult = EditorUtility.DisplayDialog("Creating Config",
                    "Configuration config with such name already exists, do you want to overwrite it?",
                    "Overwrite", "Cancel");

                if (dialogResult)
                {
                    AssetDatabase.DeleteAsset(finalPath);
                }
                else
                {
                    return;
                }
            }

            ConfigurationConfig configurationConfig = ScriptableObject.CreateInstance<ConfigurationConfig>();

            AssetDatabase.CreateAsset(configurationConfig, finalPath);
            AssetDatabase.Refresh();

            EditorUtility.SetDirty(configurationConfig);

            Refresh();
            Repaint();
        }

        private void Remove()
        {
            var fileName = selectedSerializedObject.targetObject.name;

            serializedObjects.RemoveAt(selectedIndex);

            AssetDatabase.DeleteAsset(editorConfigurationsFolderPath + "/" + fileName + ".asset");
            AssetDatabase.Refresh();
            Repaint();
        }

        private void Refresh()
        {
            LoadSerializedObject();
            Repaint();
        }

        private void Activate()
        {
            for (int i = 0; i < serializedObjects.Count; i++)
            {
                serializedObjects[i].FindProperty("ConfigurationEnabled").boolValue = serializedObjects[i] == selectedSerializedObject;
                serializedObjects[i].ApplyModifiedProperties();
            }

            Repaint();
        }

        private void AddFeature()
        {
            if (string.IsNullOrEmpty(featureName))
            {
                Debug.LogWarning("Feature key is empty or null");
                return;
            }

            var arrayProp = selectedSerializedObject.FindProperty("FeaturesToggleData");
            arrayProp.arraySize = arrayProp.arraySize + 1;

            arrayProp.GetArrayElementAtIndex(arrayProp.arraySize - 1).FindPropertyRelative("FeatureKey").stringValue = featureName;
            arrayProp.GetArrayElementAtIndex(arrayProp.arraySize - 1).FindPropertyRelative("Description").stringValue = "";

            featureName = "";

            selectedSerializedObject.ApplyModifiedProperties();
            Repaint();

        }

        public void DrawProperties(SerializedProperty prop, bool drawChildren, string displayName = "")
        {
            string lastPropPath = string.Empty;
            bool overrideDisplayName = !string.IsNullOrEmpty(displayName);

            if (prop.hasChildren)
            {
                foreach (SerializedProperty p in prop)
                {
                    if (p.isArray && p.propertyType == SerializedPropertyType.Generic)
                    {
                        EditorGUILayout.BeginHorizontal();
                        p.isExpanded = EditorGUILayout.Foldout(p.isExpanded,
                            overrideDisplayName ? displayName : p.displayName);
                        EditorGUILayout.EndHorizontal();

                        if (p.isExpanded)
                        {
                            EditorGUI.indentLevel++;
                            DrawProperties(p, drawChildren);
                            EditorGUI.indentLevel--;
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(lastPropPath) && p.propertyPath.Contains(lastPropPath)) { continue; }
                        lastPropPath = p.propertyPath;
                        EditorGUILayout.PropertyField(p, drawChildren);
                    }
                }
            }
            else
            {
                EditorGUILayout.PropertyField(prop, drawChildren);
            }
        }

        private void DrawSelected()
        {
            EditorGUILayout.BeginVertical(skin.GetStyle("box"));

            EditorGUILayout.BeginHorizontal(GUILayout.Height(30));

            if (GUILayout.Button("Features", skin.GetStyle("feature_normal"), GUILayout.ExpandHeight(true)))
            {
                featuresSelected = true;
            }

            if (GUILayout.Button("Args", skin.GetStyle("feature_normal"), GUILayout.ExpandHeight(true)))
            {
                featuresSelected = false;
            }

            EditorGUILayout.EndHorizontal();

            if (featuresSelected)
            {
                DrawFeatures();
            }
            else
            {
                DrawArgs();
            }

            EditorGUILayout.BeginHorizontal();

            float height = 32;

            featureName = EditorGUILayout.TextField(featureName,
                skin.GetStyle("textfield_centered"), GUILayout.Width(128));

            if (GUILayout.Button("Add Feature (+)", skin.GetStyle("button"), GUILayout.Height(height)))
            {
                AddFeature();
            }

            if (GUILayout.Button("Activate (~)", skin.GetStyle("button"), GUILayout.Height(height)))
            {
                Activate();
            }


            if (GUILayout.Button("Remove (-)", skin.GetStyle("button"), GUILayout.Height(height)))
            {
                Remove();
            }

            EditorGUILayout.EndHorizontal();

            EditorGUILayout.EndVertical();
        }

        private void DrawFeatures()
        {
            featureScrollPosition = EditorGUILayout.BeginScrollView(featureScrollPosition,
                            GUIStyle.none, GUI.skin.verticalScrollbar);

            var arrayProperty = selectedSerializedObject.FindProperty("FeaturesToggleData");

            for (int i = 0; i < arrayProperty.arraySize; i++)
            {
                if (DrawFeature(arrayProperty.GetArrayElementAtIndex(i)))
                {
                    arrayProperty.MoveArrayElement(i, arrayProperty.arraySize - 1);
                    arrayProperty.arraySize = arrayProperty.arraySize - 1;
                    selectedSerializedObject.ApplyModifiedProperties();
                    Repaint();
                }
            }

            selectedSerializedObject.ApplyModifiedProperties();

            EditorGUILayout.EndScrollView();
        }

        private void DrawArgs()
        {
            featureScrollPosition = EditorGUILayout.BeginScrollView(featureScrollPosition,
                            GUIStyle.none, GUI.skin.verticalScrollbar);

            DrawDictionary();
            
            selectedSerializedObject.ApplyModifiedProperties();

            EditorGUILayout.EndScrollView();
        }

        private bool DrawFeature(SerializedProperty prop)
        {
            EditorGUILayout.BeginVertical(skin.GetStyle("feature_normal"));

            var enabledProp = prop.FindPropertyRelative("Enabled");
            var featureKeyProp = prop.FindPropertyRelative("FeatureKey");
            var featureDescProp = prop.FindPropertyRelative("Description");

            //Featuer label
            var rect = EditorGUILayout.BeginHorizontal();

            EditorGUILayout.LabelField(string.IsNullOrEmpty(featureKeyProp.stringValue) ?
                "Feature key is empty" : featureKeyProp.stringValue,
                skin.GetStyle("feature_label"));

            if (GUILayout.Button("", skin.GetStyle("closeButton")))
            {
                return true;
            }

            EditorGUILayout.EndHorizontal();


            //Enable property
            EditorGUILayout.BeginHorizontal(GUILayout.ExpandWidth(true));

            EditorGUILayout.LabelField(enabledProp.displayName, skin.GetStyle("property_label"),
                GUILayout.Width(128));
            GUILayout.FlexibleSpace();
            enabledProp.boolValue = EditorGUILayout.Toggle(enabledProp.boolValue, GUILayout.Width(16));

            EditorGUILayout.EndHorizontal();


            //Feature key property
            EditorGUILayout.BeginHorizontal();

            EditorGUILayout.LabelField(featureKeyProp.displayName, skin.GetStyle("property_label"),
                GUILayout.ExpandWidth(false), GUILayout.Width(128));
            featureKeyProp.stringValue = EditorGUILayout.TextField
                (featureKeyProp.stringValue, skin.GetStyle("textfield"),
                GUILayout.ExpandWidth(true));


            EditorGUILayout.EndHorizontal();

            //Feature description property
            GUILayout.Space(4);

            EditorGUILayout.BeginHorizontal();

            EditorGUILayout.LabelField(featureDescProp.displayName, skin.GetStyle("property_label"),
                GUILayout.ExpandWidth(false), GUILayout.Width(128));

            featureDescProp.stringValue = EditorGUILayout.TextArea(featureDescProp.stringValue,
                skin.GetStyle("textarea"), GUILayout.ExpandWidth(true), GUILayout.MinWidth(244));

            EditorGUILayout.EndHorizontal();



            EditorGUILayout.EndVertical();

            return false;
        }

        private void DrawDictionary()
        {
            var obj = serializedObjects[selectedIndex];

            var keys = obj.FindProperty("Args");

            EditorGUILayout.BeginVertical(GUI.skin.box);

            EditorGUILayout.BeginHorizontal(GUI.skin.GetStyle("helpBox"));

            EditorGUILayout.LabelField("Configuration Args", GUILayout.ExpandWidth(true),
                GUILayout.Height(24), GUILayout.ExpandHeight(false));

            EditorGUILayout.EndHorizontal();

            for (int i = 0; i < keys.arraySize; i++)
            {
                var prop = keys.GetArrayElementAtIndex(i);

                EditorGUILayout.BeginHorizontal(GUI.skin.button);

                EditorGUILayout.LabelField("Key", GUILayout.Width(30));
                prop.FindPropertyRelative("Key").stringValue = EditorGUILayout.TextField(
                    prop.FindPropertyRelative("Key").stringValue);

                EditorGUILayout.LabelField("Value", GUILayout.Width(50));
                prop.FindPropertyRelative("Value").stringValue = EditorGUILayout.TextField(
                     prop.FindPropertyRelative("Value").stringValue);

                EditorGUILayout.EndHorizontal();
            }

            EditorGUILayout.BeginHorizontal();

            GUILayout.FlexibleSpace();

            float buttonsWidth = 38;
            float buttonsHeight = 24;

            if (GUILayout.Button("+", GUILayout.Width(buttonsWidth), GUILayout.Height(buttonsHeight)))
            {
                keys.arraySize++;
            }

            if (GUILayout.Button("-", GUILayout.Width(buttonsWidth), GUILayout.Height(buttonsHeight)))
            {
                if (keys.arraySize > 0)
                {
                    keys.arraySize--;
                }
            }

            EditorGUILayout.EndHorizontal();

            EditorGUILayout.EndVertical();
        }
    }
}
