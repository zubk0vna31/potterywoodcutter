using System.Collections.Generic;
using Leopotam.Ecs;
using Modules.Root.ContainerComponentModel;
using Modules.Root.ECS;
using Modules.StateGroup.Core;
using Modules.Audio;
using PWS.Common.Messages;
using PWS.Common.RestartStage;
using PWS.Common.UI;
using PWS.Pottery.Common;
using PWS.Pottery.ItemDataHolderService;
using PWS.Common.Audio;
using UnityEngine;
using PWS.Common.ResultsEvaluation;
using PWS.Common.TravelPoints;
using PWS.Features.Achievements;

namespace PWS.Pottery.Stages.ClayPreparationStage
{
    public class PotteryClayPreparationSystemsProvider : MonoBehaviour, ISystemsProvider
    {
        internal class SystemsDependencies
        {
            [Inject] public IClayPreparationDataHolder ClayData;
            [Inject] public IResultsEvaluationDataHolder ResultsEvaluationData;
            [Inject] public IAchievementsService AchievementsService;
        }

        [SerializeField] private SetStateSOCommand _nextState;
        [SerializeField] private ClayPreparationStateConfig _config;
        [SerializeField] private StateInfoData _stateInfo;

        [Header("One Shot Audio Source Prefabs")] 
        [SerializeField] private List<OneShotSoundTemplate> _touchWaterPrefabs;
        
        [Header("Voice config")] 
        [SerializeField] private VoiceConfig _voiceConfig;
        

        public EcsSystems GetSystems(EcsWorld world, EcsSystems endFrame, EcsSystems mainSystems)
        {
            SystemsDependencies dependencies = new SystemsDependencies();
            SceneContainer.Instance.Inject(dependencies);

            EcsSystems systems = new EcsSystems(world);
            systems
                .Add(new TransitToStateOnUIActionSystem<ClayPreparationState>(_nextState))
                .Add(new ClayPreparationSkipProcessing(dependencies.ClayData, _nextState))

                //restart
                .Add(new CurrentStateRestoreDataProcessing<ClayPreparationState, ClayPreparationDataHolder>(dependencies.ClayData))

                //audio
                .Add(new ClayPreparationAudioSystem(_touchWaterPrefabs))

                //state systems
                .Add(new GameObjectVisibilityProcessing<ClayPreparationState, ClayPreparationContainer>())
                .Add(new GameObjectVisibilityProcessing<ClayPreparationState, ClayCubeContainer>())
                .Add(new ClayPreparationDeformablePlacementProcessing())
                .Add(new ClayPreparationInteractorsHandler(dependencies.ClayData, _config,dependencies.AchievementsService))
                .Add(new ClayPreparationCompletionTracker(dependencies.ClayData, _config))
                .Add(new ClayPreparationWetnessProcessing(dependencies.ClayData))

                .Add(new SetGradeByProgress<ClayPreparationState>(dependencies.ResultsEvaluationData, _stateInfo))

                //ui
                .Add(new StateInfoWindowUIProcessing<ClayPreparationState>(_stateInfo))

                .Add(new StateWindowPlacementProcessing<ClayPreparationState>("Table"))
                .Add(new ClayPreparationProgressUIProcessing(dependencies.ClayData))
                .Add(new StateWindowProgressUIProcessing<ClayPreparationState>())
                .Add(new StateWindowButtonUIProcessing<ClayPreparationState>())

                .Add(new SkipStageButtonUIProcessing<ClayPreparationState>())
                .Add(new ClayPreparationRestartWhenStuckUIProcoessing(dependencies.ClayData, _config))
                
                //voice
                .Add(new VoiceAudioSystem<ClayPreparationState>(_voiceConfig))

                //travel point
                .Add(new TravelPointPositioningSystem<ClayPreparationState>("Start"))
                
                //ghost hands
                .Add(new ClayPreparationGhostHandsSystem())

                ;

            endFrame
                .OneFrame<RestartCurrentStepMessage>()
                .OneFrame<NextStateSignal>()
                .OneFrame<SkipStageMessage>()
                .OneFrame<ClayDeformerTriggerSignal>()
                .OneFrame<ClayDeformerWaterTriggerSignal>()
                ;

            return systems;
        }
    }
}
