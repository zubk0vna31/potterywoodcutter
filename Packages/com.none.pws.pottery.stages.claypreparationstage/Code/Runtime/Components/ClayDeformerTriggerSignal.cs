using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace PWS.Pottery.Stages.ClayPreparationStage
{
    public struct ClayDeformerTriggerSignal
    {
        public Vector3 Position;
    }
}
