using UnityEngine.XR.Interaction.Toolkit;

namespace PWS.Pottery.Stages.ClayPreparationStage
{
    public struct ClayDeformable
    {
        public ImpactDeformable View;
    }
}
