using UnityEngine.UI;

namespace PWS.Pottery.Stages.ClayPreparationStage
{
    public struct BalanceCompletionSlider
    {
        public Slider Slider;
    }
}
