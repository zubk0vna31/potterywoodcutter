﻿using UnityEngine;

namespace PWS.Pottery.Stages.ClayPreparationStage
{
    public struct ClayPreparationGhostHandsComponent
    {
        public GameObject GhostHandsParent;
        public float Timer;
        public float TimerTreshold;
        public bool IsActive;
    }
}