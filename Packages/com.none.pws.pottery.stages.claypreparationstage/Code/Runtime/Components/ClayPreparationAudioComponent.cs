﻿using PWS.Common.Audio;

namespace PWS.Pottery.Stages.ClayPreparationStage
{
    public struct ClayPreparationAudioComponent
    {
        public RandomSoundClipsData SculptClayClips;
        public RandomSoundClipsData WettingClayClips;
    }
}