using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace PWS.Pottery.Stages.ClayPreparationStage
{
    public struct ClayDeformer
    {
        public XRBaseInteractor Interactor;
        public Vector3 OldPosition;
        public Vector3 Velocity;
        public bool Wet;
    }
}
