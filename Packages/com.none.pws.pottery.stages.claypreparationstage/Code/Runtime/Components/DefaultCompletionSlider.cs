using UnityEngine.UI;

namespace PWS.Pottery.Stages.ClayPreparationStage
{
    public struct DefaultCompletionSlider
    {
        public Slider Slider;
    }
}
