﻿using Leopotam.Ecs;
using Modules.ViewHub;
using PWS.Common.Audio;
using UnityEngine;

namespace PWS.Pottery.Stages.ClayPreparationStage
{
    public class ClayPreparationAudioViewComponent : ViewComponent
    {
        [SerializeField] private RandomSoundClipsData _sculptClayClips;
        [SerializeField] private RandomSoundClipsData _wettingClayClips;
        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            ref var entity = ref ecsEntity.Get<ClayPreparationAudioComponent>();
            entity.SculptClayClips = _sculptClayClips;
            entity.WettingClayClips = _wettingClayClips;
        }
    }
}