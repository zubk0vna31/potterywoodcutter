using Leopotam.Ecs;
using Modules.ViewHub;
using PWS.Common.Messages;
using UniRx;
using UnityEngine.UI;

namespace PWS.Pottery.Stages.ClayPreparationStage
{
    public class RestartWhenStuckView : ViewComponent
    {
        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            ecsEntity.Get<RestartWhenStuck>();
            GetComponent<Button>().onClick.AddListener(OnClickRestartCurrent);
        }

        void OnClickRestartCurrent()
        {
            MessageBroker.Default.Publish(new RestartCurrentStepMessage());
        }
    }
}
