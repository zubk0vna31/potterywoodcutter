﻿using Leopotam.Ecs;
using Modules.ViewHub;
using UnityEngine;

namespace PWS.Pottery.Stages.ClayPreparationStage
{
    public class ClayPreparationGhostHandsView : ViewComponent
    {
        [SerializeField] private GameObject ghostHandsParent;
        [SerializeField] private float _timerTreshold;
        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            ref var entity = ref ecsEntity.Get<ClayPreparationGhostHandsComponent>();
            entity.GhostHandsParent = ghostHandsParent;
            entity.TimerTreshold = _timerTreshold;
        }
    }
}