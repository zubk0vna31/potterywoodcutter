using Leopotam.Ecs;
using Modules.ViewHub;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace PWS.Pottery.Stages.ClayPreparationStage
{
    public class ClayDeformerView : ViewComponent
    {
        const string PRODUCT_TAG = "ClayPreparationProduct";
        const string WATER_TAG = "Water";

        private EcsEntity _ecsEntity;

        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            _ecsEntity = ecsEntity;

            ref ClayDeformer clayDeformer = ref ecsEntity.Get<ClayDeformer>();
            clayDeformer.Interactor = GetComponentInParent<XRBaseInteractor>();
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.tag.Equals(PRODUCT_TAG))
            {
                ref ClayDeformerTriggerSignal signal = ref _ecsEntity.Get<ClayDeformerTriggerSignal>();
                signal.Position = transform.position;
            }
            else if (other.tag.Equals(WATER_TAG))
            {
                _ecsEntity.Get<ClayDeformerWaterTriggerSignal>();
            }
        }
    }
}
