using Leopotam.Ecs;
using Modules.ViewHub;

namespace PWS.Pottery.Stages.ClayPreparationStage
{
    public class ClayDeformableView : ViewComponent
    {
        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            ecsEntity.Get<ClayDeformable>().View = GetComponent<ImpactDeformable>();
        }
    }
}
