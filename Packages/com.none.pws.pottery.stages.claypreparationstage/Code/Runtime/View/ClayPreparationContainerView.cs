using Leopotam.Ecs;
using Modules.ViewHub;

namespace PWS.Pottery.Stages.ClayPreparationStage
{
    public class ClayPreparationContainerView : ViewComponent
    {
        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            ecsEntity.Get<ClayPreparationContainer>();
        }
    }
}
