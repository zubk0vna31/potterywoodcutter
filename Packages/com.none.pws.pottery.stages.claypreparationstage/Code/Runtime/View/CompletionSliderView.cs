using Leopotam.Ecs;
using Modules.ViewHub;
using UnityEngine;
using UnityEngine.UI;

namespace PWS.Pottery.Stages.ClayPreparationStage
{
    public enum SliderType {
        Default,
        Balance
    }

    public class CompletionSliderView : ViewComponent
    {
        [SerializeField]
        private SliderType _type;

        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            if(_type == SliderType.Default)
            {
                ecsEntity.Get<DefaultCompletionSlider>().Slider = GetComponent<Slider>();
            } else if (_type == SliderType.Balance)
            {
                ecsEntity.Get<BalanceCompletionSlider>().Slider = GetComponent<Slider>();
            }
        }
    }
}
