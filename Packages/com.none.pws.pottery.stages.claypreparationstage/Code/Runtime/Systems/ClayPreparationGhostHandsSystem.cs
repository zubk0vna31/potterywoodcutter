﻿using Leopotam.Ecs;
using Modules.StateGroup.Components;
using Modules.Utils;
using PWS.Common.UI;

namespace PWS.Pottery.Stages.ClayPreparationStage
{
    public class ClayPreparationGhostHandsSystem : IEcsRunSystem
    {
        private readonly EcsFilter<StateEnter, ClayPreparationState> _enterState;
        private readonly EcsFilter<ClayPreparationState> _inState;
        private readonly EcsFilter<StateExit, ClayPreparationState> _exitState;
        private readonly EcsFilter<ClayPreparationGhostHandsComponent> _ghostHands;
        private readonly EcsFilter<ClayDeformerTriggerSignal> _triggerSignal;
        private readonly EcsFilter<ClayDeformerWaterTriggerSignal> _waterTriggerSignal;
        private readonly EcsFilter<StateWindow ,StateProgress> _stateProgress;
        private readonly TimeService _timeService;

        public void Run()
        {
            if (!_enterState.IsEmpty())
            {
                foreach (var ghostHand in _ghostHands)
                {
                    ref var hands = ref _ghostHands.Get1(ghostHand);
                    hands.Timer = 0;
                    hands.GhostHandsParent.SetActive(true);
                }
            }

            if (!_inState.IsEmpty())
            {
                foreach (var ghostHand in _ghostHands)
                {
                    ref var hands = ref _ghostHands.Get1(ghostHand);
                    hands.Timer += _timeService.DeltaTime;
                    
                    foreach (var idx in _stateProgress)
                    {
                        ref var progress = ref _stateProgress.Get2(idx);
                        
                        if (progress.Value > progress.SkipThreshold)
                        {
                            hands.GhostHandsParent.SetActive(false);
                            return;
                        }
                    }
                    
                    if (!hands.IsActive && _triggerSignal.IsEmpty() && _waterTriggerSignal.IsEmpty() && hands.Timer > hands.TimerTreshold)
                    {
                        hands.GhostHandsParent.SetActive(true);
                        hands.IsActive = true;
                    }

                    else if (!_triggerSignal.IsEmpty() || !_waterTriggerSignal.IsEmpty())
                    {
                        hands.GhostHandsParent.SetActive(false);
                        hands.IsActive = false;
                        hands.Timer = 0;
                    }
                }
            }
            
            if (!_exitState.IsEmpty())
            {
                foreach (var ghostHand in _ghostHands)
                {
                    _ghostHands.Get1(ghostHand).GhostHandsParent.SetActive(false);
                }
            }
        }
    }
}