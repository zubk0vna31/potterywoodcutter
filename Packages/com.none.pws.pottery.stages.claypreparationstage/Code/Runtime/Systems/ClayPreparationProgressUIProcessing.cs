using Leopotam.Ecs;
using Modules.StateGroup.Components;
using PWS.Common.UI;
using PWS.Pottery.ItemDataHolderService;
using UnityEngine;

namespace PWS.Pottery.Stages.ClayPreparationStage
{
    public class ClayPreparationProgressUIProcessing : IEcsRunSystem
    {
        // auto injected fields
        readonly EcsFilter<StateEnter, ClayPreparationState> _entering;
        readonly EcsFilter<ClayPreparationState> _inState;

        readonly EcsFilter<DefaultCompletionSlider, ClayPreparationContainer> _crumpleSlider;
        readonly EcsFilter<BalanceCompletionSlider, ClayPreparationContainer> _saturationSlider;

        private IClayPreparationDataHolder _data;

        public ClayPreparationProgressUIProcessing(IClayPreparationDataHolder data)
        {
            _data = data;
        }

        public void Run()
        {
            if (!_entering.IsEmpty())
            {
                foreach (var i in _crumpleSlider)
                {
                    _crumpleSlider.Get1(i).Slider.value = 0;
                }
                foreach (var i in _saturationSlider)
                {
                    _saturationSlider.Get1(i).Slider.value = 0;
                }
            }

            if (!_inState.IsEmpty())
            {
                float crumpleCompletion = Mathf.Min(_data.Crumple, 1);
                float saturationCompletion = Mathf.Min(_data.Saturation, 2) / 2;

                foreach (var i in _crumpleSlider)
                {
                    _crumpleSlider.Get1(i).Slider.value = crumpleCompletion;
                }
                foreach (var i in _saturationSlider)
                {
                    _saturationSlider.Get1(i).Slider.value = saturationCompletion;
                }
            }
        }
    }
}
