using Leopotam.Ecs;
using Modules.StateGroup.Core;
using PWS.Common.Messages;
using PWS.Pottery.Common;
using PWS.Pottery.ItemDataHolderService;
using UnityEngine;

namespace PWS.Pottery.Stages.ClayPreparationStage
{
    public class ClayPreparationSkipProcessing : IEcsRunSystem
    {
        // auto injected fields
        private readonly EcsFilter<SkipStageMessage> _skipPerformed;
        readonly EcsFilter<ClayPreparationState> _inState;
        private readonly StateFactory _stateFactory;

        private IClayPreparationDataHolder _data;
        private SetStateSOCommand _nextState;

        public ClayPreparationSkipProcessing(IClayPreparationDataHolder data, SetStateSOCommand nextState)
        {
            _data = data;
            _nextState = nextState;
        }

        public void Run()
        {
            if (_inState.IsEmpty())
                return;
            if (_skipPerformed.IsEmpty())
                return;

            _data.Crumple = 1;
            _data.Saturation = 1;
            _data.SetDirty();

            /*foreach (var i in _skipPerformed)
            {
                _skipPerformed.GetEntity(i).Del<SkipStageMessage>();
            }*/

            _nextState.Execute(_stateFactory);
        }
    }
}
