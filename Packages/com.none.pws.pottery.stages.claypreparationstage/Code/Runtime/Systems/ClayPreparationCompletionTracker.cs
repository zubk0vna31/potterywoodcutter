using Leopotam.Ecs;
using Modules.StateGroup.Components;
using PWS.Common.Messages;
using PWS.Common.UI;
using PWS.Pottery.ItemDataHolderService;
using UnityEngine;

namespace PWS.Pottery.Stages.ClayPreparationStage
{
    public class ClayPreparationCompletionTracker : IEcsRunSystem
    {
        // auto injected fields
        readonly EcsFilter<StateEnter, ClayPreparationState> _entering;
        readonly EcsFilter<ClayPreparationState>.Exclude<StateExit> _inState;
        private readonly EcsFilter<SkipStageMessage> _skipPerformed;

        readonly EcsFilter<StateWindow, StateProgress> _stateWindowWithProgress;

        private IClayPreparationDataHolder _data;
        private ClayPreparationStateConfig _config;

        public ClayPreparationCompletionTracker(IClayPreparationDataHolder data, ClayPreparationStateConfig config)
        {
            _data = data;
            _config = config;
        }

        public void Run()
        {
            if (!_entering.IsEmpty())
            {
                foreach (var i in _stateWindowWithProgress)
                {
                    _stateWindowWithProgress.Get2(i).Value = 0;
                    _stateWindowWithProgress.Get2(i).CompletionThreshold = _config.CompletionThreshold;
                    _stateWindowWithProgress.Get2(i).SkipThreshold = _config.SkipThreshold;

                    _data.Crumple = 0;
                    _data.Saturation = 0;
                }
            }

            if (!_inState.IsEmpty())
            {
                if (!_skipPerformed.IsEmpty())
                    return;

                float crumpleCompletion = Mathf.Min(_data.Crumple, 1);
                float saturationCompletion = Mathf.PingPong(Mathf.Min(_data.Saturation, 2), 1);
                float completion = Mathf.Lerp(crumpleCompletion, saturationCompletion, 0.5f);

                foreach (var i in _stateWindowWithProgress)
                {
                    _stateWindowWithProgress.Get2(i).Value = completion;
                    //Debug.Log("Progress: " + completion);
                }
            }
        }
    }
}
