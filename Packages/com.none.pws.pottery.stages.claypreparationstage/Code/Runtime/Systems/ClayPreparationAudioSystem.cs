﻿using System.Collections.Generic;
using Leopotam.Ecs;
using Modules.Audio;
using UnityEngine;

namespace PWS.Pottery.Stages.ClayPreparationStage
{
    public class ClayPreparationAudioSystem : IEcsRunSystem
    {
        private readonly EcsFilter<ClayDeformable, AudioSourceRef, ClayPreparationAudioComponent> _deformable;
        private readonly EcsFilter<ClayDeformer, ClayDeformerTriggerSignal> _triggerSignal;
        private readonly EcsFilter<ClayDeformer, ClayDeformerWaterTriggerSignal> _waterTriggerSignal;

        private List<OneShotSoundTemplate> _touchWaterPrefabs;

        public ClayPreparationAudioSystem(List<OneShotSoundTemplate> touchWaterPrefabs)
        {
            _touchWaterPrefabs = touchWaterPrefabs;
        }
        
        public void Run()
        {
            if (!_triggerSignal.IsEmpty())
            {
                foreach (var triggerSignal in _triggerSignal)
                {
                    foreach (var deformable in _deformable)
                    {
                        ref var sourceRef = ref _deformable.Get2(deformable);
                        
                        if (sourceRef.AudioSource.isPlaying)
                            return;
                        
                        var wetDryClips = _triggerSignal.Get1(triggerSignal).Wet
                            ? _deformable.Get3(deformable).WettingClayClips.Clips
                            : _deformable.Get3(deformable).SculptClayClips.Clips;
                        
                        ref var signal = ref _deformable.GetEntity(deformable).Get<PlayGlobalAuidioSignal>();
                        var idx = Random.Range(0, wetDryClips.Count);
                        signal.AudioSource = sourceRef.AudioSource;
                        signal.AudioClip = wetDryClips[idx];
                    }
                }
            }

            if (!_waterTriggerSignal.IsEmpty())
            {
                foreach (var waterTrigger in _waterTriggerSignal)
                {
                    ref var signal = ref _waterTriggerSignal.GetEntity(waterTrigger).Get<PlaySoundAtPointSignal>();
                    var idx = Random.Range(0, _touchWaterPrefabs.Count);
                    signal.PlayPosition = _waterTriggerSignal.Get1(waterTrigger).Interactor.transform.position;
                    signal.SoundPrefab = _touchWaterPrefabs[idx];
                }
            }
        }
    }
}