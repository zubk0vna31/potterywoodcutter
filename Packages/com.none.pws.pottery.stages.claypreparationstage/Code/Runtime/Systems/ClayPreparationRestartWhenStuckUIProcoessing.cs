using Leopotam.Ecs;
using Modules.StateGroup.Components;
using Modules.ViewHub;
using PWS.Common.Messages;
using PWS.Common.UI;
using PWS.Pottery.ItemDataHolderService;
using UnityEngine;

namespace PWS.Pottery.Stages.ClayPreparationStage
{
    public class ClayPreparationRestartWhenStuckUIProcoessing : IEcsRunSystem
    {
        // auto injected fields
        readonly EcsFilter<StateEnter, ClayPreparationState> _entering;
        readonly EcsFilter<ClayPreparationState>.Exclude<StateExit> _inState;
        private readonly EcsFilter<SkipStageMessage> _skipPerformed;

        readonly EcsFilter<UnityView, ClayPreparationContainer, RestartWhenStuck> _restartButton;

        private IClayPreparationDataHolder _data;
        private ClayPreparationStateConfig _config;

        public ClayPreparationRestartWhenStuckUIProcoessing(IClayPreparationDataHolder data, ClayPreparationStateConfig config)
        {
            _data = data;
            _config = config;
        }

        public void Run()
        {
            if (!_inState.IsEmpty())
            {
                if (!_skipPerformed.IsEmpty())
                    return;

                foreach (var i in _restartButton)
                {
                    float crumpleCompletion = Mathf.Min(_data.Crumple, 1);
                    float saturationCompletion = Mathf.PingPong(Mathf.Min(_data.Saturation, 2), 1);
                    float completion = Mathf.Lerp(crumpleCompletion, saturationCompletion, 0.5f);

                    bool show = crumpleCompletion >= 1 && completion < _config.SkipThreshold && _data.Saturation > 1;
                    _restartButton.Get1(i).GameObject.SetActive(show);
                }
            }
        }
    }
}
