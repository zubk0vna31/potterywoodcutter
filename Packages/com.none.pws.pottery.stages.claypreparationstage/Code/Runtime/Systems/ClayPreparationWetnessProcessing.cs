using Leopotam.Ecs;
using Modules.StateGroup.Components;
using PWS.Common.UI;
using PWS.Pottery.Common;
using PWS.Pottery.ItemDataHolderService;
using UnityEngine;

namespace PWS.Pottery.Stages.ClayPreparationStage
{
    public class ClayPreparationWetnessProcessing : IEcsRunSystem
    {
        // auto injected fields
        readonly EcsFilter<ClayPreparationState> _inState;
        readonly EcsFilter<ClayDeformable, ClayMeshRenderer> _deformable;

        private IClayPreparationDataHolder _data;

        public ClayPreparationWetnessProcessing(IClayPreparationDataHolder data)
        {
            _data = data;
        }

        public void Run()
        {
            if (!_inState.IsEmpty())
            {
                if (_data.IsUpdated)
                {
                    float saturationCompletion = Mathf.Min(_data.Saturation, 2) / 2;

                    foreach (var i in _deformable)
                    {
                        _deformable.Get2(i).MeshRenderer.sharedMaterial.SetFloat("_Wetness", saturationCompletion);
                        _data.IsUpdated = false;
                    }
                }
            }
        }
    }
}
