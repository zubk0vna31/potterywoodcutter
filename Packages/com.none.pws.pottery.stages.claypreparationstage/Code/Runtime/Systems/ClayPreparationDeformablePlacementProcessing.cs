using Leopotam.Ecs;
using Modules.StateGroup.Components;
using Modules.ViewHub;
using PWS.Pottery.ItemDataHolderService;
using UnityEngine;

namespace PWS.Pottery.Stages.ClayPreparationStage
{
    public class ClayPreparationDeformablePlacementProcessing : IEcsRunSystem, IEcsInitSystem
    {
        // auto injected fields
        readonly EcsFilter<StateEnter, ClayPreparationState> _entering;
        readonly EcsFilter<ClayDeformable, UnityView> _deformable;

        //runtime data
        private Vector3 _initPos;
        private Quaternion _initRot;

        public void Init()
        {
            foreach (var deformable in _deformable)
            {
                _initPos = _deformable.Get2(deformable).Transform.parent.position;
                _initRot = _deformable.Get2(deformable).Transform.parent.rotation;
            }
        }

        public void Run()
        {
            if (!_entering.IsEmpty())
            {
                foreach (var deformable in _deformable)
                {
                    _deformable.Get1(deformable).View.Repair(1);
                    _deformable.Get1(deformable).View.GetComponentInParent<Rigidbody>().velocity = Vector3.zero;
                    _deformable.Get2(deformable).Transform.parent.position = _initPos;
                    _deformable.Get2(deformable).Transform.parent.rotation = _initRot;
                }
            }
        }
    }
}
