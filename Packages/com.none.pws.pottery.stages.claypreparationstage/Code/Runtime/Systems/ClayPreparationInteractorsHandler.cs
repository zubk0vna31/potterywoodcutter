using Leopotam.Ecs;
using Modules.StateGroup.Components;
using Modules.ViewHub;
using PWS.Pottery.ItemDataHolderService;
using UnityEngine;
using DG.Tweening;
using PWS.Features.Achievements;

namespace PWS.Pottery.Stages.ClayPreparationStage
{
    public class ClayPreparationInteractorsHandler : IEcsRunSystem
    {
        // auto injected fields
        readonly EcsFilter<StateEnter, ClayPreparationState> _entering;
        readonly EcsFilter<StateExit, ClayPreparationState> _exiting;
        readonly EcsFilter<ClayPreparationState> _inState;

        readonly EcsFilter<ClayDeformer, UnityView> _deformer;
        readonly EcsFilter<ClayDeformable> _deformable;
        readonly EcsFilter<ClayDeformer, ClayDeformerTriggerSignal> _triggerSignal;
        readonly EcsFilter<ClayDeformer, ClayDeformerWaterTriggerSignal> _waterTriggerSignal;

        private IClayPreparationDataHolder _data;
        private IAchievementsService _achievementsService;
        private ClayPreparationStateConfig _config;

        public ClayPreparationInteractorsHandler(IClayPreparationDataHolder data, ClayPreparationStateConfig config,
            IAchievementsService achievementsService)
        {
            _data = data;
            _config = config;
            _achievementsService = achievementsService;
        }

        public void Run()
        {
            if (!_entering.IsEmpty())
            {
                foreach (var deformer in _deformer)
                {
                    _deformer.Get1(deformer).OldPosition = _deformer.Get2(deformer).Transform.position;
                    _deformer.Get1(deformer).Velocity = Vector3.zero;
                }
            }

            if (_inState.IsEmpty())
                return;

            foreach (var deformer in _deformer)
            {
                bool isSelectActive = _deformer.Get1(deformer).Interactor.isSelectActive;
                _deformer.Get2(deformer).GameObject.SetActive(!isSelectActive);

                _deformer.Get1(deformer).Velocity = (_deformer.Get2(deformer).Transform.position - _deformer.Get1(deformer).OldPosition) / Time.deltaTime;
                _deformer.Get1(deformer).OldPosition = _deformer.Get2(deformer).Transform.position;
            }


            foreach (var waterTrigger in _waterTriggerSignal)
            {
                _waterTriggerSignal.Get1(waterTrigger).Wet = true;
            }

            foreach (var trigger in _triggerSignal)
            {
                if (_triggerSignal.Get1(trigger).Interactor.isSelectActive)
                    continue;

                Vector3 postion = _triggerSignal.Get2(trigger).Position;
                Vector3 velocity = _triggerSignal.Get1(trigger).Velocity;

                foreach (var deformable in _deformable)
                {
                    Vector3 impactForce = velocity * 0.02f;
                    _deformable.Get1(deformable).View.Deform(postion, impactForce);

                    if (_waterTriggerSignal.Get1(trigger).Wet)
                    {
                        //_data.Saturation += _config.SaturationStep;

                        float saturation = _data.Saturation + _config.SaturationStep;

                        DOTween.To(() => _data.Saturation, x => _data.Saturation = x, saturation, 0.1f)
                            .OnUpdate(() =>
                            {
                                _data.SetDirty();
                            })
                            .OnComplete(()=>
                            {
                                // Achievement
                                if (saturation > 1.8f)
                                {
                                    _achievementsService.Unlock(AchievementsNames.sour);
                                }
                            });
                        _waterTriggerSignal.Get1(trigger).Wet = false;
                    }
                    else
                    {
                        //_data.Crumple += _config.CrumpleStep;
                        float crumple = _data.Crumple + _config.CrumpleStep;
                        DOTween.To(() => _data.Crumple, x => _data.Crumple = x, crumple, 0.1f)
                            .OnUpdate(() => {
                                _data.SetDirty();
                            });
                    }
                }
            }
        }
    }
}
