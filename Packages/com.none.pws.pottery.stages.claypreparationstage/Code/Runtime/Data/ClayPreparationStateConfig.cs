using UnityEngine;

namespace PWS.Pottery.Stages.ClayPreparationStage
{
    [CreateAssetMenu(fileName = "ClayPreparationStateConfig", menuName = "PWS/Pottery/Stages/ClayPreparationStage/Config", order = 0)]
    public class ClayPreparationStateConfig : ScriptableObject
    {
        public float CompletionThreshold = 0.9f;
        public float SkipThreshold = 0.75f;

        public float CrumpleStep = 0.1f;
        public float SaturationStep = 0.1f;

    }
}
