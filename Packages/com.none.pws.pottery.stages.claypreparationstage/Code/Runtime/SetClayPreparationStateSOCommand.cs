using Modules.StateGroup.Core;
using UnityEngine;

namespace PWS.Pottery.Stages.ClayPreparationStage
{
    [CreateAssetMenu(fileName = "SetClayPreparationStateSOCommand", menuName = "PWS/Pottery/Stages/ClayPreparationStage/SetStateSOCommand")]
    public class SetClayPreparationStateSOCommand : SetStateSOCommand<ClayPreparationState>
    {

    }
}