using Modules.StateGroup.Core;
using UnityEngine;

namespace PWS.Pottery.Stages.WallsLiftingStage
{
    [CreateAssetMenu(menuName = "PWS/Pottery/Stages/WallsLiftingStage/SetStateSOCommand")]
    public class SetWallsLiftingStateSOCommand : SetStateSOCommand<WallsLiftingState>
    {

    }
}