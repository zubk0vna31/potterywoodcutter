using Leopotam.Ecs;
using Modules.Root.ContainerComponentModel;
using Modules.Root.ECS;
using Modules.StateGroup.Core;
using PWS.Common.Audio;
using PWS.Pottery.Common;
using PWS.Pottery.ItemDataHolderService;
using PWS.Pottery.MeshGeneration;
using PWS.Pottery.Sculpting.Interactor;
using UnityEngine;
using PWS.Common.Messages;
using PWS.Common.RestartStage;
using PWS.Common.UI;
using PWS.Common.ValueRegulator;
using PWS.Pottery.WheelSpeedSetupStage;
using PWS.Common.ResultsEvaluation;
using PWS.Common.TravelPoints;

namespace PWS.Pottery.Stages.WallsLiftingStage
{
    public class PotteryWallsLiftingSystemsProvider : MonoBehaviour, ISystemsProvider
    {
        internal class SystemsDependencies
        {
            [Inject] public ISculptingDataHolder SculptingData;
            [Inject] public SculptingReferenceDataHolder SculptingReferenceData;
            [Inject] public IValueRegulatorDataHolder<WheelSpeedRegulator> WheelSpeedData;
            [Inject] public IResultsEvaluationDataHolder ResultsEvaluationData;
        }

        [SerializeField] private SetStateSOCommand _nextState;
        [SerializeField] private WallsLiftingStateConfig _config;
        [SerializeField] private ValueRegulatorConfig _wheelSpeedConfig;
        [SerializeField] private StateInfoData _stateInfo;
        
        [Header("Voice config")] 
        [SerializeField] private VoiceConfig _voiceConfig;

        public EcsSystems GetSystems(EcsWorld world, EcsSystems endFrame, EcsSystems mainSystems)
        {
            SystemsDependencies dependencies = new SystemsDependencies();
            SceneContainer.Instance.Inject(dependencies);

            EcsSystems systems = new EcsSystems(world);
            systems
                .Add(new TransitToStateOnUIActionSystem<WallsLiftingState>(_nextState))
                .Add(new WallsLiftingSkipProcessing(dependencies.SculptingData,
                    dependencies.SculptingReferenceData.Data, _nextState))

                //restart
                .Add(new CurrentStateRestoreDataProcessing<WallsLiftingState, SculptingDataHolder>(dependencies
                    .SculptingData))
                .Add(new CurrentStateRestoreDataProcessing<WallsLiftingState,
                    ValueRegulatorDataHolder<WheelSpeedRegulator>>(dependencies.WheelSpeedData))

                //wheel speed systems
                .Add(new GameObjectVisibilityProcessing<WallsLiftingState, WheelSpeedRegulator>())
                .Add(new ValueRegulatorSetupProcessing<WallsLiftingState, WheelSpeedRegulator>(_wheelSpeedConfig,
                    dependencies.WheelSpeedData))
                .Add(new ValueRegulatorInRangeProcessing<WallsLiftingState, WheelSpeedRegulator, HandTag>(
                    _wheelSpeedConfig, dependencies.WheelSpeedData))

                //systems
                .Add(new HandInteractorVisibiltySystem<WallsLiftingState>(SculptingHand.WallsLifting))
                .Add(new WallsLiftingHandsInitPosition(dependencies.SculptingData))
                .Add(new HandInteractorPositionSystem<WallsLiftingState>(_config.HandMoveAxes, _config.HandSmoothSpeed))
                .Add(new WallsLiftingInteractorsHandler(dependencies.SculptingData, dependencies.SculptingReferenceData.Data, _config))
                .Add(new WallsLiftingCompletionTracker(dependencies.SculptingData, dependencies.SculptingReferenceData.Data, _config))

                .Add(new SetGradeByProgress<WallsLiftingState>(dependencies.ResultsEvaluationData, _stateInfo))

                //ui
                .Add(new StateInfoWindowUIProcessing<WallsLiftingState>(_stateInfo))

                .Add(new StateWindowPlacementProcessing<WallsLiftingState>("Wheel"))
                .Add(new StateWindowProgressUIProcessing<WallsLiftingState>())
                .Add(new StateWindowButtonUIProcessing<WallsLiftingState>())
                .Add(new SkipStageButtonUIProcessing<WallsLiftingState>())

                .Add(new MeshVisibilityProcessing<WallsLiftingState, SculptingReferenceMesherTag>())
                .Add(new MeshVisibilityProcessing<WallsLiftingState, SculptingProductMesherTag>())

                //voice
                .Add(new VoiceAudioSystem<WallsLiftingState>(_voiceConfig))

                //travel point
                .Add(new TravelPointPositioningSystem<WallsLiftingState>("Wheel"))

                ;

            endFrame
                //wheel Speed
                .Add(new ValueRegulatorUpdateProcessing<WallsLiftingState, WheelSpeedRegulator>(dependencies.WheelSpeedData))

                //sculpting reference
                .Add(new SculptingDataProcessingSystem<WallsLiftingState>(dependencies.SculptingReferenceData.Data))
                .Add(new MeshRingsDataProcessingSystem<WallsLiftingState>(dependencies.SculptingReferenceData.Data))
                .Add(new MeshUpdateSystem<WallsLiftingState, SculptingReferenceMesherTag>(dependencies.SculptingReferenceData.Data))
                //sculpting product
                .Add(new SculptingDataProcessingSystem<WallsLiftingState>(dependencies.SculptingData))
                .Add(new MeshRingsDataProcessingSystem<WallsLiftingState>(dependencies.SculptingData))
                .Add(new MeshUpdateSystem<WallsLiftingState, SculptingProductMesherTag>(dependencies.SculptingData))

                .OneFrame<RestartCurrentStepMessage>()
                .OneFrame<NextStateSignal>()
                .OneFrame<SkipStageMessage>()

                .OneFrame<ValueRegulatorChangedSignal>()
                .OneFrame<VisibilitySignal>()
                ;

            return systems;
        }
    }
}
