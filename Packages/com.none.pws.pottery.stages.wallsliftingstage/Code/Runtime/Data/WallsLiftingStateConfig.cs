using PWS.Pottery.Sculpting.Interactor;
using UnityEngine;

namespace PWS.Pottery.Stages.WallsLiftingStage
{
    [CreateAssetMenu(fileName = "WallsLiftingStageConfig", menuName = "PWS/Pottery/Stages/WallsLiftingStage/Config", order = 0)]
    public class WallsLiftingStateConfig : ScriptableObject
    {
        public float CompletionThresold = 0.9f;
        public float SkipThreshold = 0.4f;

        [Header("Hands")]
        public InteractableParameters HandMoveAxes;
        public float HandSmoothSpeed = 5;
        public float MaxHeight = 0.5f;
    }
}
