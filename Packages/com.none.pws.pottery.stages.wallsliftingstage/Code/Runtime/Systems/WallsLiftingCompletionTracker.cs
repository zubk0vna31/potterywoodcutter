using Leopotam.Ecs;
using Modules.StateGroup.Components;
using PWS.Common.Messages;
using PWS.Common.UI;
using PWS.Pottery.ItemDataHolderService;
using UnityEngine;

namespace PWS.Pottery.Stages.WallsLiftingStage
{
    public class WallsLiftingCompletionTracker : IEcsRunSystem
    {
        // auto injected fields
        readonly EcsFilter<StateEnter, WallsLiftingState> _entering;
        readonly EcsFilter<WallsLiftingState>.Exclude<StateExit> _inState;
        private readonly EcsFilter<SkipStageMessage> _skipPerformed;

        readonly EcsFilter<StateWindow> _stateWindow;
        readonly EcsFilter<StateWindow, StateProgress> _stateWindowWithProgress;

        private WallsLiftingStateConfig _config;
        private ISculptingDataHolder _referenceData;

        //runtime data
        private ISculptingDataHolder _data;
        private float _distanceBetweenMaxPoints;

        public WallsLiftingCompletionTracker(ISculptingDataHolder data, ISculptingDataHolder referenceData, WallsLiftingStateConfig config)
        {
            _data = data;
            _referenceData = referenceData;
            _config = config;
        }

        public void Run()
        {
            float targetHeight = _referenceData.GeneralParameters.FullHeight;
            if (!_entering.IsEmpty())
            {
                _distanceBetweenMaxPoints = Mathf.Abs(targetHeight - _data.GeneralParameters.FullHeight);


                foreach (var i in _stateWindowWithProgress)
                {
                    _stateWindowWithProgress.Get2(i).Value = 0;
                    _stateWindowWithProgress.Get2(i).CompletionThreshold = _config.CompletionThresold;
                    _stateWindowWithProgress.Get2(i).SkipThreshold = _config.SkipThreshold;
                }
            }

            if (!_inState.IsEmpty())
            {
                if (!_skipPerformed.IsEmpty())
                    return;

                float currentHeight = Mathf.Abs(targetHeight - _data.GeneralParameters.FullHeight);
                float completion = 1 - Mathf.Clamp01(currentHeight / _distanceBetweenMaxPoints);

                foreach (var i in _stateWindowWithProgress)
                {
                    _stateWindowWithProgress.Get2(i).Value = completion;
                }
            }
        }
    }
}
