using Leopotam.Ecs;
using Modules.StateGroup.Core;
using PWS.Common.Messages;
using PWS.Pottery.Common;
using PWS.Pottery.ItemDataHolderService;

namespace PWS.Pottery.Stages.WallsLiftingStage
{
    public class WallsLiftingSkipProcessing : IEcsRunSystem
    {
        // auto injected fields
        private readonly EcsFilter<SkipStageMessage> _skipPerformed;
        readonly EcsFilter<WallsLiftingState> _inState;
        private readonly StateFactory _stateFactory;

        private ISculptingDataHolder _referenceData;
        private SetStateSOCommand _nextState;

        //runtime data
        private ISculptingDataHolder _data;

        public WallsLiftingSkipProcessing(ISculptingDataHolder data, ISculptingDataHolder referenceData, SetStateSOCommand nextState)
        {
            _data = data;
            _referenceData = referenceData;
            _nextState = nextState;
        }

        public void Run()
        {
            if (_inState.IsEmpty())
                return;
            if (_skipPerformed.IsEmpty())
                return;

            float targetHeight = _referenceData.GeneralParameters.FullHeight;
            _data.GeneralParameters.FullHeight = targetHeight;
            _data.SetDirty();

            /*foreach (var i in _skipPerformed)
            {
                _skipPerformed.GetEntity(i).Del<SkipStageMessage>();
            }*/

            _nextState.Execute(_stateFactory);
        }
    }
}
