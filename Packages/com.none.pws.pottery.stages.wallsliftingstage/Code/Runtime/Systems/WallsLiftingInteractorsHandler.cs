using Leopotam.Ecs;
using Modules.StateGroup.Components;
using Modules.ViewHub;
using PWS.Pottery.ItemDataHolderService;
using PWS.Pottery.MeshGeneration;
using PWS.Pottery.Sculpting.Interactor;
using UnityEngine;

namespace PWS.Pottery.Stages.WallsLiftingStage
{
    public class WallsLiftingInteractorsHandler : IEcsRunSystem
    {
        // auto injected fields
        readonly EcsFilter<WallsLiftingState> _inState;

        readonly EcsFilter<UnityView, SculptingInteractorTag> _generalInteractor;
        readonly EcsFilter<UnityView, SculptingProductTag> _product;

        private WallsLiftingStateConfig _config;

        //runtime data
        private ISculptingDataHolder _data;
        private ISculptingDataHolder _referenceData;
        private float oldHeight;

        public WallsLiftingInteractorsHandler(ISculptingDataHolder data, ISculptingDataHolder referenceData, WallsLiftingStateConfig config)
        {
            _data = data;
            _referenceData = referenceData;
            _config = config;
        }

        public void Run()
        {
            if (_inState.IsEmpty())
                return;

            foreach (var productIdx in _product)
            {
                Transform product = _product.Get1(productIdx).Transform;
                foreach (var interactorIdx in _generalInteractor)
                {
                    Interaction(_generalInteractor.Get2(interactorIdx).Position, _product.Get1(productIdx).Transform.position);
                }
            }
        }

        void Interaction(Vector3 handPosition, Vector3 productPosition)
        {
            float newHeight = handPosition.y - productPosition.y;
            newHeight = Mathf.Clamp(newHeight, 0, _config.MaxHeight);

            if (newHeight == oldHeight)
                return;

            _data.GeneralParameters.FullHeight = newHeight;
            float refFullHeight = _referenceData.GeneralParameters.FullHeight;
            if (_referenceData.GeneralParameters.FullHeight <= _data.DefaultParameters.Height)
                refFullHeight = _data.DefaultParameters.Height * 2;

            float itemHeight = _data.GeneralParameters.FullHeight - _data.DefaultParameters.Height;
            float refItemHeight = refFullHeight - _data.DefaultParameters.Height;
            float valueByHeight = Mathf.Clamp01(itemHeight / refItemHeight);
            float wallWidth = Mathf.Lerp(_data.DefaultParameters.WallWidthMax, _data.DefaultParameters.WallWidthMin, valueByHeight);
            foreach (Ring ring in _data.Rings)
            {
                ring.Width = wallWidth;
            }

            _data.SetDirty();

            oldHeight = newHeight;
        }
    }
}