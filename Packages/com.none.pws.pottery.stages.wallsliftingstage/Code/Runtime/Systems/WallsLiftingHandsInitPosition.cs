using Leopotam.Ecs;
using Modules.StateGroup.Components;
using Modules.ViewHub;
using PWS.Pottery.ItemDataHolderService;
using PWS.Pottery.MeshGeneration;
using PWS.Pottery.Sculpting.Interactor;
using UnityEngine;

namespace PWS.Pottery.Stages.WallsLiftingStage
{
    public class WallsLiftingHandsInitPosition : IEcsRunSystem
    {
        // auto injected fields
        readonly EcsFilter<StateEnter, WallsLiftingState> _entering;

        readonly EcsFilter<UnityView, HandTag> _hands;
        readonly EcsFilter<UnityView, SculptingProductTag> _product;

        //runtime data
        private ISculptingDataHolder _data;
        private float oldHeight;

        public WallsLiftingHandsInitPosition(ISculptingDataHolder data)
        {
            _data = data;
        }

        public void Run()
        {
            if (!_entering.IsEmpty())
            {
                foreach (var product in _product)
                {
                    foreach (var idx in _hands)
                    {
                        ref var handsView = ref _hands.Get1(idx);
                        ref var handsTag = ref _hands.Get2(idx);
                        handsView.Transform.localPosition = Vector3.zero;
                        var pos = handsTag.View.WallsLiftingHandVisual.transform.position;
                        float height = _product.Get1(product).Transform.position.y + _data.GeneralParameters.FullHeight;
                        pos.y = height;
                        handsView.Transform.position = pos;
                        handsTag.View.WallsLiftingHandVisual.transform.position = pos;
                        handsTag.OriginLocalPosition = handsView.Transform.localPosition;
                    }
                }
            }
        }
    }
}