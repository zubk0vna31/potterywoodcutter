﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GlueMeshCreator : MonoBehaviour
{
    [SerializeField]
    private GameObject gluePrefab;

    [SerializeField]
    private Transform pointsParent;

    [SerializeField]
    private bool useLine;

    [SerializeField,Range(0,64)]
    private int lineDivisionCount;

    [SerializeField]
    private  GameObject linkObject;

    private SplineCreator splineCreator;
    private GlueMeshRenderer glueMeshRenderer;

    public GlueMeshRenderer GlueMeshRenderer => glueMeshRenderer;

    //private void OnEnable()
    //{
    //    var @object = Instantiate(gluePrefab, Vector3.zero, Quaternion.identity, null);

    //    @object.name = "Glue Mesh (Creator)";

    //    splineCreator = @object.GetComponent<SplineCreator>();
    //    splineCreator.Initialize(GetPoints());
    //}
    public void Initialize(SplineCreator splineCreator,GlueMeshRenderer glueMeshRenderer,int divisionCount)
    {
        this.splineCreator = splineCreator;
        this.glueMeshRenderer = glueMeshRenderer;
        this.lineDivisionCount = divisionCount;
    }

    public void Create()
    {
        var @object = Instantiate(gluePrefab, Vector3.zero, Quaternion.identity, null);

        @object.name = "Glue Mesh (Creator)";

        splineCreator = @object.GetComponent<SplineCreator>();
        glueMeshRenderer = @object.GetComponent<GlueMeshRenderer>();
        splineCreator.Initialize(GetPoints());
    }


    public void Create(params Vector3 [] points)
    {
        splineCreator.Initialize(GetPoints(points));
    }

    private List<Vector3> GetPoints()
    {
        if (pointsParent.childCount < 2)
        {
            Debug.LogWarning("Must be minimum 2 points!");
            return null;
        }

        List<Vector3> points = new List<Vector3>();

        if (useLine || pointsParent.childCount==2)
        {
            Vector3 pos1 = pointsParent.GetChild(0).position;
            Vector3 pos2 = pointsParent.GetChild(1).position;

            points.Add(pos1);

            for (int i = 0; i < lineDivisionCount; i++)
            {
                float t = (i+1)*1.0f/(lineDivisionCount+1);

                points.Add(Vector3.Lerp(pos1, pos2, t));
            }

            points.Add(pos2);
        }
        else
        {
            points = pointsParent.GetComponentsInChildren<Transform>()
                .Where(x => x != pointsParent).Select(x => x.position).ToList();
        }

        return points;
    }

    private List<Vector3> GetPoints(params Vector3 [] input)
    {
        List<Vector3> points = new List<Vector3>();

        if (input.Length == 2)
        {
            Vector3 pos1 = input[0];
            Vector3 pos2 = input[1];

            points.Add(pos1);

            for (int i = 0; i < lineDivisionCount; i++)
            {
                float t = (i + 1) * 1.0f / (lineDivisionCount + 1);

                points.Add(Vector3.Lerp(pos1, pos2, t));
            }

            points.Add(pos2);
        }
        else
        {
            points = input.ToList();
        }

        return points;
    }

    public void ResetComponent()
    {
        glueMeshRenderer.ResetComponent();
        gameObject.SetActive(true);
    }
}
