﻿using System.Collections.Generic;
using UnityEngine;

public class GluePushSystem : MonoBehaviour
{
    //[SerializeField]
    //private Transform pusher;

    [SerializeField]
    private float distanceFromCamera;

    [SerializeField,Range(0,10)]
    private float strength = 2.5f;

    [SerializeField]
    private float followSpeed = 10f;

    [SerializeField]
    private float followRotationSpeed = 10f;

    public float PushStrength => strength;

    //private Camera mainCamera;
    //private Vector3 previousPosition;

    private Dictionary<Transform, GlueMeshRenderer> glueMeshes;

    public void Clear()
    {
        glueMeshes.Clear();
    }

    private void Start()
    {
        //mainCamera = Camera.main;
        //previousPosition = pusher.parent.transform.position;

        glueMeshes = new Dictionary<Transform, GlueMeshRenderer>();

        GetComponentInChildren<TriggerListenerActionBased>().triggerEnter += GlueCollisionEnter;
        GetComponentInChildren<TriggerListenerActionBased>().triggerExit  += GlueCollisionExit;
    }

    private void Update()
    {
        //if(Input.GetKey(KeyCode.Mouse1))
        //    MovePusher();
        UpdateGlueMeshes();
    }

    //private void MovePusher()
    //{
    //    Vector3 worldPos = mainCamera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, distanceFromCamera));

    //    //Debug.DrawLine(mainCamera.gameObject.transform.position, worldPos, Color.green);

    //    pusher.parent.transform.position = Vector3.Lerp(pusher.parent.transform.position, worldPos, Time.deltaTime * followSpeed);

    //    if (previousPosition != pusher.transform.position)
    //    {
    //        pusher.parent.transform.rotation = Quaternion.Slerp(pusher.parent.transform.rotation,
    //            Quaternion.LookRotation((pusher.parent.transform.position - previousPosition).normalized),
    //            Time.deltaTime*followRotationSpeed);
    //    }

    //    previousPosition = pusher.parent.transform.position;
    //}

    private void UpdateGlueMeshes()
    {
        foreach (var glueMesh in glueMeshes.Values)
        {
            //glueMesh.Push(pusher,strength);
            glueMesh.Push(transform,strength);
        }
    }

    private void GlueCollisionEnter(Transform self,Transform other)
    {
        if (!other.CompareTag("Glue")) return;

        var glueMeshRenderer = other.GetComponent<GlueMeshRenderer>();

        if (!glueMeshRenderer) return;

        if(!glueMeshes.ContainsKey(other))
            glueMeshes.Add(other, glueMeshRenderer);
    }

    private void GlueCollisionExit(Transform self, Transform other)
    {
        if (!other.CompareTag("Glue")) return;

        if (glueMeshes.ContainsKey(other))
            glueMeshes.Remove(other);
    }
}
