﻿using UnityEngine;

public class GlueMeshLinker : MonoBehaviour
{
    [SerializeField]
    private GlueMeshCreator main;

    [SerializeField]
    private GlueMeshCreator linked;

    private GlueMeshRenderer mainRenderer,linkedRenderer;

    //private void Start()
    //{
    //    main.Create();
    //    linked.Create();

    //    Link(main.GlueMeshRenderer, linked.GlueMeshRenderer);
    //}

    public void Link(GlueMeshRenderer main, GlueMeshRenderer linked)
    {
        if(main.PointsAmount != linked.PointsAmount)
        {
            Debug.LogWarning("Points amount of glue mesh render must be same!");
            return;
        }

        main.OnGlueMeshModified += ModifyLinked;

        mainRenderer = main;
        linkedRenderer = linked;
    }

    private void ModifyLinked(int index,Vector3 direction,float strength)
    {
        linkedRenderer.Push(index, direction, strength);

        if (linkedRenderer.FullyPushed)
        {
            mainRenderer.OnGlueMeshModified -= ModifyLinked;

            mainRenderer.gameObject.SetActive(false);
        }
    }
}
