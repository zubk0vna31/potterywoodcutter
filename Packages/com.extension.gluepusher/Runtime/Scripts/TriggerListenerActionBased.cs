﻿using System;
using UnityEngine;

public class TriggerListenerActionBased : MonoBehaviour
{
    public Action<Transform, Transform> triggerEnter;
    public Action<Transform, Transform> triggerStay;
    public Action<Transform, Transform> triggerExit;

    private void OnTriggerEnter(Collider other)
    {
        triggerEnter?.Invoke(transform, other.transform);
    }

    private void OnTriggerStay(Collider other)
    {
        triggerStay?.Invoke(transform, other.transform);
    }

    private void OnTriggerExit(Collider other)
    {
        triggerExit?.Invoke(transform, other.transform);
    }
}
