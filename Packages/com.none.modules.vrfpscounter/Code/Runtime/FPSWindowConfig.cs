using UnityEngine;

namespace Modules.VRFPSCounter
{
    [CreateAssetMenu(menuName = "Modules/VRFPSCounter/FPSWindowConfig")]
    public class FPSWindowConfig : ScriptableObject
    {
        [SerializeField] public bool UseFPSWindow = false;
    }
}