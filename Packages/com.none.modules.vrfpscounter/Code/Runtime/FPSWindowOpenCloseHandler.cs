using System;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Modules.VRFPSCounter
{
    public class FPSWindowOpenCloseHandler : MonoBehaviour
    {
        [SerializeField] private FPSWindowConfig _fpsWindowConfig;
        [SerializeField] private InputActionReference _openAction;
        [SerializeField] private GameObject _targetObject;

        public void Awake()
        {
            if (!_fpsWindowConfig.UseFPSWindow)
            {
                Destroy(_targetObject);
                return;
            }

            _openAction.action.performed += SwitchMenuState;
        }

        public virtual void SwitchMenuState(InputAction.CallbackContext callbackContext)
        {
            _targetObject.SetActive(!_targetObject.activeSelf);
        }

        public void OnDestroy()
        {
            _openAction.action.performed -= SwitchMenuState;
        }
    }
}