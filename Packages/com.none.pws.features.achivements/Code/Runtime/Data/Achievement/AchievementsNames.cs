namespace PWS.Features.Achievements
{
    public static class AchievementsNames 
    {
        // Woodcutting
        public const string boomerang = "achv/woodcutting/boomerang";
        public const string clamping_device_i_say = "achv/woodcutting/clamping-device-i-say";
        public const string experience_gained_is_never_lost = "achv/woodcutting/experience-gained-is-never-lost";
        public const string obedient = "achv/woodcutting/obedient";
        public const string specialist = "achv/woodcutting/specialist";
        public const string tyap_lyap = "achv/woodcutting/tyap-lyap";
        public const string artist = "achv/woodcutting/artist";

        // Pottery
        public const string potter = "achv/pottery/potter";
        public const string golden_hands = "achv/pottery/golden-hands";
        public const string jeweler = "achv/pottery/jeweler";
        public const string it_happens = "achv/pottery/it-happens";
        public const string fast_walkthrough = "achv/pottery/fast-walkthrough";
        public const string sour = "achv/pottery/sour";
        public const string darkness = "achv/pottery/darkness-is-friend-of-youth";
        public const string ceramist = "achv/pottery/ceramist";
        public const string twisted = "achv/pottery/twisted";
    }
}
