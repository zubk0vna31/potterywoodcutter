using PWS.Features.Achivements;
using System.Collections.Generic;
using UnityEngine;

namespace PWS.Features.Achievements
{
    [System.Serializable]
    public class StringFloatKeyValuePair
    {
        public string Key;
        public float Value;
    }

    public class AchievementSO : ScriptableObject
    {
        public ApplicationType ApplicationType;
        public string Key;
        public string LocalizationKey;
        public Rarity Rarity;
        public ComparassionOperation CompareOperation;
        public AchievementType Type;
        public int IntValue;
        public float FloatValue;
        public bool Appendable;
        [TextArea] public string Description;
        public List<string> StringCollection = new List<string>();
        public List<StringFloatKeyValuePair> KeyValuePairs = new List<StringFloatKeyValuePair>();

        //Maybe will be needed
        public bool IsWoodCutting()
        {
            return ApplicationType == ApplicationType.WoodCutting;
        }
    }
}
