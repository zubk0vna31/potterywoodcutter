using PWS.Features.Achivements;
using UnityEngine;

namespace PWS.Features.Achievements
{
    public class AchievementFloat : AchievementSingle<float>
    {
        public AchievementFloat(string key, string localizationKey, Rarity rarity, float reference,
            bool appendable, ComparassionOperation operation) : base(key, localizationKey, rarity,
                reference, appendable, operation)
        {
        }

        public override void AppendProgress(float append)
        {
            if (IsUnlocked) return;

            Current += append;

            if (!Appendable && !IsUnlocked)
            {
                Current = 0;
            }
        }


        public override void LoadProgress(string value)
        {
            Current = value.GetFloat();
        }

        public override void Unlock()
        {
            if (IsUnlocked) return;

            if (Operation != ComparassionOperation.Less && Operation != ComparassionOperation.Greater)
                Current = Reference;
            else
            {
                if (Operation == ComparassionOperation.Less)
                {
                    if (Current >= Reference)
                    {
                        Current = Reference - Mathf.Epsilon;
                    }
                    else
                    {
                        IsDirty = true;
                    }
                }
                else
                {
                    if (Current <= Reference)
                    {
                        Current = Reference + Mathf.Epsilon;
                    }
                    else
                    {
                        IsDirty = true;
                    }
                }
            }
        }

        public override string ToString()
        {
            try
            {
                return Current.ToString();
            }
            catch
            {
                return string.Empty;
            }
        }

        public override bool Compare()
        {
            switch (Operation)
            {
                case ComparassionOperation.None:
                case ComparassionOperation.Equal:
                    return Current == Reference;
                case ComparassionOperation.Less:
                    return Current < Reference;
                case ComparassionOperation.Greater:
                    return Current > Reference;
                case ComparassionOperation.LEqual:
                    return Current <= Reference;
                case ComparassionOperation.GEqual:
                    return Current >= Reference;
                default:
                    return Current == Reference;
            }
        }
    }
}
