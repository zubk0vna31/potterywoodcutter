using PWS.Features.Achivements;
using UnityEngine;

namespace PWS.Features.Achievements
{
    public class AchievementInt : AchievementSingle<int>
    {
        public AchievementInt(string key, string localizationKey, Rarity rarity, int reference,
            bool appendable,ComparassionOperation operation) : base(key, localizationKey, rarity, 
                reference, appendable,operation)
        {
        }

        public override void AppendProgress(int append)
        {
            if (IsUnlocked) return;

            Current += append;

            if (!Appendable && !IsUnlocked)
            {
                Current = 0;
            }
        }


        public override void LoadProgress(string value)
        {
            Current = value.GetInt();
        }


        public override void Unlock()
        {
            if (IsUnlocked) return;

            if (Operation != ComparassionOperation.Less && Operation != ComparassionOperation.Greater)
                Current = Reference;
            else
            {
                if (Operation == ComparassionOperation.Less)
                {
                    if (Current >= Reference)
                    {
                        Current = Reference - 1;
                    }
                    else
                    {
                        IsDirty = true;
                    }
                }
                else
                {
                    if (Current <= Reference)
                    {
                        Current = Reference + 1;
                    }
                    else
                    {
                        IsDirty = true;
                    }
                }
            }
        }
        public override string ToString()
        {
            try
            {
                return Current.ToString();
            }
            catch
            {
                return string.Empty;
            }
        }

        public override bool Compare()
        {
            switch (Operation)
            {
                case ComparassionOperation.None:
                case ComparassionOperation.Equal:
                    return Current == Reference;
                case ComparassionOperation.Less:
                    return Current < Reference;
                case ComparassionOperation.Greater:
                    return Current > Reference;
                case ComparassionOperation.LEqual:
                    return Current <= Reference;
                case ComparassionOperation.GEqual:
                    return Current >= Reference;
                default:
                    return Current == Reference;
            }
        }
    }
}
