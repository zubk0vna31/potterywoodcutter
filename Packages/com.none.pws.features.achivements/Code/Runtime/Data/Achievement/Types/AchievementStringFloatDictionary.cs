using PWS.Features.Achievements;
using System.Collections.Generic;

namespace PWS.Features.Achivements
{
    public class AchievementStringFloatDictionary : AchievementDictionary<string, float>
    {

        public AchievementStringFloatDictionary(string key, string localizationKey, Rarity rarity,
            Dictionary<string, float> reference, bool appendable,ComparassionOperation operation) :
            base(key, localizationKey, rarity,
                reference, appendable,operation)
        {
        }

        public override void AppendProgress(string key, float value)
        {
            if (IsUnlocked) return;

            if (Current.ContainsKey(key) || !Reference.ContainsKey(key)) return;

            Current.Add(key,value);
            IsDirty = true;
        }

        public override bool CompareValue(float self,float other)
        {
            switch (Operation)
            {
                case ComparassionOperation.None:
                case ComparassionOperation.Equal:
                    return self == other;
                case ComparassionOperation.Less:
                    return self < other;
                case ComparassionOperation.Greater:
                    return self > other;
                case ComparassionOperation.LEqual:
                    return self <= other;
                case ComparassionOperation.GEqual:
                    return self >= other;
                default:
                    return self == other;
            }
        }

        public override void LoadProgress(string value)
        {
            Current = new Dictionary<string, float>(value.GetDictionary<string, float>());
        }

        public override string ToString()
        {

            if (Appendable || IsUnlocked)
            {
                try
                {
                    return Current.DictionaryToString();
                }
                catch
                {
                    return string.Empty;
                }
            }
            else
            {
                return string.Empty;
            }
        }

        public override void Unlock()
        {
            Current.Clear();
            Current = new Dictionary<string, float>(Reference);
            IsDirty = true;
        }

    }
}
