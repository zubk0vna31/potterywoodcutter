using PWS.Features.Achivements;

namespace PWS.Features.Achievements
{
    public class AchievementBool : AchievementSingle<bool>
    {
        public AchievementBool(string key, string localizationKey, Rarity rarity, bool reference,
            bool appendable,ComparassionOperation operation) : base(key, localizationKey, rarity,
                reference, appendable,operation)
        {
        }

        public override void AppendProgress(bool append)
        {
            if (IsUnlocked) return;

            if(append == Reference)
                Unlock();
        }

        public override void LoadProgress(string value)
        {
            Current = value.GetBool();
        }

        public override void Unlock()
        {
            Current = Reference;
        }

        public override string ToString()
        {
            try
            {
                return Current.ToString();
            }
            catch
            {
                return string.Empty;
            }
        }

        public override bool Compare()
        {
            return Current == Reference;
        }
    }
}
