using System.Collections.Generic;

namespace PWS.Features.Achievements
{
    public class AchievementStringCollection : AchievementIEnumerable<string>
    {
        public AchievementStringCollection(string key, string localizationKey, Rarity rarity, 
            IEnumerable<string> reference, bool appendable) :
            base(key, localizationKey, rarity, reference, appendable)
        {
        }

        public override void AppendProgress(string value)
        {
            if (IsUnlocked) return;

            if (Current.Contains(value) || !Reference.Contains(value)) return;

            Current.Add(value);
            IsDirty = true;
        }

        public override void LoadProgress(string value)
        {
            Current = new HashSet<string>(value.GetStringCollection());
        }

        public override void Unlock()
        {
            if (IsUnlocked) return;

            Current = Reference;
            IsDirty = true;
        }

        public override string ToString()
        {
            if (Appendable || IsUnlocked)
            {
                try
                {
                    return Current.StringCollectionToString();
                }
                catch
                {
                    return string.Empty;
                }
            }
            else
            {
                return string.Empty;
            }
        }

        public override bool Compare()
        {
            return Current.SetEquals(Reference);
        }
    }
}
