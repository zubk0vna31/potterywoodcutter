namespace PWS.Features.Achievements
{
    public abstract class Achievement<T> : BaseAchievement
    {
        public readonly T Reference;

        protected T Progress;
        protected bool Appendable;

        public Achievement(string key, string localizationKey, Rarity rarity,
            T reference, bool appendable) : base(key, localizationKey, rarity)
        {
            Reference = reference;
            Appendable = appendable;
        }

        protected T Current
        {
            get
            {
                return Progress;
            }

            set
            {
                Progress = value;
                IsDirty = true;
            }
        }

        public override bool IsDirty
        {
            get
            {
                return base.IsDirty;
            }

            protected set
            {
                isDirty = value;

                if (isDirty)
                {
                    IsUnlocked = Compare();
                }
            }
        }

        public T Value => Progress;

        public override abstract void LoadProgress(string value);

        public override abstract void Unlock();

        public abstract bool Compare();
    }
}
