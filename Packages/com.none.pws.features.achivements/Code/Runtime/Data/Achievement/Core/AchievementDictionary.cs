using PWS.Features.Achievements;
using System.Collections.Generic;

namespace PWS.Features.Achivements
{
    public abstract class AchievementDictionary<TKey,TValue> : Achievement<Dictionary<TKey,TValue>>
    {
        public readonly ComparassionOperation Operation;

        protected AchievementDictionary(string key, string localizationKey, Rarity rarity,
            Dictionary<TKey, TValue> reference, bool appendable, ComparassionOperation operation) : base(key, localizationKey, rarity, reference, appendable)
        {
            Current = new Dictionary<TKey, TValue>();

            Operation = operation;
        }

        public abstract void AppendProgress(TKey key,TValue value);

        public override abstract void LoadProgress(string value);

        public override abstract string ToString();

        public override abstract void Unlock();

        public override bool Compare()
        {
            if (Current is null || Reference is null) return false;

            if (Current == Reference) return true;

            if (Current.Count != Reference.Count) return false;

            foreach (var key in Reference.Keys)
            {
                if(!Current.TryGetValue(key,out var value))
                {
                    return false;
                }
                else
                {
                    if (!CompareValue(Reference[key],value))
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        public abstract bool CompareValue(TValue self,TValue other);
    }
}
