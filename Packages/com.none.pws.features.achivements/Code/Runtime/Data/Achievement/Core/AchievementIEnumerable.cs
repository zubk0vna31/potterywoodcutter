using System.Collections.Generic;

namespace PWS.Features.Achievements
{
    public abstract class AchievementIEnumerable<T> : Achievement<HashSet<T>>
    {
        protected AchievementIEnumerable(string key, string localizationKey, Rarity rarity,
            IEnumerable<T> reference, bool appendable) : base(key, localizationKey, rarity,
                new HashSet<T>(reference), appendable)
        {
            Current = new HashSet<T>();
        }

        public abstract void AppendProgress(T value);

        public override abstract void LoadProgress(string value);

        public override abstract void Unlock();

        public override abstract bool Compare();
    }
}
