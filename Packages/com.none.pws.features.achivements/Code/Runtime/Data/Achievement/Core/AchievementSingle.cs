using PWS.Features.Achivements;

namespace PWS.Features.Achievements
{
    public abstract class AchievementSingle<T> : Achievement<T>
    {
        public readonly ComparassionOperation Operation;

        protected AchievementSingle(string key, string localizationKey, Rarity rarity, T reference,
            bool appendable,ComparassionOperation operation) : base(key, localizationKey, rarity, reference, appendable)
        {
            Operation = operation;
        }

        public abstract void AppendProgress(T append);

        public override abstract void LoadProgress(string value);

        public override abstract bool Compare();
    }
}
