
namespace PWS.Features.Achievements
{
    public abstract class BaseAchievement
    {
        public readonly string Key;
        public readonly string LocalizationKey;
        public readonly Rarity Rarity;

        protected bool isDirty;

        public bool IsUnlocked { get; protected set; }

        public virtual bool IsDirty { get => isDirty; protected set { } }

        public BaseAchievement(string key, string localizationKey, Rarity rarity)
        {
            Key = key;
            LocalizationKey = localizationKey;
            Rarity = rarity;
        }

        public abstract void Unlock();

        public abstract void LoadProgress(string value);

        public override abstract string ToString();

    }
}
