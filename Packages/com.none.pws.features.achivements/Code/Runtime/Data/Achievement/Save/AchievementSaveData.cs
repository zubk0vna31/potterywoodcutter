using Oculus.Platform;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PWS.Features.Achievements
{
    public static class AchievementsExtensions
    {
        public const char PARSE_SYMBOL = '|';
        public const char DICTIONARY_PARSE_SYMBOL = ';';

        public static bool GetBool(this string value, bool defaultValue = false)
        {
            try
            {
                return bool.Parse(value);
            }
            catch
            {
                return defaultValue;
            }
        }

        public static int GetInt(this string value, int defaultValue = 0)
        {
            try
            {
                return int.Parse(value);
            }
            catch
            {
                return defaultValue;
            }
        }

        public static float GetFloat(this string value, float defaultValue = 0.0f)
        {
            try
            {
                return float.Parse(value);
            }
            catch
            {
                return defaultValue;
            }
        }

        public static IEnumerable<string> GetStringCollection(this string value, IEnumerable<string> defaultValue = default(IEnumerable<string>))
        {
            try
            {
                return value.Split(PARSE_SYMBOL);
            }
            catch
            {
                return defaultValue;
            }
        }

        public static IDictionary<TKey, TValue> GetDictionary<TKey, TValue>(this string str,
            IDictionary<TKey, TValue> defaultValue = default(IDictionary<TKey, TValue>))
        {
            try
            {
                string[] keyValuePairs = str.Split(PARSE_SYMBOL);

                Dictionary<TKey, TValue> dictionary = new Dictionary<TKey, TValue>();

                for (int i = 0; i < keyValuePairs.Length; i++)
                {
                    try
                    {
                        string[] keyValuePair = keyValuePairs[i].Split(DICTIONARY_PARSE_SYMBOL);

                        TKey key = default(TKey);
                        key = (TKey)Convert.ChangeType(keyValuePair[0], typeof(TKey));

                        TValue value = default(TValue);
                        value = (TValue)Convert.ChangeType(keyValuePair[1], typeof(TValue));

                        if (!dictionary.ContainsKey(key))
                            dictionary.Add(key, value);
                    }
                    catch
                    {
                        continue;
                    }
                }

                return dictionary;
            }
            catch
            {
                return defaultValue;
            }
        }

        public static string StringCollectionToString(this IEnumerable<string> collection)
        {
            StringBuilder stringBuilder = new StringBuilder();

            int collectionLength = collection.Count();

            int i = 0;
            foreach (var str in collection)
            {
                stringBuilder.Append(str);

                if (i < collectionLength - 1)
                    stringBuilder.Append(PARSE_SYMBOL);

                i++;
            }

            return stringBuilder.ToString();
        }

        public static string DictionaryToString<TKey, TValue>(this IDictionary<TKey, TValue> dictionary)
        {
            StringBuilder stringBuilder = new StringBuilder();

            int collectionLength = dictionary.Count();

            int i = 0;
            foreach (var str in dictionary)
            {
                stringBuilder.Append(str.Key.ToString());

                stringBuilder.Append(DICTIONARY_PARSE_SYMBOL);

                stringBuilder.Append(str.Value.ToString());

                if (i < collectionLength - 1)
                    stringBuilder.Append(PARSE_SYMBOL);

                i++;
            }

            return stringBuilder.ToString();
        }
    }

    [System.Serializable]
    public class AchievementSaveData
    {

        public string key;
        public string value;
        public bool unlocked;

        public AchievementSaveData(string key, string value, bool unlocked)
        {
            this.key = key;
            this.value = value;
            this.unlocked = unlocked;
        }


    }

    [System.Serializable]
    public class AchievementSaveConfig
    {
        public AchievementSaveData[] achievements;
        public float version;

        public AchievementSaveConfig(float version)
        {
            this.version = version;
        }
    }
}
