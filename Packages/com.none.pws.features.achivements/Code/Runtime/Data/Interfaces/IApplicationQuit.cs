namespace PWS.Features.Achievements
{
    public interface IApplicationQuit 
    {
        public void OnApplicationQuit();
    }
}
