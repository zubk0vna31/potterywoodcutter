namespace PWS.Features.Achivements
{
    [System.Serializable]
    public enum ComparassionOperation 
    {
        None=0,
        Equal,
        Less,
        Greater,
        LEqual,
        GEqual,
    }
}
