namespace PWS.Features.Achievements
{
    public enum Rarity 
    {
        Bronze=0,
        Silver,
        Gold
    }
}
