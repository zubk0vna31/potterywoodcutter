namespace PWS.Features.Achievements
{
    public enum AchievementType 
    {
        Simple=0,
        Int,
        Float,
        StringCollection,
        Dictionary
    }
}
