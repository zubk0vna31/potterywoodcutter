using Leopotam.Ecs;
using Modules.Root.ContainerComponentModel;
using Modules.Root.ECS;
using PWS.Features.Achievements;
using PWS.HUB.Simulation;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PWS.Features.Achivements
{
    public class PotteryAchievementsSystemsProvider : MonoBehaviour, ISystemsProvider
    {
        internal class Dependencies
        {
            [Inject] public IAchievementsService achivementsService;
            [Inject] public IPreviousSceneService previousSceneService;
        }

        public EcsSystems GetSystems(EcsWorld world, EcsSystems endFrame, EcsSystems mainSystems)
        {
            Dependencies dependencies = new Dependencies();
            SceneContainer.Instance.Inject(dependencies);

            EcsSystems systems = new EcsSystems(world, "Pottery Achivements Systems");

            systems

                // Twisted
                .Add(new EnterPotterySystem(dependencies.achivementsService, dependencies.previousSceneService));

                ;

            return systems;
        }
    }
}
