using Leopotam.Ecs;
using PWS.Common.Messages;
using PWS.HUB.Simulation;
using UnityEngine.SceneManagement;

namespace PWS.Features.Achievements
{
    public class AchievementSceneHandler : IEcsRunSystem
    {
        private readonly EcsFilter<RestartFirstStepMessage> _restartFirstStep;
        private readonly EcsFilter<ReturnToHubMessage> _returnToHub;

        private readonly IAchievementsService _achievementsService;
        private readonly IPreviousSceneService _previousSceneService;


        public AchievementSceneHandler(IAchievementsService achievementsService, IPreviousSceneService previousSceneService)
        {
            _achievementsService = achievementsService;
            _previousSceneService = previousSceneService;
        }

        public void Run()
        {
            if(!_restartFirstStep.IsEmpty() || !_returnToHub.IsEmpty())
            {
                _previousSceneService.SetPreviousScene(SceneManager.GetActiveScene().name);
                _achievementsService.Save();
            }
        }
    }
}
