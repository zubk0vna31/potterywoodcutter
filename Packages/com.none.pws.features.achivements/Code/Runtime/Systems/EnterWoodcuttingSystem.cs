using Leopotam.Ecs;
using PWS.Features.Achievements;
using PWS.HUB.Simulation;
using UnityEngine.SceneManagement;

namespace PWS.Features.Achivements
{
    public class EnterWoodcuttingSystem : IEcsInitSystem
    {
        private readonly IAchievementsService _achievementsService;
        private readonly IPreviousSceneService _previousSceneService;

        public EnterWoodcuttingSystem(IAchievementsService achievementsService, IPreviousSceneService previousSceneService)
        {
            _achievementsService = achievementsService;
            _previousSceneService = previousSceneService;
        }

        public void Init()
        {
            if (!_previousSceneService.GetPreviousScene().Equals(SceneManager.GetActiveScene().name))
                _achievementsService.AppendProgressInt(AchievementsNames.boomerang, 1);
        }
    }
}
