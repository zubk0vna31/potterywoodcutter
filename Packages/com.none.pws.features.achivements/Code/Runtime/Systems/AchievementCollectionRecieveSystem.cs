using Leopotam.Ecs;

namespace PWS.Features.Achievements
{
    public class AchievementCollectionRecieveSystem : IEcsRunSystem
    {
        private readonly EcsFilter<AchievementRequest> _requests;

        private readonly IAchievementsService _achievementsService;

        public AchievementCollectionRecieveSystem(IAchievementsService achievementsService )
        {
            _achievementsService = achievementsService;
        }

        public void Run()
        {
            if (_requests.IsEmpty()) return;

            foreach (var r in _requests)
            {
                _achievementsService.AppendProgressString(_requests.Get1(r).achievementKey, _requests.Get1(r).keyToUnlock);
            }
        }
    }
}
