using Leopotam.Ecs;
using Modules.Root.ContainerComponentModel;
using Modules.Root.ECS;
using Modules.SGMBLinker;
using PWS.Common.Messages;
using PWS.HUB.Simulation;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace PWS.Features.Achievements
{
    [CreateAssetMenu(menuName = "PWS/Features/Achivements/Provider")]
    public class AchivementSystemsProvider : ScriptableObject, ISystemsProvider
    {
        internal class Dependencies
        {
            [Inject] public IAchievementsService achivementsService;
            [Inject] public IPreviousSceneService previousSceneService;
        }

        [SerializeField]
        private int HubSceneIndex;

        public EcsSystems GetSystems(EcsWorld world, EcsSystems endFrame, EcsSystems mainSystems)
        {
            Dependencies dependencies = new Dependencies();
            SceneContainer.Instance.Inject(dependencies);

            EcsSystems systems = new EcsSystems(world, "Base Achivements Systems");

            if (SceneManager.GetActiveScene().buildIndex != HubSceneIndex)
            {
                systems

                    .Add(new RXMessagePropagator<ReturnToHubMessage>())
                    .Add(new AchievementSceneHandler(dependencies.achivementsService,dependencies.previousSceneService))
                    .Add(new AchievementCollectionRecieveSystem(dependencies.achivementsService))

                    ;

                endFrame

                    .OneFrame<AchievementRequest>()

                    ;
            }
            else
            {

            }

            return systems;
        }
    }
}
