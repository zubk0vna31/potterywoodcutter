using PWS.Features.Achievements;
using PWS.Features.Achivements;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using PWS.FeatureToggles;

public class AchievementShowSystem : MonoBehaviour, IAchievementServiceReciver
{
    [SerializeField]
    private GameObject canvas;

    [SerializeField]
    private AchievementView viewPrefab;

    [SerializeField]
    private Transform content;

    [SerializeField]
    private Transform foldout;

    [SerializeField]
    private Button showButton,closeButton;

    [SerializeField]
    private Sprite bronze;
    [SerializeField]
    private Sprite silver;
    [SerializeField]
    private Sprite gold;
    [SerializeField]
    private Sprite locked;

    private AchievementsService service;
    private List<AchievementView> achievementViews;

    public void Start()
    {
        if (!FeatureManagerFacade.FeatureEnabled(PWS.HUB.Simulation.Features.PWS_Achievements))
        {
            canvas.SetActive(false);
            showButton.gameObject.SetActive(false);
        }
    }

    public void Recieve(AchievementsService service)
    {
        if (!service.Enabled)
        {
            canvas.gameObject.SetActive(false);
            showButton.gameObject.SetActive(false);
            return;
        };

        this.service = service;

        Populate();
        showButton.onClick.AddListener(ShowButtonPressed);
        closeButton.onClick.AddListener(CloseButtonPressed);

        canvas.SetActive(false);
    }

    private void Populate()
    {
        achievementViews = new List<AchievementView>();

        var achievements = service.GetAllAchievements();
        foreach (var a in achievements)
        {
            var view = Instantiate(viewPrefab, content);
            view.Initialize(a.IsUnlocked ? GetSpriteByRarity(a.Rarity) : locked, a.LocalizationKey);
            achievementViews.Add(view);
        }
    }

    private Sprite GetSpriteByRarity(Rarity rarity)
    {
        switch (rarity)
        {
            case Rarity.Bronze:
                return bronze;
            case Rarity.Silver:
                return silver;
            case Rarity.Gold:
                return gold;
            default:
                return bronze;
        }
    }

    private void ShowButtonPressed()
    {
        canvas.SetActive(!canvas.activeSelf);
    }

    private void CloseButtonPressed()
    {
        canvas.SetActive(false);
    }


    private void OnDestroy()
    {
        showButton.onClick.RemoveAllListeners();
        closeButton.onClick.RemoveAllListeners();
    }
}
