using System.Collections.Generic;
using UnityEngine;


namespace PWS.Features.Achievements
{
    internal struct AchievementQueueData
    {
        public Rarity rarity;
        public string localizationKey;
    }

    [RequireComponent(typeof(AudioSource))]
    public class AchievementViewOverlaySystem : MonoBehaviour,IAchievementServiceReciver
    {
        [SerializeField][Range(0.25f,5f)]
        private float duration;

        [SerializeField]
        private AudioClip unlockedSound;

        [SerializeField]
        private AchievementView view;

        [SerializeField]
        private Sprite bronze, silver, gold;

        private AchievementsService service;
        private AudioSource audioSource;
        private Queue<AchievementQueueData> queue;

        //public void Start()
        //{
        //    //Unlock("Test1");
        //    //Unlock("Test2");
        //    //Unlock("Test3");
        //}

        public void Recieve(AchievementsService service)
        {
            this.service = service;
            this.service.OnAchievementUnlocked += Unlock;

            audioSource = GetComponent<AudioSource>();

            // Hide it on start
            view.Hide();
        }

        public void Unlock(string key)
        {
            var achievement = service.GetAchievement(key);

            if (achievement is null) return;

            AchievementQueueData data = new AchievementQueueData();

            data.rarity = achievement.Rarity;
            data.localizationKey = achievement.LocalizationKey;

            if (queue is null) queue = new Queue<AchievementQueueData>();

            queue.Enqueue(data);

            if (queue.Count == 1)
            {
                Show();
            }
        }

        private void Show()
        {
            if (queue.Count <= 0) return;

            var data = queue.Peek();

            view.Initialize(GetRarirtyIcon(data.rarity), data.localizationKey);
            view.Unlock(duration, OnShown);

            if(unlockedSound)
                audioSource.PlayOneShot(unlockedSound);
        }

        private void OnShown()
        {
            queue.Dequeue();
            Show();
        }

        private Sprite GetRarirtyIcon(Rarity rarity)
        {
            switch (rarity)
            {
                case Rarity.Bronze:
                    return bronze;
                case Rarity.Silver:
                    return silver;
                case Rarity.Gold:
                    return gold;
                default:
                    return bronze;
            }
        }

        private void OnDestroy()
        {
            service.OnAchievementUnlocked -= Unlock;
        }

        
    }
}
