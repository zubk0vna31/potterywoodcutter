using DG.Tweening;
using I2.Loc;
using System;
using UnityEngine;
using UnityEngine.UI;

public class AchievementView : MonoBehaviour
{
    private const string localizationTittleSuffix = "_title";

    [SerializeField]
    private RectTransform rectTransform;

    [SerializeField]
    private Localize titleLocalize;

    [SerializeField]
    private Localize descriptionLocalize;

    [SerializeField]
    private Image image;

    public void Initialize(Sprite sprite,string localizationKey)
    {
        titleLocalize.SetTerm(localizationKey + localizationTittleSuffix);
        descriptionLocalize.SetTerm(localizationKey);

        image.sprite = sprite;
    }

    public void Unlock(float duration = 1.0f,Action callback=null)
    {
        transform.DOKill();
        transform.localPosition = Vector3.up * -100f;
        transform.localScale = Vector3.zero;
        transform.gameObject.SetActive(true);

        float scaleTime = duration * 0.25f;

        Sequence sequence = DOTween.Sequence();

        sequence.Pause();

        sequence.Append(transform.DOScale(Vector3.one, scaleTime));
        sequence.Join(transform.DOLocalMoveY(0, scaleTime));
        sequence.Join(transform.DOLocalRotate(Vector3.up * 360f,scaleTime).SetRelative(true));

        sequence.AppendInterval(scaleTime * 2f);

        sequence.Append(transform.DOScale(Vector3.zero, scaleTime));
        sequence.Join(transform.DOLocalMoveY(-rectTransform.sizeDelta.y, scaleTime));
        sequence.Join(transform.DOLocalRotate(-Vector3.up * 360f, scaleTime).SetRelative(true));

        sequence.AppendCallback(new TweenCallback(() => transform.gameObject.SetActive(false)));
        sequence.AppendCallback(new TweenCallback(callback));

        sequence.Play();
    }

    public void Hide()
    {
        transform.gameObject.SetActive(false);
        transform.localPosition = Vector3.up * -rectTransform.sizeDelta.y;
        transform.localScale = Vector3.zero;
    }
}
