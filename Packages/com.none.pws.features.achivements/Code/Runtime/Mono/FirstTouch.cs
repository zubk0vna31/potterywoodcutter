using Leopotam.Ecs;
using Modules.ViewHub;
using PWS.Features.Achievements;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class FirstTouch : ViewComponent
{
    [SerializeField]
    private XRGrabInteractable grabInteractable;

    [SerializeField]
    private AchievementSO achievement;

    [SerializeField]
    private string keyToUnlock;

    private bool touched;
    private EcsEntity ecsEntity;
    public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
    {
        this.ecsEntity = ecsEntity;
    }

    private void Start()
    {
        grabInteractable.selectEntered.AddListener(Grab);
    }

    private void Grab(SelectEnterEventArgs arg0)
    {
        if (!touched)
        {
            touched = true;
            ref var request = ref  ecsEntity.Get<AchievementRequest>();

            request.achievementKey = achievement.Key;
            request.keyToUnlock = keyToUnlock;
        }
    }

}
