using Modules.Root.ContainerComponentModel;
using UnityEngine;

namespace PWS.Features.Achievements
{
    public class AchivementsServiceInstaller : AMonoInstaller
    {
        public UnityEngine.Object reciever;

        public bool debugLoadedAchievements;

        private IApplicationQuit applicationQuit;

        public override void Install(IContainer container)
        {
            var achievementService = new AchievementsService();
            container.Bind(achievementService as IAchievementsService);

            if (debugLoadedAchievements && achievementService.Enabled)
            {
                Debug.Log($"<color=orange>Loaded achievements:</color>");

                foreach (var ach in achievementService.GetAllAchievements())
                {
                    Debug.Log($"<color=cyan>{ ach.Key}</color>");
                }
            }

            applicationQuit = achievementService;

            (reciever as IAchievementServiceReciver).Recieve(achievementService);
        }

        // OnApplicationQuit don't work on Quest/Quest2 for some reason. Wait for fix
        public void OnApplicationPause()
        {
#if UNITY_ANDROID
            if (!(applicationQuit is null))
                applicationQuit.OnApplicationQuit();
#endif
        }

        // OnApplicationQuit work only for editor
        public void OnApplicationQuit()
        {
#if UNITY_EDITOR
            if (!(applicationQuit is null))
                applicationQuit.OnApplicationQuit();
#endif
        }
    }
}
