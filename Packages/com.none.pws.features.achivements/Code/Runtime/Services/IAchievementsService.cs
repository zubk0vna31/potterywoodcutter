namespace PWS.Features.Achievements
{
    public interface IAchievementsService 
    {
        public void Unlock(string key);
        public void AppendProgressInt(string achievementKey,int append);
        public void AppendProgressFloat(string achievementKey,float append);
        public void AppendProgressString(string achievementKey,string append);
        public void AppendProgressKey(string achievementKey,string key,float value);
        public bool IsUnlocked(string achievementKey);
        public void Save();

        public int TryGetProgressInt(string achievementKey);
        public int TryGetReferenceInt(string achievementKey);
    }
}
