using PWS.Features.Achivements;
using PWS.FeatureToggles;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

namespace PWS.Features.Achievements
{
    public class AchievementsService : IAchievementsService, IApplicationQuit
    {
        private const float FILE_VERSION = 1.0f;
        private const string RESOURCE_PATH = "Features/Achievements";
        private const string SAVE_CONFIG_NAME = "achievements.json";

        private Dictionary<string, BaseAchievement> _achievements;

        public event Action<string> OnAchievementUnlocked;
        public bool Enabled => _enabled;

        private bool _enabled;

        public AchievementsService()
        {
            _enabled = FeatureManagerFacade.FeatureEnabled(PWS.HUB.Simulation.Features.PWS_Achievements);

            if (!_enabled) return;

            var arg = FeatureManagerFacade.FindArg("appType");
            ApplicationType appType;

            if (arg.Equals(FeaturesArgs.Woodcutting_App))
            {
                appType = ApplicationType.WoodCutting;
            }
            else if (arg.Equals(FeaturesArgs.Pottery_App))
            {
                appType = ApplicationType.Pottery;
            }
            else
            {
                _enabled = false;
                return;
            }

            _achievements = new Dictionary<string, BaseAchievement>();

            AchievementSO[] achievementsSO = Resources.LoadAll<AchievementSO>(RESOURCE_PATH);


            // Initialize all achievements in dictionary
            foreach (var so in achievementsSO)
            {
                if (so.ApplicationType != appType) continue;

                switch (so.Type)
                {
                    case AchievementType.Simple:
                        _achievements.Add(so.Key, new AchievementBool(
                            so.Key, so.LocalizationKey, so.Rarity, true, so.Appendable, ComparassionOperation.None));
                        break;
                    case AchievementType.Int:
                        _achievements.Add(so.Key, new AchievementInt(
                            so.Key, so.LocalizationKey, so.Rarity, so.IntValue, so.Appendable, so.CompareOperation));
                        break;
                    case AchievementType.Float:
                        _achievements.Add(so.Key, new AchievementFloat(
                            so.Key, so.LocalizationKey, so.Rarity, so.FloatValue, so.Appendable, so.CompareOperation));
                        break;
                    case AchievementType.StringCollection:
                        _achievements.Add(so.Key, new AchievementStringCollection(
                            so.Key, so.LocalizationKey, so.Rarity, so.StringCollection, so.Appendable));
                        break;
                    case AchievementType.Dictionary:
                        _achievements.Add(so.Key, new AchievementStringFloatDictionary(
                            so.Key, so.LocalizationKey, so.Rarity,
                            so.KeyValuePairs.ToDictionary(x => x.Key, y => y.Value), so.Appendable, so.CompareOperation));
                        break;
                    default:
                        break;
                }
            }

            // Load all achievements that already saved
            AchievementSaveConfig saveConfig = LoadSaveConfig();

            if (saveConfig.achievements != null && saveConfig.version.Equals(FILE_VERSION))
            {
                foreach (var data in saveConfig.achievements)
                {
                    if (_achievements.ContainsKey(data.key))
                    {
                        if (data.unlocked)
                        {
                            _achievements[data.key].Unlock();
                        }
                        else _achievements[data.key].LoadProgress(data.value);
                    }
                }
            }
        }

        private AchievementSaveConfig LoadSaveConfig()
        {
            string path = Application.persistentDataPath + "/" + SAVE_CONFIG_NAME;
            string json;

            AchievementSaveConfig saveConfig = new AchievementSaveConfig(FILE_VERSION);

            try
            {
                json = File.ReadAllText(path);
                saveConfig = JsonUtility.FromJson<AchievementSaveConfig>(json);
            }
            catch (System.Exception)
            {
                return saveConfig;
            }

            return saveConfig;
        }

        private void SaveConfig()
        {
            AchievementSaveConfig saveConfig = new AchievementSaveConfig(FILE_VERSION);
            List<AchievementSaveData> saveData = new List<AchievementSaveData>(_achievements.Count);

            foreach (var key in _achievements.Keys)
            {
                string str = _achievements[key].ToString();

                if (string.IsNullOrEmpty(str)) continue;

                if (!_achievements[key].IsDirty && !_achievements[key].IsUnlocked) continue;

                saveData.Add(new AchievementSaveData(key, str, _achievements[key].IsUnlocked));
            }

            saveConfig.achievements = saveData.ToArray();

            string path = Application.persistentDataPath + "/" + SAVE_CONFIG_NAME;
            string json;

            try
            {
                json = JsonUtility.ToJson(saveConfig, true);
                File.WriteAllText(path, json);
            }
            catch (System.Exception)
            {
                return;
            }
        }

        public void Unlock(string key)
        {
            if (!_enabled || !_achievements.ContainsKey(key) || _achievements[key].IsUnlocked) return;

            _achievements[key].Unlock();
            OnAchievementUnlocked?.Invoke(key);
        }

        public void AppendProgressInt(string key, int append)
        {
            if (!_enabled || !_achievements.ContainsKey(key) || _achievements[key].IsUnlocked) return;

            if (!(_achievements[key] is AchievementInt)) return;

            AchievementInt achvInt = (AchievementInt)_achievements[key];
            achvInt.AppendProgress(append);

            if (_achievements[key].IsUnlocked)
            {
                OnAchievementUnlocked?.Invoke(key);
            }
        }

        public void AppendProgressFloat(string key, float append)
        {
            if (!_enabled || !_achievements.ContainsKey(key) || _achievements[key].IsUnlocked) return;

            if (!(_achievements[key] is AchievementFloat)) return;

            AchievementFloat achvFloat = (AchievementFloat)_achievements[key];
            achvFloat.AppendProgress(append);

            if (_achievements[key].IsUnlocked)
            {
                OnAchievementUnlocked?.Invoke(key);
            }
        }

        public void AppendProgressString(string key, string append)
        {
            if (!_enabled || !_achievements.ContainsKey(key) || _achievements[key].IsUnlocked) return;

            if (!(_achievements[key] is AchievementStringCollection)) return;

            AchievementStringCollection achvInt = (AchievementStringCollection)_achievements[key];
            achvInt.AppendProgress(append);

            if (_achievements[key].IsUnlocked)
            {
                OnAchievementUnlocked?.Invoke(key);
            }
        }

        public void AppendProgressKey(string achievementKey, string key, float value)
        {
            if (!_enabled || !_achievements.ContainsKey(achievementKey) || _achievements[achievementKey].IsUnlocked) return;

            if (!(_achievements[achievementKey] is AchievementDictionary<string, float>)) return;

            AchievementDictionary<string, float> achvInt =
                (AchievementDictionary<string, float>)_achievements[achievementKey];

            achvInt.AppendProgress(key, value);

            if (_achievements[achievementKey].IsUnlocked)
            {
                OnAchievementUnlocked?.Invoke(achievementKey);
            }
        }

        public bool IsUnlocked(string key)
        {
            if (_achievements.ContainsKey(key))
            {
                return _achievements[key].IsUnlocked;
            }
            else return false;
        }

        public void Save()
        {
            if (!_enabled) return;


            SaveConfig();
        }

        public void OnApplicationQuit()
        {
            if (!_enabled) return;


            SaveConfig();
        }

        public BaseAchievement GetAchievement(string key)
        {
            if (!_enabled) return null;

            if (!_achievements.ContainsKey(key)) return null;

            return _achievements[key];
        }

        public List<BaseAchievement> GetAllAchievements()
        {
            if (!_enabled) return null;

            return _achievements.Values.ToList();
        }

        public int TryGetProgressInt(string achievementKey)
        {
            if (_achievements.ContainsKey(achievementKey) && _achievements[achievementKey] is Achievement<int>)
            {
                return (_achievements[achievementKey] as Achievement<int>).Value;
            }
            else return -1;
        }

        public int TryGetReferenceInt(string achievementKey)
        {
            if (_achievements.ContainsKey(achievementKey) && _achievements[achievementKey] is Achievement<int>)
            {
                return (_achievements[achievementKey] as Achievement<int>).Reference;
            }
            else return -1;
        }
    }
}
