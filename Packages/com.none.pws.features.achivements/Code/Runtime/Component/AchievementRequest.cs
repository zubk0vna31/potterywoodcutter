namespace PWS.Features.Achievements
{
    public struct AchievementRequest 
    {
        public string achievementKey;
        public string keyToUnlock;
    }
}
