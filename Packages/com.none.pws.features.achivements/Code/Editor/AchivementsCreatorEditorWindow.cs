using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace PWS.Features.Achievements.Editor
{
    public class AchivementsCreatorEditorWindow : EditorWindow
    {
        private const string ACHIVEMENT_FOLDER_PATH = "Features/Achievements";
        private const string ACHIVEMENT_FOLDER_EDITOR_PATH = "Assets/Resources/Features/Achievements";

        private const float defaultWidth = 700;
        private const float defaultHeight = 400;

        private static Color selectedAchivementColor = new Color(0f, 1f, 0.15f, 1f);

        private static bool initialized;

        private GUIStyle achivementStyle;
        private GUIStyle fileNameStyle;

        private ApplicationType currentAppType;
        private SerializedObject[] serializedObjects;
        private int selectedIndex = -1;

        private string fileName;
        private string createdName;
        private Vector2 scrollPosition;
        private Vector2 scrollPositionRightBar;

        public ApplicationType CurrentAppType
        {
            get => currentAppType;
            set
            {
                if (currentAppType != value)
                {
                    currentAppType = value;
                    Initialize();
                }
            }
        }

        [MenuItem("Tools/Achivement Creator")]
        public static void OpenWindow()
        {
            //Get existing open window or if none, make a new one:
            AchivementsCreatorEditorWindow window = GetWindow<AchivementsCreatorEditorWindow>();



            //Initialize 
            window.titleContent = new GUIContent("Achivement Creator");
            window.minSize = new Vector2(defaultWidth, defaultHeight);

            //Centering
            var pos = EditorGUIUtility.GetMainWindowPosition();
            window.position = new Rect(
                (pos.width - defaultWidth) * 0.5f + pos.x,
                (pos.height - defaultHeight) * 0.5f + pos.y,
                defaultWidth, defaultHeight);

            initialized = false;

            window.Show();
        }

        public void OnGUI()
        {
            TryInitialize();

            EditorGUILayout.BeginHorizontal(GUILayout.ExpandHeight(true));

            //A list of all achivements in project
            var scrollRect = DrawLeftSideBar(out var achivementRects);
            //A current selected achivement(if selected) and controll buttons
            DrawRightSideBar();

            //Get mouse input
            MouseInput(scrollRect, achivementRects);

            EditorGUILayout.EndHorizontal();
        }

        private Rect DrawLeftSideBar(out List<Rect> achivementRects)
        {
            Rect scrollRect = new Rect();
            achivementRects = new List<Rect>();

            EditorGUILayout.BeginVertical(GUI.skin.GetStyle("box"), GUILayout.Width(position.width * 0.3f),
                GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(true));

            CurrentAppType = (ApplicationType)EditorGUILayout.EnumPopup(CurrentAppType);

            if (serializedObjects != null)
            {
                scrollPosition = EditorGUILayout.BeginScrollView(scrollPosition, GUIStyle.none,
                    GUI.skin.verticalScrollbar, GUILayout.ExpandHeight(true));

                for (int i = 0; i < serializedObjects.Length; i++)
                {
                    achivementRects.Add(DrawAchivement(serializedObjects[i], i));
                }

                EditorGUILayout.EndScrollView();

                scrollRect = GUILayoutUtility.GetLastRect();
            }

            GUILayout.FlexibleSpace();

            fileName = EditorGUILayout.TextField(fileName, fileNameStyle, GUILayout.Height(32),
                GUILayout.ExpandWidth(true));

            EditorGUILayout.EndVertical();

            return scrollRect;
        }

        private void DrawRightSideBar()
        {
            EditorGUILayout.BeginVertical(GUI.skin.GetStyle("box"));

            //Selected achivement
            if (selectedIndex >= 0)
            {
                EditorGUILayout.BeginVertical(GUI.skin.GetStyle("helpBox"), GUILayout.ExpandHeight(true));

                scrollPositionRightBar = EditorGUILayout.BeginScrollView(scrollPositionRightBar, GUIStyle.none,
                   GUI.skin.verticalScrollbar, GUILayout.ExpandHeight(true));

                DrawSelectedAchivement();

                EditorGUILayout.EndScrollView();

                EditorGUILayout.EndVertical();
            }
            else
            {
                GUILayout.FlexibleSpace();
            }


            //Control buttons
            EditorGUILayout.BeginHorizontal(GUILayout.Height(32));


            if (GUILayout.Button("Create (+)", GUILayout.ExpandHeight(true)))
            {
                Create();
            }

            if (GUILayout.Button("Delete (-)", GUILayout.ExpandHeight(true)))
            {
                Delete();
            }

            if (GUILayout.Button("Refresh (~)", GUILayout.ExpandHeight(true)))
            {
                Refresh();
            }

            if (GUILayout.Button("Rename (*)", GUILayout.ExpandHeight(true)))
            {
                Rename();
            }

            EditorGUILayout.EndHorizontal();


            EditorGUILayout.EndVertical();
        }

        private Rect DrawAchivement(SerializedObject serializedObject, int index)
        {
            if (GUILayout.Button(serializedObject.targetObject.name, selectedIndex == index ? achivementStyle :
                GUI.skin.button, GUILayout.Height(32)))
            {
                selectedIndex = index;
                GUI.FocusControl(null);
                Repaint();
            }

            return GUILayoutUtility.GetLastRect();
        }

        private void DrawSelectedAchivement()
        {
            var appTypeProp = serializedObjects[selectedIndex].FindProperty("ApplicationType");
            ApplicationType appType = GetEnumValue<ApplicationType>(appTypeProp.enumValueIndex);
            appTypeProp.enumValueIndex = (int)((ApplicationType)EditorGUILayout.EnumPopup(appType));

            if(appTypeProp.enumValueIndex != (int)CurrentAppType)
            {
                serializedObjects[selectedIndex].ApplyModifiedProperties();
                Initialize();
                Repaint();
                return;
            }

            EditorGUILayout.PropertyField(serializedObjects[selectedIndex].FindProperty("Key"));

            EditorGUILayout.PropertyField(serializedObjects[selectedIndex].FindProperty("LocalizationKey"));


            EditorGUILayout.PropertyField(serializedObjects[selectedIndex].FindProperty("Rarity"));

            var typeProp = serializedObjects[selectedIndex].FindProperty("Type");
            EditorGUILayout.PropertyField(typeProp);

            EditorGUILayout.PropertyField(serializedObjects[selectedIndex].FindProperty("Description"),
             GUILayout.Height(96));

            AchievementType type = GetEnumValue<AchievementType>(typeProp.enumValueIndex);

            switch (type)
            {
                case AchievementType.Simple:
                    break;
                case AchievementType.Int:
                    {
                        EditorGUILayout.PropertyField(serializedObjects[selectedIndex].FindProperty("Appendable"));
                        EditorGUILayout.PropertyField(serializedObjects[selectedIndex].FindProperty("IntValue"));
                        EditorGUILayout.PropertyField(serializedObjects[selectedIndex].FindProperty("CompareOperation"));
                        break;
                    }
                case AchievementType.Float:
                    {
                        EditorGUILayout.PropertyField(serializedObjects[selectedIndex].FindProperty("Appendable"));
                        EditorGUILayout.PropertyField(serializedObjects[selectedIndex].FindProperty("FloatValue"));
                        EditorGUILayout.PropertyField(serializedObjects[selectedIndex].FindProperty("CompareOperation"));
                        break;
                    }
                case AchievementType.StringCollection:
                    {
                        EditorGUILayout.PropertyField(serializedObjects[selectedIndex].FindProperty("Appendable"));
                        EditorGUILayout.PropertyField(serializedObjects[selectedIndex].FindProperty("StringCollection"));

                        break;
                    }

                case AchievementType.Dictionary:
                    {
                        EditorGUILayout.PropertyField(serializedObjects[selectedIndex].FindProperty("Appendable"));
                        EditorGUILayout.PropertyField(serializedObjects[selectedIndex].FindProperty("CompareOperation"));
                        DrawDictionary();

                        break;
                    }
                default:
                    {
                        break;
                    }
            }


            serializedObjects[selectedIndex].ApplyModifiedProperties();
        }

        public void DrawProperties(SerializedProperty prop, bool drawChildren, string displayName = "")
        {
            string lastPropPath = string.Empty;
            bool overrideDisplayName = !string.IsNullOrEmpty(displayName);

            if (prop.hasChildren)
            {
                foreach (SerializedProperty p in prop)
                {
                    if (p.isArray && p.propertyType == SerializedPropertyType.Generic)
                    {
                        EditorGUILayout.BeginHorizontal();
                        p.isExpanded = EditorGUILayout.Foldout(p.isExpanded,
                            overrideDisplayName ? displayName : p.displayName);
                        EditorGUILayout.EndHorizontal();

                        if (p.isExpanded)
                        {
                            EditorGUI.indentLevel++;
                            DrawProperties(p, drawChildren);
                            EditorGUI.indentLevel--;
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(lastPropPath) && p.propertyPath.Contains(lastPropPath)) { continue; }
                        lastPropPath = p.propertyPath;
                        EditorGUILayout.PropertyField(p, drawChildren);
                    }
                }
            }
            else
            {
                EditorGUILayout.PropertyField(prop, drawChildren);
            }
        }

        private void DrawDictionary()
        {
            var obj = serializedObjects[selectedIndex];

            var keys = obj.FindProperty("KeyValuePairs");

            EditorGUILayout.BeginVertical(GUI.skin.box);

            EditorGUILayout.BeginHorizontal(GUI.skin.GetStyle("helpBox"));

            EditorGUILayout.LabelField("String Float Dictionary", GUILayout.ExpandWidth(true),
                GUILayout.Height(24), GUILayout.ExpandHeight(false));

            EditorGUILayout.EndHorizontal();

            for (int i = 0; i < keys.arraySize; i++)
            {
                var prop = keys.GetArrayElementAtIndex(i);

                EditorGUILayout.BeginHorizontal(GUI.skin.button);

                EditorGUILayout.LabelField("Key", GUILayout.Width(30));
                prop.FindPropertyRelative("Key").stringValue = EditorGUILayout.TextField(
                    prop.FindPropertyRelative("Key").stringValue,
                    GUILayout.ExpandWidth(true));

                EditorGUILayout.LabelField("Value", GUILayout.Width(50));
                prop.FindPropertyRelative("Value").floatValue = EditorGUILayout.FloatField(
                     prop.FindPropertyRelative("Value").floatValue, GUILayout.Width(100));

                EditorGUILayout.EndHorizontal();
            }

            EditorGUILayout.BeginHorizontal();

            GUILayout.FlexibleSpace();

            float buttonsWidth = 38;
            float buttonsHeight = 24;

            if (GUILayout.Button("+", GUILayout.Width(buttonsWidth), GUILayout.Height(buttonsHeight)))
            {
                keys.arraySize++;
            }

            if (GUILayout.Button("-", GUILayout.Width(buttonsWidth), GUILayout.Height(buttonsHeight)))
            {
                if (keys.arraySize > 0)
                {
                    keys.arraySize--;
                }
            }

            EditorGUILayout.EndHorizontal();

            EditorGUILayout.EndVertical();
        }

        private void TryInitialize()
        {
            if (initialized) return;
            initialized = true;

            //Create achivement list button style
            achivementStyle = new GUIStyle(GUI.skin.button);
            achivementStyle.normal.textColor =
                achivementStyle.active.textColor =
                achivementStyle.focused.textColor =
                achivementStyle.hover.textColor = selectedAchivementColor;
            achivementStyle.fontSize = 14;

            //Create file name text field style
            fileNameStyle = new GUIStyle(GUI.skin.textField);
            fileNameStyle.fontSize = 14;
            fileNameStyle.alignment = TextAnchor.MiddleLeft;

            Initialize();
        }

        private void MouseInput(Rect insideRect, List<Rect> targetRects)
        {
            if (Event.current.type != EventType.MouseDown || targetRects.Count < 1
                || !insideRect.Contains(Event.current.mousePosition)) return;

            for (int i = 0; i < targetRects.Count; i++)
            {
                if (targetRects[i].Contains(Event.current.mousePosition))
                {
                    break;
                }
            }

            selectedIndex = -1;

            Repaint();
        }

        private void Initialize()
        {
            //Load all achivements
            AchievementSO[] achivements = Resources.LoadAll<AchievementSO>(ACHIVEMENT_FOLDER_PATH);

            var list = new List<SerializedObject>(achivements.Length);
            int listCounter = 0;
            int createdIndex = -1;
            for (int i = 0; i < achivements.Length; i++)
            {
                if (achivements[i].ApplicationType == currentAppType)
                {
                    if (createdIndex<0 && achivements[i].name.Equals(createdName))
                    {
                        createdIndex = listCounter;
                    }

                    list.Add(new SerializedObject(achivements[i]));
                    listCounter++;
                }
            }

            serializedObjects = list.ToArray();
            selectedIndex = -1;

            if (createdIndex>=0) selectedIndex = createdIndex;

            //Repaint to update info in window
            Repaint();
        }

        private void Refresh()
        {
            Initialize();
        }

        private void Create()
        {
            if (string.IsNullOrEmpty(fileName))
            {
                Debug.LogWarning("File name is empty/null");
                return;
            }

            if (fileName.IndexOfAny(new char[] { '\\', '/', '|' }) >= 0)
            {
                Debug.LogWarning("File name contains wrong symbols!");
                return;
            }

            string loadPath = ACHIVEMENT_FOLDER_EDITOR_PATH + "/" + fileName + ".asset";

            if (AssetDatabase.LoadAssetAtPath(
                loadPath, typeof(AchievementSO)) != null)
            {
                bool dialogResult = EditorUtility.DisplayDialog("Creating achivement",
                    "Achivement with such name is already exits. Do you wanna to overwrite it?",
                    "Overwrite", "Cancel");

                if (dialogResult)
                {
                    AssetDatabase.DeleteAsset(loadPath);
                }
                else
                {
                    return;
                }
            }

            AchievementSO achivement = CreateInstance<AchievementSO>();
            achivement.Key = fileName;
            achivement.ApplicationType = CurrentAppType;

            AssetDatabase.CreateAsset(achivement, loadPath);
            AssetDatabase.Refresh();

            EditorUtility.SetDirty(achivement);

            createdName = fileName;
            fileName = "";
            GUI.FocusControl(null);


            Refresh();
        }

        private void Delete()
        {
            var fileName = serializedObjects[selectedIndex].targetObject.name;

            SerializedObject[] copy = new SerializedObject[serializedObjects.Length - 1];

            for (int i = 0, j = 0; i < serializedObjects.Length; i++)
            {
                if (i == selectedIndex)
                {
                    continue;
                }

                copy[j] = serializedObjects[i];

                j++;
            }

            serializedObjects = copy;

            AssetDatabase.DeleteAsset(ACHIVEMENT_FOLDER_EDITOR_PATH + "/" + fileName + ".asset");
            AssetDatabase.Refresh();

            selectedIndex = -1;
            Repaint();
        }

        private void Rename()
        {
            if (string.IsNullOrEmpty(fileName))
            {
                Debug.LogWarning("File name is empty/null");
                return;
            }

            string loadPath = ACHIVEMENT_FOLDER_EDITOR_PATH + "/"
                + serializedObjects[selectedIndex].targetObject.name + ".asset";

            AssetDatabase.RenameAsset(loadPath, fileName);
            AssetDatabase.Refresh();

            EditorUtility.SetDirty(serializedObjects[selectedIndex].targetObject);

            fileName = "";
            GUI.FocusControl(null);

            Refresh();
        }

        private T GetEnumValue<T>(int enumIndex) where T : Enum
        {
            try
            {
                return (T)Enum.Parse(typeof(T), enumIndex.ToString());
            }
            catch
            {
                return default(T);
            }
        }

        private int GetIntValue<T>(T value) where T : Enum
        {
            try
            {
                return Convert.ToInt32(value);
            }
            catch
            {
                return 0;
            }
        }
    }
}
