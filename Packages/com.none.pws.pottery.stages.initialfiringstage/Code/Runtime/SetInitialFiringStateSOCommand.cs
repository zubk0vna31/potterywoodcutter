using Modules.StateGroup.Core;
using UnityEngine;

namespace PWS.Pottery.Stages.InitialFiringStage
{
    [CreateAssetMenu(fileName = "SetInitialFiringStateSOCommand", menuName = "PWS/Pottery/Stages/InitialFiringStage/SetStateSOCommand")]
    public class SetInitialFiringStateSOCommand : SetStateSOCommand<InitialFiringItemPlacingState>
    {
    }
}
