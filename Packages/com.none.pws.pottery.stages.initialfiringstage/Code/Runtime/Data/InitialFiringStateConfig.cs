using UnityEngine;

namespace PWS.Pottery.Stages.InitialFiringStage
{
    [CreateAssetMenu(fileName = "InitialFiringStateConfig", menuName = "PWS/Pottery/Stages/InitialFiringStage/Config", order = 0)]
    public class InitialFiringStateConfig : ScriptableObject
    {
        public float TargetHours = 6;
        public float MinTemperature = 850;
        public float MaxTemperature = 900;

        [Header("Stove Door")]
        public Vector3 OpenRotation;
        public Vector3 CloseRotation;
        public float Duration;

        [Header("Material")]
        public float Firing = 0.4f;
        public float FiringSpecular = 0.8f;
    }
}
