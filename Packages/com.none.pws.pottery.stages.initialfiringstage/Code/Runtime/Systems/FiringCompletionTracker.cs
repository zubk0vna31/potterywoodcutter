﻿
using Leopotam.Ecs;
using PWS.Common.UI;
using Modules.StateGroup.Components;
using PWS.Common.ValueRegulator;
using PWS.Pottery.ItemDataHolderService;
using Modules.VRFeatures;

namespace PWS.Pottery.Stages.InitialFiringStage
{
    public class FiringCompletionTracker<StateT> : IEcsRunSystem where StateT : struct
    {

        // auto injected fields
        readonly EcsFilter<StateEnter, StateT> _entering;
        readonly EcsFilter<StateT> _inState;
        readonly EcsFilter<StateWindow, StateProgress> _stateWindowWithProgress;

        readonly EcsFilter<ValueRegulatorChangedSignal, DryingRegulator> _changedSignal;

        private ValueRegulatorConfig _hoursConfig;
        private ValueRegulatorConfig _temperatureConfig;
        private IValueRegulatorDataHolder<HoursRegulator> _hoursData;
        private IValueRegulatorDataHolder<TemperatureRegulator> _temperatureData;

        //runtime data
        private float _minValueHours;
        private float _maxValueHours;
        private float _minValueTemperature;
        private float _maxValueTemperature;

        public FiringCompletionTracker(ValueRegulatorConfig hoursConfig, ValueRegulatorConfig temperatureConfig,
            IValueRegulatorDataHolder<HoursRegulator> hoursData, IValueRegulatorDataHolder<TemperatureRegulator> temperatureData)
        {
            _hoursConfig = hoursConfig;
            _temperatureConfig = temperatureConfig;
            _hoursData = hoursData;
            _temperatureData = temperatureData;

            _minValueHours = _hoursData.ConvertFromCustom(_hoursConfig.CompletionCustomValueMin);
            _maxValueHours = _hoursData.ConvertFromCustom(_hoursConfig.CompletionCustomValueMax);
            _minValueTemperature = _temperatureData.ConvertFromCustom(_temperatureConfig.CompletionCustomValueMin);
            _maxValueTemperature = _temperatureData.ConvertFromCustom(_temperatureConfig.CompletionCustomValueMax);
        }

        public void Run()
        {
            if (!_entering.IsEmpty())
            {
                foreach (var i in _stateWindowWithProgress)
                {
                    _stateWindowWithProgress.Get2(i).Value = 0;
                    _stateWindowWithProgress.Get2(i).CompletionThreshold = 0.5f;
                }
            }

            if (!_inState.IsEmpty())
            {
                bool show = _hoursData.Value >= _minValueHours && _hoursData.Value <= _maxValueHours;
                show &= _temperatureData.Value >= _minValueTemperature && _temperatureData.Value <= _maxValueTemperature;

                foreach (var i in _stateWindowWithProgress)
                {
                    _stateWindowWithProgress.Get2(i).Value = show ? 1 : 0;
                }
            }
        }
    }
}