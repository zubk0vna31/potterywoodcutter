using Leopotam.Ecs;
using Modules.StateGroup.Components;
using DG.Tweening;
using Modules.ViewHub;

namespace PWS.Pottery.Stages.InitialFiringStage
{
    public class StoveDoorProcessing<StateT> : IEcsRunSystem where StateT : struct
    {
        // auto injected fields
        readonly EcsFilter<StateEnter, StateT> _entering;
        readonly EcsFilter<StateExit, StateT> _exiting;
        readonly EcsFilter<UnityView, StoveDoor> _door;

        private InitialFiringStateConfig _config;

        public StoveDoorProcessing(InitialFiringStateConfig config)
        {
            _config = config;
        }

        public void Run()
        {
            if (!_entering.IsEmpty())
            {
                foreach (var door in _door)
                {
                    _door.Get1(door).Transform.DOLocalRotate(_config.CloseRotation, _config.Duration);
                }
            }
            if (!_exiting.IsEmpty())
            {
                foreach (var door in _door)
                {
                    _door.Get1(door).Transform.DOLocalRotate(_config.OpenRotation, _config.Duration);
                }
            }
        }
    }
}
