﻿using Leopotam.Ecs;
using Modules.StateGroup.Components;
using PWS.Pottery.Common;
using PWS.Pottery.MeshGeneration;

namespace PWS.Pottery.Stages.InitialFiringStage
{
    public class InitialFiringBlackProcessing : IEcsRunSystem
    {
        // auto injected fields
        readonly EcsFilter<StateExit, InitialFiringState> _exiting;

        readonly EcsFilter<ClayMeshRenderer, SculptingProductTag> _meshRenderer;

        private InitialFiringStateConfig _config;
        private const string FIRING_ID = "_Firing";

        public InitialFiringBlackProcessing(InitialFiringStateConfig config)
        {
            _config = config;
        }

        public void Run()
        {
            if (!_exiting.IsEmpty())
            {
                foreach (var mr in _meshRenderer)
                {
                    _meshRenderer.Get1(mr).MeshRenderer.sharedMaterial.SetFloat(FIRING_ID, _config.Firing);
                }
            }
        }
    }
}