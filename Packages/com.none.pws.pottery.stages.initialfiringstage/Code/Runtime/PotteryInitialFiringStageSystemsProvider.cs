using Leopotam.Ecs;
using Modules.Root.ContainerComponentModel;
using Modules.Root.ECS;
using Modules.StateGroup.Core;
using Modules.VRFeatures;
using PWS.Common.Audio;
using PWS.Common.Messages;
using PWS.Common.RestartStage;
using PWS.Common.ResultsEvaluation;
using PWS.Common.SceneSwitchService;
using PWS.Common.TravelPoints;
using PWS.Common.UI;
using PWS.Common.ValueRegulator;
using PWS.Pottery.Common;
using PWS.Pottery.ItemDataHolderService;
using PWS.Pottery.MeshGeneration;
using UnityEngine;

namespace PWS.Pottery.Stages.InitialFiringStage
{
    public class PotteryInitialFiringStageSystemsProvider : MonoBehaviour, ISystemsProvider
    {
        internal class SystemsDependencies
        {
            [Inject] public IValueRegulatorDataHolder<HoursRegulator> HoursData;
            [Inject] public IValueRegulatorDataHolder<TemperatureRegulator> TemperatureData;
            [Inject] public IResultsEvaluationDataHolder ResultsEvaluationData;
            [Inject] public ISceneSwitchService SceneSwitchService;
        }

        [SerializeField] private SetStateSOCommand _nextState;
        [SerializeField] private InitialFiringStateConfig _config;
        [SerializeField] private ValueRegulatorConfig _hoursRegulatorConfig;
        [SerializeField] private ValueRegulatorConfig _temperatureRegulatorConfig;
        [SerializeField] private StateInfoData _stateInfo;
        
        [Header("Voice config")] 
        [SerializeField] private VoiceConfig _itemPlacingState;
        [SerializeField] private VoiceConfig _initialFiringState;

        public EcsSystems GetSystems(EcsWorld world, EcsSystems endFrame, EcsSystems mainSystems)
        {
            SystemsDependencies dependencies = new SystemsDependencies();
            SceneContainer.Instance.Inject(dependencies);

            EcsSystems systems = new EcsSystems(world);
            systems
                .Add(new FadeToStateOnUIActionSystem<InitialFiringState>(_nextState, dependencies.SceneSwitchService))
                .Add(new CurrentStateRestoreDataProcessing<InitialFiringState, ValueRegulatorDataHolder<HoursRegulator>>(dependencies.HoursData))
                .Add(new CurrentStateRestoreDataProcessing<InitialFiringState, ValueRegulatorDataHolder<TemperatureRegulator>>(dependencies.TemperatureData))

                //Outline
                .Add(new OutlineByStateSystem<InitialFiringItemPlacingState>(0))

                //item placing state systems
                .Add(new SnapSocketActivityProcessing<InitialFiringItemPlacingState, FiringSnapSocket>())

                .Add(new FiringItemPlacingProcessing<InitialFiringItemPlacingState, InitialFiringState>())

                .Add(new MeshVisibilityProcessing<InitialFiringItemPlacingState, SculptingProductMesherTag>())
                .Add(new XRInteractableProcessing<InitialFiringItemPlacingState, SculptingProductTag>())

                .Add(new StateInfoWindowUIProcessing<InitialFiringItemPlacingState>(_stateInfo))

                //state systems
                .Add(new StoveDoorProcessing<InitialFiringState>(_config))
                .Add(new GameObjectVisibilityProcessing<InitialFiringState, FiringContainer>())

                .Add(new GameObjectVisibilityProcessing<InitialFiringState, HoursRegulator>())
                .Add(new ValueRegulatorSetupProcessing<InitialFiringState, HoursRegulator>(_hoursRegulatorConfig, dependencies.HoursData, true))
                .Add(new GameObjectVisibilityProcessing<InitialFiringState, TemperatureRegulator>())
                .Add(new ValueRegulatorSetupProcessing<InitialFiringState, TemperatureRegulator>(_temperatureRegulatorConfig, dependencies.TemperatureData, true))

                .Add(new FiringCompletionTracker<InitialFiringState>(_hoursRegulatorConfig, _temperatureRegulatorConfig, dependencies.HoursData, dependencies.TemperatureData))
                .Add(new InitialFiringBlackProcessing(_config))


                .Add(new SetGradeByValueRegulator<InitialFiringState, HoursRegulator>(dependencies.ResultsEvaluationData,
                dependencies.HoursData, _stateInfo, _config.TargetHours))
                .Add(new SetGradeByValueRegulator<InitialFiringState, TemperatureRegulator>(dependencies.ResultsEvaluationData,
                dependencies.TemperatureData, _stateInfo, _config.MinTemperature, _config.MaxTemperature, true))

                .Add(new StateInfoWindowUIProcessing<InitialFiringState>(_stateInfo))
                .Add(new StateWindowPlacementProcessing<InitialFiringState>("Firing"))
                .Add(new StateWindowButtonUIProcessing<InitialFiringState>())

                .Add(new XRInteractableProcessing<InitialFiringState, SculptingProductTag>())

                .Add(new MeshVisibilityProcessing<InitialFiringState, SculptingProductTag>())

                //voice
                .Add(new VoiceAudioSystem<InitialFiringItemPlacingState>(_itemPlacingState))
                .Add(new VoiceAudioSystem<InitialFiringState>(_initialFiringState))
                
                //sounds
                .Add(new StoveDoorSoundSystem<InitialFiringState>())
                .Add(new StoveSoundSystem<InitialFiringState>(dependencies.TemperatureData))

                //travel point
                .Add(new TravelPointPositioningSystem<InitialFiringItemPlacingState>("Stelaj"))
                .Add(new TravelPointPositioningSystem<InitialFiringState>("Stelaj"))

                ;

            endFrame
                .Add(new ValueRegulatorUpdateProcessing<InitialFiringState, HoursRegulator>(dependencies.HoursData))
                .Add(new ValueRegulatorUpdateProcessing<InitialFiringState, TemperatureRegulator>(dependencies.TemperatureData))

                .OneFrame<RestartCurrentStepMessage>()
                .OneFrame<NextStateSignal>()
                .OneFrame<ValueRegulatorChangedSignal>()
                ;

            return systems;
        }
    }
}
