﻿using Leopotam.Ecs;

namespace PWS.Pottery.Stages.InitialFiringStage
{
    public struct InitialFiringState : IEcsIgnoreInFilter
    {
    }
    public struct InitialFiringItemPlacingState : IEcsIgnoreInFilter
    {
    }
}