using Leopotam.Ecs;
using Modules.ViewHub;

namespace PWS.Pottery.Stages.InitialFiringStage
{
    public class FiringContainerView : ViewComponent
    {
        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            ecsEntity.Get<FiringContainer>();
        }
    }
}
