using Leopotam.Ecs;
using Modules.ViewHub;
using PWS.Pottery.ItemDataHolderService;

namespace PWS.Pottery.Stages.InitialFiringStage
{
    public class TemperatureRegulatorView : ViewComponent
    {
        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            ecsEntity.Get<TemperatureRegulator>();
        }
    }
}
