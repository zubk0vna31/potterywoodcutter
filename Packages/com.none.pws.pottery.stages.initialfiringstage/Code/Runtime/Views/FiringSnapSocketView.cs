using Leopotam.Ecs;
using Modules.ViewHub;

namespace PWS.Pottery.Stages.InitialFiringStage
{
    public class FiringSnapSocketView : ViewComponent
    {
        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            ecsEntity.Get<FiringSnapSocket>();
        }
    }
}
