using Leopotam.Ecs;
using Modules.ViewHub;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

namespace PWS.WoodCutting {

    public class GlueTemplate : EntityTemplate
    {
        [SerializeField]
        private XRGrabInteractable grabInteractable;

        [SerializeField]
        private Transform ray;

        private EcsEntity entity;

        protected override void SetupEntity(EcsEntity entity, EcsWorld world)
        {
            base.SetupEntity(entity, world);

            this.entity = entity;

            entity.Get<GlueContainter>().ray = ray;

            grabInteractable.selectEntered.AddListener(SelectEnter);
            grabInteractable.selectExited.AddListener(SelectExit);
            grabInteractable.activated.AddListener(Activated);
            grabInteractable.deactivated.AddListener(Deactivated);
        }

        private void SelectEnter(SelectEnterEventArgs args)
        {
            entity.Get<SelectStay>();
        }

        private void SelectExit(SelectExitEventArgs args)
        {
            entity.Del<SelectStay>();

            entity.Get<GlueContainter>().Enabled = false;
        }

        private void Activated(ActivateEventArgs args)
        {
            entity.Get<GluePressed>();
            entity.Get<GlueContainter>().Enabled = true;
        }

        private void Deactivated(DeactivateEventArgs args)
        {
            entity.Get<GlueContainter>().Enabled = false;
            entity.Del<GluePressed>();
        }
    }
}