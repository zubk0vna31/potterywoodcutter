using Leopotam.Ecs;
using Modules.ViewHub;
using System.Linq;
using UnityEngine;

namespace PWS.WoodCutting
{
    public class PlankGluedTemplate : EntityTemplate
    {
        [SerializeField]
        private GlueingStateConfig _config;

        [SerializeField, Range(0, 2)]
        private int _drawOrder;

        protected override void SetupEntity(EcsEntity entity, EcsWorld world)
        {
            base.SetupEntity(entity, world);

            ref var plank = ref entity.Get<PlankGlued>();

            plank.drawOrder = _drawOrder;
            

            var ratingSystem = GetComponentsInChildren<GlueRatingSystem>().ToArray();
            plank.drawEntities = new EcsEntity[ratingSystem.Length];

            for (int i = 0; i < plank.drawEntities.Length; i++)
            {
                plank.drawEntities[i] = world.NewEntity();
                ref var drawSide = ref plank.drawEntities[i].Get<PlankDrawSide>();

                drawSide.glueRatingSystem = ratingSystem[i];
                drawSide.glueRatingSystem.SetConfigData(_config.successPercentage, _config.rateDuration);
                drawSide.plankEntity = entity;
                drawSide.collider = ratingSystem[i].GetComponentInChildren<Collider>();

                drawSide.collider.enabled = false;

                ref var unityView = ref plank.drawEntities[i].Get<UnityView>();

                unityView.GameObject = ratingSystem[i].gameObject;
                unityView.Transform = ratingSystem[i].transform;

                drawSide.OnGluedSide += () => entity.Get<PlankGlued>().currentSide++;
                drawSide.order=-1;
            }
        }
    }
}
