using Modules.StateGroup.Core;
using UnityEngine;

namespace PWS.WoodCutting
{
    [CreateAssetMenu(menuName = "PWS/WoodCutting/Stages/Glueing/SetStateCommand")]
    public class SetGlueingStateSOCommand : SetStateSOCommand<GlueingState>
    {

    }
}