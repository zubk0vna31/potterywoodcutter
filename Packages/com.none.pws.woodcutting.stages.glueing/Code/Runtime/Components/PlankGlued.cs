using Leopotam.Ecs;
using PWS.Common.GameModeInfoService;
using UnityEngine;

namespace PWS.WoodCutting
{
    public struct PlankGlued
    {
        public int drawOrder;
        public EcsEntity next;

        public Transform glueHolder;

        public Vector3 initialLocalPosition;
        public Vector3 initialLocalRotation;

        public EcsEntity[] drawEntities;
        public int currentSide;

        public bool onDrawPosition;
        public bool inited;
        public bool switchSideAfterRating;

        public bool IsCompleted(GameMode difficulty)
        {

            if (difficulty.Equals(GameMode.Free))
            {
                Debug.Log("HERE0");
                Debug.Log($"Current:{currentSide}");
                Debug.Log($"Draw:{drawEntities.Length}");

                return currentSide ==drawEntities.Length;
            }
            else
            {
                int completedAmount = 0;
                for (int i = 0; i < drawEntities.Length; i++)
                {
                    if (drawEntities[i].Get<PlankDrawSide>().Completed) completedAmount++;
                }

                return completedAmount == drawEntities.Length;
            }
        }
    }
}
