using System.Threading;
using UnityEngine;

namespace PWS.WoodCutting
{
    public struct Drawable 
    {
        private float timer;
        private float duration;
        private bool inited;

        public float Timer
        {
            get => timer;
            set
            {
                if (!inited)
                {
                    inited = true;
                    duration = value;
                }

                timer = value;
            }
        }

        public bool Update(float dt)
        {
            if (Timer > 0)
            {
                Timer -= dt;

                if (Timer <= 0)
                {
                    Timer = duration;
                    return true;
                }
            }

            return false;
        }
    }
}
