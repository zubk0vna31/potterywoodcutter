using PWS.Common.GameModeInfoService;
using UnityEngine.UI;

namespace PWS.WoodCutting
{
    public struct ShowRateVisual
    {
        public float Timer;
        public GameMode Difficulty;
    }
}
