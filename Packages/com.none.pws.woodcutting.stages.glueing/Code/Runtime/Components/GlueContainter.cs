using UnityEngine;

namespace PWS.WoodCutting
{
    public struct GlueContainter 
    {
        public Transform ray;
        
        public bool Enabled
        {
            get => ray.gameObject.activeSelf;

            set => ray.gameObject.SetActive(value);
        }

        public float Scale
        {
            set => ray.transform.localScale = new Vector3(1f, 100f * value, 1f);
        }
    }
}
