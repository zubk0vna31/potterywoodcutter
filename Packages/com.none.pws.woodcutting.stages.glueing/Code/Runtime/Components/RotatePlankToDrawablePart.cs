using UnityEngine;
using UnityEngine.UI;

namespace PWS.WoodCutting
{
    public struct RotatePlankToDrawablePart 
    {
        public float Timer;
        public Button Button;
        public Collider Collider;
    }
}
