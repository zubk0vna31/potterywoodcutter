using Leopotam.Ecs;
using System;
using UnityEngine;

namespace PWS.WoodCutting
{
    public struct PlankDrawSide
    {
        public int order;

        public GlueRatingSystem glueRatingSystem;
        public EcsEntity plankEntity;

        public Collider collider;

        public Action OnGluedSide;

        private bool completed;

        public float Percentage => glueRatingSystem.Percentage;

        public void ResetUnityComponents()
        {
            glueRatingSystem.ResetComponent();
        }

        public bool Completed
        {
            get
            {
                return completed;
            }
            set
            {
                completed = value;

                if (value)
                {
                    plankEntity.Get<PlankGlued>().currentSide++;

                    Debug.Log(plankEntity);
                    Debug.Log(plankEntity.Get<PlankGlued>().currentSide);
                }
            }
        }

    }
}
