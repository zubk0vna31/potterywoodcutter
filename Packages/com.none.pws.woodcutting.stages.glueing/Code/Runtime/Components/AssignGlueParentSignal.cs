using UnityEngine;

namespace PWS.WoodCutting
{
    public struct AssignGlueParentSignal 
    {
        public Transform glueHolder;
    }
}
