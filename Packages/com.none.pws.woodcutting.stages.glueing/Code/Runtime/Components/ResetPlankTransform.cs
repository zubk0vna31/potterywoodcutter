using UnityEngine.UI;

namespace PWS.WoodCutting
{
    public struct ResetPlankTransform 
    {
        public float Timer;
        public Button Button;
    }
}
