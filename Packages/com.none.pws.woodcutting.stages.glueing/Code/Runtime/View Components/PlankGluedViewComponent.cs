using Leopotam.Ecs;
using Modules.ViewHub;
using System.Linq;
using UnityEngine;

namespace PWS.WoodCutting
{
    public class PlankGluedViewComponent : ViewComponent
    {
        [SerializeField]
        private GlueingStateConfig _config;

        [SerializeField]
        private Transform glueHolder;

        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            ref var plank = ref ecsEntity.Get<PlankGlued>();

            plank.glueHolder = glueHolder;

            plank.drawOrder = -1;

            var ratingSystem = GetComponentsInChildren<GlueRatingSystem>().ToArray();
            plank.drawEntities = new EcsEntity[ratingSystem.Length];

            for (int i = 0; i < plank.drawEntities.Length; i++)
            {
                plank.drawEntities[i] = ecsWorld.NewEntity();
                ref var drawSide = ref plank.drawEntities[i].Get<PlankDrawSide>();

                drawSide.glueRatingSystem = ratingSystem[i];
                drawSide.glueRatingSystem.SetConfigData(_config.successPercentage, _config.rateDuration);
                drawSide.plankEntity = ecsEntity;
                drawSide.collider = ratingSystem[i].GetComponentInChildren<Collider>();

                drawSide.collider.enabled = false;

                ref var unityView = ref plank.drawEntities[i].Get<UnityView>();

                unityView.GameObject = ratingSystem[i].gameObject;
                unityView.Transform = ratingSystem[i].transform;

                drawSide.OnGluedSide += () => ecsEntity.Get<PlankGlued>().currentSide++;
            }
        }
    }
}
