using Leopotam.Ecs;
using Modules.StateGroup.Components;
using PWS.Common.UI;
using PWS.WoodCutting.Common;
using System.Collections.Generic;

namespace PWS.WoodCutting
{
    public class GlueingSubstageProcessTracker<T> : IEcsRunSystem where T:struct
    {
        private readonly EcsFilter<T> _state;
        private readonly EcsFilter<StateEnter, T> _stateEnter;

        private readonly EcsFilter<PlankDrawSide> _drawSides;
        private readonly EcsFilter<SubstageProgress> _substageProgress;
        private readonly EcsFilter<SubstageSignal> _substageSignal;

        private readonly GlueingStateConfig _stateConfig;

        private List<PlankDrawSide> _hashedDrawSides = new List<PlankDrawSide>();

        private int currentSubstage;
        private int substageAmount;
        private bool completed;


        public GlueingSubstageProcessTracker(StateConfig stateConfig)
        {
            _stateConfig = stateConfig as GlueingStateConfig;
        }

        public void Run()
        {
            if (_state.IsEmpty()) return;

            StateEnter();
            SubstageSignal();
            State();
        }

        private void SubstageSignal()
        {
            if (_substageSignal.IsEmpty()) return;

            if (currentSubstage < substageAmount)
            {
                currentSubstage++;
                foreach (var i in _substageProgress)
                {
                    _substageProgress.Get1(i).Value = 0;
                }
            }
            else
            {
                completed = true;
            }
        }

        private void StateEnter()
        {
            if (_stateEnter.IsEmpty()) return;

            substageAmount = _drawSides.GetEntitiesCount();
            currentSubstage = 0;

            foreach (var i in _drawSides)
            {
                _hashedDrawSides.Add(_drawSides.Get1(i));
            }
        }

        private void State()
        {
            if (completed) return;

            foreach (var i in _substageProgress)
            {
                _substageProgress.Get1(i).Value = _hashedDrawSides[currentSubstage].Percentage;
            }
        }
    }
}
