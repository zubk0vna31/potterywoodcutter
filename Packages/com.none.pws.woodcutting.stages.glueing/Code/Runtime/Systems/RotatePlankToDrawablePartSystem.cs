using Leopotam.Ecs;
using PWS.Common.UI;

namespace PWS.WoodCutting
{
    public class RotatePlankToDrawablePartSystem : IEcsRunSystem
    {
        // auto injected fields
        private readonly EcsFilter<PlankGlued, RotatePlankToDrawablePart> _filter;
        private readonly EcsFilter<GlueingState> _state;

        private readonly EcsFilter<StateWindow> _stateInfoWindow;


        private Modules.Utils.TimeService _time;

        public void Run()
        {
            if (_state.IsEmpty()) return;


            foreach (var i in _filter)
            {
                ref var rotate = ref _filter.Get2(i);

                if (rotate.Timer > 0)
                {
                    rotate.Timer -= _time.DeltaTime;

                    if (rotate.Timer <= 0)
                    {
                        foreach (var j in _stateInfoWindow)
                        {
                            _stateInfoWindow.Get1(j).Template.ToggleButtonInteraction(true);
                        }

                        _filter.Get2(i).Collider.enabled = true;

                        _filter.GetEntity(i).Del<RotatePlankToDrawablePart>();
                    }
                }
            }
        }
    }
}
