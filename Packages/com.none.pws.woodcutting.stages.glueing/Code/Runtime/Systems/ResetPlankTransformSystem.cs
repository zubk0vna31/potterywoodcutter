using Leopotam.Ecs;
using PWS.Common.UI;

namespace PWS.WoodCutting
{
    public class ResetPlankTransformSystem : IEcsRunSystem
    {
        // auto injected fields
        private readonly EcsFilter<GlueingState> _state;
        private readonly EcsFilter<PlankGlued, ResetPlankTransform> _filter;
        private readonly EcsFilter<StateWindow> _stateInfoWindow;

        private Modules.Utils.TimeService _time;

        public void Run()
        {
            if (_state.IsEmpty()) return;


            foreach (var i in _filter)
            {
                ref var reset = ref _filter.Get2(i);

                if (reset.Timer > 0)
                {
                    reset.Timer -= _time.DeltaTime;

                    if (reset.Timer <= 0)
                    {
                        _filter.Get1(i).onDrawPosition = false;

                        if (_filter.Get1(i).next != EcsEntity.Null)
                        {
                            foreach (var j in _stateInfoWindow)
                            {
                                _stateInfoWindow.Get1(j).Template.MakeSignal();
                            }
                        }

                        _filter.GetEntity(i).Del<ResetPlankTransform>();
                    }
                }
            }
        }
    }
}
