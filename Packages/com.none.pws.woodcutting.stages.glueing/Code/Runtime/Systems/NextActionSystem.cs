using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using DG.Tweening;
using Leopotam.Ecs;
using Modules.ViewHub;
using PWS.Common.GameModeInfoService;
using Modules.StateGroup.Core;
using Modules.StateGroup.Components;
using PWS.Common.UI;
using PWS.WoodCutting.Common;
using PWS.Common;
using PWS.Features.Achievements;
using PWS.Features.Achivements;

namespace PWS.WoodCutting
{
    public class NextActionSystem<T> : IEcsRunSystem where T : struct
    {
        // auto injected fields
        private readonly EcsFilter<PlankComponent, PlankGlued,UnityView> _filter;
        private readonly EcsFilter<T> _state;
        private readonly EcsFilter<StateEnter,T> _stateEnter;

        private readonly EcsFilter<PlankDrawSide> _drawSides;
        private readonly EcsFilter<StateWindow, StateProgress> _stateWindowWithProgress;
        private readonly EcsFilter<NextStateSignal> _nextStateSignal;

        private readonly EcsWorld _ecsWorld;

        private readonly GlueingStateConfig _stateConfig;
        private readonly Transform _drawPosition;

        private readonly IAchievementsService _achievementsService;

        private readonly StateFactory _stateFactory;

        // runtime data
        private EcsEntity _current;
        private GameMode _gameMode;
        private bool stageCompleted;

        public NextActionSystem(GameMode gameMode, StateConfig stateConfig, Transform drawPosition,
            IAchievementsService achievementsService)
        {
            _gameMode = gameMode;
            _stateConfig = stateConfig as GlueingStateConfig;
            _drawPosition = drawPosition;
            _achievementsService = achievementsService;
        }

        public void Run()
        {
            if (_state.IsEmpty()) return;

            StateEnter();

            if (!_nextStateSignal.IsEmpty())
                Next();
        }
       

        private void StateEnter()
        {
            if (_stateEnter.IsEmpty()) return;

            foreach (var i in _stateWindowWithProgress)
            {
                _stateWindowWithProgress.Get1(i).Template.ChangeButtonTitle(_stateConfig.buttonSubstageTitleTerm);
            }

            ToggleButtonVisibility(true);
            ToggleButtonInteraction(true);

            InitializePlanks();
            Next();
        }

        private void InitializePlanks()
        {
            bool firstEntitySkipped = false;
            EcsEntity previous = EcsEntity.Null;

            List<EcsEntity> entities = new List<EcsEntity>();

            foreach (var i in _filter)
            {
                _filter.Get2(i).initialLocalPosition = _filter.Get3(i).Transform.localPosition;
                _filter.Get2(i).initialLocalRotation = _filter.Get3(i).Transform.localEulerAngles;
                _filter.Get2(i).drawOrder = _filter.Get1(i).slotID;
                entities.Add(_filter.GetEntity(i));
            }

            entities = entities.OrderBy(x => x.Get<PlankGlued>().drawOrder).ToList();

            int index = 0;
            foreach (var entity in entities)
            {
                if (!firstEntitySkipped)
                {
                    firstEntitySkipped = true;
                    previous = _current = entity;

                    RemovePlankDrawSide(entity,true);

                    index++;
                    continue;
                }


                if (index == entities.Count - 1)
                {
                    RemovePlankDrawSide(entity,false);
                }

                previous.Get<PlankGlued>().next = entity;
                previous = entity;
                index++;
            }

            index = 0;
            var _entity = entities[0];
            while (_entity != EcsEntity.Null)
            {
                foreach (var i in _entity.Get<PlankGlued>().drawEntities)
                {
                    i.Get<PlankDrawSide>().order = index;
                    index++;
                }

                _entity = _entity.Get<PlankGlued>().next;
            }

            _ecsWorld.NewEntity().Get<AssignGlueParentSignal>().glueHolder = _current.Get<PlankGlued>().glueHolder;
        }

        private void RemovePlankDrawSide(EcsEntity plank,bool first)
        {
            ref var plankGlued = ref plank.Get<PlankGlued>();

            int result = 0;

            Vector3 localPos = _drawPosition.InverseTransformPoint(
                plankGlued.drawEntities[result].Get<UnityView>().Transform.GetChild(0).position);
            for (int i = 1; i < plankGlued.drawEntities.Length; i++)
            {
                Vector3 comparePos = _drawPosition.InverseTransformPoint(
                    plankGlued.drawEntities[i].Get<UnityView>().Transform.GetChild(0).position);

                if (first)
                {
                    if (comparePos.x > localPos.x)
                    {
                        result = i;
                        localPos = comparePos;
                    }
                }
                else
                {
                    if (comparePos.x < localPos.x)
                    {
                        result = i;
                        localPos = comparePos;
                    }
                }
            }


            var tmp = plankGlued.drawEntities;

            plankGlued.drawEntities = new EcsEntity[tmp.Length - 1];

            for (int i = 0,j=0; i < tmp.Length; i++)
            {
                if (i == result)
                {
                    tmp[i].Get<PlankDrawSide>().order = -1;
                    continue;
                }

                plankGlued.drawEntities[j] = tmp[i];
                j++;
            }
        }

        private void Next()
        {
            if (_current == EcsEntity.Null)
            {
                // Grade
                float grade = 0;
                foreach (var i in _stateWindowWithProgress)
                {
                    grade = _stateWindowWithProgress.Get2(i).ProgressValue;
                }
                _ecsWorld.NewEntity().Get<SetGradeSignal>().grade = grade;

                if (stageCompleted)
                {
                    _stateConfig.nextState.Execute(_stateFactory);
                }

                ToggleButtonVisibility(false);
                ToggleButtonInteraction(true);

                return;
            }

            ref var plank = ref _current.Get<PlankGlued>();

            if (plank.IsCompleted(_gameMode))
            {

                _ecsWorld.NewEntity().Get<ChangeDrawablePartSignal>();

                if (plank.next == EcsEntity.Null)
                {
                    ToggleButtonInteraction(false);

                    //Reset button title to default
                    foreach (var i in _stateWindowWithProgress)
                    {
                        _stateWindowWithProgress.Get1(i).Template.ResetButtonTitle();
                    }

                    //Dissable all drawing parts
                    foreach (var i in _drawSides)
                    {
                        _drawSides.Get1(i).collider.enabled = false;
                    }

                    //Reset plank transform
                    ResetPlankTransform(_current);
                    _current = EcsEntity.Null;
                    stageCompleted = true;

                    //DoTween helps with delay state execute
                    float value = 0f;
                    DOTween.To(() => value, (x) => value = x, 1f,
                        _stateConfig.resetTransformDuration+_stateConfig.switchSideDuration)
                        .OnComplete(() => ToggleButtonInteraction(true));
                }
                else
                {
                    //Reset plank transform
                    ResetPlankTransform(_current);
                    _current = plank.next;

                    _ecsWorld.NewEntity().Get<AssignGlueParentSignal>().glueHolder
                        = _current.Get<PlankGlued>().glueHolder;
                }
            }
            else
            {
                if (!plank.onDrawPosition)
                {
                    MovePlankToDrawPosition();
                }
                else
                {
                    if (!plank.inited)
                    {
                        plank.inited = true;
                        RotatePlankToDrawablePart();
                    }
                    else
                    {
                        if (plank.switchSideAfterRating)
                        {
                            plank.switchSideAfterRating = false;
                            RotatePlankToDrawablePart();
                        }
                        else ShowRateVisualProcessing();
                    }
                }
            }
        }

        private void ToggleButtonVisibility(bool value)
        {
            foreach (var i in _stateWindowWithProgress)
            {
                _stateWindowWithProgress.Get1(i).Template.ShowButton(value);
            }
        }

        private void ToggleButtonInteraction(bool value)
        {
            foreach (var i in _stateWindowWithProgress)
            {
                _stateWindowWithProgress.Get1(i).Template.ToggleButtonInteraction(value);
            }
        }

        private void ResetPlankTransform(EcsEntity previous)
        {
            //Dissable button
            ToggleButtonInteraction(false);

            //Adding reset component
            ref var reset = ref previous.Get<ResetPlankTransform>();
            reset.Timer = _stateConfig.resetTransformDuration;

            //DoTween
            ref var view = ref previous.Get<UnityView>();
            ref var plank = ref previous.Get<PlankGlued>();

            var sequence = DOTween.Sequence();
            sequence.Pause();

            sequence.Append(view.Transform.DOLocalRotate(plank.initialLocalRotation,
                _stateConfig.switchSideDuration));
            sequence.Append(view.Transform.DOLocalMove(plank.initialLocalPosition,
                _stateConfig.resetTransformDuration));

            sequence.Play();
        }

        private void MovePlankToDrawPosition()
        {
            //Dissable button
            ToggleButtonInteraction(false);

            //Adding move component
            ref var move = ref _current.Get<MovePlankToDrawPosition>();
            move.Timer = _stateConfig.resetTransformDuration;

            //DoTween
            _current.Get<UnityView>().Transform.DOMove(_drawPosition.position, _stateConfig.resetTransformDuration);
        }

        private void RotatePlankToDrawablePart()
        {
            //Dissable button
            ToggleButtonInteraction(false);

            //Get plank component
            ref var plank = ref _current.Get<PlankGlued>();

            //Adding rotate component
            ref var rotate = ref _current.Get<RotatePlankToDrawablePart>();
            rotate.Timer = _stateConfig.switchSideDuration;

            ref var drawSide = ref plank.drawEntities[plank.currentSide].Get<PlankDrawSide>();

            rotate.Collider = drawSide.collider;
            rotate.Collider.enabled = false;

            //DoTween
            Transform currentSide = plank.drawEntities[plank.currentSide].Get<UnityView>().Transform;
            Transform plankTransform = _current.Get<UnityView>().Transform;

            Quaternion rotation = Quaternion.LookRotation(_drawPosition.forward, plankTransform.up)
                * Quaternion.AngleAxis(90 * Mathf.Sign(Vector3.Dot(currentSide.forward, plankTransform.forward)), Vector3.up);

            plankTransform.DORotateQuaternion(rotation, _stateConfig.switchSideDuration);
        }

        private void ShowRateVisualProcessing()
        {
            //Dissable button
            ToggleButtonInteraction(false);

            //Disable drawing
            var entity = _current.Get<PlankGlued>().drawEntities[_current.Get<PlankGlued>().currentSide];
            entity.Get<PlankDrawSide>().collider.enabled = false;

            if (!_gameMode.Equals(GameMode.Free))
            {
                entity.Get<PlankDrawSide>().Completed = entity.Get<PlankDrawSide>().glueRatingSystem.Rate(_stateConfig.rateDuration);
            }
            else
            {
                entity.Get<PlankDrawSide>().glueRatingSystem.Rate();
                entity.Get<PlankDrawSide>().Completed = true;
                _current.Get<PlankGlued>().switchSideAfterRating = true;
                Next();
                return;
            }

            //Adding show component
            ref var show = ref entity.Get<ShowRateVisual>();
            show.Timer = _stateConfig.rateDuration;
        }
    }
}
