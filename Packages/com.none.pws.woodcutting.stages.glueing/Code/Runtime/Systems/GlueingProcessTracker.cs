using Leopotam.Ecs;
using Modules.StateGroup.Components;
using PWS.Common.UI;

namespace PWS.WoodCutting
{
    public class GlueingProcessTracker<T> : IEcsRunSystem where T : struct
    {
        private readonly EcsFilter<T> _state;
        private readonly EcsFilter<StateEnter, T> _stateEnter;

        private readonly EcsFilter<StateWindow, StateProgress> _stateWindowWithProgress;
        private readonly EcsFilter<PlankDrawSide> _drawSides;


        public void Run()
        {
            if (_state.IsEmpty()) return;

            StateEnter();
            State();
        }

        private void StateEnter()
        {
            if (_stateEnter.IsEmpty()) return;

            foreach (var i in _stateWindowWithProgress)
            {
                ref var progress = ref _stateWindowWithProgress.Get2(i);

                progress.Value = 0;
                progress.CompletionThreshold = 2f;
                progress.SkipThreshold = 0.76f;
            }
        }

        private void State()
        {
            foreach (var i in _stateWindowWithProgress)
            {
                float progress = 0f;
                int actualSideAmount = 0;

                foreach (var j in _drawSides)
                {
                    if (_drawSides.Get1(j).order >= 0)
                    {
                        progress += _drawSides.Get1(j).Percentage;
                        actualSideAmount++;
                    }
                }

                progress /= actualSideAmount;

                _stateWindowWithProgress.Get2(i).Value = progress;
            }
        }
    }
}
