﻿using Leopotam.Ecs;
using Modules.Audio;
using Modules.StateGroup.Components;
using PWS.Common.Audio;
using UnityEngine;

namespace PWS.WoodCutting
{
    public class WrongGluingVoiceSystem : IEcsRunSystem
    {
        private readonly EcsFilter<GlueingState>.Exclude<StateEnter> _inState;
        private readonly EcsFilter<PlankDrawSide, ShowRateVisual> _filter;
        private readonly EcsFilter<AudioSourceRef, GrandDadTag> _grandDad;
        private readonly EcsFilter<VoiceConfigComponent> _voiceConfigComponent;

        private bool _played;
        public void Run()
        {
            if (_inState.IsEmpty())
                return;
            
            if (!_filter.IsEmpty() && !_played)
            {
                foreach (var side in _filter)
                {
                    if (!_filter.Get1(side).Completed)
                    {
                        foreach (var i in _voiceConfigComponent)
                        {
                            ref var config = ref _voiceConfigComponent.Get1(i);
                            var id = Random.Range(0, config.WrongClipsTerms.Length);
                            PlayClip(config.WrongClipsTerms, ref id);
                            _played = true;
                        }
                    }
                }
            }
            else if (_filter.IsEmpty() && _played)
            {
                _played = false;
            }
        }
        
        private void PlayClip(string[] clips, ref int idx)
        {
            foreach (var dad in _grandDad)
            {
                ref var sourceRef = ref _grandDad.Get1(dad);
                if (sourceRef.AudioSource.isPlaying)
                    return;
                
                ref var signal = ref _grandDad.GetEntity(dad).Get<PlayGlobalLocalizedVoiceSignal>();
                signal.VoiceLocalize = _grandDad.Get1(dad).VoiceLocalize;
                signal.AudioSource = _grandDad.Get1(dad).AudioSource;
                signal.Term = clips[idx];
                idx++;
            }
        }
    }
}