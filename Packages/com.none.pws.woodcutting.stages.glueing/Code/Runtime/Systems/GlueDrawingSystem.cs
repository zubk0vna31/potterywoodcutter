using Leopotam.Ecs;
using PWS.WoodCutting.Common;
using UnityEngine;

namespace PWS.WoodCutting
{
    public class GlueDrawingSystem : IEcsRunSystem
    {
        // auto injected fields
        private readonly EcsFilter<GlueContainter, SelectStay, GluePressed> _glueFilter;
        private readonly EcsFilter<GlueingState> _state;
        private readonly EcsFilter<AssignGlueParentSignal> _assignParentSignal;
        private readonly EcsFilter<ChangeDrawablePartSignal> _changeDrawableSignal;

        private readonly GlueingStateConfig _stateConfig;
        private readonly GlueDrawerSystem _glueDrawerSystem;

        private Transform _currentGlueHolder;

        public GlueDrawingSystem(StateConfig stateConfig, GlueDrawerSystem glueDrawerSystem)
        {
            _stateConfig = stateConfig as GlueingStateConfig;
            _glueDrawerSystem = glueDrawerSystem;

            _glueDrawerSystem.Initialize(_stateConfig.gluePrefab, _stateConfig.drawMask);
        }

        public void Run()
        {
            if (_state.IsEmpty()) return;


            if (!_assignParentSignal.IsEmpty())
            {
                foreach (var i in _assignParentSignal)
                {
                    _currentGlueHolder = _assignParentSignal.Get1(i).glueHolder;
                    break;
                }
            }

            if (!_changeDrawableSignal.IsEmpty())
            {
                _glueDrawerSystem.ToggleCollider(false);
                _glueDrawerSystem.Clear();
            }

            if (_glueFilter.IsEmpty())
            {
                _glueDrawerSystem.InputLost();
                return;
            }

            HandleDrawing();
        }

        private void HandleDrawing()
        {
            foreach (var i in _glueFilter)
            {
                Ray ray = new Ray(_glueFilter.Get1(i).ray.position, _glueFilter.Get1(i).ray.up);

                if (_glueDrawerSystem.Draw(ray, _stateConfig.glueRayLength, out var hit,_currentGlueHolder))
                {
                    _glueFilter.Get1(i).Scale = hit.distance;
                }
                else
                {
                    _glueFilter.Get1(i).Scale = _stateConfig.glueRayLength;
                }
            }
        }
    }
}
