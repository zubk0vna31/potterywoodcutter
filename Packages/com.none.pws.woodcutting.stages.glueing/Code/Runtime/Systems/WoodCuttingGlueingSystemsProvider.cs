using Leopotam.Ecs;
using Modules.Root.ContainerComponentModel;
using Modules.Root.ECS;
using UnityEngine;
using PWS.Common.GameModeInfoService;
using PWS.Common.UI;
using PWS.Common.Audio;
using PWS.WoodCutting.Common;
using PWS.Common.ResultsEvaluation;
using PWS.Common.TravelPoints;
using PWS.Features.Achievements;

namespace PWS.WoodCutting
{
    internal class StateDependencies
    {
        [Inject] public IGameModeInfoService gameModeInfoService;
        [Inject] public IResultsEvaluationDataHolder resultsEvaluation;
        [Inject] public IAchievementsService achievementsService;

    }

    public class WoodCuttingGlueingSystemsProvider : MonoBehaviour, ISystemsProvider
    {
        [Header("Core")]
        [SerializeField]
        private StateConfig _stateConfig;

        [SerializeField]
        private StateInfoData _stateInfo;


        [Header("Scene Related")]
        [SerializeField]
        private Transform _drawPosition;
        [SerializeField]
        private GlueDrawerSystem _glueDrawerSystem;
        
        [Header("Voice config")] 
        [SerializeField] private VoiceConfig _voiceConfig;

        public EcsSystems GetSystems(EcsWorld world, EcsSystems endFrame, EcsSystems mainSystems)
        {
            var dependencies = new StateDependencies();
            SceneContainer.Instance.Inject(dependencies);

            EcsSystems systems = new EcsSystems(world, "WoodCutting Glueing");

            systems

                // Core
                .Add(new OutlineByStateSystem<GlueingState>(_stateConfig))


                .Add(new MovePlankToDrawPositionSystem())
                .Add(new RotatePlankToDrawablePartSystem())
                .Add(new ResetPlankTransformSystem())
                .Add(new ShowRateVisualSystem())
                .Add(new NextActionSystem<GlueingState>(dependencies.gameModeInfoService.CurrentMode,
                _stateConfig, _drawPosition, dependencies.achievementsService))
                .Add(new GlueDrawingSystem(_stateConfig, _glueDrawerSystem))

                // Tracker
                .Add(new GlueingProcessTracker<GlueingState>())

                // UI
                .Add(new StateInfoWindowUIProcessing<GlueingState>(_stateInfo))
                .Add(new StateWindowPlacementProcessing<GlueingState>("Main"))
                .Add(new StateWindowProgressUIProcessing<GlueingState>())

                // Travel point
                .Add(new TravelPointPositioningSystem<GlueingState>("Default"))

                 .Add(new WoodCuttingSetGradeByProgress<GlueingState>(dependencies.resultsEvaluation,
                _stateInfo))

                //Voice
                .Add(new VoiceAudioSystem<GlueingState>(_voiceConfig))
                .Add(new WrongGluingVoiceSystem())
                
                // Sounds
                .Add(new ToolRandomSoundAudioSystem<GluePressed>())

            ;

            endFrame

                // OneFrame
                .OneFrame<AssignGlueParentSignal>()
                .OneFrame<ChangeDrawablePartSignal>()
                ;
            
            return systems;
        }
    }
}
