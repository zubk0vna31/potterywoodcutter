using Leopotam.Ecs;
using PWS.Common.UI;

namespace PWS.WoodCutting
{
    public class MovePlankToDrawPositionSystem : IEcsRunSystem
    {
        // auto injected fields
        private readonly EcsFilter<GlueingState> _state;
        private readonly EcsFilter<PlankGlued, MovePlankToDrawPosition> _filter;

        private readonly EcsFilter<StateWindow> _stateInfoWindow;

        private Modules.Utils.TimeService _time;

        public void Run()
        {
            if (_state.IsEmpty()) return;


            foreach (var i in _filter)
            {
                ref var move = ref _filter.Get2(i);

                if (move.Timer > 0)
                {
                    move.Timer -= _time.DeltaTime;

                    if (move.Timer <= 0)
                    {
                        _filter.Get1(i).onDrawPosition = true;

                        foreach (var j in _stateInfoWindow)
                        {
                            _stateInfoWindow.Get1(j).Template.ToggleButtonInteraction(true);
                            _stateInfoWindow.Get1(j).Template.MakeSignal();
                        }

                        _filter.GetEntity(i).Del<MovePlankToDrawPosition>();
                    }
                }
            }
        }
    }
}
