using Leopotam.Ecs;
using PWS.Common.UI;

namespace PWS.WoodCutting
{
    public class ShowRateVisualSystem : IEcsRunSystem
    {
        // auto injected fields
        private readonly EcsFilter<PlankDrawSide, ShowRateVisual> _filter;
        private readonly EcsFilter<GlueingState> _state;

        private readonly EcsFilter<StateWindow> _stateInfoWindow;


        private Modules.Utils.TimeService _time;

        public void Run()
        {
            if (_state.IsEmpty()) return;


            foreach (var i in _filter)
            {
                ref var show = ref _filter.Get2(i);

                if (show.Timer > 0)
                {
                    show.Timer -= _time.DeltaTime;

                    if (show.Timer <= 0)
                    {
                        if (_filter.Get1(i).Completed)
                        {
                            _filter.Get1(i).plankEntity.Get<PlankGlued>().switchSideAfterRating = true;

                            foreach (var j in _stateInfoWindow)
                            {
                                _stateInfoWindow.Get1(j).Template.MakeSignal();
                            }
                        }
                        else
                        {
                            foreach (var j in _stateInfoWindow)
                            {
                                _stateInfoWindow.Get1(j).Template.ToggleButtonInteraction(true);
                            }

                            _filter.Get1(i).collider.enabled = true;
                        }

                        _filter.GetEntity(i).Del<ShowRateVisual>();
                    }
                }
            }
        }
    }
}
