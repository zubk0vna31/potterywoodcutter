using PWS.WoodCutting.Common;
using UnityEngine;

namespace PWS.WoodCutting
{
    [CreateAssetMenu(fileName = "GlueingStateConfig", menuName = "PWS/WoodCutting/Stages/Glueing/Config")]
    public class GlueingStateConfig : StateConfig
    {
        [Header("Strings")]
        public string buttonSubstageTitleTerm;
        public string buttonSubstageTitle;

        [Header("DoTween Durations")]

        [Range(0.1f,3f)]
        public float switchSideDuration = 0.5f;
        [Range(0.1f, 3f)]
        public float resetTransformDuration = 0.5f;

        [Range(0.1f, 3f)]
        public float rateDuration = 0.5f;

        [Header("Glueing")]
        public GameObject gluePrefab;
        public LayerMask drawMask;

        [Range(0f,1f)]
        public float successPercentage = 0.9f;

        [Range(0.1f, 10f)]
        public float glueRayLength = 2.5f;
    }
}
