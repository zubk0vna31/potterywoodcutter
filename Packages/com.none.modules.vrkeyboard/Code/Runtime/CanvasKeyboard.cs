﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Reflection;
using UnityEngine.EventSystems;
using TMPro;
using System;

namespace Modules.VRKeyboard
{
	public class CanvasKeyboard : MonoBehaviour 
	{
		public TMP_InputField inputField;

        public string text 
		{
			get => inputField.text;
            set => inputField.text = value;
        }

        public void Start()
        {
            inputField.onSelect.AddListener(OpenKeyboard);
        }

        public void OpenKeyboard(string arg0)
        {
            gameObject.SetActive(true);
        }
        public void CloseKeyboard(string arg0)
        {
            gameObject.SetActive(false);
        }

        #region Keyboard Receiving Input

        public void SendKeyString(string keyString)
		{
			if (keyString.Length == 1 && keyString[0] == 8/*ASCII.Backspace*/)
			{
				if (text.Length > 0)
				{
					text = text.Remove(text.Length - 1); 
				}
			}
			else
			{
				text += keyString;
			}

			// Workaround: Restore focus to input fields (because Unity UI buttons always steal focus)
			ReactivateInputField(inputField);

		}

        #endregion


        #region Steal Focus Workaround

        void ReactivateInputField(TMP_InputField inputField)
		{
			if (inputField != null)
			{
				StartCoroutine(ActivateInputFieldWithoutSelection(inputField));
			}
		}

		IEnumerator ActivateInputFieldWithoutSelection(TMP_InputField inputField)
		{
			inputField.ActivateInputField();

			// wait for the activation to occur in a lateupdate
			yield return new WaitForEndOfFrame();

			// make sure we're still the active ui
			if (EventSystem.current.currentSelectedGameObject == inputField.gameObject)
			{
				// To remove hilight we'll just show the caret at the end of the line
				inputField.MoveTextEnd(false);
			}
		}

		#endregion

	}
}