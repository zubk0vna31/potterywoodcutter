﻿using Leopotam.Ecs;
using Modules.ViewHub;
using UnityEngine;

namespace PWS.Common.Confetti
{
    public class ConfettiViewComponent : ViewComponent
    {
        [SerializeField] private ParticleSystem _confettiParticleSystema;
        
        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            ecsEntity.Get<ConfettiComponent>().ConfettiParticleSystem = _confettiParticleSystema;
        }
    }
}