﻿using UnityEngine;

namespace PWS.Common.Confetti
{
    public struct ConfettiComponent
    {
        public ParticleSystem ConfettiParticleSystem;
    }
}