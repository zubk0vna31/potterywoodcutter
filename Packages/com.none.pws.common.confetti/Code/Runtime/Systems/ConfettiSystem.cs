﻿using Leopotam.Ecs;
using Modules.StateGroup.Components;

namespace PWS.Common.Confetti
{
    public class ConfettiSystem<TState> : IEcsRunSystem where TState : struct
    {
        private readonly EcsFilter<StateEnter, TState> _enterState;
        private readonly EcsFilter<ConfettiComponent> _confetti;
        
        public void Run()
        {
            if (!_enterState.IsEmpty())
            {
                foreach (var idx in _confetti)
                {
                    _confetti.Get1(idx).ConfettiParticleSystem.Play();
                }
            }
        }
    }
}