﻿using Leopotam.Ecs;
using Modules.ViewHub;

namespace PWS.WoodCutting
{
    public class PencilDrawingViewComponent : ViewComponent
    {
        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            ecsEntity.Get<PencilTag>();
        }
    }
}