using Leopotam.Ecs;
using Modules.ViewHub;
using PWS.WoodCutting.Common;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

namespace PWS.WoodCutting.MarkupPainting
{
    public class PatternSelectMenuViewComponent : ViewComponent
    {
        [Header("Default Configs")]
        [SerializeField]
        private PatternConfig defaultPatternConfig;
        [SerializeField]
        private MarkupToolConfig defaultMarkupToolConfig;
        [SerializeField]
        private DrillToolConfig defaultDrillToolConfig;
        [SerializeField]
        private JigsawToolConfig defaultJigsawToolConfig;
        [SerializeField]
        private ChamferToolConfig defaultChamferToolConfig;
        [SerializeField]
        private VertexPathFollowConfig defaultVertexPathFollowConfig;

       [Header("Prefabs")]
        [SerializeField]
        private GameObject categoryPrefab;
        [SerializeField]
        private GameObject itemPrefab;

        [Header("Base")]
        [SerializeField]
        private ToggleGroup toggleGroup;

        [SerializeField]
        private GameObject hideObject;

        [SerializeField]
        private Transform categoryContainer;

        [Header("Buttons")]
        [SerializeField]
        private Button confirmButton;

        [SerializeField]
        private Button resetButton;

        private EcsEntity entity;
        private string resourceFolderPlatbandPath;
        private bool initialized;

        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            hideObject.gameObject.SetActive(false);

            confirmButton.onClick.AddListener(ConfirmButtonPressed);
            resetButton.onClick.AddListener(ResetButtonPressed);

            entity = ecsEntity;
            ecsEntity.Get<PatternSelectMenu>().View = this;
        }


        public void Initialize()
        {
            if (initialized) return;

            initialized = true;

            PathsConfig pathsConfig = Resources.Load<PathsConfig>
                (WoodCuttingPaths.pathsConfigPathFromResources);

            Dictionary<string, CategoryView> categoryTransformData = new Dictionary<string, CategoryView>();
            HashSet<string> platbandsPaths = new HashSet<string>();

            toggleGroup = categoryContainer.GetComponent<ToggleGroup>();
            toggleGroup.SetAllTogglesOff();

            foreach (var path in pathsConfig.Paths)
            {
                var categoryPath = Path.GetDirectoryName(Path.GetDirectoryName(path));
                categoryPath = categoryPath.Substring(categoryPath.IndexOf("WoodCutting"));

                if (!categoryTransformData.ContainsKey(categoryPath))
                {
                    categoryTransformData.Add(categoryPath, Instantiate(
                        categoryPrefab, categoryContainer).GetComponent<CategoryView>());

                    var groupConfigPath = $"{categoryPath}/{WoodCuttingPaths.groupConfigName}";

                    categoryTransformData[categoryPath].Initialize(
                        Resources.Load<GroupConfig>(groupConfigPath).HeaderTerm);
                }

                var platbandPath = Path.GetDirectoryName(path);
                platbandPath = platbandPath.Substring(platbandPath.IndexOf("WoodCutting"));


                if (platbandsPaths.Contains(platbandPath))
                {
                    continue;
                }

                platbandsPaths.Add(platbandPath);

                var item = Instantiate(itemPrefab, categoryTransformData[categoryPath].ItemContainter).
                    transform.GetChild(1).GetComponent<Image>();

                var toggle = item.transform.parent.GetComponent<Toggle>();
                toggle.group = toggleGroup;

                item.transform.parent.name = platbandPath;

                var previewPath = path.Substring(path.IndexOf("WoodCutting"));
                previewPath = Path.GetDirectoryName(previewPath) + $"/{WoodCuttingPaths.previewName}";

                item.sprite = Resources.Load<Sprite>(previewPath);
            }
        }

        public void Show()
        {
            hideObject.gameObject.SetActive(true);
        }

        public void Confirm()
        {
            var path = GetSelectedPath();

            resourceFolderPlatbandPath = path + $"/{WoodCuttingPaths.finalName}";

            hideObject.gameObject.SetActive(false);

            entity.Get<PlatbandData>().main = GeneratePatternData($"{path}/{WoodCuttingPaths.mainPatternFolderName}");
            entity.Get<PlatbandData>().sub =  GeneratePatternData($"{path}/{WoodCuttingPaths.subPatternFolderName}");
        }

        public void ResetMenu()
        {
            toggleGroup.SetAllTogglesOff();
            toggleGroup.allowSwitchOff = false;

            toggleGroup.GetComponentInChildren<Toggle>().isOn = true;
        }

        private void ConfirmButtonPressed()
        {
            Debug.Log("Confirm");
            entity.Get<ConfirmSignal>();
        }

        private void ResetButtonPressed()
        {
            entity.Get<ResetSignal>();
        }

        private string GetSelectedPath()
        {
            var toggle = toggleGroup.GetFirstActiveToggle();

            if (!toggle)
            {
                toggle = toggleGroup.GetComponentInChildren<Toggle>();
                toggle.isOn = true;
            }

            return toggle.name;
        }

        private Pattern GeneratePatternData(string path)
        {
            GameObject prefab = Resources.Load<GameObject>(
                $"{path}{WoodCuttingPaths.outputPrefabName.Replace(".prefab", "")}");

            if (prefab == null)
            {
                Debug.LogError($"{path} doesn't contain PatternPrefab");
                return null;
            }

            PatternConfig patternConfig = Resources.Load<PatternConfig>(
                $"{path}{WoodCuttingPaths.inputPatternConfigPath.Replace(".asset", "")}");

            if (!patternConfig)
            {
                patternConfig = defaultPatternConfig;
            }

            SpriteConfig spriteConfig = Resources.Load<SpriteConfig>(
                $"{path}{WoodCuttingPaths.outputSpriteConfigName.Replace(".asset","")}");

            MeshConfig meshConfig = Resources.Load<MeshConfig>(
                $"{path}{WoodCuttingPaths.outputMeshConfigName.Replace(".asset", "")}");

            RegionConfig regionConfig = Resources.Load<RegionConfig>(
                $"{path}{WoodCuttingPaths.outputRegionConfigName.Replace(".asset", "")}");

            VertexPathConfig vertexPathConfig = Resources.Load<VertexPathConfig>(
               $"{path}{WoodCuttingPaths.outputVertexPathConfigName.Replace(".asset", "")}");

            // Load all tool and follow configs
            MarkupToolConfig markupToolConfig = Resources.Load<MarkupToolConfig>(
                $"{path}{WoodCuttingPaths.inputMarkupToolConfig.Replace(".asset", "")}");

            if (!markupToolConfig)
            {
                markupToolConfig = defaultMarkupToolConfig;
            }

            DrillToolConfig drillToolConfig = Resources.Load<DrillToolConfig>(
                $"{path}{WoodCuttingPaths.inputDrillToolConfig.Replace(".asset", "")}");

            if (!drillToolConfig)
            {
                drillToolConfig = defaultDrillToolConfig;
            }

            JigsawToolConfig jigsawToolConfig = Resources.Load<JigsawToolConfig>(
                $"{path}{WoodCuttingPaths.inputJigsawToolConfig.Replace(".asset", "")}");

            if (!jigsawToolConfig)
            {
                jigsawToolConfig = defaultJigsawToolConfig;
            }

            ChamferToolConfig chamferToolConfig = Resources.Load<ChamferToolConfig>(
                $"{path}{WoodCuttingPaths.inputChamferToolConfig.Replace(".asset", "")}");

            if (!chamferToolConfig)
            {
                chamferToolConfig = defaultChamferToolConfig;
            }


            VertexPathFollowConfig vertexPathFollowConfig = Resources.Load<VertexPathFollowConfig>(
               $"{path}{WoodCuttingPaths.inputVertexPathFollowConfig.Replace(".asset", "")}");

            if (!vertexPathFollowConfig)
            {
                vertexPathFollowConfig = defaultVertexPathFollowConfig;
            }

            // Create pattern instance
            var pattern =  new Pattern(spriteConfig.main, prefab, path,
                patternConfig, spriteConfig, meshConfig, regionConfig,vertexPathConfig);
            
            // Setting tool and follow configs
            pattern.SetMarkupTool(markupToolConfig);
            pattern.SetDrillTool(drillToolConfig);
            pattern.SetJigsawTool(jigsawToolConfig);
            pattern.SetChamferTool(chamferToolConfig);
            pattern.SetVertexPathFollowConfig(vertexPathFollowConfig);

            return pattern;
        }

        public GameObject LoadPlatband(Transform parent)
        {
            var prefab = Resources.Load<GameObject>(resourceFolderPlatbandPath);
            return Instantiate(prefab, parent);
        }
    }
}
