using Modules.StateGroup.Core;
using UnityEngine;

namespace PWS.WoodCutting.MarkupPainting
{
    [CreateAssetMenu(menuName = "PWS/WoodCutting/Stages/Markup Painting/SetStateCommand")]
    public class MarkupPaintingSetStateSOCommand : SetStateSOCommand<MarkupPaintingState>
    {

    }

}
