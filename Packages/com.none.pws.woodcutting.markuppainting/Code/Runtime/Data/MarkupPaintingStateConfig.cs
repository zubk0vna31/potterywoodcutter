using PWS.WoodCutting.Common;
using UnityEngine;

namespace PWS.WoodCutting.MarkupPainting
{
    [CreateAssetMenu(fileName = "MarkupPaintingStateConfig", menuName = "PWS/WoodCutting/Stages/Markup Painting/Config")]
    public class MarkupPaintingStateConfig : StateConfig
    {
        [Range(0f,1f)]
        public float skipThreshold = 0.75f;
        [Range(0f, 1f)]
        public float planksMoveDuration = 1f;

        [Range(0f,5f)]
        public float paintAllDuration = 1f;

        [Header("Visual")]

        public Color normalColor;
        public Color markupColor;
        [Range(0f, 1f)]
        public float clipping = 0.5f;
    }
}
