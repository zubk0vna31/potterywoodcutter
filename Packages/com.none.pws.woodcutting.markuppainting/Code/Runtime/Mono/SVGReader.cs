using UnityEngine;
using System.IO;
using System.Collections.Generic;
using System.Globalization;
using System;

namespace alicewithalex.SVGReader
{
    public struct CubicCommand
    {
        public List<Vector2> points_in_out_next;

        public void ConsoleWrite()
        {
            foreach (var p in points_in_out_next)
            {
                Debug.Log(p);
            }
        }
    }

    public class SVGPath
    {
        public Vector2 dimensions;
        public Vector2 start;
        public List<CubicCommand> cubicCommands;

        public SVGPath(Vector2 dimensions)
        {
            this.dimensions = dimensions;
            cubicCommands = new List<CubicCommand>();
        }

        public void AddCommand(CubicCommand cubicCommand)
        {
            cubicCommands.Add(cubicCommand);
        }
    }

    public class BeizerPath
    {
        public Vector3[] points;

        public BeizerPath(SVGPath svgPath)
        {
            if (svgPath == null) return;

            points = ConvertSVGPath(svgPath);
        }

        private Vector3[] ConvertSVGPath(SVGPath svgPath)
        {
            Vector3[] p = new Vector3[1 + svgPath.cubicCommands.Count * 3];

            float aspectRatio = svgPath.dimensions.x / svgPath.dimensions.y;

            p[0] = svgPath.start;
            p[0].x = (p[0].x-svgPath.dimensions.x*0.5f)/svgPath.dimensions.x* aspectRatio;
            p[0].y = (p[0].y+svgPath.dimensions.y*0.5f)/svgPath.dimensions.y;

            for (int i = 1, j = 0; i < p.Length; i += 3, j++)
            {
                p[i] = svgPath.cubicCommands[j].points_in_out_next[0];
                p[i].x = (p[i].x - svgPath.dimensions.x * 0.5f) / svgPath.dimensions.x* aspectRatio;
                p[i].y = (p[i].y + svgPath.dimensions.y * 0.5f) / svgPath.dimensions.y;


                p[i + 1] = svgPath.cubicCommands[j].points_in_out_next[1];
                p[i + 1].x = (p[i + 1].x - svgPath.dimensions.x * 0.5f) / svgPath.dimensions.x* aspectRatio;
                p[i + 1].y = (p[i + 1].y + svgPath.dimensions.y * 0.5f) / svgPath.dimensions.y;

                p[i + 2] = svgPath.cubicCommands[j].points_in_out_next[2];
                p[i + 2].x = (p[i + 2].x - svgPath.dimensions.x * 0.5f) / svgPath.dimensions.x* aspectRatio;
                p[i + 2].y = (p[i + 2].y + svgPath.dimensions.y * 0.5f) / svgPath.dimensions.y;

            }

            return p;
        }

        private Vector3 Cubic(Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3, float t)
        {
            return Mathf.Pow(1 - t, 3) * p0 + 3 * Mathf.Pow(1 - t, 2) * t * p1 +
                3 * (1 - t) * t * t * p2 + t * t * t * p3;
        }

        public Vector3[] GetPath(int resolution = 0,
            SVGReader.ProjectionPlane projectionPlane = SVGReader.ProjectionPlane.XZ)
        {
            Vector3[] p = new Vector3[1 + (1 + resolution) * AnchorAmount];

            //p[0] = points[0];
            p[0] = SVGReader.ProjectPoint(points[0], projectionPlane);
            p[0] = SVGReader.FlipVerticaly(1, p[0], projectionPlane);

            for (int i = 0, j = 1; i < points.Length - 3; i += 3, j += (resolution + 1))
            {
                //Vector3 current = points[i];
                //Vector3 inTangent = points[i + 1];
                //Vector3 outTangent = points[i + 2];
                //Vector3 next = points[i + 3];

                Vector3 current = SVGReader.ProjectPoint(points[i], projectionPlane);
                Vector3 inTangent = SVGReader.ProjectPoint(points[i + 1], projectionPlane);
                Vector3 outTangent = SVGReader.ProjectPoint(points[i + 2], projectionPlane);
                Vector3 next = SVGReader.ProjectPoint(points[i + 3], projectionPlane);


                float t = 0;
                for (int w = 0; w < resolution; w++)
                {
                    t = ((w + 1) * 1.0f) / (resolution + 1);
                    p[j + w] = Cubic(current, inTangent, outTangent, next, t);
                    p[j + w] = SVGReader.FlipVerticaly(1, p[j + w], projectionPlane);

                }
                p[j + resolution] = SVGReader.FlipVerticaly(1,next, projectionPlane);
                //p[j + resolution] = next;
            }

            return p;
        }

        private int AnchorAmount => (points.Length - 1) / 3;
        private int ControlAmount => points.Length - AnchorAmount;
    }


    public class SVGReader : MonoBehaviour
    {
        [Serializable]
        public enum ProjectionPlane
        {
            XY,
            XZ,
            YZ
        }

        [Header("File Settings")]
        public string filePath;
        public string fileName;

        [Header("Core")]
        public ProjectionPlane projectionPlane;

        [Header("Render Settings")]
        [Range(0, 32)]
        public int resolution = 1;
        [Range(0.001f, 5f)]
        public float width = 0.5f;
        public Material pathMaterial;

        [Header("Debug")]
        public bool debug;

        private Vector2 svgDimensions;

        void Start()
        {
            var path = Application.persistentDataPath + filePath + "/" + fileName + ".svg";
            var beizerPath = new BeizerPath(GetSVGPath(LoadSVGFromFile(path)));

            if (beizerPath.points == null)
            {
                Debug.LogWarning($"Can't read file using [{path}] path!");
                return;
            }

            var positions = beizerPath.GetPath(resolution, projectionPlane);

            LineRenderer lineRenderer = new GameObject("SVG Path").AddComponent<LineRenderer>();
            lineRenderer.transform.position = transform.position;
            lineRenderer.transform.localEulerAngles = transform.localEulerAngles;
            lineRenderer.material = pathMaterial;
            lineRenderer.startWidth = lineRenderer.endWidth = width;
            lineRenderer.positionCount = positions.Length;
            lineRenderer.useWorldSpace = false;
            lineRenderer.SetPositions(positions);
        }

        private string LoadSVGFromFile(string path)
        {
            Debug.Log($"Reading file from [{path}] path");

            if (string.IsNullOrEmpty(path)) return string.Empty;

            string source = string.Empty;

            try
            {
                source = File.ReadAllText(path);
            }
            catch (System.Exception)
            {
                Debug.Log("File reading error!");
            }

            if (string.IsNullOrEmpty(source)) return source;

            var pathData = GetStringBetween(source, "d=", "style", out var endIdx);

            //Remove "" from start and end of string
            pathData = pathData.Substring(1, pathData.Length - 3);

            string svgDimensions = GetStringBetween(
                source, "viewBox=\"", "\"", out endIdx);

            string[] viewBoxValues = svgDimensions.Split(' ');

            if (ParsePoint(viewBoxValues[2], out float x) && ParsePoint(viewBoxValues[3], out float y))
            {
                this.svgDimensions = new Vector2(x, y);
            }

            return pathData;
        }

        private string GetStringBetween(string source, string start, string end, out int out_endIdx)
        {
            int startIdx = source.IndexOf(start, 0) + start.Length;
            int endIdx = source.IndexOf(end, startIdx);

            if (endIdx < 0)
            {
                out_endIdx = -1;
                return string.Empty;
            }

            out_endIdx = endIdx;

            return source.Substring(startIdx, endIdx - startIdx);
        }

        private SVGPath GetSVGPath(string svgFile)
        {
            if (string.IsNullOrEmpty(svgFile))
            {
                return null;
            }

            float beginTime = Time.realtimeSinceStartup;


            SVGPath path = new SVGPath(svgDimensions);

            //Get start point
            string startPointValue = GetStringBetween(svgFile, "M", "C", out var endIdx);

            if (string.IsNullOrEmpty(startPointValue))
            {
                startPointValue = GetStringBetween(svgFile, "C", "Z", out endIdx);

                if (string.IsNullOrEmpty(startPointValue))
                {
                    return null;
                }
            }

            //Parse start pint
            if (ParsePoint(startPointValue, out Vector2 startPoint))
            {
                path.start = startPoint;
            }
            else
            {
                return null;
            }

            //Go for cubic command untile reach until string length
            svgFile = svgFile.Substring(endIdx, svgFile.Length - endIdx);

            bool exit = false;

            do
            {
                //Get point
                string pointValue = GetStringBetween(svgFile, "C", "C", out endIdx);

                if (string.IsNullOrEmpty(pointValue))
                {
                    pointValue = GetStringBetween(svgFile, "C", "Z", out endIdx);

                    if (string.IsNullOrEmpty(pointValue))
                    {
                        return null;
                    }

                    exit = true;
                }

                //Make string into 3 substrings
                string[] points = pointValue.Split(' ');

                CubicCommand cubicCommand = new CubicCommand();
                cubicCommand.points_in_out_next = new List<Vector2>();

                for (int i = 0; i < points.Length; i++)
                {
                    if (ParsePoint(points[i], out Vector2 point))
                    {
                        cubicCommand.points_in_out_next.Add(point);
                    }
                    else
                    {
                        return null;
                    }
                }

                path.AddCommand(cubicCommand);

                svgFile = svgFile.Substring(endIdx, svgFile.Length - endIdx);
            }
            while (!exit && (Time.realtimeSinceStartup - beginTime) < 5f);

            if (debug)
            {
                Debug.Log($"Start:[{path.start}]");

                for (int i = 0; i < path.cubicCommands.Count; i++)
                {
                    path.cubicCommands[i].ConsoleWrite();
                }
            }

            return path;
        }

        private bool ParsePoint(string notSeparetedValue, out Vector2 point)
        {
            point = Vector2.zero;

            string[] points = notSeparetedValue.Split(',');

            bool succsess = false;

            try
            {
                point.x = float.Parse(points[0], CultureInfo.InvariantCulture);
                point.y = float.Parse(points[1], CultureInfo.InvariantCulture);

                succsess = true;
            }
            catch (System.Exception)
            {
                Debug.LogWarning("Can't parse start point!");
            }

            return succsess;
        }

        private bool ParsePoint(string pointValue, out float point)
        {
            point = 0f;

            bool succsess = false;

            try
            {
                point = float.Parse(pointValue, CultureInfo.InvariantCulture);

                succsess = true;
            }
            catch (System.Exception)
            {
            }

            return succsess;
        }

        public static Vector3 FlipVerticaly(float height, Vector3 input, ProjectionPlane projectionPlane)
        {
            switch (projectionPlane)
            {
                case ProjectionPlane.XY:
                    {
                        return new Vector3(input.x, height - input.y, 0f);
                    }
                case ProjectionPlane.XZ:
                    {
                        return new Vector3(input.x, 0f, height - input.z);
                    }
                case ProjectionPlane.YZ:
                    {
                        return new Vector3(0f, input.y, height - input.z);
                    }
                default:
                    {
                        return new Vector3(input.x, 0f, height - input.z);
                    }
            }
        }


        public static Vector3 ProjectPoint(Vector3 input, ProjectionPlane projectionPlane)
        {
            switch (projectionPlane)
            {
                case ProjectionPlane.XY:
                    {
                        return input;
                    }
                case ProjectionPlane.XZ:
                    {
                        return new Vector3(input.x, 0f, input.y);
                    }
                case ProjectionPlane.YZ:
                    {
                        return new Vector3(0f, input.x, input.y);
                    }
                default:
                    {
                        return new Vector3(input.x, 0f, input.y);
                    }
            }
        }
    }
}
