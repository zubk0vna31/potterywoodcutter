using System.Linq;
using UnityEngine;

namespace PWS
{
    public class PotteryDrawable : MonoBehaviour
    {
        [Header("Core")]
        [SerializeField]
        private MeshRenderer m_MeshRenderer;
        [SerializeField]
        private Texture2D m_Reference;
        [SerializeField,Range(0f,1f)]
        private float m_PixelThreshold=0.5f;
        [Header("Compare Function")]
        [SerializeField]
        private bool m_Greater = true;
        [Header("Drawing")]
        [SerializeField]
        private int m_Radius;
        [SerializeField]
        private bool m_UseCircle;

        private Texture2D m_Mask;
        private Material m_Material;
        private int m_MaskID;
        private int m_ReferenceAmount;
        private int m_PaintedAmount;

        public float Percentage => (m_PaintedAmount * 1.0f) / m_ReferenceAmount;

        public void Start()
        {
            //Material setup
            m_MaskID = Shader.PropertyToID("_Mask");
            m_Material = m_MeshRenderer.material;

            //Create mask texture
            m_Mask = new Texture2D(m_Reference.width, m_Reference.height, TextureFormat.R8, false);
            m_Mask.filterMode = FilterMode.Bilinear;

            m_Mask.SetPixels(Enumerable.Repeat(
                Color.black, m_Reference.width * m_Reference.height).ToArray());
            m_Mask.Apply();

            //Set mask texture to material
            m_Material.SetTexture(m_MaskID, m_Mask);

            //Calculate reference amount
            var colors = m_Reference.GetPixels();
            for (int i = 0; i < colors.Length; i++)
            {
                var threshold = CalculateThreshold(colors[i]);
                var success = m_Greater ? (threshold >= m_PixelThreshold) : (threshold <= m_PixelThreshold);

                if (success) m_ReferenceAmount++;
            }
        }

        public void Paint(Vector2Int pixel, int radius = 0, bool useCircle = false)
        {
            pixel.x = Mathf.Clamp(pixel.x, 0, m_Reference.width);
            pixel.y = Mathf.Clamp(pixel.y, 0, m_Reference.height);

            if (radius < 1)
            {
                useCircle = false;
            }

            bool needApplyTexture = false;

            for (int i = pixel.x-radius; i <= pixel.x+radius; i++)
            {
                for (int j = pixel.y - radius; j <= pixel.y + radius; j++)
                {
                    if (InBounds(i, j, m_Reference.width, m_Reference.height))
                    {
                        var current = new Vector2Int(i, j);
                        var threshold = CalculateThreshold(current);
                        var success = m_Greater ? (threshold >= m_PixelThreshold) : (threshold <= m_PixelThreshold);

                        if (!success) continue;


                        if (useCircle)
                        {
                            if (InCircle(new Vector2Int(i, j), pixel, radius))
                            {
                                m_Mask.SetPixel(i, j, Color.red);
                                needApplyTexture = true;

                                m_PaintedAmount++;
                            }
                        }
                        else
                        {
                            m_Mask.SetPixel(i, j, Color.red);
                            needApplyTexture = true;

                            m_PaintedAmount++;
                        }

                    }
                }
            }

            if (needApplyTexture)
            {
                m_Mask.Apply();
                m_Material.SetTexture(m_MaskID, m_Mask);
            }
        }

        private bool InBounds(int x, int y, int w, int h)
        {
            return x >= 0 && x < w && y >= 0 && y < h;
        }

        private bool InCircle(Vector2Int p,Vector2Int o,int radius)
        {
            return (Mathf.Pow(p.x - o.x, 2) + Mathf.Pow(p.y - o.y, 2) <= radius * radius);
        }

        private float CalculateThreshold(Vector2Int p)
        {
            return 0f;
        }

        private float CalculateThreshold(Color color)
        {
            return 0f;
        }
    }
}
