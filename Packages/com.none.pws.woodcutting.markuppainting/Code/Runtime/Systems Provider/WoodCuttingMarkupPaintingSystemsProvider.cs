using Leopotam.Ecs;
using Modules.Root.ContainerComponentModel;
using Modules.Root.ECS;
using PWS.Common.Audio;
using PWS.Common.GameModeInfoService;
using PWS.Common.ResultsEvaluation;
using PWS.Common.UI;
using PWS.WoodCutting.Common;
using UnityEngine;
using PWS.Common.TravelPoints;
using PWS.Features.Achievements;

namespace PWS.WoodCutting.MarkupPainting
{
    internal class StateDependencies
    {
        [Inject] public IGameModeInfoService gameModeInfoService;
        [Inject] public IResultsEvaluationDataHolder resultsEvaluation;
        [Inject] public IAchievementsService achievementsService;
    }

    public class WoodCuttingMarkupPaintingSystemsProvider : MonoBehaviour, ISystemsProvider
    {
        [Header("Core")]
        [SerializeField]
        private StateConfig _stateConfig;
        [SerializeField]
        private StateInfoData _stateInfo;

        [Header("Scene Related")]
        [SerializeField]
        private Transform _mainPlankTransform;
        [SerializeField]
        private Transform _additionalPlankTransform;
        [SerializeField]
        private GameObject _selectMenu;
        [SerializeField]
        private GameObject _colorMenu;

        [Header("Voice config")]
        [SerializeField] VoiceConfig _voiceConfig;


        public EcsSystems GetSystems(EcsWorld world, EcsSystems endFrame, EcsSystems mainSystems)
        {
            StateDependencies dependecies = new StateDependencies();
            SceneContainer.Instance.Inject(dependecies);

            EcsSystems systems = new EcsSystems(world, "WoodCutting Markup Painting");

            systems

                .Add(new SetStateDelayedSystem<MarkupPaintingState>())
                .Add(new SubstateSystem<MarkupPaintingState,MarkupPaintingSubstates,MarkupPaintingStateConfig>(_stateConfig,
                MarkupPaintingSubstates.SelectPattern,MarkupPaintingSubstates.Transition,
                MarkupPaintingSubstates.None))

                .Add(new OutlineByStateSystem<MarkupPaintingState>(_stateConfig))


                .Add(new MarkupPaintingSelectPatternSubstateSystemHandler(_stateConfig, MarkupPaintingSubstates.SelectPattern,_selectMenu, _mainPlankTransform))
                .Add(new MarkupPaintingDrawPatternSubstateSystemHandler(_stateConfig, MarkupPaintingSubstates.DrawingPattern,
                _additionalPlankTransform,dependecies.gameModeInfoService,dependecies.achievementsService,_colorMenu))


                //Tracker
                .Add(new MarkupPaintingTrackerSystem(_stateConfig,dependecies.gameModeInfoService))

                 .Add(new WoodCuttingSetGradeByProgress<MarkupPaintingState>(dependecies.resultsEvaluation,
                _stateInfo))

                .Add(new StateInfoWindowUIProcessing<MarkupPaintingState>(_stateInfo))
                .Add(new StateWindowPlacementProcessing<MarkupPaintingState>("Main"))

                .Add(new TravelPointPositioningSystem<MarkupPaintingState>("Default"))


                .Add(new MarkupPaintingUIProgressProcessing(_stateConfig))
                .Add(new MarkupPaintingUIButtonProcessing(_stateConfig))
                
                //Voice
                .Add(new VoiceAudioSystem<MarkupPaintingState>(_voiceConfig))

                ;

            endFrame

                .OneFrame<SubstateEnter>()
                .OneFrame<SubstateExit>()
                .OneFrame<ConfirmSignal>()
                .OneFrame<ResetSignal>()

                ;


            return systems;
        }
    }
}
