using Leopotam.Ecs;
using PWS.Common.GameModeInfoService;
using PWS.Common.UI;
using PWS.WoodCutting.Common;
using UnityEngine;

namespace PWS.WoodCutting.MarkupPainting
{
    public class MarkupPaintingTrackerSystem : RunSystem<MarkupPaintingState, MarkupPaintingStateConfig>
    {
        private readonly EcsFilter<Substate<MarkupPaintingSubstates>> _substate;
        private readonly EcsFilter<StateWindow, StateProgress> _stateWindowWithProgress;
        private readonly EcsFilter<PlankFinal> _plankFinal;
        private readonly IGameModeInfoService _gameModeInfoService;

        public MarkupPaintingTrackerSystem(StateConfig config,IGameModeInfoService gameModeInfoService) : base(config)
        {
            _gameModeInfoService = gameModeInfoService;
        }

        protected override void OnStateEnter()
        {
            base.OnStateEnter();

            foreach (var i in _stateWindowWithProgress)
            {
                ref var progress = ref _stateWindowWithProgress.Get2(i);

                progress.Value = 0.0f;
                progress.CompletionThreshold = 2f;
                
            }
        }

        protected override void OnStateExit()
        {
            base.OnStateExit();
        }

        protected override void OnStateUpdate()
        {
            base.OnStateUpdate();

            foreach (var i in _stateWindowWithProgress)
            {
                float value = 0f;

                foreach (var plank in _plankFinal)
                {
                    value += _plankFinal.Get1(plank).markupPaintingPattern.Progress;
                }

                value /= _plankFinal.GetEntitiesCount();

                if (value > 0.99f)
                {
                    value = 1f;
                }

                _stateWindowWithProgress.Get2(i).Value = value;
            }
        }
    }
}
