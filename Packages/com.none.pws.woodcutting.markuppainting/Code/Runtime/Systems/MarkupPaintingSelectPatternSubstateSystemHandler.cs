using DG.Tweening;
using Leopotam.Ecs;
using PWS.Common.UI;
using PWS.WoodCutting.Common;
using UnityEngine;

namespace PWS.WoodCutting.MarkupPainting
{
    public class MarkupPaintingSelectPatternSubstateSystemHandler : RunSubstateSystem<
        MarkupPaintingState, MarkupPaintingSubstates,MarkupPaintingStateConfig>
    {

        private readonly EcsFilter<PlankParent> _plankParent;
        private readonly EcsFilter<StateProgress> _stateProgress;

        private readonly EcsFilter<ConfirmSignal> _confirm;
        private readonly EcsFilter<ResetSignal> _reset;

        private readonly EcsFilter<PatternSelectMenu> _selectMenu;
        private readonly GameObject _menu;
        private readonly Transform _mainPlankTransform;

        public MarkupPaintingSelectPatternSubstateSystemHandler(StateConfig stateConfig,
            MarkupPaintingSubstates initialSubstate, 
             GameObject selectMenu, Transform mainPlankTransform) : base(stateConfig, initialSubstate)
        {
            _menu = selectMenu;
            _mainPlankTransform = mainPlankTransform;
        }

        protected override void OnStateEnter()
        {
            base.OnStateEnter();

            foreach (var i in _selectMenu)
            {
                _selectMenu.Get1(i).View.Initialize();
            }

            _menu.gameObject.SetActive(true);
        }

        protected override void OnSubstateEnter()
        {
            base.OnSubstateEnter();

            foreach(var i in _selectMenu)
                {
                _selectMenu.Get1(i).View.Show();
            }

            foreach (var i in _stateProgress)
            {
                _stateProgress.Get1(i).Value = 0f;
            }
        }

        protected override void OnSubstateUpdate()
        {
            base.OnSubstateUpdate();

            if (!_confirm.IsEmpty())
            {
                foreach (var i in _selectMenu)
                {
                    _selectMenu.Get1(i).View.Confirm();
                }

                foreach (var i in _substate)
                {
                    ref var signal = ref _substate.GetEntity(i).Get<ChangeSubstateSignal<MarkupPaintingSubstates>>();

                    signal.nextSubstate = MarkupPaintingSubstates.DrawingPattern;
                    signal.delay = _config.planksMoveDuration;
                }


                foreach (var i in _plankParent)
                {
                    _plankParent.Get1(i).transform.DOMove(_mainPlankTransform.position, _config.planksMoveDuration);
                }

                _menu.gameObject.SetActive(false);

                return;
            }

            if (!_reset.IsEmpty())
            {
                foreach (var i in _selectMenu)
                {
                    _selectMenu.Get1(i).View.ResetMenu();
                }

                return;
            }
        }

    }
}
