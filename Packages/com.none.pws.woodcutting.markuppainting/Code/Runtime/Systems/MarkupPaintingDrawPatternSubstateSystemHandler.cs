using Leopotam.Ecs;
using Modules.ViewHub;
using PWS.Common;
using PWS.Common.GameModeInfoService;
using PWS.Common.UI;
using PWS.Features.Achievements;
using PWS.Pottery.Stages.DecorativePainting;
using PWS.WoodCutting.Common;
using UnityEngine;

namespace PWS.WoodCutting.MarkupPainting
{
    public class MarkupPaintingDrawPatternSubstateSystemHandler : RunSubstateSystem<
        MarkupPaintingState, MarkupPaintingSubstates, MarkupPaintingStateConfig>
    {
        private readonly EcsFilter<NextStateSignal> _nextStateSignal;
        private readonly EcsFilter<PlatbandData> _platbandData;
        private readonly EcsFilter<PlankFinal> _plankFinal;
        private readonly EcsFilter<PlankParent, UnityView> _plankParrent;

        private readonly EcsFilter<StateProgress> _stateProgress;

        private readonly IGameModeInfoService _gameModeInfoService;
        private readonly IAchievementsService _achievementsService;
        private readonly Transform _additionalPlankTransform;
        private readonly GameObject _colorMenu;


        public MarkupPaintingDrawPatternSubstateSystemHandler(StateConfig stateConfig, 
            MarkupPaintingSubstates currentSubstate,Transform additionalPlankTransform,
            IGameModeInfoService gameModeInfoService, IAchievementsService achievementsService, GameObject colorMenu) : base(stateConfig, currentSubstate)
        {
            _additionalPlankTransform = additionalPlankTransform;
            _gameModeInfoService = gameModeInfoService;
            _achievementsService = achievementsService;
            _colorMenu = colorMenu;
        }


        protected override void OnSubstateEnter()
        {
            base.OnSubstateEnter();

            PlatbandData platbandData = default(PlatbandData);

            foreach (var data in _platbandData)
            {
                platbandData = _platbandData.Get1(data);
            }

            Transform plankParrent = null;

            foreach (var parent in _plankParrent)
            {
                plankParrent = _plankParrent.Get2(parent).Transform;
                _plankParrent.Get1(parent).Deactivate();
            }


            InitializePattern(platbandData.main, plankParrent);
            InitializePattern(platbandData.sub, plankParrent);

            foreach (var i in _stateProgress) 
            {
                    _stateProgress.Get1(i).SkipThreshold = _gameModeInfoService.CurrentMode.Equals(GameMode.Free) ?
                  0.0f : _config.skipThreshold;
            }
            
        }

        protected override void OnSubstateUpdate()
        {
            base.OnSubstateUpdate();

            if (!_nextStateSignal.IsEmpty())
            {
                float grade = 0f;
                foreach (var i in _stateProgress)
                {
                    grade = _stateProgress.Get1(i).ProgressValue;

                    // Achievement
                    _achievementsService.AppendProgressFloat(AchievementsNames.artist, grade);
                }
                _ecsWorld.NewEntity().Get<SetGradeSignal>().grade = grade;

                foreach (var i in _nextStateSignal)
                {
                    _nextStateSignal.Get1(i).button.gameObject.SetActive(false);
                    _nextStateSignal.GetEntity(i).Get<Ignore>();
                }

                foreach (var i in _plankFinal)
                {
                    _plankFinal.Get1(i).markupPaintingPattern.PaintAllWithDuration(_config.paintAllDuration);
                }

                var entity = _ecsWorld.NewEntity();
                entity.Get<SetStateDelayed>().state = _config.nextState;
                entity.Get<SetStateDelayed>().delay = _config.paintAllDuration;

                foreach (var i in _plankFinal)
                {
                    var entityTemplates = _plankFinal.Get1(i).view.State7.GetComponentsInChildren<EntityTemplate>(true);

                    foreach (var drillObject in entityTemplates)
                    {
                        drillObject.Init(_ecsWorld.NewEntity(), _ecsWorld);
                    }
                }

            }
        }
        private void InitializePattern(Pattern pattern,Transform plankParent)
        {
            Vector3 direction = plankParent.right;

            var go = Object.Instantiate(pattern.prefab,
                pattern.main ?
                (plankParent.position) : _additionalPlankTransform.position,
                pattern.main ? 
                Quaternion.LookRotation(direction, Vector3.up) : _additionalPlankTransform.rotation, null);


            var entity = _ecsWorld.NewEntity();

            var template = go.GetComponent<EntityTemplate>();
            template.Init(entity, _ecsWorld);

            ref var plankFinal = ref entity.Get<PlankFinal>();

            plankFinal.pattern = pattern;
            plankFinal.markupPaintingPattern.Initialize(pattern, _config.normalColor,
                _config.markupColor, _config.clipping);

        }
    }


}
