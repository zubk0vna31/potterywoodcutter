using Leopotam.Ecs;
using PWS.WoodCutting.Common;

namespace PWS.WoodCutting.MarkupPainting
{
    public class MarkupPaintingSubstateSystem : RunSystem<MarkupPaintingState,MarkupPaintingStateConfig>
    {
        // auto-injected fields
        private readonly EcsWorld _ecsWorld;
        private readonly EcsFilter<ChangeSubstateSignal<MarkupPaintingSubstates>> _changeSubstateSignal;

        // runtime data
        private EcsEntity _substateEntity;

        public MarkupPaintingSubstateSystem(StateConfig stateConfig):base(stateConfig)
        {

        }

        protected override void OnStateEnter()
        {
            base.OnStateEnter();

            _substateEntity = _ecsWorld.NewEntity();
            _substateEntity.Get<Substate<MarkupPaintingSubstates>>().substate = MarkupPaintingSubstates.SelectPattern;
            _substateEntity.Get<Substate<MarkupPaintingSubstates>>().nextSubstate = MarkupPaintingSubstates.None;
            _substateEntity.Get<SubstateEnter>();

        }

        protected override void OnStateUpdate()
        {
            base.OnStateUpdate();

            if (_substateEntity.Get<Substate<MarkupPaintingSubstates>>().nextSubstate != MarkupPaintingSubstates.None)
            {
                _substateEntity.Get<Substate<MarkupPaintingSubstates>>().substate = 
                    _substateEntity.Get<Substate<MarkupPaintingSubstates>>().nextSubstate;
                _substateEntity.Get<SubstateEnter>();
                _substateEntity.Get<Substate<MarkupPaintingSubstates>>().nextSubstate = MarkupPaintingSubstates.None;


                return;
            }

            if (_changeSubstateSignal.IsEmpty()) return;
         

            foreach (var i in _changeSubstateSignal)
            {
                _substateEntity.Get<SubstateExit>();
                _substateEntity.Get<Substate<MarkupPaintingSubstates>>().nextSubstate = _changeSubstateSignal.Get1(i).nextSubstate;
                _changeSubstateSignal.GetEntity(i).Del<ChangeSubstateSignal<MarkupPaintingSubstates>>();
            }
        }

        protected override void OnStateExit()
        {
            base.OnStateExit();


            _substateEntity.Destroy();
        }

    }
}
