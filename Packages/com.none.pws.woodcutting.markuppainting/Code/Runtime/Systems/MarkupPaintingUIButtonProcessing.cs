using Leopotam.Ecs;
using PWS.Common.UI;
using PWS.WoodCutting.Common;

namespace PWS.WoodCutting.MarkupPainting
{
    public class MarkupPaintingUIButtonProcessing : RunSystem<MarkupPaintingState, MarkupPaintingStateConfig>
    {
        private readonly EcsFilter<Substate<MarkupPaintingSubstates>> _substate;
        private readonly EcsFilter<Substate<MarkupPaintingSubstates>, SubstateEnter> _substateEnter;

        private readonly EcsFilter<StateWindow, StateProgress>.Exclude<Ignore> _stateWindowWithProgress;
        private readonly EcsFilter<StateWindow, Ignore> _stateWindowIgnored;


        public MarkupPaintingUIButtonProcessing(StateConfig config) : base(config)
        {
        }

        protected override void OnStateEnter()
        {
            base.OnStateEnter();

            foreach (var i in _stateWindowIgnored)
            {
                _stateWindowWithProgress.GetEntity(i).Del<Ignore>();
            }

        }

        protected override void OnStateExit()
        {
            base.OnStateExit();

            foreach (var i in _stateWindowIgnored)
            {
                _stateWindowWithProgress.Get1(i).Template.ShowButton(false);
            }
        }

        protected override void OnStateUpdate()
        {
            base.OnStateUpdate();

            if (_substate.IsEmpty()) return;

            foreach (var i in _substate)
            {
                if (_substate.Get1(i).substate != MarkupPaintingSubstates.DrawingPattern)
                {
                    return;
                }
            }

            if (!_substateEnter.IsEmpty())
            {
                foreach (var i in _stateWindowWithProgress)
                {
                    _stateWindowWithProgress.Get1(i).Template.ShowSlider(true);
                }
            }

            foreach (var i in _stateWindowWithProgress)
            {
                _stateWindowWithProgress.Get1(i).Template.ShowButton(
                        _stateWindowWithProgress.Get2(i).Value >= _stateWindowWithProgress.Get2(i).SkipThreshold);
            }
        }


    }
}
