using Leopotam.Ecs;
using PWS.Common.UI;
using PWS.WoodCutting.Common;
using UnityEngine;

namespace PWS.WoodCutting.MarkupPainting
{
    public class MarkupPaintingUIProgressProcessing : RunSystem<MarkupPaintingState,MarkupPaintingStateConfig>
    {
        private readonly EcsFilter<Substate<MarkupPaintingSubstates>> _substate;
        private readonly EcsFilter<Substate<MarkupPaintingSubstates>, SubstateEnter> _substateEnter;
        private readonly EcsFilter<StateWindow, StateProgress> _stateWindowWithProgress;

        public MarkupPaintingUIProgressProcessing(StateConfig config) : base(config)
        {
        }

        protected override void OnStateEnter()
        {
            base.OnStateEnter();
        }

        protected override void OnStateExit()
        {
            base.OnStateExit();

            foreach (var i in _stateWindowWithProgress)
            {
                _stateWindowWithProgress.Get1(i).Template.SetSliderValue(0);
                _stateWindowWithProgress.Get1(i).Template.SetSliderLabel(0);
                _stateWindowWithProgress.Get1(i).Template.ShowSlider(false);
            }
        }

        protected override void OnStateUpdate()
        {
            base.OnStateUpdate();

            if (_substate.IsEmpty()) return;

            foreach (var i in _substate)
            {
                if (_substate.Get1(i).substate != MarkupPaintingSubstates.DrawingPattern)
                {
                    return;
                }
            }

            if (!_substateEnter.IsEmpty())
            {
                foreach (var i in _stateWindowWithProgress)
                {
                    _stateWindowWithProgress.Get1(i).Template.ShowSlider(true);
                }
            }

            foreach (var i in _stateWindowWithProgress)
            {
                float value = _stateWindowWithProgress.Get2(i).Value;
                float thresold = _stateWindowWithProgress.Get2(i).CompletionThreshold;

                _stateWindowWithProgress.Get1(i).Template.SetSliderValue(value);
                _stateWindowWithProgress.Get1(i).Template.SetSliderLabel(Mathf.Floor(value * 1000) / 10);
            }
        }

       
    }
}
