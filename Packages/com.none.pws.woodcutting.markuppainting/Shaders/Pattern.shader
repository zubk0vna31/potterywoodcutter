Shader "Unlit/Pattern"
{
    Properties
    {
        _MainTex("Main Texture", 2D) = "white" {}
        _MarkupTex("Markup Texture", 2D) = "white" {}
        _Color1("Color 1", Color) = (1,1,1,1)
        _Color2("Color 2", Color) = (0,0,0,1)
        _Clipping("Clipping",Range(0,1)) = 0.5
        //[HideInInspector]
        _Mask ("Mask", 2D) = "black" {}
    }
    SubShader
    {
         Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}
        LOD 100

        ZWrite Off
        Blend SrcAlpha OneMinusSrcAlpha 

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
            };

            sampler2D _Mask,_MarkupTex;
            sampler2D _MainTex;

            half4 _Color1,_Color2;
            half _Clipping;
            int _PaintAll;
            float _PaintAllValue;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }
            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 col = tex2D(_MainTex,i.uv);
                fixed4 col2 = tex2D(_MarkupTex,i.uv);


                half mask = tex2D(_Mask, i.uv).r;


                if(_PaintAll==0)
                {
                    col = lerp(col,col2,mask);
                    col = col*lerp(_Color1,_Color2,mask);
                }
                else 
                {
                    float t = lerp(mask,1,_PaintAllValue);
                    col = lerp(col,col2,t);
                    col = col*lerp(_Color1,_Color2,t);
                }

                UNITY_APPLY_FOG(i.fogCoord, col);

                 if (col.a < _Clipping)
                // alpha value less than user-specified threshold?
                {
                discard; // yes: discard this fragment
                }

                return col;
            }
            ENDCG
        }
    }
}
