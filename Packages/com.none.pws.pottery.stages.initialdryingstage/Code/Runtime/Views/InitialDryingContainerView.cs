using Leopotam.Ecs;
using Modules.ViewHub;

namespace PWS.Pottery.Stages.InitialDryingStage
{
    public class InitialDryingContainerView : ViewComponent
    {
        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            ecsEntity.Get<DryingContainer>();
        }
    }
}
