using Leopotam.Ecs;
using Modules.ViewHub;
using PWS.Pottery.ItemDataHolderService;

namespace PWS.Pottery.Stages.InitialDryingStage
{
    public class DryingRegulatorView : ViewComponent
    {
        public override void EntityInit(EcsEntity ecsEntity, EcsWorld ecsWorld)
        {
            ecsEntity.Get<DryingRegulator>();
        }
    }
}
