using Leopotam.Ecs;
using Modules.Root.ContainerComponentModel;
using Modules.Root.ECS;
using Modules.StateGroup.Core;
using Modules.VRFeatures;
using PWS.Common.Audio;
using PWS.Common.Messages;
using PWS.Common.RestartStage;
using PWS.Common.ResultsEvaluation;
using PWS.Common.SceneSwitchService;
using PWS.Common.TravelPoints;
using PWS.Common.UI;
using PWS.Common.ValueRegulator;
using PWS.Features.Achievements;
using PWS.Pottery.Common;
using PWS.Pottery.ItemDataHolderService;
using PWS.Pottery.MeshGeneration;
using UnityEngine;

namespace PWS.Pottery.Stages.InitialDryingStage
{
    public class PotteryInitialDryingStageSystemsProvider : MonoBehaviour, ISystemsProvider
    {
        internal class SystemsDependencies
        {
            [Inject] public IValueRegulatorDataHolder<DryingRegulator> DryingData;
            [Inject] public IResultsEvaluationDataHolder ResultsEvaluationData;
            [Inject] public ISceneSwitchService SceneSwitchService;
            [Inject] public IAchievementsService AchievementsService;
        }

        [SerializeField] private SetStateSOCommand _nextState;
        [SerializeField] private DryingStateConfig _config;
        [SerializeField] private ValueRegulatorConfig _regulatorConfig;
        [SerializeField] private StateInfoData _stateInfo;
        
        [Header("Voice config")] 
        [SerializeField] private VoiceConfig _voiceConfig;

        [Header("SoundConfig")] 
        [SerializeField] private SculptingProductSoundConfig _soundConfig;

        public EcsSystems GetSystems(EcsWorld world, EcsSystems endFrame, EcsSystems mainSystems)
        {
            SystemsDependencies dependencies = new SystemsDependencies();
            SceneContainer.Instance.Inject(dependencies);

            EcsSystems systems = new EcsSystems(world);
            systems
                .Add(new FadeToStateOnUIActionSystem<InitialDryingState>(_nextState, dependencies.SceneSwitchService))
                .Add(new CurrentStateRestoreDataProcessing<InitialDryingState, ValueRegulatorDataHolder<DryingRegulator>>(dependencies.DryingData))

                //state systems
                .Add(new SnapSocketActivityProcessing<InitialDryingState, InitialDryingSnapSocket>())
                .Add(new GameObjectVisibilityProcessing<InitialDryingState, DryingContainer>())

                .Add(new GameObjectVisibilityProcessing<InitialDryingState, DryingRegulator>(true))
                .Add(new ValueRegulatorSetupProcessing<InitialDryingState, DryingRegulator>(_regulatorConfig, dependencies.DryingData, true))

                .Add(new DryingCompletionTracker<InitialDryingState>(_regulatorConfig, dependencies.DryingData))
                .Add(new DryingDrynessProcessing<InitialDryingState>(_config))

                // Achievement
                .Add(new DarknessAchievementSender<InitialDryingState>(dependencies.AchievementsService))

                //Outline
                .Add(new OutlineByStateSystem<InitialDryingState>(0))

                .Add(new SetGradeByValueRegulator<InitialDryingState, DryingRegulator>(dependencies.ResultsEvaluationData,
                dependencies.DryingData, _stateInfo, _config.TargetDays))

                //ui
                .Add(new StateInfoWindowUIProcessing<InitialDryingState>(_stateInfo))
                .Add(new StateWindowPlacementProcessing<InitialDryingState>("Drying"))
                .Add(new StateWindowButtonUIProcessing<InitialDryingState>())

                .Add(new XRInteractableProcessing<InitialDryingState, SculptingProductTag>())

                .Add(new MeshVisibilityProcessing<InitialDryingState, SculptingProductTag>())
                
                //voice
                .Add(new VoiceAudioSystem<InitialDryingState>(_voiceConfig))
                
                //sound
                .Add(new FoleySoundsAddingSystem(_soundConfig))

                //travel point
                .Add(new TravelPointPositioningSystem<InitialDryingState>("Shelf"))

                ;

            endFrame
                //drying
                .Add(new ValueRegulatorUpdateProcessing<InitialDryingState, DryingRegulator>(dependencies.DryingData))

                .OneFrame<RestartCurrentStepMessage>()
                .OneFrame<NextStateSignal>()
                .OneFrame<ValueRegulatorChangedSignal>()
                ;

            return systems;
        }
    }
}
