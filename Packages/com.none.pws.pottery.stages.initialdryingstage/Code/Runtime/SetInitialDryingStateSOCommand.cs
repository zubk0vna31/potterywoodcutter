using Modules.StateGroup.Core;
using UnityEngine;

namespace PWS.Pottery.Stages.InitialDryingStage
{
    [CreateAssetMenu(fileName = "SetInitialDryingStateSOCommand", menuName = "PWS/Pottery/Stages/InitialDryingStage/SetStateSOCommand")]
    public class SetInitialDryingStateSOCommand : SetStateSOCommand<InitialDryingState>
    {

    }
}