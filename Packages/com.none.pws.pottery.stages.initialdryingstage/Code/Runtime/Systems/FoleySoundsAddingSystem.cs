﻿using Leopotam.Ecs;
using Modules.StateGroup.Components;
using Modules.ViewHub;
using PWS.Common.Audio;
using PWS.Pottery.MeshGeneration;
using UnityEngine.XR.Interaction.Toolkit;

namespace PWS.Pottery.Stages.InitialDryingStage
{
    public class FoleySoundsAddingSystem : IEcsRunSystem
    {
        private readonly EcsFilter<StateEnter, InitialDryingState> _enterState;
        private readonly EcsFilter<UnityView, SculptingProductTag> _product;

        private SculptingProductSoundConfig _config;

        public FoleySoundsAddingSystem(SculptingProductSoundConfig config)
        {
            _config = config;
        }
        public void Run()
        {
            if (!_enterState.IsEmpty())
            {
                foreach (var i in _product)
                {
                    ref var product = ref _product.GetEntity(i).Get<FoleyComponent>();
                    product.Interactable = _product.Get1(i).GameObject.GetComponent<XRGrabInteractable>();
                    product.DropClips = _config.DropClips;
                    product.PickUpClips = _config.PickUpClips;
                    product.DropSoundPrefab = _config.DropSoundPrefab;
                    product.PickUpSoundPrefab = _config.PickUpSoundPrefab;
                    product.Interactable.selectEntered.AddListener(arg =>
                    {
                        _product.GetEntity(i).Get<PickUpSignal>();
                    });
                }
            }
        }
    }
}