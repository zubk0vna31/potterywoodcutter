using Leopotam.Ecs;
using Modules.StateGroup.Components;
using Modules.Utils;
using Modules.VRFeatures;
using PWS.Features.Achievements;
using System;
using UnityEngine;

namespace PWS.Pottery.Stages.InitialDryingStage
{
    public class DarknessAchievementSender<T> : IEcsRunSystem where T : struct
    {
        private readonly EcsFilter<T> _state;
        private readonly EcsFilter<StateEnter,T> _stateEnter;

        private readonly EcsFilter<InitialDryingSnapSocket, SelectEntered> _enter;
        private readonly EcsFilter<InitialDryingSnapSocket, SelectExited> _exit;

        private readonly IAchievementsService _achievementsService;
        private readonly TimeService _timeService;

        private DateTime _dateTime;
        private bool _inSocket;
        private bool _enabled;
        private float _timer,_maxValue;
        private float _previousValue=-1f;


        public DarknessAchievementSender(IAchievementsService achievementsService)
        {
            _achievementsService = achievementsService;
        }

        private bool InSocket
        {
            get => _inSocket;

            set
            {
                _inSocket = value;
                OnSocketValueChanged(_inSocket);
            }
        }

        private void OnSocketValueChanged(bool inSocket)
        {
            if (inSocket)
            {
                _dateTime = DateTime.UtcNow;

                if (_previousValue < 0)
                {
                    _previousValue = _achievementsService.TryGetProgressInt(AchievementsNames.darkness);
                    _maxValue = _achievementsService.TryGetReferenceInt(AchievementsNames.darkness);
                }

                _timer = _previousValue;
            }
            else
            {
                if(_dateTime.Equals(default(DateTime))) _dateTime = DateTime.UtcNow;

                var span = DateTime.UtcNow - _dateTime;
                _previousValue = Mathf.Clamp(_previousValue+(int)span.TotalSeconds,0,_maxValue);
                _achievementsService.AppendProgressInt(AchievementsNames.darkness, (int)span.TotalSeconds);
                _enabled = !_achievementsService.IsUnlocked(AchievementsNames.darkness);
            }
        }

        public void Run()
        {
            if (_state.IsEmpty()) return;

            if (!_stateEnter.IsEmpty())
            {
                _enabled = true;
            }

            if (!_enabled) return;

            if (!_enter.IsEmpty())
            {
                InSocket = true;
            }

            if (!_exit.IsEmpty())
            {
                InSocket = false;
            }

            if (InSocket)
            {
                if (_timer < _maxValue)
                {
                    _timer += _timeService.DeltaTime;

                    if(_timer >= _maxValue)
                    {
                        _achievementsService.Unlock(AchievementsNames.darkness);
                        _enabled = false;
                    }
                }
            }
        }
    }
}
