﻿using Leopotam.Ecs;
using Modules.StateGroup.Components;
using PWS.Pottery.Common;
using PWS.Pottery.MeshGeneration;

namespace PWS.Pottery.Stages.InitialDryingStage
{
    public class DryingDrynessProcessing<StateT> : IEcsRunSystem where StateT : struct
    {
        // auto injected fields
        readonly EcsFilter<StateExit, StateT> _exiting;

        readonly EcsFilter<ClayMeshRenderer, SculptingProductTag> _meshRenderer;

        private DryingStateConfig _config;
        private const string Dryness_ID = "_Dryness";

        public DryingDrynessProcessing(DryingStateConfig config)
        {
            _config = config;
        }

        public void Run()
        {
            if (!_exiting.IsEmpty())
            {
                foreach (var mr in _meshRenderer)
                {
                    _meshRenderer.Get1(mr).MeshRenderer.sharedMaterial.SetFloat(Dryness_ID, _config.DrynessMultiplier);
                }
            }
        }
    }
}