﻿
using Leopotam.Ecs;
using PWS.Common.UI;
using Modules.StateGroup.Components;
using PWS.Common.ValueRegulator;
using PWS.Pottery.ItemDataHolderService;
using Modules.VRFeatures;

namespace PWS.Pottery.Stages.InitialDryingStage
{
    public class DryingCompletionTracker<StateT> : IEcsRunSystem where StateT : struct
    {

        // auto injected fields
        readonly EcsFilter<StateEnter, StateT> _entering;
        readonly EcsFilter<StateT> _inState;
        readonly EcsFilter<StateWindow, StateProgress> _stateWindowWithProgress;

        readonly EcsFilter<ValueRegulatorChangedSignal, DryingRegulator> _changedSignal;

        readonly EcsFilter<InitialDryingSnapSocket, SelectEntered> _selectEnteredSignal;
        readonly EcsFilter<InitialDryingSnapSocket, SelectExited> _selectExitedSignal;

        private ValueRegulatorConfig _config;
        private IValueRegulatorDataHolder<DryingRegulator> _data;

        //runtime data
        private float _minValue;
        private float _maxValue;
        private bool _inSocket;

        public DryingCompletionTracker(ValueRegulatorConfig config, IValueRegulatorDataHolder<DryingRegulator> data)
        {
            _config = config;
            _data = data;

            _minValue = _data.ConvertFromCustom(_config.CompletionCustomValueMin);
            _maxValue = _data.ConvertFromCustom(_config.CompletionCustomValueMax);
        }

        public void Run()
        {
            if (!_entering.IsEmpty())
            {
                //_inSocket = false;

                foreach (var i in _stateWindowWithProgress)
                {
                    _stateWindowWithProgress.Get2(i).Value = 0;
                    _stateWindowWithProgress.Get2(i).CompletionThreshold = 0.5f;
                }
            }

            if (!_selectEnteredSignal.IsEmpty())
                _inSocket = true;
            if (!_selectExitedSignal.IsEmpty())
                _inSocket = false;

            if (!_inState.IsEmpty())
            {
                bool show = _data.Value >= _minValue && _data.Value <= _maxValue;

                foreach (var i in _stateWindowWithProgress)
                {
                    _stateWindowWithProgress.Get2(i).Value = show && _inSocket ? 1 : 0;
                }
            }
        }
    }
}