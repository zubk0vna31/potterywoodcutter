using UnityEngine;

namespace PWS.Pottery.Stages.InitialDryingStage
{
    [CreateAssetMenu(fileName = "InitialDryingStateConfig", menuName = "PWS/Pottery/Stages/InitialDryingStage/Config", order = 0)]
    public class DryingStateConfig : ScriptableObject
    {
        public float TargetDays = 2;
        [Range(0, 1)]
        public float DrynessMultiplier = 0.5f;

    }
}
