﻿using Modules.Audio;
using PWS.Common.Audio;
using UnityEngine;

namespace PWS.Pottery.Stages.InitialDryingStage
{
    [CreateAssetMenu(menuName = "PWS/Common/Audio/SculptingProductSoundConfig", fileName = "SculptingProductSoundConfig", order = 0)]
    public class SculptingProductSoundConfig : ScriptableObject
    {
        public OneShotSoundTemplate PickUpSoundPrefab;
        public OneShotSoundTemplate DropSoundPrefab;

        public RandomSoundClipsData PickUpClips;
        public RandomSoundClipsData DropClips;
    }
}