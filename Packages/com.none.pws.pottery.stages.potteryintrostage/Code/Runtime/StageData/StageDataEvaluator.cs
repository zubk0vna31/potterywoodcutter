using PWS.Pottery.Common;

namespace PWS.Pottery.Stages.PotteryIntroStage
{
    public class StageDataEvaluator : IStageDataEvaluator
    {
        public float CompletionPercentage => 1.0f;
    }
}