using PWS.Pottery.Common;

namespace PWS.Pottery.Stages.PotteryIntroStage
{
    public class StageDataVisualiser : IStageVisualiser
    {
        private StageData _stageData;

        public StageDataVisualiser(StageData stageData)
        {
            _stageData = stageData;
        }

        public void Visualise()
        {
            // pass, nothing to visualise in pottery intro stage
            return;
        }
    }
}