﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;
using UnityEngine.Rendering;

public enum RotationAxis
{
    Right,  
    Up,
    Forward
}

public class CircularMotion : MonoBehaviour,IInteractable
{
    [Header("Core")]
    [SerializeField]
    private RotationAxis axis;

    [SerializeField]
    private float minAngle = 0,maxAngle = 90;

    [SerializeField]
    private bool forceStart;

    [SerializeField]
    private float startAngle=45;

    [Space(30)]
    [SerializeField]
    private UnityEvent onHoverBegin, onHoverEnd,onMinAngleReach,onMaxAngleReach;

    private Quaternion startRotation;
    private float currentAngle;

    private float minMaxAngleThreshold = 1.0f;

    private Vector3 worldPlaneNormal,localPlaneNormal;

    private bool affecting;
    private Vector3 previousProjectPosition;

    public Action<float> OnRotationPerformed;

    private bool _enabled;
    public bool Enabled
    {
        get => _enabled;

        set
        {
            _enabled = value;

            if (!_enabled && affecting)
            {
                affecting = false;
                onHoverEnd?.Invoke();
            }
        }
    }

    public float TwistAmount => Mathf.Clamp01(currentAngle / maxAngle);

    private void Start()
    {
        Initialize();
    }

    private void OnEnable()
    {
        Enabled = true;
    }

    private void OnDisable()
    {
        Enabled = false;
    }
    private void Initialize()
    {
        worldPlaneNormal = Vector3.zero;
        worldPlaneNormal[(int)axis] = 1.0f;
        localPlaneNormal = worldPlaneNormal;

        if (transform.parent)
        {
            worldPlaneNormal = transform.parent.localToWorldMatrix.MultiplyVector(worldPlaneNormal).normalized;
        }

        startRotation = transform.localRotation;
        currentAngle = 0f;

        if (forceStart)
        {
            currentAngle = Mathf.Clamp(startAngle, minAngle, maxAngle);
        }

        UpdateTransform();
    }

    private Vector3 ComputeProjectPosition(Hand hand)
    {
        Vector3 relative = (hand.transform.position - transform.position).normalized;

        return Vector3.ProjectOnPlane(relative, worldPlaneNormal).normalized;
    }

    private void ComputeAngle(Hand hand)
    {
        Vector3 currentProjectPosition = ComputeProjectPosition(hand);

        if (currentProjectPosition.Equals(previousProjectPosition)) return;

        float absAngle = Vector3.Angle(previousProjectPosition, currentProjectPosition);

        if (absAngle < Mathf.Epsilon) return;

        Vector3 cross = Vector3.Cross(previousProjectPosition, currentProjectPosition);

        float dot = Vector3.Dot(worldPlaneNormal, cross);

        float signedAngle = absAngle;

        if (dot < 0.0f)
        {
            signedAngle = -signedAngle;
        }


        float angleTmp = Mathf.Clamp(currentAngle + signedAngle, minAngle, maxAngle);

        bool success = true;

        if(currentAngle == minAngle)
        {
            if(!(angleTmp > minAngle && absAngle < minMaxAngleThreshold))
            {
                success = false;
            }
        }
        else if(currentAngle == maxAngle)
        {
            if (!(angleTmp < maxAngle && absAngle < minMaxAngleThreshold))
            {
                success = false;
            }
        }
        else if(angleTmp == minAngle)
        {
            onMinAngleReach?.Invoke();
        }
        else if(angleTmp == maxAngle)
        {
            onMaxAngleReach?.Invoke();
            Debug.Log("Max Angle Reached");
        }

        if (success)
        {
            currentAngle = angleTmp;
            previousProjectPosition = currentProjectPosition;
        }
    }
    
    private void UpdateTransform()
    {
        transform.localRotation = startRotation * Quaternion.AngleAxis(currentAngle, localPlaneNormal);

        OnRotationPerformed?.Invoke(Mathf.InverseLerp(minAngle, maxAngle, currentAngle));
    }

    public void HandHoverBegin(Hand hand)
    {
        if (!Enabled) return;

        onHoverBegin?.Invoke();
    }

    public void HandHoverUpdate(Hand hand)
    {
        if (!Enabled) return;

        if (hand.GrabButton(InputType.Down))
        {
            previousProjectPosition =  ComputeProjectPosition(hand);

            affecting = true;

            ComputeAngle(hand);
            UpdateTransform();
        }
        else if (hand.GrabButton(InputType.Up))
        {
            affecting = false;
        }
        else if(affecting && hand.GrabButton(InputType.Hold))
        {
            ComputeAngle(hand);
            UpdateTransform();
        }
    }

    public void HandHoverEnd(Hand hand)
    {
        if (!Enabled) return;

        if (affecting)
            affecting = false;

        onHoverEnd?.Invoke();
    }

    public void AddMaxAngleReachedEvent(UnityAction maxAngleReachEvent)
    {
        onMaxAngleReach.AddListener(maxAngleReachEvent);
    }

    public void AddMinAngleReachedEvent(UnityAction minAngleReachEvent)
    {
        onMinAngleReach.AddListener(minAngleReachEvent);
    }
}
