﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IInteractable
{
    void HandHoverBegin(Hand hand);

    void HandHoverUpdate(Hand hand);

    void HandHoverEnd(Hand hand);

}
