﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public enum InputType
{
    Down,
    Hold,
    Up
}

[RequireComponent(typeof(SphereCollider),typeof(Rigidbody))]
public class Hand : MonoBehaviour
{
    public string interactionTag;
    public InputActionReference grabAction;

    private HashSet<IInteractable> interactables = new HashSet<IInteractable>();

    private bool pressedDown,pressed, pressedUp;
    private bool pressedDownDissable, pressedUpDissable;

    #region Unity Methods

    private void Start()
    {
        grabAction.action.started += (InputAction.CallbackContext context) =>
        {
            pressedDown = true;
            pressed = true;

            pressedDownDissable = true;
        };
        grabAction.action.canceled += (InputAction.CallbackContext context) =>
        {
            pressedUp = true;
            pressed = false;

            pressedUpDissable = true;
        };
    }

    private void Update()
    {
        foreach (var interactable in interactables)
        {
            interactable.HandHoverUpdate(this);
        }
    }

    private void LateUpdate()
    {
        if (pressedDownDissable)
        {
            pressedDown = false;
            pressedDownDissable = false;
        }

        if (pressedUpDissable)
        {
            pressedUp = false;
            pressedUpDissable = false;
        }
    }

    #endregion

    #region Trigger Events

    private void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag(interactionTag)) return;

        var component = other.GetComponentInParent<IInteractable>();

        if (component!=null && !interactables.Contains(component))
        {
            interactables.Add(component);
            component.HandHoverBegin(this);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (!other.CompareTag(interactionTag) ) return;

        var component = other.GetComponentInParent<IInteractable>();

        if (component != null && interactables.Contains(component))
        {
            component.HandHoverEnd(this);
            interactables.Remove(component);
        }
    }

    #endregion

    #region Public Methods

    public bool GrabButton(InputType type)
    {
        switch (type)
        {
            case InputType.Down:
                {
                    return pressedDown;
                }
            case InputType.Hold:
                {
                    return pressed;
                }
            case InputType.Up:
                {
                    return pressedUp;
                }
            default:
                {
                    return false;
                }
        }
            
    }

    #endregion
}
