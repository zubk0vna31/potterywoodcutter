﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseMove : MonoBehaviour
{
    private float distance;
    private Vector3 offset;

    private void OnMouseDown()
    {
        distance = Vector3.Distance(Camera.main.transform.position, transform.position);
        offset = transform.position - CalculateMouseWorldPos();
    }

    private Vector3 CalculateMouseWorldPos()
    {
        return Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, distance));
    }

    private void OnMouseDrag()
    {
        transform.position = CalculateMouseWorldPos() + offset;
    }
}
