public interface IGrabbableDependable
{
    void UpdateDependency(float value);
}
