﻿using UnityEngine;

public class AxisMover : MonoBehaviour,IGrabbableDependable
{
    [SerializeField]
    private RotationAxis moveAxis;

    [SerializeField]
    private Transform endPosition;

    [SerializeField]
    private bool useRaycast;

    [SerializeField]
    private LayerMask searchLayer;

    [SerializeField]
    private bool pivotCentered;

    private Vector3 initialPosition;
    private Vector3 direction;
    private float length;
    private bool initialized;

    private void OnEnable()
    {
        initialPosition = transform.localPosition;

        direction = Vector3.zero;
        direction[(int)moveAxis] = 1.0f;

        if (transform.parent)
        {
            direction = transform.parent.localToWorldMatrix.MultiplyVector(direction).normalized;
        }

        Vector3 projectedPosition = endPosition.position;

        for (int i = 0; i < 3; i++)
        {
            if ((int)moveAxis == i) continue;

            Vector3 currentDirection = Vector3.zero;
            currentDirection[i] = 1.0f;

            if (transform.parent)
            {
                currentDirection = transform.parent.localToWorldMatrix.MultiplyVector(currentDirection).normalized;
            }

            projectedPosition = Vector3.ProjectOnPlane(projectedPosition, currentDirection);
            projectedPosition = projectedPosition + currentDirection * Vector3.Dot(transform.position - projectedPosition, currentDirection);
        }

        Vector3 worldDirection = (projectedPosition - transform.position).normalized;

        //Transform to local space 
        direction = transform.parent.InverseTransformDirection(worldDirection);

        var boxCollider = GetComponent<BoxCollider>();

        if (useRaycast &&
            Physics.Raycast(transform.position, worldDirection, out var hit, 100f,
            searchLayer, QueryTriggerInteraction.Collide))
        {
            if (pivotCentered)
            {
                length = hit.distance - boxCollider.size[(int)moveAxis] * 0.5f;
            }
            else
            {
                length = hit.distance;
            }
            endPosition.position = transform.position + worldDirection * length;
        }
        else
        {
            length = (projectedPosition - transform.position).magnitude - boxCollider.size[(int)moveAxis] * 0.5f;
            endPosition.position = transform.position + worldDirection * length;
        }

        initialized = true;
    }

    public void UpdateDependency(float value)
    {
        if (!initialized) return;
        transform.localPosition = initialPosition + Mathf.Lerp(0f, length, value) * direction;
    }

    public void ResetComponent()
    {
        UpdateDependency(0f);
    }
}
