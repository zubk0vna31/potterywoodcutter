﻿using I2.Loc;
using UnityEngine;

namespace PWS.FeatureToggles
{
    public class AdditionalFeaturesToggle : MonoBehaviour
    {
        [SerializeField]
        private SetLanguage _defaultLanguage;

        [SerializeField]
        private GameObject _target;

        [SerializeField]
        private GameObject _russian;

        [SerializeField]
        private GameObject _english;

        private void Start()
        {
            bool achievements = FeatureManagerFacade.FeatureEnabled(PWS.HUB.Simulation.Features.PWS_Achievements);
            bool localization = FeatureManagerFacade.FeatureEnabled(PWS.HUB.Simulation.Features.PWS_Localization);

            if (!achievements && !localization)
            {
                _target.SetActive(false);

                /*if (_defaultLanguage)
                    _defaultLanguage.ApplyLanguage();*/
            }
            else
            {
                _russian.SetActive(localization);
                _english.SetActive(localization);

               
            }
        }
    }
}