using Leopotam.Ecs;
using Modules.UIMessaging.Data;
using Modules.UIMessaging.Messages;
using PWS.HUB.Simulation.Components;
using Modules.StateGroup.Components;
using Modules.StateGroup.Core;
using UnityEngine;

namespace PWS.HUB.Simulation
{
    public class RegistrationStateProcessing : IEcsRunSystem
    {
        // auto injected fields
        private readonly EcsFilter<RegistrationState> _inState;
        private readonly EcsFilter<StateEnter, RegistrationState> _entering;
        private readonly EcsFilter<UIActionPerformedMessage> _actionPerformed;
        private readonly StateFactory _stateFactory;
        
        private TargetActionPair _show;
        private TargetActionPair _confirm;

        public RegistrationStateProcessing(TargetActionPair show, TargetActionPair confirm)
        {
            _show = show;
            _confirm = confirm;
        }

        public void Run()
        {
            if (!_entering.IsEmpty())
            {
                UniRx.MessageBroker.Default.Publish(new PerformUIActionMessage(_show));
            }
            if (!_inState.IsEmpty())
            {
                foreach (var i in _actionPerformed)
                {
                    if (_confirm.IsIn(_actionPerformed.Get1(i)))
                    {
                        _stateFactory.SetState<TutorialProposalState>();
                    }
                }
            }
        }
    }
}
