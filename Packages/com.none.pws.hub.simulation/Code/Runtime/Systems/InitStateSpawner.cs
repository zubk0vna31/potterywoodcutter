using Leopotam.Ecs;
using Modules.Root.ContainerComponentModel;
using Modules.StateGroup.Components;
using Modules.StateGroup.Core;
using PWS.Common.UserInfoService;
using PWS.HUB.Simulation.Components;
using PWS.Tutorial;
using PWS.HUB.UserRegistrtationPad;
using PWS.FeatureToggles;

namespace PWS.HUB.Simulation.Systems
{
    // issues initial simulation state on initialization of ecs systems
    public class InitStateSpawner : IEcsInitSystem
    {
        // auto injected fields
        private StateFactory _stateFactory;
        
        private IUserInfoService _userInfoService;
        private ITutorialService _tutorialService;

        public InitStateSpawner(IUserInfoService userInfoService, ITutorialService tutorialService)
        {
            _userInfoService = userInfoService;
            _tutorialService = tutorialService;
        }

        public void Init()
        {
            if (!_userInfoService.Registered)
            {
                bool localization = FeatureManagerFacade.FeatureEnabled(PWS.HUB.Simulation.Features.PWS_Localization);
                
                if(localization)
                    _stateFactory.SetState<LanguageSelectionState>();
                else
                    _stateFactory.SetState<RegistrationState>();

                return;
            }

            if (!_tutorialService.TutorialCompleted)
            {
                _stateFactory.SetState<TutorialProposalState>();
                return;
            }

            _stateFactory.SetState<RoomSelectionState>();
        }
    }
}