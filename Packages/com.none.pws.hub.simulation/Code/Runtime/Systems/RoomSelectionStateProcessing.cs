using Leopotam.Ecs;
using Modules.StateGroup.Components;
using Modules.StateGroup.Core;
using Modules.UIMessaging.Data;
using Modules.UIMessaging.Messages;
using PWS.HUB.Simulation.Components;
using PWS.Common.SceneSwitchService;
using UnityEngine.SceneManagement;

namespace PWS.HUB.Simulation
{
    public class RoomSelectionStateProcessing : IEcsRunSystem
    {
        private ISceneSwitchService _gameModeInfoService;
        private IPreviousSceneService _previousSceneService;

        private readonly EcsFilter<UIActionPerformedMessage> _actionPerformed;
        private TargetActionPair _show;
        private TargetActionPair _pottery;
        private TargetActionPair _woodcutter;
        private TargetActionPair _tutorial;

        private StringReference _potteryScene;
        private StringReference _woodcutterScene;
        private StringReference _tutorialScene;

        // auto injected fields
        readonly EcsFilter<RoomSelectionState> _inState;
        readonly EcsFilter<StateEnter, RoomSelectionState> _entering;
        private StateFactory _stateFactory;

        public RoomSelectionStateProcessing(ISceneSwitchService gameModeInfoService,
            IPreviousSceneService previousSceneService, TargetActionPair
            show, TargetActionPair pottery, TargetActionPair woodcutter,TargetActionPair tutorial,
            StringReference potteryScene, StringReference woodcutterScene,StringReference tutorialScene)
        {
            _gameModeInfoService = gameModeInfoService;
            _previousSceneService = previousSceneService;
            _show = show;
            _pottery = pottery;
            _woodcutter = woodcutter;
            _tutorial = tutorial;

            _potteryScene = potteryScene;
            _woodcutterScene = woodcutterScene;
            _tutorialScene = tutorialScene;
        }

        public void Run()
        {
            if (!_entering.IsEmpty())
            {
                UniRx.MessageBroker.Default.Publish(new PerformUIActionMessage(_show));
            }
            if (!_inState.IsEmpty())
            {
                foreach (var i in _actionPerformed)
                {
                    if (_pottery.IsIn(_actionPerformed.Get1(i)))
                    {
                        _previousSceneService.SetPreviousScene(SceneManager.GetActiveScene().name);
                        _gameModeInfoService.LoadScene(_potteryScene.Value);
                    } else if (_woodcutter.IsIn(_actionPerformed.Get1(i)))
                    {
                        _previousSceneService.SetPreviousScene(SceneManager.GetActiveScene().name);
                        _gameModeInfoService.LoadScene(_woodcutterScene.Value);
                    }
                    else if (_tutorial.IsIn(_actionPerformed.Get1(i)))
                    {
                        _previousSceneService.SetPreviousScene(SceneManager.GetActiveScene().name);
                        _gameModeInfoService.LoadScene(_tutorialScene.Value);
                    }
                }
            }
        }
    }
}
