using Leopotam.Ecs;
using Modules.StateGroup.Components;
using Modules.StateGroup.Core;
using Modules.UIMessaging.Data;
using Modules.UIMessaging.Messages;
using PWS.HUB.Simulation.Components;
using PWS.Tutorial;

namespace PWS.HUB.Simulation
{
    public class TutorialProposalStateProcessing : IEcsRunSystem
    {
        private ITutorialService _tutorialService;

        private readonly EcsFilter<UIActionPerformedMessage> _actionPerformed;
        private TargetActionPair _show;
        private TargetActionPair _yes;
        private TargetActionPair _no;

        // auto injected fields
        readonly EcsFilter<TutorialProposalState> _inState;
        readonly EcsFilter<StateEnter, TutorialProposalState> _entering;
        private StateFactory _stateFactory;

        public TutorialProposalStateProcessing(ITutorialService tutorialService, TargetActionPair show, TargetActionPair yes, TargetActionPair no)
        {
            _tutorialService = tutorialService;
            _show = show;
            _yes = yes;
            _no = no;
        }

        public void Run()
        {
            if (!_entering.IsEmpty())
            {
                UniRx.MessageBroker.Default.Publish(new PerformUIActionMessage(_show));
            }
            if (!_inState.IsEmpty())
            {
                foreach (var i in _actionPerformed)
                {
                    if (_yes.IsIn(_actionPerformed.Get1(i)))
                    {
                        _tutorialService.LoadTutorial();
                    } else if (_no.IsIn(_actionPerformed.Get1(i)))
                    {
                        _stateFactory.SetState<RoomSelectionState>();
                    }
                }
            }
        }
    }
}