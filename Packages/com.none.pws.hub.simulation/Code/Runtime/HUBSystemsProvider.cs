using Leopotam.Ecs;
using Modules.Root.ContainerComponentModel;
using Modules.Root.ECS;
using Modules.UIMessaging.Data;
using PWS.Common.UserInfoService;
using PWS.HUB.Simulation.Systems;
using PWS.Tutorial;
using UnityEngine;
using Modules.UIMessaging;
using PWS.Common.SceneSwitchService;

namespace PWS.HUB.Simulation
{
    internal class HUBSystemsDependencies
    {
        [Inject] public IUserInfoService UserInfoService;
        [Inject] public ITutorialService TutorialService;
        [Inject] public ISceneSwitchService GameModeInfoService;

        [Inject] public IStringToIDMappingService StringToIdMapping;
        [Inject] public IPreviousSceneService PreviousSceneService;
    }

    public class HUBSystemsProvider : MonoBehaviour, ISystemsProvider
    {

        [Header("Language Selection")]
        [SerializeField] private StringReference languageSelectionId;
        [SerializeField] private StringReference languageSelectionShowId;
        [SerializeField] private StringReference languageSelectedId;

        [Header("User Registration Pad")]
        [SerializeField] private StringReference registrationPadId;
        [SerializeField] private StringReference registrationPadShowId;
        [SerializeField] private StringReference registrationCompletedId;

        [Header("Tutorial Proposal")]
        [SerializeField] private StringReference tutorialProposalId;
        [SerializeField] private StringReference tutorialShowId;
        [SerializeField] private StringReference tutorialYesId;
        [SerializeField] private StringReference tutorialNoId;

        [Header("Room Selection")]
        [SerializeField] private StringReference roomSelectionId;
        [SerializeField] private StringReference roomSelectionShowId;
        [SerializeField] private StringReference roomSelectionPotteryId;
        [SerializeField] private StringReference roomSelectionWoodcutterId;
        [SerializeField] private StringReference roomSelectionTutorialId;
        [SerializeField] private StringReference potteryScene;
        [SerializeField] private StringReference woodcutterScene;
        [SerializeField] private StringReference tutorialScene;

        public EcsSystems GetSystems(EcsWorld world, EcsSystems endFrame, EcsSystems mainSystems)
        {
            HUBSystemsDependencies dependencies = new HUBSystemsDependencies();
            SceneContainer.Instance.Inject(dependencies);

            EcsSystems systems = new EcsSystems(world);

            //Language Selection
            var languageSelectionShow = dependencies.StringToIdMapping.GetTargetActionPair(languageSelectionId, languageSelectionShowId);
            var languageSelected = dependencies.StringToIdMapping.GetTargetActionPair(languageSelectionId, languageSelectedId);

            //registration pad
            var registrationShow = dependencies.StringToIdMapping.GetTargetActionPair(registrationPadId, registrationPadShowId);
            var registrationConfirm = dependencies.StringToIdMapping.GetTargetActionPair(registrationPadId, registrationCompletedId);

            //tutorial proposal
            var tutorialShow = dependencies.StringToIdMapping.GetTargetActionPair(tutorialProposalId, tutorialShowId);
            var tutorialYes = dependencies.StringToIdMapping.GetTargetActionPair(tutorialProposalId, tutorialYesId);
            var tutorialNo = dependencies.StringToIdMapping.GetTargetActionPair(tutorialProposalId, tutorialNoId);

            //room selection
            var roomShow = dependencies.StringToIdMapping.GetTargetActionPair(roomSelectionId, roomSelectionShowId);
            var roomPottery = dependencies.StringToIdMapping.GetTargetActionPair(roomSelectionId, roomSelectionPotteryId);
            var roomWoodcutter = dependencies.StringToIdMapping.GetTargetActionPair(roomSelectionId, roomSelectionWoodcutterId);
            var roomTutorial = dependencies.StringToIdMapping.GetTargetActionPair(roomSelectionId,
                roomSelectionTutorialId);

            systems
                .Add(new InitStateSpawner(dependencies.UserInfoService, dependencies.TutorialService))
                .Add(new LanguageSelectionStateProcessing(languageSelectionShow, languageSelected))
                .Add(new RegistrationStateProcessing(registrationShow, registrationConfirm))
                .Add(new TutorialProposalStateProcessing(dependencies.TutorialService, tutorialShow, tutorialYes, tutorialNo))
                .Add(new RoomSelectionStateProcessing(dependencies.GameModeInfoService,dependencies.PreviousSceneService, roomShow, 
                roomPottery, roomWoodcutter, roomTutorial, potteryScene, woodcutterScene,tutorialScene));

            return systems;
        }
    }
}