using I2.Loc;
using PWS.HUB.Simulation;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace PWS.FeatureToggles {

    public class SceneLoaderFeatureComponent : MonoBehaviour
    {
        [SerializeField]
        private bool _tutorialDoor;

        [SerializeField]
        private bool _potteryDoor;

        [SerializeField]
        private Localize _doorLocalize;
        [SerializeField]
        private Button _button;

        [SerializeField] 
        private UnityEvent _potteryClickEvent;
        [SerializeField] 
        private UnityEvent _woodcuttingClickEvent;
        [SerializeField]
        private UnityEvent _tutorialClickEvent;


        [SerializeField]
        private string _tutorialTerm, _potteryTerm, _woodcuttingTerm;

        private UnityAction _hashedAction;


        public void Start()
        {
            UnityAction tutorialAction = _tutorialClickEvent.Invoke;
            UnityAction potteryAction = _potteryClickEvent.Invoke;
            UnityAction woodcuttingAction = _woodcuttingClickEvent.Invoke;

            string term = "";

            if (FeatureManagerFacade.FeatureEnabled(Features.PWS_Pottery_SceneLoader)
                && FeatureManagerFacade.FeatureEnabled(Features.PWS_WoodCutting_SceneLoader))
            {
                term = _potteryDoor ? _potteryTerm : _woodcuttingTerm;
                _hashedAction = _potteryDoor ? potteryAction : woodcuttingAction;
            }
            else if (FeatureManagerFacade.FeatureEnabled(Features.PWS_Pottery_SceneLoader))
            {
                term = _tutorialDoor ? _tutorialTerm : _potteryTerm;
                _hashedAction = _tutorialDoor ? tutorialAction : potteryAction;
            }
            else if (FeatureManagerFacade.FeatureEnabled(Features.PWS_WoodCutting_SceneLoader))
            {
                term = _tutorialDoor ? _tutorialTerm : _woodcuttingTerm;
                _hashedAction = _tutorialDoor ? tutorialAction : woodcuttingAction;
            }
            else
            {
                term = _tutorialTerm;
                _hashedAction =tutorialAction;
            }

            _doorLocalize.SetTerm(term);
            _button.onClick.AddListener(_hashedAction);
        }

        private void OnDestroy()
        {
            _button.onClick.RemoveListener(_hashedAction);
        }
    }
}
