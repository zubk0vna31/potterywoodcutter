using Leopotam.Ecs;

namespace PWS.HUB.Simulation.Components
{
    // second state in hub, appears when tutorial not passed
    public struct TutorialProposalState : IEcsIgnoreInFilter
    {
        
    }
}