using Leopotam.Ecs;

namespace PWS.HUB.Simulation.Components
{
    // first state in hub, appears when user not registered
    public struct RegistrationState : IEcsIgnoreInFilter
    {
        
    }
}