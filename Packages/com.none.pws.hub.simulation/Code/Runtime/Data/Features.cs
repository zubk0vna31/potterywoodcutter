namespace PWS.HUB.Simulation
{
    public static class Features 
    {
        //Scene loaders
        public const string PWS_Pottery_SceneLoader = "pws.pottery.sceneloader";
        public const string PWS_WoodCutting_SceneLoader = "pws.woodcutting.sceneloader"; 
        public const string PWS_PotteryWoodCutting_SceneLoader = "pws.potterywoodcutting.sceneloader";
        public const string PWS_Achievements = "pws.achievements";
        public const string PWS_Localization = "pws.localization";

        public const string ReinforcingMessages = "ReinforcingMessages";
    }
}
