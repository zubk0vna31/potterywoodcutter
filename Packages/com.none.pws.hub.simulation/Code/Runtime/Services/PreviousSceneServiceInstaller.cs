using Modules.Root.ContainerComponentModel;

namespace PWS.HUB.Simulation
{

    public class PreviousSceneServiceInstaller : AMonoInstaller
    {
        public string playerPrefsSaveKey = "previousSceneName";

        public override void Install(IContainer container)
        {
            var previousSceneService = new PreviousSceneService(playerPrefsSaveKey);
            container.Bind(previousSceneService as IPreviousSceneService);
        }
    }
}
