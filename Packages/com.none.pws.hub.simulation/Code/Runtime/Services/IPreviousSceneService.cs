
namespace PWS.HUB.Simulation
{
    public interface IPreviousSceneService 
    {
        public void SetPreviousScene(string sceneName);

        public string GetPreviousScene();
    }
}
