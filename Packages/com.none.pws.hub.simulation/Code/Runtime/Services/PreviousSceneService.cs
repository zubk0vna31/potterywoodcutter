using UnityEngine;

namespace PWS.HUB.Simulation
{
    public class PreviousSceneService : IPreviousSceneService
    {
        private readonly string playerPrefsKey;

        public PreviousSceneService(string playerPrefsKey)
        {
            this.playerPrefsKey = playerPrefsKey;
            SetPreviousScene(GetPreviousScene());
        }

        public string GetPreviousScene()
        {
            if(!PlayerPrefs.HasKey(playerPrefsKey))
                SetPreviousScene(string.Empty);

            var value = PlayerPrefs.GetString(playerPrefsKey);

            return value;
        }

        public void SetPreviousScene(string sceneName)
        {
            PlayerPrefs.SetString(playerPrefsKey, sceneName);
        }
    }
}
