using Modules.Root.ContainerComponentModel;
using PWS.Tutorial;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace PWS.HUB.TutorialProposalWindow
{
    public class TutorialProposalWindow : MonoBehaviour
    {
        [Header("View")]
        [SerializeField] private GameObject _canvas;
        [SerializeField] private Button _yesButton;
        [SerializeField] private Button _noButton;

        [SerializeField] private UnityEvent _yesEvent;
        [SerializeField] private UnityEvent _noEvent;

        private void Awake()
        {
            AppContainer.Inject(this);
        }

        // Start is called before the first frame update
        void Start()
        {
            Hide();
            _yesButton.onClick.AddListener(OnClickYes);
            _noButton.onClick.AddListener(OnClickNo);
        }

        public void Show()
        {
            _canvas.SetActive(true);
        }

        public void Hide()
        {
            _canvas.SetActive(false);
        }

        public void OnClickYes()
        {
            //_tutorialService.LoadTutorial();
            _yesEvent.Invoke();
        }
        public void OnClickNo()
        {
            Hide();
            _noEvent.Invoke();
        }

    }
}
